<?php

namespace backend\controllers;

use Yii;
use common\models\BlogCategory;
use common\models\BlogCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use common\components\AccessRule;

/**
 * BlogCategoryController implements the CRUD actions for BlogCategory model.
 */
class BlogCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                     [

                        'actions' => ['login','index', 'error','logout','create','update','view','delete'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                    [

                        'actions' => ['login','index', 'error','logout','create','update','view','delete'],
                        'allow' => true,
                        'roles' => ['BlogLogin'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BlogCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $searchModel = new BlogCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BlogCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BlogCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new BlogCategory();

        if ($model->load(Yii::$app->request->post())) {
            $model->OnDate=date('Y-m-d');	
			/*Create Slug*/
			$slug = Yii::$app->request->post()['BlogCategory']['BlogCategoryName'];
			$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
			$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
			$slug = preg_replace('~[^-\w]+~', '', $slug);
			$slug = trim($slug, '-');
			$slug = preg_replace('~-+~', '-', $slug);
			$slug = strtolower($slug);				
			/*Check Slug*/
			$count = BlogCategory::find()
					->where([
						'Slug' => $slug
					])
					->count();
			if($count > 0){
				$model->Slug = $slug.'-'.rand(9, 999);
			}else{
				$model->Slug = $slug;
			}
			$model->MetaTitle = Yii::$app->request->post()['BlogCategory']['MetaTitle'];
			$model->MetaDescription = Yii::$app->request->post()['BlogCategory']['MetaDescription'];
			$model->MetaKeywords = Yii::$app->request->post()['BlogCategory']['MetaKeywords'];
			
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Blog Category Created Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
            }
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BlogCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
			
			$model->MetaTitle = Yii::$app->request->post()['BlogCategory']['MetaTitle'];
			$model->MetaDescription = Yii::$app->request->post()['BlogCategory']['MetaDescription'];
			$model->MetaKeywords = Yii::$app->request->post()['BlogCategory']['MetaKeywords'];
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "BlogCategory Updated Successfully");
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
                return $this->refresh();
            }
            return $this->redirect(['view', 'id' => $model->BlogCategoryId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BlogCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BlogCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BlogCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BlogCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
