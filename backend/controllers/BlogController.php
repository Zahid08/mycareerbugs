<?php
namespace backend\controllers;
use Yii;
use common\models\Blog;
use common\models\BlogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\BlogCategory;
use common\models\Documents;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

use yii\filters\AccessControl;
use common\components\AccessRule;
/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [

                        'actions' => ['login','index', 'error','logout','create','update','view','delete'],
                        'allow' => true,
                        'roles' => ['BlogLogin'],
                    ],
                    [

                        'actions' => ['login','index', 'error','logout','create','update','view','delete'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
        $searchModel = new BlogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new Blog();
        $docmodel=new Documents();
        $blogcategory=ArrayHelper::map(BlogCategory::find()->where(['IsDelete'=>0])->all(),'BlogCategoryId','BlogCategoryName');
        if ($model->load(Yii::$app->request->post())) {
             $image = UploadedFile::getInstance($model, 'BlogImageId');
                if($image)
                {
                $image_id=$docmodel->imageUpload($image,'Blogphoto');
                $model->BlogImageId=$image_id;
                $model->OnDate=date('Y-m-d');
				
				/*Create Slug*/
				$slug = Yii::$app->request->post()['Blog']['BlogName'];
				$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
				$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
				$slug = preg_replace('~[^-\w]+~', '', $slug);
				$slug = trim($slug, '-');
				$slug = preg_replace('~-+~', '-', $slug);
				$slug = strtolower($slug);
					
				/*check slug*/
				$count = Blog::find()
						->where([
							'BlogSlug' => $slug
						])
						->count();
				if($count > 0){
					$model->BlogSlug = $slug.'-'.rand(9, 999);
				}else{
					$model->BlogSlug = $slug;
				}
				
				$model->MetaTitle = Yii::$app->request->post()['Blog']['MetaTitle'];
				$model->MetaDescription = Yii::$app->request->post()['Blog']['MetaDescription'];
				$model->MetaKeywords = Yii::$app->request->post()['Blog']['MetaKeywords'];
				
				if($model->save())
                {
                    Yii::$app->session->setFlash('success', "Blog Created Successfully");
                }
                else{
                    Yii::$app->session->setFlash('error', "There is some error Please try again");
                }
                }
                else
                {
                    Yii::$app->session->setFlash('error', "Please provide blog image");
                }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,'blogcategory'=>$blogcategory
            ]);
        }
    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        
        $model = $this->findModel($id);
         $docmodel=new Documents();
        $blogcategory=ArrayHelper::map(BlogCategory::find()->where(['IsDelete'=>0])->all(),'BlogCategoryId','BlogCategoryName');
        $oldblogid=$model->BlogImageId;
        if ($model->load(Yii::$app->request->post())) {			
			$model->MetaTitle = Yii::$app->request->post()['Blog']['MetaTitle'];
			$model->MetaDescription = Yii::$app->request->post()['Blog']['MetaDescription'];
			$model->MetaKeywords = Yii::$app->request->post()['Blog']['MetaKeywords'];		
			
            $image = UploadedFile::getInstance($model, 'BlogImageId');
                if($image)
                {
                $image_id=$docmodel->imageUpload($image,'Blogphoto');
                $model->BlogImageId=$image_id;
                }
                else{
                    $model->BlogImageId=$oldblogid;
                }
                
               if($model->save())
            {
                Yii::$app->session->setFlash('success', "Blog Updated Successfully");
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,'blogcategory'=>$blogcategory
            ]);
        }
    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->IsDelete=1;
        if($model->save())
            {
                Yii::$app->session->setFlash('success', "Blog Deleted Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('success', "There is some error please try again");
            }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
