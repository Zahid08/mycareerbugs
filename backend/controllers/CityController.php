<?php

namespace backend\controllers;

use Yii;
use common\models\City;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;
use common\models\Documents;

use yii\filters\AccessControl;
use common\components\AccessRule;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [

                        'actions' => ['index','view','create','update','delete','showinfront'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => City::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new City();
         $docmodel=new Documents();  $docmodel1=new Documents();
        if ($model->load(Yii::$app->request->post())) {
             $image = UploadedFile::getInstance($model, 'Cityimage');
                if($image)
                {
                $image_id=$docmodel->imageUpload($image,'City');
                }
                else{
                    $image_id=0;
                }
                
            $whiteimage = UploadedFile::getInstance($model, 'CityWhiteImage');
                if($whiteimage)
                {
                $whiteimage_id=$docmodel1->imageUpload($whiteimage,'City');
                }
                else{
                    $whiteimage_id=0;
                }
            $model->Cityimage=$image_id;
            $model->CityWhiteImage=$whiteimage_id;
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "City Created Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
            }
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionShowinfront($id,$status)
    {
        $model=$this->findModel($id);
        $model->ShowInFront=$status;
        $model->save();
        return $this->redirect(['index']);
    }


    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldimageid=$model->Cityimage;$oldwhite=$model->CityWhiteImage;
        $docmodel=new Documents();  $docmodel1=new Documents();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'Cityimage');
                if($image)
                {
                $image_id=$docmodel->imageUpload($image,'City');
                }
                else{
                    $image_id=$oldimageid;
                }
            $whiteimage = UploadedFile::getInstance($model, 'CityWhiteImage');
                if($whiteimage)
                {
                $whiteimage_id=$docmodel1->imageUpload($whiteimage,'City');
                }
                else{
                    $whiteimage_id=$oldwhite;
                }
            $model->Cityimage=$image_id;
            $model->CityWhiteImage=$whiteimage_id;
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "City Updated Successfully");
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
