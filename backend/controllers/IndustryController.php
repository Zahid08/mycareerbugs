<?php

namespace backend\controllers;

use Yii;
use common\models\Industry;
use common\models\IndustrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Documents;

use yii\filters\AccessControl;
use common\components\AccessRule;
/**
 * IndustryController implements the CRUD actions for Industry model.
 */
class IndustryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [

                        'actions' => ['index','view','create','update','delete','showinfront','topcompany'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Industry models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $searchModel = new IndustrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Industry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Industry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new Industry();
         $docmodel=new Documents();  $docmodel1=new Documents();
        if ($model->load(Yii::$app->request->post())) {
            $model->OnDate=date('Y-m-d');
            $image = UploadedFile::getInstance($model, 'Imageid');
                if($image)
                {
                $image_id=$docmodel->imageUpload($image,'JobCategory');
                }
                else{
                    $image_id=0;
                }
                
            $whiteimage = UploadedFile::getInstance($model, 'ImageWhiteId');
                if($whiteimage)
                {
                $whiteimage_id=$docmodel1->imageUpload($whiteimage,'JobCategory');
                }
                else{
                    $whiteimage_id=0;
                }
            $model->Imageid=$image_id;
            $model->ImageWhiteId=$whiteimage_id;
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Industry Created Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
            }
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Industry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = $this->findModel($id);
        $oldimageid=$model->Imageid;$oldwhite=$model->ImageWhiteId;
        $docmodel=new Documents();  $docmodel1=new Documents();
        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'Imageid');
                if($image)
                {
                $image_id=$docmodel->imageUpload($image,'JobCategoryorrange');
                }
                else{
                    $image_id=$oldimageid;
                }
            $whiteimage = UploadedFile::getInstance($model, 'ImageWhiteId');
                if($whiteimage)
                {
                $whiteimage_id=$docmodel1->imageUpload($whiteimage,'JobCategorywhite');
                }
                else{
                    $whiteimage_id=$oldwhite;
                }
            $model->Imageid=$image_id;
            $model->ImageWhiteId=$whiteimage_id;
            
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Industry Updated Successfully");
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Industry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->IsDelete=1;
       if($model->save())
            {
                Yii::$app->session->setFlash('success', "Industry Deleted Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('success', "There is some error please try again");
            }

        return $this->redirect(['index']);
    }

     public function actionShowinfront($id,$status)
    {
        $model=$this->findModel($id);
        $model->ShowInFront=$status;
        $model->save();
        return $this->redirect(['index']);
    }
    public function actionTopcompany($id,$status)
    {
        $model=$this->findModel($id);
        $model->TopCompany=$status;
        $model->save();
        return $this->redirect(['index']);
    }
    /**
     * Finds the Industry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Industry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Industry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
