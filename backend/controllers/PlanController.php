<?php

namespace backend\controllers;

use Yii;
use common\models\Plan;
use common\models\PlanSearch;
use common\models\PlanAssign;
use common\models\AllUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

use yii\filters\AccessControl;
use common\components\AccessRule;
/**
 * PlanController implements the CRUD actions for Plan model.
 */
class PlanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
             'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [

                        'actions' => ['index','view','create','update','delete','planassign','viewassign','deleteassign'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Plan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PlanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Plan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Plan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Plan();
        if ($model->load(Yii::$app->request->post())) {
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Plan Created Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
            }
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionPlanassign()
    {
        $model = new PlanAssign();
     $employer=ArrayHelper::map(AllUser::find()->where(['IsDelete'=>0,'UserTypeId'=>3])->all(),'UserId','Name');
     $plan=ArrayHelper::map(Plan::find()->where(['IsDelete'=>0])->all(),'PlanId','PlanName');
        if ($model->load(Yii::$app->request->post())) {
            $plandetail=$this->findModel($model->PlanId);
            $model->TotalSMS=$plandetail->TotalSMS;
            $model->TotalEmail=$plandetail->TotalEmail;
            $model->TotalDownload=$plandetail->TotalDownload;
            $model->CVDownload=$plandetail->CVDownload;
            $model->ViewContact=$plandetail->ViewContact;
            $model->IsDelete=0;
            $model->OnDate=date('Y-m-d');
            if($model->save())
            {
                Yii::$app->session->setFlash('success', "Plan Assign Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
            }
            return $this->redirect(['planassign']);
        } else {
            return $this->render('planassign', [
                'model' => $model,'employer'=>$employer,'plan'=>$plan
            ]);
        }
    }
    
    public function actionViewassign()
    {
        $planassign=PlanAssign::find()->where(['IsDelete'=>0])->all();
        
        return $this->render('viewassign',['planassign'=>$planassign]);
    }
    
    public function actionDeleteassign($id)
    {
        $planassign=PlanAssign::find()->where(['PlanAssign'=>$id])->one();
        $planassign->IsDelete=1;
        if($planassign->save())
        {
          Yii::$app->session->setFlash('success', "Record Deleted Successfully");
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
            }
            return $this->redirect(['viewassign']);
    }

    /**
     * Updates an existing Plan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
             if($model->save())
            {
                Yii::$app->session->setFlash('success', "Plan Updated Successfully");
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
                return $this->refresh();
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Plan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->IsDelete=1;
         if($model->save())
            {
                Yii::$app->session->setFlash('success', "Plan deleted Successfully");
                return $this->redirect(['index']);
            }
            else
            {
                Yii::$app->session->setFlash('error', "There is some error Please try again");
                return $this->refresh();
            }
    }

    /**
     * Finds the Plan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Plan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Plan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
