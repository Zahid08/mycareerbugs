<?php
namespace backend\controllers;
use common\models\Notification;
use Yii;

use common\models\EmpNotification;
use common\models\PlanRecord;
use common\models\Follow;
use common\models\Messages;
use common\models\Likes;
use common\models\EmployeeSkill;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use yii\data\ActiveDataProvider;
use common\models\AppliedJob;
use common\models\AllUser;
use common\models\LikeToPost;
use common\models\AllUserSearch; 
use common\models\CandiReq;
use common\models\LeaveComment;
use common\models\CandiReqSearch; 
use yii\web\NotFoundHttpException; 
use common\models\Education;
use common\models\Experience;
use common\models\PlanAssign;
use common\models\ContactUs;
use common\models\PostJob;
use common\models\HrContactLimit;
use common\models\AppliedJobRequest;
use common\models\CompanyWallPost;
use common\models\CommentToPost;
use common\models\CampusPost;
use common\models\CampusCourseMap;
use common\models\CampusContact;
use common\models\Wallpostsetting;
use common\models\CollegePic;
use common\models\Skill;
use common\models\Course;
use common\models\Position;
use common\models\Documents;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use common\models\Industry;
use common\models\UserType;
use yii\helpers\Url;
use common\models\TeamMembers;
use yii\filters\AccessControl;
use common\components\AccessRule;

use yii\data\Pagination;

/**
 * AboutController implements the CRUD actions for About model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [

                        'actions' => ['candidatelist','candireq','hrlmt','conslmt','compslmt','candidatelist1','companylist','showinfront','candidatedetailview','contactuslist','deleteemployee','updatejoblimit','editemployeeview','deleteemployer','editemployee','employeedetail','employerdetail','campuslist','campusdetail','campuspost','campusjobdetail','deletecampusjob','deletecampus','campuscontactuslist','teamlist','teamdetail','deleteteam','approve','approve1','approveemp','campusstudent','wallpostsetting','hrlist','wallpost','jobpost','palnrequest','hrcontactlimit'],
                        'allow' => true,
                        'roles' => ['Admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST','GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return mixed
     */
    
    
     public function actionDeleteemployee($id)
    {
        $model = new AllUser();
        $employee = $model->find()->where(['UserId'=>$id])->one();
        $LeaveComment= LeaveComment::deleteAll(['CommentTo' =>$id]);
        $LeaveComment2= LeaveComment::deleteAll(['CommentFrom' =>$id]);
        $Follow= Follow::deleteAll(['FollowBy' =>$id]);
        $commentPost  =CommentToPost::deleteAll(['EmpId' =>$id]);
        $like       =Likes::deleteAll(['LikeFrom' =>$id]); 
        $like       =Likes::deleteAll(['LikeTo' =>$id]);
        $notifications =Notification::deleteAll(['UserId' =>$id]);
        $EmployeeSkill =EmpNotification::deleteAll(['UserId' =>$id]);
        $Messages  =Messages::deleteAll(['MessageTo' =>$id]);
        $MessagesF  =Messages::deleteAll(['MessageFrom' =>$id]);
        $EmployeeSkill =EmployeeSkill::deleteAll(['UserId' =>$id]);
        $education  =Education::deleteAll(['UserId' =>$id]);
        $appliedJOb= AppliedJob::deleteAll(['UserId' =>$id]);
        $appliedJOb= CollegePic::deleteAll(['CampusId' =>$id]);
        
         if (!$employee->delete()) {
                  throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
        
        // $employee->IsDelete=1;
        // if($employee->save())
        // {
        //     Yii::$app->session->setFlash('success', 'Employee deleted successfully');
        // }
        // else{
        //     Yii::$app->session->setFlash('error', 'There is some error, please try again');
        // }
        return $this->redirect(['candidatelist']);
    }
    
    public function actionDeleteteam($id)
    {
        $model = new AllUser();
        $employee = $model->find()->where(['UserId'=>$id])->one();
        $detailExperiance= Experience::deleteAll(['UserId' =>$id]);
        $appliedJOb= AppliedJob::deleteAll(['UserId' =>$id]);
        $appliedJOb= CollegePic::deleteAll(['CampusId' =>$id]);
        $teamMember= TeamMembers::deleteAll(['TeamId' =>$id]);
        $education  =Education::deleteAll(['UserId' =>$id]);
        $commentPost  =CommentToPost::deleteAll(['EmpId' =>$id]);
        $like       =Likes::deleteAll(['LikeFrom' =>$id]); 
        $like       =Likes::deleteAll(['LikeTo' =>$id]);
        $notifications =Notification::deleteAll(['UserId' =>$id]);
        $notifications =Notification::deleteAll(['UserId' =>$id]);
        $EmployeeSkill =EmpNotification::deleteAll(['UserId' =>$id]);
        
        if($employee){
             if (!$employee->delete()) {
                  throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
        }    
        // $employee->IsDelete=1;
        // if($employee->save())
        // {
        //     Yii::$app->session->setFlash('success', 'Team deleted successfully');
        // }
        // else{
        //     Yii::$app->session->setFlash('error', 'There is some error, please try again');
        // }
        
        return $this->redirect(['teamlist']);
    }
    
    public function actionEmployeedetail($id){
    $employee = AllUser::find()->where(['UserId'=>$id])->one();
    return $this->render('employeedetail', ['model' => $employee,]);
    }
    
    
	public function actionUpdatejoblimit($id){
	    $post=Yii::$app->request->post();
	    if(!empty($post['Job'])){
	        foreach($post['Job'] as $key=>$items){
	           $postJob = PostJob::findOne($items['JobId']);
	        
	           if($postJob){
	               $postJob->viewLimit=$items['limit_set'];
	                $postJob->update(false);
	           }
	        
	        }
	    }
	    
       $allpost = PostJob::find()->where([
            'JobStatus' => 0,
            'IsDelete' => 0,
            'EmployerId' => $id
        ])->orderBy([
                'OnDate' => SORT_DESC
            ])
            ->all();
                    
	    
	    	$allusr = AllUser::findOne($id);
	    	
	  
	   return $this->render('editemployeeview',['allusr'=>$allusr,'allpost'=>$allpost]);
	}
    
	public function actionEditemployeeview($id){
		
		   $allpost = PostJob::find()->where([
                    'JobStatus' => 0,
                    'IsDelete' => 0,
                    'EmployerId' => $id
                ])->orderBy([
                        'OnDate' => SORT_DESC
                    ])
                    ->all();
                    
                  
                    
		$allusr = AllUser::findOne($id);
		
		 if ($allusr->load(Yii::$app->request->post())) {
			 $allusr->viewlimit = Yii::$app->request->post('AllUser')['viewlimit'];
			 $allusr->update(false);
		    Yii::$app->session->setFlash('success', "Profile Updated Successfully");
            return $this->render('editemployeeview',['allusr'=>$allusr,'allpost'=>$allpost]);
         
		 }
		 
		
            return $this->render('editemployeeview',['allusr'=>$allusr,'allpost'=>$allpost]);
	}
		
		
	
	public function actionEditemployee($id){
	
    $employee = AllUser::find()->where(['UserId'=>$id])->one();
	
	$alluser=new AllUser();
        $profile=$alluser->find()->where(['UserId'=>$id])->one();
        if($profile->PhotoId!=0)
        {
            $url='/backend/web/';
            $pimage=$url.$profile->photo->Doc;
        }
        else
        {
            $pimage='/images/user.png';
        }
        
        $skill=new Skill();
        $position=new Position();
        $course=new Course();
        $docmodel=new Documents();
        $docmodel1=new Documents();
        
        $allskill=$skill->find()->where("IsDelete=0")->all();
        $allposition=$position->find()->where("IsDelete=0")->all();
        $allcourse=$course->find()->where("IsDelete=0")->all();
        $allindustry= ArrayHelper::map(Industry::find()->where(['IsDelete'=>0])->orderBy(['IndustryName'=>'SORT_ASC'])->all(),'IndustryId','IndustryName');
        $cvid=$profile->CVId;
        $photoid=$profile->PhotoId;
        
        if ($profile->load(Yii::$app->request->post())) {
            $cv = UploadedFile::getInstance($profile, 'CVId');
            if($cv)
            {
            $cv_id=$docmodel->imageUpload($cv,'CVId');
            }
            else
            {
                $cv_id=$cvid;
            }
            
            $photo_id=$photoid;
            if($docmodel1->load(Yii::$app->request->post()))
                {
                     $docmodel1->image = \yii\web\UploadedFile::getInstance($docmodel1, 'Doc');
                    if($docmodel1->image)
                    {
                    $image=UploadedFile::getInstance($docmodel1, 'Doc');
                    $ext = end((explode(".", $image->name)));
                    $ppp=Yii::$app->security->generateRandomString();
                    $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";
                    $docmodel1->Doc = 'imageupload/'. $ppp.".{$ext}";
                    $docmodel1->OnDate=date('Y-m-d');
                    $docmodel1->From='photo';
                            if ($docmodel1->save()) 
                            {
                                $photo_id=$docmodel1->DocId;
                            }
                            else
                            {
                               $photo_id=$photoid;
                            }
                    }
                    
                }
                
                
                
            if(Yii::$app->request->post()['AllUser']['CompanyName']!='')
            {
            $profile->EntryType='Experience';
            }
            else
            {
            $profile->EntryType='Fresher';    
            }
            $profile->CVId=$cv_id;
            $profile->PhotoId=$photo_id;
            $profile->save();
            
            if($photo_id!=0)
                {
                    $profile=$alluser->find()->where(['UserId'=>$id])->one();
                    $url='/backend/web/';
                    $pimage=$url.$profile->photo->Doc;
                }
                else
                {
                    $pimage='/images/user.png';
                }
             /*
            $education=Education::find()->where(['UserId'=>$profile->UserId])->one();
            if($education)
            {
            $education->HighestQualification='none';
            $education->CourseId=Yii::$app->request->post()['AllUser']['CourseId'];
            $education->University=Yii::$app->request->post()['AllUser']['University'];
            $education->SkillId=Yii::$app->request->post()['AllUser']['SkillId'];
            $education->PassingYear=Yii::$app->request->post()['AllUser']['PassingYear'];
            $education->DurationTo=Yii::$app->request->post()['AllUser']['PassingYear'];
            $education->save();
            }
            else
            {
              $education=new Education();
              $education->UserId=$profile->UserId;
              $education->CourseId=Yii::$app->request->post()['AllUser']['CourseId'];
              $education->PassingYear=Yii::$app->request->post()['AllUser']['PassingYear'];
              $education->DurationTo=Yii::$app->request->post()['AllUser']['PassingYear'];
              $education->University=Yii::$app->request->post()['AllUser']['University'];
              $education->SkillId=Yii::$app->request->post()['AllUser']['SkillId'];
              $education->OnDate=date('Y-m-d');
              $education->save();
            }*/
            
            $IsExp=0;
			if(isset(Yii::$app->request->post()['AllUser']['CompanyName'])){
            foreach(Yii::$app->request->post()['AllUser']['CompanyName'] as $key=>$val)
            {
                if(Yii::$app->request->post()['AllUser']['CompanyName'][$key]!='')
                {
                    $IsExp=1;
                if(isset(Yii::$app->request->post()['AllUser']['ExperienceId'][$key]) && Yii::$app->request->post()['AllUser']['ExperienceId'][$key]!='')
                {
					
                $experience=Experience::find()->where(['ExperienceId'=>Yii::$app->request->post()['AllUser']['ExperienceId'][$key]])->one();
                
                $experience->CompanyName=Yii::$app->request->post()['AllUser']['CompanyName'][$key];
                $experience->PositionId=Yii::$app->request->post()['AllUser']['PositionId'][$key];
                $experience->IndustryId=Yii::$app->request->post()['AllUser']['CIndustryId'][$key];
                $experience->Experience=Yii::$app->request->post()['AllUser']['Experience'][$key];
                $experience->YearFrom=Yii::$app->request->post()['AllUser']['YearFrom'][$key];
                $experience->YearTo=Yii::$app->request->post()['AllUser']['YearTo'][$key];
                $experience->Salary=Yii::$app->request->post()['AllUser']['Salary'][$key];
                $experience->save();
                }
                else
                {
					
                $experience=new Experience();
                $experience->UserId=$profile->UserId;
                $experience->CompanyName=Yii::$app->request->post()['AllUser']['CompanyName'][$key];
                $experience->PositionId=Yii::$app->request->post()['AllUser']['PositionId'][$key];
                $experience->IndustryId=Yii::$app->request->post()['AllUser']['CIndustryId'][$key];
				$experience->YearFrom=Yii::$app->request->post()['AllUser']['YearFrom'][$key];
				
                
                $experience->YearTo=Yii::$app->request->post()['AllUser']['YearTo'][$key];
				if(!Yii::$app->request->post()['AllUser']['YearTo'][$key])
				{
					$experience->YearTo='Till Now';
				}
                $experience->Experience=Yii::$app->request->post()['AllUser']['Experience'][$key];
                $experience->Salary=Yii::$app->request->post()['AllUser']['Salary'][$key];
                $experience->OnDate=date('Y-m-d');
                $experience->save();
                
                }
                }
            }
		}
			
			if(isset(Yii::$app->request->post()['AllUser']['RawSkill'])){
            EmployeeSkill::deleteAll(['UserId' => $profile->UserId]);
            
            //new skill
            $rawskill=explode(",",Yii::$app->request->post()['AllUser']['RawSkill']);
            foreach($rawskill as $rk=>$rval)
            {
                $indskill=trim($rval);
                $nskill=new Skill();
                $cskill=$nskill->find()->where(['Skill'=>$indskill,'IsDelete'=>0])->one();
                if(!$cskill)
                {
                    $nskill->Skill=$indskill;
                    $nskill->OnDate=date('Y-m-d');
                    $nskill->save();
                    $skid=$nskill->SkillId;
                }
                else
                {
                    $skid=$cskill->SkillId;
                }
                    $empskill=new EmployeeSkill();
                    $empskill->SkillId=$skid;
                    $empskill->UserId=$profile->UserId;
                    $empskill->OnDate=date('Y-m-d');
                    $empskill->save();
            }
		}
            
            if($IsExp==1)
            {
                $uuser=AllUser::find()->where(['UserId'=>$profile->UserId])->one();
                $uuser->IsExp=$IsExp;
                $uuser->save();
            }
            
            //return $this->render('editprofile',['profile'=>$profile,'skill'=>$allskill,'position'=>$allposition,'course'=>$allcourse]);
            Yii::$app->session->setFlash('success', "Profile Updated Successfully");
            return $this->render('editemployee',['pimage'=>$pimage,'profile'=>$profile,'skill'=>$allskill,'position'=>$allposition,'course'=>$allcourse,'industry'=>$allindustry,'docmodel'=>$docmodel1]);
            }
             else{
            return $this->render('editemployee',['pimage'=>$pimage,'profile'=>$profile,'skill'=>$allskill,'position'=>$allposition,'course'=>$allcourse,'industry'=>$allindustry,'docmodel'=>$docmodel1]);
        }
	
    }
    
    public function actionEmployerdetail($id){
    $employer = AllUser::find()->where(['UserId'=>$id])->one();
    $totaljob=PostJob::find()->where(['EmployerId'=>$id])->count();
    $totalopenjob=PostJob::find()->where(['EmployerId'=>$id,'JobStatus'=>0])->count();
    return $this->render('employerdetail', ['model' => $employer,'totaljob'=>$totaljob,'totalopenjob'=>$totalopenjob]);
    }
	
	public function actionCandidatelist()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $searchModel = new AllUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->Where(['UserTypeId'=>2,'IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        
        return $this->render('candidatelist', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			
        ]);
    }
	
		public function actionCandidatelist1()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $dataProvider = AllUser::find()->where(['UserTypeId'=>2,'IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        
        return $this->render('candidatelistold', [
            'dataProvider' => $dataProvider,
        ]);
    }
	
    
    public function actionCompanylist()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
        $cmpdata = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0])
		->andWhere(['!=','EntryType','HR'])
		->orderBy(['UpdatedDate'=>SORT_DESC]);
		
		
		$ct =  AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0])
		->andWhere(['!=','EntryType','HR'])
		->orderBy(['UpdatedDate'=>SORT_DESC])->count();
		
		$usrdr = new AllUser();
		if ($usrdr->load(Yii::$app->request->post())) {
		if(Yii::$app->request->post('AllUser')['Name'] != '')
		{
        $cmpdata = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0,'Name' => Yii::$app->request->post('AllUser')['Name']])
		->andWhere(['!=','EntryType','HR'])
		->orderBy(['UpdatedDate'=>SORT_DESC]); 
		$ct =  AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0,'Name' => Yii::$app->request->post('AllUser')['Name']])
		->andWhere(['!=','EntryType','HR'])
		->orderBy(['UpdatedDate'=>SORT_DESC])->count();
		
		}
		}
		  
		$pages = new Pagination(['totalCount' => $ct]);
		$models = $cmpdata->offset($pages->offset)->limit($pages->limit)->all(); 

  
			
        return $this->render('companylist', [
            'models' => $models,
            'usrdr' => $usrdr,
            'pages' => $pages,
        ]);
    }
	
	public function actionHrlist($id = NULL )
    {
		
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        } 
		
		
		if($id != ''){
		 
			    $usr = AllUser::find()->where(['UserId' => $id])->one();
			    $PlanAssign= PlanAssign::deleteAll(['EmployerId' =>$id]);
			    $education  =Education::deleteAll(['UserId' =>$id]);
			    
    			$wallPost = CompanyWallPost::find()->where(['EmployerId' => $id])->one();
    			if($wallPost){
    			     $WallPostId=$wallPost->WallPostId;
    			      $like =LikeToPost::deleteAll(['PostId' =>$WallPostId]); 
    			 }
    			 $jobPostList=PostJob::find()->where(['EmployerId'=>$id,'JobStatus'=>0])->one();
                if($jobPostList){
                    $jobId=$jobPostList->JobId;
                    $JobRelatedSkill    =JobRelatedSkill::deleteAll(['JobId'=>$jobId]);
                }
        
			    $CompanyWallPost= CompanyWallPost::deleteAll(['EmployerId' =>$id]);
                $commentPost  =CommentToPost::deleteAll(['EmpId' =>$id]);
                
                $like       =Likes::deleteAll(['LikeFrom' =>$id]); 
                $like       =Likes::deleteAll(['LikeTo' =>$id]);
                $notifications =Notification::deleteAll(['UserId' =>$id]);
                $EmployeeSkill =EmpNotification::deleteAll(['UserId' =>$id]);
              
			if (!$usr->delete()) {
                  throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
            }
            
		    
		}
			 
			 
        $dataProvider = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0, 'EntryType'=>'HR'])->orderBy(['UpdatedDate'=>SORT_DESC]);
		$ct = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0, 'EntryType'=>'HR'])->orderBy(['UpdatedDate'=>SORT_DESC])->count();
		$usrdr = new AllUser();
		if ($usrdr->load(Yii::$app->request->post())) {
			 if(Yii::$app->request->post('AllUser')['Name'] != '')
		{
        $dataProvider = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0, 'EntryType'=>'HR','Name'=> Yii::$app->request->post('AllUser')['Name']])->orderBy(['UpdatedDate'=>SORT_DESC]);
		$ct = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0, 'EntryType'=>'HR','Name'=> Yii::$app->request->post('AllUser')['Name']])->orderBy(['UpdatedDate'=>SORT_DESC])->count();
		} 
		}
		
	 
		$pages = new Pagination(['totalCount' => $ct]);
		$models = $dataProvider->offset($pages->offset)->limit($pages->limit)->all(); 

		 
        return $this->render('hrlist', [
            'models' => $models,
            'usrdr' => $usrdr,
            'pages' => $pages,
        ]);
    }
	
	public function actionWallpost()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
		$query = CompanyWallPost::find()->where(['IsDelete' => 0]);
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		
		$allpost = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
		
		//$allpost=CompanyWallPost::find()->where(['IsDelete'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
		
		
		if(Yii::$app->request->post()['save']=='Save' && Yii::$app->request->post()['wallpostaction']=='delete'){
			$wallPostData = Yii::$app->request->post()['wallPost'];
			
			foreach($wallPostData as $val){
				
				Yii::$app->db->createCommand('UPDATE CompanyWallPost SET IsDelete=1 WHERE WallPostId="'.$val.'"')->execute();
				
			}
			
			Yii::$app->session->setFlash('success', 'Wall post setting save successfully');
		}
		
		if(Yii::$app->request->post()['save']=='Save' && Yii::$app->request->post()['wallpostaction']=='hide'){
			$wallPostData = Yii::$app->request->post()['wallPost'];
			foreach($wallPostData as $val){
				Yii::$app->db->createCommand('UPDATE CompanyWallPost SET IsHide=1 WHERE WallPostId="'.$val.'"')->execute();
			}
			Yii::$app->session->setFlash('success', 'Wall post setting save successfully');
		}
		
		if(Yii::$app->request->post()['save']=='Save' && Yii::$app->request->post()['wallpostaction']=='unhide'){
			$wallPostData = Yii::$app->request->post()['wallPost'];
			foreach($wallPostData as $val){
				Yii::$app->db->createCommand('UPDATE CompanyWallPost SET IsHide=0 WHERE WallPostId="'.$val.'"')->execute();
			}
			Yii::$app->session->setFlash('success', 'Wall post setting save successfully');
		}
		
        return $this->render('wallpost', [
            'allpost' => $allpost,
			'pages' => $pages
        ]);
    }
	
	public function actionJobpost()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
		$query = PostJob::find()->where(['IsDelete' => 0]);
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		
		$allpost = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
		
		
		
		if(Yii::$app->request->post()['save']=='Save' && Yii::$app->request->post()['jobpostaction']=='delete'){
			$jobPostData = Yii::$app->request->post()['JobPost'];
			
			foreach($jobPostData as $val){
				
				Yii::$app->db->createCommand('UPDATE PostJob SET IsDelete=1 WHERE JobId="'.$val.'"')->execute();
				
			}
			
			Yii::$app->session->setFlash('success', 'Job post setting save successfully');
		}
		
		if(Yii::$app->request->post()['save']=='Save' && Yii::$app->request->post()['jobpostaction']=='hide'){
			$jobPostData = Yii::$app->request->post()['JobPost'];
			foreach($jobPostData as $val){
				Yii::$app->db->createCommand('UPDATE PostJob SET IsHide=1 WHERE JobId="'.$val.'"')->execute();
			}
			Yii::$app->session->setFlash('success', 'Job post setting save successfully');
		}
		
		if(Yii::$app->request->post()['save']=='Save' && Yii::$app->request->post()['jobpostaction']=='unhide'){
			$jobPostData = Yii::$app->request->post()['JobPost'];
			foreach($jobPostData as $val){
				Yii::$app->db->createCommand('UPDATE PostJob SET IsHide=0 WHERE JobId="'.$val.'"')->execute();
			}
			Yii::$app->session->setFlash('success', 'Job post setting save successfully');
		}
		
        return $this->render('jobpost', [
            'allpost' => $allpost,
			'pages' => $pages
        ]);
    }
	
	
    public function actionWallpostsetting()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
		$query = AllUser::find()->where(['UserTypeId'=>3,'IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC]);
		$countQuery = clone $query;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		
		$dataProvider = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
		
				
		if(Yii::$app->request->post()['save']=='Save'){
			Yii::$app->db->createCommand()->truncateTable('Wallpostsetting')->execute();
			$wallPostSettingdata = Yii::$app->request->post()['wallPostSetting'];
			foreach($wallPostSettingdata as $key=>$val){
				$wallpostsetting=new Wallpostsetting();
				$wallpostsetting->companyId = $key;
				$wallpostsetting->wallPostSetting = $val;
				$wallpostsetting->save();
			}
			Yii::$app->session->setFlash('success', 'Wall post setting save successfully');
		}
		$newdata = $tempdata = array();
		foreach($dataProvider as $dProvider){
			$newdata['UserId'] = $dProvider->UserId;
			$newdata['LogoId'] = $dProvider->LogoId;
			$newdata['IndustryName'] = $dProvider->industry->IndustryName;
			$newdata['Doc'] = $dProvider->logo->Doc;
			$newdata['ContactPerson'] = $dProvider->ContactPerson;
			$newdata['MobileNo'] = $dProvider->MobileNo;
			$newdata['ContactNo'] = $dProvider->ContactNo;
			$Wallpostsetting=Wallpostsetting::find()->where(['companyId'=>$dProvider->UserId])->one();
			$newdata['wallPostSetting'] = $Wallpostsetting->wallPostSetting;
			
			$tempdata[] = $newdata;
		}
		
		
        return $this->render('wallpostsetting', [
            'dataProvider' => $tempdata,
			'pages' => $pages
        ]);
    }
	
    public function actionCampuslist()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $dataProvider = AllUser::find()->where(['UserTypeId'=>4,'IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        return $this->render('campuslist', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTeamlist()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $dataProvider = AllUser::find()->where(['UserTypeId'=>5,'IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        return $this->render('teamlist', [
            'dataProvider' => $dataProvider,
        ]);
    } 
	
	
	// Applied Candidate 
	
	
    public function actionCandireq()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		 
		$searchModel = new CandiReqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		 
        return $this->render('candireq', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 
			
        ]);
	}
    
    public function actionHrlmt()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
		$alusr = new AllUser();
		$searchModel = new AllUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->Where(['EntryType'=>'HR','IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        
		
		 if ($alusr->load(Yii::$app->request->post())) {
			
			 AllUser::updateAll(['viewlimit' => Yii::$app->request->post('AllUser')['viewlimit']], ['like', 'EntryType', 'HR']);
			  Yii::$app->session->setFlash('success', "All HR Profile Updated Successfully");
           
		 }
		 
		 
		 
		 
        return $this->render('hrlmt', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'alusr' => $alusr,
			
        ]);
		
		
		 
    }
	
	
	
    
    public function actionConslmt()
    {
		
		 if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
		$searchModel = new AllUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->Where(['EntryType'=>'Consultancy','IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        $alusr = new AllUser();
		 if ($alusr->load(Yii::$app->request->post())) {
			
			 AllUser::updateAll(['viewlimit' => Yii::$app->request->post('AllUser')['viewlimit']], ['like', 'EntryType', 'Consultancy']);
			  Yii::$app->session->setFlash('success', "All Consultancy Profile Updated Successfully");
           
		 }
		 
		 
        return $this->render('conslmt', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'alusr' => $alusr,
			
        ]);
		
		
		 
    }
	
	
	
    
    public function actionCompslmt()
    {
		
		
		 if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
		
		$searchModel = new AllUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->query->Where(['EntryType'=>'Company','IsDelete'=>0])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        $alusr = new AllUser();
		 if ($alusr->load(Yii::$app->request->post())) {
			
			 AllUser::updateAll(['viewlimit' => Yii::$app->request->post('AllUser')['viewlimit']], ['like', 'EntryType', 'Company']);
			  Yii::$app->session->setFlash('success', "All Company Profile Updated Successfully");
           
		 }
		 
        return $this->render('compslmt', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'alusr' => $alusr,
			
        ]); 
    }
	
	
    
    public function actionCampusdetail($Id)
    {
        $employer = AllUser::find()->where(['UserId'=>$Id])->one();
        $totaljob=CampusPost::find()->where(['CampusId'=>$Id])->count();
        $totalopenjob=CampusPost::find()->where(['CampusId'=>$Id,'JobStatus'=>0])->count();
        $campuscourse=CampusCourseMap::find()->where(['CampusId'=>$Id])->orderBy(['CourseId'=>SORT_ASC])->all();
        return $this->render('campusdetail', ['model' => $employer,'totaljob'=>$totaljob,'totalopenjob'=>$totalopenjob,'campuscourse'=>$campuscourse]);
    }
    
    public function actionTeamdetail($Id)
    {
        $team = AllUser::find()->where(['UserId'=>$Id])->one();
        return $this->render('teamdetail', ['model' => $team,]);
    }
    
    public function actionShowinfront($id,$status)
    {
        $model=AllUser::find()->where(['UserId'=>$id])->one();
        $model->ShowInFront=$status;
        $model->save();
        return $this->redirect(['companylist']);
    }
    
     public function actionDeleteemployer($id)
    {
        $model =new AllUser();
        $employer   =$model->find()->where(['UserId'=>$id])->one();
        $jobPostList=PostJob::find()->where(['EmployerId'=>$id,'JobStatus'=>0])->one();
        if($jobPostList){
            $jobId=$jobPostList->JobId;
            $JobRelatedSkill    =JobRelatedSkill::deleteAll(['JobId'=>$jobId]);
        }
        
        $jobpost    =PostJob::deleteAll(['EmployerId'=>$id,'JobStatus'=>0]);
        $PlanAssign= PlanAssign::deleteAll(['EmployerId' =>$id]);
        $CompanyWallPost= CompanyWallPost::deleteAll(['EmployerId' =>$id]);
        $CompanyWallPost= CompanyWallPost::deleteAll(['EmployerId' =>$id]);
        $PlanRecord    =PlanRecord::deleteAll(['EmployerId'=>$id]);
        
        $commentPost  =CommentToPost::deleteAll(['EmpId' =>$id]);
        $like       =Likes::deleteAll(['LikeFrom' =>$id]); 
        $like       =Likes::deleteAll(['LikeTo' =>$id]);
        $notifications =Notification::deleteAll(['UserId' =>$id]);
        $EmployeeSkill =EmpNotification::deleteAll(['UserId' =>$id]);
        $education  =Education::deleteAll(['UserId' =>$id]);
        
        if (!$employer->delete()) {
              throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
        }
       
        Yii::$app->session->setFlash('success', 'Employer deleted successfully');
       
        return $this->redirect(['companylist']);
    }
    
    public function actionCandidatedetailview($userid)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $userdetail=AllUser::find()->where(['UserId'=>$userid])->one();
        return $this->render('candidatedetailview',['profile'=>$userdetail]);
    }
    
    public function actionContactuslist()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $contactdetail=ContactUs::find()->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        return $this->render('contactuslist',['contactdetail'=>$contactdetail]);
    }
    
    public function actionCampuscontactuslist()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $contactdetail=CampusContact::find()->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        return $this->render('campuscontactuslist',['contactdetail'=>$contactdetail]);
    }
    
    public function actionCampuspost()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new CampusPost();
        $alljob = $model->find()->where(['IsDelete'=>0,'Jobstatus'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
        return $this->render('campusjob', [
                'model' => $alljob,
            ]);
    }
    
    public function actionCampusjobdetail($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new CampusPost();
        $onejob = $model->find()->where(['IsDelete'=>0,'JobStatus'=>0,'CampusPostId'=>$id])->one();
        return $this->render('campusjobdetail', [
                'model' => $onejob,
            ]);
    }
    
    public function actionDeletecampusjob($id)
    {
         $model = new CampusPost();
        $job = $model->find()->where(['CampusPostId'=>$id])->one();
        $job->IsDelete=1;
        $job->JobStatus=1;
        $job->save();      
        return $this->redirect(['campuspost']);
    }
    
    public function actionDeletecampus($id)
    {
        // $jobpost=CampusPost::find()->where(['CampusId'=>$id,'JobStatus'=>0])->all();
        // foreach($jobpost as $k=>$v)
        // {
        //     $jb=CampusPost::find()->where(['CampusPostId'=>$v->CampusPostId])->one();
        //     $jb->JobStatus=1;
        //     $jb->save();
        // }
        // //if($jobpost)
        // //{
        // //    Yii::$app->session->setFlash('error', 'Can not delete this employeer because a job is posted by this employer till  open ');
        // //}
        // //else
        // //{
        // $model = new AllUser();
        // $employer = $model->find()->where(['UserId'=>$id])->one();
        // $employer->IsDelete=1;
        // $employer->save();
        
        $model = new AllUser();
        $employee = $model->find()->where(['UserId'=>$id])->one();
        $appliedJOb= AppliedJob::deleteAll(['UserId' =>$id]);
        $appliedJOb= CollegePic::deleteAll(['CampusId' =>$id]);
        $teamMember= TeamMembers::deleteAll(['TeamId' =>$id]);
        $education  =Education::deleteAll(['UserId' =>$id]);
        $commentPost  =CommentToPost::deleteAll(['EmpId' =>$id]);
        $like       =Likes::deleteAll(['LikeFrom' =>$id]); 
        $like       =Likes::deleteAll(['LikeTo' =>$id]);
        $notifications =Notification::deleteAll(['UserId' =>$id]);
        $EmployeeSkill =EmployeeSkill::deleteAll(['UserId' =>$id]);
        $EmployeeSkill =EmpNotification::deleteAll(['UserId' =>$id]);
         
           if (!$employee->delete()) {
              throw new Exception(Yii::t('app', 'The record cannot be deleted.'));
        }
        
        Yii::$app->session->setFlash('success', 'Campus deleted successfully');
        //}
        return $this->redirect(['campuslist']);
    }
    
    public function actionApprove($id)
    {
        $user=AllUser::find()->where(['UserId'=>$id])->one();
        if($user->IsApprove==0 )
        {
            $user->IsApprove=1;
            $user->save(false);
        }
        else
        {
            $user->IsApprove=0;
            $user->save(false);
        }
        return $this->redirect(['candidatelist']);
    }
     public function actionApprove1($id,$type)
    {
        $user=AllUser::find()->where(['UserId'=>$id])->one();
        if($type=='approve')
        {
            $user->IsApprove=1;
            $user->save();
        }
        else
        {
            $user->IsApprove=0;
            $user->save();
        }
        
        return $this->redirect(['candidatelist1']);
    }
     
    public function actionApproveemp($id,$type)
    {
        $user=AllUser::find()->where(['UserId'=>$id])->one();
        if($type=='approve')
        {
            $user->IsApprove=1;
            $user->save();
        }
        else
        {
            $user->IsApprove=0;
            $user->save();
        }
        
        return $this->redirect(['employerdetail','id'=>$id]);
    }
    
    public function actionCampusstudent($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $dataProvider = AllUser::find()->where(['UserTypeId'=>2,'IsDelete'=>0,'CampusId'=>$id])->orderBy(['UpdatedDate'=>SORT_DESC])->all();
        
        return $this->render('candidatelist', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionPalnrequest()
    {
        if(isset($_GET['approve']) && !empty($_GET['approve'])){
            $model = AppliedJobRequest::find()->where(['id'=>$_GET['approve']])->one();
            if(count($model)>0){
                $model->is_approve = '1';
                $model->approve_on = date('Y-m-d H:i:s');
                $model->save(false);
                return $this->redirect(['palnrequest']);
            }
        }
        $dataProvider = AppliedJobRequest::find()->all();

        return $this->render('planapprovelist', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionHrcontactlimit()
    {
        $model = HrContactLimit::find()->where(['id'=>1])->one();
        if(empty($model)){
            $model = new HrContactLimit();
        }
        if(isset($_POST['HrContactLimit'])){
            $model->allow_limit = $_POST['HrContactLimit']['allow_limit'];
            if($model->validate()){
                $model->save();
            }
        }
        return $this->render('hrcontactlimit', [
            'model' => $model,
        ]);
    }
    
}
