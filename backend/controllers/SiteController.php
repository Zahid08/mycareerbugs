<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use common\models\LoginForm;

use common\models\AllUser;
use common\models\Industry;
use common\models\UserType;
use yii\heleprs\Url;


use common\models\Skill;
use common\models\Course;
use common\models\Position;
use common\models\Documents;
use common\models\Education;
use common\models\Experience;
use common\models\ContactUs;
use common\models\PostJob;
use common\models\CampusPost;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use common\models\NewsLetter;
use common\models\CompanyWallPost;
use common\models\User;
use yii\web\UploadedFile;
use common\models\PasswordForm;
use common\models\JobRelevant;

use yii\filters\AccessControl;
use common\components\AccessRule;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                    ],
                    [

                        'actions' => ['login','index', 'error','alljob','deletejob','allemployee','deleteemployee','allemployer','deleteemployer','newslettersubscriber','deletenews','topjob','logout','jobdetail','editlogo','changepassword','feedbackrecord'],
                        'allow' => true,
                        'roles' => ['Admin','CandiateList','CompanyList'],
                    ],
                    [

                        'actions' => ['login','index', 'error','logout','changepassword'],
                        'allow' => true,
                        'roles' => ['BlogLogin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
       $firstday=date("Y-m-d", strtotime('monday this week'));   
        $lastday=date("Y-m-d", strtotime('sunday this week'));
        $firstday=date('Y-m-d',strtotime ( '-1 days' , strtotime ( $firstday ) ) );
        
        
        $day=array();$date=$firstday;
        for($i=0;$i<7;$i++)
        {
            $day[$i]=date('Y-m-d',strtotime($date . "+1 days"));
            $date=$day[$i];
        }
        
        $result=array();
        foreach($day as $dk=>$dval)
        {
            $userlist=AllUser::find()->select(['COUNT(CASE WHEN UserTypeId =2 THEN 1 ELSE NULL END) AS Candidate','COUNT(CASE WHEN UserTypeId =3 THEN 1 ELSE NULL END) AS Company','COUNT(CASE WHEN UserTypeId =4 THEN 1 ELSE NULL END) AS Campus','COUNT(CASE WHEN UserTypeId =5 THEN 1 ELSE NULL END) AS Team','DATE(OnDate) AS OnDate'])->where(['DATE(OnDate)'=>$dval])->asarray()->one();
            $result['candidate'][$dval]=($userlist['Candidate'])?$userlist['Candidate']:0;
            $result['company'][$dval]=($userlist['Company'])?$userlist['Company']:0;
            $result['campus'][$dval]=($userlist['Campus'])?$userlist['Campus']:0;
            $result['team'][$dval]=($userlist['Team'])?$userlist['Team']:0;
        }
        
        $jobpost=PostJob::find()->select(['COUNT(CASE WHEN PostFor ="Candidate" THEN 1 ELSE NULL END) AS Candidate','COUNT(CASE WHEN PostFor ="Both" THEN 1 ELSE NULL END) AS Botht','COUNT(CASE WHEN PostFor ="Team" THEN 1 ELSE NULL END) AS Team','COUNT(CASE WHEN PostFor ="Company" THEN 1 ELSE NULL END) AS Company'])->asArray()->one();
        
        $campuspost=CampusPost::find()->select(['Count(*) AS CampusPost'])->asArray()->one();
        
        $totalmember=AllUser::find()->count();
        $totaljob=PostJob::find()->count();
        $careerbugwallpost=CompanyWallPost::find()->count();
        
        $datee=date('Y-m-d');
        $datee1=date('Y-m-d',strtotime ( '-1 days' , strtotime ( $datee ) ) );
        $datee2=date('Y-m-d',strtotime ( '-1 days' , strtotime ( $datee1 ) ) );

        $todaysuser=AllUser::find()->where(['OnDate'=>$datee,'IsDelete'=>0])->all();
        $todaysuser1=AllUser::find()->where(['OnDate'=>$datee1,'IsDelete'=>0])->all();
        $todaysuser2=AllUser::find()->where(['OnDate'=>$datee2,'IsDelete'=>0])->all();
        
        return $this->render('home',['days'=>$day,'userlist'=>$result,'jobpost'=>$jobpost,'campuspost'=>$campuspost,'totalmember'=>$totalmember,'totaljob'=>$totaljob,'wallpost'=>$careerbugwallpost,'datee'=>$datee,'datee1'=>$datee1,'datee2'=>$datee2,'today'=>$todaysuser,'today1'=>$todaysuser1,'today2'=>$todaysuser2]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    
    
    public function actionLogin()
    {
             
        $this->layout='login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
    
/* print_r($_REQUEST);
die(); */ 
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    public function actionAlljob()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new PostJob();
		
		if(isset($_GET['userid']) && $_GET['userid'] > 0 && isset($_GET['status'])){
			
			$alljob = $model->find()->where(['EmployerId'=>$_GET['userid'], 'JobStatus'=>$_GET['status']])->orderBy(['OnDate'=>SORT_DESC])->all();
		
		}elseif(isset($_GET['userid']) && $_GET['userid'] > 0){
			
			$alljob = $model->find()->where(['EmployerId'=>$_GET['userid']])->orderBy(['OnDate'=>SORT_DESC])->all();
		
		}else{
			$alljob = $model->find()->where(['IsDelete'=>0,'JobStatus'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
		}
        return $this->render('alljob', [
                'model' => $alljob,
            ]);
    }
    
     public function actionJobdetail($id)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new PostJob();
        $onejob = $model->find()->where(['IsDelete'=>0,'JobStatus'=>0,'JobId'=>$id])->one();
        return $this->render('jobdetail', [
                'model' => $onejob,
            ]);
    }
    
    public function actionTopjob($id,$tpjob)
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new PostJob();
        $job = $model->find()->where(['JobId'=>$id])->one();
        if($tpjob==1)
        {
            $tjb=0;
        }
        else{
            $tjb=1;
        }
        $job->TopJob=$tjb;
        $job->save();      
        echo json_encode(1);
    }
    
    public function actionDeletejob($id)
    {
        $model = new PostJob();
        $job = $model->find()->where(['JobId'=>$id])->one();
        $job->IsDelete=1;
        $job->JobStatus=1;
        $job->save();      
        return $this->redirect(['alljob']);
    }

    
    public function actionAllemployee()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new AllUser();
        $allemployee = $model->find()->where(['IsDelete'=>0,'UserTypeId'=>2])->orderBy(['Ondate'=>SORT_DESC])->all();
        
        return $this->render('allemployee', [
                'model' => $allemployee,
            ]);
    }
    
    public function actionDeleteemployee($id)
    {
        $model = new AllUser();
        $employee = $model->find()->where(['UserId'=>$id])->one();
        $employee->IsDelete=1;
        $employee->save();      
        return $this->redirect(['allemployee']);
    }
    
    public function actionAllemployer()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $model = new AllUser();
        $allemployer = $model->find()->where(['IsDelete'=>0,'UserTypeId'=>3])->orderBy(['OnDate'=>SORT_DESC])->all();
        
        return $this->render('allemployer', [
                'model' => $allemployer,
            ]);
    }
    public function actionDeleteemployer($id)
    {
        $model = new AllUser();
        $employer = $model->find()->where(['UserId'=>$id])->one();
        $employer->IsDelete=1;
        $employer->save();   
        return $this->redirect(['allemployer']);
    }
    
    public function actionNewslettersubscriber()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $news=new NewsLetter();
        $allnewsletter=$news->find()->where(['IsDelete'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
        return $this->render('newslettersubscriber',['allnews'=>$allnewsletter]);
    }
    
    public function actionDeletenews($id)
    {
        $news=new NewsLetter();
        $newsone=$news->find()->where(['NewsLetterId'=>$id])->one();
        $newsone->IsDelete=1;
        if($newsone->save())
        {
            Yii::$app->session->setFlash('success', 'Email Deleted Succesfully');
        }
        else
        {
            Yii::$app->session->setFlash('error', 'There is some error , please try again');
        }
        
        return $this->redirect(['newslettersubscriber']);
    }
    
    public function actionEditlogo($companyid)
    {
        $docmodel=new Documents();
        $model=AllUser::find()->where(['UserId'=>$companyid])->one();
        if ($model->load(Yii::$app->request->post())) {
            $logo = UploadedFile::getInstance($model, 'HiringCompaniesLogoId');
                if($logo)
                {
                $logo_id=$docmodel->imageUpload($logo,'HiringCompaniesLogoId');
                }
                else
                {
                    $logo_id=0;
                }
                $model->HiringCompaniesLogoId=$logo_id;
                $model->save();
                 return $this->render('editlogo',['model'=>$model]);
        }
        else
        {
            return $this->render('editlogo',['model'=>$model]);
        }
    }
    
    public function actionFeedbackrecord()
    {
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        $jobrelevant=new JobRelevant();
        $allnewsletter=$jobrelevant->find()->orderBy(['OnDate'=>SORT_DESC])->all();
        return $this->render('feedbackrecord',['allnews'=>$allnewsletter]);
    }
   
    
    public function actionChangepassword(){
        
     
        if(Yii::$app->user->identity->type->TypeName=='BlogLogin')
        {
            $this->layout='bloglayout';
        }
        
        $model = new PasswordForm;
        $modeluser = User::find()->where([
            'LoginId'=>Yii::$app->user->identity->LoginId
        ])->one();
       
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
              try{
                  
                 
                    $modeluser->Password = md5($_POST['PasswordForm']['newpass']);
                    if($modeluser->save()){
                        
                        Yii::$app->getSession()->setFlash(
                            'success','Password changed'
                        );
                        return $this->redirect(['changepassword']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                        return $this->redirect(['index']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',[
                        'model'=>$model
                    ]);
                }
            }else{
                Yii::$app->getSession()->setFlash(
                        'error',"{".json_encode($model->getErrors())."}"
                    );
                return $this->render('changepassword',[
                    'model'=>$model
                ]);
            }
        }else{
            return $this->render('changepassword',[
                'model'=>$model
            ]);
        }
    }
    
}
