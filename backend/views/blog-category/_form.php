<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BlogCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
		<label class="control-label col-lg-2">Blog Category Name</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'BlogCategoryName')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>
     <div class="form-group">
		<label class="control-label col-lg-2">Meta Title</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'MetaTitle')->textInput(['placeholder' => "Enter Meta Title"])->label(false) ?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-lg-2">Meta Description</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'MetaDescription')->textarea(['placeholder' => "Enter Meta Description"])->label(false) ?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-lg-2">Meta Keywords</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'MetaKeywords')->textarea(['placeholder' => "Enter Meta Keywords"])->label(false) ?>
		</div>
	</div>
     <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Create <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
