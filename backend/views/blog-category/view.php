<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BlogCategory */

$this->title = $model->BlogCategoryId;
$this->params['breadcrumbs'][] = ['label' => 'Blog Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="blog-category-view">

    <legend class="text-bold"><?= Html::encode($this->title) ?></legend>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->BlogCategoryId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->BlogCategoryId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'BlogCategoryId',
            'BlogCategoryName',
            //'IsDelete',
            'OnDate',
            //'UpdatedDate',
        ],
    ]) ?>

</div>
    </div></div>
