<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model common\models\Blog */
/* @var $form yii\widgets\ActiveForm */
$url='/backend/web';
?>

<div class="blog-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group">
		<label class="control-label col-lg-2">Blog Category</label>
		<div class="col-lg-10">
    <?= $form->field($model, 'BlogCategoryId')->dropDownList($blogcategory)->label(false) ?>
        </div>
    </div>
    
    <div class="form-group">
		<label class="control-label col-lg-2">Blog Name</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'BlogName')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>
    <?php
	if(!$model->isNewRecord)
	{
	?>
	<div class="form-group">
		<label class="control-label col-lg-2"></label>
		<div class="col-lg-10">
            <img src="<?=$url.'/'.$model->blogImage->Doc;?>" style="width: 100px;" />
		</div>
	</div>
	<?php
	}
	?>
    <div class="form-group">
		<label class="control-label col-lg-2">Blog Image</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'BlogImageId')->fileInput(['accept' => "application/jpeg, application/png, application/jpg"])->label(false) ?>
		</div>
	</div>
     <div class="form-group">
		<label class="control-label col-lg-2">Blog Description</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'BlogDesc')->widget(TinyMce::className(), [
									'options' => ['rows' => 6],'class'=>'form-control textarea-small',
									'language' => 'en_CA',
									'clientOptions' => [
										'plugins' => [
											"advlist autolink lists link charmap print preview anchor",
											"searchreplace visualblocks code fullscreen",
											"insertdatetime media table  paste spellchecker"
										],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								])->label(false);?>
		</div>
	</div>
	
	 <div class="form-group">
		<label class="control-label col-lg-2">Meta Title</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'MetaTitle')->textInput(['placeholder' => "Enter Meta Title"])->label(false) ?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-lg-2">Meta Description</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'MetaDescription')->textarea(['placeholder' => "Enter Meta Description"])->label(false) ?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-lg-2">Meta Keywords</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'MetaKeywords')->textarea(['placeholder' => "Enter Meta Keywords"])->label(false) ?>
		</div>
	</div>

    <div class="form-group">
		<label class="control-label col-lg-2">Facebook Visible</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'IsFacebook')->checkbox(array())->label(false) ?>
		</div>
	</div>
    <div class="form-group">
		<label class="control-label col-lg-2">Facebook Link</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'FacebookLink')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>

    <div class="form-group">
		<label class="control-label col-lg-2">Twitter Visible</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'IsTwitter')->checkbox(array())->label(false) ?>
		</div>
	</div>
    <div class="form-group">
		<label class="control-label col-lg-2">Twitter Link</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'TwitterLink')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>
    <div class="form-group">
		<label class="control-label col-lg-2">Linkedin Visible</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'IsLinkedIn')->checkbox(array())->label(false) ?>
		</div>
	</div>
    <div class="form-group">
		<label class="control-label col-lg-2">Linkedin Link</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'LinkedinLink')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>
    <div class="form-group">
		<label class="control-label col-lg-2">Googleplus Visible</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'IsGoogleplus')->checkbox(array())->label(false) ?>
		</div>
	</div>
    <div class="form-group">
		<label class="control-label col-lg-2">Googleplus Link</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'GooglePlusLink')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>
    
     <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Create <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
