<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BlogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blog-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'BlogId') ?>

    <?= $form->field($model, 'BlogCategoryId') ?>

    <?= $form->field($model, 'BlogName') ?>

    <?= $form->field($model, 'BlogImageId') ?>

    <?= $form->field($model, 'BlogDesc') ?>

    <?php // echo $form->field($model, 'IsFacebook') ?>

    <?php // echo $form->field($model, 'FacebookLink') ?>

    <?php // echo $form->field($model, 'IsTwitter') ?>

    <?php // echo $form->field($model, 'TwitterLink') ?>

    <?php // echo $form->field($model, 'IsLinkedIn') ?>

    <?php // echo $form->field($model, 'LinkedinLink') ?>

    <?php // echo $form->field($model, 'IsGoogleplus') ?>

    <?php // echo $form->field($model, 'GooglePlusLink') ?>

    <?php // echo $form->field($model, 'IsDelete') ?>

    <?php // echo $form->field($model, 'OnDate') ?>

    <?php // echo $form->field($model, 'UpdatedDate') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
