<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="blog-index">

<legend class="text-bold"><?= Html::encode($this->title) ?></legend>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'BlogId',
            ['attribute'=>'BlogCategory',
            'value'=>'blogCategory.BlogCategoryName'
            ],
            'BlogName',
            [
                'attribute' => 'Blog Image',
                'format' => 'html',    
                'value' => function ($data) {
		    $url=str_replace('web','',Yii::$app->getUrlManager()->getBaseUrl());
		    
		    return Html::img('/backend/web/'.$data->blogImage->Doc,
                       ['width' => '70px']);
		    
                },
                ],
            //'BlogDesc:ntext',
            // 'IsFacebook',
            // 'FacebookLink',
            // 'IsTwitter',
            // 'TwitterLink',
            // 'IsLinkedIn',
            // 'LinkedinLink',
            // 'IsGoogleplus',
            // 'GooglePlusLink',
            // 'IsDelete',
            // 'OnDate',
            // 'UpdatedDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
    </div>
</div>
