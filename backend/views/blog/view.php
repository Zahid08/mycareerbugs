<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = $model->BlogId;
$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$url=str_replace('web','',Yii::$app->getUrlManager()->getBaseUrl());
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="blog-view">
    <legend class="text-bold"><?= Html::encode($this->title) ?></legend>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->BlogId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->BlogId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'BlogId',
            ['attribute'=>'BlogCategory',
            'value'=>$model->blogCategory->BlogCategoryName
            ],
            [
                'attribute'=>'photo',
                'value'=>$url.'/'.$model->blogImage->Doc,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'BlogName',
            'BlogImageId',
            'BlogDesc:ntext',
            'IsFacebook',
            'FacebookLink',
            'IsTwitter',
            'TwitterLink',
            'IsLinkedIn',
            'LinkedinLink',
            'IsGoogleplus',
            'GooglePlusLink',
            //'IsDelete',
            'OnDate',
            //'UpdatedDate',
        ],
    ]) ?>

</div>
    </div>
</div>