<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo "<pre>";print_r($dataProvider->getModels());die(); ?>
    <p>
        <?= Html::a('Create City', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'CityId',
            'CityName',
            [
                'attribute' => 'City Colour Image',
                'format' => 'html',    
                'value' => function ($data) {
		    $url=str_replace('web','',Yii::$app->getUrlManager()->getBaseUrl());
		    
		    return Html::img($url.'/'.$data->cityimage->Doc,
                       ['width' => '70px']);
		    
                },
            ],
            [
                'attribute' => 'City White Image',
                'format' => 'html',    
                'value' => function ($data) {
		    $url=str_replace('web','',Yii::$app->getUrlManager()->getBaseUrl());
		    
		    return Html::img($url.'/'.$data->citywhiteimage->Doc,
                       ['width' => '70px']);
		    
                },
            ],
            [ 'attribute'=>'Show In Index Page',
             'content' => function ($data) {
		    if($data->ShowInFront==0)
		    {
		    return Html::a('<input type="checkbox" />Show', ['showinfront', 'id' => $data->CityID, 'status' => 1]);
		    }
		    else if($data->ShowInFront==1)
		    {
		    return Html::a('<input type="checkbox" checked/>Hide', ['showinfront', 'id' => $data->CityID, 'status' => 0]);
		    }
				}
             ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
	</div>
</div>