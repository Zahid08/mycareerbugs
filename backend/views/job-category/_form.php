<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\JobCategory */
/* @var $form yii\widgets\ActiveForm */
$url='/backend/web';
?>

<div class="job-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
		<label class="control-label col-lg-2">Category Name</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'CategoryName')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-2">Short Name (Show in Index Page)</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'caption')->textInput(['maxlength' => true])->label(false) ?>
		</div>
	</div>
	
	<?php
	if(!$model->isNewRecord)
	{
		if($model->photo)
		{
	?>
	<div class="form-group">
		<label class="control-label col-lg-2"></label>
		<div class="col-lg-10">
            <img src="<?=$url.'/'.$model->photo->Doc;?>" style="width: 100px;" />
		</div>
	</div>
	<?php
		}
	}
	?>
    <div class="form-group">
		<label class="control-label col-lg-2">Orrange Image</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'Imageid')->fileInput(['accept' => "application/jpeg, application/png, application/jpg"])->label(false) ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="control-label col-lg-2">White Image</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'ImageWhiteId')->fileInput(['accept' => "application/jpeg, application/png, application/jpg"])->label(false) ?>
		</div>
	</div>
	
    <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Create <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
