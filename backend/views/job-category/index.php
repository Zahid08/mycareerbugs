<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JobCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Job Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="job-category-index">
<legend class="text-bold"><?= Html::encode($this->title) ?></legend>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Job Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'JobCategoryId',
            'CategoryName',
            [ 'attribute'=>'Show In Index Page',
             'content' => function ($data) {
		    if($data->ShowInFront==0)
		    {
		    return Html::a('<input type="checkbox" />Show', ['showinfront', 'id' => $data->JobCategoryId, 'status' => 1]);
		    }
		    else if($data->ShowInFront==1)
		    {
		    return Html::a('<input type="checkbox" checked/>Hide', ['showinfront', 'id' => $data->JobCategoryId, 'status' => 0]);
		    }
				}
             ],
            //'IsDelete',
            //'OnDate',
            //'UpdatedDate',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
    </div>
    </div>