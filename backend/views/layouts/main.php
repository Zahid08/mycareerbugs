<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
 $session = Yii::$app->session;
 $csrfToken = Yii::$app->request->getCsrfToken();
$url=str_replace('frontend','backend',(str_replace('web','',Yii::$app->getUrlManager()->getBaseUrl())));
$imageurl='/backend/web/';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link rel="shortcut icon" href="/images/icons/favicon.png"/>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
</head>

<body class="navbar-bottom" onload="alertload()">
<?php $this->beginBody() ?>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href=""><img src="<?=$imageurl;?>bassets/images/logo_light.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

				 
			</ul>

		 

			<ul class="nav navbar-nav navbar-right">
				 

				  

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?=$imageurl;?>bassets/images/demo/users/face11.jpg" alt="">
						<span><?=Yii::$app->user->identity->Name;?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="<?= Url::to(['site/changepassword'])?>"><i class="icon-user-plus"></i>Change Password</a></li>
						<!--<li><a href="#"><i class="icon-coins"></i> My balance</a></li>
						<li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>-->
						<li><a href="<?= Url::to(['site/logout'])?>" data-method="post"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page header -->
	<div class="page-header">
		<div class="breadcrumb-line">
			<ul class="breadcrumb">
				<li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ul>
 
		</div>

	 
	</div>
	<!-- /page header -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-title h6">
							<span>Main navigation</span>
							<ul class="icons-list">
								<li><a href="#" data-action="collapse"></a></li>
							</ul>
						</div>

						<div class="category-content sidebar-user">
							<div class="media">
								<a href="#" class="media-left"><img src="<?=$imageurl;?>bassets/images/demo/users/face11.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?=Yii::$app->user->identity->Name;?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;India
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>

						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="" data-original-title="Main pages"></i></li>
								<li><a href="<?=Url::to(['site/index']);?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>																<?php if(Yii::$app->user->identity->TypeId == 1 ){ ?>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack2"></i> <span>Location</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['location/create'])?>">Add</a></li>
										<li><a href="<?=Url::to(['location/index'])?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack2"></i> <span>States</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['states/create'])?>">Add</a></li>
										<li><a href="<?=Url::to(['states/index'])?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack2"></i> <span>City</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['city/create'])?>">Add</a></li>
										<li><a href="<?=Url::to(['city/index'])?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack2"></i> <span>Course</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['course/create'])?>">Add</a></li>
										<li><a href="<?=Url::to(['course/index'])?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack2"></i> <span>Specialization</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['specialization/create'])?>">Add</a></li>
										<li><a href="<?=Url::to(['specialization/index'])?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-copy"></i> <span>Skill</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['skill/create'])?>" id="layout1">Add</a></li>
										<li><a href="<?=Url::to(['skill/index'])?>" id="layout1">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-droplet2"></i> <span>Industry</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['industry/create'])?>">Add</a></li>
										<li><a href="<?=Url::to(['industry/index'])?>">View</a></li>
									</ul>
								</li>
								<li style="display: none;">
									<a href="#" class="has-ul"><i class="icon-stack"></i> <span>Job Category</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['job-category/create']);?>">Add</a></li>
										<li><a href="<?=Url::to(['job-category/index']);?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack"></i> <span>Role</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['position/create']);?>">Add</a></li>
										<li><a href="<?=Url::to(['position/index']);?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack"></i> <span>Plan</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['plan/create']);?>">Add</a></li>
										<li><a href="<?=Url::to(['plan/index']);?>">View</a></li>
									</ul>
								</li>
								<li>
									<a href="#" class="has-ul"><i class="icon-stack2"></i> <span>Content</span></a>
									<ul class="hidden-ul">
										<li><a href="<?=Url::to(['content/create'])?>">Add</a></li>
									</ul>
								</li>                                <?php } ?>
								
								<li class="navigation-header"><span style=" text-align: center; font-weight: bold;font-size: 14px;">Applied Candidate</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								  
									<li><a href="<?=Url::to(['report/candireq']);?>"><i class="icon-width"></i> <span>Candidate View Limit</span></a></li>
									<li><a href="<?=Url::to(['report/hrlmt']);?>"><i class="icon-width"></i> <span>HR Limit</span></a></li>
									<li><a href="<?=Url::to(['report/conslmt']);?>"><i class="icon-width"></i> <span>Consultancy Limit</span></a></li>
									<li><a href="<?=Url::to(['report/compslmt']);?>"><i class="icon-width"></i> <span>Company Limit</span></a></li>
									
									
									
								                                 <?php if(Yii::$app->user->identity->TypeId == 1  || Yii::$app->user->identity->TypeId==6){ ?>								 
								 <li class="navigation-header"><span style=" font-weight: bold;font-size: 14px; text-align: center;">   Candidate List</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>                                 
								 									
									<li style="display: none;"><a href="#"><i class="icon-home4"></i> <span> Upload Resume   </span></a></li> 
									<li><a href="<?=Url::to(['report/candidatelist']);?>"><i class="icon-home4"></i> <span> Candidate List     </span></a></li>
									<li><a href="<?=Url::to(['report/candidatelist1']);?>"><i class="icon-home4"></i> <span> Candidate List Old </span></a></li>
            <li  style="display: none;"><a href="<?=Url::to(['site/allemployee']);?>"><i class="icon-width"></i> <span>All Candidate</span></a></li> 																			<?php } ?> <?php if(Yii::$app->user->identity->TypeId == 1){ ?>
            <li><a href="<?=Url::to(['report/contactuslist']);?>"><i class="icon-home4"></i> <span> Contactus Details     </span></a></li>  
								  <?php } ?>
   
   
									 <?php if(Yii::$app->user->identity->TypeId == 1  || Yii::$app->user->identity->TypeId==7){ ?>
					               <li class="navigation-header"><span style=" font-weight: bold;font-size: 14px; text-align: center;"> Company List</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								 	
									<li><a href="<?=Url::to(['report/palnrequest']);?>"><i class="icon-width"></i> <span>Plan Approve Request</span></a></li>
									<li><a href="<?=Url::to(['report/companylist']);?>"><i class="icon-width"></i> <span>Total Company List</span></a></li>
									<li><a href="<?=Url::to(['site/alljob']);?>"><i class="icon-stack"></i> <span> Company Job Opening  </span></a></li> 																		<li><a href="<?=Url::to(['report/wallpostsetting']);?>"><i class="icon-stack"></i> <span>Wall Post Setting  </span></a></li> 									<li><a href="<?=Url::to(['report/wallpost']);?>"><i class="icon-width"></i> <span>Wall Posts</span></a></li>									<li><a href="<?=Url::to(['report/jobpost']);?>"><i class="icon-width"></i> <span>Job Posts</span></a></li>
                                    <li style="display: none;"><a href="<?=Url::to(['site/allemployer']);?>"><i class="icon-width"></i> <span>All Company</span></a></li>  <?php } ?>  <?php if(Yii::$app->user->identity->TypeId == 1){ ?>
								<li class="navigation-header"><span style=" font-weight: bold;font-size: 14px; text-align: center;"> HR List</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>								 									<li><a href="<?=Url::to(['report/hrlist']);?>"><i class="icon-width"></i> <span>Total HR List</span></a></li>									<li><a href="<?=Url::to(['report/hrcontactlimit']);?>"><i class="icon-width"></i> <span>HR Contact Limit</span></a></li>
                                                                <!-- /main -->

								   <li class="navigation-header"><span style=" text-align: center; font-weight: bold;font-size: 14px;"> Campuse Zone</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								  
									<li><a href="<?=Url::to(['report/campuslist']);?>"><i class="icon-width"></i> <span>Total Campus List</span></a></li>
									<li><a href="<?=Url::to(['report/campuspost']);?>"><i class="icon-width"></i> <span>Campus Job Post</span></a></li>
									<li><a href="<?=Url::to(['report/campuscontactuslist']);?>"><i class="icon-home4"></i> <span> Campus Contactus Details     </span></a></li>  
								<!-- /main -->
									
									<li class="navigation-header"><span style=" text-align: center; font-weight: bold;font-size: 14px;">Team Zone</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								  
									<li><a href="<?=Url::to(['report/teamlist']);?>"><i class="icon-width"></i> <span>Total Team List</span></a></li>
									
								 
									<li class="navigation-header"><span style=" text-align: center; font-weight: bold;font-size: 14px; background:#166dba; color:#fff">  LIMIT SET</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								  
									<li><a href="<?=Url::to(['report/hrlmt']);?>"><i class="icon-width"></i> <span>HR Limit</span></a></li>
									<li><a href="<?=Url::to(['report/conslmt']);?>"><i class="icon-width"></i> <span>Consultancy Limit</span></a></li>
									<li><a href="<?=Url::to(['report/compslmt']);?>"><i class="icon-width"></i> <span>Company Limit</span></a></li>
										<li><a href=""><i class="icon-width"></i> <span>Search bar Limit</span></a></li>
										 
										
								
								    <li class="navigation-header"><span style="text-align: center; font-weight: bold;font-size: 14px;">  Paymeny List </span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
							<li><a href="<?=Url::to(['plan/planassign']);?>"><i class="icon-copy"></i> <span>Plan Assign </span></a></li>
							<li><a href="<?=Url::to(['plan/viewassign']);?>"><i class="icon-copy"></i> <span>View Plan Assign </span></a></li>
								 
											<!--<li><a href="#"><i class="icon-copy"></i> <span>   Candidate Payment List   </span></a></li>
											<li><a href="#"><i class="icon-copy"></i> <span>   Company Payment List   </span></a></li>
											<li><a href="#"><i class="icon-copy"></i> <span>   Campus Payment List   </span></a></li>
											<li><a href="#"><i class="icon-copy"></i> <span> Company Total Invoice  </span></a></li> 
											<li><a href="#"><i class="icon-copy"></i> <span> Need To renewal  </span></a></li> 
											<li><a href="#"><i class="icon-copy"></i> <span> Expired  </span></a></li>--> 

  
  
								   <!--<li class="navigation-header"><span> Home page Job  </span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								 
									<!--<li><a href="#"><i class="icon-stack"></i> <span> Total Job  </span></a></li>
									<li><a href="<?=Url::to(['site/alljob']);?>"><i class="icon-stack"></i> <span> Total Job Opening  </span></a></li> -->

 
 
 
								  <!--<li class="navigation-header"><span>Home page Advertisement</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								
								  <li> <a href="index.html"><i class="icon-puzzle4"></i> <span>    Advertisement Block 1</span></a> </li>
								  <li> <a href="index.html"><i class="icon-puzzle4"></i> <span>  Advertisement Block 2</span></a> </li>-->
								
								
								
								
								<li class="navigation-header"><span>Home page Bottom</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
							      
								 <!--<li> <a href="index.html"><i class="icon-puzzle4"></i> <span>  Candidate Step</span></a> </li>
								 <li> <a href="index.html"><i class="icon-puzzle4"></i> <span>Companies Logo Block </span></a> </li>-->
								 <li> <a href="<?=Url::to(['footer/peoplesayblock']);?>"><i class="icon-puzzle4"></i> <span> People Say Block</span></a> </li>
								 <li> <a href="<?=Url::to(['footer/allfeedback']);?>"><i class="icon-puzzle4"></i> <span> All Feedback</span></a> </li>
								 <li> <a href="<?=Url::to(['site/feedbackrecord']);?>"><i class="icon-puzzle4"></i> <span>Job Relavant Feedback Record</span></a> </li>
								
								
								<!-- Extensions -->
								<li class="navigation-header"><span>Footer</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								<li> <a href="<?=Url::to(['footer/aboutindex']);?>"><i class="icon-puzzle4"></i> <span>First Block</span></a> </li>
								<li> <a href=""><i class="icon-puzzle4"></i> <span>Second Block</span></a> </li>
								<li><a href="<?=Url::to(['footer/thirdindex']);?>"><i class="icon-puzzle4"></i> <span>Third Block</span></a></li>
								<li><a href="<?=Url::to(['footer/contactindex']);?>"><i class="icon-puzzle4"></i> <span>  Contact Us</span></a></li>
								<li><a href="<?=Url::to(['footer/socialiconindex']);?>"><i class="icon-puzzle4"></i> <span>Social Icons</span></a></li> 
								<li><a href="<?=Url::to(['footer/copyrightindex']);?>"><i class="icon-puzzle4"></i> <span>Copy Right</span></a></li> 
							    <li><a href="<?=Url::to(['footer/developedblock']);?>"><i class="icon-puzzle4"></i> <span>  Developed Block</span></a></li> 
								<li class="navigation-header"><span>News Letter</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								<li> <a href="<?=Url::to(['site/newslettersubscriber']);?>"><i class="icon-puzzle4"></i> <span>Newsletter Subscriber</span></a> </li>
								
								
								<li class="navigation-header"><span>Blog</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
								<li> <a href="<?=Url::to(['blog-category/create']);?>"><i class="icon-puzzle4"></i> <span>Add BlogCategory</span></a> </li>
								<li> <a href="<?=Url::to(['blog-category/index']);?>"><i class="icon-puzzle4"></i> <span>View BlogCategory</span></a> </li>

								<li> <a href="<?=Url::to(['blog/create']);?>"><i class="icon-puzzle4"></i> <span>Add Blog</span></a> </li>
								<li> <a href="<?=Url::to(['blog/index']);?>"><i class="icon-puzzle4"></i> <span>View Blog</span></a> </li>  <?php } ?>

							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
				<?= Alert::widget() ?>
				<?= $content ?>
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


	<!-- Footer -->
	<div class="navbar navbar-default navbar-fixed-bottom footer">
		<ul class="nav navbar-nav visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#footer"><i class="icon-circle-up2"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="footer">
			<div class="navbar-text">
				&copy; 2019 <a href="#" class="navbar-link">Developed</a> by <a href="" class="navbar-link" target="_blank">  E-developed Technology</a>
			</div>

			<div class="navbar-right">
				<!--<ul class="nav navbar-nav">
					<li><a href="#">About</a></li>
					<li><a href="#">Terms</a></li>
					<li><a href="#">Contact</a></li>
				</ul>-->
			</div>
		</div>
	</div>
	<!-- /footer -->
	<script type="text/javascript">
		function alertload() {
		setTimeout(function() {
            $('.alert').fadeOut('fast');
            }, 3000); // <-- time in milliseconds
        }
	</script>
</body>

<?php $this->endBody() ?>
<?php $this->endPage() ?>