<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Plan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="form-group">
        <label class="control-label col-lg-2">Plan Name</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'PlanName')->textInput(['maxlength' => true])->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Price</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'Price')->textInput(['maxlength' => true])->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Total SMS</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'TotalSMS')->textInput()->label(false)?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Total Email</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'TotalEmail')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Total Download</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'TotalDownload')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Total CV Download</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'CVDownload')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Total contact view</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'ViewContact')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Total WhatsApp</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'whatsapptotal')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Daily total SMS</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'daily_sms')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Daily total email</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'total_daily_email')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Daily cv Download</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'total_daily_cv_download')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Daily total contact view</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'total_daily_view_contact')->textInput()->label(false) ?>
        </div>
</div>
<div class="form-group">
        <label class="control-label col-lg-2">Daily WhatsApp Total</label>
        <div class="col-lg-10">
    <?= $form->field($model, 'daily_whatsapptotal')->textInput()->label(false) ?>
        </div>
</div>
    <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Create <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
    </div>
</div>