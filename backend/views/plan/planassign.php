<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Plan */

$this->title = 'Plan Assign';
$this->params['breadcrumbs'][] = ['label' => 'Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="plan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
			<label class="control-label col-lg-2">Plan Name</label>
			<div class="col-lg-10">
		<?= $form->field($model, 'PlanId')->dropDownList($plan)->label(false) ?>
			</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-2">Company</label>
		<div class="col-lg-10">
    <?= $form->field($model, 'EmployerId')->dropDownList($employer)->label(false) ?>
        </div>
	</div>

    <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Assign <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
    </div>
</div>