<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Plan */

$this->title = $model->PlanId;
$this->params['breadcrumbs'][] = ['label' => 'Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="plan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->PlanId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->PlanId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PlanId',
            'PlanName',
            'TotalSMS',
            'TotalEmail:email',
            'TotalDownload',
            'ViewContact',
            'IsDelete',
            'OnDate',
            'UpdatedDate',
            'whatsapptotal',
            'daily_sms',
            'total_daily_email',
            'total_daily_cv_download',
            'total_daily_view_contact',
            'daily_whatsapptotal'
        ],
    ]) ?>

</div>
