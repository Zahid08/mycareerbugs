<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PlanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View Plans Assign';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="plan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Assign Plan', ['planassign'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="grid-view" id="w0">
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>#</th>
            <th><a>Plan Name</a></th>
            <th><a >Company</a></th>
            <th class="action-column">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if($planassign)
        {
            $i=0;
            foreach($planassign as $pk=>$pval)
            {
                $i++;
            ?>
        <tr>
            <td><?=$i;?></td>
            <td><?=$pval->plan->PlanName;?></td>
            <td><?=$pval->employer->Name;?></td>
            <td>
                <a data-method="post" data-confirm="Are you sure you want to delete this item?" data-pjax="0" aria-label="Delete" title="Delete" href="/backend/web/index.php/plan/deleteassign/<?=$pval->PlanAssign;?>"><span class="glyphicon glyphicon-trash"></span></a>
            </td>
        </tr>
        <?php
            }
        }
        ?>
    </tbody>
</table>
</div>
</div>
    </div>
</div>
