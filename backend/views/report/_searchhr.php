<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FilesSearch */
/* @var $form yii\widgets\ActiveForm */
?> 
<div class="files-search">

    <?php $form = ActiveForm::begin([ 	
    ]); ?> 
	
	<div class="row">
	<div class="col-sm-4">
    <?= $form->field($model, 'viewlimit')->textInput(['type' => 'number', 'required'=>true, 'min'=>1 ,'placeholder' => 'View Limit']) ?>   
		</div>	</div>
	 
	 
       
		
	  <div class="form-group"> 
	  <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>  
    </div>

	 
    <?php ActiveForm::end(); ?>

</div>
<br/><br/> 