<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">Campus Contact Details</legend>
   <table class="table table-striped table-bordered">
			<thead>
				<tr>
				   <th><a href="#">#</a></th>
				   <th><a href="#">Date</a></th>
				   <th><a href="#">College Name</a></th>
				   <!--<th><a href="#">City</a></th>-->
				   <th><a href="#">College Address</a></th>
				   <th><a href="#">Contact No</a></th>
				   <th><a href="#">Contact Person</a></th>
				   <th><a href="#">College Email</a></th>
				   <th><a href="#">City</a></th>
				   <th><a href="#">Course</a></th>
				</tr>
			</thead>
			<tbody>
 
<?php $i=0; foreach($contactdetail as $value){ $i++; ?>
				<tr>
				<td><?=$i;?></td>
			       <td> <?=date("d-m-Y",strtotime($value->OnDate));?></td>
				<td> <?=$value->CollegeName;?></td>
                <td style="display: none;"><?=$value->City;?></td>
                <td><?=$value->CollegeAddress;?></td>
                               <td> +91  <?=$value->ContactNo;?></td>
                               <td><?=$value->ContactPerson;?></td>
                               <td><?=$value->CollegeEmail;?></td>
                               <td><?=$value->City;?></td>
                               <td><?=$value->Course;?></td>
				</tr>
<?php } ?>
   

			</tbody>
			</table> 
</div>
</div>
</div>
