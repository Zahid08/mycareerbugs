<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\AllUser;
use yii\helpers\Url;

$alluser=new AllUser();
$this->title = 'Company Details';
$home='/backend/web/';
?>
<style>
    .catlabel {
    width: 100%;
    font-weight: bold;
    font-size: 14px;
    float: left;
}
.subcatlabel {
    width: 95%;
    font-size: 12px;
    float: left;
    margin-left: 5%;
}
</style>
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">
<?php
if($model->LogoId!=0)
{
 $logo=$home.'/'.$model->logo->Doc; 
}
else
{
$logo='/images/user.png';
}
?>
</legend>
  
   <table class="table table-striped table-bordered" >
	<tr>
		<td>
			<span><img src="<?php echo $logo; ?>" class="img-circle img-sm" style="border-radius: 10%;height: 100px !important;width: 100px !important;"/></span>
		</td>
		<td>
			<?php echo $model->Name; ?>
		</td>
	</tr>
	<tr>
		<td>Total No Of Student</td>
		<td><a href="<?=Url::to(['report/campusstudent','id'=>$model->UserId]);?>"><?=$alluser->totalStudent($model->UserId);?></a></td>
	</tr>
	<tr>
		<td>Total Job Posted</td>
		<td><b><?=$totaljob;?></b></td>
	</tr>
	<tr>
		<td>Total Job Open</td>
		<td><b style="color: green;"><?=$totalopenjob;?></b></td>
	</tr>
	<tr>
		<td>Total Job Closed</td>
		<td><b style="color: red;"><?=$totaljob-$totalopenjob;?></b></td>
	</tr>
	<tr>
		<td>
			Type
		</td>
		<td>
			<?php echo $model->EntryType; ?>
		</td>
	</tr>
	<tr>
		<td>
			 Email
		</td>
		<td>
			<?php echo $model->Email; ?>
		</td>
	</tr>
	<tr>
		<td>
			Address
		</td>
		<td>
			<?php echo $model->Address; ?>
		</td>
	</tr>
	<tr>
		<td>
			Coordinator Contact No
		</td>
		<td>
			<?php echo $model->MobileNo; ?>
		</td>
	</tr>
	
	<tr>
		<td>
			City
		</td>
		<td>
			<?php echo $model->City; ?>
		</td>
	</tr>
	<tr>
		<td>
			State
		</td>
		<td>
			<?php echo $model->State; ?>
		</td>
	</tr>
	<tr>
		<td>
			Country
		</td>
		<td>
			<?php echo $model->Country; ?>
		</td>
	</tr>
	<tr>
		<td>
			Pincode
		</td>
		<td>
			<?php echo $model->PinCode; ?>
		</td>
	</tr>
	<tr>
		<td>
			 About College 
		</td>
		<td>
			<?php echo html_entity_decode($model->CompanyDesc); ?>
		</td>
    </tr>
    <tr>
		<td>
			College Website
		</td>
		<td>
			<?php echo $model->CollegeWebsite; ?>
		</td>
    </tr>
    <tr>
		<td>
			 College Contact No
		</td>
		<td>
			<?php echo $model->ContactNo; ?>
		</td>
    </tr>
    <tr>
		<td>
			 Dean / Director / Principal
		</td>
		<td>
			<?php echo $model->Dean; ?>
		</td>
    </tr>
    <tr>
		<td>
			About Dean / Director / Principal
		</td>
		<td>
			<?php echo html_entity_decode($model->AboutYou); ?>
		</td>
    </tr>
    <tr>
		<td>
			 Dean / Director / Principal Photo
		</td>
		<td>
            <?php
									if($model->PhotoId!=0)
									{
										$campusphoto=$home.$model->photo->Doc;
									?>
			<img src="<?=$campusphoto;?>" style="width: 150px;"/>
            <?php
                                    }
                                    ?>
		</td>
    </tr>
    <tr>
		<td>
			 College Picture
		</td>
		<td>
			<?php
				if($model->collegepics)
				{
				foreach($model->collegepics as $key=>$value)
                                            {
                                                ?>
                                            <div style="width: 110px;float: left;border: 1px solid #333;padding: 5px;margin: 5px;position: relative;" id="collegepic<?=$value->CollegePicId;?>">
                                                <img src="<?=$home.$value->pic->Doc;?>" style="width: 100px;height: 100px;"/>
												
                                            </div>
                                            
                                                <?php
                                            }
                }
                ?>
		</td>
    </tr>
    <tr>
		<td>
			 Presentations
		</td>
		<td>
			 
		</td>
    </tr>
    <tr>
		<td>
			 Courses Provided
		</td>
		<td>
			<?php
								$cr=0;
								foreach($campuscourse as $ck=>$cval)
								{
								  if($cval!='')
								  {
								  if($cr!=$cval->CourseId)
								  {
								  ?>
								  <div class="catlabel" id="<?=$cval->CourseId;?>"><?=$cval->course->CourseName;?></div>
								  <?php
								  }
								  $cr=$cval->CourseId;
								  ?>
								  <div class="subcatlabel subcat<?=$cval->SpecializationId;?> cat<?=$cval->CourseId;?>" id="<?=$cval->CourseId.'.'.$cval->SpecializationId;?>"><?=$cval->specialization->Specialization;?></div>
								  <?php
								  }
								}
								?>
		</td>
    </tr>	
   </table> 
</div>
</div>
</div>