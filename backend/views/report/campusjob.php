<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Job Opening';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold"> Total Job Opening</legend>
<div style="width: 100%;float: left;overflow: auto;">
   <table class="table table-striped table-bordered">
			<thead>
				<tr>
				   <th><a href="#">#</a></th>
				   <th><a href="#">Date</a></th>
				   <th><a href="#">JobTitle</a></th>
				   <th><a href="#">Name</a></th>
				   <th><a href="#">Email</a></th>
				   <th><a href="#">Ph No.</a></th>
				   <th><a href="#">Location</a></th>
				   <th><a href="#">Action</a></th>
				   <th><a href="#">Job Status</a></th>
				</tr>
			</thead>
			<tbody>
 
<?php $i=0; if($model){ foreach($model as $value){ $i++; ?>
			<tr>
			       <td><?=$i;?></td>
			       <td> <?=date("d-m-Y",strtotime($value->OnDate));?></td>
			       <td>
					<a href="<?=Url::to(['report/campusjobdetail','id'=>$value->CampusPostId]);?>" style="cursor: pointer;" ><?=$value->JobTitle;?></a></td>
                   <td><?=$value->Name;?></td>
                  <td><?=$value->Email;?></td>
                  <td><?=$value->Phone;?></td>
			       <td><?=$value->City.' , '.$value->State;?></td>
			       <td>
				<a href="<?=Url::to(['report/deletecampusjob','id'=>$value->CampusPostId]);?>" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this job?" data-method="post" data-pjax="0">
				<span class="glyphicon glyphicon-trash"></span></a>
			       </td>
				   <td>
					  <?php
					  if($value->JobStatus==0)
					  {
					  ?>
					  <b style="color: green;">Open</b>
					  <?php
					  }
					  else
					  {
					  ?>
					  <b style="color: red;">Closed</b>
					  <?php
					  }
					  ?>
				   </td>
			</tr>
<?php }}else{ ?>
			<tr>
				<td colspan="9">No Results Found.</td>
			</tr>
   
<?php } ?>
			</tbody>
			</table>
</div>
</div>
</div>
</div>
