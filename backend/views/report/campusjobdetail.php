<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Job Details';
$home='/backend/web';
?>	
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">
<?php
echo $model->JobTitle; 
if($model->campus->LogoId!=0)
{
 $logo=$home.'/'.$model->campus->logo->Doc; 
}
else
{
$logo='/images/user.png';
}
?>


</legend>
  
   <table >
	
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Employer Details
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			<span><img src="<?php echo $logo; ?>" class="img-circle img-sm" style="border-radius: 2%;height: 100px !important;width: 150px !important;"/></span>
		</td>
		<td>
			<strong>Name : </strong> <?php echo $model->Name; ?>  <br/>
			<strong>Email : </strong><?php echo $model->Email; ?>  <br/>
			<strong>Ph No. : </strong>+ <?php echo $model->Phone; ?> <br/>
			<strong>Website : </strong> <?php echo $model->campus->CollegeWebsite; ?> <br/>
			<strong>Address : </strong><?=$model->City.' ,'.$model->State;?><br/>						
			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Short Discription
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Posted On : </strong> <?=date('d M, Y',strtotime($model->OnDate));?> <br/>
			<strong>No Of Vacancy : </strong><?=$model->NoofVacancy;?><br/>						
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Job Description
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			 <?=htmlspecialchars_decode($model->JobDescription);?>
		</td>
	</tr>
   </table> 
</div>
</div>
</div>