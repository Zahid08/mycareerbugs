<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\AllUser;
 
 
$this->title = 'All Candidates';
$this->params['breadcrumbs'][] = $this->title;
 
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="position-index"> 
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  
	
	 <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
		['attribute' =>'Name',
		'value'=>'Name',
		'contentOptions'   => function ($model, $index, $widget, $grid) {
								return [
									'id' => $model['UserId'], 
									'onclick' => 'location.href="'
										. Yii::$app->urlManager->createUrl('report/editemployee') 
										. '/"+(this.id);',
									'style' =>'cursor:pointer;', 
								];
						 },
			],
            'Email',
            'MobileNo', 
            'Address',
            'Country',  
			[
			'label' =>'Approve',
			'value' => function($model)
			{
				  if($model->IsApprove==0){
					 return '<div class="btn btn-success">Approve</div>';
				 } else {
					 return '<div class="btn btn-danger">Dis Approve</div>';
				 }
			},
			'contentOptions'   => function ($model, $index, $widget, $grid) {
								return [
									'id' => $model['UserId'], 
									'onclick' => 'location.href="'
										. Yii::$app->urlManager->createUrl('report/approve') 
										. '/"+(this.id);', 
									'style' =>'cursor:pointer;', 
								];
						 },
						 'format' => 'raw',
			],
			[
			'label' =>'Delete',
			'value' => function($model)
			{
				 return 'Delete';
				 
			},
			'contentOptions'   => function ($model, $index, $widget, $grid) {
								return [
									'id' => $model['UserId'], 
									'onclick' => 'location.href="'
										. Yii::$app->urlManager->createUrl('report/deleteemployee') 
										. '/"+(this.id);',
									'style' =>'cursor:pointer;', 
								];
						 },
			
			]
    ],
]); ?>

	
</div>
    </div></div>
