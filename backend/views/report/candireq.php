<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CandiReq;
 
 
$this->title = 'All Candidates';
$this->params['breadcrumbs'][] = $this->title;
 
   
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="position-index">  
	
	 <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
		 'Userid','JobId' 
    ],
]); ?>

	
</div>
    </div></div>
