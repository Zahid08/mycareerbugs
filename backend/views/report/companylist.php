<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm; 

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Profile';
$this->params['breadcrumbs'][] = $this->title;
$home='/backend/web';
?>
<!-- Square thumbs -->
			 
				<h6 class=" text-semibold" style=" margin-bottom: 10px; margin-top: 0px !important;">
					Company Profile List 
					
					<div class="usrfrm" style="float: right;">
						<?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['report/companylist	']),'enableClientValidation'=>false]); ?>

			<?= $form->field($usrdr, 'Name')->textInput(['placeholder' => 'Search Companies'])->label(false); ?>
		
		<?php ActiveForm::end(); ?>
		
			   </div>
				</h6> 
  
				<div class="row">
			   <?php
			   foreach($models  as $key=>$value)
			   {
                            if($value->LogoId!=0)
                                {
                                   $logo=$home.'/'.$value->logo->Doc; 
                                }
                                else
                                {
                                    $logo='/images/user.png';
                                }
			   ?>
			    <a href="<?= Url::toRoute(['employerdetail','id'=>$value->UserId]);?>" style="cursor: pointer;" >
					<div class="col-lg-3 col-md-6">
						<div class="thumbnail">
						  <div class="media-right media-middle position_right_main">
									<ul class="icons-list icons-list-vertical">
				                    	<li class="dropdown">
					                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
					                    	<ul class="dropdown-menu dropdown-menu-right"> 
												<li><a href="#">
												<?php
												if($value->ShowInFront==0)
												{
												?>
												<input type="checkbox" onclick="window.location.href='<?= Url::toRoute(['showinfront','status'=>1,'id'=>$value->UserId]);?>'"/>
												<?php
												}
												else
												{
												?>
												<input type="checkbox" checked onclick="window.location.href='<?= Url::toRoute(['showinfront','status'=>0,'id'=>$value->UserId]);?>'"/>
												<?php
												}
												?>
												Show in 
"Hiring Companies" block</a></li>
												<li><a href="<?= Url::toRoute(['site/editlogo','companyid'=>$value->UserId])?>">Edit Logo for "Hiring Companies"</a></li>
												<li class="divider"></li>
												<li><a href="<?= Url::toRoute(['deleteemployer','id'=>$value->UserId]);?>" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this company?" data-method="post" data-pjax="0"><i class="glyphicon-trash pull-right glyphicon" ></i> Delete </a></li>
											</ul>
				                    	</li>
			                    	</ul>
								</div>
								<div class="clearfix"></div>
							<div class="thumb thumb-rounded">
								<a href="<?= Url::toRoute(['employerdetail','id'=>$value->UserId]);?>" style="cursor: pointer;" >
			<img src="<?=$logo;?>" alt=""> </a>
							</div>
						
					    	<div class="caption text-center" style="height: 150px;">
					    		<h6 class="text-semibold no-margin"><?=$value->industry->IndustryName;?> 
								<small class="display-block"> <?=$value->Name;?>  </small> 
								<small class="display-block"> Mobile: +91 <?=$value->MobileNo;?>  </small>
								<small class="display-block"> Contact: +91 <?=$value->ContactNo;?>  </small>
								<small class="display-block"> Contact Person: <?=$value->ContactPerson;?>  </small>
								</h6> 
					    	</div>
				    	</div>
					</div>
					</a>
			   <?php
			   }
			   
			
			   ?>
			   </div>
			   
			   <div class="pag">
			     
			   <?php echo \yii\widgets\LinkPager::widget([
    'pagination' => $pages,
]);
?>
			   </div>
			<!-- /main content -->
