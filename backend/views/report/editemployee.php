<?php
$this->title = 'Edit Employee Profile';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';

use bupy7\cropbox\Cropbox;
?>
<style>
  .editcontainer{
    width: 100%;
    height: 30px;
    padding: 4px;
    float: left;
    cursor: pointer;
}
.select-wrapper {
    background-size: cover;
    display: block;
    position: relative;
    width: 100%;
    height: 30px;
}
#image_src {
    width: 100%;
    height: 30px;
    
    opacity: 0;
    filter: alpha(opacity=0); /* IE 5-7 */
}
</style>

<div id="wrapper"><!-- start main wrapper -->
		 
		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
				 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>
		
			   <div class="col-md-2 col-sm-2 col-xs-12">
			                 <div class="user-profile">
                                    <img src="<?=$pimage;?>" alt="" class="img-responsive center-block ">
				
				   
				   
                                </div> 
			      	</div>
              
                       <div class="col-md-10 col-sm-10 col-xs-12">
                            <div class="job-short-detail" id="edit_profile_page">
                                <div class="heading-inner">
                                    <p class="title">Profile detail</p>
									<?= Html::submitButton('<i class="fa fa-floppy-o orange"></i> Save Changes') ?>
									
                                </div>
                                <dl>
                                    <dt>Name:</dt>
                                    <dd>
                                        <input type="text" class="form-control" required name="AllUser[Name]" id="Name" value="<?=$profile->Name;?>" placeholder="Name">
									   </dd> 
                                    <dt>Phone:</dt>
                                    <dd>
                                        <input type="text" class="form-control" name="AllUser[MobileNo]" onkeypress="return numbersonly(event)" onblur="return IsMobileno(this.value);" maxlength="10" id="MobileNo" value="<?=$profile->MobileNo;?>" required> </dd>

                                    <dt>Email:</dt>
                                    <dd><input type="email" class="form-control" name="AllUser[Email]" value="<?=$profile->Email;?>" id="email" placeholder="martine-aug234@domain.com" readonly></dd>
				    
				    <dt>Gender:</dt>
                                    <dd><input type="radio"  name="AllUser[Gender]" value="Male" <?php if($profile->Gender=='Male') echo "checked";?> required/> &nbsp; Male
					<input type="radio"  name="AllUser[Gender]" value="Female" <?php if($profile->Gender=='Female') echo "checked";?>/> &nbsp; Female</dd>
 
                                    <dt>Address:</dt> 
									<dd><input type="text" class="form-control" name="AllUser[Address]" id="address" value="<?=$profile->Address;?>" placeholder="234 Uptown new City Tower "></dd>
									
									 <dt>Pincode:</dt> 
									<dd><input type="text" class="form-control" name="AllUser[PinCode]" id="address" value="<?=$profile->PinCode;?>" onkeypress="return numbersonly(event)" maxlength="6"></dd>

                                    

                                    <dt>Country:</dt>
                                   
									<dd><input type="text" class="form-control" name="AllUser[Country]" id="country" placeholder="Somewere at Antarctica " value="<?=$profile->Country;?>"> </dd>
									
									<dt>About Me:</dt>
                                   
									<dd>
									  <textarea name="AllUser[AboutYou]" value="<?=$profile->AboutYou;?>" class="form-control textarea" maxlength="250" onkeyup="$(this).next().html($(this).val().length +'- 250 character'); if($(this).val().length>250){$(this).next().css('color','red');}else{$(this).next().css('color','#333');}"><?=$profile->AboutYou;?></textarea>
									<p class="help-block">250 Characters</p>
									</dd>

                                    <dt>Applied Candidate View Limit:</dt>
                                   
									<dd><input type="number" class="form-control" name="AllUser[viewlimit]" id="viewlimit" placeholder="View Limit" value="<?=$profile->viewlimit;?>"> </dd>
									
                                </dl>
                            </div>
 
                        </div>
						 
	 <div class="clearfix"> </div>
	
			  

					   </div>
	
    <?php ActiveForm::end(); ?>
  </div>
            </div>
       </div>
		 
		<div class="border"></div>
<script type="text/javascript">
		var p=0;
		function admoreexp() {
		  if ($('#workexpdiv').css('display') == 'none') {
            $('#workexpdiv').show();$('#yearbox').show();
          }
		  else
		  {
				p++;
				var result=$('#workexpdiv').html();
				var date='<div class="row education-box"><div class="col-md-4 col-xs-12 col-sm-4"><div class="resume-icon"><i class="fa fa-calendar" aria-hidden="true"></i></div><div class="insti-name"> <h4>Year</h4></div></div><div class="col-xs-12 col-md-8 col-sm-8"><div class="degree-info"><div class="cols-sm-5"><div class="input-group" style="width: 33%;float: left;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="AllUser[YearFrom][]" readonly  style="cursor: pointer;background: #fff;" autocomplete="off" placeholder="From"  onkeypress="return numbersonly(event)"/></div><div class="input-group" style="width: 33%;float: left;margin-left: 10%;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="AllUser[YearTo][]" readonly  style="cursor: pointer;background: #fff;" autocomplete="off" placeholder="To" onkeypress="return numbersonly(event);" onchange="if(new Date($(this).val()).getTime()<=new Date($(&#39;#YearFrom&#39;).val()).getTime()){alert(&#39;Wrong Year Duration&#39;);$(this).val(&#39; &#39;);}"/></div></div></div></div></div>';
            $('#moreskill').append('<div id="workexpindiv'+p+'" ><span style="color:red;cursor:pointer;" onclick=del('+p+');>X</span>'+date+' '+result+'</div>');
			$(".date").datepicker({ dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true });
		  }
        }
		function del(id) {
            $('#workexpindiv'+id).remove();
        }

  setTimeout(function(){
		var loc = new locationInfo();
		$('#stateId').val('<?=$state;?>');
		<?php if($profile->CampusId!=0){ ?>
		$('#stateId').attr('disabled','disabled');
		<?php
		}
		?>
		var loc1='<?=$state;?>';
		 var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');
        if(stateId != ''){
        loc.getCities(stateId);
        }
		setTimeout(function(){$('#cityId').val('<?=$city;?>');},2000);
		
	},5000);
</script>