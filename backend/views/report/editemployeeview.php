<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm; 

/* @var $this yii\web\View */
/* @var $model app\models\juser */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?> 
	
	 <?= $form->field($allusr, 'viewlimit')->textInput(['type' => 'number', 'min'=>1 ,'placeholder' => 'View Limit']) ?> 
	 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    
    
    	<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30'],'action'=>'/admin/report/updatejoblimit/'.$allusr->UserId.'']); ?>
    	
      <h2>Limit Set Per Job</h2>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Job Name</th>
                <th>Limit</th>
              </tr>
            </thead>
            <tbody>
            <?php 
             foreach($allpost as $key=>$items){
            ?>    
              <tr>
                <td>
                    <?=$items->JobTitle?>
                    <input type="hidden" name="Job[<?=$key?>][JobId]" value="<?=$items->JobId?>" ?>
                    </td>
                <td>
                    <input type="number" id="alluser-viewlimit" class="form-control" name="Job[<?=$key?>][limit_set]" value="<?=!empty($items->viewLimit)?$items->viewLimit:0?>" min="1" placeholder="Set Limit" aria-invalid="false">
                </td>
              </tr>
             
            <?php } ?>
            
            </tbody>
          </table>
          <br/>
              <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
              
            <?php ActiveForm::end(); ?>
  

</div>


