<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Candidate Details';

$home='/backend/web';
?>
		
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">
Candidate Details
<?php
if($model->CVId!=0)
{
 $na=explode(" ",$model->Name);
$name=$na[0];
$extar=explode(".",$model->cV->Doc);
$ext=$extar[1];
?>
<a href="<?php echo $home.'/'.$model->cV->Doc; ?>" download="<?=$name.'.'.$ext;?>" target="_blank" ><button type="submit" style="float: right;bottom: 20px;" class="btn btn-primary">Download CV  <i class="icon-arrow-right14 position-right"></i></button></a>
<?php
}
if($model->PhotoId!=0)
{
 $logo=$home.'/'.$model->photo->Doc; 
}
else
{
$logo='/images/user.png';
}
?>
</legend>
  
   <table>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Personal Details
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			<span><img src="<?php echo $logo; ?>" class="img-circle img-sm" style="border-radius: 2%;height: 180px !important;width: 150px !important;"/></span>
			
		</td>
		<td>
			<strong>Name : </strong> <?php echo $model->Name; ?>  <br/>
			<strong>Email : </strong><?php echo $model->Email; ?>  <br/>
			<strong>Ph No. : </strong> <?php echo $model->MobileNo; ?> <br/>
			<strong>Address : </strong><?php echo $model->Address; ?><br/>						
			<strong>City  :</strong>  <?php echo $model->City; ?><br/>						
			<strong>State :</strong>	<?php echo $model->State; ?><br/>						
			<strong>Country :</strong> <?php echo $model->Country; ?><br/>						
			<strong>Pincode :</strong> <?php echo $model->PinCode; ?><br/>
						
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Educational Information <?=date('Y',strtotime($model->educations[0]->DurationFrom));?> to <?=date('Y',strtotime($model->educations[0]->DurationTo));?>
			</legend>
		</td>
	</tr>
	
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Higest Qualification : </strong> <?=$model->educations[0]->HighestQualification;?>  <br/>
			<strong>Course : </strong> <?=$model->educations[0]->course->CourseName;?> <br/>
			<strong>University / College : </strong> <?=$model->educations[0]->University;?>  <br/>
			<?php
			$skill='';
			foreach($model->empRelatedSkills as $ask=>$asv)
			{
				$skill.=$asv->skill->Skill.', ';
			}
			$skill=trim(trim($skill),",");
			?>
			<strong>Skills : </strong><?php echo $skill; ?><br/>
						
		</td>
	</tr>
	<?php
	if($model->experiences)
	{
	 if($model->experiences[0]->industry)
	 {
	  $industryname=$model->experiences[0]->industry->IndustryName;
	 }
	 else{
	  $industryname='';
	 }
	?>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Work Experience <?=date('Y',strtotime($model->experiences[0]->YearFrom));?> to <?=date('Y',strtotime($model->experiences[0]->YearTo));?>
			</legend>
		</td>
	</tr>
	
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Company name : </strong> <?=$model->experiences[0]->CompanyName;?>  <br/>
			<strong>Role : </strong> <?=$model->experiences[0]->position->Position;?>  <br/>
			<strong>Industry : </strong> <?=$industryname;?>  <br/>
			<strong>Experience : </strong> <?=$model->experiences[0]->Experience;?> Year  <br/>
		</td>
	</tr>
	
	<?php } ?>
   </table> 
</div>
</div>
</div>