<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Company Details';
$home='/backend/web';
?>	
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">
<?php
if($model->LogoId!=0)
{
 $logo=$home.'/'.$model->logo->Doc; 
}
else
{
$logo='/images/user.png';
}
?>
</legend>
  
   <table class="table table-striped table-bordered" >
	<tr style="display: none;">
	 <td></td>
	 <td>
	 <?php
	 if($model->IsApprove==0)
	 {
	  ?>
	  <a href="<?= Url::toRoute(['report/approveemp','id'=>$model->UserId,'type'=>'approve'])?>"><input type="button" value="Approve" class="btn btn-primary" /></a>
	  <?php
	 }
	 else
	 {
	  ?>
	  <a href="<?= Url::toRoute(['report/approveemp','id'=>$model->UserId,'type'=>'disapprove'])?>"><input type="button" value="Disapprove" class="btn btn-primary" /></a>
	  <?php
	 }
	 ?>
	 </td>
	</tr>
	<tr>
		<td>
			<span><img src="<?php echo $logo; ?>" class="img-circle img-sm" style="border-radius: 10%;height: 100px !important;width: 100px !important;"/></span>
		</td>
		<td>
			<?php echo $model->Name; ?>
		</td>
	</tr>
	<tr>
		<td>Total Job Posted</td>
		<td>
		<?php if(isset($totaljob) && $totaljob > 0){ ?>
			<a href="<?= Url::toRoute(['site/alljob','userid'=>$model->UserId])?>"><b><?=$totaljob;?></b></a>
		<?php }else{ ?>
			<b><?=$totaljob;?>
		<?php } ?>
		</td>
	</tr>
	<tr>
		<td>Total Job Open</td>
		<td>
		<?php if(isset($totalopenjob) && $totalopenjob > 0){ ?>
			<a href="<?= Url::toRoute(['site/alljob','userid'=>$model->UserId,'status'=>0])?>"><b style="color: green;"><?=$totalopenjob;?></b></a>
		<?php }else{ ?>
			<b style="color: green;"><?=$totalopenjob;?></b>
		<?php } ?>
		
		</td>
	</tr>
	<tr>
		<td>Total Job Closed</td>
		<td>
		<?php if(($totaljob-$totalopenjob) > 0){ ?>
			<a href="<?= Url::toRoute(['site/alljob','userid'=>$model->UserId,'status'=>1])?>"><b style="color: red;"><?=$totaljob-$totalopenjob;?></b></a>
		<?php }else{ ?>
			<b style="color: red;"><?=$totaljob-$totalopenjob;?></b>
		<?php } ?>
		
		</td>
	</tr>
	<tr>
		<td>
			Type
		</td>
		<td>
			<?php echo $model->EntryType; ?>
		</td>
	</tr>
	<tr>
		<td>
			 Email
		</td>
		<td>
			<?php echo $model->Email; ?>
		</td>
	</tr>
	<tr>
		<td>
			Address
		</td>
		<td>
			<?php echo $model->Address; ?>
		</td>
	</tr>
	<tr>
		<td>
			Mobile Number
		</td>
		<td>
			<?php echo $model->MobileNo; ?>
		</td>
	</tr>
	
	<tr>
		<td>
			Contact Number
		</td>
		<td>
			<?php echo $model->ContactNo; ?>
		</td>
	</tr>
	
	<tr>
		<td>
			Contact Person
		</td>
		<td>
			<?php echo $model->ContactPerson; ?>
		</td>
	</tr>
	
	<tr>
		<td>
			City
		</td>
		<td>
			<?php echo $model->City; ?>
		</td>
	</tr>
	<tr>
		<td>
			State
		</td>
		<td>
			<?php echo $model->State; ?>
		</td>
	</tr>
	<tr>
		<td>
			Country
		</td>
		<td>
			<?php echo $model->Country; ?>
		</td>
	</tr>
	<tr>
		<td>
			Pincode
		</td>
		<td>
			<?php echo $model->PinCode; ?>
		</td>
	</tr>
	<tr>
		<td>
			 Description
		</td>
		<td>
			<?php echo html_entity_decode($model->CompanyDesc); ?>
		</td>
	</tr>	
   </table> 
</div>
</div>
</div>