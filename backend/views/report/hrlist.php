<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView; 
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Company Profile';
$this->params['breadcrumbs'][] = $this->title;
$home='/backend/web';
?>
<!-- Square thumbs -->
				<h6 class="content-group text-semibold">
					Company Profile List 
					
					<div class="usrfrm" style="float: right;">
						<?php $form = ActiveForm::begin(['action' => Yii::$app->urlManager->createUrl(['report/hrlist']),'enableClientValidation'=>false]); ?>

			<?= $form->field($usrdr, 'Name')->textInput(['placeholder' => 'Search Hr'])->label(false); ?>
		
		<?php ActiveForm::end(); ?>
		
			   </div>
				</h6> 
				 

   
				<div class="row">
			   <?php
			   foreach($models as $key=>$value)
			   {
                            if($value->LogoId!=0)
                                {
                                   $logo=$home.'/'.$value->logo->Doc; 
                                }
                                else
                                {
                                    $logo='/images/user.png';
                                }
			   ?>
			    <a href="<?= Url::toRoute(['employerdetail','id'=>$value->UserId]);?>" style="cursor: pointer;" >
					<div class="col-lg-3 col-md-6">
						<div class="thumbnail">
						  
								<div class="clearfix"></div>
							<div class="thumb thumb-rounded">
								<a href="<?= Url::toRoute(['employerdetail','id'=>$value->UserId]);?>" style="cursor: pointer;" >
			<img src="<?=$logo;?>" alt=""> </a>
							</div>
						
					    	<div class="caption text-center" style="height: 150px;padding-right: 6px;">
					    		<h6 class="text-semibold no-margin"><?=$value->industry->IndustryName;?> 
								<small class="display-block"> <?=$value->Name;?>  </small> 
								<small class="display-block"> Mobile: +91 <?=$value->MobileNo;?>  </small>
								<small class="display-block"> Contact: +91 <?=$value->ContactNo;?>  </small>
								<small class="display-block"> Contact Person: <?=$value->ContactPerson;?>  </small>
								</h6> 
								<div class="caption text-center" style="float: right;top: 62px;padding-left: 0px;border-left-width: 1px;margin-left: 30px;padding-right: 0px;padding-top: 44px;"> 
							<a href="<?= Url::toRoute(['hrlist','id'=>$value->UserId]);?>" style="cursor: pointer;" > Delete</a>
							</div>
					    	</div>
							
				    	</div>
					</div>
					</a>
			   <?php
			   }
			   ?>
			   </div>
			   <div class="pag">
			     
			   <?php echo \yii\widgets\LinkPager::widget([
    'pagination' => $pages,
]);
?>
			   </div>
			<!-- /main content -->
