<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\AllUser;
 
 
$this->title = 'All Candidates';
$this->params['breadcrumbs'][] = $this->title;
  
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="position-index"> 
    
    <p align="right">
        <?= Html::a('Update All', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_searchhr', ['model' => $alusr]); ?>
    </div>
  
	 
	 <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
	
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
		['attribute' =>'Name',
		'value'=>'Name',
		'contentOptions'   => function ($model, $index, $widget, $grid) {
								return [
									'id' => $model['UserId'], 
									'onclick' => 'location.href="'
										. Yii::$app->urlManager->createUrl('report/editemployeeview') 
										. '/"+(this.id);',
									'style' =>'cursor:pointer;', 
								];
						 },
			],
            'Email',
            'MobileNo', 
           'viewlimit',
    ],
]); ?>

	
</div>
    </div></div>
