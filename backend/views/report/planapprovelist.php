<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Plan Request';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">Plan Request</legend>
   <table class="table table-striped table-bordered">
            <thead>
                <tr>
                   <th><a href="#">Company</a></th>
                   <th><a href="#">Ruquest On</a></th>
                   <!-- <th><a href="#">Is Approve</a></th> -->
                   <th><a href="#"></a></th>
                   
                </tr>
            </thead>
            <tbody>
 
<?php $i=0; foreach($dataProvider as $value){ $i++; ?>
                <tr>
                    <td><?=$value->user->Name;?></td>
                    <td><?=date('Y-m-d',strtotime($value->request_on));?></td>
                   
                    <td><?php if($value->is_approve==0){ ?>
                        <a href="<?=Url::to(['report/palnrequest','approve'=>$value->id]);?>"><span>Approve Request</span></a>
                    <?php }else{
                        echo "Approved";
                    } ?></td>
                </tr>
<?php } ?>
   

            </tbody>
            </table> 
</div>
</div>
</div>
