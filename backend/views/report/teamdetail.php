<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Team Details';

$home='/backend/web';
?>
		
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">
Team Details
<?php
if($model->PhotoId!=0)
{
 $logo=$home.'/'.$model->photo->Doc; 
}
else
{
$logo='/images/user.png';
}
?>
</legend>
  
   <table>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Personal Details
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			<span><img src="<?php echo $logo; ?>" class="img-circle img-sm" style="border-radius: 2%;height: 180px !important;width: 150px !important;"/></span>
			
		</td>
		<td>
			<strong>Name : </strong> <?php echo $model->Name; ?>  <br/>
			<strong>Email : </strong><?php echo $model->Email; ?>  <br/>
			<strong>Ph No. : </strong> <?php echo $model->MobileNo; ?> <br/>
			<strong>Address : </strong><?php echo $model->Address; ?><br/>						
			<strong>City  :</strong>  <?php echo $model->City; ?><br/>						
			<strong>State :</strong>	<?php echo $model->State; ?><br/>						
			<strong>Country :</strong> <?php echo $model->Country; ?><br/>						
			<strong>Pincode :</strong> <?php echo $model->PinCode; ?><br/>
						
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Educational Information <?=$model->educations[0]->DurationFrom;?> to <?=$model->educations[0]->DurationTo;?>
			</legend>
		</td>
	</tr>
	
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Higest Qualification : </strong> <?=$model->educations[0]->HighestQualification;?>  <br/>
			<strong>Course : </strong> <?=$model->educations[0]->course->CourseName;?> <br/>
			<strong>University / College : </strong> <?=$model->educations[0]->University;?>  <br/>
			<?php
			$skill='';
			foreach($model->empRelatedSkills as $ask=>$asv)
			{
				$skill.=$asv->skill->Skill.', ';
			}
			$skill=trim(trim($skill),",");
			?>
			<strong>Skills : </strong><?php echo $skill; ?><br/>
						
		</td>
	</tr>
	<?php
	if($model->experiences)
	{
	 if($model->experiences[0]->industry)
	 {
	  $industryname=$model->experiences[0]->industry->IndustryName;
	 }
	 else{
	  $industryname='';
	 }
	?>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Work Experience <?=$model->experiences[0]->YearFrom;?> to <?=$model->experiences[0]->YearTo;?>
			</legend>
		</td>
	</tr>
	
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Company name : </strong> <?=$model->experiences[0]->CompanyName;?>  <br/>
			<strong>Role : </strong> <?=$model->experiences[0]->position->Position;?>  <br/>
			<strong>Industry : </strong> <?=$industryname;?>  <br/>
			<strong>Experience : </strong> <?=$model->experiences[0]->Experience;?> Year  <br/>
		</td>
	</tr>
	
	<?php } ?>
	
	<?php
	if($model->teams)
	{
	?>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Team Members
			</legend>
		</td>
	</tr>
	
	<tr>
		<td style="width: 300px;">
			<?php
                                                    foreach($model->teams as $key=>$value)
                                                    {
													  if($value->MemberPhoto!=0)
													  {
														$teammemp=$home.'/'.$value->memberphoto->Doc;
													  }
													  else
													  {
														$teammemp='/images/user.png';
													  }
                                                        ?>
                                                    <div style="width: 90px;float: left;border: 1px solid #333;padding: 5px;margin: 5px;position: relative;" id="teammember<?=$value->TeamMembersId;?>">
                                                        <img src="<?=$teammemp;?>" style="width: 80px;height: 80px;"/><br/><div style="height: 20px;font-size: 9px;"><?=$value->MemberName;?></div>
                                                       
                                                    </div>
                                                    
                                                        <?php
                                                    }
												?>
		</td>
		<td>
			<strong>No Of Team Member : </strong> <?=$model->NoOfTeamMember;?>  <br/>
			<strong>Description :</strong> <?=$model->TeamDesc;?>
		</td>
	</tr>
	
	<?php } ?>
	
	<?php
	if($model->minqualification)
    {
	?>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Team Members Minimum Qualification
			</legend>
		</td>
	</tr>
	
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Min Qualification : </strong> <?=$model->minqualification->MinQualification;?>  <br/>
			<strong>Course :</strong> <?=$model->minqualification->course->CourseName;?><br/>
			<?php
			$skill='';
			foreach($model->teamRelatedSkills as $ask=>$asv)
			{
				$skill.=$asv->skill->Skill.', ';
			}
			$skill=trim(trim($skill),",");
			?>
			<strong>Skills : </strong><?php echo $skill; ?><br/>
		</td>
	</tr>
	
	<?php } ?>
   </table> 
</div>
</div>
</div>