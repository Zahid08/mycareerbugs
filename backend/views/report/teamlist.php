<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Team List';
$this->params['breadcrumbs'][] = $this->title;
$home='/backend/web';
?>
<!-- Square thumbs -->
				<h6 class="content-group text-semibold">
					Team List 
				</h6> 
  
				<div class="row">
			   <?php
			   foreach($dataProvider as $key=>$value)
			   {
                            if($value->PhotoId!=0)
                                {
                                   $logo=$home.'/'.$value->photo->Doc; 
                                }
                                else
                                {
                                    $logo='/images/user.png';
                                }
			   ?>
			    <a href="<?=Url::to(['report/teamdetail','Id'=>$value->UserId]);?>" style="cursor: pointer;" >
					<div class="col-lg-3 col-md-6">
						<div class="thumbnail">
						  <div class="media-right media-middle position_right_main">
									<ul class="icons-list icons-list-vertical">
				                    	<li class="dropdown">
					                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
					                    	<ul class="dropdown-menu dropdown-menu-right"> 
												<li><a href="<?=Url::to(['report/deleteteam','id'=>$value->UserId]);?>" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this Campus?" data-method="post" data-pjax="0"><i class="glyphicon-trash pull-right glyphicon" ></i> Delete </a></li>
											</ul>
				                    	</li>
			                    	</ul>
								</div>
								<div class="clearfix"></div>
							<div class="thumb thumb-rounded">
								<a href="<?=Url::to(['report/teamdetail','Id'=>$value->UserId]);?>" style="cursor: pointer;" >
			<img src="<?=$logo;?>" alt=""> </a>
							</div>
						
					    	<div class="caption text-center">
					    		<h6 class="text-semibold no-margin">
								<small class="display-block"> <?=$value->Name;?>  </small> 
								<small class="display-block"> Contact: +91 <?=$value->MobileNo;?>  </small> </h6> 
					    	</div>
				    	</div>
					</div>
					</a>
			   <?php
			   }
			   ?>
			   </div>
			<!-- /main content -->
