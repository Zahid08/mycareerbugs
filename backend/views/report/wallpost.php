<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wall Post Setting';
$this->params['breadcrumbs'][] = $this->title;
$home='/backend/web';
?>
<!-- Square thumbs -->
				<h6 class="content-group text-semibold">
					Wall Posts
				</h6>
				
				<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>
				<?php
					// display pagination
					echo LinkPager::widget([
						'pagination' => $pages,
					]);
				?>
				<select name="wallpostaction" required="required">
					<option value="">Select Action</option>
					<option value="delete">Delete</option>
					<option value="hide">Hide</option>
					<option value="unhide">Unhide</option>
				</select>
				<input type="submit" name="save" value="Save" style="margin-left:10%;">
				<div class="row">
				<table cellpadding="10" cellspacing="10" width="100%" border="1">
					<tr>
						<th width="5%">S.No</th>
						<th width="5%">#</th>
						<th width="5%">Status</th>
						<th width="90%">Post</th>
					</tr>
				
			   <?php
			   $sno=1;
			   foreach($allpost as $key=>$value)
			   {
                
			   ?>
			   <tr>
						<td><?php echo $sno++; ?></td>
						<td><input type="checkbox" name="wallPost[]" value="<?php echo $value['WallPostId'] ?>"></td>
						<td>
						<?php if($value['IsHide'] ==1){
								echo "Unhide";
							  }else{
								  echo 'Hide';
							  }
						?>
						</td>
						<td><?=$value['Post'];?></td>
						
					</tr>
			   
			   <?php
			   }
			   ?>
			   </table>
			   </div>
			<!-- /main content -->
			<?php ActiveForm::end(); ?>
