<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wall Post Setting';
$this->params['breadcrumbs'][] = $this->title;
$home='/backend/web';
?>
<!-- Square thumbs -->
				<h6 class="content-group text-semibold">
					Wall Post Setting For Company
				</h6> 
				<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>
				<?php
					// display pagination
					echo LinkPager::widget([
						'pagination' => $pages,
					]);
				?>
				<input type="submit" name="save" value="Save" style="float:right; margin-right:10%;">
				<div class="row">
				<table cellpadding="10" cellspacing="10" width="100%" border="1">
					<tr>
						<th>Company Name</th>
						<th>Logo</th>
						<th>Mobile</th>
						<th>Contact</th>
						<th>Contact Person</th>
						<th>Wall Post Setting</th>
					</tr>
				
			   <?php
			   foreach($dataProvider as $key=>$value)
			   {
                            if($value['LogoId']!=0)
                                {
                                   $logo=$home.'/'.$value['Doc']; 
                                }
                                else
                                {
                                    $logo='/images/user.png';
                                }
			   ?>
			   <tr>
						<td><?=$value['IndustryName'];?></td>
						<td><a href="<?= Url::toRoute(['employerdetail','id'=>$value['UserId']]);?>" style="cursor: pointer;" target="_blank" >
			<img src="<?=$logo;?>" alt="" width="50"> </a></td>
						<td>+91 <?=$value['MobileNo'];?></td>
						<td>+91 <?=$value['ContactNo'];?></td>
						<td>Contact Person: <?=$value['ContactPerson'];?></td>
						<td>
						<input type="text" name="wallPostSetting[<?php echo $value['UserId'] ?>]" value="<?php echo (isset($value['wallPostSetting']))?$value['wallPostSetting']:3 ?>" style="width:50px;">
						</td>
					</tr>
			   
			   <?php
			   }
			   ?>
			   </table>
			   </div>
			<!-- /main content -->
			<input type="submit" name="save" value="Save" style="float:right; margin-right:10%;">
			<?php ActiveForm::end(); ?>
