<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Plan */

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = ['label' => 'Change Password', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="plan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="plan-form">
    <?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
			<label class="control-label col-lg-2">Old Password</label>
			<div class="col-lg-10">
		     <?= $form->field($model,'oldpass',['inputOptions'=>[
            'placeholder'=>'Old Password'
        ]])->passwordInput()->label(false) ?>
			</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-2 col-lgdate">New Password</label>
		<div class="col-lg-10">
     <?= $form->field($model,'newpass',['inputOptions'=>[
            'placeholder'=>'New Password'
        ]])->passwordInput()->label(false) ?>
        </div>
	</div>
    
    <div class="form-group">
		<label class="control-label col-lg-2 col-lgdate">Confirm Password</label>
		<div class="col-lg-10">
     <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
            'placeholder'=>'New Password'
        ]])->passwordInput()->label(false) ?>
        </div>
	</div>

    <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton('Set <i class="icon-arrow-right14 position-right"></i>', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
    </div>
</div>