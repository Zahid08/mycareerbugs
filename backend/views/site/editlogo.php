<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$url='/backend/web';
/* @var $this yii\web\View */
/* @var $model common\models\JobCategory */

$this->title = 'Edit Logo';
//$this->params['breadcrumbs'][] = ['label' => 'Job Categories', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="job-category-create">
<legend class="text-bold"><?= Html::encode($this->title) ?></legend>
<div class="job-category-form">
    <?php $form = ActiveForm::begin(); ?>
	
	<?php
		if($model->logo)
		{
	?>
	<div class="form-group">
		<label class="control-label col-lg-2"></label>
		<div class="col-lg-10">
            <img src="<?=$url.'/'.$model->logo->Doc;?>" style="width: 100px;" />
		</div>
	</div>
	<?php
		}
	?>
    <div class="form-group">
		<label class="control-label col-lg-2">Logo For Hiring companies Block</label>
		<div class="col-lg-10">
            <?= $form->field($model, 'HiringCompaniesLogoId')->fileInput(['accept' => "application/jpeg, application/png, application/jpg"])->label(false) ?>
		</div>
	</div>

	
    <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Create <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
    </div>
</div>