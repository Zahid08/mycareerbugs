<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PositionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Job Relevant feedback Record';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold"> Job Relevant Feedback Record</legend>
   <table class="table table-striped table-bordered" style="width: 100%;">
			<thead>
				<tr>
				   <th><a href="#">#</a></th>
				   <th><a href="#">Date</a></th>
				   <th><a href="#">Email</a></th>
				   <th><a href="#">Feedback</a></th>
				</tr>
			</thead>
			<tbody>
 
<?php $i=0; if($allnews){ foreach($allnews as $value){ $i++; ?>
			<tr>
			       <td><?=$i;?></td>
			       <td> <?=date("d-m-Y",strtotime($value->OnDate));?></td>
			       <td> <?=$value->Email;?></td>
			       <td>
                        <?=$value->Type;?>
			       </td>
			</tr>
<?php }}else{ ?>
			<tr>
				<td colspan="9">No Results Found.</td>
			</tr>
   
<?php } ?>
			</tbody>
			</table> 
</div>
</div>
</div>
