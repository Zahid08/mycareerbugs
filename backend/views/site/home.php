<?php


use yii\helpers\Html;


use yii\bootstrap\ActiveForm;


use common\models\Total;


$url='/backend/web/';


$this->title = 'Admin';


$total = Total::find()->where(['id' => 1])->one();

?>


<!-- Main charts -->


				<div class="row">


					 


					 


					 


					 	<div class="col-lg-4" style="display: none;">


					 


					 <!-- Sales stats -->


						<div class="panel panel-flat">


					 <div class="panel-heading">


								<h6 class="panel-title">My messages</h6>


								<div class="heading-elements">


									<span class="heading-text"><i class="icon-history text-warning position-left"></i> Jul 7, 10:30</span>


									<span class="label bg-success heading-text">Online</span>


								</div>


							</div>





							<!-- Numbers -->


							<div class="container-fluid">


								<div class="row text-center">


									<div class="col-md-4">


										<div class="content-group">


											<h6 class="text-semibold no-margin"><i class="icon-clipboard3 position-left text-slate"></i> 2,345</h6>


											<span class="text-muted text-size-small">this week</span>


										</div>


									</div>





									<div class="col-md-4">


										<div class="content-group">


											<h6 class="text-semibold no-margin"><i class="icon-calendar3 position-left text-slate"></i> 3,568</h6>


											<span class="text-muted text-size-small">this month</span>


										</div>


									</div>





									<div class="col-md-4">


										<div class="content-group">


											<h6 class="text-semibold no-margin"><i class="icon-comments position-left text-slate"></i> 32,693</h6>


											<span class="text-muted text-size-small">all messages</span>


										</div>


									</div>


								</div>


							</div>


							<!-- /numbers --></div>


	</div>


							


							





					<div class="col-lg-12">





						<!-- Sales stats -->


						<div class="panel panel-flat">


							<div class="panel-heading">


								<h6 class="panel-title">Job Post Statistics</h6>


								<div class="heading-elements" style="display: none;">


									<form class="heading-form" action="#">


										<div class="form-group">


											<select class="change-date select-sm" id="select_date">


											 


													<option value="val1">June, 29 - July, 5</option>


													<option value="val2">June, 22 - June 28</option>


													<option value="val3" selected="selected">June, 15 - June, 21</option>


													<option value="val4">June, 8 - June, 14</option>


												</optgroup>


											</select>


										</div>


									</form>


			                	</div>


							</div>





							<div class="container-fluid">


								<div class="row text-center">


									<div class="col-md-3">


										<div class="content-group">


											<h5 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i><?=$jobpost['Candidate'];?></h5>


											<span class="text-muted text-size-small">For Candidate</span>


										</div>


									</div>





									<div class="col-md-3">


										<div class="content-group">


											<h5 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i><?=$jobpost['Team'];?></h5>


											<span class="text-muted text-size-small">For Team</span>


										</div>


									</div>





									<div class="col-md-3">


										<div class="content-group">


											<h5 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i><?=$jobpost['Botht'];?></h5>


											<span class="text-muted text-size-small">For Both Team & Candidate</span>


										</div>


									</div>


									


									<div class="col-md-3">


										<div class="content-group">


											<h5 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i><?=$campuspost['CampusPost'];?></h5>


											<span class="text-muted text-size-small">Campus Post For Company</span>


										</div>


									</div>


									


								</div>


							</div>





							<div class="content-group-sm" id="app_sales"></div>


							<div id="monthly-sales-stats"></div>


						</div>


						<!-- /sales stats -->





					</div>


				</div>


				<!-- /main charts -->





				


				<!-- Dashboard content -->


				<div class="row">


					<div class="col-lg-8">


						<div class="row">


								<div class="col-lg-12">


										<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>


								</div>


						</div>


						<br/>


						


						<!-- Quick stats boxes -->


						<div class="row">


							<div class="col-lg-3">





								<!-- Members online -->


								<div class="panel bg-teal-400">


									<div class="panel-body">


										<div class="heading-elements">


											<!--<span class="heading-text badge bg-teal-800">+53,6%</span>-->


										</div>





										<h3 class="no-margin"><?=$totalmember;?></h3>


										Total Member


										<div class="text-muted text-size-small">Candidate, Company, Team & Campus</div>


									</div>





									<div class="container-fluid">


										<div id="members-online"></div>


									</div>


								</div>


								<!-- /members online -->





							</div>





							<div class="col-lg-3">





								<!-- Current server load -->


								<div class="panel bg-pink-400">


									<div class="panel-body">


										<div class="heading-elements" style="display: none;">


											<ul class="icons-list">


						                		<li class="dropdown">


						                			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>


													<ul class="dropdown-menu dropdown-menu-right">


														<li><a href="#"><i class="icon-sync"></i> Update data</a></li>


														<li><a href="#"><i class="icon-list-unordered"></i> Detailed log</a></li>


														<li><a href="#"><i class="icon-pie5"></i> Statistics</a></li>


														<li><a href="#"><i class="icon-cross3"></i> Clear list</a></li>


													</ul>


						                		</li>


						                	</ul>


										</div>





										<h3 class="no-margin"><?=$totaljob;?></h3>


										Total Job Post


										<div class="text-muted text-size-small">For Candidate, Team & Campus</div>


									</div>





									<div id="server-load"></div>


								</div>


								<!-- /current server load -->





							</div>





							<div class="col-lg-3">





								<!-- Today's revenue -->


								<div class="panel bg-blue-400">


									<div class="panel-body">


										<div class="heading-elements" style="display: none;">


											<ul class="icons-list">


						                		<li><a data-action="reload"></a></li>


						                	</ul>


					                	</div>





										<h3 class="no-margin"><?=$wallpost;?></h3>


										Wall Post


										<div class="text-muted text-size-small">Total Wall Post <br/>By Company</div>


									</div>





									<div id="today-revenue"></div>


								</div>


								<!-- /today's revenue -->





							</div>
							<div class="col-lg-3">



								<!-- Members online -->

								<div class="panel bg-teal-400" style="padding-bottom: 36px;">

									<div class="panel-body">

										<div class="heading-elements">

											<!--<span class="heading-text badge bg-teal-800">+53,6%</span>-->

										</div>



										<h3 class="no-margin"><?= $total->totalvisit;?></h3>

										Total Visitor

										 

									</div>



									<div class="container-fluid">

										<div id="members-online"></div>

									</div>

								</div>

								<!-- /members online -->



							</div>

							


						</div>


						<!-- /quick stats boxes -->





						


					 





 





					</div>





					<div class="col-lg-4">





						<!-- My messages -->


						<div class="panel panel-flat">


							<!-- Area chart -->


							<div id="messages-stats"></div>


							<!-- /area chart -->








							<!-- Tabs -->


		                	<ul class="nav nav-lg nav-tabs nav-justified no-margin no-border-radius bg-indigo-400 border-top border-top-indigo-300">


								<li class="active">


									<a href="#messages-tue" class="text-size-small text-uppercase" data-toggle="tab">


										<?=date('l',strtotime($datee));?>


									</a>


								</li>





								<li>


									<a href="#messages-mon" class="text-size-small text-uppercase" data-toggle="tab">


										<?=date('l',strtotime($datee1));?>


									</a>


								</li>





								<li>


									<a href="#messages-fri" class="text-size-small text-uppercase" data-toggle="tab">


										<?=date('l',strtotime($datee2));?>


									</a>


								</li>


							</ul>


							<!-- /tabs -->








							<!-- Tabs content -->


							<div class="tab-content">


								<div class="tab-pane active fade in has-padding" id="messages-tue">


									<ul class="media-list">


										<?php


										if($today)


										{


										foreach($today as $tk=>$tval)


										{


										if($tval->UserTypeId==2 || $tval->UserTypeId==5)


										{


												$doc=($tval->PhotoId!=0)?$url.$tval->photo->Doc:'/frontend/web/images/user.png';


										}


										else


										{


												$doc=($tval->LogoId!=0)?$url.$tval->logo->Doc:'/frontend/web/images/user.png';


										}


										if($tval->UserTypeId==2){$type='Candidate';}elseif($tval->UserTypeId==3){$type='Company';}elseif($tval->UserTypeId==4){$type='Campus';}elseif($tval->UserTypeId==5){$type='Team';}
        

										?>


										<li class="media">


											<div class="media-left">


												<img src="<?=$doc;?>" class="img-circle img-xs" alt="">


											</div>





											<div class="media-body">


												<a href="#/">


													<?=$tval->Name;?>


													<span class="media-annotation pull-right"><?=$type;?></span>


												</a>





												<span class="display-block text-muted">


														<?=$tval->City;?> , <?=$tval->State;?>


												</span>


											</div>


										</li>


										<?php


										}


										}


										?>


									</ul>


								</div>





								<div class="tab-pane fade has-padding" id="messages-mon">


									<ul class="media-list">


										<?php


										if($today1)


										{


										foreach($today1 as $tk=>$tval)


										{


										if($tval->UserTypeId==2 || $tval->UserTypeId==5)


										{


												$doc=($tval->PhotoId!=0)?$url.$tval->photo->Doc:'/frontend/web/images/user.png';


										}


										else


										{


												$doc=($tval->LogoId!=0)?$url.$tval->logo->Doc:'/frontend/web/images/user.png';


										}


										if($tval->UserTypeId==2){$type='Candidate';}elseif($tval->UserTypeId==3){$type='Company';}elseif($tval->UserTypeId==4){$type='Campus';}elseif($tval->UserTypeId==5){$type='Team';}


										?>


										<li class="media">


											<div class="media-left">


												<img src="<?=$doc;?>" class="img-circle img-xs" alt="">


											</div>





											<div class="media-body">


												<a href="#/">


													<?=$tval->Name;?>


													<span class="media-annotation pull-right"><?=$type;?></span>


												</a>





												<span class="display-block text-muted">


														<?=$tval->City;?> , <?=$tval->State;?>


												</span>


											</div>


										</li>


										<?php


										}


										}


										?>


									</ul>


								</div>





								<div class="tab-pane fade has-padding" id="messages-fri">


									<ul class="media-list">


										<?php


										if($today2)


										{


										foreach($today2 as $tk=>$tval)


										{


										if($tval->UserTypeId==2 || $tval->UserTypeId==5)


										{


												$doc=($tval->PhotoId!=0)?$url.$tval->photo->Doc:'/frontend/web/images/user.png';


										}


										else


										{


												$doc=($tval->LogoId!=0)?$url.$tval->logo->Doc:'/frontend/web/images/user.png';


										}


										if($tval->UserTypeId==2){$type='Candidate';}elseif($tval->UserTypeId==3){$type='Company';}elseif($tval->UserTypeId==4){$type='Campus';}elseif($tval->UserTypeId==5){$type='Team';}


										?>


										<li class="media">


											<div class="media-left">


												<img src="<?=$doc;?>" class="img-circle img-xs" alt="">


											</div>





											<div class="media-body">


												<a href="#/">


													<?=$tval->Name;?>


													<span class="media-annotation pull-right"><?=$type;?></span>


												</a>





												<span class="display-block text-muted">


														<?=$tval->City;?> , <?=$tval->State;?>


												</span>


											</div>


										</li>


										<?php


										}


										}


										?>


									</ul>


								</div>


							</div>


							<!-- /tabs content -->





						</div>


						<!-- /my messages -->








						 





					</div>


				</div>


				<!-- /dashboard content -->


<?php


$datelist='';


		foreach($days as $dk=>$dval)


		{


				$datelist.="'".$dval."',";


		}


		$datelist=trim($datelist,',');


		


		$candidate='';


		foreach($userlist['candidate'] as $ck=>$cval)


		{


			$candidate.=$cval.',';


		}


		$candidate=trim($candidate,',');


		


		$candidate='';


		foreach($userlist['candidate'] as $ck=>$cval)


		{


			$candidate.=$cval.',';


		}


		$candidate=trim($candidate,',');


		


		$company='';


		foreach($userlist['company'] as $ck=>$cval1)


		{


			$company.=$cval1.',';


		}


		$company=trim($company,',');


		


		$campus='';


		foreach($userlist['campus'] as $ck=>$cval2)


		{


			$campus.=$cval2.',';


		}


		$campus=trim($campus,',');


		


		$team='';


		foreach($userlist['team'] as $ck=>$cval3)


		{


			$team.=$cval3.',';


		}


		$team=trim($team,',');


		?>


		


<script src="https://code.highcharts.com/highcharts.js"></script>


<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script type="text/javascript">


		Highcharts.chart('container', {


    chart: {


        type: 'column'


    },


    title: {


        text: 'Daily User Register'


    },


    subtitle: {


        text: 'Source: https://www.localhost'


    },


    xAxis: {


        categories: [<?=$datelist;?>


        ],


        crosshair: true


    },


    yAxis: {


        min: 0,


        title: {


            text: 'Total Register'


        }


    },


    tooltip: {


        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',


        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +


            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',


        footerFormat: '</table>',


        shared: true,


        useHTML: true


    },


    plotOptions: {


        column: {


            pointPadding: 0.2,


            borderWidth: 0


        }


    },


    series: [{


        name: 'Candidate',


        data: [<?=$candidate;?>]





    }, {


        name: 'Company',


        data: [<?=$company;?>]





    }, {


        name: 'Campus',


        data: [<?=$campus;?>]





    }, {


        name: 'Team',


        data: [<?=$team;?>]





    }]


});


</script>