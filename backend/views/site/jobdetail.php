<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Job Details';
$home='/backend/web';
?>	
<div class="panel panel-flat">
<div class="panel-body">
<div class="position-index">
<legend class="text-bold">
<?php
echo $model->JobTitle; 
if($model->LogoId!=0)
{
 $logo=$home.'/'.$model->employer->logo->Doc; 
}
else
{
$logo='/images/user.png';
}
?>


</legend>
  
   <table >
	
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Employer Details
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			<span><img src="<?php echo $logo; ?>" class="img-circle img-sm" style="border-radius: 2%;height: 100px !important;width: 150px !important;"/></span>
		</td>
		<td>
			<strong>Company Name : </strong> <?php echo $model->CompanyName; ?>  <br/>
			<strong>Email : </strong><?php echo $model->Email; ?>  <br/>
			<strong>Ph No. : </strong>+ <?php echo $model->Phone; ?> <br/>
			<strong>Website : </strong> <?php echo $model->Website; ?> <br/>
			<strong>Address : </strong><?=$model->City.' ,'.$model->State;?><br/>						
			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Short Discription
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<strong>Job Type : </strong> <?php echo $model->JobType; ?>  <br/>
			<strong>Skill : </strong>
			<?php
			$jskill='';
											foreach($model->jobRelatedSkills as $k=>$v)
											{
											$jskill.=$v->skill->Skill.' , ';
											}
											echo trim(trim($jskill),",");
											?> <br/><br/>
			<strong>Eligibility : </strong><?php if($model->Experience!='Fresher'){ echo $model->Experience.' Year';}else{echo $model->Experience;}?>  <br/>
			<strong>Role :</strong> <?=$model->position->Position;?> <br/>
			<strong>Industry :</strong> <?=$model->jobCategory->IndustryName;?> <br/>
			<strong>Job Shift. : </strong> <?php echo $model->JobShift; ?> <br/>
			<strong>Posted On : </strong> <?=date('d M, Y',strtotime($model->OnDate));?> <br/>
			<strong>Location : </strong><?=$model->City.' ,'.$model->Location;?><br/>						
			<strong>Expected Salary : </strong><?=$model->City.' ,'.$model->Salary;?><br/>
			<strong>Other Salary :</strong> <?=($model->OtherSalary=='')?'Not mentioned':$allpost->OtherSalary;?> <br/>
			<strong>No Of Vacancy :</strong> <?=$model->NoofVacancy;?> <br/>
			<strong>Specific Qualification :</strong> <?=$model->jobCategory->IndustryName;?> <br/>
			<strong>Industry :</strong> <?=($model->SpecialQualification=='')?'Not mentioned':$allpost->SpecialQualification;?> <br/>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Job Description
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			 <?=htmlspecialchars_decode($model->JobDescription);?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Job Specification
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			 <?=htmlspecialchars_decode($model->JobSpecification);?>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<legend class="text-bold">
				Technical Guidance
			</legend>
		</td>
	</tr>
	<tr>
		<td style="width: 300px;">
			
		</td>
		<td>
			<?=htmlspecialchars_decode($model->TechnicalGuidance);?>
		</td>
	</tr>
	<?php
	if($model->IsWalkin==1)
	{
	 ?>
	<tr>
		<td style="width: 300px;">
			Walkin details
		</td>
		<td>
			Venue :  <?=$model->Venue;?><br/>
										Interview Date & time : <?=date('dS M Y',strtotime($model->WalkinFrom)).'-'.date('dS M Y',strtotime($model->WalkinTo));?> between <?=$model->	WalkinTimeFrom;?> to <?=$model->WalkinTimeTo;?>
		</td>
	</tr>
	<?php
	}
	
	if($model->AltEmail!='')
	{
	?>
	<tr>
		<td style="width: 300px;">
			Extra Email
		</td>
		<td>
			<?=str_replace("|",",",$model->AltEmail);?>
		</td>
	</tr>
	<?php
	}
	?>
	
	
   </table> 
</div>
</div>
</div>