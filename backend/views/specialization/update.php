<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Specialization */

$this->title = 'Update Specialization: ' . $model->SpecializationId;
$this->params['breadcrumbs'][] = ['label' => 'Specializations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SpecializationId, 'url' => ['view', 'id' => $model->SpecializationId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel panel-flat">
    <div class="panel-body">
<div class="specialization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'course'=>$course
    ]) ?>

</div>
    </div>
</div>