<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Country;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

<div class="form-group">
		<label class="control-label col-lg-2">State Name</label>
		<div class="col-lg-10">
    <?= $form->field($model, 'StateName')->textInput(['maxlength' => true])->label(false); ?>
        </div>
</div>
<div class="form-group">
		<label class="control-label col-lg-2">Category Name</label>
		<div class="col-lg-10">
    <?= $form->field($model, 'CountryId')->dropDownList(ArrayHelper::map(Country::find()->select(['CountryName','CountryId'])->all(), 'CountryId', 'CountryName'),['prompt' => 'Select Country'])->label(false); ?>
        </div>
</div>

     <div class="form-group">
        <label class="control-label col-lg-2">&nbsp;</label>
        <div class="col-lg-10">
        <?= Html::submitButton($model->isNewRecord ? 'Create <i class="icon-arrow-right14 position-right"></i>' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
