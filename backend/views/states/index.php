<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'States';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-flat">
	<div class="panel-body">
		<div class="city-index">

			<h1><?= Html::encode($this->title) ?></h1>
    <?php //echo "<pre>";print_r($dataProvider->getModels());die(); ?>
    <p>
        <?= Html::a('Create States', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn'
            ],
            // 'CityId',
            'StateName',
            [
                'attribute' => 'Country',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->country->CountryName;
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn'
            ]
        ]
    ]);
    ?>
</div>
	</div>
</div>