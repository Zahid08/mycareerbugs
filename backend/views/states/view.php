<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = $model->StateName;
$this->params['breadcrumbs'][] = [
    'label' => 'States',
    'url' => [
        'index'
    ]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?= Html::a('Update', ['update', 'id' => $model->StateId], ['class' => 'btn btn-primary']) ?>
        <?=Html::a('Delete', ['delete','id' => $model->StateId], ['class' => 'btn btn-danger','data' => ['confirm' => 'Are you sure you want to delete this item?','method' => 'post']])?>
    </p>

    <?php

echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'StateId',
            'StateName',
            [
                'attributes' => 'Country',
                'value' => $model->country->CountryName
            ]
        ]
    ])?>

</div>
