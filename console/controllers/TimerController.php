<?php
namespace console\controllers;

use common\models\PostJob;
use yii\console\Controller;

class TimerController extends Controller
{
    public function log($string)
    {
        echo $string . PHP_EOL;
    }

    public function actionJobAlert()
    {
        \common\models\AllUser::sendReminder();
    }
}

