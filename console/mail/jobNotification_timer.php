<?php 
$url = '/backend/web/';

?>


<!DOCTYPE HTML>
<html>
<head>
<title>Job Notification</title>
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content=" " />
<!--google fonts-->
<style>
</style>
</head>
<body style="background: #fff; font-family: arial;">
	<div id="margin-minus-top13"
		style="width: 660px; margin: 0 auto; font-family: arial;">
		<div class="company_logo">
			<img class="main-logo" style="margin: 20px auto; display: block;"
				src="https://mycareerbugs.com/images/logo.png" alt="My Career Bugs">
		</div>
		<aside
			style="width: 660px; background: #fff; margin: 0 auto 10px; clear: both; overflow: hidden; padding: 0px; border: 1px solid #cccccc">
			<h2
				style="font-size: 14px; display: block; padding: 10px; font-family: arial; background: #6d136a; margin: 0 0 0px 0; color: #fff">
				Dear <span style="font-weight: bold"><?php echo $user->Name?>, </span> your My Career Bugs profile
			</h2>
			<div style="padding: 20px;">

				<p
					style="width: 200px; margin: 0px; float: left; font-weight: bold; margin: 0px; font-family: arial; font-size: 12px;">
					Role :</p>
				<p
					style="width: 400px; margin: 0px; float: left; font-weight: normal; font-family: arial; font-size: 12px;">
					<?=$user->getEducationRoles();?></p>
				<div style="clear: both; overflow: hidden; height: 10px"></div>


				<p
					style="width: 200px; float: left; font-weight: bold; font-family: arial; font-size: 12px; margin: 0px;">
					Key Skills:</p>
				<p
					style="width: 400px; margin: 0px; float: left; font-weight: normal; font-family: arial; font-size: 12px;">
					<?php  foreach ($user->empRelatedSkills as $ask => $asv) {
    				      if($asv->TypeId == 0){
    				  ?>
                      	<?=$asv->skill->Skill;?>
    				  <?php }
    				  }  ?></p>
				<div style="clear: both; overflow: hidden; height: 10px"></div>
				<p
					style="width: 200px; margin: 0px; float: left; font-weight: bold; font-family: arial; font-size: 12px;">
					Preferred Job Location:</p>
				<p
					style="width: 400px; float: left; font-weight: normal; font-family: arial; margin: 0px; font-size: 12px;">
					 <?=$user->getPreferedJobLocation();?> </p>
				<div style="clear: both; overflow: hidden; height: 10px"></div>
				<p
					style="width: 200px; float: left; font-weight: bold; font-family: arial; font-size: 12px; margin: 0px;">
					Specialization :</p>
				<p
					style="width: 400px; float: left; font-weight: normal; font-family: arial; font-size: 12px; margin: 0px;">
					<?=$user->educations[0]->specialization_id;?></p>
				<div style="clear: both; overflow: hidden; height: 10px;"></div>

				<a href="<?php echo yii\helpers\Url::to('/editprofile')?>"
					style="width: 100px; margin: 10px auto 0; background: #f16b22; font-size: 13px; font-weight: bold; text-align: center; color: #fff; padding: 7px 10px; font-family: Arial, Helvetica, sans-serif; text-decoration: none; display: block;">
					Update Proifle</a>
			</div>

		</aside>





		<aside
			style="width: 660px; background: #6d136a; margin: 0 auto 0px; clear: both; overflow: hidden; padding: 0px; border: 1px solid #cccccc">

			<h2
				style="font-size: 14px; display: block; padding: 10px; margin: 0 0 0px 0; color: #fff; font-weight: bold;">
				Jobs you may be interested based on your Skills</h2>
		</aside>
		<?php if (!empty($models)) : ?>
		<?php foreach ($models as $model) : ?>

		<aside
			style="width: 660px; background: #fff; margin: 0 auto 10px; clear: both; overflow: hidden; padding: 0px; border: 1px solid #cccccc; border-bottom: 0px solid #f16b22;">
			<div class="company-detail"
				style="clear: both; overflow: hidden; padding: 0px; border-bottom: 1px solid #cccccc">
				<div class="company-img"
					style="width: 50px; padding: 10px; float: left">
					<img
						src="https://mycareerbugs.com/backend/web/imageupload/thumb_1179.jpg"
						class="img-responsive" alt=""
						style="width: 50px; border-radius: 200px;">
				</div>
				<div class="company-contact-detail" id="job-details"
					style="width: 570px; float: left; padding: 10px;">
					<div class="heading_main_tilte12"
						style="clear: both; overflow: hidden">
						<h2
							style="line-height: 23px; font-size: 13px; font-weight: bold; color: #f16b22; margin: 2px 0 10px 0; border-bottom: 2px solid #f16b22; display: inline-block; padding: 0 0 5px 0; color: #f16b22;">
							<?php echo $model->JobTitle?>( <?php echo $model->Experience?>  )</h2>
						<a href="<?php echo yii\helpers\Url::to('jobdetail',['JobId' => $model->JobId])?>"
							style="width: 100px; margin: 10px 20px 0 0; float: right; background: #f16b22; font-size: 13px; font-weight: bold; text-align: center; color: #fff; padding: 7px 10px; font-family: Arial, Helvetica, sans-serif; text-decoration: none; display: block;">
							Apply Now</a>
						<div style="clear: both; overflow: hidden"></div>
						<p
							style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0 0 0px 0">
							<strong style="font-weight: bold;">Company Name: </strong>
							<?php echo $model->CompanyName?>
						</p>
						<div style="clear: both; overflow: hidden"></div>
					</div>

				</div>
				<div style="padding: 15px">
					<div class="heading_main_tilte12">
						<h2
							style="line-height: 23px; font-size: 16px; font-weight: bold; color: #f16b22; margin: 2px 0 0px 0; border-bottom: 0px solid #f16b22; display: inline-block; padding: 0 0 5px 0; color: #f16b22;">
							Job Description</h2>
					</div>
					<p style="font-size: 12px; margin: 0 0 7px 0;">
						<?php echo $model->Description?>
					</p>
				</div>
		</aside>

		<?php endforeach;?>
		<?php endif;?>
		<?php if(!empty($wallPosts)) : ?>
		<aside
			style="width: 660px; background: #6d136a; margin: 0 auto 10px; clear: both; overflow: hidden; padding: 0px; border: 1px solid #cccccc">

			<h2
				style="font-size: 14px; display: block; padding: 10px; margin: 0 0 0px 0; color: #fff; font-weight: bold;">
				MCB Wall Post Jobs</h2>
		</aside>
		<?php endif;?>
		<?php if(!empty($wallPosts)) : ?>
		<?php foreach ($wallPosts as $key => $post):
		    if ($post->employer->LogoId != '') {
		        $logo = $url . $post->employer->logo->Doc;
		    } else {
		        $logo = $url . 'images/user.png';
		    }
		    $postinid = \common\models\Position::findOne($post->PositionId);
		    ?>
		
		<aside
			style="width: 320px; background: #fff; margin: 0 10px 10px 0; float: left; padding: 0px; border: 1px solid #cccccc; border-bottom: 0px solid #f16b22;">
			<div class="company-detail"
				style="clear: both; overflow: hidden; padding: 0px; border-bottom: 1px solid #cccccc">
				<div class="company-img"
					style="width: 50px; padding: 10px; float: left">
					<img
						src="<?php echo $logo?>"
						class="img-responsive" alt=""
						style="width: 50px; border-radius: 200px;">
				</div>
				<div class="company-contact-detail" id="job-details"
					style="width: 230px; float: left; padding: 10px;">
					<div class="heading_main_tilte12"
						style="clear: both; overflow: hidden">
						<h2
							style="line-height: 23px; font-size: 12px; font-weight: bold; color: #f16b22; margin: 2px 0 10px 0; border-bottom: 2px solid #f16b22; display: inline-block; padding: 0 0 5px 0; color: #f16b22;">
							<?=$post->employer->Name;?></h2>

						<div style="clear: both; overflow: hidden"></div>
						<p
							style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin: 0 0 0px 0">
							<strong style="font-weight: bold;"> Role: </strong><?= $postinid->Position;?>
						</p>
						<div style="clear: both; overflow: hidden"></div>
					</div>

				</div>
				<div style="padding: 15px">
					<p style="font-size: 12px; margin: 0 0 7px 0;">
						<?=substr (str_replace("\n", '<br />',  $post->Post), 0 , 150);?>
					</p>
					<a href="<?= yii\helpers\Url::toRoute(['searchcompany','userid'=>$post->EmployerId])?>"
						style="width: 100px; margin: 10px 0 0 0; background: #f16b22; font-size: 13px; font-weight: bold; text-align: center; color: #fff; padding: 7px 10px; font-family: Arial, Helvetica, sans-serif; text-decoration: none; display: block;">Apply
						Now</a>
				</div>
		
		</aside>
		<?php if ($key % 2 == 1) {?>
			<div style="clear: both; overflow: hidden"></div>
		<?php }?>
		<?php endforeach;?>
		<?php endif;?>
		
		<p
			style="font-size: 12px; margin: 20px 0 0px 0; text-align: center; padding: 10px; color: #000;">
			Get more Recruiters to view your Profile<br> Showcase the best of
			your professionalism with a Professional Resume and Cover Letter
		</p>
		<tr style="width: 620px">
			<td
				style="padding: 30px 0 0 0; wdth: 620px; background-color: #ffffff;">
				<p
					style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #000;">Visit
					us at:</p>
				<div
					style="width: 250px; margin: 10px auto 10px; display: block; clear: both; overflow: hidden">
					<a href="#" style="margin: 0 10px; float: left; width: 30px"><img
						style="width: 100%" src="facebook.png"></a> <a href=""
						style="margin: 0 10px; float: left; width: 30px"><img
						style="width: 100%" src="twitter.png"></a> <a href=""
						style="margin: 0 10px; float: left; width: 30px"><img
						style="width: 100%" src="linkedin.png"></a> <a href=""
						style="margin: 0 10px; float: left; width: 30px"><img
						style="width: 100%" src="instagram.png"></a> <a href=""
						style="margin: 0 10px; float: left; width: 30px"><img
						style="width: 100%" src="youtube.png"></a>

				</div>
			</td>
		</tr>
		<p
			style="font-size: 12px; margin: 0px 0 0px 0; text-align: center; padding: 10px; color: #000;">
			The sender of this email is registered with mycareerbugs.com and
			using mycareerbugs.com services. The responsibility of checking the
			authenticity of offers/correspondence lies with you.</p>
		<p
			style="font-size: 12px; margin: 0px 0 20px 0; text-align: center; padding: 0 10px 10px 10px; color: #000;">
			� 2019 My Career Bugs Job Portal Pvt Ltd</p>
	</div>
</body>
</html>