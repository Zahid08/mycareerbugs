<?php
defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
defined('YII_ENABLE_EXCEPTION_HANDLER') or define('YII_ENABLE_EXCEPTION_HANDLER', false);

// Turn off all error reporting
// error_reporting(0);

// Report all errors except E_NOTICE
// This is the default value set in php.ini
error_reporting(E_ALL ^ E_NOTICE);
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','debug'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset'
    ],
    'modules' => [

        

    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend','baseUrl' => '/',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'jsOptions' => [ 'position' => \yii\web\View::POS_HEAD ],
                ],
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'authClientCollection' => [
        'class' => 'yii\authclient\Collection',
        'clients' => [
         'facebook' => [
            'class' => 'yii\authclient\clients\Facebook',
            'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
            'clientId' => '425379908073850',
            'clientSecret' => '0543df98e3f905df72d8217470fdbef0',
            //   'clientId' => '271482453915891',
            //   'clientSecret' => 'e2357f6500b173181af6e35ff27530fa',
            'attributeNames' => [
                    'id',
                    'name',
                    'first_name',
                    'last_name',
                    'link',
                    'about',
                    'work',
                    'education',
                    'gender',
                    'email',
                    'timezone',
                    'locale',
                    'verified',
                    'updated_time',
                ],
          ],
              
               'google' => [
                      'class' => 'yii\authclient\clients\Google',
                      // 'clientId' => '428032634391-0uc87692h87trpbrsp37l1ibm2shj9lv.apps.googleusercontent.com',
                      // 'clientSecret' => 'Ks7oK_WqsuBtGrasLn7E5SM2',
                         
                         // Changes made by manish
                         'clientId' => '192109191185-lbhg2jm1eqt5cqiojt44uo7sc6b2ojbt.apps.googleusercontent.com',
                         'clientSecret' => 'hGLKuETJdaMv-tIb-f4K1zVz',
                      //'scope'=>' https://www.googleapis.com/auth/plus.profile.emails.read',
                  ],
	       'linkedin' => [
                'class' => 'yii\authclient\clients\LinkedIn',
                'clientId' => '86rxwzz80rv60l',
                'clientSecret' => 'FxNXE4p1yyUUWivI',
            ],
       
        ],
      ],
       'urlManager' => [
            'baseUrl' => 'https://www.localhost/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,		 
            'rules' => [
                 'jobsearch/<alias>' => 'site/jobsearch',  
                 'jobsearch/white-collar/<alias>' => 'site/jobsearch',
                 'jobsearch/blue-collar/<alias>' => 'site/jobsearch',
				 'job/<alias>' => 'site/jobdetail',
				 'campus/search-company-post' => 'campus/jobsearch',
				 'campus-post/<alias>' => 'campus/jobdetail',
				 'wall-post/<alias>' => 'wall/post-view',
				 'blog' => 'site/blog',
				 'blog/<category>' => 'site/blogcategory',
				 'blog/<category>/<alias>' => 'site/blogdetails',
				 'job-alert' => 'site/jobalert'
                ]
        ],
       'urlManagerBackend' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => '/backend/imageupload/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
    'params' => $params,
];
