<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\AllUser;
use common\models\Industry;
use common\models\UserType;
use yii\helpers\Url;
use common\models\NaukariQualData;
use common\models\HrContact;
use common\models\NaukariSpecialization;
use common\models\Mail;
use common\models\States;
use common\models\Notification;
use common\models\EmpNotification;
use common\models\Skill;
use common\models\Course;
use common\models\Position;
use common\models\Documents;
use common\models\Education;
use common\models\Experience;
use common\models\ContactUs;
use common\models\PostJob;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use common\models\NewsLetter;
use common\models\AppliedJob;
use common\models\FooterAboutus;
use common\models\FooterContactus;
use common\models\FooterCopyright;
use common\models\FooterDevelopedblock;
use common\models\SocialIcon;
use common\models\FooterThirdBlock;
use common\models\Feedback;
use common\models\PeoplesayBlock;
use common\models\EmployeeSkill;
use common\models\Blog;
use common\models\BlogCategory;
use common\models\BlogComment;
use common\models\JobAlert;
use common\models\City;
use common\models\Specialization;
use common\models\CampusCourseMap;
use common\models\CollegePic;
use common\models\CampusPost;
use common\models\CampusSkill;
use common\models\CampusContact;
use common\models\PlanAssign;
use common\models\PlanRecord;
use common\models\CampusPostApplied;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

/**
 * Site controller
 */
class CampusController extends Controller
{

    /**
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'signup'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'signup',
                            'index',
                            'jobseach',
                            'employersregister',
                            'companyprofileeditpage',
                            'resetpassword',
                            'applyjob'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?'
                        ]
                    ],
                    [
                        'actions' => [
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    // job by company
    public function actionJobbycompany()
    { // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;
  $this->layout = 'main';
        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        $postfor = array(
            'Candidate',
            'Both'
        );
        if (isset(Yii::$app->request->get()['by'])) {
            $by = Yii::$app->request->get()['by'];
            $jobbycompany = PostJob::find()->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->andWhere([
                'LIKE',
                'CompanyName',
                $by . '%',
                false
            ])
                ->orderBy([
                'CompanyName' => SORT_ASC
            ])
                ->groupBy([
                'CompanyName'
            ])
                ->all();
        } else {
            $jobbycompany = PostJob::find()->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->orderBy([
                'CompanyName' => SORT_ASC
            ])
                ->groupBy([
                'CompanyName'
            ])
                ->all();
        }

        $peoplesayblock = new PeoplesayBlock();
        $pb = $peoplesayblock->find()->one();

        // all feedback
        $fdbk = new Feedback();
        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();
        return $this->render('jobbycompany', [
            'peoplesayblock' => $pb,
            'companylist' => $jobbycompany,
            'allfeedback' => $allfeedback
        ]);
    }

    // job by company
    public function actionJobbycampus()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

$this->layout = 'layout';


        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        if (isset(Yii::$app->request->post()['campussearch'])) {
            $course = Yii::$app->request->post()['course'];
            $location = Yii::$app->request->post()['location'];
            $specialization = Yii::$app->request->post()['specialization'];

            $jobbycompany = AllUser::find()->where([
                'IsDelete' => 0,
                'UserTypeId' => 4
            ])
                ->joinWith([
                'campuscourse'
            ])
                ->andFilterWhere([
                'CourseId' => $course,
                'SpecializationId' => $specialization,
                'State' => $location
            ])
                ->orderBy([
                'State' => SORT_ASC
            ])
                ->
            // ->groupBy(['State'])
            all();
        } else if (isset(Yii::$app->request->get()['by'])) {
            $by = Yii::$app->request->get()['by'];
            $jobbycompany = AllUser::find()->where([
                'AllUser.IsDelete' => 0,
                'UserTypeId' => 4
            ])
                ->andWhere([
                'LIKE',
                'CollegeName',
                $by . '%',
                false
            ])
                ->orderBy([
                'State' => SORT_ASC
            ])
                ->
            // ->groupBy(['State'])
            all();
        } else {
            $jobbycompany = AllUser::find()->where([
                'IsDelete' => 0,
                'UserTypeId' => 4
            ])
                ->orderBy([
                'State' => SORT_ASC
            ])
                ->groupBy([
                'State'
            ])
                ->all();
        }

        $allcourse = Course::find()->where([
            'IsDelete' => 0
        ])->all();
        $states = States::find()->where([
            'CountryId' => 101
        ])->all();
        $specialization = Specialization::find()->where([
            'IsDelete' => 0
        ])->all();

        $peoplesayblock = new PeoplesayBlock();
        $pb = $peoplesayblock->find()->one();

        // all feedback
        $fdbk = new Feedback();
        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();
        return $this->render('jobbycampus', [
            'peoplesayblock' => $pb,
            'companylist' => $jobbycompany,
            'allfeedback' => $allfeedback,
            'course' => $allcourse,
            'states' => $states,
            'specialization' => $specialization
        ]);
    }

    public function actionAllcollege()
    {
        $state = Yii::$app->request->get()['State'];

        $peoplesayblock = new PeoplesayBlock();
        $pb = $peoplesayblock->find()->one();

        // all feedback
        $fdbk = new Feedback();
        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();
        return $this->render('allcollege', [
            'state' => $state,
            'peoplesayblock' => $pb,
            'allfeedback' => $allfeedback
        ]);
    }

    public function actionCampusdetail($userid)
    {
        // $this->layout='blanklayout';
        $campusdetail = AllUser::find()->where([
            'UserId' => $userid
        ])->one();
        return $this->render('campusdetail', [
            'model' => $campusdetail
        ]);
    }

    public function actionGetcampus()
    {
        $rest = array();
        $state = Yii::$app->request->get()['state'];
        $campus = AllUser::find()->where([
            'State' => $state,
            'UserTypeId' => 4,
            'IsDelete' => 0
        ])->all();

        foreach ($campus as $key => $value) {
            $rest[$key]['Userid'] = $value->UserId;
            $rest[$key]['Campus'] = $value->CollegeName;
        }
        echo json_encode($rest);
    }

    public function actionGetcampuscourse()
    {
        $rest = array();
        $userid = Yii::$app->request->get()['userid'];
        $campuscourse = CampusCourseMap::find()->where([
            'CampusId' => $userid
        ])
            ->groupBy([
            'CourseId'
        ])->all();

        foreach ($campuscourse as $key => $value) {
            $rest[$key]['Courseid'] = $value->CourseId;
            $rest[$key]['Course'] = $value->course->name;
        }
        echo json_encode($rest);
    }

    public function actionGetspec()
    {
        $rest = array();
        $userid = Yii::$app->request->get()['userid'];
        $courseid = Yii::$app->request->get()['courseid'];

        $getspec = CampusCourseMap::find()->where([
            'CampusId' => $userid,
            'CourseId' => $courseid
        ])->all();

        foreach ($getspec as $key => $value) {
            $rest[$key]['Specid'] = $value->SpecializationId;
            $rest[$key]['Specialization'] = $value->specialization->name;
        }

        echo json_encode($rest);
    }

    public function actionAllstudent()
    {
        $this->layout='layout';
        
        if (isset(Yii::$app->session['Campusid'])) {
            $userid = Yii::$app->session['Campusid'];
        } else {
            $userid = Yii::$app->request->get()['userid'];
        }
        $specid = Yii::$app->request->get()['specid'];
        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $planassign = PlanAssign::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'IsDelete' => 0
        ])->one();
        $totalemail = $planassign->TotalEmail - $planassign->UseEmail;
        $mailmodel = new Mail();
        if ($mailmodel->load(Yii::$app->request->post())) {
            $mailmodel->Type = 'Mail';
            $jobdet = PostJob::find()->where([
                'JobId' => $mailmodel->JobId
            ])->one();
            if ($jobdet->IsExp == 0) {
                $experience = $jobdet->Experience;
            } else if ($jobdet->IsExp == 1) {
                $experience = $jobdet->Experience . ' Years';
            } else if ($jobdet->IsExp == 2) {
                $experience = 'Both Fresher and Experience';
            }
            $location = $jobdet->Location;
            $mailmodel->Experience = $experience;
            $mailmodel->Location = $location;
            if ($mailmodel->save()) {
                $sender = AllUser::find()->where([
                    'UserId' => Yii::$app->session['Employerid']
                ])->one();
                $mailtext = $mailmodel->MailText;
                $email = trim($mailmodel->EmployeeId, "|");
                $to = explode("|", $email);
                $from = $sender->Name . ' <' . $sender->Email . '>';
                $subject = $mailmodel->Subject;
                $senderemail = $sender->Email;
                $sendername = $sender->Name;
                $jobid = $mailmodel->JobId;

                if (isset(Yii::$app->session['Employerid'])) {
                    // for employer
                    $mailgo = ($totalemail > count($to)) ? count($to) : count($to) - $totalemail;
                    $m = 0;
                    if ($mailgo > 0) {
                        $planassign->UseEmail = $planassign->UseEmail + $mailgo;
                        $planassign->save();

                        $planrecord = new PlanRecord();
                        $planrecord->PlanId = $planassign->PlanId;
                        $planrecord->EmployerId = $planassign->EmployerId;
                        $planrecord->Type = 'Email';
                        $planrecord->Amount = $mailgo;
                        $planrecord->save();

                        foreach ($to as $key => $value) {
                            $m ++;
                            if ($m <= $mailgo) {
                                $receiver = AllUser::find()->select([
                                    'Name'
                                ])
                                    ->Where([
                                    'Email' => $value
                                ])
                                    ->one();
                                $receivername = $receiver->Name;
                                $html = "<html>
                    <head>
                    <title>$subject</title>
                    <style>
                    .im {
                    color:#4d4c4c!important;		
                    }
                    </style>
                    </head>
                    <body>
                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;line-height: 1.6;'>
                     <tbody><tr>
                                            <td><img src='http://mycareerbugs.com/images/logo.png' title='Careerbug' alt='Careerbug' style='margin-top:10px;width:200px;'></td>
                                        </tr>
                                       
                    <tr>
                                            <td><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:3%;margin:auto;color: #333;text-align: left;font-size:12px'>
                            The sender of this email is registered with mycareerbugs.com as $sendername. To respond back directly to the Employer, please click on Reply button, or send an email to $senderemail 
                    </h2>
                    </td>
                    </tr>
                    
                    <tr>
                    <td>
                    <table style='width:91%;height:auto;color:#4d4c4c;background:#fff;text-align:center;text-align: left;padding: 4%;padding-top: 0%;font-family: arial;font-size: 13px;margin-left: 4.5%;line-height: 1.8;'>
                   <tr>
                    <td><b>Experience required for the Job:</b> $experience </td>
                   </tr>
                   <tr>
                    <td><b>Job Location:</b> $location  <br/><br/></td>
                   </tr>
                   <tr>
                    <td>
                    <a href='http://mycareerbugs.com/site/jobdetail?JobId=$jobid'><input type='button' value='Apply Now' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;border: none;'/></a>
                    <a href='mailto:$senderemail'><input type='button' value='Reply' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;margin-left: 20px;border: none;'/></a>
                    </td>
                   </tr>
                   <tr>
                    <td><br/> <b>   Dear $receivername,</b> <br/><br/></td>
                   </tr>
                   <tr>
                    <td>
                    $mailtext
                    </td>
                   </tr>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td width='275' bgcolor='#f4f8fe' style='border:1px solid #e7f0fe;font:bold 13px Arial,Helvetica,sans-serif;color:#5d6f8a;padding:10px;margin:0 auto'>Is this job relevant to you? <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;margin:0 5px;font-weight:bold' href='https://www.mycareerbugs.com/site/jobrelevant?type=yes'>Yes</a> <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;font-weight:bold' href='https://www.mycareerbugs.com/site/jobrelevant?type=no'>No</a></td>
                   </tr>
                    <tr>
                    <td>
                    <p style='font-size: 11px;color: #333;'>Your feedback would help us in sending you the most relevant job opportunities</p>
                    </td>
                    </tr>
                    <tr>
                                        <td style='border-bottom: 1px solid #ccc;'></td>
                    </tr>
                    <td>
                    <p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'><b>Disclaimer:</b></p>
                    </td>
                    </tr>
                    <tr>
                                        <td style='border-bottom: 1px solid #333;'></td>
                    </tr>
                    <tr>
                    <td><p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'>
                    The sender of this email is registered with <a href='https://www.mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> as Cygnet Infotech Pvt. Ltd (rushad.shah@cygnetinfotech.com, 2 Manikyam Opp Samudra Annexes Off. C.G.Road,, AHMEDABAD, Gujarat - 380009) using <a href='https://www.mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> services. The responsibility of checking the authenticity of offers/correspondence lies with you. If you consider the content of this email inappropriate or spam, you may: Forward this email to: compliance@mycareerbugs.com. <br/>
<b>Please Note:</b> This mail is a private message from the recruiter. We have enabled auto login for your convenience, you are strongly advised not to forward this email to protect your account from unauthorized access.<br/>
<b>Advisory:</b> Please do not pay any money to anyone who promises to find you a job. This could be in the form of a registration fee or document processing fee or visa charges or any other pretext. The money could be asked for upfront or it could be asked after trust has been built after some correspondence has been exchanged. Also please note that in case you get a job offer or a letter of intent without having been through an interview process it is probably a scam and you should contact compliance@mycareerbugs.com for advise.
                    </p></td>
                    </tr>
                     </tbody>
                    </table>
                    </body>
                    </html>";
                                $mail = new ContactForm();
                                $mail->sendEmail($value, $from, $html, $subject);
                            }
                        }
                        Yii::$app->session->setFlash('success', "Mail sent Successfully");
                    } else {
                        Yii::$app->session->setFlash('error', "Your Data plan is already Used");
                    }
                    // for employer
                } elseif (isset(Yii::$app->session['Campusid'])) {
                    // for campus login
                    foreach ($to as $key => $value) {

                        $receiver = AllUser::find()->select([
                            'Name'
                        ])
                            ->Where([
                            'Email' => $value
                        ])
                            ->one();
                        $receivername = $receiver->Name;
                        $html = "<html>
                    <head>
                    <title>$subject</title>
                    <style>
                    .im {
                    color:#4d4c4c!important;		
                    }
                    </style>
                    </head>
                    <body>
                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;line-height: 1.6;'>
                     <tbody><tr>
                                            <td><img src='http://mycareerbugs.com/images/logo.png' title='Careerbug' alt='Careerbug' style='margin-top:10px;width:200px;'></td>
                                        </tr>
                                       
                    <tr>
                                            <td><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:3%;margin:auto;color: #333;text-align: left;font-size:12px'>
                            The sender of this email is registered with mycareerbugs.com as $sendername. To respond back directly to the Employer, please click on Reply button, or send an email to $senderemail 
                    </h2>
                    </td>
                    </tr>
                    
                    <tr>
                    <td>
                    <table style='width:91%;height:auto;color:#4d4c4c;background:#fff;text-align:center;text-align: left;padding: 4%;padding-top: 0%;font-family: arial;font-size: 13px;margin-left: 4.5%;line-height: 1.8;'>
                   <tr>
                    <td><b>Experience required for the Job:</b> $experience </td>
                   </tr>
                   <tr>
                    <td><b>Job Location:</b> $location  <br/><br/></td>
                   </tr>
                   <tr>
                    <td>
                    <a href='http://mycareerbugs.com/site/jobdetail?JobId=$jobid'><input type='button' value='Apply Now' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;border: none;'/></a>
                    <a href='mailto:$senderemail'><input type='button' value='Reply' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;margin-left: 20px;border: none;'/></a>
                    </td>
                   </tr>
                   <tr>
                    <td><br/> <b>   Dear $receivername,</b> <br/><br/></td>
                   </tr>
                   <tr>
                    <td>
                    $mailtext
                    </td>
                   </tr>
                    </table>
                    </td>
                    </tr>
                    <tr>
                    <td width='275' bgcolor='#f4f8fe' style='border:1px solid #e7f0fe;font:bold 13px Arial,Helvetica,sans-serif;color:#5d6f8a;padding:10px;margin:0 auto'>Is this job relevant to you? <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;margin:0 5px;font-weight:bold' href='https://www.mycareerbugs.com/site/jobrelevant?type=yes'>Yes</a> <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;font-weight:bold' href='https://www.mycareerbugs.com/site/jobrelevant?type=no'>No</a></td>
                   </tr>
                    <tr>
                    <td>
                    <p style='font-size: 11px;color: #333;'>Your feedback would help us in sending you the most relevant job opportunities</p>
                    </td>
                    </tr>
                    <tr>
                                        <td style='border-bottom: 1px solid #ccc;'></td>
                    </tr>
                    <td>
                    <p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'><b>Disclaimer:</b></p>
                    </td>
                    </tr>
                    <tr>
                                        <td style='border-bottom: 1px solid #333;'></td>
                    </tr>
                    <tr>
                    <td><p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'>
                    The sender of this email is registered with <a href='https://www.mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> as Cygnet Infotech Pvt. Ltd (rushad.shah@cygnetinfotech.com, 2 Manikyam Opp Samudra Annexes Off. C.G.Road,, AHMEDABAD, Gujarat - 380009) using <a href='https://www.mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> services. The responsibility of checking the authenticity of offers/correspondence lies with you. If you consider the content of this email inappropriate or spam, you may: Forward this email to: compliance@mycareerbugs.com. <br/>
<b>Please Note:</b> This mail is a private message from the recruiter. We have enabled auto login for your convenience, you are strongly advised not to forward this email to protect your account from unauthorized access.<br/>
<b>Advisory:</b> Please do not pay any money to anyone who promises to find you a job. This could be in the form of a registration fee or document processing fee or visa charges or any other pretext. The money could be asked for upfront or it could be asked after trust has been built after some correspondence has been exchanged. Also please note that in case you get a job offer or a letter of intent without having been through an interview process it is probably a scam and you should contact compliance@mycareerbugs.com for advise.
                    </p></td>
                    </tr>
                     </tbody>
                    </table>
                    </body>
                    </html>";
                        $mail = new ContactForm();
                        $mail->sendEmail($value, $from, $html, $subject);
                    }
                    Yii::$app->session->setFlash('success', "Mail sent Successfully");

                    // for campus login
                }
            } else {
                // var_dump($mailmodel->getErrors());
                Yii::$app->session->setFlash('error', "There is some error please try again");
            }

            return $this->redirect([
                'allstudent',
                'userid' => $userid,
                'specid' => $specid
            ]);
        } elseif (isset(Yii::$app->request->get()['filter']) && Yii::$app->request->get()['filter'] == 1) {
            $gender = Yii::$app->request->get()['gender'];
            $experience = Yii::$app->request->get()['experience'];
            $cresume = Yii::$app->request->get()['cresume'];
            if ($cresume == 1) {
                $allstudent = AllUser::find()->where([
                    'SpecId' => $specid,
                    'CampusId' => $userid
                ])
                    ->joinWith([
                    'experiences'
                ])
                    ->andWhere([
                    '!=',
                    'CVId',
                    0
                ])
                    ->andFilterWhere([
                    'Gender' => $gender,
                    'Experience.Experience' => $experience
                ]);
            } else {
                $allstudent = AllUser::find()->where([
                    'SpecId' => $specid,
                    'CampusId' => $userid
                ])
                    ->joinWith([
                    'experiences'
                ])
                    ->andFilterWhere([
                    'Gender' => $gender,
                    'Experience.Experience' => $experience
                ]);
            }
        } elseif (isset(Yii::$app->request->post()['studentsearch'])) {
            $studentname = Yii::$app->request->post()['studentname'];
            $courseid = Yii::$app->request->post()['course'];
            $specialization = Yii::$app->request->post()['specialization'];
            $spec = array();
            $specializationlist = Specialization::find()->where([
                'CourseId' => $courseid,
                'IsDelete' => 0
            ])->all();
            foreach ($specializationlist as $sk => $sval) {
                $spec[] = $sval->SpecializationId;
            }
            if ($specialization != '') {
                $spec = $specialization;
            }

            $allstudent = AllUser::find()->where([
                'CampusId' => Yii::$app->session['Campusid']
            ])
                ->andFilterWhere([
                'LIKE',
                'Name',
                $studentname . '%',
                false
            ])
                ->andFilterWhere([
                'SpecId' => $spec
            ]);
        } else {
            $allstudent = AllUser::find()->where([
                'SpecId' => $specid,
                'CampusId' => $userid
            ]);
        }

        $pages = new Pagination([
            'totalCount' => $allstudent->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }

        $allstudent = $allstudent->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $userdetail = AllUser::find()->where([
            'UserId' => $userid
        ])->one();

        $specialization = Specialization::find()->select([
            'Specialization',
            'CourseId'
        ])
            ->where([
            'SpecializationId' => $specid
        ])
            ->one();
        $allpost = ArrayHelper::map(PostJob::find()->where([
            'JobStatus' => 0,
            'IsDelete' => 0,
            'PostFor' => 'Campus'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all(), 'JobId', 'JobTitle');

        $course = CampusCourseMap::find()->where([
            'CampusId' => Yii::$app->session['Campusid']
        ])
            ->groupBy([
            'CourseId'
        ])
            ->all();
        $spec = CampusCourseMap::find()->where([
            'CampusId' => Yii::$app->session['Campusid']
        ])
            ->groupBy([
            'SpecializationId'
        ])
            ->all();

        return $this->render('allstudent', [
            'model' => $allstudent,
            'pages' => $pages,
            'campusdetail' => $userdetail,
            'specialization' => $specialization,
            'mailmodel' => $mailmodel,
            'allpost' => $allpost,
            'course' => $course,
            'spec' => $spec
        ]);
    }

    // campus zone
    public function actionCampuszone()
    {
        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;
        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $noofjobapplied = AppliedJob::find()->where([
            'Status' => 'Applied',
            'UserId' => Yii::$app->session['Campusid'],
            'IsDelete' => 0
        ])->count();
        Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied;

        $course = Course::find()->where([
            'IsDelete' => 0
        ])
            ->OrderBy([
            'CourseName' => SORT_ASC
        ])
            ->all();

        $model = new AllUser();
        $campuscontact = new CampusContact();
        if ($model->load(Yii::$app->request->post())) {
            $password = md5($model->Password);
            $rest = $model->find()
                ->where([
                'Email' => $model->Email,
                'IsDelete' => 0,
                'UserTypeId' => 4
            ])
                ->one();
            if ($rest) {
                if ($password == $rest->Password) {
                    if ($rest->VerifyStatus == 0) {
                        Yii::$app->session->setFlash('error', 'Please Check your mail for Email Verification.');
                        return $this->redirect([
                            'campus/campuszone'
                        ]);
                    } else {
                        $this->layout = 'layout';
                        $rest = $model->find()
                            ->where([
                            'Email' => $model->Email,
                            'Password' => $password,
                            'IsDelete' => 0,
                            'UserTypeId' => 4
                        ])
                            ->one();

                        $session = Yii::$app->session;
                        $session->open();
                        Yii::$app->session['Campusid'] = $rest->UserId;
                        Yii::$app->session['CampusName'] = $rest->Name;
                        Yii::$app->session['CampusEmail'] = $model->Email;
                        if ($rest->LogoId != 0) {
                            $url = '/backend/web/';
                            $pimage = $url . $rest->logo->Doc;
                        } else {
                            $pimage = '/images/user.png';
                        }

                        Yii::$app->session['CampusDP'] = $pimage;

                        $noofjobapplied = AppliedJob::find()->where([
                            'Status' => 'Applied',
                            'UserId' => Yii::$app->session['Campusid'],
                            'IsDelete' => 0
                        ])->count();
                        Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied;

                         return $this->redirect(['campusprofile']);
                        // return $this->redirect([
                        //     'site/index'
                        // ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Wrong Password");
                    return $this->render('campuszone', [
                        'model' => $model
                    ]);
                }
            } else {
                Yii::$app->session->setFlash('error', "Wrong Email");
                return $this->render('campuszone', [
                    'model' => $model
                ]);
            }
        } else if ($campuscontact->load(Yii::$app->request->post())) {

            if (! empty(Yii::$app->request->post()['CampusContact']['Course'])) {

                $campuscontact->Course = implode(',', Yii::$app->request->post()['CampusContact']['Course']);
            }
            $campuscontact->OnDate = date('Y-m-d');
            if ($campuscontact->save()) {
                Yii::$app->session->setFlash('success', "Thank you for contacting us. We will respond to you as soon as possible");
                $to = $campuscontact->CollegeEmail;
                $contactname = $campuscontact->CollegeName;
                $from = Yii::$app->params['adminEmail'];
                $subject = "Thank you";

                $html = "<html>
               <head>
               <title>Thank you</title>
               </head>
               <body>
               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>
                <tbody><tr>
                                       <td><img src='http://mycareerbugs.com/images/logo.png' title='Career Bugs' alt='Career Bugs' style='margin-top:10px;width:200px;'></td>
                                   </tr>
                                   <tr>
                                       <td style='height:30px'></td>
                                   </tr>
                           <tr>
                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://mycareerbugs.com' target='_blank'> Career Bugs </a>
               <br><br>
               <span style='font-size:16px;line-height:1.5'>
                 <h3> Dear  $contactname, </h3>
Thank you for contacting us. We will respond to you as soon as possible.!
               <br/>
               </span>
               </h2>
               </td>
               </tr>
               </tbody>
               </table>
               </body>
               </html>";
                $mail = new ContactForm();
                $mail->sendEmail($to, $from, $html, $subject);
            } else {

                Yii::$app->session->setFlash('error', "There was an error sending email");
            }
            return $this->redirect([
                'campuszone'
            ]);
        } else {
            return $this->render('campuszone', [
                'model' => $model,
                'campuscontact' => $campuscontact,
                'course' => $course
            ]);
        }
    }

    // campus student register
    public function actionStudentregister()
    {
        $allcampus = new AllUser();
        if ($allcampus->load(Yii::$app->request->post())) {
            $getCam = AllUser::find()->select([
                'Name',
                'CollegeName'
            ])
                ->where([
                'UserId' => Yii::$app->request->post()['AllUser']['College']
            ])
                ->one();
            $session = Yii::$app->session;
            $session->open();
            Yii::$app->session['State'] = Yii::$app->request->post()['AllUser']['State'];
            Yii::$app->session['University'] = $getCam->CollegeName;
            Yii::$app->session['Universityid'] = Yii::$app->request->post()['AllUser']['College'];
            Yii::$app->session['Course'] = Yii::$app->request->post()['AllUser']['Course'];
            Yii::$app->session['Specialization'] = Yii::$app->request->post()['AllUser']['Specialization'];

            return $this->redirect([
                'site/jobseekersregister'
            ]);
        } else {
            return $this->render('studentregister', [
                'allcampus' => $allcampus
            ]);
        }
    }

    // campus register
    public function actionCampusregister()
    {
        if (! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Employeeid']) && ! isset(Yii::$app->session['Campusid'])) {
		
            // footer section
            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

            $course = Course::find()->where([
                'IsDelete' => 0
            ])
                ->OrderBy([
                'CourseName' => SORT_ASC
            ])
                ->all();
            $model = new AllUser();
            $docmodel = new Documents();
            $docmodel3 = new Documents();
            $collegepic = new CollegePic();
            if ($model->load(Yii::$app->request->post())) {
				//echo '<pre>';
				//print_r(Yii::$app->request->post());die;
                // echo "<pre>";print_r(json_decode($_POST['AllUser']['Education'][0],true));die();
                $count = $model->find()
                    ->where([
                    'Email' => $model->Email,
                    'IsDelete' => 0
                ])
                    ->count();
                if ($count > 0) {
                    Yii::$app->session->setFlash('error', "This Mail-id Already Exist.");
                    return $this->render('campusregister', [
                        'model' => $model,
                        'course' => $course
                    ]);
                } else {
                    $model->EntryType = 'Campus';
                    $model->UserTypeId = 4;
                    $model->Password = md5($model->Password);
                    $model->ConfirmPassword = md5($model->Password);
                    $vkey = 'CB' . time();
                    $model->VerifyKey = $vkey;
                    $model->VerifyStatus = 1;
                    $model->Ondate = date('Y-m-d');
                    $model->Address = Yii::$app->request->post()['AllUser']['Address1'] . ' , ' . Yii::$app->request->post()['AllUser']['Address2'];
                    $model->language = 'English';
                    $photo = UploadedFile::getInstance($model, 'PhotoId');
                    if ($photo) {
                        $photo_id = $docmodel->imageUpload($photo, 'PhotoId');
                    } else {
                        $photo_id = 0;
                    }
                    $model->PhotoId = $photo_id;
                    $docmodel1 = new Documents();
                    $logo = UploadedFile::getInstance($model, 'LogoId');
                    if ($logo) {
                        $logo_id = $docmodel1->imageUpload($logo, 'LogoId');
                    } else {
                        $logo_id = 0;
                    }
                    $model->LogoId = $logo_id;

                    $presentation = UploadedFile::getInstance($model, 'PresentaionId');
                    if ($presentation) {
                        $presentation_id = $docmodel3->imageUpload($presentation, 'PresentaionId');
                    } else {
                        $presentation_id = 0;
                    }
                    $model->PresentaionId = $presentation_id;
                    // echo "<pre>";print_r($model);die();
					$model->save();
                    /*if ($model->save() && isset(Yii::$app->request->post()['AllUser']['Education'][0]) && ! empty(Yii::$app->request->post()['AllUser']['Education'][0])) {
                        $ary = json_decode($_POST['AllUser']['Education'][0], true);
                        foreach ($ary as $key => $value) {

                            $cid = explode('_', $key);
                            $education = new Education();
                            $education->CourseId = 1;
                            $education->UserId = $model->UserId;
                            $education->specialization_id = $value;
                            $education->course_id = $cid[1];
                            $education->save(false);
                        }
                    } else {
                        echo "<pre>";
                        print_r($model->getErrors());
                        die();
                    }*/

                    $CollegePic = UploadedFile::getInstances($collegepic, 'PicId');
                    foreach ($CollegePic as $file) {
                        $docmodel2 = new Documents();
                        $collegepic_id = $docmodel2->imageUpload($file, 'CollegePicId');
                        $collegepic = new CollegePic();
                        $collegepic->CampusId = $model->UserId;
                        $collegepic->PicId = $collegepic_id;
                        $collegepic->save();
                    }
					
					/*Save Course & Specialization*/
					if(!empty(Yii::$app->request->post()['AllUser']['Course'])){
						$courseSpecial = Yii::$app->request->post()['AllUser']['Course'];
						$courseList = array();
						foreach($courseSpecial as $courseData){
							$courseList[$courseData['course']] = isset($courseData['specialization'])?$courseData['specialization']:"";
						}
						
						if(!empty($courseList)){
							foreach($courseList as $courseId => $specializationRecord){
								if(empty($specializationRecord)){
									$spec = new CampusCourseMap();
									$spec->CampusId = $model->UserId;
									$spec->CourseId = $courseId;
									$spec->SpecializationId = 0;
									$spec->save();
								}else{
									foreach($specializationRecord as $specialRec){
										$specl = new CampusCourseMap();
										$specl->CampusId = $model->UserId;
										$specl->CourseId = $courseId;
										$specl->SpecializationId = $specialRec;
										if(!$specl->save()){
											 echo "<pre>";
											print_r($specl->getErrors());
											die();
										}
									}
									
								}
							}
						}
						
					}
					

                    /*$allcs = Yii::$app->request->post()['AllUser']['AllC'];
                    if ($allcs != '') {
                        $specs = explode("|", $allcs);
                        foreach ($specs as $key => $value) {
                            if ($value != '') {
                                $valar = explode("-", $value);
                                $spec = new CampusCourseMap();
                                $spec->CampusId = $model->UserId;
                                $spec->CourseId = $valar[0];
                                $spec->SpecializationId = $valar[1];
                                $spec->save();
                                // print_r($spec->getErrors());
                            }
                        }
                    }*/
					
                    $name = $model->Name;
                    $to = $model->Email;
                    $from = Yii::$app->params['adminEmail'];
                    $subject = "Registration Success";

                    $html = "<html>
               <head>
               <title>Registration Success</title>
               </head>
               <body>
               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>
                <tbody><tr>
                                       <td><img src='http://mycareerbugs.com/images/logo.png' title='Career Bugs' alt='Career Bugs' style='margin-top:10px;width:200px;'></td>
                                   </tr>
                                   <tr>
                                       <td style='height:30px'></td>
                                   </tr>
                           <tr>
                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://mycareerbugs.com' target='_blank'> Career Bugs </a>
               <br><br>
               <span style='font-size:16px;line-height:1.5'>
                 <h3> Dear  $name, </h3>
                    Congratulations! You have been registered successfully on Careerbug!!
               <br/>
               </span>
               </h2>
               </td>
               </tr>
               </tbody>
               </table>
               </body>
               </html>";
                    $mail = new ContactForm();
                    $mail->sendEmail($to, $from, $html, $subject);

                    $session = Yii::$app->session;
                    $session->open();
                    Yii::$app->session['Campusid'] = $model->UserId;
                    Yii::$app->session['CampusName'] = $model->Name;
                    Yii::$app->session['CampusEmail'] = $model->Email;
                    if ($model->LogoId != 0) {
                        $url = '/backend/web/';
                        $pimage = $url . $model->logo->Doc;
                    } else {
                        $pimage = 'images/user.png';
                    }
                    Yii::$app->session['CampusDP'] = $pimage;

                    // Yii::$app->session->setFlash('success', "Account Created Successfully");
                    return $this->redirect([
                        'site/thankyou'
                    ]);
                }
            } else {
                $NaukariQualData = NaukariSpecialization::find()->all();

                $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3 and id!=4 and id!=5 and id!=6")->all();
                return $this->render('campusregister', [
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'model' => $model,
                    'course' => $course,
                    'collegepic' => $collegepic
                ]);
            }
        } else {
            return $this->redirect([
                'site/index'
            ]);
        }
    }

    public function actionGetspecialization($courseid)
    {
        $this->layout = 'blanklayout';
        $specialization = Specialization::find()->where([
            'CourseId' => $courseid,
            'IsDelete' => 0
        ])->all();
        $data = array();
        foreach ($specialization as $key => $value) {
            $data[$key]['Specializationid'] = $value->SpecializationId;
            $data[$key]['Specialization'] = $value->Specialization;
        }
        echo json_encode($data);
    }

    public function actionDelpics($picid)
    {
        $this->layout = 'blanklayout';
        $model = CollegePic::find()->where([
            'CollegePicId' => $picid
        ])
            ->one()
            ->delete();
        echo json_encode(1);
    }
    
    public function actionDelcamppic($picid)
    {
        $this->layout = 'blanklayout';
        $model = Documents::find()->where([
            'DocId' => $picid
        ])
            ->one()
            ->delete();
        echo json_encode(1);
    }

    

    public function actionCollegeprofile($UserId)
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $model = new AllUser();
        $campusone = $model->find()
            ->where([
            'UserId' => $UserId
        ])
            ->one();
        $planassign = PlanAssign::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'IsDelete' => 0
        ])->one();
        $planrecord_cnt = PlanRecord::find()->where([

            'EmployerId' => Yii::$app->session['Employerid'],

            'UserId' => $_GET['UserId'],

            'Type' => 'ContactView',

            'PlanId' => $planassign->PlanId
        ])->count();
        $daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView', $planassign->plan->PlanId, date("Y-m-d"));
        // echo $daily_Contact."==";
        $availableviewcontact = $planassign->plan->total_daily_view_contact - $daily_Contact;
        // echo $availableviewcontact;die('==');
        if ($availableviewcontact <= 0 && $planrecord_cnt == 0) {
            throw new \yii\web\NotFoundHttpException();
            exit();
        }

        if ($planrecord_cnt == 0) {

            $planassign->UseViewContact = $planassign->UseViewContact + 1;

            $planassign->save(false);

            $planrecord = new PlanRecord();

            $planrecord->PlanId = $planassign->PlanId;

            $planrecord->EmployerId = $planassign->EmployerId;

            $planrecord->Type = 'ContactView';

            $planrecord->Amount = 1;

            $planrecord->UserId = $_GET['UserId'];

            $planrecord->save(false);
        }
        $camar = array();
        $campuscourse = CampusCourseMap::find()->where([
            'CampusId' => $UserId
        ])
            ->orderBy([
            'CourseId' => SORT_ASC
        ])
            ->all();
        foreach ($campuscourse as $cam => $camval) {
            $camar[$camval->CourseId]['course'] = $camval->course->CourseName;
            $camar[$camval->CourseId]['spec'][] = $camval->specialization->Specialization;
        }
        return $this->render('collegeprofile', [
            'campus' => $campusone,
            'campuscourse' => $camar
        ]);
    }

    public function actionCampusprofile()
    {
         $this->layout='layout';
         
        if (isset(Yii::$app->session['Campusid'])) {
            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

            $model = new AllUser();
            $campusid = Yii::$app->session['Campusid'];
            $campusone = $model->find()
                ->where([
                'UserId' => $campusid
            ])
                ->one();

            $camar = array();
            $campuscourse = CampusCourseMap::find()->where([
                'CampusId' => $campusid
            ])
                ->orderBy([
                'CourseId' => SORT_ASC
            ])
                ->all();
            // $planassign = PlanAssign::find()->where([
            //     'EmployerId' => Yii::$app->session['Employerid'],
            //     'IsDelete' => 0
            // ])->one();

            // $daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView', $isassign->plan->PlanId, date("Y-m-d"));
            // $availableviewcontact = $isassign->plan->total_daily_view_contact - $daily_Contact;
            // if ($availableviewcontact <= 0) {
            //     throw new \yii\web\NotFoundHttpException();
            //     exit();
            // }
            // $planrecord_cnt = PlanRecord::find()->where([
            //     'EmployerId' => Yii::$app->session['Employerid'],
            //     'UserId' => $_GET['UserId'],
            //     'Type' => 'ContactView',
            //     'PlanId' => $planassign->PlanId
            // ])->count();

            // if ($planrecord_cnt == 0) {
            //     $planassign->UseViewContact = $planassign->UseViewContact + 1;
            //     $planassign->save(false);
            //     $planrecord = new PlanRecord();
            //     $planrecord->PlanId = $planassign->PlanId;
            //     $planrecord->EmployerId = $planassign->EmployerId;
            //     $planrecord->Type = 'ContactView';
            //     $planrecord->Amount = 1;
            //     $planrecord->UserId = $_GET['UserId'];
            //     $planrecord->save(false);
            // }
            foreach ($campuscourse as $cam => $camval) {
                $camar[$camval->CourseId]['course'] = $camval->course->name;
                $camar[$camval->CourseId]['spec'][] = $camval->specialization->name;
            }
            return $this->render('campusprofile', [
                'campus' => $campusone,
                'campuscourse' => $camar
            ]);
        } else {
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionCampuslogout()
    {
        $session = Yii::$app->session;
        $session->open();
        unset(Yii::$app->session['Campusid']);
        unset(Yii::$app->session['CampusName']);
        unset(Yii::$app->session['CampusEmail']);
        unset(Yii::$app->session['CampusDP']);
        unset(Yii::$app->session['SocialEmail']);
        unset(Yii::$app->session['SocialName']);

        return $this->redirect([
            'campuszone'
        ]);
    }

    public function actionCampusprofileedit()
    {
        $this->layout='layout';
        // notification for employee
        $allnotification = [];
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = [];
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $model = new AllUser();
        $campusid = Yii::$app->session['Campusid'];
        $campusone = $model->find()
            ->where([
            'UserId' => $campusid
        ])
            ->one();
        $course = Course::find()->where([
            'IsDelete' => 0
        ])
            ->OrderBy([
            'CourseName' => SORT_ASC
        ])
            ->all();

        $campuscourse = CampusCourseMap::find()->where([
            'CampusId' => $campusid
        ])
            ->orderBy([
            'CourseId' => SORT_ASC
        ])
            ->all();
        $collegepic = new CollegePic();

        $docmodel = new Documents();
        $pid = $campusone->PhotoId;
        $lid = $campusone->LogoId;
        $prnid = $campusone->PresentaionId;
        if ($campusone->load(Yii::$app->request->post())) {
			//echo '<pre>';
			//print_r(Yii::$app->request->post());die;
			
            $photo = UploadedFile::getInstance($model, 'PhotoId');
            if ($photo) {
                $photo_id = $docmodel->imageUpload($photo, 'PhotoId');
            } else {
                $photo_id = $pid;
            }
            $campusone->PhotoId = $photo_id;
            $docmodel1 = new Documents();
            $logo = UploadedFile::getInstance($model, 'LogoId');
            if ($logo) {
                $logo_id = $docmodel1->imageUpload($logo, 'LogoId');
                $url = '/backend/web/';
                $pimage = $url . $model->logo->Doc;

                Yii::$app->session['CampusDP'] = $pimage;
            } else {
                $logo_id = $lid;
            }
            $campusone->LogoId = $logo_id;

            $docmodel3 = new Documents();
            $presentation = UploadedFile::getInstance($model, 'PresentaionId');
            if ($presentation) {
                $presentation_id = $docmodel3->imageUpload($presentation, 'PresentaionId');
            } else {
                $presentation_id = $prnid;
            }
            $campusone->PresentaionId = $presentation_id;
            $campusone->Address = Yii::$app->request->post()['AllUser']['Address1'] . ' , ' . Yii::$app->request->post()['AllUser']['Address2'];
            if ($campusone->save()) {
                $CollegePic = UploadedFile::getInstances($collegepic, 'PicId');
                foreach ($CollegePic as $file) {
                    $docmodel2 = new Documents();
                    $collegepic_id = $docmodel2->imageUpload($file, 'CollegePicId');
                    $collegepic = new CollegePic();
                    $collegepic->CampusId = $campusid;
                    $collegepic->PicId = $collegepic_id;
                    $collegepic->save();
                }
				
				/*Save Course & Specialization*/
					if(!empty(Yii::$app->request->post()['AllUser']['Course'])){
						/*Delete data*/
						CampusCourseMap::deleteAll([
							'CampusId' => $campusid
						]);
						$courseSpecial = Yii::$app->request->post()['AllUser']['Course'];
						$courseList = array();
						foreach($courseSpecial as $courseData){
							$courseList[$courseData['course']] = isset($courseData['specialization'])?$courseData['specialization']:"";
						}
						
						if(!empty($courseList)){
							/*echo '<pre>';
							print_r($courseList);die;*/
							foreach($courseList as $courseId => $specializationRecord){
								if(empty($specializationRecord)){
									$spec = new CampusCourseMap();
									$spec->CampusId = $campusid;
									$spec->CourseId = $courseId;
									//$spec->SpecializationId = 0;
									$spec->save();
								}else{
									foreach($specializationRecord as $specialRec){
										$spec = new CampusCourseMap();
										$spec->CampusId = $campusid;
										$spec->CourseId = $courseId;
										$spec->SpecializationId = $specialRec;
										if($spec->save(false)){
											
										}else{
											/*echo "MODEL NOT SAVED";
											  print_r($spec->getAttributes());
											  print_r($spec->getErrors());
											  exit;*/
										}
									}
								}
							}
						}
					}
                /*$allcs = Yii::$app->request->post()['AllUser']['AllC'];
                if ($allcs != '') {
                    CampusCourseMap::deleteAll([
                        'CampusId' => $campusid
                    ]);
                    $specs = explode("|", $allcs);
                    try{
                        foreach ($specs as $key => $value) {
                            if ($value != '') {
                                $valar = explode("-", $value);
                                $spec = new CampusCourseMap();
                                $spec->CampusId = $campusid;
                                $spec->CourseId = $valar[0];
                                $spec->SpecializationId = $valar[1];
                                $spec->save();
                            }
                        }
                    } catch(\Exception $e){
                        //echo "<pre>"; print_r($e->getMessage()); die;
                    }
                }*/
                Yii::$app->session->setFlash('success', "Profile edited successfully");
            } else {
                Yii::$app->session->setFlash('error', "There is some error please try again");
            }
            return $this->redirect([
                'campusprofile'
            ]);
        } else {
            $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3 and id!=4 and id!=5 and id!=6")->all();
            $NaukariQualData = NaukariSpecialization::find()->all();
            return $this->render('campusprofileedit', [
                'campus' => $campusone,
                'course' => $course,
                'campuscourse' => $campuscourse,
                'collegepic' => $collegepic,
                'naukari_specialization' => $naukari_specialization,
                'NaukariQualData' => $NaukariQualData
            ]);
        }
    }

    public function actionCreatepost()
    {
         $this->layout = 'layout';
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $employerd = AllUser::find()->select([
            'MobileNo',
            'Email',
            'ContactNo',
            'Name',
            'City',
            'State',
            'Country'
        ])
            ->where([
            'UserId' => Yii::$app->session['Campusid']
        ])
            ->one();
            
        $postajob = new CampusPost();
        if ($postajob->load(Yii::$app->request->post())) {
            //echo "<pre>";print_r(Yii::$app->request->post());die();        
            $date = date('Y-m-d');
            $totaljob = CampusPost::find()->where([
                'CampusId' => Yii::$app->session['Campusid'],
                'date(OnDate)' => $date
            ])->count();
            if ($totaljob < 2) {
                $postajob->CampusId = Yii::$app->session['Campusid'];
                $postajob->OnDate = date('Y-m-d');
                if (! empty(Yii::$app->request->post()['CampusPost']['Specialization'])) {
                    $postajob->specialization = implode(',', Yii::$app->request->post()['CampusPost']['Specialization']);
                }
                if (! empty(Yii::$app->request->post()['CampusPost']['skill'])) {
                    $postajob->skills = implode(',', Yii::$app->request->post()['CampusPost']['skill']);
                }
                $postajob->role = Yii::$app->request->post()['CampusPost']['role'];
                $postajob->course = Yii::$app->request->post()['CampusPost']['Qualification'];
				$postajob->WalkinTo = Yii::$app->request->post()['CampusPost']['WalkinTo'];
				
                if ($postajob->save()) {
                    $val = 1;
                    Yii::$app->session->setFlash('success', 'Thank you For Posting Job');
                } else {
                    $val = 0;
                    Yii::$app->session->setFlash('error', 'There is some error ,Please try again');
                }


$this->layout = 'layout';


                $jobskill = array();
                // new skill
                $rawskill = explode(",", trim(Yii::$app->request->post()['CampusPost']['RawSkill']));
                foreach ($rawskill as $rk => $rval) {
                    if ($rval != '') {
                        $indskill = trim($rval);
                        $nskill = new Skill();
                        $cskill = $nskill->find()
                            ->where([
                            'Skill' => $indskill,
                            'IsDelete' => 0
                        ])
                            ->one();
                        if (! $cskill) {
                            $nskill->Skill = $indskill;
                            $nskill->OnDate = date('Y-m-d');
                            $nskill->save();
                            $skid = $nskill->SkillId;
                        } else {
                            $skid = $cskill->SkillId;
                        }
                        $jobskill[] = $skid;
                        $jobrelatedskill = new CampusSkill();
                        $jobrelatedskill->JobId = $postajob->CampusPostId;
                        $jobrelatedskill->SkillId = $skid;
                        $jobrelatedskill->OnDate = date('Y-m-d');
                        $jobrelatedskill->save();
                    }
                }

                // $userlist=AllUser::find()->where(['AllUser.IsDelete'=>0,'VerifyStatus'=>1,'UserTypeId'=>4,'EmployeeSkill.SkillId'=>$jobskill])->joinWith(['skilldetail'])->all();
                // if($userlist)
                // {
                // foreach($userlist as $uk=>$uval)
                // {
                // $nt=new Notification();
                // $nt->JobId=$postajob->JobId;
                // $nt->UserId=$uval->UserId;
                // $nt->IsView=0;
                // $nt->save();
                // }
                // }
            } else {
                Yii::$app->session->setFlash('error', 'Your job post limit exceed, Please choose A plan or Contact admin');
            }
            return $this->redirect([
                'yourpost'
            ]);
        } else {
            $naukari_specialization = NaukariSpecialization::find()->all();

            $NaukariQualData = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3 and id!=4 and id!=5 and id!=6")->all();
            $allcategory = Position::find()->select([
                'Position',
                'PositionId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'Position' => SORT_ASC
            ])
                ->all();
            return $this->render('createpost', [
                'allcategory' => $allcategory,
                'naukari_specialization' => $naukari_specialization,
                'NaukariQualData' => $NaukariQualData,
                'model' => $postajob,
                'empd' => $employerd
            ]);
        }
    }

    public function actionChangepwd()
    {

$this->layout='layout';

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        $model = new AllUser();

        if ($model->load(Yii::$app->request->post())) {
            $cpass = md5(Yii::$app->request->post('AllUser')['CPassword']);
            $npass = Yii::$app->request->post('AllUser')['NPassword'];
            $UserId = Yii::$app->request->post('AllUser')['UserId'];
            $chk = $model->find()
                ->where([
                'UserId' => $UserId
            ])
                ->one();
            if ($chk->Password == $cpass) {
                $chk->Password = md5($npass);
                $chk->save();
                Yii::$app->session->setFlash('success', 'Password Has been Changed.');
                return $this->redirect([
                    'campusprofile'
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'Incorrect Current Password.');
                return $this->render('changepassword', [
                    'model' => $model
                ]);
            }
        } else {
            return $this->render('changepassword');
        }
    }

    public function actionForgotpassword()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $model = new AllUser();
        if ($model->load(Yii::$app->request->post())) {
            $email = Yii::$app->request->post('AllUser')['Email'];
            $chk = $model->find()
                ->where([
                'Email' => $email,
                'UserTypeId' => 4
            ])
                ->one();
            if ($chk) {
                $ucode = 'CB' . time();
                $chk->VerifyKey = $ucode;
                $chk->ForgotStatus = 0;
                $chk->save();

                // var_dump($chk->getErrors());

                $to = $email;
                $from = Yii::$app->params['adminEmail'];
                $subject = "Reset Password";

                $html = "<html>
               <head>
               <title>Create a new password</title>
               </head>
               <body>
               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>
                <tbody><tr>
                                       <td><img src='http://mycareerbugs.com/images/logo.png' title='Career Bugs' alt='Career Bugs' style='margin-top:10px;width:150px;'></td>
                                   </tr>
                                   <tr>
                                       <td style='height:30px'></td>
                                   </tr>
                           <tr>
                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://mycareerbugs.com' target='_blank'> Career Bugs </a>
               <br><br>
               <span style='font-size:16px;line-height:1.5'>
               Please click in the link to create your new password
               <br/>
               <a href='http://mycareerbugs.com/index.php/campus/resetpassword?ucode=$ucode'>
               <span style='display: block; height:50 px;width:100px;color: #ffffff;text-decoration: none;font-size: 14px;text-align: center;font-family: 'Open Sans',Gill Sans,Arial,Helvetica,sans-serif;font-weight: bold;line-height: 45px;'>Click Here</span>
               </a>
              
               </span>
               </h2>
               </td>
               </tr>
               </tbody>
               </table>
               </body>
               </html>";
                $mail = new ContactForm();
                $mail->sendEmail($to, $from, $html, $subject);
                Yii::$app->session->setFlash('success', 'An email has been sent to your mail-id.Please click on the link provided in the mail to reset your password.
In case you do not receive your password reset email shortly, please check your spam folder also..');
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('forgotpassword', [
            'model' => $model
        ]);
    }

    public function actionResetpassword($ucode)
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $model = new AllUser();
        $chk = $model->find()
            ->where([
            'VerifyKey' => $ucode,
            'UserTypeId' => 4
        ])
            ->one();

        if ($model->load(Yii::$app->request->post())) {
            $chk->Password = md5($model->Password);
            $chk->save();
            Yii::$app->session->setFlash('success', 'You have successfully created a new password.');
            return $this->redirect([
                'campuszone'
            ]);
        } else {
            if ($chk) {
                if ($chk->ForgotStatus == 0) {
                    $chk->ForgotStatus = 1;
                    $chk->save();
                    return $this->render('resetpassword', [
                        'ucode' => $ucode,
                        'model' => $model
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', 'Your reset password link expired');
                    return $this->redirect([
                        'forgotpassword'
                    ]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Please give your registered EmailId for create a new password.');
                return $this->redirect([
                    'forgotpassword'
                ]);
            }
        }
    }

    public function actionDashboard()
    {
        $this->layout='layout';
        
        if (isset(Yii::$app->session['Campusid'])) {

            // contactus block
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block
            $cp = new FooterCopyright();
            $allcp = $cp->find()->one();
            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block
            $dblock = new FooterDevelopedblock();
            $developerblock = $dblock->find()->one();
            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block
            $socialicon = new SocialIcon();
            $sicon = $socialicon->find()->one();
            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block
            $th = new FooterThirdBlock();
            $thirdblock = $th->find()->one();
            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

            $postjob = new CampusPost();
            $totaljob = $postjob->find()
                ->where([
                'CampusId' => Yii::$app->session['Campusid']
            ])
                ->count();
            $totalactive = $postjob->find()
                ->where([
                'CampusId' => Yii::$app->session['Campusid'],
                'JobStatus' => 0
            ])
                ->count();

            $candidateapplied = 0;

            $candidatebookmarked = 0;

            return $this->render('dashboard', [
                'totaljob' => $totaljob,
                'totalactivejob' => $totalactive,
                'appliedjob' => $candidateapplied,
                'bookmarked' => $candidatebookmarked
            ]);
        } else {
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionYourpost()
    {
         $this->layout = 'layout';
         
        if (isset(Yii::$app->session['Campusid'])) {

            // contactus block
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block
            $cp = new FooterCopyright();
            $allcp = $cp->find()->one();
            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block
            $dblock = new FooterDevelopedblock();
            $developerblock = $dblock->find()->one();
            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block
            $socialicon = new SocialIcon();
            $sicon = $socialicon->find()->one();
            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block
            $th = new FooterThirdBlock();
            $thirdblock = $th->find()->one();
            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

            $postjob = new CampusPost();
            $allpost = $postjob->find()
                ->where([
                'CampusId' => Yii::$app->session['Campusid']
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
            return $this->render('yourpost', [
                'allpost' => $allpost
            ]);
        } else {
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionPostdetail($JobId)
    {
        if (isset(Yii::$app->session['Campusid'])) {

            // contactus block
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block
            $cp = new FooterCopyright();
            $allcp = $cp->find()->one();
            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block
            $dblock = new FooterDevelopedblock();
            $developerblock = $dblock->find()->one();
            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block
            $socialicon = new SocialIcon();
            $sicon = $socialicon->find()->one();
            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block
            $th = new FooterThirdBlock();
            $thirdblock = $th->find()->one();
            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

        } 
		
		
            $postjob = new CampusPost();
            $allpost = $postjob->find()
                ->where([
                'CampusPostId' => $JobId
            ])
                ->one();
            return $this->render('postdetail', [
                'allpost' => $allpost
            ]);
    }

    public function actionClosestatus()
    {
        $jobid = Yii::$app->request->get()['jobid'];
        $postjob = new CampusPost();
        $pdetail = $postjob->find()
            ->where([
            'CampusPostId' => $jobid
        ])
            ->one();
        $pdetail->JobStatus = 1;
        if ($pdetail->save()) {
            $msg = "Job Closed Successfully";
        } else {
            $msg = "There is some error please try again";
        }

        echo json_encode($msg);
    }

    public function actionJobedit()
    {
		Yii::$app->cache->flush();	
        if (isset(Yii::$app->session['Campusid'])) {

            // contactus block
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block
            $cp = new FooterCopyright();
            $allcp = $cp->find()->one();
            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block
            $dblock = new FooterDevelopedblock();
            $developerblock = $dblock->find()->one();
            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block
            $socialicon = new SocialIcon();
            $sicon = $socialicon->find()->one();
            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block
            $th = new FooterThirdBlock();
            $thirdblock = $th->find()->one();
            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

            $JobId = Yii::$app->request->get()['JobId'];
            $postjob = new CampusPost();
            $postdetail = $postjob->find()
                ->where([
                'CampusPostId' => $JobId
            ])
                ->one();

            if ($postdetail->load(Yii::$app->request->post())) {
				
				if (! empty(Yii::$app->request->post()['CampusPost']['Specialization'])) {
                    $postdetail->specialization = implode(',', Yii::$app->request->post()['CampusPost']['Specialization']);
                }
				
				$postdetail->course = Yii::$app->request->post()['CampusPost']['Qualification'];
				
				if (! empty(Yii::$app->request->post()['CampusPost']['WalkinTo'])) {
					$postdetail->WalkinTo = Yii::$app->request->post()['CampusPost']['WalkinTo'];
				}
				
				
                $postdetail->save();
                // var_dump($postdetail->getErrors());
				
                $jobrelatedskill = new CampusSkill();
                CampusSkill::deleteAll([
                    'JobId' => $JobId
                ]);
                // new skill
                $rawskill = explode(",", Yii::$app->request->post()['CampusPost']['RawSkill']);
                foreach ($rawskill as $rk => $rval) {
                    if ($rval != '') {
                        $indskill = trim($rval);
                        $nskill = new Skill();
                        $cskill = $nskill->find()
                            ->where([
                            'Skill' => $indskill,
                            'IsDelete' => 0
                        ])
                            ->one();
                        if (! $cskill) {
                            $nskill->Skill = $indskill;
                            $nskill->OnDate = date('Y-m-d');
                            $nskill->save();
                            $skid = $nskill->SkillId;
                        } else {
                            $skid = $cskill->SkillId;
                        }
                        $jobrelatedskill = new CampusSkill();
                        $jobrelatedskill->JobId = $JobId;
                        $jobrelatedskill->SkillId = $skid;
                        $jobrelatedskill->OnDate = date('Y-m-d');
                        $jobrelatedskill->save();
                    }
                }
                Yii::$app->session->setFlash('success', 'Successfully Edited This Job.');
                return $this->redirect([
                    'yourpost'
                ]);
            } else {
                return $this->render('jobedit', [
                    'allpost' => $postdetail
                ]);
            }
        } else {
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionJobsearch()
    {

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $postjob = new PostJob();

        // All Job
        if (isset(Yii::$app->request->get()['JobCategoryId'])) {
            $jobcategoryid = Yii::$app->request->get()['JobCategoryId'];
        } else {
            $jobcategoryid = '';
        }
        $latest = '';
        $role = '';
        $state = '';
        $jobtype = array();
        $skillby = array();
        $companyby = array();
        if (isset(Yii::$app->request->get()['latest'])) {
            $latest = Yii::$app->request->get()['latest'];
        }
        if (isset(Yii::$app->request->get()['role'])) {
            $role = Yii::$app->request->get()['role'];
        }
        if (isset(Yii::$app->request->get()['location'])) {
            $state = Yii::$app->request->get()['location'];
        }
        if (isset(Yii::$app->request->get()['salaryrange']) && Yii::$app->request->get()['salaryrange'] != '') {
            $salary = explode(",", Yii::$app->request->get()['salaryrange']);
        } else {
            $salary = '';
        }
        if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {
            $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
        }
        if (isset(Yii::$app->request->get()['skillby']) && Yii::$app->request->get()['skillby'] != '') {
            $skillby = explode(",", Yii::$app->request->get()['skillby']);
        }
        if (isset(Yii::$app->request->get()['companyby']) && Yii::$app->request->get()['companyby'] != '') {
            $companyby = explode(",", Yii::$app->request->get()['companyby']);
        }
		
		$ddate = date('Y-m-d', strtotime("-$latest days"));

        $alljob = $postjob->find()
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus'
        ])
            ->andFilterWhere([
            'JobCategoryId' => $jobcategoryid,
            'PositionId' => $role,
            'JobType' => $jobtype,
            'Salary' => $salary,
            'JobRelatedSkill.SkillId' => $skillby,
            'CompanyName' => $companyby
        ])
		->andFilterWhere([
			'or',
			[
				'like',
				'Location',
				$state
			]
		])
		->andFilterWhere([
			'>=',
			'DATE( PostJob.OnDate )',
			$ddate
		])
         ->joinWith([
            'jobRelatedSkills'
        ])
            ->orderBy([
            'PostJob.OnDate' => SORT_DESC
        ]);
		
		if (isset(Yii::$app->request->get()['nak_course']) && Yii::$app->request->get()['nak_course'] != '') {
			
			$alljob = $alljob->andFilterWhere([
				'or',

                [
                    'like',
                    'PostJob.course',
                    Yii::$app->request->get()['nak_course']
                ]
            ]);
            
        }
		
		if (isset(Yii::$app->request->get()['education']) && Yii::$app->request->get()['education'] != '') {
			
			$alljob = $alljob->andFilterWhere([
				'or',

                [
                    'like',
                    'PostJob.SpecialQualification',
                    Yii::$app->request->get()['education']
                ]
            ]);
            
        }
		
		if (isset(Yii::$app->request->get()['qualification']) && Yii::$app->request->get()['qualification'] != '') {
			
            $alljob = $alljob->andFilterWhere([
				'or',

                [
                    'like',
                    'qualifications',
                    Yii::$app->request->get()['qualification']
                ]
            ]);
			
        }

        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }
        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // position
        $position = new Position();
        $allposition = $position->find()
            ->where([
            'IsDelete' => 0
        ])
            ->all();

        // skill
        $allskill = Skill::find()->where([
            'IsDelete' => 0
        ]);
        if (isset($_GET['role']) && $_GET['role'] != '') {
            $allskill->andWhere(['PositionId' => $_GET['role']]);
            $allskill = $allskill->all();
        }else {
            $allskill = [];
        }
		
		
        // company
        $allcompany = PostJob::find()->select([
            'CompanyName'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => 'Campus'
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        //
        // hot categories
        $hotcategory = $postjob->find()
            ->select([
            'count(*) as cnt',
            'Industry.IndustryName as CategoryName',
            'Industry.IndustryId'
        ])
            ->joinWith([
            'jobCategory'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus',
            'Industry.IsDelete' => 0
        ])
            ->groupBy([
            'PostJob.JobCategoryId'
        ])
            ->all();
			
		$NaukariQualData = NaukariSpecialization::find()->all();

        $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3")->all();	
        $qualifications = NaukariQualData::find()->where("parent_id=0")->all();	
		$allcategory = Industry::find()->where("IsDelete=0")
            ->orderBy([
            'IndustryName' => SORT_ASC
        ])
            ->all();
		$jbpst = new PostJob();	
		  $this->layout = 'layout';
		  
		if (Yii::$app->request->isAjax) {
            return $this->renderPartial('jobsearch_ajax', array(
                'jbpst' => $jbpst,
                'alljob' => $alljob,
                'pages' => $pages
            ));
        } else {
			return $this->render('jobsearch', [
				'alljob' => $alljob,
				'pages' => $pages,
				'hotcategory' => $hotcategory,
				'role' => $allposition,
				'allskill' => $allskill,
				'allcompany' => $allcompany,
				'NaukariQualData' => $NaukariQualData,
				'naukari_specialization' => $naukari_specialization,
				'industry' => $allcategory,
				'qualifications' => $qualifications
			]);
		}
    }

    public function actionCompanyjob()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $postjob = new PostJob();

        // All Job
        if (isset(Yii::$app->request->get()['JobCategoryId'])) {
            $jobcategoryid = Yii::$app->request->get()['JobCategoryId'];
        } else {
            $jobcategoryid = '';
        }
        $latest = '';
        $role = '';
        $state = '';
        $jobtype = array();
        $skillby = array();
        $companyby = array();
        if (isset(Yii::$app->request->get()['latest'])) {
            $latest = Yii::$app->request->get()['latest'];
        }
        if (isset(Yii::$app->request->get()['role'])) {
            $role = Yii::$app->request->get()['role'];
        }
        if (isset(Yii::$app->request->get()['state'])) {
            $state = Yii::$app->request->get()['state'];
        }
        if (isset(Yii::$app->request->get()['salaryrange']) && Yii::$app->request->get()['salaryrange'] != '') {
            $salary = explode(",", Yii::$app->request->get()['salaryrange']);
        } else {
            $salary = '';
        }
        if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {
            $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
        }
        if (isset(Yii::$app->request->get()['skillby']) && Yii::$app->request->get()['skillby'] != '') {
            $skillby = explode(",", Yii::$app->request->get()['skillby']);
        }
        if (isset(Yii::$app->request->get()['companyby']) && Yii::$app->request->get()['companyby'] != '') {
            $companyby = explode(",", Yii::$app->request->get()['companyby']);
        }

        $alljob = $postjob->find()
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus'
        ])
            ->andFilterWhere([
            'JobCategoryId' => $jobcategoryid,
            'PositionId' => $role,
            'State' => $state,
            'JobType' => $jobtype,
            'Salary' => $salary,
            'JobRelatedSkill.SkillId' => $skillby,
            'CompanyName' => $companyby
        ])
            ->andFilterWhere([
            '>',
            'DATE( PostJob.OnDate )',
            "(DATE(NOW( ) - INTERVAL $latest DAY))"
        ])
            ->joinWith([
            'jobRelatedSkills',
            ''
        ])
            ->orderBy([
            'PostJob.OnDate' => SORT_DESC
        ]);

        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }
        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // position
        $position = new Position();
        $allposition = $position->find()
            ->where([
            'IsDelete' => 0
        ])
            ->all();

        // skill
        $allskill = Skill::find()->where([
            'IsDelete' => 0
        ])->all();

        // company
        $allcompany = PostJob::find()->select([
            'CompanyName'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => 'Candidate'
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        //
        // hot categories
        $hotcategory = $postjob->find()
            ->select([
            'count(*) as cnt',
            'Industry.IndustryName as CategoryName',
            'Industry.IndustryId'
        ])
            ->joinWith([
            'jobCategory'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus',
            'Industry.IsDelete' => 0
        ])
            ->groupBy([
            'PostJob.JobCategoryId'
        ])
            ->all();
        return $this->render('jobsearch', [
            'alljob' => $alljob,
            'pages' => $pages,
            'hotcategory' => $hotcategory,
            'role' => $allposition,
            'allskill' => $allskill,
            'allcompany' => $allcompany
        ]);
    }

    public function actionJobdetail()
    {
		
        if (isset(Yii::$app->session['Campusid'])) {

            // contactus block
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block
            $cp = new FooterCopyright();
            $allcp = $cp->find()->one();
            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block
            $dblock = new FooterDevelopedblock();
            $developerblock = $dblock->find()->one();
            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block
            $socialicon = new SocialIcon();
            $sicon = $socialicon->find()->one();
            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block
            $th = new FooterThirdBlock();
            $thirdblock = $th->find()->one();
            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end

            $this->layout = 'layout';
            $JobId = Yii::$app->request->get()['JobId'];
			$slug = (isset(Yii::$app->request->get()['alias'])?Yii::$app->request->get()['alias']:"");
			$postjob = new PostJob();
            $postdetail = $postjob->find()
                ->where([
                'Slug' => $slug
            ])
                ->one();
			/*echo '<pre>';
			print_r($postdetail);die;*/
            if (isset(Yii::$app->request->get()['Nid'])) {
                $nid = Yii::$app->request->get()['Nid'];
                $notification = Notification::find()->where([
                    'NotificationId' => $nid
                ])->one();
                $notification->IsView = 1;
                $notification->save();
            }
            return $this->render('jobdetail', [
                'allpost' => $postdetail
            ]);
        } else {
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionBookmark($JobId)
    {
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        if (isset(Yii::$app->session['Campusid'])) {
            $model = new AppliedJob();
            $alreadyapplied = $model->find()
                ->where([
                'IsDelete' => 0,
                'JobId' => $JobId,
                'UserId' => Yii::$app->session['Campusid']
            ])
                ->one();
            if ($alreadyapplied) {
                if ($alreadyapplied->Status == 'Bookmark') {
                    Yii::$app->session->setFlash('error', "You have already bookmarked  this Job.");
                } else {
                    Yii::$app->session->setFlash('error', "You have already Applied  this Job.");
                }
            } else {
                $model->JobId = $JobId;
                $model->UserId = Yii::$app->session['Campusid'];
                $model->OnDate = date("Y-m-d");
                $model->Status = "Bookmark";
                $model->save();

                $noofjobapplied = $model->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Campusid'],
                    'IsDelete' => 0
                ])
                    ->count();
                Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied;

                $jobdet = PostJob::find()->where([
                    'JobId' => $JobId
                ])->one();
                $empnt = new EmpNotification();
                $empnt->EmpId = $jobdet->EmployerId;
                $empnt->UserId = Yii::$app->session['Campusid'];
                $empnt->JobId = $JobId;
                $empnt->Type = 'Bookmarked';
                $empnt->ForType = 'Candidate';
                $empnt->IsView = 0;
                $empnt->OnDate = date('Y-m-d H:i:s');
                $empnt->save();

                // var_dump($model->getErrors());
                Yii::$app->session->setFlash('success', "You have Bookmarked a job.");
            }
            return $this->redirect([
                'jobsearch'
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Please Login to bookmark this job");
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionApplyjob($JobId)
    {
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;
        if (isset(Yii::$app->session['Campusid'])) {
            $model = new AppliedJob();
            $alreadyapplied = $model->find()
                ->where([
                'IsDelete' => 0,
                'JobId' => $JobId,
                'UserId' => Yii::$app->session['Campusid']
            ])
                ->one();
            if ($alreadyapplied) {
                if ($alreadyapplied->Status == 'Bookmark') {
                    $alreadyapplied->Status = "Applied";
                    $alreadyapplied->save();
                    Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
                } else {
                    Yii::$app->session->setFlash('error', "You have already applied for this Job.");
                }
            } else {
                $model->JobId = $JobId;
                $model->UserId = Yii::$app->session['Campusid'];
                $model->OnDate = date("Y-m-d");
                $model->Status = "Applied";
                $model->save();

                Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
            }

            $jobdet = PostJob::find()->where([
                'JobId' => $JobId
            ])->one();
            $empnt = new EmpNotification();
            $empnt->EmpId = $jobdet->EmployerId;
            $empnt->UserId = Yii::$app->session['Employeeid'];
            $empnt->JobId = $JobId;
            $empnt->Type = 'Applied';
            $empnt->ForType = 'Candidate';
            $empnt->IsView = 0;
            $empnt->OnDate = date('Y-m-d H:i:s');
            $empnt->save();

            $noofjobapplied = $model->find()
                ->where([
                'Status' => 'Applied',
                'UserId' => Yii::$app->session['Campusid'],
                'IsDelete' => 0
            ])
                ->count();
            Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied;

            return $this->redirect([
                'jobsearch'
            ]);
        } else {
            Yii::$app->session->setFlash('error', "Please Login to apply this job");
            return $this->redirect([
                'campuszone'
            ]);
        }
    }

    public function actionBookmarkedjob()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $model = new AppliedJob();
        $appliedjobs = $model->find()->where([
            'IsDelete' => 0,
            'Status' => 'Bookmark',
            'UserId' => Yii::$app->session['Campusid']
        ]);

        $pages = new Pagination([
            'totalCount' => $appliedjobs->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }
        $appliedjobs = $appliedjobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('bookmarkedjob', [
            'model' => $appliedjobs,
            'pages' => $pages
        ]);
    }

    public function actionAppliedjob()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $appliedjobs = AppliedJob::find()->where([
            'IsDelete' => 0,
            'Status' => 'Applied',
            'UserId' => Yii::$app->session['Campusid']
        ]);
        $noofjobapplied = clone $appliedjobs;
        Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied->count();
        // Top Job
        $topjob = PostJob::find()->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $appliedjobs
        ]);

 $this->layout = 'layout';
 
        return $this->render('appliedjob', [
            'model' => $appliedjobs,
            'pages' => $pages,
            'topjob' => $topjob,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionTeam()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        return $this->render('team');
    }

    public function actionCampuspost()
    {

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

       $this->layout = 'layout';
       
       
        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;
		
        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        $state = '';
        if (isset(Yii::$app->request->get()['state'])) {
            $state = Yii::$app->request->get()['state'];
        }
		
		$city = '';
        if (isset(Yii::$app->request->get()['city'])) {
            $city = Yii::$app->request->get()['city'];
        }
		
        $campuspost = CampusPost::find()->where([
            'IsDelete' => 0,
            'JobStatus' => 0
        ])
            ->andFilterWhere([
            'State' => $state
        ]) 
		->andFilterWhere([
            'City' => $city
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ]);
		
        if (! empty(Yii::$app->request->get()['role'])) {
            $campuspost = $campuspost->andWhere([
                'role' => Yii::$app->request->get()['role']
            ]);
        }
        if (! empty(Yii::$app->request->get()['course'])) {
            $campuspost = $campuspost->andWhere([
                'course' => Yii::$app->request->get()['course']
            ]);
        }
		
		if (! empty(Yii::$app->request->get()['latest'])){
			$latest = Yii::$app->request->get()['latest'];
			$ddate = date('Y-m-d', strtotime("-$latest days"));
			$campuspost = $campuspost->andWhere([
				'>=',
                'DATE( CampusPost.OnDate )',
                $ddate
			]);
		}

        if (! empty(Yii::$app->request->get()['specialization'])) {
			
            $campuspost->andWhere([
                'like',
                'specialization',
                Yii::$app->request->get()['specialization']
            ]);
        }
        if (! empty(Yii::$app->request->get()['skill'])) {
            $language_ary = explode(',', Yii::$app->request->get()['skill']);
            if (isset($language_ary) && ! empty($language_ary)) {
                foreach ($language_ary as $key => $value) {
                    $campuspost->andFilterWhere([
                        'or',
                        [
                            'like',
                            'skills',
                            '%' . $value . '%'
                        ]
                    ]);
                }
            }
        }
		
		if (! empty(Yii::$app->request->get()['collegeby'])) {
            $college_ary = explode(',', Yii::$app->request->get()['collegeby']);
            if (isset($college_ary) && !empty($college_ary)) {
				$campuspost->andFilterWhere([
					'CampusId'=>$college_ary
				]);
            }
        }
		 /*$query = $campuspost->createCommand()->rawSql;
          echo "<pre>";
           print_r($query);
            die();*/
		
        $pages = new Pagination([
            'totalCount' => $campuspost->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 4;
        }
        $campuspost = $campuspost->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
			
				
        $allcategory = Position::find()->select([
            'Position',
            'PositionId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'Position' => SORT_ASC
        ])
            ->all();
        $naukari_specialization = NaukariSpecialization::find()->all();
        $NaukariQualData = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3 and id!=4 and id!=5 and id!=6")->all();
		
		$campusIds = ArrayHelper::map(CampusPost::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ]) 
		
		->groupBy([
            'CampusId'
        ]) 
            ->all(), 'CampusId', 'CampusId');
			
		$collegeList = ArrayHelper::map(AllUser::find()->where([
            'IsDelete' => 0,
			'UserId' => $campusIds,
			'EntryType' => 'Campus'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ]) 
		->groupBy([
            'UserId'
        ]) 
        ->all(), 'UserId', 'CollegeName');
		
        return $this->render('campuspost', [
            'NaukariQualData' => $NaukariQualData,
            'naukari_specialization' => $naukari_specialization,
            'allcategory' => $allcategory,
            'model' => $campuspost,
			'collegeList' => $collegeList,
            'pages' => $pages
        ]);
    }

    public function actionStudentlist()
    {
        $this->layout='layout';
        
        if (isset(Yii::$app->request->post()['studentsearch'])) {
            $courseid = Yii::$app->request->post()['course'];
            $specialization = Yii::$app->request->post()['specialization'];
            $allcourse = CampusCourseMap::find()->where([
                'CampusId' => Yii::$app->session['Campusid']
            ])->andFilterWhere([
                'CourseId' => $courseid,
                'SpecializationId' => $specialization
            ]);
        } else {
            $allcourse = CampusCourseMap::find()->where([
                'CampusId' => Yii::$app->session['Campusid']
            ]);
        }
        $pages = new Pagination([
            'totalCount' => $allcourse->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }
        $allcourse = $allcourse->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $course = CampusCourseMap::find()->where([
            'CampusId' => Yii::$app->session['Campusid']
        ])
            ->groupBy([
            'CourseId'
        ])
            ->all();
        $specialization = CampusCourseMap::find()->where([
            'CampusId' => Yii::$app->session['Campusid']
        ])
            ->groupBy([
            'SpecializationId'
        ])
            ->all();

        $allstudent = AllUser::find()->where([
            'CampusId' => Yii::$app->session['Campusid'],
            'IsDelete' => 0
        ])->all();
        
     
         $this->layout = 'layout';
        return $this->render('studentlist', [
            'allstudent' => $allstudent,
            'allcourse' => $allcourse,
            'course' => $course,
            'specialization' => $specialization,
            'pages' => $pages
        ]);
    }
	
	public function actionSpecializationlist()
	{
		$courseId = Yii::$app->request->get()['courseId'];
		$response = array();
		if(!empty($courseId)){
			$response = NaukariSpecialization::getSpecializationList();
		}
		echo json_encode($response);die;
	}
	
	/*Applied Campus*/
	public function actionAppliedcampus(){
		$campusPostId = Yii::$app->request->get()['CampusPostId'];
		$userId = Yii::$app->session['Employerid'];
		
		if(!empty($campusPostId) && !empty($userId)){
			$appliedCampus = CampusPostApplied::find()->where([
				'UserId' => Yii::$app->session['Employerid'],
				'CampusPostId' => $campusPostId
			])->one();
			if($appliedCampus){
				Yii::$app->session->setFlash('error', "You have already applied this post.");
			}else{
				$appliedCampus = new CampusPostApplied();
				$appliedCampus->UserId = $userId;
				$appliedCampus->CampusPostId = $campusPostId;
				$appliedCampus->CreatedAt = date('Y-m-d H:i:s');
				$appliedCampus->save();
				Yii::$app->session->setFlash('success', "You have successfully applied this post.");
			}
		}
		 return $this->redirect([
                'campuspost'
            ]);
	}
	
	/*Applied Company*/
	public function actionAppliedcompany(){
		// contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
		$campusPostId = ArrayHelper::map(CampusPost::find()->where([
            'CampusId' => Yii::$app->session['Campusid'],
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all(), 'CampusPostId', 'CampusPostId');  
		
		
		
        $appliedjobs = CampusPostApplied::find()->where([
            'CampusPostId' => $campusPostId
        ]);
        $noofjobapplied = clone $appliedjobs;
        Yii::$app->session['NoofCompanyAppliedc'] = $noofjobapplied->count();
        // Top Job
        $topjob = PostJob::find()->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $appliedjobs
        ]);

 $this->layout = 'layout';
        return $this->render('appliedcompany', [
            'model' => $appliedjobs,
            'pages' => $pages,
            'topjob' => $topjob,
            'dataProvider' => $dataProvider
        ]);
	}

    public function actionSkills()

    {
        $PositionId = Yii::$app->request->get()['roleid'];
        $type = '';
        if (isset(Yii::$app->request->get()['type'])) {
            $type = Yii::$app->request->get()['type'];
        }

        // skill
        if (empty($PositionId)) {
            die('Not Found');
        }
        $allcategory = Position::find()->select([
            'Position',
            'PositionId'
        ])
            ->where([
            'Position' => $PositionId
        ])
            ->one();
        if (count($allcategory) > 0) {
            $PositionId = $allcategory->PositionId;
        }
        $allskill = Skill::find()->where([
            'IsDelete' => 0,
            'PositionId' => $PositionId
        ])->all();

        if ((isset($type) && $type == 'candidate')) {

            $onclick = 'candidatefsearch()';

            $class = 'cskillby';
        } elseif (isset($type) && $type == 'ecandidate') {

            $onclick = 'candidatesearch()';

            $class = 'cskillby';
        } elseif (isset($type) && $type == 'tskillby') {

            $onclick = 'getteamsearch()';

            $class = 'tskillby';
        } else {

            $onclick = 'getsearch()';

            $class = 'skillby';
        }

        foreach ($allskill as $sk => $svalue) {

            ?>

<span>

	<div class="checkbox">

		<label> <input name="CampusPost[skill][]" type="checkbox"
			class="<?php echo $class; ?>" value="<?=$svalue->Skill;?>"> <span
			class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> <a><?=$svalue->Skill;?> </a>

		</label>

	</div>

</span>

<?php
        }

        die();
    }
}
?>