<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\AllUser;
use common\models\Industry;
use common\models\UserType;
use yii\helpers\Url;
use common\models\Notification;
use common\models\EmpNotification;
use common\models\Skill;
use common\models\Course;
use common\models\Position;
use common\models\Documents;
use common\models\Education;
use common\models\Experience;
use common\models\ContactUs;
use common\models\PostJob;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use common\models\NewsLetter;
use common\models\AppliedJob;
use common\models\FooterAboutus;
use common\models\FooterContactus;
use common\models\FooterCopyright;
use common\models\FooterDevelopedblock;
use common\models\SocialIcon;
use common\models\FooterThirdBlock;
use common\models\Feedback;
use common\models\PeoplesayBlock;
use common\models\EmployeeSkill;
use common\models\Blog;
use common\models\BlogCategory;
use common\models\BlogComment;
use common\models\JobAlert;
use common\models\City;
use common\models\Plan;
use common\models\Content;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
$basepath = str_replace('frontend', 'backend', Yii::$app->basePath);
Yii::$app->params['uploadPath'] = $basepath . '/web/imageupload/';

use kartik\mpdf\Pdf;
use common\models\PlanAssign;
use common\models\PlanRecord;

/**
 * Site controller
 */
class ContentController extends Controller
{

    /**
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'signup'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'signup',
                            'index',
                            'jobseach',
                            'employersregister',
                            'companyprofileeditpage',
                            'resetpassword',
                            'applyjob'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?'
                        ]
                    ],
                    [
                        'actions' => [
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    // find team
    public function actionHelp()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        return $this->render('help');
    }

    public function actionPrivacy()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $content = Content::find()->one();
        return $this->render('privacy', [
            'model' => $content
        ]);
    }

    public function actionTerms()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $content = Content::find()->one();
        return $this->render('terms', [
            'model' => $content
        ]);
    }

    public function actionFaq()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end

        $content = Content::find()->one();
        return $this->render('faq', [
            'model' => $content
        ]);
    }

    public function actionMycareerbugcv()
    {
        if (isset(Yii::$app->session['Employerid'])) {
         
              if(isset($_REQUEST['JobId'])){ 
               
                $userId=$_REQUEST['UserId'];
               
                $postJob = PostJob::findOne($_REQUEST['JobId']);
                $viewLimit=$postJob->viewLimit;
                $oldJsonViewProfileData=$postJob->viewProfile;
                $previousSeenProfile='';
                
                if(!empty($postJob->viewProfile)){
                  $viewProfileArray = explode(',', $oldJsonViewProfileData);
                  if (in_array($userId, $viewProfileArray)){
                      $previousSeenProfile=1;
                        return $this->redirect(['/content/mycareerbugcv','UserId' => $userId,'amp;JobId' => $_REQUEST['JobId']]);
                  }
                }
                
                if($viewLimit>0){
                  if(empty($previousSeenProfile)){
                    $updateModel=PostJob::findOne($_REQUEST['JobId']);
                    $updateModel->viewLimit=$viewLimit-1;
                    if($oldJsonViewProfileData){
                         $updateModel->viewProfile .=','.$userId;
                    }else{
                         $updateModel->viewProfile =$userId;
                    }
                    $updateModel->save();
                    }
                }
           }
           
            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();
           
         
            // Start // Vishal Sinha
            if ($planassign->isExpired()) {
                $data = [];
                $data['message'] = 'Your Plan is Expired, Please Contact Administration';
                \Yii::$app->session->setFlash('error', $data['message']);
                return $this->redirect([
                    '/hirecandidate'
                ]);
            }
            
        $userId = !empty($_GET['UserId']) ? $_GET['UserId'] : '';

        if(empty($userId))
        {
          echo json_encode(['status'=>'error','message'=>'something is wrong']);
          exit;
        }

        $planRecordModel = Plan::find()->where(['PlanId'=>$planassign->PlanId])->select(['total_daily_cv_download'])->one();

        $todayusedCVDownload = PlanRecord::find()->where([
            'PlanId'=>$planassign->PlanId,
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'CVDownload'
            ])->count();

        if($planRecordModel->total_daily_cv_download >= $todayusedCVDownload)
        {
          $planrecord_cnt = PlanRecord::isAlreadyRecord($planassign, $_GET['UserId'], 'CVDownload');
            if ($planrecord_cnt == 0) {
                PlanRecord::createNewPlanRecord($planassign, $_GET['UserId'],'CVDownload','UseCVDownload');
            }
        }else{

            $todayusedCVDownloads = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'UserId'=> $userId,
                'Type' => 'CVDownload'
            ]);
            if(empty($todayusedCVDownloads))
            {
                $data['message'] = 'Your daily limit exhausted';
                \Yii::$app->session->setFlash('error', $data['message']);
                return $this->redirect([
                    '/hirecandidate'
                ]);
            }
        }
            
            
            
           
            // end
        }

        $this->layout = 'blanklayout';
        $profile = AllUser::find()->where([
            'UserId' => $_GET['UserId']
        ])->one();
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('makecv', [
            'profile' => $profile
        ], true);

        $ppp = preg_replace('/\s+/', '_', $profile->Name . $profile->UserId);
        $ext = 'pdf';

        $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'filename' => $path,
            'destination' => Pdf::DEST_FILE,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options' => [
                'title' => 'My Career Bugs'
            ],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => [
                    'My Career Bugs Job Portal'
                ],
                'SetFooter' => [
                    '{PAGENO}'
                ]
            ]
        ]);

        // return the pdf output as per the destination setting
        echo $pdf->render($path, 'F');
        echo '<script>window.location.href="/backend/web/imageupload/' . $ppp . ".{$ext}" . '"</script>';
    }
}
?>