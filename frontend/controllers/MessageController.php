<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\AllUser;
use common\models\Industry;
use common\models\UserType;
use yii\helpers\Url;

use common\models\Notification;
use common\models\EmpNotification;
use common\models\Skill;
use common\models\Course;
use common\models\Position;
use common\models\Documents;
use common\models\Education;
use common\models\Experience;
use common\models\ContactUs;
use common\models\PostJob;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use common\models\NewsLetter;
use common\models\AppliedJob;
use common\models\FooterAboutus;
use common\models\FooterContactus;
use common\models\FooterCopyright;
use common\models\FooterDevelopedblock;
use common\models\SocialIcon;
use common\models\FooterThirdBlock;
use common\models\Feedback;
use common\models\PeoplesayBlock;
use common\models\EmployeeSkill;
use common\models\Blog;
use common\models\BlogCategory;
use common\models\BlogComment;
use common\models\JobAlert;
use common\models\City;
use common\models\Content;
use common\models\Messages;

use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
/**
 * Site controller
 */
class MessageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup','index','jobseach','employersregister','companyprofileeditpage','resetpassword','applyjob'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    
    //find team
    public function actionIndex()
    {
         //footer section
        
        //first block
        $about=new FooterAboutus();
        $footerabout=$about->find()->one();
        Yii::$app->view->params['footerabout']=$footerabout;
        
        //contactus block
        $cn=new FooterContactus();
        $footercontact=$cn->find()->one();
        Yii::$app->view->params['footercontact']=$footercontact;
        
        //second block
        $jobcategory=new JobCategory();
        $allhotcategory=$jobcategory->find()->select(['CategoryName','JobCategoryId'])->where(['IsDelete'=>0])->orderBy(['OnDate'=>SORT_DESC])->limit(7)->all();
        Yii::$app->view->params['hotcategory']=$allhotcategory;
        
        //copyright block
        $cp=new FooterCopyright();
        $allcp=$cp->find()->one();
        Yii::$app->view->params['footercopyright']=$allcp;
        
        //developer block
        $dblock=new FooterDevelopedblock();
        $developerblock=$dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock']=$developerblock;
        
        //socialicon block
        $socialicon=new SocialIcon();
        $sicon=$socialicon->find()->one();
        Yii::$app->view->params['footersocialicon']=$sicon;
        
        //third block
        $th=new FooterThirdBlock();
        $thirdblock=$th->find()->one();
        Yii::$app->view->params['footerthirdblock']=$thirdblock;
        
        //footer section
         //notification for employee
        $allnotification=array();
		if(isset(Yii::$app->session['Employeeid']))
		{
        $notification=new Notification();
        $allnotification=$notification->find()->where(['UserId'=>Yii::$app->session['Employeeid'],'IsView'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
        }
        Yii::$app->view->params['employeenotification']=$allnotification;
        
        //notification for employer
        $allntemp=array();
        if(isset(Yii::$app->session['Employerid']))
        {
            $empnot=new EmpNotification();
            $allntemp=$empnot->find()->where(['EmpId'=>Yii::$app->session['Employerid'],'IsView'=>0])->orderBy(['OnDate'=>SORT_DESC])->all();
        }
        Yii::$app->view->params['employernotification']=$allntemp;
        //notification end
        
        $this->layout='messagelayout';
        
        if(isset(Yii::$app->session['Employerid']))
        {
            $empid=Yii::$app->session['Employerid'];
        }
        elseif(isset(Yii::$app->session['Employeeid']))
        {
            $empid=Yii::$app->session['Employeeid'];
        }
        elseif(isset(Yii::$app->session['Teamid']))
        {
            $empid=Yii::$app->session['Teamid'];
        }
        elseif(isset(Yii::$app->session['Campusid']))
        {
            $empid=Yii::$app->session['Campusid'];
        }
        $msgg=new Messages();
        $allfriend=$msgg->getAllfriend($empid);
        //$allcandidate=AllUser::find()->where(['AllUser.IsDelete'=>0,'VerifyStatus'=>1,'UserTypeId'=>2])->joinWith(['experiences','skilldetail'])->orderBy(['AllUser.OnDate'=>SORT_DESC])->all();
        $firstfromid=$allfriend[0]['From'];
        $firsttoid=$allfriend[0]['To'];
        Yii::$app->view->params['Messagefrom']=$firstfromid;
        Yii::$app->view->params['Messageto']=$firsttoid;
        
        return $this->render('message',['allfriend'=>$allfriend,'empid'=>$empid]);
    }
    
    public function actionGetmessagelist($empid,$userid)
    {
		if(isset(Yii::$app->session['Employerid']))
		{
			$id=Yii::$app->session['Employerid'];
		}
		elseif(isset(Yii::$app->session['Employeeid']))
		{
			$id=Yii::$app->session['Employeeid'];
		}
		elseif(isset(Yii::$app->session['Teamid']))
		{
			$id=Yii::$app->session['Teamid'];
		}elseif(isset(Yii::$app->session['Campusid']))
		{
			$id=Yii::$app->session['Campusid'];
		}

        if($id==$empid){$coloumn='IsMessageFromClear';}
        elseif($id==$userid){$coloumn='IsMessageToClear';}
        
        //$messagelist=Messages::find()->
        //where(['IsDelete'=>0,$coloumn=>0])
        //->andWhere(['MessageTo'=>$userid,'MessageFrom'=>$empid])
        //->orWhere(['MessageTo'=>$empid,'MessageFrom'=>$userid])
        //->orderBy(['OnDates'=>SORT_ASC])->all();

        $connection = \Yii::$app->db;
        $sql=$connection->createCommand("SELECT * FROM(SELECT * FROM `Messages` WHERE (`MessageFrom`=$userid and `MessageTo`=$empid) or (`MessageTo`=$userid and `MessageFrom`=$empid) and `IsDelete`=0 ) c where c.`ClearBy`!=$id and c.`DelBy`!=$id");
        $messagelist=$sql->queryAll();
        $this->layout='blanklayout';
        
        Messages::updateAll(['IsView' => 1], "MessageTo = $id and MessageFrom=$userid and IsView=0");
        
        $to=AllUser::find()->where(['UserId'=>$userid])->one();
        $from=AllUser::find()->where(['UserId'=>$empid])->one();
		
        return $this->render('messagelist',['messagelist'=>$messagelist,'from'=>$from,'to'=>$to]);
    }
    
    public function actionClearchat($from,$to,$empid)
    {
        $count=Messages::find()->where("MessageFrom = $from AND MessageTo = $to OR MessageFrom = $to AND MessageTo = $from")->all();
        foreach($count as $key=>$value)
        {
            if($value->ClearBy!=0)
            {
                Messages::findOne($value->MessagesId)->delete();
            }
        }
        Messages::updateAll(['ClearBy' => $empid], "MessageFrom = $from AND MessageTo = $to OR MessageFrom = $to AND MessageTo = $from" );
        echo json_encode(1);
    }
    
    public function actionDelchat($from,$to,$empid)
    {
        $count=Messages::find()->where("MessageFrom = $from AND MessageTo = $to OR MessageFrom = $to AND MessageTo = $from")->all();
        foreach($count as $key=>$value)
        {
            if($value->DelBy!=0)
            {
                Messages::findOne($value->MessagesId)->delete();
            }
        }
        
        Messages::updateAll(['DelBy' => $empid], "MessageFrom = $from AND MessageTo = $to OR MessageFrom = $to AND MessageTo = $from" );
        echo json_encode(1);
    }
    
    public function actionMessageadd($Messages,$from,$to)
    {
		
        $messages=json_decode($Messages);
        $me=new Messages();
        $me->MessageTo=$to;
        $me->MessageFrom=$from;
        $me->Message=$messages;
        $me->IsDelete=0;
        $me->OnDate=date('Y-m-d H:i:s');
        $me->save();
        echo json_encode(1);
    }
}
?>