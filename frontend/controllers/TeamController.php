<?php
namespace frontend\controllers;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\AllUser;
use common\models\Industry;
use common\models\UserType;
use yii\helpers\Url;
use common\models\NaukariSpecialization;
use common\models\NaukariQualData;
use common\models\Notification;
use common\models\EmpNotification;
use common\models\Skill;
use common\models\Course;
use common\models\Position;
use common\models\Documents;
use common\models\Education;
use common\models\Experience;
use common\models\ContactUs;
use common\models\PostJob;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use common\models\NewsLetter;
use common\models\AppliedJob;
use common\models\FooterAboutus;
use common\models\FooterContactus;
use common\models\FooterCopyright;
use common\models\FooterDevelopedblock;
use common\models\SocialIcon;
use common\models\FooterThirdBlock;
use common\models\Feedback;
use common\models\PeoplesayBlock;
use common\models\EmployeeSkill;
use common\models\Blog;
use common\models\BlogCategory;
use common\models\BlogComment;
use common\models\JobAlert;
use common\models\States;
use common\models\City;
use common\models\Specialization;
use common\models\CampusCourseMap;
use common\models\CollegePic;
use common\models\CampusPost;
use common\models\CampusSkill;
use common\models\TeamMembers;
use common\models\TeamMembersMinQualification;
use common\models\TeamQualificationSkillMap;
use common\models\Mail;
use common\models\PlanAssign;
use common\models\Plan;
use common\models\PlanRecord;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use common\models\Cities;
use common\models\LanguagesOpts;
use common\models\WhiteCategory;
use common\models\WhiteSkill;
use common\models\WhiteRole;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;


/**
 * Site controller
 */
class TeamController extends Controller
{

    /**
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'signup'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?'
                        ]
                    ],
                    [
                        'actions' => [
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    // find team
    public function actionIndex()
    {
        if (! isset(Yii::$app->session['Teamid'])) {

            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end
            $docmodel1 = new Documents();

            $allcourse = Course::find()->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();
            $allposition = Position::find()->where("IsDelete=0")
                ->orderBy([
                'Position' => 'SORT_ASC'
            ])
                ->all();
            $allindustry = ArrayHelper::map(Industry::find()->select([
                'IndustryId',
                'IndustryName'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all(), 'IndustryId', 'IndustryName');
            $alluser = new AllUser();
            if ($alluser->load(Yii::$app->request->post())) {
				/*echo '<pre>';
				print_r(Yii::$app->request->post());die;*/
                $count = $alluser->find()
                    ->where([
                    'Email' => $alluser->Email,
                    'IsDelete' => 0
                ])
                    ->count();
                if ($count > 0) {
                    Yii::$app->session->setFlash('error', "This Emailid Already Exist.");
                    return $this->redirect([
                        'index'
                    ]);
                } else {

                    $photo_id = 0;
                    if ($docmodel1->load(Yii::$app->request->post())) {
						/*echo '<pre>';
						print_r(Yii::$app->request->post());die;*/
                        /*
                         * $docmodel1->image = \yii\web\UploadedFile::getInstance($docmodel1, 'Doc');
                         * if ($docmodel1->image) {
                         * $image = UploadedFile::getInstance($docmodel1, 'Doc');
                         * $ext = end((explode(".", $image->name)));
                         * $ppp = Yii::$app->security->generateRandomString();
                         * $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";
                         * $docmodel1->Doc = 'imageupload/' . $ppp . ".{$ext}";
                         * $docmodel1->OnDate = date('Y-m-d');
                         * $docmodel1->From = 'photo';
                         * if ($docmodel1->save()) {
                         * $photo_id = $docmodel1->DocId;
                         * $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
                         * $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;
                         * if (file_exists($thumb)) {
                         * $document = Documents::find()->where([
                         * 'DocId' => $photo_id
                         * ])->one();
                         * $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;
                         * $document->save();
                         * }
                         * } else {
                         * $photo_id = 0;
                         * }
                         * }
                         */
                    }
                    // echo $photo_id;
                    // die();
                    // other course
                    if (isset(Yii::$app->request->post()['AllUser']['OtherCourse']) && Yii::$app->request->post()['AllUser']['OtherCourse'] != '') {
                        $ncourse = new Course();
                        $ncourse->CourseName = Yii::$app->request->post()['AllUser']['OtherCourse'];
                        $ncourse->OnDate = date('Y-m-d');
                        $ncourse->save();
                        $courseId = $ncourse->CourseId;
                    } else {
                        $courseId = Yii::$app->request->post()['AllUser']['CourseId'];
                    }

                    $alluser->UserTypeId = 5;
                    $alluser->EntryType = 'Team';
                    $alluser->Password = md5(Yii::$app->request->post()['AllUser']['Password']);
                    $alluser->PhotoId = Documents::handleImageUpload($docmodel1, 'Doc', 'PhotoId');
                    $alluser->Ondate = date('Y-m-d');
                    $vkey = 'CB' . time();
                    $alluser->VerifyKey = $vkey;
                    $alluser->VerifyStatus = 1;
                    $alluser->language = 'English';
                    $alluser->CollarType = 'white';
					$alluser->CVId = Documents::handleImageUpload($docmodel1, 'cvFile', 'cv');
                    $alluser->Salary = Yii::$app->request->post()['AllUser']['MySalary'];
                    $alluser->whatsappno = '91' . Yii::$app->request->post()['AllUser']['WhatsAppNo'];
                    //reset(Yii::$app->request->post()['AllUser']['CompanyName']);
                    //$expKey = key(Yii::$app->request->post()['AllUser']['CompanyName']);
                    //$alluser->IsExp = (!empty(Yii::$app->request->post()['Experience'])) ? 1 : 0;
                    if (! $alluser->save()) {
                        echo "<pre>";
                        print_r($alluser->getErrors());
                        die();
                    }
                    // var_dump($alluser->getErrors());
                    // new skill
                    $skillidlist = '';
                    // $skilllist=trim(trim(Yii::$app->request->post()['AllUser']['RawSkill']),",");

                    $post = Yii::$app->request->post();
                    // $rawskill = Yii::$app->request->post()['AllUser']['SkillId'];
                    /*$rawskill = $post['AllUser']['EducationSkillId'];
                    EmployeeSkill::saveSkillListByType($rawskill, EmployeeSkill::TYPE_EDUCATION, $alluser);*/

                    //$rawskill = $post['AllUser']['ExperienceSkillId'];
                    //EmployeeSkill::saveSkillListByType($rawskill, EmployeeSkill::TYPE_EXPERIENCE, $alluser);

                    /*
                     * $skills_ary = array();
                     * if (empty($rawskill)) {
                     * $rawskill = array();
                     * } else {
                     * $skill_s = new Skill();
                     * $skills_res = $skill_s->find()
                     * ->where([
                     * 'SkillId' => $rawskill
                     * ])
                     * ->all();
                     * if (! empty($skills_res)) {
                     * foreach ($skills_res as $keys => $values) {
                     * $skills_ary[$values->SkillId] = $values->PositionId;
                     * }
                     * }
                     * }
                     * foreach ($rawskill as $rk => $skid) {
                     * if ($skid != '') {
                     * $skillidlist .= $skid;
                     * $empskill = new EmployeeSkill();
                     * if (isset($skills_ary[$skid])) {
                     * $skillid_s = $skills_ary[$skid];
                     * } else {
                     * $skillid_s = 0;
                     * }
                     * $empskill->SkillRoleId = $skillid_s;
                     * $empskill->SkillId = $skid;
                     * $empskill->UserId = $alluser->UserId;
                     * $empskill->OnDate = date('Y-m-d');
                     * $empskill->save(false);
                     * // var_dump($empskill->getErrors());
                     * }
                     * }
                     */
                    if (Yii::$app->request->post()['AllUser']['EducationSkillId'] == '') {
                        $skil = $skillidlist;
                    } else {
                        $skil = is_array(Yii::$app->request->post()['AllUser']['EducationSkillId']) ? implode(',', Yii::$app->request->post()['AllUser']['EducationSkillId']) : '';
                    }

                    $education = new Education();
                    $education->specialization_id = Yii::$app->request->post()['AllUser']['Specialization'];
                    if (! empty(Yii::$app->request->post()['AllUser']['board_id'])) {
                        $education->board_id = Yii::$app->request->post()['AllUser']['board_id'];
                    }					
					
					if (! empty(Yii::$app->request->post()['AllUser']['BoardId'])) {
                        $education->board_id = Yii::$app->request->post()['AllUser']['BoardId'];
                    }
                    if (! empty(Yii::$app->request->post()['AllUser']['HighestQualId'])) {
                        $education->highest_qual_id = Yii::$app->request->post()['AllUser']['HighestQualId'];
                    }

                    if (! empty(Yii::$app->request->post()['AllUser']['CourseId'])) {
                        $education->course_id = Yii::$app->request->post()['AllUser']['CourseId'];
                    }

					if (! empty(Yii::$app->request->post()['AllUser']['University'])) {
                        $education->University = Yii::$app->request->post()['AllUser']['University'];
                    }

                    $education->UserId = $alluser->UserId;
                    $education->HighestQualification = 'none';
                    $education->CourseId = 1;
                    $education->University = 'none';
                    $education->University=Yii::$app->request->post()['AllUser']['University'];
                    $education->PassingYear = Yii::$app->request->post()['AllUser']['DurationTo'];
                    $education->SkillId = 1;
                    $education->DurationFrom = '';
                    $education->DurationTo = Yii::$app->request->post()['AllUser']['DurationTo'];
                    $education->OnDate = date('Y-m-d');
                    $education->save();

                    // var_dump($education->getErrors());die();

                    // other position
                    /*
                     * $positionId = '';
                     * if (isset(Yii::$app->request->post()['AllUser']['OtherPosition']) && Yii::$app->request->post()['AllUser']['OtherPosition'] != '') {
                     * $nposition = new Position();
                     * $nposition->Position = Yii::$app->request->post()['AllUser']['OtherPosition'];
                     * $nposition->OnDate = date('Y-m-d');
                     * $nposition->save();
                     * $positionId = $nposition->PositionId;
                     * } else {
                     * $positionId = Yii::$app->request->post()['AllUser']['PositionId'];
                     * }
                     */
                    $IsExp = 0;

                    /*
                     * echo "<pre>";
                     * print_r(Yii::$app->request->post()['AllUser']);
                     * die();
                     */
					 
					 /*Save White Collar Job Category, Skills, Role*/
					if(!empty($post['AllUser']['Category'])){
						foreach($post['AllUser']['Category'] as $data){
							if(!empty($data['Category'])){
								$whiteCategory = WhiteCategory::find()->where([
													'id' => $data['Category']
												])->one();
								if(!empty($whiteCategory)){
									$userJobCate = new UserJobCategory();
									$userJobCate->user_id = $alluser->UserId;
									$userJobCate->category_id = $data['Category'];
									$userJobCate->category = $whiteCategory->name;
									$userJobCate->on_date = date('Y-m-d H:i:s');
									if(!$userJobCate->save()){
										echo "<pre>Job Category : ";
                                        print_r($userJobCate->getErrors());
                                        die();
									}
									/*Save White Role*/
									if(!empty($data['WhiteRole'])){
										foreach($data['WhiteRole'] as $whiteRoleId){
											if(!empty($whiteRoleId)){
												$whiteRoleData = WhiteRole::find()->where([
													'id' => $whiteRoleId
												])->one();
												if(!empty($whiteRoleData)){
													$userWhiteRole = new UserWhiteRole();
													$userWhiteRole->user_id = $alluser->UserId;
													$userWhiteRole->category_id = $data['Category'];
													$userWhiteRole->role_id = $whiteRoleId;
													$userWhiteRole->role = $whiteRoleData->name;
													if(!$userWhiteRole->save()){
														echo "<pre>Role : ";
														print_r($userWhiteRole->getErrors());
														die();
													}
												}	
											}
										}
									}
									
								}
							}
						}
					}
					
					/*Save White Collar Skills*/
					if(!empty($post['AllUser']['WhiteSkills'])){
						foreach($post['AllUser']['WhiteSkills'] as $skillId){
							if(!empty($skillId)){
								$whiteSkillRec = WhiteSkill::find()->where([
												'id' => $skillId
											])->one();
								if(!empty($whiteSkillRec)){
									$userWSkill = new UserWhiteSkill();
									$userWSkill->user_id = $alluser->UserId;
									$userWSkill->skill_id = $skillId;
									$userWSkill->skill = $whiteSkillRec->name;
									$userWSkill->on_date = date('Y-m-d');
									if(!$userWSkill->save()){
										echo "<pre>Skills : ";
										print_r($userWSkill->getErrors());
										die();
									}	
								}
							}
						}
					}
					 
					 
					 
					 
					 if (!empty($post['Experience'])) {
						/*Update New Code*/
						foreach($post['Experience'] as $experienceData){
							if(!empty($experienceData['YearFrom'])){
								$experienceModel = new Experience();
								$experienceModel->UserId = $alluser->UserId;
								$experienceModel->CompanyName = $experienceData['CompanyName'];
								$experienceModel->PositionId = (isset($experienceData['PositionId']) && $experienceData['PositionId']) ? $experienceData['PositionId'] : 1;
								$experienceModel->IndustryId = (isset($experienceData['CIndustryId']) && $experienceData['CIndustryId']) ? $experienceData['CIndustryId'] : 0;
								$experienceModel->PositionName = $experienceData['PositionName'];
								$experienceModel->YearFrom = $experienceData['YearFrom'];
								$experienceModel->YearTo = (isset($experienceData['YearTo']) && $experienceData['YearTo'] != '') ? $experienceData['YearTo'] : 'Till Now';
								if(isset($experienceData['CurrentWorking']) && !empty($experienceData['CurrentWorking'])){
									$experienceModel->YearTo = 'Till Now';
									$experienceModel->CurrentWorking = 1;
								}
								$experienceModel->Experience = $experienceData['Experience'];
								$experienceModel->Salary = $experienceData['Salary'];
								$experienceModel->skills = 1; // implode(',', $experience->skills[$key]);
								$experienceModel->OnDate = date('Y-m-d');
								$experienceModel->CategoryId = $experienceData['Category'];
								
								if ($experienceModel->save()) {
									$IsExp = 1;
										/*if(isset($experienceData['skills'][$experienceData['PositionId']]) && !empty($experienceData['skills'][$experienceData['PositionId']])){
											$skillData = $experienceData['skills'][$experienceData['PositionId']];
											foreach($skillData as $skill){
												$employeeSkill = new EmployeeSkill();
                                                $employeeSkill->UserId = $alluser->UserId;
                                                $employeeSkill->SkillRoleId = $experienceData['PositionId'];
                                                $employeeSkill->SkillId = $skill;
                                                $employeeSkill->OnDate = date('Y-m-d');
                                                $employeeSkill->TypeId = EmployeeSkill::TYPE_EXPERIENCE;
                                                $employeeSkill->ExperienceId = $experienceModel->ExperienceId;
                                                if (! $employeeSkill->save()) {
                                                    echo "<pre>Employee Skill : ";
                                                    print_r($employeeSkill->getErrors());
                                                    die();
                                                }
											}
										}*/
										
										if(!empty($experienceData['Category'])){
											/*Save Roles*/
											if(!empty($experienceData['WhiteRole'])){
												foreach($experienceData['WhiteRole'] as $wRoleId){
													$whiteRole = WhiteRole::find()->where([
															'id' => $wRoleId
														])->one();
													if(!empty($whiteRole)){
														$expWhiteRole = new ExperienceWhiteRole();
														$expWhiteRole->user_id = $alluser->UserId;
														$expWhiteRole->experience_id = $experienceModel->ExperienceId;
														$expWhiteRole->category_id = $experienceData['Category'];
														$expWhiteRole->role_id = $wRoleId;
														$expWhiteRole->role = $whiteRole->name;
														if(!$expWhiteRole->save()){
															echo "<pre>Employee Skill : ";
															print_r($expWhiteRole->getErrors());
															die();
														}
													}
												}
											}
										}
										
										/*Save Skills*/
										if(!empty($experienceData['WhiteSkills'])){
											foreach($experienceData['WhiteSkills'] as $wSkillId){
												$whiteSkill = WhiteSkill::find()->where([
														'id' => $wSkillId
													])->one();
												if(!empty($whiteSkill)){
													$expWhiteSkill = new ExperienceWhiteSkill();
													$expWhiteSkill->user_id = $alluser->UserId;
													$expWhiteSkill->experience_id = $experienceModel->ExperienceId;
													$expWhiteSkill->skill_id = $wSkillId;
													$expWhiteSkill->skill = $whiteSkill->name;
													$expWhiteSkill->save();
												}
											}
										}
									}else{
										echo "<pre>Employee Skill : ";
										print_r($experienceModel->getErrors());
										die();
									}
							}
							
						}
						
						if ($IsExp == 1) {

							$uuser = AllUser::find()->where([
								'UserId' => $alluser->UserId
							])->one();

							$uuser->IsExp = $IsExp;

							$uuser->save(false);
						}
							
						}
					 
                    /*foreach (Yii::$app->request->post()['AllUser']['CompanyName'] as $key => $vv) {
                        if (Yii::$app->request->post()['AllUser']['CompanyName'][$key] != '') {
                            $IsExp = 1;
                            // other position
                            $positionId = 0;
                            if (isset(Yii::$app->request->post()['AllUser']['OtherPosition'][$key]) && Yii::$app->request->post()['AllUser']['OtherPosition'][$key] != '') {
                                $nposition = new Position();
                                $nposition->Position = Yii::$app->request->post()['AllUser']['OtherPosition'][$key];
                                $nposition->OnDate = date('Y-m-d');
                                $nposition->save(false);
                                $positionId = $nposition->PositionId;
                            } else {
                                $positionId = Yii::$app->request->post()['AllUser']['PositionId'][$key];
                            }
                            $experience = new Experience();
                            $experience->UserId = $alluser->UserId;
                            $experience->CompanyName = Yii::$app->request->post()['AllUser']['CompanyName'][$key];
                            $experience->PositionId = $positionId;
                            $experience->PositionName = Yii::$app->request->post()['Experience']['PositionName'][$key];
                            $experience->IndustryId = Yii::$app->request->post()['AllUser']['CIndustryId'][$key];
                            $experience->YearFrom = Yii::$app->request->post()['AllUser']['YearFrom'][$key];
                            $experience->YearTo = Yii::$app->request->post()['AllUser']['YearTo'][$key];
							
							if(isset(Yii::$app->request->post()['AllUser']['CurrentWorking'][$key]) && !empty(Yii::$app->request->post()['AllUser']['CurrentWorking'][$key])){
								$experience->CurrentWorking = 1;
							}
							
                            if (! Yii::$app->request->post()['AllUser']['YearTo'][$key]) {
                                $experience->YearTo = 'Till Now';
                            }
                            $experience->Experience = Yii::$app->request->post()['AllUser']['Experience'][$key];
                            $experience->Salary = Yii::$app->request->post()['AllUser']['Salary'][$key];
                            if (! empty(Yii::$app->request->post()['AllUser']['SkillsId'])) {
                                $experience->skills = implode(',', Yii::$app->request->post()['AllUser']['SkillsId']);
                            }
                            $experience->OnDate = date('Y-m-d');
                            $experience->save(false);
							
							
							
                        }
                    }*/
                    /*
                     * $experience = new Experience();
                     * $experience->UserId = $alluser->UserId;
                     * $experience->CompanyName = Yii::$app->request->post()['AllUser']['CompanyName'];
                     * $experience->PositionId = $positionId;
                     * $experience->IndustryId = Yii::$app->request->post()['AllUser']['CIndustryId'];
                     * $experience->YearFrom = Yii::$app->request->post()['AllUser']['YearFrom'];
                     * $experience->YearTo = Yii::$app->request->post()['AllUser']['YearTo'];
                     * $experience->Experience = Yii::$app->request->post()['AllUser']['Experience'];
                     * $experience->Salary = Yii::$app->request->post()['AllUser']['Salary'];
                     * $experience->OnDate = date('Y-m-d');
                     * $experience->save();
                     */

                    // team member
                    $Teamphoto = UploadedFile::getInstances($alluser, 'TeamMemberPhoto');
                    foreach (Yii::$app->request->post()['AllUser']['TeamName'] as $fk => $value) {
                        if (Yii::$app->request->post()['AllUser']['TeamName'][$fk] != '') {

                            if (! empty(Yii::$app->request->post()['AllUser']['TeamName'][$fk])) {
                                $docmodel2 = new Documents();
                                $memberpic_id = $docmodel2->imageUpload($Teamphoto[$fk], 'TeamMemberPhoto');
                            } else {
                                $memberpic_id = 0;
                            }

                            $teammembers = new TeamMembers();
                            $teammembers->MemberName = Yii::$app->request->post()['AllUser']['TeamName'][$fk];
                            $teammembers->MemberQualification = Yii::$app->request->post()['AllUser']['MemberQualification'][$fk];
                            $teammembers->MemberCourse = Yii::$app->request->post()['AllUser']['MemberCourse'][$fk];
                            $teammembers->MemberSalary = Yii::$app->request->post()['AllUser']['MemberSalary'][$fk];
                            $teammembers->MemberPhoto = $memberpic_id ? $memberpic_id : 0;
                            $teammembers->TeamId = $alluser->UserId;
                            $teammembers->IsDelete = 0;
                            $teammembers->OnDate = date('Y-m-d');
                            $teammembers->save();
                        }
                    }
                    // die();
                    // team member minimum skill
                    if (isset(Yii::$app->request->post()['AllUser']['TeamOtherCourse']) && Yii::$app->request->post()['AllUser']['TeamOtherCourse'] != '') {
                        $ncourse = new Course();
                        $ncourse->CourseName = Yii::$app->request->post()['AllUser']['TeamOtherCourse'];
                        $ncourse->OnDate = date('Y-m-d');
                        $ncourse->save();
                        $tcourseId = $ncourse->CourseId;
                    } else {
                        $tcourseId = Yii::$app->request->post()['AllUser']['TeamCourseId'];
                    }
                    $TeamMembersMinQualification = new TeamMembersMinQualification();
                    $TeamMembersMinQualification->MinQualification = Yii::$app->request->post()['AllUser']['MinQualification'];
                    $TeamMembersMinQualification->CourseId = $tcourseId;
                    $TeamMembersMinQualification->TeamId = $alluser->UserId;
                    $TeamMembersMinQualification->OnDate = date('Y-m-d');
                    $TeamMembersMinQualification->save();
                    // var_dump($TeamMembersMinQualification->getErrors());

                    // team member skill
                    $skillidlist = '';
                    $skilllist = trim(trim(Yii::$app->request->post()['AllUser']['TeamRawSkill']), ",");
                    $rawskill = explode(",", $skilllist);
                    foreach ($rawskill as $rk => $rval) {
                        if ($rval != '') {
                            $indskill = trim($rval);
                            $nskill = new Skill();
                            $cskill = $nskill->find()
                                ->where([
                                'Skill' => $indskill,
                                'IsDelete' => 0
                            ])
                                ->one();
                            if (! $cskill) {
                                $nskill->Skill = $indskill;
                                $nskill->OnDate = date('Y-m-d');
                                $nskill->save();
                                $skid = $nskill->SkillId;
                            } else {
                                $skid = $cskill->SkillId;
                            }
                            $skillidlist .= $skid;
                            $TeamQualificationSkillMap = new TeamQualificationSkillMap();
                            $TeamQualificationSkillMap->SkillId = $skid;
                            $TeamQualificationSkillMap->TeamId = $alluser->UserId;
                            $TeamQualificationSkillMap->MinQId = $TeamMembersMinQualification->MinId;
                            $TeamQualificationSkillMap->OnDate = date('Y-m-d');
                            $TeamQualificationSkillMap->save();
                            // var_dump($TeamQualificationSkillMap->getErrors());
                        }
                    }
                    $name = $alluser->Name;
                    $to = $alluser->Email;
                    $from = Yii::$app->params['adminEmail'];
                    $subject = "Registration Success";

                    $html = "<html>
               <head>
               <title>Registration Success</title>
               </head>
               <body>
               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>
                <tbody><tr>
                                       <td><img src='http://mycareerbugs.com/images/logo.png' title='Career Bugs' alt='Career Bugs' style='margin-top:10px;width:200px;'></td>
                                   </tr>
                                   <tr>
                                       <td style='height:30px'></td>
                                   </tr>
                           <tr>
                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://mycareerbugs.com' target='_blank'> Career Bugs </a>
               <br><br>
               <span style='font-size:16px;line-height:1.5'>
                 <h3> Dear  $name, </h3>
                    Congratulations! You have been registered successfully on Careerbug!!
               <br/>
               </span>
               </h2>
               </td>
               </tr>
               </tbody>
               </table>
               </body>
               </html>";
                    $mail = new ContactForm();
                    $mail->sendEmail($to, $from, $html, $subject);

                    $session = Yii::$app->session;
                    $session->open();
                    Yii::$app->session['Teamid'] = $alluser->UserId;
                    Yii::$app->session['TeamName'] = $alluser->Name;
                    Yii::$app->session['TeamEmail'] = $alluser->Email;

                    if (isset($alluser->photo->Doc)) {
                        $pimage = $url . $alluser->photo->Doc;
                        $img = explode(".", $pimage);
                        $ext = $img[1];
                        $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
                        $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;
                        if (file_exists($thumb)) {
                            $document = Documents::find()->where([
                                'DocId' => $photo_id
                            ])->one();
                            $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;
                            $document->save();
                        }
                        /*
                         * $document = Documents::find()->where([
                         * 'DocId' => $photo_id
                         * ])->one();
                         * $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;
                         * $document->save();
                         */
                    }
                    if ($alluser->PhotoId != 0) {
                        $url = '/backend/web/';
                        $pimage = $url . $alluser->photo->Doc;
                    } else {
                        $pimage = 'images/user.png';
                    }
                    Yii::$app->session['TeamDP'] = $pimage;

                    // Yii::$app->session->setFlash('success', "Account Created Successfully");
                    return $this->redirect([
                        'site/thankyou'
                    ]);
                }
            } else {
                $naukari_specialization = NaukariSpecialization::find()->all();
                $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();
                $teammembers = new TeamMembers();
				$whiteCategoryList =  ArrayHelper::map(WhiteCategory::find()->where([
						'status' => 1
					])
						->orderBy([
						'name' => 'SORT_ASC'
					])
					->all(), 'id', 'name');
                return $this->render('register', [
                    'course' => $allcourse,
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'position' => $allposition,
                    'industry' => $allindustry,
                    'docmodel' => $docmodel1,
                    'teammembers' => $teammembers,
					'whiteCategories' => $whiteCategoryList
                ]);
            }
        } else {
            return $this->redirect([
                'site/index'
            ]);
        }
    }

    // find team
    public function actionFindteam()
    {
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;
        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $state = '';
        $jobtype = '';
        $industry = '';
        $skill = '';
        $salary = '';
        $education = '';
        $latest = '';
        $gender = '';
        $cresume = '';
        $role = '';
        $exp = '';

        if (isset(Yii::$app->session['Employerid'])) {

            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();

            $totalemail = $planassign->TotalEmail - $planassign->UseEmail;
        }

        $mailmodel = new Mail();

        if ($mailmodel->load(Yii::$app->request->post())) {

            $mailmodel->Type = 'Mail';

            $jobdet = PostJob::find()->where([
                'JobId' => $mailmodel->JobId
            ])->one();

            if ($jobdet->IsExp == 0) {

                $experience = $jobdet->Experience;
            } else if ($jobdet->IsExp == 1) {

                $experience = $jobdet->Experience . ' Years';
            } else if ($jobdet->IsExp == 2) {

                $experience = 'Both Fresher and Experience';
            }

            $location = $jobdet->Location;

            $mailmodel->Experience = $experience;

            $mailmodel->Location = $location;

            if ($mailmodel->save(false)) {

                $sender = AllUser::find()->where([
                    'UserId' => Yii::$app->session['Employerid']
                ])->one();

                $mailtext = $mailmodel->MailText;

                $email = trim($mailmodel->EmployeeId, "|");

                $to = explode("|", $email);

                $to_mail_userid = explode("|", trim(Yii::$app->request->post()['Mail']['UserId'], "|"));

                $subject = $mailmodel->Subject;

                $senderemail = $sender->Email;

                $sendername = $sender->Name;

                $jobid = $mailmodel->JobId;

                $mailgo = ($totalemail > count($to)) ? count($to) : count($to) - $totalemail;

                $m = 0;

                if ($mailgo > 0) {

                    if (! empty($to_mail_userid)) {

                        $j = 0;

                        foreach ($to_mail_userid as $key => $value1) {

                            $planrecord_cnt = PlanRecord::find()->where([

                                'EmployerId' => Yii::$app->session['Employerid'],

                                'UserId' => $value1,

                                'Type' => 'Email',

                                'PlanId' => $planassign->PlanId
                            ])->count();

                            /*if ($planrecord_cnt == 0) {
                                
                            }*/
							$planrecord = new PlanRecord();
							$planrecord->PlanId = $planassign->PlanId;
							$planrecord->EmployerId = $planassign->EmployerId;
							$planrecord->Type = 'Email';
							$planrecord->UserId = $value1;
							$planrecord->Amount = 1;
							$planrecord->UpdatedDate = date('Y-m-d H:i:s');
							$planrecord->save(false);
							$j ++;
                        }
                        $planassign->UseEmail = $planassign->UseEmail + $j;
                        $planassign->save(false);
                    }

                    foreach ($to as $key => $value) {
                        $m ++;
                        if ($m <= $mailgo) {
                            $receiver = AllUser::find()->select([
                                'Name'
                            ])
                                ->Where([
                                'Email' => $value
                            ])
                                ->one();
                            $receivername = $receiver->Name;
                            $html = "<html>
                            
                    <head>
                    
                    <title>$subject</title>
                    
                    <style>
                    
                    .im {
                    
                    color:#4d4c4c!important;
                    
                    }
                    
                    </style>
                    
                    </head>
                    
                    <body>
                    
                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;line-height: 1.6;'>
                    
                     <tbody><tr>
                     
                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='Careerbug' alt='Careerbug' style='margin-top:10px;width:200px;'></td>
                                            
                                        </tr>
                                        
                                        
                                        
                    <tr>
                    
                                            <td><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:3%;margin:auto;color: #333;text-align: left;font-size:12px'>
                                            
                            The sender of this email is registered with mycareerbugs.com as $sendername. To respond back directly to the Employer, please click on Reply button, or send an email to $senderemail
                            
                    </h2>
                    
                    </td>
                    
                    </tr>
                    
                    
                    
                    <tr>
                    
                    <td>
                    
                    <table style='width:91%;height:auto;color:#4d4c4c;background:#fff;text-align:center;text-align: left;padding: 4%;padding-top: 0%;font-family: arial;font-size: 13px;margin-left: 4.5%;line-height: 1.8;'>
                    
                   <tr>
                   
                    <td><b>Experience required for the Job:</b> $experience </td>
                    
                   </tr>
                   
                   <tr>
                   
                    <td><b>Job Location:</b> $location  <br/><br/></td>
                    
                   </tr>
                   
                   <tr>
                   
                    <td>
                    
                    <a href='https://mycareerbugs.com/site/jobdetail?JobId=$jobid'><input type='button' value='Apply Now' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;border: none;'/></a>
                    
                    <a href='mailto:$senderemail'><input type='button' value='Reply' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;margin-left: 20px;border: none;'/></a>
                    
                    </td>
                    
                   </tr>
                   
                   <tr>
                   
                    <td><br/> <b>   Dear $receivername,</b> <br/><br/></td>
                    
                   </tr>
                   
                   <tr>
                   
                    <td>
                    
                    $mailtext
                    
                    </td>
                    
                   </tr>
                   
                    </table>
                    
                    </td>
                    
                    </tr>
                    
                    <tr>
                    
                    <td width='275' bgcolor='#f4f8fe' style='border:1px solid #e7f0fe;font:bold 13px Arial,Helvetica,sans-serif;color:#5d6f8a;padding:10px;margin:0 auto'>Is this job relevant to you? <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;margin:0 5px;font-weight:bold' href='https://www.mycareerbugs.com/site/jobrelevant?type=yes&email=$value&JobId=$jobid'>Yes</a> <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;font-weight:bold' href='https://www.mycareerbugs.com/site/jobrelevant?type=no&email=$value&JobId=$jobid'>No</a></td>
                    
                   </tr>
                   
                    <tr>
                    
                    <td>
                    
                    <p style='font-size: 11px;color: #333;'>Your feedback would help us in sending you the most relevant job opportunities</p>
                    
                    </td>
                    
                    </tr>
                    
                    <tr>
                    
                                        <td style='border-bottom: 1px solid #ccc;'></td>
                                        
                    </tr>
                    
                    <td>
                    
                    <p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'><b>Disclaimer:</b></p>
                    
                    </td>
                    
                    </tr>
                    
                    <tr>
                    
                                        <td style='border-bottom: 1px solid #333;'></td>
                                        
                    </tr>
                    
                    <tr>
                    
                    <td><p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'>
                    
                    The sender of this email is registered with <a href='https://www.mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> as Cygnet Infotech Pvt. Ltd (rushad.shah@cygnetinfotech.com, 2 Manikyam Opp Samudra Annexes Off. C.G.Road,, AHMEDABAD, Gujarat - 380009) using <a href='https://www.mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> services. The responsibility of checking the authenticity of offers/correspondence lies with you. If you consider the content of this email inappropriate or spam, you may: Forward this email to: compliance@mycareerbugs.com. <br/>
                    
<b>Please Note:</b> This mail is a private message from the recruiter. We have enabled auto login for your convenience, you are strongly advised not to forward this email to protect your account from unauthorized access.<br/>

<b>Advisory:</b> Please do not pay any money to anyone who promises to find you a job. This could be in the form of a registration fee or document processing fee or visa charges or any other pretext. The money could be asked for upfront or it could be asked after trust has been built after some correspondence has been exchanged. Also please note that in case you get a job offer or a letter of intent without having been through an interview process it is probably a scam and you should contact compliance@mycareerbugs.com for advise.

                    </p></td>
                    
                    </tr>
                    
                     </tbody>
                     
                    </table>
                    
                    </body>
                    
                    </html>";

                            $mail = new ContactForm();

                            $mail->sendEmail($value, $senderemail, $html, $subject);
                        }
                    }

                    Yii::$app->session->setFlash('success', "Mail sent Successfully");
                } else {

                    Yii::$app->session->setFlash('error', "Your Data plan is already Used");
                }
            } else {

                // var_dump($mailmodel->getErrors());

                Yii::$app->session->setFlash('error', "There is some error please try again");
            }

            return $this->redirect([
                'findteam'
            ]);
        } else {

            $alluser = new AllUser();
            $keyname = '';
            $indexlocation = '';
            $experience = '';
            $category = '';
            if (isset(Yii::$app->request->post()['candidatesearch'])) {
                $keyname = Yii::$app->request->post()['keysearch'];
                $indexlocation = Yii::$app->request->post()['location'];
                $experience = Yii::$app->request->post()['experience'];
                $category = Yii::$app->request->post()['category'];
                $allcandidate = $alluser->find()
                    ->where([
                    'AllUser.IsDelete' => 0,
                    'VerifyStatus' => 1,
                    'UserTypeId' => 5
                ])
                    ->joinWith([
                    'experiences',
                    'experiences.industry',
                    'educations',
                    'teamRelatedSkills'
                ])
                    ->andFilterWhere([
                    'or',

                    [
                        'like',
                        'Industry.IndustryName',
                        $keyname
                    ],
                    [
                        'like',
                        'AllUser.Name',
                        $keyname
                    ],
                    [
                        'like',
                        'Education.HighestQualification',
                        $keyname
                    ]
                ])
                    ->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,State)'))
                    ->andFilterWhere([
                    // 'State' => $indexlocation,
                    'Experience.Experience' => $experience,
                    'Experience.PositionId' => $category
                ])
                    ->addParams([
                    ':cat_to_find' => $indexlocation
                ])
                    ->groupBy([
                    'AllUser.UserId'
                ])
                    ->orderBy([
                    'AllUser.OnDate' => SORT_DESC
                ]);
                    
//                     echo "<pre>";
//                     print_r($allcandidate->createCommand()->rawSql);
//                     die();
                    
            } elseif (isset(Yii::$app->request->get()['stype'])) {

                // fresher search

                if (Yii::$app->request->get()['stype'] == 'fresher') {

                    if (isset(Yii::$app->request->get()['state'])) {

                        $state = Yii::$app->request->get()['state'];
                    }

                    if (isset(Yii::$app->request->get()['education'])) {

                        $education = Yii::$app->request->get()['education'];
                    }

                    if (isset(Yii::$app->request->get()['specialization'])) {

                        $naukari_specialization = Yii::$app->request->get()['specialization'];
                    }

                    if (isset(Yii::$app->request->get()['course'])) {

                        $naukari_course = Yii::$app->request->get()['course'];
                    }

                    if (isset(Yii::$app->request->get()['latest'])) {

                        $latest = Yii::$app->request->get()['latest'];
                    }

                    if (isset(Yii::$app->request->get()['gender']) && Yii::$app->request->get()['gender'] != '') {

                        $gender = explode(",", Yii::$app->request->get()['gender']);
                    }

                    if (isset(Yii::$app->request->get()['cresume']) && Yii::$app->request->get()['cresume'] != '') {

                        $cresume = Yii::$app->request->get()['cresume'];
                    }

                    if (isset(Yii::$app->request->get()['language']) && Yii::$app->request->get()['language'] != '') {

                        $language = Yii::$app->request->get()['language'];

                        $language_ary = explode(',', $language);
                    }

                    $ddate = date('Y-m-d', strtotime("-$latest days"));

                    if ($cresume == 1) {

                        $allcandidate = $alluser->find()
                            ->where([
                            'AllUser.IsDelete' => 0,
                            'VerifyStatus' => 1,
                            'UserTypeId' => 5,
                            'IsExp' => 0
                        ])
                            ->joinWith([
                            'educations',
                            'skilldetail',
                            'experiences',
                            'teamRelatedSkills'
                        ])
                            ->andFilterWhere([

                            'State' => $state,

                            'Gender' => $gender
                        ])
                            ->
                        // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                        andWhere([
                            '!=',
                            'CVId',
                            0
                        ])
                            ->groupBy([
                            'AllUser.UserId'
                        ])
                            ->orderBy([
                            'AllUser.OnDate' => SORT_DESC
                        ]);
                    } else {

                        $allcandidate = $alluser->find()
                            ->where([
                            'AllUser.IsDelete' => 0,
                            'VerifyStatus' => 1,
                            'UserTypeId' => 5,
                            'IsExp' => 0
                        ])
                            ->joinWith([
                            'educations',
                            'skilldetail',
                            'experiences',
                            'teamRelatedSkills'
                        ])
                            ->andFilterWhere([

                            'State' => $state,

                            'Gender' => $gender
                        ])
                            ->
                        // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                        groupBy([
                            'AllUser.UserId'
                        ])
                            ->orderBy([
                            'AllUser.OnDate' => SORT_DESC
                        ]);
                    }

                    if (! empty($education)) {

                        $allcandidate->andWhere([
                            'Education.highest_qual_id' => $education
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['latest']) && ! empty(Yii::$app->request->get()['latest'])) {

                        $allcandidate->andFilterWhere([
                            '>=',
                            'DATE( AllUser.UpdatedDate )',
                            $ddate
                        ]);
                    }

                    if (isset($naukari_specialization) && ! empty($naukari_specialization)) {

                        $allcandidate->andFilterWhere([
                            'Education.specialization_id' => $naukari_specialization
                        ]);
                    }

                    if (isset($naukari_course) && ! empty($naukari_course)) {

                        $allcandidate->andFilterWhere([
                            'Education.course_id' => $naukari_course
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['skill']) && Yii::$app->request->get()['skill'] != '') {

                        $skill = explode(",", Yii::$app->request->get()['skill']);

                        $allcandidate->andFilterWhere([
                            'EmployeeSkill.SkillId' => $skill
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['role']) && Yii::$app->request->get()['role'] != '') {

                        $role = Yii::$app->request->get()['role'];

                        $allcandidate->andFilterWhere([
                            'EmployeeSkill.SkillRoleId' => $role
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'Y') {

                        $allcandidate->andWhere([
                            '!=',
                            'AllUser.whatsappno',
                            " "
                        ]);
                    }
					
					if (isset(Yii::$app->request->get()['state']) && Yii::$app->request->get()['state'] != '') {

                        $allcandidate->andFilterWhere([
                            'AllUser.State' => Yii::$app->request->get()['state']
                        ]);
                    }
					
					if (isset(Yii::$app->request->get()['city']) && Yii::$app->request->get()['city'] != '') {

                        $allcandidate->andFilterWhere([
                            'AllUser.City' => Yii::$app->request->get()['city']
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'N') {

                        $allcandidate->andWhere([
                            '=',
                            'AllUser.whatsappno',
                            " "
                        ]);
                    }
                } // fresher search

                // experience search

                else {

                    if (isset(Yii::$app->request->get()['state'])) {
                        $state = Yii::$app->request->get()['state'];
                    }

                    if (isset(Yii::$app->request->get()['education'])) {
                        $education = Yii::$app->request->get()['education'];
                    }

                    if (isset(Yii::$app->request->get()['latest'])) {
                        $latest = Yii::$app->request->get()['latest'];
                    }

                    if (isset(Yii::$app->request->get()['gender']) && Yii::$app->request->get()['gender'] != '') {
                        $gender = explode(",", Yii::$app->request->get()['gender']);
                    }

                    if (isset(Yii::$app->request->get()['salaryrange']) && Yii::$app->request->get()['salaryrange'] != '') {

                        $salary = explode(",", Yii::$app->request->get()['salaryrange']);
                    }

                    if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {

                        $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
                    }

                    if (isset(Yii::$app->request->get()['industry']) && Yii::$app->request->get()['industry'] != '') {

                        $industry = explode(",", Yii::$app->request->get()['industry']);
                    }

                    if (isset(Yii::$app->request->get()['skill']) && Yii::$app->request->get()['skill'] != '') {

                        $skill = explode(",", Yii::$app->request->get()['skill']);
                    }

                    if (isset(Yii::$app->request->get()['role']) && Yii::$app->request->get()['role'] != '') {

                        $role = Yii::$app->request->get()['role'];
                    }

                    if (isset(Yii::$app->request->get()['experience']) && Yii::$app->request->get()['experience'] != '') {

                        $exp = Yii::$app->request->get()['experience'];
                    }

                    if (isset(Yii::$app->request->get()['cresume']) && Yii::$app->request->get()['cresume'] != '') {

                        $cresume = Yii::$app->request->get()['cresume'];
                    }

                    if (isset(Yii::$app->request->get()['specialization'])) {

                        $naukari_specialization = Yii::$app->request->get()['specialization'];
                    }

                    if (isset(Yii::$app->request->get()['course'])) {

                        $naukari_course = Yii::$app->request->get()['course'];
                    }

                    if (isset(Yii::$app->request->get()['language']) && Yii::$app->request->get()['language'] != '') {

                        $language = Yii::$app->request->get()['language'];

                        $language_ary = explode(',', $language);
                    }

                    $ddate = date('Y-m-d', strtotime("-$latest days"));

                    if ($role != '' && $exp != '') {

                        if ($cresume == 1) {

                            $allcandidate = $alluser->find()
                                ->where([
                                'AllUser.IsDelete' => 0,
                                'VerifyStatus' => 1,
                                'UserTypeId' => 5,
                                'IsExp' => 1
                            ])
                                ->joinWith([
                                'educations',
                                'skilldetail',
                                'experiences',
                                'teamRelatedSkills'
                            ])
                                ->andWhere([
                                '!=',
                                'CVId',
                                0
                            ])
                                ->andFilterWhere([
                                'State' => $state,
                                'Gender' => $gender,
                                'Experience.IndustryId' => $industry,
                                'EmployeeSkill.SkillRoleId' => $role,
                                'Experience.Experience' => $exp,
                                'EmployeeSkill.SkillId' => $skill
                            ])
                                ->
                            // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                            groupBy([
                                'AllUser.UserId'
                            ])
                                ->orderBy([
                                'AllUser.OnDate' => SORT_DESC
                            ]);
                        } else {

                            $allcandidate = $alluser->find()
                                ->where([
                                'AllUser.IsDelete' => 0,
                                'VerifyStatus' => 1,
                                'UserTypeId' => 5,
                                'IsExp' => 1
                            ])
                                ->joinWith([
                                'educations',
                                'skilldetail',
                                'experiences',
                                'teamRelatedSkills'
                            ])
                                ->andFilterWhere([
                                'State' => $state,
                                'Gender' => $gender,
                                'Experience.IndustryId' => $industry,
                                'EmployeeSkill.SkillRoleId' => $role,
                                'Experience.Experience' => $exp,
                                'EmployeeSkill.SkillId' => $skill
                            ])
                                ->
                            // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                            groupBy([
                                'AllUser.UserId'
                            ])
                                ->orderBy([
                                'AllUser.OnDate' => SORT_DESC
                            ]);
                        }
                    } else {
                        if ($exp != '') {
                            if ($cresume == 1) {
                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 5,
                                    'IsExp' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences',
                                    'teamRelatedSkills'
                                ])
                                    ->andWhere([
                                    '!=',
                                    'CVId',
                                    0
                                ])
                                    ->andFilterWhere([
                                    'State' => $state,
                                    'Gender' => $gender,
                                    'Experience.IndustryId' => $industry,
                                    'EmployeeSkill.SkillRoleId' => $role,
                                    'Experience.Experience' => $exp,
                                    'EmployeeSkill.SkillId' => $skill
                                ])
                                    ->
                                // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                                groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC,
                                    'Experience.Experience' => SORT_DESC
                                ]);
                            } else {
                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 5,
                                    'IsExp' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences',
                                    'teamRelatedSkills'
                                ])
                                    ->andFilterWhere([
                                    'State' => $state,
                                    'Gender' => $gender,
                                    'Experience.IndustryId' => $industry,
                                    'EmployeeSkill.SkillRoleId' => $role,
                                    'Experience.Experience' => $exp,
                                    'EmployeeSkill.SkillId' => $skill
                                ])
                                    ->andFilterWhere([
                                    '>=',
                                    'DATE( AllUser.UpdatedDate )',
                                    $ddate
                                ])
                                    ->groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC,
                                    'Experience.Experience' => SORT_DESC
                                ]);
                            }
                        } else {

                            if ($cresume == 1) {
                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 5,
                                    'IsExp' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences',
                                    'teamRelatedSkills'
                                ])
                                    ->andWhere([
                                    '!=',
                                    'CVId',
                                    0
                                ])
                                    ->andFilterWhere([
                                    'State' => $state,
                                    'Gender' => $gender,
                                    'Experience.IndustryId' => $industry,
                                    'EmployeeSkill.SkillRoleId' => $role,
                                    // 'Experience.Experience'=>$exp,
                                    'EmployeeSkill.SkillId' => $skill
                                ])
                                    ->
                                // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])
                                groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC
                                ]);
                            } else {

                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 5,
                                    'IsExp' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences',
                                    'teamRelatedSkills'
                                ])
                                    ->andFilterWhere([
                                    'State' => $state,
                                    'Gender' => $gender,
                                    'Experience.IndustryId' => $industry,
                                    'EmployeeSkill.SkillRoleId' => $role,
                                    // 'Experience.Experience'=>$exp,
                                    'EmployeeSkill.SkillId' => $skill
                                ])
                                    ->
                                // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])
                                groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC
                                ]);
                            }
                        }
                    }
                }

                // experience search
            } else {
				//die('Test')
                $allcandidate = $alluser->find()
                    ->where([
                    'AllUser.IsDelete' => 0,
                    'IsExp' => 0,
                    'VerifyStatus' => 1,
                    'UserTypeId' => 5
                ])
                    ->joinWith([
                    'educations',
                    'skilldetail',
                    'teamRelatedSkills'
                ])
                    ->andFilterWhere([

                    'State' => $state,

                    'Gender' => $gender,

                    'Experience.IndustryId' => $industry,

                    'EmployeeSkill.SkillId' => $skill
                ])
                    ->groupBy([
                    'AllUser.UserId'
                ])
                    ->orderBy([
                    'AllUser.OnDate' => SORT_DESC
                ]);
            }
            if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'Y') {

                $allcandidate->andWhere([
                    '!=',
                    'AllUser.whatsappno',
                    " "
                ]);
            }
            if (! empty($education)) {

                $allcandidate->andWhere([
                    'Education.highest_qual_id' => $education
                ]);
            }
            if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'N') {

                $allcandidate->andWhere([
                    '=',
                    'AllUser.whatsappno',
                    " "
                ]);
            }
            if (! empty($salary)) {
                $tmp_ary = array(
                    'or'
                );
                foreach ($salary as $key => $value) {
                    $tmp_ary[] = [
                        'like',
                        'Experience.Salary',
                        $value
                    ];
                }
                $allcandidate->andFilterWhere($tmp_ary);
            }

            if (isset(Yii::$app->request->get()['latest']) && ! empty(Yii::$app->request->get()['latest'])) {

                $allcandidate->andFilterWhere([
                    '>=',
                    'DATE( AllUser.UpdatedDate )',
                    $ddate
                ]);
            }

            if (isset($naukari_specialization) && ! empty($naukari_specialization)) {

                $allcandidate->andFilterWhere([
                    'Education.specialization_id' => $naukari_specialization
                ]);
            }

            if (isset($naukari_course) && ! empty($naukari_course)) {

                $allcandidate->andFilterWhere([
                    'Education.course_id' => $naukari_course
                ]);
            }

            if (isset(Yii::$app->request->get()['condidateage']) && Yii::$app->request->get()['condidateage'] != '') {

                $condidateage = Yii::$app->request->get()['condidateage'];

                $allcandidate->andFilterWhere([
                    '<=',
                    'AllUser.Age',
                    $condidateage
                ]);
            }

            if (isset(Yii::$app->request->get()['worktype']) && Yii::$app->request->get()['worktype'] != '') {

                $worktype = Yii::$app->request->get()['worktype'];

                $allcandidate->andFilterWhere([
                    'AllUser.WorkType' => $worktype
                ]);
            }

            if (isset(Yii::$app->request->get()['PreferredLocation']) && Yii::$app->request->get()['PreferredLocation'] != '') {

                $PreferredLocation = explode(',', Yii::$app->request->get()['PreferredLocation']);

                foreach ($PreferredLocation as $pl) {

                    $allcandidate->andFilterWhere([
                        'OR',
                        [
                            'LIKE',
                            'AllUser.PreferredLocation',
                            $pl
                        ]
                    ]);
                }
            }
            if (isset($language_ary) && ! empty($language_ary)) {
                $tmp_ary = array(
                    'or'
                );
                foreach ($language_ary as $key => $value) {
                    $tmp_ary[] = [
                        'like',
                        'AllUser.language',
                        $value
                    ];
                }
                $allcandidate->andFilterWhere($tmp_ary);
            }
            // echo $allcandidate->createCommand()->getRawSql(); die;
			
			if (isset(Yii::$app->request->get()['category_id']) && Yii::$app->request->get()['category_id'] != '') {

                $categoryId = Yii::$app->request->get()['category_id'];
				$userIds = ArrayHelper::map(UserJobCategory::find()->where([
											'category_id' => $categoryId
										])->all(), 'user_id', 'user_id');
				if(!empty($userIds)){
					$allcandidate->andFilterWhere([
						'AllUser.UserId' => $userIds
					]);
				}
				
			}
			
			if (isset(Yii::$app->request->get()['collar_role']) && Yii::$app->request->get()['collar_role'] != '') {

                $collarRole = Yii::$app->request->get()['collar_role'];
				if(!empty($collarRole)){
					$collarRoleId = explode(',', $collarRole);
					$userIds = ArrayHelper::map(UserWhiteRole::find()->where([
											'role_id' => $collarRoleId
										])->all(), 'user_id', 'user_id');
					if(!empty($userIds)){
						$allcandidate->andFilterWhere([
							'AllUser.UserId' => $userIds
						]);
					}					
				}				
					
			}
			
			if (isset(Yii::$app->request->get()['collar_skills']) && Yii::$app->request->get()['collar_skills'] != '') {

                $collarSkills = Yii::$app->request->get()['collar_skills'];
				if(!empty($collarSkills)){
					$skillId = explode(',', $collarSkills);
					$userIds = ArrayHelper::map(UserWhiteSkill::find()->where([
											'skill_id' => $skillId
										])->all(), 'user_id', 'user_id');
					if(!empty($userIds)){
						$allcandidate->andFilterWhere([
							'AllUser.UserId' => $userIds
						]);
					}					
				}
				
				
			}
			
			
			
			
            $pages = new Pagination([
                'totalCount' => $allcandidate->count()
            ]);

            if (isset(Yii::$app->request->get()['perpage'])) {

                $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
            } else {

                $pages->defaultPageSize = 10;
            }

            $allcandidate = $allcandidate->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            // echo $allcandidate->createCommand()->getRawSql(); die;

            $allposition = Position::find()->select([
                'Position',
                'PositionId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'Position' => SORT_ASC
            ])
                ->all();

            $allindustry = Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all();

            $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();

            // skill

            $allskill = Skill::find()->where([
                'IsDelete' => 0
            ])->all();

            $allcourse = Course::find()->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();

            $allpost = ArrayHelper::map(PostJob::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'JobStatus' => 0,
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all(), 'JobId', 'JobTitle');

            $cities = Cities::find()->all();

            $naukari_specialization = NaukariSpecialization::find()->all();

            $language_opts = LanguagesOpts::find()->all();

            $plan = new Plan();

            $allplan = $plan->getAllplan();

            $planassign = new PlanAssign();

            $isassign = $planassign->isAssign(Yii::$app->session['Employerid']);

            if (Yii::$app->request->isAjax) {

                $salaryrange = array();
                

                return $this->renderPartial('_findteam_ajax', array(
                    'candidate' => $allcandidate,
                    'pages' => $pages,
                    'allplan' => $allplan,
                    'planassign' => $planassign,
                    'url' => $url,
                    'imageurl' => $imageurl,
                    'isassign' => $isassign,
                    'salaryrange' => $salaryrange
                ));
            } else {
				$whiteCategoryList =  ArrayHelper::map(WhiteCategory::find()->where([
						'status' => 1
					])
						->orderBy([
						'name' => 'SORT_ASC'
					])
					->all(), 'id', 'name');
                return $this->render('findteam', [
                    'language_opts' => $language_opts,
                    'allplan' => $allplan,
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'candidate' => $allcandidate,
                    'pages' => $pages,
                    'allcategory' => $allposition,
                    'allindustry' => $allindustry,
                    'isassign' => $isassign,
                    'allskill' => $allskill,
                    'mailmodel' => $mailmodel,
                    'course' => $allcourse,
                    'allpost' => $allpost,
                    'cities' => $cities,
                    'keyname' => $keyname,
                    'indexlocation' => $indexlocation,
                    'experience' => $experience,
                    'category' => $category,
					'whiteCategories' => $whiteCategoryList
                ]);
            }
        }
    }

    public function actionExportteam($Eid)
    {
        $Eid = explode(",", trim($Eid, ","));
        $model = AllUser::find()->where([
            'UserId' => $Eid
        ])->all();

        $planassign = PlanAssign::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'IsDelete' => 0
        ])->one();
        if (! empty($Eid) && ! empty($planassign)) {

            foreach ($Eid as $key => $value1) {

                $planrecord_cnt = PlanRecord::find()->where([

                    'EmployerId' => Yii::$app->session['Employerid'],

                    'UserId' => $value1,

                    'Type' => 'Download',

                    'PlanId' => $planassign->PlanId
                ])->count();

                if ($planrecord_cnt == 0) {

                    $planrecord = new PlanRecord();

                    $planrecord->PlanId = $planassign->PlanId;

                    $planrecord->EmployerId = $planassign->EmployerId;

                    $planrecord->Type = 'Download';

                    $planrecord->Amount = 1;

                    $planrecord->UserId = $value1;
					
                    $planrecord->UpdatedDate = date('Y-m-d H:i:s');

                    $planrecord->save(false);

                    $k ++;
                }
            }
            $planassign->UseDownload = $planassign->UseDownload + $k;

            $planassign->save(false);
        }
        $rest = array();

        foreach ($model as $key => $value) {
            $rest[$key]['Name'] = $value->Name;
            $rest[$key]['Email'] = $value->Email;
            $rest[$key]['MobileNo'] = $value->MobileNo;
            $rest[$key]['Address'] = $value->Address;
            $rest[$key]['State'] = $value->State;
            $rest[$key]['City'] = $value->City;
            $rest[$key]['Pincode'] = $value->PinCode;
            $rest[$key]['TotalTeamMember'] = $value->NoOfTeamMember;
            $skill = '';
            foreach ($value->empRelatedSkills as $ask => $asv) {
                $skill .= $asv->skill->Skill . ', ';
            }
            $skill = trim(trim($skill), ",");
            $rest[$key]['Skills'] = $skill;
            if ($value->experiences) {
                $count = count($value->experiences);
                $salary = $value->experiences[$count - 1]->Salary . ' Lakh';
                $exp = $value->experiences[$count - 1]->Experience . ' Year';
            } else {
                $salary = 'nil';
                $exp = 'nil';
            }
            if ($value->minqualification) {
                $min = $value->minqualification->MinQualification;
            } else {
                $min = '';
            }
            $rest[$key]['Experience'] = $exp;
            $rest[$key]['TeamMembersMinimumQualification'] = $min;
        }

        $doc = new \PHPExcel();
        $doc->setActiveSheetIndex(0);

        $doc->getActiveSheet()->fromArray(array_keys(current($rest)), null, 'A1');
        $doc->getActiveSheet()->fromArray($rest, null, 'A2');

        $styleArray = array(
            'font' => array(
                'bold' => false,
                'color' => array(
                    'rgb' => '337ab7'
                ),
                'size' => 13,
                'name' => 'Arial'
            )
        );
        $doc->getActiveSheet()
            ->getStyle('A1:L1')
            ->applyFromArray($styleArray);

        $doc->getActiveSheet()
            ->getDefaultStyle()
            ->getFont()
            ->setName('Arial')
            ->setSize(10);

        foreach (range('A', 'L') as $columnID) {
            $doc->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true);
        }
        foreach ($doc->getActiveSheet()->getRowDimensions() as $rd) {
            $rd->setRowHeight(- 1);
        }

        $doc->getActiveSheet()
            ->getRowDimension(1)
            ->setRowHeight(20);
        $doc->getActiveSheet()->setTitle("Team List");
        $doc->getActiveSheet()
            ->getHeaderFooter()
            ->setOddHeader("Team List");
        $doc->getActiveSheet()
            ->getStyle($doc->getActiveSheet()
            ->calculateWorksheetDimension())
            ->getAlignment()
            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="team.xls"');
        header('Cache-Control: max-age=0');

        // Do your stuff here
        $writer = \PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
        ob_end_clean();
        ob_start();
        $writer->save('php://output');
    }

    public function actionTeamprofile()
    {
        $this->layout = 'layout';

        if (isset(Yii::$app->session['Teamid'])) {
            $model = new AllUser();
            $teamid = Yii::$app->session['Teamid'];
            $team = $model->find()
                ->where([
                'UserId' => $teamid
            ])
                ->one();

            $noofjobapplied = AppliedJob::find()->where([
                'Status' => 'Applied',
                'UserId' => Yii::$app->session['Teamid'],
                'IsDelete' => 0
            ])->count();
            Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;

            $photo_id = $team->PhotoId;
            if ($photo_id != 0) {
                $pimage = $url . $team->photo->Doc;
                $img = explode(".", $pimage);
                $ext = $img[1];
                $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
                $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;
                if (file_exists($thumb)) {
                    $document = Documents::find()->where([
                        'DocId' => $photo_id
                    ])->one();
                    $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;
                    $document->save();
                }
                $profile = AllUser::find()->where([
                    'UserId' => Yii::$app->session['Teamid']
                ])->one();
                $url = '/backend/web/';
                $pimage = $url . $profile->photo->Doc;
            } else {
                $pimage = '/images/user.png';
            }
            Yii::$app->session['TeamDP'] = $pimage;
            $image = Documents::getImageByAttr($team, 'PhotoId', 'photo');
            return $this->render('teamprofile', [
                'team' => $team,
                'image' => $image
            ]);
        } else {
            return $this->redirect([
                'site/index'
            ]);
        }
    }

    public function actionEditprofile()
    {
        if (isset(Yii::$app->session['Teamid'])) {
            $profile = AllUser::find()->where([
                'UserId' => Yii::$app->session['Teamid']
            ])->one();
            if ($profile->PhotoId != 0) {
                $url = '/backend/web/';
                $pimage = $url . $profile->photo->Doc;
            } else {
                $pimage = '/images/user.png';
            }

            $skill = new Skill();
            $position = new Position();
            $course = new Course();
            $docmodel = new Documents();

            $allskill = $skill->find()
                ->where("IsDelete=0")
                ->all();
            $allposition = $position->find()
                ->where("IsDelete=0")
                ->all();
            $allcourse = $course->find()
                ->where("IsDelete=0")
                ->all();
            $allindustry = ArrayHelper::map(Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all(), 'IndustryId', 'IndustryName');
            $photoid = $profile->PhotoId;
            $post = Yii::$app->request->post();

            /*
             * echo "<pre>";
             * print_r($post);
             * die();
             */

            if ($profile->load($post)) {
				
				//echo '<pre>';
				//print_r(Yii::$app->request->post());die;
				//echo '<pre>';
				//print_r($_FILES['TeamMemberPhoto']['name']);
				//print_r($post);
				//die;
                $photo_id = $photoid;
                if ($docmodel->load(Yii::$app->request->post())) {
                    $docmodel->image = UploadedFile::getInstance($docmodel, 'Doc');
                    if ($docmodel->image) {
						$photo_id = Documents::handleImageUpload($docmodel, 'Doc', 'PhotoId');	
                        // Documents::handleRawImageUpload();
                        /*$image = UploadedFile::getInstance($docmodel, 'Doc');
                        $ext = end((explode(".", $image->name)));
                        $ppp = Yii::$app->security->generateRandomString();
                        $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";
                        $docmodel->Doc = 'imageupload/' . $ppp . ".{$ext}";
                        $docmodel->OnDate = date('Y-m-d');
                        $docmodel->From = 'photo';
                        if ($docmodel->save()) {
                            $photo_id = $docmodel->DocId;
                        } else {
                            $photo_id = $photoid;
                        }*/
                    }
                }
                /*
                 * echo "<pre>";
                 * print_r($profile);
                 * die();
                 */
				 $cv = UploadedFile::getInstance($profile, 'CVId');

                if ($cv) {

                    $cv_id = $docmodel->imageUpload($cv, 'CVId');
                } else {

                    $cv_id = $cvid;
                }

				 

                $profile->State = States::findOne(['StateId' => $post['AllUser']['State']])->StateName;
                $profile->City = City::findOne(['CityID' => $post['AllUser']['City']])->CityName;
                $profile->Salary = $post['AllUser']['MySalary'];
                $profile->PhotoId = $photo_id;
				$profile->CVId = $cv_id;
                $profile->save(false);

                if ($profile->PhotoId != 0) {
                    $url = '/backend/web/';
                    $pimage = $url . $profile->photo->Doc;
                } else {
                    $pimage = '/images/user.png';
                }
                Yii::$app->session['TeamDP'] = $pimage;
				
                $education = Education::find()->where([
                    'UserId' => $profile->UserId
                ])->one();
                if ($education) {
					
                    $education->HighestQualification = isset(Yii::$app->request->post()['AllUser']['HighestQualId']) ? Yii::$app->request->post()['AllUser']['HighestQualId'] : 'none';
                    $education->CourseId = Yii::$app->request->post()['AllUser']['CourseId'];
                    $education->University = Yii::$app->request->post()['AllUser']['University'];
                    $education->SkillId = is_array($post['AllUser']['SkillRoleId']) ? implode(',', $post['AllUser']['SkillRoleId']) : $post['AllUser']['SkillRoleId'];
                    $education->PassingYear = Yii::$app->request->post()['AllUser']['PassingYear'];
                    $education->DurationTo = Yii::$app->request->post()['AllUser']['PassingYear'];
                    $education->specialization_id = $post['AllUser']['Specialization'];
                    $education->board_id = $post['AllUser']['board_id'];
					
                   if(!$education->save()){
					    echo "<pre>";
                        print_r($education->getErrors());
                        die();
				   }
                } else {
                    $education = new Education();
                    $education->HighestQualification = isset(Yii::$app->request->post()['AllUser']['HighestQualId']) ? Yii::$app->request->post()['AllUser']['HighestQualId'] : 'none';
                    $education->UserId = $profile->UserId;
                    $education->CourseId = Yii::$app->request->post()['AllUser']['CourseId'];
                    $education->PassingYear = Yii::$app->request->post()['AllUser']['PassingYear'];
                    $education->DurationTo = Yii::$app->request->post()['AllUser']['PassingYear'];
                    $education->University = Yii::$app->request->post()['AllUser']['University'];
                    $education->SkillId = is_array($post['AllUser']['SkillRoleId']) ? implode(',', $post['AllUser']['SkillRoleId']) : $post['AllUser']['SkillRoleId'];
                    $education->OnDate = date('Y-m-d');
                    $education->specialization_id = $post['AllUser']['Specialization'];
                    $education->board_id = $post['AllUser']['board_id'];
					
                    if (! $education->save()) {
                        echo "<pre>";
                        print_r($education->getErrors());
                        die();
                    }
                }
				
				/*Save Experience Info*/
				
				/*Save & Update Experience Code*/
				if(!empty(Yii::$app->request->post()['AllUser']['Experience'])){
					$experienceData = Yii::$app->request->post()['AllUser']['Experience'];
					/*Delete All Experience Skills*/
					EmployeeSkill::deleteAll([
						'UserId' => $profile->UserId,
						'TypeId' => 1
					]);
					foreach($experienceData as $expData){
						if(!empty($expData['YearFrom']) && !empty($expData['CompanyName'])){
						
						if(!empty($expData['ExperienceId'])){
							$experience = Experience::find()->where([
                                'ExperienceId' => $expData['ExperienceId']
                            ])
                                ->one();

                            $experience->CompanyName = $expData['CompanyName'];

                            $experience->PositionId = $expData['PositionId'];

                            $experience->IndustryId = $expData['CIndustryId'];

                            $experience->Experience = $expData['Experience'];
                            $experience->PositionName = $expData['PositionName'];
                            $experience->YearFrom = $expData['YearFrom'];
							

                            $experience->YearTo = $expData['YearTo'];
							
							if(isset($expData['CurrentWorking']) && !empty($expData['CurrentWorking'])){
								$experience->YearTo = 'Till Now';
								$experience->CurrentWorking = 1;
							}else{
								$experience->CurrentWorking = 0;
							}

                            $experience->Salary = $expData['Salary'];

                            $experience->save(false);
							
						}else{
							$experience = new Experience();

                            $experience->UserId = $profile->UserId;

                            $experience->CompanyName = $expData['CompanyName'];

                            $experience->PositionId = $expData['PositionId'];

                            $experience->IndustryId = $expData['CIndustryId'];

                            $experience->YearFrom = $expData['YearFrom'];

                            $experience->YearTo = $expData['YearTo'];

                            if (! $expData['YearTo'] ) {

                                $experience->YearTo = 'Till Now';
                            }
							
							if(isset($expData['CurrentWorking']) && !empty($expData['CurrentWorking'])){
								$experience->YearTo = 'Till Now';
								$experience->CurrentWorking = 1;
							}else{
								$experience->CurrentWorking = 0;
							}
                            $experience->Experience = $expData['Experience'];
                            $experience->PositionName = $expData['PositionName'];
                            $experience->Salary = $expData['Salary'];
                            $experience->OnDate = date('Y-m-d');
                            $experience->save(false);							
						}
						
						 $experienceId =  $experience->ExperienceId;
						 /*Save Experience Skills*/
						 if(!empty($expData['ExperienceSkillId']) && !empty($expData['PositionId'])){
							 foreach($expData['ExperienceSkillId'] as $skill){
								$empskill = new EmployeeSkill();
								
								$empskill->SkillRoleId = $expData['PositionId'];

								$empskill->SkillId = $skill;

								$empskill->UserId = $profile->UserId;

								$empskill->ExperienceId = $experienceId;
								
								$empskill->TypeId = 1;

								$empskill->OnDate = date('Y-m-d');

								$empskill->save(false);
							 }
							 
						 }
						 
						}
						 
					}
					
				}

                /*$exps = isset($post['AllUser']['CompanyName']) ? $post['AllUser']['CompanyName'] : [];
                if ($exps) {
                    Experience::deleteAll([
                        'UserId' => $profile->UserId
                    ]);
                    foreach ($exps as $key => $exp) {
                        $experience = new Experience();
                        $experience->YearFrom = $post['AllUser']['YearFrom'][$key];
                        $experience->YearTo = $post['AllUser']['YearTo'][$key];
                        $experience->CompanyName = $post['AllUser']['CompanyName'][$key];
                        $experience->PositionName = $post['AllUser']['PositionName'][$key];
                        $experience->IndustryId = $post['AllUser']['CIndustryId'][$key];
                        $experience->UserId = $profile->UserId;
                        $experience->PositionId = $post['AllUser']['PositionId'][$key];
                        $experience->Experience = $post['AllUser']['Experience'][$key];
                        $experience->Salary = $post['AllUser']['Salary'][$key];
                        $experience->OnDate = date('Y-m-d');
                        if (! $experience->save()) {
                            echo "<pre>Experience  :  ";
                            print_r($experience->getErrors());
                            die();
                        }
                    }
                }*/

                $skillidlist = '';
                // $skilllist=trim(trim(Yii::$app->request->post()['AllUser']['RawSkill']),",");

                $rawskill = $post['AllUser']['EducationSkillId'];
                EmployeeSkill::saveSkillListByType($rawskill, EmployeeSkill::TYPE_EDUCATION, $profile);

                /*$rawskill = $post['AllUser']['ExperienceSkillId'];
                EmployeeSkill::saveSkillListByType($rawskill, EmployeeSkill::TYPE_EXPERIENCE, $profile);*/

                // team member
				$Teamphoto = UploadedFile::getInstancesByName('TeamMemberPhoto');
				if(!empty(Yii::$app->request->post()['AllUser']['Team'])){
					$teamData = Yii::$app->request->post()['AllUser']['Team'];
					$files = (isset($_FILES['TeamMemberPhoto']['name'])?$_FILES['TeamMemberPhoto']['name']:"");
					$f = 0;
					foreach($teamData as $key => $team){
						if(!empty($team['TeamMembersId'])){
							$teammembers = TeamMembers::find()->where([
                                'TeamMembersId' => $team['TeamMembersId']
                            ])->one();
							
							$teammembers->MemberName = $team['TeamName'];
							$teammembers->MemberQualification = $team['MemberQualification'];
							$teammembers->MemberCourse = $team['MemberCourse'];
							$teammembers->MemberSalary = $team['MemberSalary'];
							$teammembers->TeamId = $profile->UserId;
							$teammembers->IsDelete = 0;
							$teammembers->OnDate = date('Y-m-d');
							$teammembers->save();
							
						}else{
							if(isset($files[$key]) && !empty($files[$key])){
								if (! empty($Teamphoto[$f])) {
									$docmodel2 = new Documents();
									$memberpic_id = $docmodel2->imageUpload($Teamphoto[$f], 'Teammemberphoto');
									$f++;
								} else {
									$memberpic_id = 0;
								}
								$teammembers = new TeamMembers();
								$teammembers->MemberName = $team['TeamName'];
								$teammembers->MemberQualification = $team['MemberQualification'];
								$teammembers->MemberCourse = $team['MemberCourse'];
								$teammembers->MemberSalary = $team['MemberSalary'];
								$teammembers->MemberPhoto = $memberpic_id;
								$teammembers->TeamId = $profile->UserId;
								$teammembers->IsDelete = 0;
								$teammembers->OnDate = date('Y-m-d');
								$teammembers->save();
							}
							
							
							
						}		
					}
					
				}
				
				
				
                /*if (Yii::$app->request->post()['AllUser']['TeamName']) {
                    TeamMembers::deleteAll([
                        'TeamId' => $profile->UserId
                    ]);
                }
                foreach (Yii::$app->request->post()['AllUser']['TeamName'] as $fk => $value) {
                    if (Yii::$app->request->post()['AllUser']['TeamName'][$fk] != '') {
                        if (! empty($Teamphoto[$fk])) {
                            $docmodel2 = new Documents();
                            $memberpic_id = $docmodel2->imageUpload($Teamphoto[$fk], 'Teammemberphoto');
                        } else {
                            $memberpic_id = 0;
                        }

                        $teammembers = new TeamMembers();
                        $teammembers->MemberName = Yii::$app->request->post()['AllUser']['TeamName'][$fk];
                        $teammembers->MemberQualification = Yii::$app->request->post()['AllUser']['MemberQualification'][$fk];
                        $teammembers->MemberCourse = Yii::$app->request->post()['AllUser']['MemberCourse'][$fk];
                        $teammembers->MemberSalary = Yii::$app->request->post()['AllUser']['MemberSalary'][$fk];
                        $teammembers->MemberPhoto = $memberpic_id;
                        $teammembers->TeamId = $profile->UserId;
                        $teammembers->IsDelete = 0;
                        $teammembers->OnDate = date('Y-m-d');
                        $teammembers->save();
                    }
                }*/

                // team member minimum skill
                /*
                 * /**
                 * Vishal
                 * $tcourseId = Yii::$app->request->post()['AllUser']['TeamCourseId'];
                 * $TeamMembersMinQualification = TeamMembersMinQualification::find()->where([
                 * 'TeamId' => $profile->UserId
                 * ])->one();
                 * if (empty($TeamMembersMinQualification)) {
                 * $TeamMembersMinQualification = new TeamMembersMinQualification();
                 * }
                 * $TeamMembersMinQualification->MinQualification = Yii::$app->request->post()['AllUser']['MinQualification'];
                 * $TeamMembersMinQualification->CourseId = $tcourseId;
                 * $TeamMembersMinQualification->save();
                 */
                // var_dump($TeamMembersMinQualification->getErrors());

                // team member skill
                /*
                 * TeamQualificationSkillMap::deleteAll([
                 * 'TeamId' => $profile->UserId
                 * ]);
                 * $skillidlist = '';
                 * $skilllist = trim(trim(Yii::$app->request->post()['AllUser']['TeamRawSkill']), ",");
                 * $rawskill = explode(",", $skilllist);
                 * foreach ($rawskill as $rk => $rval) {
                 * if ($rval != '') {
                 * $indskill = trim($rval);
                 * $nskill = new Skill();
                 * $cskill = $nskill->find()
                 * ->where([
                 * 'Skill' => $indskill,
                 * 'IsDelete' => 0
                 * ])
                 * ->one();
                 * if (! $cskill) {
                 * $nskill->Skill = $indskill;
                 * $nskill->OnDate = date('Y-m-d');
                 * $nskill->save();
                 * $skid = $nskill->SkillId;
                 * } else {
                 * $skid = $cskill->SkillId;
                 * }
                 * $skillidlist .= $skid;
                 * $TeamQualificationSkillMap = new TeamQualificationSkillMap();
                 * $TeamQualificationSkillMap->SkillId = $skid;
                 * $TeamQualificationSkillMap->TeamId = $profile->UserId;
                 * $TeamQualificationSkillMap->MinQId = $TeamMembersMinQualification->MinId;
                 * $TeamQualificationSkillMap->OnDate = date('Y-m-d');
                 * $TeamQualificationSkillMap->save();
                 * // var_dump($TeamQualificationSkillMap->getErrors());
                 * }
                 * }
                 */

                Yii::$app->session->setFlash('success', "Profile Updated Successfully");
                return $this->redirect([
                    'teamprofile'
                ]);
            } else {
                $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();
                $naukriSpecialization = NaukariSpecialization::getSpecializationList();
                $board = NaukariQualData::getHighestQualificationList([
                    4,
                    5
                ]);
                $employeeRoleSkill = EmployeeSkill::find()->where([
                    'UserId' => $profile->UserId,
                    'TypeId' => EmployeeSkill::TYPE_EDUCATION
                ])
                    ->groupBy('SkillRoleId')
                    ->all();
                /*
                 * echo "<pre>";
                 * print_r($employeeRoleSkill);
                 * die();
                 */
				 
				 $whiteCategoryList =  ArrayHelper::map(WhiteCategory::find()->where([
						'status' => 1
					])
						->orderBy([
						'name' => 'SORT_ASC'
					])
					->all(), 'id', 'name');

                $this->layout = 'layout';

                return $this->render('editprofile', [
                    'profile' => $profile,
                    'skill' => $allskill,
                    'position' => $allposition,
                    'course' => $allcourse,
                    'industry' => $allindustry,
                    'docmodel' => $docmodel,
                    'NaukariQualData' => $NaukariQualData,
                    'naukriSpecialization' => $naukriSpecialization,
                    'board' => $board,
                    'employeeRoleSkill' => $employeeRoleSkill,
					'whiteCategories' => $whiteCategoryList
                ]);
            }
        } else {
            return $this->redirect([
                'site/index'
            ]);
        }
    }

    public function actionChangepwd()
    {
        $model = new AllUser();

        if ($model->load(Yii::$app->request->post())) {
            $cpass = md5(Yii::$app->request->post('AllUser')['CPassword']);
            $npass = Yii::$app->request->post('AllUser')['NPassword'];
            $UserId = Yii::$app->request->post('AllUser')['UserId'];
            $chk = $model->find()
                ->where([
                'UserId' => $UserId
            ])
                ->one();
            if ($chk->Password == $cpass) {
                $chk->Password = md5($npass);
                $chk->save();
                Yii::$app->session->setFlash('success', 'Password Has been Changed.');
                return $this->redirect([
                    'teamprofile'
                ]);
            } else {
                Yii::$app->session->setFlash('error', 'Incorrect Current Password.');
                return $this->render('changepassword', [
                    'model' => $model
                ]);
            }
        } else {
            return $this->render('changepassword');
        }
    }

    public function actionDashboard()
    {
        if (isset(Yii::$app->session['Teamid'])) {
            // footer section
            // first block
            $about = new FooterAboutus();
            $footerabout = $about->find()->one();
            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block
            $cp = new FooterCopyright();
            $allcp = $cp->find()->one();
            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block
            $dblock = new FooterDevelopedblock();
            $developerblock = $dblock->find()->one();
            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block
            $socialicon = new SocialIcon();
            $sicon = $socialicon->find()->one();
            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block
            $th = new FooterThirdBlock();
            $thirdblock = $th->find()->one();
            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            $candidateapplied = AppliedJob::find()->where([
                'UserId' => Yii::$app->session['Teamid'],
                'Status' => 'Applied'
            ])->count();

            $candidatebookmarked = AppliedJob::find()->where([
                'UserId' => Yii::$app->session['Teamid'],
                'Status' => 'Bookmark'
            ])->count();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Candidate'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(10)
                ->all();
                    $this->layout = 'layout';
            return $this->render('dashboard', [
                'alljob' => $recentjob,
                'appliedjob' => $candidateapplied,
                'bookmarked' => $candidatebookmarked
            ]);
        } else {
            return $this->redirect([
                'site/index'
            ]);
        }
    }

    public function actionTeamlogout()
    {
        $session = Yii::$app->session;
        $session->open();
        unset(Yii::$app->session['Teamid']);
        unset(Yii::$app->session['TeamName']);
        unset(Yii::$app->session['TeamEmail']);
        unset(Yii::$app->session['TeamDP']);
        unset(Yii::$app->session['SocialEmail']);
        unset(Yii::$app->session['SocialName']);

        return $this->redirect([
            'site/index'
        ]);
    }

    public function actionBookmarkedjob()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        $model = new AppliedJob();
        $appliedjobs = $model->find()->where([
            'IsDelete' => 0,
            'Status' => 'Bookmark',
            'UserId' => Yii::$app->session['Teamid']
        ]);

        $pages = new Pagination([
            'totalCount' => $appliedjobs->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }
        $appliedjobs = $appliedjobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->layout = 'layout';
    
        return $this->render('bookmarkedjob', [
            'model' => $appliedjobs,
            'pages' => $pages
        ]);
    }

    public function actionAppliedjob()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        $model = new AppliedJob();
        $appliedjobs = $model->find()->where([
            'IsDelete' => 0,
            'Status' => 'Applied',
            'UserId' => Yii::$app->session['Teamid']
        ]);

        $pages = new Pagination([
            'totalCount' => $appliedjobs->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 10;
        }
        $appliedjobs = $appliedjobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        // Top Job
        $topjob = PostJob::find()->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Candidate'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();
            
        $this->layout = 'layout';
                
        return $this->render('appliedjob', [
            'model' => $appliedjobs,
            'pages' => $pages,
            'topjob' => $topjob
        ]);
    }

    public function actionDelmember($picid)
    {
        $model = TeamMembers::find()->where([
            'TeamMembersId' => $picid
        ])
            ->one()
            ->delete();
        echo json_encode(1);
    }

    public function actionTeamdetail($UserId)
    {
        // footer section
        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        $model = new AllUser();
        $teamid = Yii::$app->request->get()['UserId'];
        $team = $model->find()
            ->where([
            'UserId' => $teamid
        ])
            ->one();

        $noofjobapplied = AppliedJob::find()->where([
            'Status' => 'Applied',
            'UserId' => $teamid,
            'IsDelete' => 0
        ])->count();
        Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;

        $photo_id = $team->PhotoId;
        if ($photo_id != 0) {
            $pimage = $url . $team->photo->Doc;
            $img = explode(".", $pimage);
            $ext = $img[1];
            $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
            $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;
            if (file_exists($thumb)) {
                $document = Documents::find()->where([
                    'DocId' => $photo_id
                ])->one();
                $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;
                $document->save();
            }
            $profile = AllUser::find()->where([
                'UserId' => $teamid
            ])->one();
            $url = '/backend/web/';
            $pimage = $url . $profile->photo->Doc;
        } else {
            $pimage = '/images/user.png';
        }
        $teamimage = $pimage;

        $planassign = PlanAssign::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'IsDelete' => 0
        ])->one();
        $planrecord_cnt = PlanRecord::find()->where([

            'EmployerId' => Yii::$app->session['Employerid'],

            'UserId' => $_GET['UserId'],

            'Type' => 'ContactView',

            'PlanId' => $planassign->PlanId
        ])->count();
        $daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView', $planassign->plan->PlanId, date("Y-m-d"));
        $availableviewcontact = $planassign->plan->total_daily_view_contact - $daily_Contact;
        if ($availableviewcontact <= 0 && $planrecord_cnt == 0) {
            throw new \yii\web\NotFoundHttpException();
            exit();
        }

        if ($planrecord_cnt == 0) {

            $planassign->UseViewContact = $planassign->UseViewContact + 1;

            $planassign->save(false);

            $planrecord = new PlanRecord();

            $planrecord->PlanId = $planassign->PlanId;

            $planrecord->EmployerId = $planassign->EmployerId;

            $planrecord->Type = 'ContactView';

            $planrecord->Amount = 1;

            $planrecord->UserId = $_GET['UserId'];
			
			$planrecord->UpdatedDate = date('Y-m-d H:i:s');

            $planrecord->save(false);
        }
        return $this->render('teamdetail', [
            'team' => $team,
            'teamim' => $teamimage
        ]);
    }
	
	/*Delete User Resume*/
	public function actionDeleteuserresume(){
		if (isset(Yii::$app->session['Teamid'])) {
			$userId = Yii::$app->session['Teamid'];
			if(!empty($userId)){
				$userData = AllUser::find()->where([
								'UserId' => $userId
							])->one();
				if(!empty($userData)){
					$userData->CVId = 0;
					$userData->update(false);
					echo true;
				}			
			}
		}
		exit();
	}
	
}
?>