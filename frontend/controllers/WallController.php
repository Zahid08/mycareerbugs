<?php
namespace frontend\controllers;

use common\models\AllUser;
use common\models\EmployeeSkill;
use common\models\AppliedJob;
use common\models\CampusPost;
use common\models\Cities;
use common\models\City;
use common\models\CommentToPost;
use common\models\CompanyWallPost;
use common\models\Documents;
use common\models\EmpNotification;
use common\models\Follow;
use common\models\FooterAboutus;
use common\models\FooterContactus;
use common\models\FooterCopyright;
use common\models\FooterDevelopedblock;
use common\models\FooterThirdBlock;
use common\models\Industry;
use common\models\JobCategory;
use common\models\LeaveComment;
use common\models\LikeToPost;
use common\models\WatchToPost;
use common\models\Likes;
use common\models\Mail;
use common\models\Messages;
use common\models\Notification;
use common\models\Position;
use common\models\PostJob;
use common\models\Skill;
use common\models\SocialIcon;
use common\models\Wallpostsetting;
use frontend\models\ContactForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use common\models\UserWhiteRole;
use common\models\ExperienceWhiteRole;
use common\models\UserWhiteSkill;
use common\models\ExperienceWhiteSkill;


/**
 * Site controller
 */
class WallController extends Controller
{

    /**
     *
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'logout',
                    'signup'
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?'
                        ]
                    ],
                    [
                        'actions' => [
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [
                    $this,
                    'oAuthSuccess'
                ],
                'successUrl' => Url::to([
                    'wall/usersocial'
                ])
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    public function oAuthSuccess($client)
    {

        // get user data from client
        $userAttributes = $client->getUserAttributes();
        // var_dump($userAttributes);
        // die();
        if ($_GET['authclient'] == 'facebook') {
            $name = $userAttributes['name'];
            $id = $userAttributes['id'];
            $email = $userAttributes['email'];
            $gender = $userAttributes['gender'];
        } else if ($_GET['authclient'] == 'google') {
            $name = $userAttributes['name']['givenName'];
            $id = $userAttributes['id'];
            $email = $userAttributes['emails'][0]['value'];
        }

        $client = $_GET['authclient'];
        $session = Yii::$app->session;
        $session->open();

        $userlist = new AllUser();
        $count = $userlist->find()
            ->where([
            'Email' => $email,
            'IsDelete' => 0,
            'VerifyStatus' => 1
        ])
            ->andWhere([
            '!=',
            'UserTypeId',
            4
        ])
            ->count();
        if ($count > 0) {
            $rest = $userlist->find()
                ->where([
                'Email' => $email,
                'IsDelete' => 0,
                'VerifyStatus' => 1
            ])
                ->andWhere([
                '!=',
                'UserTypeId',
                4
            ])
                ->one();
            if ($rest->UserTypeId == 2) {
                Yii::$app->session['EmployeeEmail'] = $rest->Email;
                Yii::$app->session['Employeeid'] = $rest->UserId;
                Yii::$app->session['EmployeeName'] = $rest->Name;
                if ($rest->PhotoId != 0) {
                    $url = '/backend/web/';
                    $pimage = $url . $rest->photo->Doc;
                } else {
                    $pimage = '/images/user.png';
                }
                $appliedjob = new AppliedJob();
                $noofjobapplied = $appliedjob->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsDelete' => 0
                ])
                    ->count();
                Yii::$app->session['NoofjobApplied'] = $noofjobapplied;
                Yii::$app->session['EmployeeDP'] = $pimage;
            } elseif ($rest->UserTypeId == 3) {
                Yii::$app->session['Employerid'] = $rest->UserId;
                Yii::$app->session['EmployerName'] = $rest->Name;
                Yii::$app->session['EmployerEmail'] = $email;

                $employerone = $userlist->find()
                    ->where([
                    'UserId' => $rest->UserId
                ])
                    ->one();

                if ($employerone->LogoId != 0) {
                    $url = '/backend/web/';
                    $pimage = $url . $employerone->logo->Doc;
                } else {
                    $pimage = '/images/user.png';
                }
                Yii::$app->session['EmployerDP'] = $pimage;
            } elseif ($rest->UserTypeId == 5) {
                Yii::$app->session['Teamid'] = $rest->UserId;
                Yii::$app->session['TeamName'] = $rest->Name;
                Yii::$app->session['TeamEmail'] = $rest->Email;

                if ($rest->PhotoId != 0) {
                    $url = '/backend/web/';
                    $pimage = $url . $rest->photo->Doc;
                } else {
                    $pimage = '/images/user.png';
                }
                $appliedjob = new AppliedJob();
                $noofjobapplied = $appliedjob->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Teamid'],
                    'AppliedJob.IsDelete' => 0
                ])
                    ->count();
                Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;
                Yii::$app->session['TeamDP'] = $pimage;
            }
        } else {
            Yii::$app->session['SocialEmail'] = $email;
            Yii::$app->session['SocialName'] = $name;
            Yii::$app->session['SocialGender'] = $gender;
        }

        // do some thing with user data. for example with $userAttributes['email']
    }

    public function actionUsersocial()
    {
        // echo Yii::$app->session['referrer'];
        if (isset(Yii::$app->session['SocialEmail'])) {
            return $this->redirect([
                'site/register'
            ]);
        } else {
            return $this->redirect([
                'wall/mcbwallpost'
            ]);
        }
    }

    // index
    public function actionIndex()
    {
        if (isset(Yii::$app->session['Employeeid'])) {
            // return $this->redirect(['candidateprofile']);
            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif (isset(Yii::$app->session['Employerid'])) {
            // return $this->redirect(['companyprofile']);
            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif (isset(Yii::$app->session['Campusid'])) {
            // return $this->redirect(['campusprofile']);
            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif (isset(Yii::$app->session['Teamid'])) {
            // return $this->redirect(['teamprofile']);
            return $this->redirect([
                'mcbwallpost'
            ]);
        } else {
            $this->layout = 'wallindexlayout';
            $model = new AllUser();
            if ($model->load(Yii::$app->request->post())) {
                $password = md5($model->Password);
                $rest = $model->find()
                    ->where([
                    'Email' => $model->Email,
                    'IsDelete' => 0
                ])
                    ->one();
                if ($rest) {
                    if ($password == $rest->Password) {
                        if ($rest->VerifyStatus == 0) {
                            Yii::$app->session->setFlash('error', 'Please Check your mail for Email Verification.');
                            return $this->render('wall', [
                                'model' => $model
                            ]);
                        } else {
                            $rest = $model->find()
                                ->where([
                                'Email' => $model->Email,
                                'Password' => $password,
                                'IsDelete' => 0
                            ])
                                ->one();
                            Yii::$app->session['usrtid'] = $rest->UserId;
                            if ($rest->UserTypeId == 2) {
                                $session = Yii::$app->session;
                                $session->open();

                                Yii::$app->session['Employeeid'] = $rest->UserId;
                                Yii::$app->session['EmployeeName'] = $rest->Name;
                                Yii::$app->session['EmployeeEmail'] = $model->Email;
                                if ($rest->PhotoId != 0) {
                                    $url = '/backend/web/';
                                    $pimage = $url . $rest->photo->Doc;
                                } else {
                                    $pimage = 'images/user.png';
                                }
                                $appliedjob = new AppliedJob();
                                $noofjobapplied = $appliedjob->find()
                                    ->where([
                                    'Status' => 'Applied',
                                    'UserId' => Yii::$app->session['Employeeid'],
                                    'IsDelete' => 0
                                ])
                                    ->count();
                                Yii::$app->session['NoofjobApplied'] = $noofjobapplied;
                                Yii::$app->session['EmployeeDP'] = $pimage;
                                return $this->redirect([
                                    'mcbwallpost'
                                ]);
                            } elseif ($rest->UserTypeId == 3) {
                                $session = Yii::$app->session;
                                $session->open();
                                Yii::$app->session['Employerid'] = $rest->UserId;
                                Yii::$app->session['EmployerName'] = $rest->Name;
                                Yii::$app->session['EmployerEmail'] = $model->Email;

                                $employerone = $model->find()
                                    ->where([
                                    'UserId' => $rest->UserId
                                ])
                                    ->one();

                                if ($employerone->LogoId != 0) {
                                    $url = '/backend/web/';
                                    $pimage = $url . $employerone->logo->Doc;
                                } else {
                                    $pimage = 'images/user.png';
                                }
                                Yii::$app->session['EmployerDP'] = $pimage;

                                return $this->redirect([
                                    'mcbwallpost'
                                ]);
                            } elseif ($rest->UserTypeId == 4) {
                                $session = Yii::$app->session;
                                $session->open();
                                Yii::$app->session['Campusid'] = $rest->UserId;
                                Yii::$app->session['CampusName'] = $rest->Name;
                                Yii::$app->session['CampusEmail'] = $model->Email;
                                if ($rest->LogoId != 0) {
                                    $url = '/backend/web/';
                                    $pimage = $url . $rest->logo->Doc;
                                } else {
                                    $pimage = 'images/user.png';
                                }

                                Yii::$app->session['CampusDP'] = $pimage;

                                $noofjobapplied = AppliedJob::find()->where([
                                    'Status' => 'Applied',
                                    'UserId' => Yii::$app->session['Campusid']
                                ])->count();
                                Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied;

                                return $this->redirect([
                                    'mcbwallpost'
                                ]);
                            } elseif ($rest->UserTypeId == 5) {
                                $session = Yii::$app->session;
                                $session->open();
                                Yii::$app->session['Teamid'] = $rest->UserId;
                                Yii::$app->session['TeamName'] = $rest->Name;
                                Yii::$app->session['TeamEmail'] = $rest->Email;

                                if ($rest->PhotoId != 0) {
                                    $url = '/backend/web/';
                                    $pimage = $url . $rest->photo->Doc;
                                } else {
                                    $pimage = 'images/user.png';
                                }
                                Yii::$app->session['TeamDP'] = $pimage;
                                return $this->redirect([
                                    'mcbwallpost'
                                ]);
                            }
                        }
                    } else {
                        Yii::$app->session->setFlash('error', "Wrong Password");
                        return $this->render('wall', [
                            'model' => $model
                        ]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Wrong Email");
                    return $this->render('wall', [
                        'model' => $model
                    ]);
                }
            } else {
                return $this->render('wall', [
                    'model' => $model
                ]);
            }
        }
    }

    // candidate profile
    public function actionCandidateprofile()
    {
        if (isset(Yii::$app->session['Employeeid'])) {
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    //'IsView' => 0
                ])
                    ->orderBy([
                    'NotificationId' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                 ->limit(5)->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;

            $profile = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])->one();
            
            $skill = new Skill();
            $allskill = $skill->find()
                ->where("IsDelete=0")
                ->all();
                
            $skill = array();
            foreach ($profile->empRelatedSkills as $k => $v) {
                if($v->TypeId == 1){
                    $skill[$v->skill->SkillId] = $v->skill->Skill;
                }
            }
            
            $this->layout = 'walllayout';

            if ($profile->load(Yii::$app->request->post())) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($profile, 'CVId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'cv');
                } else {
                    $image_id = 0;
                }

                $profile->CVId = $image_id;
                if ($profile->save()) {
                    Yii::$app->session->setFlash('success', "Successfully CV uploaded");
                    return $this->redirect([
                        'candidateprofile'
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "There is some error please try again");
                    return $this->redirect([
                        'candidateprofile'
                    ]);
                }
            } else {
				//echo '<pre>';
				//print_r($skill);
                return $this->render('candidateprofile', [
                    'profile' => $profile,
                    'skill' => $skill,
                    'allskill' => $allskill
                ]);
            }
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    // company profile
    public function actionCompanyprofile()
    {
        if (isset(Yii::$app->session['Employerid'])) {

            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    //'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                 ->limit(5)->all();
            }
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end
            $this->layout = 'walllayout';

            $model = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employerid']
            ])->one();
            $totalactive = PostJob::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'JobStatus' => 0,
                'IsDelete' => 0,
                'IsHide' => 0
            ])->count();
            $allskill = PostJob::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'JobStatus' => 0,
                'IsDelete' => 0,
                'IsHide' => 0
            ])->all();
            $skill = array();
            foreach ($allskill as $key => $value) {
                foreach ($value->jobRelatedSkills as $k => $v) {
                    $skill[$v->SkillId] = $v->skill->Skill;
                }
            }
            $suggested = EmpNotification::find()->where([
                'EmpId' => Yii::$app->session['Employerid']
            ])
                ->groupBy([
                'Userid'
            ])
                ->all();

            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'EmployerId' => Yii::$app->session['Employerid']
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
            $comment = new CommentToPost();
            $leavecomment = new LeaveComment();
            $wallpost = new CompanyWallPost();
			
            if (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
				
                $date = date('Y-m-d');
                $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
                // var_dump(Yii::$app->request->post('CompanyWallPost'));
                $totaljob = CompanyWallPost::find()->where([
                    'WallPostId' => $id
                ])->one();

                $docmodel = new Documents();
				/*echo '<pre>';
				print_r(Yii::$app->request->post('CompanyWallPost'));die;*/
                $totaljob->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
                $totaljob->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
                //$totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];
                $totaljob->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
                $totaljob->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
				
				if(!empty(Yii::$app->request->post('q'))){
					$totaljob->Locationat = Yii::$app->request->post('q');
				}

                if ($totaljob->save()) {
                    // Yii::$app->session->setFlash('success', 'Post edited Successfully');
                } else {
                    // var_dump($wallpost->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }

                return $this->redirect([
                    'companyprofile'
                ]);
            } elseif ($wallpost->load(Yii::$app->request->post())) {
                $date = date('Y-m-d');
            $totaljob = CompanyWallPost::find()->where([
                'IsHide' => 0,
                'EmployerId' => Yii::$app->session['Employerid'],
                'date(OnDate)' => $date
            ])->count();
            $Wallpostsetting = Wallpostsetting::find()->where([
                'companyId' => Yii::$app->session['Employerid']
            ])->one();
            $Wallpostsettingcount = (isset($Wallpostsetting->wallPostSetting)) ? $Wallpostsetting->wallPostSetting : 24;

            $artt = Yii::$app->request->post('CompanyWallPost')['Locationat'];
			
            if ($totaljob < $Wallpostsettingcount) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                } else {
                    $image_id = 0;
                }

                $wallpost->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
				
				/*Create Slug*/
				$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
				$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
				$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
				$slug = preg_replace('~[^-\w]+~', '', $slug);
				$slug = trim($slug, '-');
				$slug = preg_replace('~-+~', '-', $slug);
				$slug = strtolower($slug);
				
				/*check slug*/
				$count = CompanyWallPost::find()
							->where([
								'Slug' => $slug
							])
							->count();
				if($count > 0){
					$slug = $slug.'-'.rand(9, 999);
				}
				
				
                $wallpost->Slug = $slug;
                $wallpost->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
                $wallpost->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
                $wallpost->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
                $wallpost->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];
                $wallpost->ImageId = $image_id;
                $wallpost->VideoId = 0;
                $wallpost->EmployerId = Yii::$app->session['Employerid'];
                $wallpost->OnDate = date('Y-m-d H:i:s');

                if ($wallpost->save()) {
                    Notification::createNotificationToRoleUser($wallpost);
                    Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    // var_dump($wallpost->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
                } else {
                    Yii::$app->session->setFlash('error', 'Your post limit exceed, Please Contact +91 8240362294');
                }
                return $this->redirect([
                    'companyprofile'
                ]);
            } elseif (isset(Yii::$app->request->post('CommentToPost')['CommentToPostId'])) {
                $id = Yii::$app->request->post('CommentToPost')['CommentToPostId'];
                $cmntcnt = $comment->find()
                    ->where([
                    'CommentToPostId' => $id
                ])
                    ->one();
                $cmntcnt->Message = Yii::$app->request->post('CommentToPost')['Message'];
                $cmntcnt->save();
                return $this->redirect([
                    'companyprofile'
                ]);
            } elseif ($comment->load(Yii::$app->request->post())) {
                $comment->OnDate = date('Y-m-d H:i:s');
                if ($comment->save()) {
                    // Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    var_dump($comment->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
                return $this->redirect([
                    'companyprofile'
                ]);
            } else {
				$allposition = Position::find()->where([
						"IsDelete" => 0
					])
						->orderBy([
						'Position' => 'SORT_ASC'
					])
						->all();
				$allindustry = Industry::getIndustryLists();
				
                return $this->render('companyprofile', [
                    'model' => $model,
                    'totalactive' => $totalactive,
                    'allskill' => $allskill,
                    'skills' => $skill,
                    'wallpost' => $wallpost,
                    'allpost' => $allpost,
                    'comment' => $comment,
                    'suggested' => $suggested,
                    'leavecomment' => $leavecomment,
					'industry' => $allindustry,
					'roles' => $allposition
                ]);
            }
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    // campus profile
    public function actionCampusprofile()
    {
        if (isset(Yii::$app->session['Campusid'])) {

            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            $this->layout = 'walllayout';
            $campus = AllUser::find()->where([
                'UserId' => Yii::$app->session['Campusid']
            ])->one();
            $totalactive = CampusPost::find()->where([
                'CampusId' => Yii::$app->session['Campusid'],
                'JobStatus' => 0
            ])->count();

            $allskill = CampusPost::find()->where([
                'CampusId' => Yii::$app->session['Campusid'],
                'JobStatus' => 0,
                'IsDelete' => 0
            ])->all();
            $skill = array();
            foreach ($allskill as $key => $value) {
                foreach ($value->jobRelatedSkills as $k => $v) {
                    $skill[$v->SkillId] = $v->skill->Skill;
                }
            }
            $flist = Likes::find()->select('LikeTo')
                ->where([
                'LikeFrom' => Yii::$app->session['Campusid']
            ])
                ->all();
            $fl = array();
            foreach ($flist as $k => $v) {
                $fl[] = $v->LikeTo;
            }

            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'EmployerId' => $fl
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Campus'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();
            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();
            $comment = new CommentToPost();
            if ($comment->load(Yii::$app->request->post())) {
                $comment->OnDate = date('Y-m-d H:i:s');
                if ($comment->save()) {
                    // Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    // var_dump($comment->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
                return $this->redirect([
                    'campusprofile'
                ]);
            } else {
                return $this->render('campusprofile', [
                    'campus' => $campus,
                    'totalactive' => $totalactive,
                    'skills' => $skill,
                    'allpost' => $allpost,
                    'comment' => $comment,
                    'recentjob' => $recentjob,
                    'topcompany' => $topcompanyjob
                ]);
            }
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionSearchcampuspost()
    {
        $flist = Likes::find()->select('LikeTo')
            ->where([
            'LikeFrom' => Yii::$app->session['Campusid']
        ])
            ->all();
        $fl = array();
        foreach ($flist as $k => $v) {
            $fl[] = $v->LikeTo;
        }

        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => $fl
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all();
        $this->layout = 'blanklayout';
        return $this->render('campusprofileallpost', [
            'allpost' => $allpost
        ]);
    }

    // search
    public function actionSearch()
    {
        $data = [];
        $data['status'] = 'NOK';
        $term = Yii::$app->request->get('q');
        $model = AllUser::find()->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'Name',
            $term
        ])->all();
			
	    $sklist=[]; 
        $roleList=[];
        $skillUserId=[];
	    $roleUserId=[];
	
        
         if(isset(Yii::$app->session['Employerid'])){
             //Search SKIL name
            $skilllist = Skill::find()->where(['like','Skill', $term ])->all();
    
            foreach ($skilllist as $sk => $sv) {
                $sklist[] = $sv->SkillId;
            }
        
            //Set UserID 
            if(!empty($sklist)){
                 $employerSkill = ArrayHelper::map(EmployeeSkill::find()->where(['in', 'SkillId', $sklist])->all(), 'UserId', 'UserId');
                 foreach ($employerSkill as $key=>$items){
        		    $skillUserId[]=$items;
        		}
            }
            //Role
              $allrole = Position::find()->where([ 'IsDelete' => 0])->andWhere([ 'LIKE','Position',$term])->all();
              foreach ($allrole as $key => $value) {
                  $roleList[]=$value->PositionId;
              }
               
               
            if(!empty($roleList)){
             $employersRole = ArrayHelper::map(EmployeeSkill::find()->where(['in', 'SkillRoleId', $roleList])->all(), 'UserId', 'UserId');
             foreach ($employersRole as $key=>$items){
    		    $roleUserId[]=$items;
    		}
            }
        }
		
// 		echo "<pre>";
// 		print_r($roleUserId);
// 		exit();
		
		
		if(!empty($roleUserId)){
		    	$model1 = AllUser::find()->where(['IsDelete' => 0])->andWhere(['LIKE','CollegeName',$term])->orWhere(['in', 'UserId', $roleUserId])->all();	
		}else if(!empty($skillUserId)){
			    $model1 = AllUser::find()->where(['IsDelete' => 0])->andWhere(['LIKE','CollegeName',$term])->orWhere(['in', 'UserId', $skillUserId])->all();	
		}else{
		    $model1 = AllUser::find()->where(['IsDelete' => 0])->andWhere(['LIKE','CollegeName',$term])->all();	
		}
	
// 	$model1 = AllUser::find()->where([
//             'IsDelete' => 0
//         ])
// 		->andWhere([
//             'LIKE',
//             'CollegeName',
//             $term])->orWhere(['in', 'UserId', $roleUserId])->all();	
        
        // echo "<pre>";
        // print_r($model1);
        // exit();
        
        $url = '/backend/web/';
        $data = array();
		$data1 = array();
		$result  = array();
        if (! empty($model)) {
            $result['status'] = 'OK';
            foreach ($model as $key => $value) {
                if ($value->UserTypeId == AllUser::TYPE_COMPANY) {
                    $img = Documents::getImageByAttr($value, 'LogoId', 'logo');
                } else {
                    $img = Documents::getImageByAttr($value, 'PhotoId', 'photo');
                }

                $url = '/wall/itsearch?term=' . $value->UserId;
                $url = preg_replace('/\s+/', '', $url);
                if ($value->UserTypeId != AllUser::TYPE_CANDIDATE) {
                    $emp_name = preg_replace('/\s+/', '', $value->Name);
                    $url = Yii::$app->urlManager->createAbsoluteUrl('') . $emp_name . "-" . $value->UserId;
                }

				if ($value->UserTypeId == AllUser::TYPE_CAMPUS) {
					$value->Name =  $value->CollegeName;
					$img = Documents::getImageByAttr($value, 'LogoId', 'logo');
				}
				
				
                $data[] = [
                    'id' => $value->UserId,
                    'name' => $value->Name,
                    'image' => $img,
                    'location' => $value->City,
                    'baseUrl' => $url
                ];
            }
        }
		
		if (! empty($model1)) {
            $result['status'] = 'OK';
            foreach ($model1 as $key => $value) {
                if ($value->UserTypeId == AllUser::TYPE_COMPANY) {
                    $img = Documents::getImageByAttr($value, 'LogoId', 'logo');
                } else {
                    $img = Documents::getImageByAttr($value, 'PhotoId', 'photo');
                }

                $url = '/wall/searchcandidate?userid=' . $value->UserId;
                $url = preg_replace('/\s+/', '', $url);
                if ($value->UserTypeId != AllUser::TYPE_CANDIDATE) {
                    $emp_name = preg_replace('/\s+/', '', $value->Name);
                    $url = Yii::$app->urlManager->createAbsoluteUrl('') . $emp_name . "-" . $value->UserId;
                }
				
				if ($value->UserTypeId == AllUser::TYPE_CAMPUS) {
					$value->Name =  $value->CollegeName;
					$img = Documents::getImageByAttr($value, 'LogoId', 'logo');
				}
				
				
                $data1[] = [
                    'id' => $value->UserId,
                    'name' => $value->Name,
                    'image' => $img,
                    'location' => $value->City,
                    'baseUrl' => $url
                ];
            }
        }
		
		$result['results'] = array_merge($data, $data1);
		
        return $this->asJson($result);
    }

    public function actionItsearch()
    {
        $term = Yii::$app->request->get()['term'];
        $model = AllUser::find()->where([
            'IsDelete' => 0,
            'UserId' => $term
        ])->one();
        
        $userid = $model->UserId;
        
        $viewLimit='';
        if (isset(Yii::$app->session['Employerid'])){

            $employerData = AllUser::find()->where(['UserId' => Yii::$app->session['Employerid']])->one();
            $viewLimit=$employerData->viewlimit;
            $oldJsonViewProfileData=$employerData->viewProfile;
            $previousSeenProfile='';
            
            if(!empty($employerData->viewProfile)){
              $viewProfileArray = explode(',', $oldJsonViewProfileData);
              if (in_array($userid, $viewProfileArray)){
                $previousSeenProfile=1;
                  return $this->redirect(['/wall/searchcandidate','userid' => $model->UserId]);
                
                   //Yii::$app->session->setFlash('success', 'You have already seen this profile. Please check another profile.');
              }
            }
        
        if($viewLimit>0){
            if(empty($previousSeenProfile)){
                $updateModel=AllUser::findOne($employerData->UserId);
                $updateModel->viewlimit=$viewLimit-1;
                if($oldJsonViewProfileData){
                     $updateModel->viewProfile .=','.$userid;
                }else{
                     $updateModel->viewProfile =$userid;
                }
               
                $updateModel->save();
            }
        
            if ($model->UserTypeId == 2) {
                return $this->redirect([
                    'searchcandidate',
                    'userid' => $userid
                ]);
            } elseif ($model->UserTypeId == 3) {
                return $this->redirect([
                    'searchcompany',
                    'userid' => $userid
                ]);
            } elseif ($model->UserTypeId == 4) {
                return $this->redirect([
                    'searchcampus',
                    'userid' => $userid
                ]);
            } elseif ($model->UserTypeId == 5) {
                return $this->redirect([
                    'searchteam',
                    'userid' => $userid
                ]);
            }
                
            }else{
                if (isset($_REQUEST['q'])) {
                    $term = Yii::$app->request->get()['q'];
                    return $this->redirect(['/wall/search-company-index', 'q' => $term]);
                }else{
                    return $this->redirect(['/wall/search-company-index', 'q' => $model->Name]);
                }
            }
        
         }else{
                if ($model->UserTypeId == 2) {
                    return $this->redirect([
                        'searchcandidate',
                        'userid' => $userid
                    ]);
                } elseif ($model->UserTypeId == 3) {
                    return $this->redirect([
                        'searchcompany',
                        'userid' => $userid
                    ]);
                } elseif ($model->UserTypeId == 4) {
                    return $this->redirect([
                        'searchcampus',
                        'userid' => $userid
                    ]);
                } elseif ($model->UserTypeId == 5) {
                    return $this->redirect([
                        'searchteam',
                        'userid' => $userid
                    ]);
                }
         }
    }

    public function actionSearchteam()
    {
        $userid = Yii::$app->request->get()['userid'];
        $profile = AllUser::find()->where([
            'IsDelete' => 0,
            'UserId' => $userid
        ])->one();
        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
             ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
             ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }

        $recentjob = PostJob::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Candidate'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        $university = $profile->educations[0]->University;
        $peopleyouknow = array();
        if ($university) {
            $peopleyouknow = AllUser::find()->where([
                'like',
                'Education.University',
                $university
            ])
                ->andWhere([
                'UserTypeId' => 2,
                'AllUser.IsDelete' => 0
            ])
                ->andWhere([
                '!=',
                'AllUser.UserId',
                $userid
            ])
                ->joinWith([
                'educations'
            ])
                ->all();
        }
        $topcompanyjob = AllUser::find()->where([
            'UserTypeId' => 3,
            'IsDelete' => 0
        ])
            ->limit(5)
            ->all();
        $this->layout = 'walllayout';

        $leavecomment = new LeaveComment();
        $message = new Messages();
        if ($leavecomment->load(Yii::$app->request->post())) {
            $leavecomment->OnDate = date('Y-m-d H:i:s');
            $leavecomment->CommentFrom = $empid;
            $leavecomment->CommentTo = $userid;
            if ($leavecomment->save()) {
                Yii::$app->session->setFlash('success', 'Comment posted successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchteam',
                'userid' => Yii::$app->request->get()['userid'],
                'lc' => 1
            ]);
        } elseif ($message->load(Yii::$app->request->post())) {
            $message->OnDate = date('Y-m-d H:i:s');
            $message->MessageFrom = $empid;
            $message->MessageTo = $userid;
            $message->IsDelete = 0;
            $message->IsView = 0;
            if ($message->save()) {
                Yii::$app->session->setFlash('success', 'Message sent successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchteam',
                'userid' => Yii::$app->request->get()['userid']
            ]);
        } else {
            return $this->render('searchteam', [
                'profile' => $profile,
                'recentjob' => $recentjob,
                'peopleyouknow' => $peopleyouknow,
                'topcompany' => $topcompanyjob,
                'empid' => $empid,
                'leavecomment' => $leavecomment,
                'message' => $message
            ]);
        }
    }

    public function actionSearchcampus()
    {
        $userid = Yii::$app->request->get()['userid'];
        $profile = AllUser::find()->where([
            'IsDelete' => 0,
            'UserId' => $userid
        ])->one();
        // footer section

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
             ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
             ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }

        $totalactive = CampusPost::find()->where([
            'CampusId' => Yii::$app->session['Campusid'],
            'JobStatus' => 0
        ])->count();

        $allskill = CampusPost::find()->where([
            'CampusId' => Yii::$app->session['Campusid'],
            'JobStatus' => 0,
            'IsDelete' => 0
        ])->all();
        $skill = array();
        foreach ($allskill as $key => $value) {
            foreach ($value->jobRelatedSkills as $k => $v) {
                $skill[$v->SkillId] = $v->skill->Skill;
            }
        }
        // $allpost=CompanyWallPost::find()->where(['IsDelete'=>0])->all();
        $recentjob = PostJob::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'Jobstatus' => 0,
            'PostFor' => 'Campus'
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();
        $topcompanyjob = AllUser::find()->where([
            'UserTypeId' => 3,
            'IsDelete' => 0
        ])
            ->limit(5)
            ->all();

        $leavecomment = new LeaveComment();
        $message = new Messages();
        if ($leavecomment->load(Yii::$app->request->post())) {
            $leavecomment->OnDate = date('Y-m-d H:i:s');
            $leavecomment->CommentFrom = $empid;
            $leavecomment->CommentTo = $userid;
            if ($leavecomment->save()) {
                Yii::$app->session->setFlash('success', 'Comment posted successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchcampus',
                'userid' => Yii::$app->request->get()['userid'],
                'lc' => 1
            ]);
        } elseif ($message->load(Yii::$app->request->post())) {
            $message->OnDate = date('Y-m-d H:i:s');
            $message->MessageFrom = $empid;
            $message->MessageTo = $userid;
            $message->IsDelete = 0;
            $message->IsView = 0;
            if ($message->save()) {
                Yii::$app->session->setFlash('success', 'Message sent successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchcampus',
                'userid' => Yii::$app->request->get()['userid']
            ]);
        } else {
            return $this->render('searchcampus', [
                'campus' => $profile,
                'empid' => $empid,
                'skills' => $skill,
                'totalactive' => $totalactive,
                'recentjob' => $recentjob,
                'topcompany' => $topcompanyjob,
                'leavecomment' => $leavecomment,
                'message' => $message
            ]);
        }
    }

    public function actionSearchcompany()
    {
        $userid = Yii::$app->request->get()['userid'];
        $profile = AllUser::find()->where([
            'IsDelete' => 0,
            'UserId' => $userid
        ])->one();
		
		$allposition = Position::find()->where([
            "IsDelete" => 0
        ])
            ->orderBy([
            'Position' => 'SORT_ASC'
        ])
            ->all();
        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
            ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
            ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }

        $totalactive = PostJob::find()->where([
            'EmployerId' => $userid,
            'IsHide' => 0,
            'JobStatus' => 0,
            'IsDelete' => 0,
            //'PostFor' => 'Candidate'
        ])->count();
        $allskill = PostJob::find()->where([
            'EmployerId' => $userid,
            'IsHide' => 0,
            'JobStatus' => 0,
            'IsDelete' => 0
        ])->all();
		
		$allindustry = ArrayHelper::map(Industry::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'IndustryName' => 'SORT_ASC'
        ])
            ->all(), 'IndustryId', 'IndustryName');
		
		
		
		
        $skill = array();
        foreach ($allskill as $key => $value) {
            foreach ($value->jobRelatedSkills as $k => $v) {
                $skill[$v->SkillId] = $v->skill->Skill;
            }
        }
        $suggested = EmpNotification::find()->where([
            'EmpId' => $userid
        ])
            ->groupBy([
            'Userid'
        ])
            ->all();

        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => $userid
        ])->orderBy([
            'OnDate' => SORT_DESC
        ]);
		
		$wallPostCount =  CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
			'EmployerId' => $userid
        ])->count();

        $leavecomment = new LeaveComment();
        $message = new Messages();
        $comment = new CommentToPost();
		 $wallpost = new CompanyWallPost();
		if (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
            $date = date('Y-m-d');
            $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
            // var_dump(Yii::$app->request->post('CompanyWallPost'));
            // die();
            $totaljob = CompanyWallPost::find()->where([
                'WallPostId' => $id
            ])->one();
			
            $docmodel = new Documents();
            if ($totaljob->load(\Yii::$app->request->post())) {
                $totaljob->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
                // $totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];

                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $docmodel = new Documents();
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                    if ($image_id > 0) {
                        Documents::removeImage($totaljob->ImageId);
                    }
                } else {
                    $image_id = 0;
                }
                $totaljob->ImageId = $image_id;
            }			
			
			if(!empty(Yii::$app->request->post('q'))){
				$totaljob->Locationat = Yii::$app->request->post('q');
			}
			
			/*Create Slug*/
			if(empty($totaljob->Slug)){
				$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
				$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
				$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
				$slug = preg_replace('~[^-\w]+~', '', $slug);
				$slug = trim($slug, '-');
				$slug = preg_replace('~-+~', '-', $slug);
				$slug = strtolower($slug);
				
				/*check slug*/
				$count = CompanyWallPost::find()
							->where([
								'Slug' => $slug
							])
							->count();
				if($count > 0){
					$slug = $slug.'-'.rand(9, 999);
				}						
				$totaljob->Slug = $slug;
			}

            if ($totaljob->save()) {
                Yii::$app->session->setFlash('success', 'Post Successfully Updated');
            } else {
                // var_dump($wallpost->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }    
			
			return $this->redirect([
                'searchcompany',
                'userid' => Yii::$app->request->get()['userid']
            ]);
        }elseif ($wallpost->load(Yii::$app->request->post())) {
            $date = date('Y-m-d');
            $totaljob = CompanyWallPost::find()->where([
                'IsHide' => 0,
                'EmployerId' => Yii::$app->session['Employerid'],
                'date(OnDate)' => $date
            ])->count();
            $Wallpostsetting = Wallpostsetting::find()->where([
                'companyId' => Yii::$app->session['Employerid']
            ])->one();
            $Wallpostsettingcount = (isset($Wallpostsetting->wallPostSetting)) ? $Wallpostsetting->wallPostSetting : 24;

            $artt = Yii::$app->request->post('CompanyWallPost')['Locationat'];
			$Wallpostsettingcount = 100;
            if ($totaljob < $Wallpostsettingcount) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                } else {
                    $image_id = 0;
                }

                $wallpost->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
				
				/*Create Slug*/
				$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
				$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
				$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
				$slug = preg_replace('~[^-\w]+~', '', $slug);
				$slug = trim($slug, '-');
				$slug = preg_replace('~-+~', '-', $slug);
				$slug = strtolower($slug);
				
				/*check slug*/
				$count = CompanyWallPost::find()
							->where([
								'Slug' => $slug
							])
							->count();
				if($count > 0){
					$slug = $slug.'-'.rand(9, 999);
				}

                $locations= Yii::$app->request->post('CompanyWallPost')['Locationat'];
                $locationsList=implode(", ", $locations);

                $wallpost->Slug = $slug;
                $wallpost->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
                $wallpost->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
                $wallpost->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
                $wallpost->Locationat = $locationsList;
                $wallpost->ImageId = $image_id;
                $wallpost->VideoId = 0;
                $wallpost->EmployerId = Yii::$app->session['Employerid'];
                $wallpost->OnDate = date('Y-m-d H:i:s');
				
                if ($wallpost->save()) {
                    Notification::createNotificationToRoleUser($wallpost);
                    Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    // var_dump($wallpost->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Your post limit exceed, Please Contact admin');
            }	

			return $this->redirect([
                'searchcompany',
                'userid' => Yii::$app->request->get()['userid']
            ]);
		
		} elseif($leavecomment->load(Yii::$app->request->post())) {
            $leavecomment->OnDate = date('Y-m-d H:i:s');
            $leavecomment->CommentFrom = $empid;
            $leavecomment->CommentTo = $userid;
            if ($leavecomment->save()) {
                Yii::$app->session->setFlash('success', 'Comment posted successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchcompany',
                'userid' => Yii::$app->request->get()['userid'],
                'lc' => 1
            ]);
        } elseif ($message->load(Yii::$app->request->post())) {
            $message->OnDate = date('Y-m-d H:i:s');
            $message->MessageFrom = $empid;
            $message->MessageTo = $userid;
            $message->IsDelete = 0;
            $message->IsView = 0;
            if ($message->save()) {
                Yii::$app->session->setFlash('success', 'Message sent successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchcompany',
                'userid' => Yii::$app->request->get()['userid']
            ]);
        } elseif ($comment->load(Yii::$app->request->post())) {
            $comment->OnDate = date('Y-m-d H:i:s');
            if ($comment->save()) {
                // Yii::$app->session->setFlash('success', 'Posted Successfully');
            } else {

                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchcompany',
                'userid' => Yii::$app->request->get()['userid']
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => $allpost
            ]);
            
              $mailmodel = new Mail();
            $dataProvider = new ActiveDataProvider([
                'query' => $allpost,
                'pagination' => [
                    'pageSize' => 50
                ]
            ]);
            
            return $this->render('searchcompany', [
                'model' => $profile,
                'mailmodel' => $mailmodel,
                'empid' => $empid,
                'totalactive' => $totalactive,
                'skills' => $skill,
                'suggested' => $suggested,
                'allpost' => $allpost,
                'leavecomment' => $leavecomment,
                'message' => $message,
                'comment' => $comment,
                'dataProvider' => $dataProvider,
				'postCount' => $wallPostCount,
				'industry' => $allindustry,
				'roles' => $allposition
            ]);
        }
    }
	

    public function actionSearchCompanyIndex()
    {
        // notification for employer
        $allntemp = array();
        $viewLimit=0;
        $oldJsonViewdData='';
        if (isset(Yii::$app->session['Employerid'])) {
            $employerData  = AllUser::find()->where(['UserId' => Yii::$app->session['Employerid']])->one();
            $viewLimit     =$employerData->viewlimit;
            $oldJsonViewdData=$employerData->viewProfile;
          
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
            ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
               // 'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
            ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        $searchResult = AllUser::getSearchResult();
        return $this->render('searchCompanyIndex', [
            'searchResult' => $searchResult,
            'viewLimit' => $viewLimit,
            'oldJsonViewdData'=>$oldJsonViewdData
        ]);
    }

    public function actionSearchcompanypost()
    {
        $userid = Yii::$app->request->get()['userid'];
        $empid = Yii::$app->request->get()['empid'];
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => $userid
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all();
        $this->layout = 'blanklayout';
        return $this->render('searchcompanypost', [
            'allpost' => $allpost,
            'empid' => $empid,
            'userid' => $userid
        ]);
    }

    public function actionSearchcandidate()
    {
        $userid = Yii::$app->request->get()['userid'];
        $profile = AllUser::find()->where([
            'IsDelete' => 0,
            'UserId' => $userid
        ])->one();
        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
            ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
				])
              ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $university = is_null($profile->educations[0]->University) ? '' : $profile->educations[0]->University;

        $peopleyouknow = AllUser::getPeopleYouMayKnow($university, $userid);
        AllUser::find()->where([
            'like',
            'Education.University',
            $university
        ])
            ->andWhere([
            'UserTypeId' => 2,
            'AllUser.IsDelete' => 0
        ])
            ->andWhere([
            '!=',
            'AllUser.UserId',
            $userid
        ])
            ->joinWith([
            'educations'
        ])
            ->all();
        $topcompanyjob = AllUser::getTopCompany();
        $this->layout = 'walllayout';

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }

        $leavecomment = new LeaveComment();
        $message = new Messages();
        if ($leavecomment->load(Yii::$app->request->post())) {
          
            $url=str_replace(' ','',$profile->Name).'-'.$profile->UserId;
            
            $leavecomment->OnDate = date('Y-m-d H:i:s');
            $leavecomment->CommentFrom = $empid;
            $leavecomment->CommentTo = $userid;
            if ($leavecomment->save()) {
                Yii::$app->session->setFlash('success', 'Comment posted successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            
            if(isset(Yii::$app->session['Employerid'])){
                 return $this->redirect([
                'searchcandidate',
                'userid' => Yii::$app->request->get()['userid'],
                'lc' => 1
            ]);
            }else{
            return $this->redirect('/'.$url);
                
            }
            
        } elseif ($message->load(Yii::$app->request->post())) {
            $message->OnDate = date('Y-m-d H:i:s');
            $message->MessageFrom = $empid;
            $message->MessageTo = $userid;
            $message->IsDelete = 0;
            $message->IsView = 0;
            if ($message->save()) {
                Yii::$app->session->setFlash('success', 'Message sent successfully');
            } else {
                // var_dump($comment->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'searchcandidate',
                'userid' => Yii::$app->request->get()['userid']
            ]);
        } else {
            return $this->render('searchcandidate', [
                'profile' => $profile,
                'recentjob' => $recentjob,
                'peopleyouknow' => $peopleyouknow,
                'topcompany' => $topcompanyjob,
                'empid' => $empid,
                'leavecomment' => $leavecomment,
                'message' => $message
            ]);
        }
    }

    public function actionFollow($userid)
    {
        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }

        $likes = new Likes();
        $cnt = $likes->find()
            ->where([
            'LikeFrom' => $empid,
            'LikeTo' => $userid
        ])
            ->one();
        if ($cnt) {
            $cnt->IsLike = 1;
            $cnt->save();
        } else {
            $likes->LikeFrom = $empid;
            $likes->LikeTo = $userid;
            $likes->IsLike = 1;
            $likes->save();
        }

        echo json_encode(1);
    }

    public function actionUnfollow($userid)
    {
        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }

        $likes = Likes::find()->where([
            'LikeFrom' => $empid,
            'LikeTo' => $userid
        ])->one();
        $likes->IsLike = 0;
        $likes->save();

        echo json_encode(1);
    }

    // // career bug wall
    // public function actionMcbwallpost()
    // {

    // // Role
    // $allposition = Position::find()->where("IsDelete=0")
    // ->orderBy([
    // 'Position' => 'SORT_ASC'
    // ])
    // ->all();
    // $allindustry = ArrayHelper::map(Industry::find()->where([
    // 'IsDelete' => 0
    // ])
    // ->orderBy([
    // 'IndustryName' => 'SORT_ASC'
    // ])
    // ->all(), 'IndustryId', 'IndustryName');
    // // footer section

    // // first block
    // $about = new FooterAboutus();
    // $footerabout = $about->find()->one();
    // Yii::$app->view->params['footerabout'] = $footerabout;

    // // contactus block
    // $cn = new FooterContactus();
    // $footercontact = $cn->find()->one();
    // Yii::$app->view->params['footercontact'] = $footercontact;

    // // second block
    // $jobcategory = new JobCategory();
    // $allhotcategory = $jobcategory->find()
    // ->select([
    // 'CategoryName',
    // 'JobCategoryId'
    // ])
    // ->where([
    // 'IsDelete' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->limit(7)
    // ->all();
    // Yii::$app->view->params['hotcategory'] = $allhotcategory;

    // // copyright block
    // $cp = new FooterCopyright();
    // $allcp = $cp->find()->one();
    // Yii::$app->view->params['footercopyright'] = $allcp;

    // // developer block
    // $dblock = new FooterDevelopedblock();
    // $developerblock = $dblock->find()->one();
    // Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

    // // socialicon block
    // $socialicon = new SocialIcon();
    // $sicon = $socialicon->find()->one();
    // Yii::$app->view->params['footersocialicon'] = $sicon;

    // // third block
    // $th = new FooterThirdBlock();
    // $thirdblock = $th->find()->one();
    // Yii::$app->view->params['footerthirdblock'] = $thirdblock;

    // // notification for employer
    // $allntemp = array();
    // if (isset(Yii::$app->session['Employerid'])) {

    // $allntemp = EmpNotification::find()->where([
    // 'EmpId' => Yii::$app->session['Employerid'],
    // 'IsView' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // }
    // Yii::$app->view->params['employernotification'] = $allntemp;
    // // notification end
    // // notification for employee
    // $allnotification = array();
    // if (isset(Yii::$app->session['Employeeid'])) {
    // $notification = new Notification();

    // $allnotification = $notification->find()
    // ->where([
    // 'UserId' => Yii::$app->session['Employeeid'],
    // 'IsView' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // }
    // Yii::$app->view->params['employeenotification'] = $allnotification;

    // $this->layout = 'walllayout';

    // $allmypost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0,
    // 'EmployerId' => Yii::$app->session['Employerid']
    // ])->all();

    // $recentjob = array();
    // $peopleyouknow = array();
    // $topcompanyjob = array();
    // $suggested = array();

    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();

    // $user = new AllUser();

    // if (isset(Yii::$app->session['Employeeid'])) {
    // $empid = Yii::$app->session['Employeeid'];
    // $profile = AllUser::find()->where([
    // 'UserId' => $empid
    // ])->one();
    // $recentjob = PostJob::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0,
    // 'Jobstatus' => 0,
    // 'PostFor' => 'Candidate'
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->limit(5)
    // ->all();
    // $university = $profile->educations[0]->University;
    // $peopleyouknow = AllUser::find()->where([
    // 'like',
    // 'Education.University',
    // $university
    // ])
    // ->andWhere([
    // 'UserTypeId' => 2,
    // 'AllUser.IsDelete' => 0
    // ])
    // ->andWhere([
    // '!=',
    // 'AllUser.UserId',
    // $empid
    // ])
    // ->joinWith([
    // 'educations'
    // ])
    // ->all();

    // $topcompanyjob = AllUser::find()->where([
    // 'UserTypeId' => 3,
    // 'IsDelete' => 0
    // ])
    // ->limit(5)
    // ->all();

    // $user = AllUser::find()->where([
    // 'UserId' => Yii::$app->session['Employeeid']
    // ])->one();
    // Yii::$app->session['usrtidsa'] = $user->UserId;
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // if ($user->PreferredLocation != '') {
    // $ctys = explode(",", $user->PreferredLocation);
    // foreach ($ctys as $cty) {
    // $cties = Cities::find()->where([
    // 'CityId' => $cty
    // ])->one();
    // $ctyiess[] = $cties->CityName;
    // }
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->where([
    // 'in',
    // 'Locationat',
    // $ctyiess
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // }
    // } elseif (isset(Yii::$app->session['Employerid'])) {
    // $empid = Yii::$app->session['Employerid'];
    // $profile = AllUser::find()->where([
    // 'UserId' => $empid,
    // 'IsDelete' => 0
    // ])->one();
    // $suggested = EmpNotification::find()->where([
    // 'EmpId' => $empid
    // ])
    // ->groupBy([
    // 'Userid'
    // ])
    // ->all();
    // $user = AllUser::find()->where([
    // 'UserId' => Yii::$app->session['Employerid']
    // ])->one();
    // Yii::$app->session['usrtidsa'] = $user->UserId;
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // if ($user->PreferredLocation != '') {
    // $ctys = explode(",", $user->PreferredLocation);
    // foreach ($ctys as $cty) {
    // $cties = Cities::find()->where([
    // 'CityId' => $cty
    // ])->one();
    // $ctyiess[] = $cties->CityName;
    // }
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->where([
    // 'in',
    // 'Locationat',
    // $ctyiess
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // }
    // } elseif (Yii::$app->session['Campusid']) {
    // $empid = Yii::$app->session['Campusid'];
    // $profile = AllUser::find()->where([
    // 'UserId' => $empid
    // ])->one();
    // $recentjob = PostJob::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0,
    // 'Jobstatus' => 0,
    // 'PostFor' => 'Campus'
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->limit(5)
    // ->all();

    // $topcompanyjob = AllUser::find()->where([
    // 'UserTypeId' => 3,
    // 'IsDelete' => 0
    // ])
    // ->limit(5)
    // ->all();
    // $user = AllUser::find()->where([
    // 'UserId' => Yii::$app->session['Campusid']
    // ])->one();
    // Yii::$app->session['usrtidsa'] = $user->UserId;
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // if ($user->PreferredLocation != '') {
    // $ctys = explode(",", $user->PreferredLocation);
    // $ctyiess = [];
    // foreach ($ctys as $cty) {
    // $cties = Cities::find()->where([
    // 'CityId' => $cty
    // ])->one();
    // $ctyiess[] = $cties->CityName;
    // }
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->where([
    // 'in',
    // 'Locationat',
    // $ctyiess
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // }
    // } elseif (Yii::$app->session['Teamid']) {
    // $empid = Yii::$app->session['Teamid'];

    // $profile = AllUser::find()->where([
    // 'UserId' => $empid
    // ])->one();
    // $recentjob = PostJob::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0,
    // 'Jobstatus' => 0,
    // 'PostFor' => 'Candidate'
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->limit(5)
    // ->all();
    // $university = $profile->educations[0]->University;
    // $peopleyouknow = AllUser::find()->where([
    // 'like',
    // 'Education.University',
    // $university
    // ])
    // ->andWhere([
    // 'UserTypeId' => 2,
    // 'AllUser.IsDelete' => 0
    // ])
    // ->andWhere([
    // '!=',
    // 'AllUser.UserId',
    // $empid
    // ])
    // ->joinWith([
    // 'educations'
    // ])
    // ->all();

    // $topcompanyjob = AllUser::find()->where([
    // 'UserTypeId' => 3,
    // 'IsDelete' => 0
    // ])
    // ->limit(5)
    // ->all();
    // $user = AllUser::find()->where([
    // 'UserId' => Yii::$app->session['Teamid']
    // ])->one();
    // Yii::$app->session['usrtidsa'] = $user->UserId;
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // if ($user->PreferredLocation != '') {
    // $ctys = explode(",", $user->PreferredLocation);
    // foreach ($ctys as $cty) {
    // $cties = Cities::find()->where([
    // 'CityId' => $cty
    // ])->one();
    // $ctyiess[] = $cties->CityName;
    // }
    // $allpost = CompanyWallPost::find()->where([
    // 'IsDelete' => 0,
    // 'IsHide' => 0
    // ])
    // ->where([
    // 'in',
    // 'Locationat',
    // $ctyiess
    // ])
    // ->orderBy([
    // 'OnDate' => SORT_DESC
    // ])
    // ->all();
    // }
    // } else {
    // return $this->redirect([
    // 'index'
    // ]);
    // }

    // $wallpost = new CompanyWallPost();
    // $comment = new CommentToPost();

    // if (isset(Yii::$app->request->post('CommentToPost')['CommentToPostId'])) {
    // $id = Yii::$app->request->post('CommentToPost')['CommentToPostId'];
    // $cmntcnt = $comment->find()
    // ->where([
    // 'CommentToPostId' => $id
    // ])
    // ->one();
    // $cmntcnt->Message = Yii::$app->request->post('CommentToPost')['Message'];
    // $cmntcnt->save();
    // return $this->redirect([
    // 'mcbwallpost'
    // ]);
    // } elseif ($comment->load(Yii::$app->request->post())) {
    // $comment->OnDate = date('Y-m-d H:i:s');
    // if ($comment->save()) {
    // // Yii::$app->session->setFlash('success', 'Posted Successfully');
    // } else {
    // Yii::$app->session->setFlash('error', 'There is some error please try again.');
    // }
    // return $this->redirect([
    // 'mcbwallpost'
    // ]);
    // } elseif (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
    // $date = date('Y-m-d');
    // $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
    // // var_dump(Yii::$app->request->post('CompanyWallPost'));
    // $totaljob = CompanyWallPost::find()->where([
    // 'WallPostId' => $id
    // ])->one();

    // $docmodel = new Documents();

    // $totaljob->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
    // $totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];

    // if ($totaljob->save()) {
    // // Yii::$app->session->setFlash('success', 'Post edited Successfully');
    // } else {
    // // var_dump($wallpost->getErrors());
    // Yii::$app->session->setFlash('error', 'There is some error please try again.');
    // }

    // return $this->redirect([
    // 'mcbwallpost'
    // ]);
    // } elseif ($wallpost->load(Yii::$app->request->post())) {
    // $date = date('Y-m-d');
    // $totaljob = CompanyWallPost::find()->where([
    // 'IsHide' => 0,
    // 'EmployerId' => Yii::$app->session['Employerid'],
    // 'date(OnDate)' => $date
    // ])->count();
    // $Wallpostsetting = Wallpostsetting::find()->where([
    // 'companyId' => Yii::$app->session['Employerid']
    // ])->one();
    // $Wallpostsettingcount = (isset($Wallpostsetting->wallPostSetting)) ? $Wallpostsetting->wallPostSetting : 24;

    // $artt = Yii::$app->request->post('CompanyWallPost')['Locationat'];

    // if ($totaljob < $Wallpostsettingcount) {
    // $docmodel = new Documents();
    // $image = UploadedFile::getInstance($wallpost, 'ImageId');
    // if ($image) {
    // $image_id = $docmodel->imageUpload($image, 'Wallpost');
    // } else {
    // $image_id = 0;
    // }

    // $wallpost->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
    // $wallpost->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
    // $wallpost->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
    // $wallpost->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];
    // $wallpost->ImageId = $image_id;
    // $wallpost->VideoId = 0;
    // $wallpost->EmployerId = Yii::$app->session['Employerid'];
    // $wallpost->OnDate = date('Y-m-d H:I:S');

    // if ($wallpost->save()) {
    // Notification::createNotificationToRoleUser($wallpost);
    // Yii::$app->session->setFlash('success', 'Posted Successfully');
    // } else {
    // // var_dump($wallpost->getErrors());
    // Yii::$app->session->setFlash('error', 'There is some error please try again.');
    // }
    // } else {
    // Yii::$app->session->setFlash('error', 'Your post limit exceed, Please Contact admin');
    // }

    // return $this->redirect([
    // 'mcbwallpost'
    // ]);
    // } else {
    // $mailmodel = new Mail();
    // return $this->render('mcbwallpost', [
    // 'mailmodel' => $mailmodel,
    // 'allpost' => $allpost,
    // 'profile' => $profile,
    // 'recentjob' => $recentjob,
    // 'peopleyouknow' => $peopleyouknow,
    // 'topcompany' => $topcompanyjob,
    // 'allmypost' => $allmypost,
    // 'suggested' => $suggested,
    // 'wallpost' => $wallpost,
    // 'user' => $user,
    // 'roles' => $allposition,
    // 'industry' => $allindustry
    // ]);
    // }
    // }
    
    public function actionWatchlist(){
         $allposition = Position::find()->where([
            "IsDelete" => 0
        ])
            ->orderBy([
            'Position' => 'SORT_ASC'
        ])
            ->all();
        $allindustry = Industry::getIndustryLists();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // notification for employer
        $allntemp = [];
        if (isset(Yii::$app->session['Employerid'])) {
            $allntemp = EmpNotification::getNotifications(Yii::$app->session['Employerid']);
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = [];
        if (isset(Yii::$app->session['Employeeid'])) {
            $allnotification = Notification::getNotifications(Yii::$app->session['Employeeid']);
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        $allmypost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => Yii::$app->session['Employerid']
        ])->all();

        $recentjob = array();
        $peopleyouknow = array();
        $topcompanyjob = array();
        $suggested = array();
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])->orderBy([
            'WallPostId' => SORT_DESC
        ]);
		
		$wallPostCount =  CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])->count();
		
        $user = new AllUser();

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctyiess = [];
                $ctys = explode(",", $user->PreferredLocation);
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ]);
            }
          
            $watchList = new WatchToPost();
            $allWatchList = $watchList->find()->select('PostId')
                ->where([
                'EmpId' =>$empid,
            ])->asArray()->all();
            
            if($allWatchList){
              $filterData=[];
            foreach($allWatchList as $key=>$items){
               $filterData[]=$items['PostId'];
            } 
        
            $allpost = $allpost->andWhere([
                    'in',
                    'WallPostId',
                    $filterData
                ]);
            }
            
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid,
                'IsDelete' => 0
            ])->one();
            $suggested = EmpNotification::find()->where([
                'EmpId' => $empid
            ])
                ->groupBy([
                'Userid'
            ])
                ->all();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employerid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctys = explode(",", $user->PreferredLocation);
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ]);
            }
             $watchList = new WatchToPost();
            $allWatchList = $watchList->find()->select('PostId')
                ->where([
                'EmpId' =>$empid,
            ])->asArray()->all();
            
            if($allWatchList){
              $filterData=[];
            foreach($allWatchList as $key=>$items){
               $filterData[]=$items['PostId'];
            } 
        
            $allpost = $allpost->andWhere([
                    'in',
                    'WallPostId',
                    $filterData
                ]);
            }
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Campus'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();

            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Campusid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctys = explode(",", $user->PreferredLocation);
                $ctyiess = [];
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ]);
            }
            
             $watchList = new WatchToPost();
            $allWatchList = $watchList->find()->select('PostId')
                ->where([
                'EmpId' =>$empid,
            ])->asArray()->all();
            
            if($allWatchList){
              $filterData=[];
            foreach($allWatchList as $key=>$items){
               $filterData[]=$items['PostId'];
            } 
        
            $allpost = $allpost->andWhere([
                    'in',
                    'WallPostId',
                    $filterData
                ]);
            }
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];

            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Candidate'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();

            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctys = explode(",", $user->PreferredLocation);
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ])->orderBy([
                    'WallPostId' => SORT_DESC
                ]);
            }
            
            $watchList = new WatchToPost();
            $allWatchList = $watchList->find()->select('PostId')
                ->where([
                'EmpId' =>$empid,
            ])->asArray()->all();
            
            if($allWatchList){
              $filterData=[];
            foreach($allWatchList as $key=>$items){
               $filterData[]=$items['PostId'];
            } 
        
            $allpost = $allpost->andWhere([
                    'in',
                    'WallPostId',
                    $filterData
                ]);
            }
        } else {
            return $this->redirect([
                'index'
            ]);
        }

        $wallpost = new CompanyWallPost();
        $comment = new CommentToPost();
		
        if (isset(Yii::$app->request->post('CommentToPost')['CommentToPostId'])) {
            $id = Yii::$app->request->post('CommentToPost')['CommentToPostId'];
            $cmntcnt = $comment->find()
                ->where([
                'CommentToPostId' => $id
            ])
                ->one();
            
			
            $cmntcnt->save();
            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif ($comment->load(Yii::$app->request->post())) {
            $comment->OnDate = date('Y-m-d H:i:s');
            if ($comment->save()) {
                // Yii::$app->session->setFlash('success', 'Posted Successfully');
            } else {
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'mcbwallposts'
            ]);
        } elseif (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
            $date = date('Y-m-d');
            $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
            // var_dump(Yii::$app->request->post('CompanyWallPost'));
            // die();
            $totaljob = CompanyWallPost::find()->where([
                'WallPostId' => $id
            ])->one();
			
            $docmodel = new Documents();
            if ($totaljob->load(\Yii::$app->request->post())) {
                $totaljob->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
                // $totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];

                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $docmodel = new Documents();
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                    if ($image_id > 0) {
                        Documents::removeImage($totaljob->ImageId);
                    }
                } else {
                    $image_id = 0;
                }
                $totaljob->ImageId = $image_id;
            }
			
			if(!empty(Yii::$app->request->post('q'))){
				$totaljob->Locationat = Yii::$app->request->post('q');
			}
			
			/*Create Slug*/
			if(empty($totaljob->Slug)){
			$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
			$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
			$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
			$slug = preg_replace('~[^-\w]+~', '', $slug);
			$slug = trim($slug, '-');
			$slug = preg_replace('~-+~', '-', $slug);
			$slug = strtolower($slug);
			
			/*check slug*/
			$count = CompanyWallPost::find()
						->where([
							'Slug' => $slug
						])
						->count();
			if($count > 0){
				$slug = $slug.'-'.rand(9, 999);
			}
						
				$totaljob->Slug = $slug;
			}

            if ($totaljob->save()) {
                Yii::$app->session->setFlash('success', 'Post Successfully Updated');
            } else {
                // var_dump($wallpost->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }

            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif ($wallpost->load(Yii::$app->request->post())) {
            $date = date('Y-m-d');
            $totaljob = CompanyWallPost::find()->where([
                'IsHide' => 0,
                'EmployerId' => Yii::$app->session['Employerid'],
                'date(OnDate)' => $date
            ])->count();
            $Wallpostsetting = Wallpostsetting::find()->where([
                'companyId' => Yii::$app->session['Employerid']
            ])->one();
            $Wallpostsettingcount = (isset($Wallpostsetting->wallPostSetting)) ? $Wallpostsetting->wallPostSetting : 24;

            $artt = Yii::$app->request->post('CompanyWallPost')['Locationat'];
			
            if ($totaljob < $Wallpostsettingcount) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                } else {
                    $image_id = 0;
                }

                $wallpost->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
				
				/*Create Slug*/
				$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
				$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
				$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
				$slug = preg_replace('~[^-\w]+~', '', $slug);
				$slug = trim($slug, '-');
				$slug = preg_replace('~-+~', '-', $slug);
				$slug = strtolower($slug);
				
				/*check slug*/
				$count = CompanyWallPost::find()
							->where([
								'Slug' => $slug
							])
							->count();
				if($count > 0){
					$slug = $slug.'-'.rand(9, 999);
				}
				
				
                $wallpost->Slug = $slug;
                $wallpost->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
                $wallpost->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
                $wallpost->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
                $wallpost->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];
                $wallpost->ImageId = $image_id;
                $wallpost->VideoId = 0;
                $wallpost->EmployerId = Yii::$app->session['Employerid'];
                $wallpost->OnDate = date('Y-m-d H:i:s');

                if ($wallpost->save()) {
                    Notification::createNotificationToRoleUser($wallpost);
                    Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    // var_dump($wallpost->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Your post limit exceed, Please Contact admin');
            }

            return $this->redirect([
                'mcbwallpost'
            ]);
        } else {
            $mailmodel = new Mail();
            $dataProvider = new ActiveDataProvider([
                'query' => $allpost,
                'pagination' => [
                    'pageSize' => 50
                ]
            ]);
            $industry = Industry::getIndustryLists();
            $roles = Position::getRoles();
            $wallpost = new CompanyWallPost();
            
            return $this->render('watch_mcbwallpost', [
                'mailmodel' => $mailmodel,
                'allpost' => $allpost,
                'profile' => $profile,
                'recentjob' => $recentjob,
                'peopleyouknow' => $peopleyouknow,
                'topcompany' => $topcompanyjob,
                'allmypost' => $allmypost,
                'suggested' => $suggested,
                'wallpost' => $wallpost,
                'user' => $user,
                'dataProvider' => $dataProvider,
                'roles' => $allposition,
                'industry' => $allindustry,
				'postCount' => $wallPostCount
            ]);
        }
         
    }
    public function actionMcbwallpost()
    {
        $allposition = Position::find()->where([
            "IsDelete" => 0
        ])
            ->orderBy([
            'Position' => 'SORT_ASC'
        ])
            ->all();
        $allindustry = Industry::getIndustryLists();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // notification for employer
        $allntemp = [];
        if (isset(Yii::$app->session['Employerid'])) {
            $allntemp = EmpNotification::getNotifications(Yii::$app->session['Employerid']);
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = [];
        if (isset(Yii::$app->session['Employeeid'])) {
            $allnotification = Notification::getNotifications(Yii::$app->session['Employeeid']);
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        $allmypost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => Yii::$app->session['Employerid']
        ])->all();

        $recentjob = array();
        $peopleyouknow = array();
        $topcompanyjob = array();
        $suggested = array();
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])->orderBy([
            'WallPostId' => SORT_DESC
        ]);
		
		$wallPostCount =  CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])->count();
		
        $user = new AllUser();

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctyiess = [];
                $ctys = explode(",", $user->PreferredLocation);
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ]);
            }
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid,
                'IsDelete' => 0
            ])->one();
            $suggested = EmpNotification::find()->where([
                'EmpId' => $empid
            ])
                ->groupBy([
                'Userid'
            ])
                ->all();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employerid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctys = explode(",", $user->PreferredLocation);
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ]);
            }
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Campus'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();

            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Campusid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctys = explode(",", $user->PreferredLocation);
                $ctyiess = [];
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ]);
            }
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];

            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Candidate'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();

            Yii::$app->session['usrtidsa'] = $user->UserId;

            if ($user->PreferredLocation != '') {
                $ctys = explode(",", $user->PreferredLocation);
                foreach ($ctys as $cty) {
                    $cties = Cities::find()->where([
                        'CityId' => $cty
                    ])->one();
                    $ctyiess[] = $cties->CityName;
                }
                $allpost = $allpost->andWhere([
                    'in',
                    'Locationat',
                    $ctyiess
                ])->orderBy([
                    'WallPostId' => SORT_DESC
                ]);
            }
        } else {
            return $this->redirect([
                'index'
            ]);
        }

        $wallpost = new CompanyWallPost();
        $comment = new CommentToPost();
		
        if (isset(Yii::$app->request->post('CommentToPost')['CommentToPostId'])) {
            $id = Yii::$app->request->post('CommentToPost')['CommentToPostId'];
            $cmntcnt = $comment->find()
                ->where([
                'CommentToPostId' => $id
            ])
                ->one();

            $cmntcntModel=CommentToPost::findOne($id);
            $cmntcntModel->Message=Yii::$app->request->post('CommentToPost')['Message'];

            if (! $cmntcntModel->save()) {
                echo "<prE>";
                print_r($cmntcntModel->getErrors());
                die();
            }

            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif ($comment->load(Yii::$app->request->post())) {
            $comment->OnDate = date('Y-m-d H:i:s');
            if ($comment->save()) {
                // Yii::$app->session->setFlash('success', 'Posted Successfully');
            } else {
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'mcbwallposts'
            ]);
        } elseif (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
            $date = date('Y-m-d');
            $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
            // var_dump(Yii::$app->request->post('CompanyWallPost'));
            // die();
            $totaljob = CompanyWallPost::find()->where([
                'WallPostId' => $id
            ])->one();
			
            $docmodel = new Documents();
            if ($totaljob->load(\Yii::$app->request->post())) {
                $totaljob->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
                // $totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];

                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $docmodel = new Documents();
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                    if ($image_id > 0) {
                        Documents::removeImage($totaljob->ImageId);
                    }
                } else {
                    $image_id = 0;
                }
                $totaljob->ImageId = $image_id;
            }
			
			if(!empty(Yii::$app->request->post('q'))){
				$totaljob->Locationat = Yii::$app->request->post('q');
			}
			
			/*Create Slug*/
			if(empty($totaljob->Slug)){
			$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
			$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
			$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
			$slug = preg_replace('~[^-\w]+~', '', $slug);
			$slug = trim($slug, '-');
			$slug = preg_replace('~-+~', '-', $slug);
			$slug = strtolower($slug);
			
			/*check slug*/
			$count = CompanyWallPost::find()
						->where([
							'Slug' => $slug
						])
						->count();
			if($count > 0){
				$slug = $slug.'-'.rand(9, 999);
			}
						
				$totaljob->Slug = $slug;
			}

            if ($totaljob->save()) {
                Yii::$app->session->setFlash('success', 'Post Successfully Updated');
            } else {
                // var_dump($wallpost->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }

            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif ($wallpost->load(Yii::$app->request->post())) {

            $date = date('Y-m-d');
            $totaljob = CompanyWallPost::find()->where([
                'IsHide' => 0,
                'EmployerId' => Yii::$app->session['Employerid'],
                'date(OnDate)' => $date
            ])->count();
            $Wallpostsetting = Wallpostsetting::find()->where([
                'companyId' => Yii::$app->session['Employerid']
            ])->one();
            $Wallpostsettingcount = (isset($Wallpostsetting->wallPostSetting)) ? $Wallpostsetting->wallPostSetting : 24;

            $artt = Yii::$app->request->post('CompanyWallPost')['Locationat'];
			
            if ($totaljob < $Wallpostsettingcount) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                } else {
                    $image_id = 0;
                }

                $wallpost->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
				
				/*Create Slug*/
				$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
				$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
				$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
				$slug = preg_replace('~[^-\w]+~', '', $slug);
				$slug = trim($slug, '-');
				$slug = preg_replace('~-+~', '-', $slug);
				$slug = strtolower($slug);
				
				/*check slug*/
				$count = CompanyWallPost::find()
							->where([
								'Slug' => $slug
							])
							->count();
				if($count > 0){
					$slug = $slug.'-'.rand(9, 999);
				}
				

				$locations= Yii::$app->request->post('CompanyWallPost')['Locationat'];
                $locationsList=implode(", ", $locations);

                $wallpost->Slug = $slug;
                $wallpost->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
                $wallpost->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
                $wallpost->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
                $wallpost->Locationat =$locationsList;
                $wallpost->ImageId = $image_id;
                $wallpost->VideoId = 0;
                $wallpost->EmployerId = Yii::$app->session['Employerid'];
                $wallpost->OnDate = date('Y-m-d H:i:s');

                if ($wallpost->save()) {
                    Notification::createNotificationToRoleUser($wallpost);
                    Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    // var_dump($wallpost->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Your post limit exceed, Please Contact admin');
            }

            return $this->redirect([
                'mcbwallpost'
            ]);
        } else {
            $mailmodel = new Mail();
            $dataProvider = new ActiveDataProvider([
                'query' => $allpost,
                'pagination' => [
                    'pageSize' => 50
                ]
            ]);
            $industry = Industry::getIndustryLists();
            $roles = Position::getRoles();
            $wallpost = new CompanyWallPost();
            return $this->render('mcbwallpost', [
                'mailmodel' => $mailmodel,
                'allpost' => $allpost,
                'profile' => $profile,
                'recentjob' => $recentjob,
                'peopleyouknow' => $peopleyouknow,
                'topcompany' => $topcompanyjob,
                'allmypost' => $allmypost,
                'suggested' => $suggested,
                'wallpost' => $wallpost,
                'user' => $user,
                'dataProvider' => $dataProvider,
                'roles' => $allposition,
                'industry' => $allindustry,
				'postCount' => $wallPostCount
            ]);
        }
    }

    public function actionSharePostViaMail()
    {
        // $post = CompanyWallPost::findOne($id);
        print_r('Working');
        die();
        if (! empty($post)) {}
    }

    public function actionPostView($id = null)
    {	
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
            ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
             ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';
        $suggested = array();
		
		if(empty($id)){
			
			$slug = (isset(Yii::$app->request->get()['alias'])?Yii::$app->request->get()['alias']:"");
			if(!empty($slug)){
				$postdetail = CompanyWallPost::find()->where([
					'Slug' => $slug
				])->one();
				if(empty($postdetail)){
					return $this->redirect([
								'wall/mcbwallpost'
							]);
				}
				$id = $postdetail->WallPostId;
			}
		}
		
		 if (isset(Yii::$app->request->get()['Nid'])) {
            $nid = Yii::$app->request->get()['Nid'];
            $notification = Notification::find()->where([
                'NotificationId' => $nid
            ])->one();
            $notification->IsView = 1;
            $notification->save(false);
        }

        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])->andWhere([
            'WallPostId' => $id
        ]);

        $user = new AllUser();

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $university = $profile->educations[0]->University;

            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])->andWhere([
                'WallPostId' => $id
            ]);
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid,
                'IsDelete' => 0
            ])->one();
            $suggested = EmpNotification::find()->where([
                'EmpId' => $empid
            ])
                ->groupBy([
                'Userid'
            ])
                ->all();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employerid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])->andWhere([
                'WallPostId' => $id
            ]);
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Campusid']
            ])->one();
            Yii::$app->session['usrtidsa'] = $user->UserId;
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->andWhere([
                'WallPostId' => $id
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ]);
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();

            Yii::$app->session['usrtidsa'] = $user->UserId;
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])->andWhere([
                'WallPostId' => $id
            ]);
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $allpost
        ]);
        $mailmodel = new Mail();
        $industry = Industry::getIndustryLists();
        $roles = Position::find()->where([
            "IsDelete" => 0
        ])
            ->orderBy([
            'Position' => 'SORT_ASC'
        ])
            ->all();
        $wallpost = new CompanyWallPost();
		$wallpostData = CompanyWallPost::find()->where(['WallPostId'=> $id])->one();
		if(!empty($wallpostData)){
			$string = trim(preg_replace('/\s\s+/', ' ', $wallpostData->Post));
			
			$this->view->params['meta_description'] = substr(strip_tags($string),0,150);
			$titleArr = array();
			if(!empty($wallpostData->Locationat)){
				$titleArr[0] = $wallpostData->Locationat;
			}
			if(!empty($wallpostData->industry->IndustryName)){
				$titleArr[1] = $wallpostData->industry->IndustryName;
			}
			if(!empty($wallpostData->position->Position)){
				$titleArr[2] = $wallpostData->position->Position;
			}
			
			$title = !empty($titleArr)?implode('/',$titleArr):"";
	
			$titles = !empty($wallpostData->Post) ? $wallpostData->PostTitle.' in '.$title : '';
	
			$this->view->params['og_title'] = $titles;
			$this->view->params['og_description'] = substr(strip_tags($string),0,150);
			$jobUrl = Url::base().'/wall/post-view/'.$id;
			$this->view->params['og_url'] = $jobUrl;
			/*if ($postdetail->docDetail) {
				$url = Url::base().'/backend/web/';
				$doc = $url . $postdetail->docDetail->Doc;
				$this->view->params['og_image'] = $doc;
			}*/			
			
		}
		
		$model = $wallpostData->employer;
		
		if (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
            $date = date('Y-m-d');
            $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
            // var_dump(Yii::$app->request->post('CompanyWallPost'));
            // die();
            $totaljob = CompanyWallPost::find()->where([
                'WallPostId' => $id
            ])->one();
			
            $docmodel = new Documents();
            if ($totaljob->load(\Yii::$app->request->post())) {
                $totaljob->PostTitle = Yii::$app->request->post('CompanyWallPost')['PostTitle'];
                // $totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];

                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $docmodel = new Documents();
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                    if ($image_id > 0) {
                        Documents::removeImage($totaljob->ImageId);
                    }
                } else {
                    $image_id = 0;
                }
                $totaljob->ImageId = $image_id;
            }
			
			if(!empty(Yii::$app->request->post('q'))){
				$totaljob->Locationat = Yii::$app->request->post('q');
			}
			
			/*Create Slug*/
			if(empty($totaljob->Slug)){
			$slug = Yii::$app->request->post()['CompanyWallPost']['PostTitle'];
			$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
			$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
			$slug = preg_replace('~[^-\w]+~', '', $slug);
			$slug = trim($slug, '-');
			$slug = preg_replace('~-+~', '-', $slug);
			$slug = strtolower($slug);
			
			/*check slug*/
			$count = CompanyWallPost::find()
						->where([
							'Slug' => $slug
						])
						->count();
			if($count > 0){
				$slug = $slug.'-'.rand(9, 999);
			}
						
				$totaljob->Slug = $slug;
			}

            if ($totaljob->save()) {
                Yii::$app->session->setFlash('success', 'Post Successfully Updated');
            } else {
                // var_dump($wallpost->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }

            return $this->redirect([
                'wall-post/'.$slug
            ]);
			
        }else{		
			return $this->render('post_view', [
				'mailmodel' => $mailmodel,
				'allpost' => $allpost,
				'profile' => $profile,
				'wallpostId'=>$id,
				'suggested' => $suggested,
				'user' => $user,
				'dataProvider' => $dataProvider,
				'industry' => $industry,
				'roles' => $roles,
				'wallpost' => $wallpost,
				'wallpostData' => $wallpostData,
				'model' => $model
			]);
		}
    }

   
   public function actionPagesearch()
    {
        $user = new AllUser();
        if ($user->load(Yii::$app->request->post())) {
            $nme = Yii::$app->request->post('AllUser')['Name'];
            $user = AllUser::find()->where([
                'Name' => $nme
            ])->one();
            if ($user->Name == '') {

                return $this->redirect([
                    'mcbwallpost'
                ]);
            }
        }

        $user = AllUser::find()->where([
            'Name' => $nme
        ])->one();

        $position = new Position();
        $allposition = $position->find()
            ->where("IsDelete=0")
            ->orderBy([
            'Position' => 'SORT_ASC'
        ])
            ->all();
        $allindustry = ArrayHelper::map(Industry::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'IndustryName' => 'SORT_ASC'
        ])
            ->all(), 'IndustryId', 'IndustryName');
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
            ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
				])
              ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        $allmypost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => Yii::$app->session['Employerid']
        ])->all();

        $recentjob = array();
        $peopleyouknow = array();
        $topcompanyjob = array();
        $suggested = array();

        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all();

        $user = new AllUser();

        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Candidate'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();
            $university = $profile->educations[0]->University;
            $peopleyouknow = AllUser::find()->where([
                'like',
                'Education.University',
                $university
            ])
                ->andWhere([
                'UserTypeId' => 2,
                'AllUser.IsDelete' => 0
            ])
                ->andWhere([
                '!=',
                'AllUser.UserId',
                $empid
            ])
                ->joinWith([
                'educations'
            ])
                ->all();

            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();

            $user = AllUser::find()->where([
                'Name' => $nme
            ])->one();
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid,
                'IsDelete' => 0
            ])->one();
            $suggested = EmpNotification::find()->where([
                'EmpId' => $empid
            ])
                ->groupBy([
                'Userid'
            ])
                ->all();
            $user = AllUser::find()->where([
                'Name' => $nme
            ])->one();
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Campus'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();

            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();
            $user = AllUser::find()->where([
                'Name' => $nme
            ])->one();
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];

            $profile = AllUser::find()->where([
                'UserId' => $empid
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Candidate'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();
            $university = $profile->educations[0]->University;
            $peopleyouknow = AllUser::find()->where([
                'like',
                'Education.University',
                $university
            ])
                ->andWhere([
                'UserTypeId' => 2,
                'AllUser.IsDelete' => 0
            ])
                ->andWhere([
                '!=',
                'AllUser.UserId',
                $empid
            ])
                ->joinWith([
                'educations'
            ])
                ->all();

            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();
            $user = AllUser::find()->where([
                'Name' => $nme
            ])->one();
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        } else {
            return $this->redirect([
                'index'
            ]);
        }

        $wallpost = new CompanyWallPost();
        $comment = new CommentToPost();

        if (isset(Yii::$app->request->post('CommentToPost')['CommentToPostId'])) {
            $id = Yii::$app->request->post('CommentToPost')['CommentToPostId'];
            $cmntcnt = $comment->find()
                ->where([
                'CommentToPostId' => $id
            ])
                ->one();
            $cmntcnt->Message = Yii::$app->request->post('CommentToPost')['Message'];
            $cmntcnt->save();
            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif ($comment->load(Yii::$app->request->post())) {
            $comment->OnDate = date('Y-m-d H:i:s');
            if ($comment->save()) {
                // Yii::$app->session->setFlash('success', 'Posted Successfully');
            } else {
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }
            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif (isset(Yii::$app->request->post('CompanyWallPost')['WallPostId'])) {
            $date = date('Y-m-d');
            $id = Yii::$app->request->post('CompanyWallPost')['WallPostId'];
            // var_dump(Yii::$app->request->post('CompanyWallPost'));
            $totaljob = CompanyWallPost::find()->where([
                'WallPostId' => $id
            ])->one();

            $docmodel = new Documents();

            $totaljob->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
            $totaljob->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];

            if ($totaljob->save()) {
                // Yii::$app->session->setFlash('success', 'Post edited Successfully');
            } else {
                // var_dump($wallpost->getErrors());
                Yii::$app->session->setFlash('error', 'There is some error please try again.');
            }

            return $this->redirect([
                'mcbwallpost'
            ]);
        } elseif ($wallpost->load(Yii::$app->request->post())) {
            $date = date('Y-m-d');
            $totaljob = CompanyWallPost::find()->where([
                'IsHide' => 0,
                'EmployerId' => Yii::$app->session['Employerid'],
                'date(OnDate)' => $date
            ])->count();
            $Wallpostsetting = Wallpostsetting::find()->where([
                'companyId' => Yii::$app->session['Employerid']
            ])->one();
            $Wallpostsettingcount = (isset($Wallpostsetting->wallPostSetting)) ? $Wallpostsetting->wallPostSetting : 24;

            $artt = Yii::$app->request->post('CompanyWallPost')['Locationat'];

            if ($totaljob < $Wallpostsettingcount) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($wallpost, 'ImageId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'Wallpost');
                } else {
                    $image_id = 0;
                }

                $wallpost->Post = Yii::$app->request->post('CompanyWallPost')['Post'];
                $wallpost->CIndustryId = Yii::$app->request->post('CompanyWallPost')['CIndustryId'];
                $wallpost->PositionId = Yii::$app->request->post('CompanyWallPost')['PositionId'];
                $wallpost->Locationat = Yii::$app->request->post('CompanyWallPost')['Locationat'];
                $wallpost->ImageId = $image_id;
                $wallpost->VideoId = 0;
                $wallpost->EmployerId = Yii::$app->session['Employerid'];
                $wallpost->OnDate = date('Y-m-d H:I:S');

                if ($wallpost->save()) {
                    Yii::$app->session->setFlash('success', 'Posted Successfully');
                } else {
                    // var_dump($wallpost->getErrors());
                    Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
            } else {
                Yii::$app->session->setFlash('error', 'Your post limit exceed, Please Contact admin');
            }
            return $this->redirect([
                'mcbwallpost'
            ]);
        } else {

            return $this->render('mcbwallpost', [
                'allpost' => $allpost,
                'profile' => $profile,
                'recentjob' => $recentjob,
                'peopleyouknow' => $peopleyouknow,
                'topcompany' => $topcompanyjob,
                'allmypost' => $allmypost,
                'suggested' => $suggested,
                'wallpost' => $wallpost,
                'user' => $user,
                'roles' => $allposition,
                'industry' => $allindustry
            ]);
        }
    }
    
    public function actionWatchopost($postid)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];
        $data['status'] = 'NOK';
        if (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }
        
        $watchPost = WatchToPost::find()->where([
            'EmpId' => $empid,
            'PostId' => $postid
        ])->one();
        
        
        //echo $empid;exit;
        
        if (! empty($watchPost)){
            
            $data['status'] = 'OK';
            $isWatch = 0;
            
            if ($watchPost->IsWatch == 0) {
                $isWatch = 1;
            }
            
            $watchPost->IsWatch = $isWatch;
            $watchPost->save([
                'IsWatch'
            ]);
           
           
            if ($watchPost->IsWatch == 1) {
                $data['button'] = '<i class="fa fa-thumbs-o-down"></i> UnLike';
            } else {
                $data['button'] = '<i class="fa fa-thumbs-o-up"></i> Like';
            }
            
            
        } else {
            
			$isWatch = 1;
            $data['status'] = 'OK';
            $watchPost = new WatchToPost();
            $watchPost->EmpId = $empid;
            $watchPost->PostId = $postid;
            $watchPost->IsWatch = $isWatch;
            
            if(!$watchPost->save()){
				return json_encode($watchPost->getErrors());
			};
		
            if ($watchPost->IsWatch == 1) {
                $data['button'] = '<i class="fa fa-thumbs-o-down"></i> UnLike';
            } else {
                $data['button'] = '<i class="fa fa-thumbs-o-up"></i> Like';
            }
        }
        
        return $this->asJson($data);
    }
    

    public function actionLiketopost($postid)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];
        $data['status'] = 'NOK';
        if (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }
        $likePost = LikeToPost::find()->where([
            'EmpId' => $empid,
            'PostId' => $postid
        ])->one();
        //echo $empid;exit;
		
        if (! empty($likePost)) {
            
            $data['status'] = 'OK';
            $islike = 0;
            if ($likePost->IsLike == 0) {
                $islike = 1;
                 $cwp = CompanyWallPost::find()->where(['WallPostId'=>$postid])->one();
                $notifications =  EmpNotification::find()->where(['wallpostid'=>$postid])->andWhere(['Type'=>'Like'])->one();
                if($notifications==null){
                     $notifications = new EmpNotification();
            $notifications->EmpId = $cwp->EmployerId;
            $notifications->UserId = $empid;
            $notifications->JobId = 0;
            $notifications->wallpostid = $postid;
            $notifications->Type = 'Like';
            $notifications->IsView = 0;
            $notifications->ForType = 'Company';
            $notifications->OnDate = date("Y-m-d H:i:s",time());
            $notifications->save(false);
                }
                
            }
            $notifications =  EmpNotification::find()->where(['wallpostid'=>$postid])->one();
            $notifications->delete();
            $likePost->IsLike = $islike;
            $likePost->save([
                'IsLike'
            ]);
           
            
            if ($likePost->IsLike == 1) {
                $data['button'] = '<i class="fa fa-thumbs-o-down"></i> UnLike';
            } else {
                $data['button'] = '<i class="fa fa-thumbs-o-up"></i> Like';
            }
            
        } else {
			$islike = 1;
            $data['status'] = 'OK';
            $likePost = new LikeToPost();
            $likePost->EmpId = $empid;
            $likePost->PostId = $postid;
            $likePost->IsLike = $islike;
            if(!$likePost->save()){
				return json_encode($likePost->getErrors());
			};
			  $cwp = CompanyWallPost::find()->where(['wallpostid'=>$postid])->one();
                $notifications =  EmpNotification::find()->where(['wallpostid'=>$postid])->one();
                if($notifications==null){
                     $notifications = new EmpNotification();
            $notifications->EmpId = $cwp->EmployerId;
            $notifications->UserId = $empid;
            $notifications->JobId = 0;
            $notifications->wallpostid = $postid;
            $notifications->Type = 'Like';
            $notifications->IsView = 0;
            $notifications->ForType = 'Company';
            $notifications->OnDate = date("Y-m-d H:i:s",time());
            $notifications->save(false);
                }
            if ($likePost->IsLike == 1) {
                $data['button'] = '<i class="fa fa-thumbs-o-down"></i> UnLike';
            } else {
                $data['button'] = '<i class="fa fa-thumbs-o-up"></i> Like';
            }
        }
        $data['total_like'] = LikeToPost::getPostLikeCount($postid);
        return $this->asJson($data);
    }

    public function actionCommenttopost()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $postvalue = CompanyWallPost::findOne(Yii::$app->request->post('commentpostid'));
        $data = [];
        $data['status'] = 'NOK';
        $comment = new CommentToPost();
        $comment->OnDate = date('Y-m-d H:i:s');
        $comment->PostId = Yii::$app->request->post('commentpostid');
        $comment->EmpId = Yii::$app->request->post('commentempid');
        $comment->Message = Yii::$app->request->post('comment');
        if ($comment->save()) {
             $cwp = CompanyWallPost::find()->where(['WallPostId'=>Yii::$app->request->post('commentpostid')])->one();
                $notifications =  EmpNotification::find()->where(['wallpostid'=>$postid])->andWhere(['Type'=>'Comment'])->one();
                if($notifications==null){
                     $notifications = new EmpNotification();
            $notifications->EmpId = $cwp->EmployerId;
            $notifications->UserId = Yii::$app->request->post('commentempid');
            $notifications->JobId = 0;
            $notifications->wallpostid = Yii::$app->request->post('commentpostid');
            $notifications->Type = 'Comment';
            $notifications->IsView = 0;
            $notifications->ForType = 'Company';
            $notifications->OnDate = date("Y-m-d H:i:s",time());
            $notifications->save(false);
                }
            $data['status'] = 'OK';
            $dataProvider = $postvalue->getPostCommentData();
            $data['html'] = $this->renderAjax('_postComments', compact('dataProvider'));
        } else {
            $data['html'] = $comment->getErrors();
        }
        return $this->asJson($data);
    }

    public function actionGetpreferredlocation($page, $q = null, $id = null)
    {

        $limit = 5;
        $offset = ($page - 1) * $limit;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [
            'results' => [
                'id' => '',
                'text' => '',
                'qtyLeft' => '',
                'serialNumbers' => ''
            ]
        ];
        if (! is_null($q)) {
            $query = new \yii\db\Query();
            $query->select([
                'CityName as id',
                'CityName as CityName'
            ])
                ->from('Cities')
                ->where([
                    'like',
                    'CityName',
                    $q.'%',
                    false
                ]) ->groupBy([
                    'CityName'
                ])
                ->offset($offset)
                ->limit($limit);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            $out['pagination'] = [
                'more' => ! empty($data) ? true : false
            ];
        } elseif ($id > 0) {
            $out['results'] = [
                'id' => $id,
                'text' => Cities::find()->where([
                    'CityId' => $id
                ])->one()->CityName
            ];
        }else{
            $query = new \yii\db\Query();
            $query->select([
                'CityName as id',
                'CityName as CityName'
            ])
                ->from('Cities')
                ->groupBy([
                    'CityName'
                ])
                ->orderBy(new Expression('rand()'))
                ->offset($offset)
                ->limit($limit);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            $out['pagination'] = [
                'more' => ! empty($data) ? true : false
            ];
        }
        return $out;
    }

	public function actionNestedcommenttopost(){
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$data = [];
        $data['status'] = 'NOK';
		$postData = Yii::$app->request->post();
		
		if(!empty($postData) && !empty($postData['CommentToPost']['EmpId'])){
			$comment = new CommentToPost();
			
			$userId = $postData['CommentToPost']['EmpId'];
			$comment->OnDate = date('Y-m-d H:i:s');			
			$comment->PostId = $postData['CommentToPost']['PostId'];
			$comment->CommentId = $postData['CommentToPost']['CommentToPostId'];
			$comment->EmpId = $postData['CommentToPost']['EmpId'];
			$comment->Message = $postData['CommentToPost']['Message'];

			if ($comment->save()) {
				$commentId = $comment->CommentToPostId;
				$commentData = $comment::findOne($commentId);
				
				$date = date('h:i A D M', strtotime($commentData->OnDate));
				if ($commentData->emp->UserTypeId == 2 || $commentData->emp->UserTypeId == 5) {
					$rr = Documents::getImageByAttr($commentData->emp, 'PhotoId', 'photo');
				} else {
					$rr = Documents::getImageByAttr($commentData->emp, 'LogoId', 'photo');
				}

                $cwp = CompanyWallPost::find()->where(['WallPostId'=>$comment->PostId])->one();

                $nt = new Notification();
                $nt->JobId =$comment->PostId;
                $nt->UserId =$postData['CommentToPost']['previousId'];
                $nt->IsView = 0;
                $nt->OnDate = date('Y-m-d H:i:s');
                $nt->save(false);

				$data['date'] = $date;
				$data['image'] = $rr;
				$data['comment_id'] = $commentId;
				$data['message'] = $postData['CommentToPost']['Message'];
				$data['status'] = 'OK';
				
			}			
			return $this->asJson($data);
			
		}
		
	}

    // candidate profile
    public function actionTeamprofile()
    {
        if (isset(Yii::$app->session['Teamid'])) {
            $profile = AllUser::find()->where([
                'UserId' => Yii::$app->session['Teamid']
            ])->one();
            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'Jobstatus' => 0,
                'PostFor' => 'Candidate'
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(5)
                ->all();

            $university = $profile->educations[0]->University;
            $peopleyouknow = AllUser::find()->where([
                'like',
                'Education.University',
                $university
            ])
                ->andWhere([
                'UserTypeId' => 2,
                'AllUser.IsDelete' => 0
            ])
                ->andWhere([
                '!=',
                'AllUser.UserId',
                Yii::$app->session['Employeeid']
            ])
                ->joinWith([
                'educations'
            ])
                ->all();
            $topcompanyjob = AllUser::find()->where([
                'UserTypeId' => 3,
                'IsDelete' => 0
            ])
                ->limit(5)
                ->all();
            $this->layout = 'walllayout';
			
			if ($profile->load(Yii::$app->request->post())) {
                $docmodel = new Documents();
                $image = UploadedFile::getInstance($profile, 'CVId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'cv');
                } else {
                    $image_id = 0;
                }

                $profile->CVId = $image_id;
                if ($profile->save()) {
                    Yii::$app->session->setFlash('success', "Successfully CV uploaded");
                    return $this->redirect([
                        'teamprofile'
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "There is some error please try again");
                    return $this->redirect([
                        'teamprofile'
                    ]);
                }
            } else {
			
            return $this->render('teamprofile', [
                'profile' => $profile,
                'recentjob' => $recentjob,
                'peopleyouknow' => $peopleyouknow,
                'topcompany' => $topcompanyjob
            ]);
			}
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionDeletepost()
    {
        $postid = Yii::$app->request->get()['postid'];
        $wallpost = CompanyWallPost::find()->where([
            'WallPostId' => $postid
        ])->one();
        $wallpost->IsDelete = 1;
        $wallpost->save();
        echo json_encode(1);
    }

    public function actionAllpost()
    {
        $user = AllUser::find()->where([
            'UserId' => Yii::$app->session['usrtidsa']
        ])->one();
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all();
        if ($user->PreferredLocation != '') {
            $ctys = explode(",", $user->PreferredLocation);
            foreach ($ctys as $cty) {
                $cties = Cities::find()->where([
                    'CityId' => $cty
                ])->one();
                $ctyiess[] = $cties->CityName;
            }
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->where([
                'in',
                'Locationat',
                $ctyiess
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        $this->layout = 'blanklayout';
        return $this->render('allpost', [
            'allpost' => $allpost
        ]);
    }

    public function actionSinglePost($id)
    {
        $user = AllUser::find()->where([
            'UserId' => Yii::$app->session['usrtidsa']
        ])->one();
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])
            ->andWhere([
            'WallPostId' => $id
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all();
        if ($user->PreferredLocation != '') {
            $ctys = explode(",", $user->PreferredLocation);
            foreach ($ctys as $cty) {
                $cties = Cities::find()->where([
                    'CityId' => $cty
                ])->one();
                $ctyiess[] = $cties->CityName;
            }
            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0
            ])
                ->where([
                'in',
                'Locationat',
                $ctyiess
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        $this->layout = 'blanklayout';
        return $this->render('allpost', [
            'allpost' => $allpost
        ]);
    }

    public function actionAllmypost()
    {
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => Yii::$app->session['Employerid']
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->all();
        $this->layout = 'blanklayout';
        return $this->render('allpost1', [
            'allpost' => $allpost
        ]);
    }

    public function actionDelimg()
    {
        $postid = Yii::$app->request->get()['id'];
        $wallpost = CompanyWallPost::find()->where([
            'WallPostId' => $postid
        ])->one();
        $docId = $wallpost->ImageId;
        $wallpost->ImageId = 0;
        if ($wallpost->save()) {
            Documents::removeImage($docId);
        }

        echo json_encode(1);
    }

    public function actionDelcomment()
    {
        $postid = Yii::$app->request->get()['id'];
        CommentToPost::deleteAll([
            'CommentToPostId' => $postid
        ]);
        echo json_encode(1);
    }

    public function actionFollower()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
             ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
				])
              ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        $userid = Yii::$app->request->get()['userid'];
        $lik = new Likes();
        $followers = $lik->getTotalfollow($userid);
        return $this->render('follower', [
            'followers' => $followers
        ]);
    }

    public function actionWallfollower()
    {
        // footer section

        // first block
        $about = new FooterAboutus();
        $footerabout = $about->find()->one();
        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
              ->limit(5)->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                //'IsView' => 0
            ])
                ->orderBy([
                'NotificationId' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
				])
               ->limit(5)->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $this->layout = 'walllayout';

        $follow = new Follow();
        $followers = $follow->find()
            ->where([
            'IsFollow' => 1
        ])
            ->all();
        return $this->render('wallfollower', [
            'followers' => $followers
        ]);
    }

    public function actionForgotpassword()
    {
        $this->layout = 'wallindexlayout';

        $model = new AllUser();
        if ($model->load(Yii::$app->request->post())) {
            $email = Yii::$app->request->post('AllUser')['Email'];
            $chk = $model->find()
                ->where([
                'Email' => $email
            ])
                ->one();
            if ($chk) {
                $ucode = 'CB' . time();
                $chk->VerifyKey = $ucode;
                $chk->ForgotStatus = 0;
                $chk->save();

                // var_dump($chk->getErrors());

                $to = $email;
                $from = Yii::$app->params['adminEmail'];
                $subject = "Reset Password";

                $html = "<html>
               <head>
               <title>Create a new password</title>
               </head>
               <body>
               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>
                <tbody><tr>
                                       <td><img src='http://mycareerbugs.com/images/logo.png' title='My  Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:150px;'></td>
                                   </tr>
                                   <tr>
                                       <td style='height:30px'></td>
                                   </tr>
                           <tr>
                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://mycareerbugs.com' target='_blank'> Career Bugs </a>
               <br><br>
               <span style='font-size:16px;line-height:1.5'>
               Please click in the link to create your new password
               <br/>
               <a href='http://mycareerbugs.com/wall/resetpassword?ucode=$ucode'>
               <span style='display: block; height:50 px;width:100px;color: #ffffff;text-decoration: none;font-size: 14px;text-align: center;font-family: 'Open Sans',Gill Sans,Arial,Helvetica,sans-serif;font-weight: bold;line-height: 45px;'>Click Here</span>
               </a>
              
               </span>
               </h2>
               </td>
               </tr>
               </tbody>
               </table>
               </body>
               </html>";
                $mail = new ContactForm();
                $mail->sendEmail($to, $from, $html, $subject);
                Yii::$app->session->setFlash('success', 'An email has been sent to your mail-id.Please click on the link provided in the mail to reset your password.
In case you do not receive your password reset email shortly, please check your spam folder also..');
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('forgotpassword', [
            'model' => $model
        ]);
    }

    public function actionResetpassword($ucode)
    {
        $this->layout = 'wallindexlayout';

        $model = new AllUser();
        $chk = $model->find()
            ->where([
            'VerifyKey' => $ucode
        ])
            ->one();

        if ($model->load(Yii::$app->request->post())) {
            $chk->Password = md5($model->Password);
            $chk->save();
            Yii::$app->session->setFlash('success', 'You have successfully created a new password.');
            return $this->redirect([
                'index'
            ]);
        } else {
            if ($chk) {
                if ($chk->ForgotStatus == 0) {
                    $chk->ForgotStatus = 1;
                    $chk->save();
                    return $this->render('resetpassword', [
                        'ucode' => $ucode,
                        'model' => $model
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', 'Your reset password link expired');
                    return $this->redirect([
                        'forgotpassword'
                    ]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'Please give your registered EmailId for create a new password.');
                return $this->redirect([
                    'forgotpassword'
                ]);
            }
        }
    }

    public function actionLocse()
    {
        $ctyloc = City::find()->all();
        foreach ($ctyloc as $cty) {
            $che[] = $cty->CityName;
            $keys[] = $cty->CityName;
        }
        $cj = array_combine($che, $keys);

        return json_encode($cj);
    }

    public function actionFollowbox()
    {
        $userid = Yii::$app->request->get()['userid'];
        $empid = Yii::$app->request->get()['empid'];
        $this->layout = 'blanklayout';
        return $this->render('followbox', [
            'empid' => $empid,
            'userid' => $userid
        ]);
    }

    public function actionWallfollow()
    {
        $empid = Yii::$app->request->get()['empid'];
        $follow = new Follow();
        $count = $follow->find()
            ->where([
            'FollowBy' => $empid
        ])
            ->one();
        if ($count) {
            if ($count->IsFollow == 1) {
                $isfollow = 0;
            } else {
                $isfollow = 1;
            }
            $count->IsFollow = $isfollow;
            $count->save();
        } else {
            $follow->FollowBy = $empid;
            $follow->IsFollow = 1;
            $follow->save();
        }

        $this->layout = 'blanklayout';
        return $this->render('wallfollow', [
            'empid' => $empid
        ]);
    }

    public function actionWalllocation()
    {
        $term = Yii::$app->request->get()['term'];
        $allteam = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])
            ->andWhere([
            'LIKE',
            'Locationat',
            $term
        ])
            ->all();

        $data = array();
        foreach ($allteam as $key => $value) {
            $data[] = array(
                'id' => $value->Locationat,
                'value' => $value->Locationat
            );
        }

        echo json_encode($data);
    }

    public function actionLocationsearch()
    {
        if (! \Yii::$app->request->isAjax) {
            return $this->redirect([
                'mcbwallpost'
            ]);
        }
        $walllocation = Yii::$app->request->get()['walllocation'];
        $CIndustryId = Yii::$app->request->get()['CIndustryId'];
        $PositionId = Yii::$app->request->get()['PositionId'];
        /*
         * $allpost = CompanyWallPost::find()->where([
         * 'IsDelete' => 0,
         * 'IsHide' => 0
         * ])
         * ->orderBy([
         * 'OnDate' => SORT_DESC
         * ])
         * ->all();
         *
         * $allpt = "SELECT * FROM CompanyWallPost WHERE IsDelete = 0 AND IsHide = 0 ";
         * $hmfilcat = [];
         * if ($walllocation != '') {
         * $hmfilcat[] = ' AND Locationat = "' . $walllocation . '" ';
         * }
         * if ($PositionId != 0) {
         * $hmfilcat[] = ' AND PositionId = "' . $PositionId . '" ';
         * }
         * if ($CIndustryId != 0) {
         * $hmfilcat[] = ' AND CIndustryId = "' . $CIndustryId . '" ';
         * }
         *
         * $hmfilcat[] = ' ORDER BY `Ondate` DESC ';
         * $allpt .= implode(" ", $hmfilcat);
         * $cmd = Yii::$app->db->createCommand($allpt);
         * $allpost = $cmd->queryAll();
         */
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0
        ])
		->orderBy([
			'WallPostId' => SORT_DESC
		]);

        if ($walllocation != '') {
            $allpost->andWhere(['like', 'Locationat', '%'.$walllocation .'%', false]);
        }
        if ($PositionId != 0) {
            $allpost->andWhere([
                'PositionId' => $PositionId
            ]);
        }
        if ($CIndustryId != 0) {
            $allpost->andWhere([
                'CIndustryId' => $CIndustryId
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $allpost,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        // $this->layout = 'blanklayout';
        return $this->renderAjax('allpost', compact('dataProvider'));
    }

    public function actionGetcities()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $post = \Yii::$app->request->get('term');

        $datas = ArrayHelper::map(Cities::find()->where([
            'like',
            'CityName',
            $post
        ])->all(), 'CityId', 'CityName');
        return $this->asJson($datas);
    }

    public function actionGetcitieslist()
    {
        $post = \Yii::$app->request->get('q');
        $data = [];
        $data['status'] = 'NOK';
        $models = ArrayHelper::map(Cities::find()->where([
            'like',
            'CityName',
            $post
        ])->all(), 'CityId', 'CityName');
        if (! empty($models)) {
            $data['status'] = 'OK';
            foreach ($models as $model) {
                $data['results'][] = $model;
            }
        }
        return $this->asJson($data);
    }

    public function actionPage()
    {
        $postModel = CompanyWallPost::find()->where([
            'IsDelete' => 0,
            'IsHide' => 0,
            'EmployerId' => Yii::$app->session['Employerid']
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $postModel,
            'pagination' => [
                'pageSize' => 4
            ]
        ]);

        return $this->render('pagecheck', compact('dataProvider'));
    }

    public function actionEditPost($id)
    {
        $model = CompanyWallPost::findOne($id);
        $data['status'] = 'NOK';
        if (! empty($model)) {
            $post = \Yii::$app->request->post();
            if ($model->load()) {
                if ($model->save()) {
                    $data['status'] = 'OK';
                    $data['model'] = $model;
                }
                return $this->asJson($data);
            }

            return $this->renderAjax('_editpost', compact('model'));
        }
    }
}
?>