<!DOCTYPE HTML>
<html>
<head>
<title>Job Share</title>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- Custom Theme files -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content=" " />
<!--google fonts-->
<style>
</style>
</head>
<body>
	<div id="margin-minus-top13" style="width: 660px; margin: 0 auto;">
		<aside
			style="width: 660px; margin: 0 auto; clear: both; overflow: hidden; padding: 0px; border: 1px solid #cccccc">
			<div style="">
				<img class="main-logo" style="margin: 20px auto; display: block;"
					src="https://mycareerbugs.com/images/logo.png" alt="My Career Bugs">
			</div>
			<h2
				style="font-size: 16px; display: block; padding: 0 10px; margin: 0 0 10px 0">Dear,</h2>

			<p style="font-size: 13px; padding: 0 10px; margin: 0 0 20px 0">
				You are receiving this mail because the recruiter considers your
				profile to be suitable for the following job opportunity posted on <a
					href="">www.mycareerbugs.com</a>, and would like you to apply for
				the job.
			</p>

			<div class="company-detail"
				style="clear: both; overflow: hidden; padding: 0px; border-top: 1px solid #cccccc; border-bottom: 1px solid #cccccc">
				<div class="company-img"
					style="width: 90px; padding: 10px; float: left">
					<img
						src="https://mycareerbugs.com/backend/web/imageupload/thumb_1179.jpg"
						class="img-responsive" alt=""
						style="width: 90px; border-radius: 200px;">
				</div>


				<div class="company-contact-detail" id="job-details"
					style="width: 520px; float: left; padding: 10px;">
					<div class="heading_main_tilte12">
						<h2
							style="line-height: 23px; font-size: 18px; font-weight: bold; color: #f16b22; margin: 2px 0 15px 0; border-bottom: 2px solid #f16b22; display: inline-block; padding: 0 0 5px 0; color: #f16b22;">
							Hr Recruiter</h2>
					</div>

					<p style="font-size: 12px; margin: 0 0 5px 0">
						<strong style="font-weight: bold;">Company Name: </strong><?=$model->employer->Name;?>
					</p>
					<p style="font-size: 12px; margin: 0 0 5px 0">
						<strong style="font-weight: bold;"> Company Details: </strong> My
						Career Bugs, India's Job Portal Site provide you bes... <a
							href="https://mycareerbugs.com/wall/searchcompany?userid=6"
							target="_blank" style="text-decoration: underline;">See More</a>
					</p>
					<div style="clear: both; overflow: hidden">
						<p
							style="width: 40%; font-size: 12px; margin: 0 0 5px 0; float: left">
							<strong style="font-weight: bold;">State:</strong> West Bengal
						</p>
						<p
							style="width: 60%; font-size: 12px; margin: 0 0 5px 0; float: left">
							<strong style="font-weight: bold;"> City:</strong> Kolkata
						</p>
					</div>
					<div
						style="height: 1px; background: #cccccc; margin: 5px 0 10px 0;"></div>
					<p style="font-size: 12px; margin: 0 0 7px 0;">
						<strong style="color: #f16b22; font-weight: bold;"><i
							class="fa fa-location-arrow"></i> Job Location: </strong> Central
						Delhi,Kolkata
					</p>
					<div style="clear: both; overflow: hidden">

						<p
							style="width: 40%; float: left; font-size: 13px; margin: 0 0 7px 0;">
							<strong style="color: #f16b22; font-weight: bold;"><i
								class="fa fa-calendar"></i> Role: </strong> Accountant
						</p>

						<p
							style="width: 60%; float: left; font-size: 13px; margin: 0 0 7px 0;">
							<strong style="color: #f16b22; font-weight: bold;"> Skill: </strong>
							TALLY , MS Excel , GST
						</p>
					</div>
				</div>


				<div style="padding: 15px">
					<div class="heading_main_tilte12">
						<h2
							style="line-height: 23px; font-size: 18px; font-weight: bold; color: #f16b22; margin: 2px 0 15px 0; border-bottom: 2px solid #f16b22; display: inline-block; padding: 0 0 5px 0; color: #f16b22;">
							Job Description</h2>
					</div>

					<p style="font-size: 12px; margin: 0 0 7px 0;"><?=str_replace("\n", '<br />',  $model->Post);?></p>



				</div>



			</div>



			<a href="<?php Yii::$app->urlManager->createAbsoluteUrl(['/wall/post-view','id' => $model->WallPostId])?>"
				style="background: #f16b22; font-size: 18px; font-weight: bold; text-align: center; color: #fff; padding: 7px 10px; font-family: Arial, Helvetica, sans-serif; text-decoration: none; display: block;">Apply
				Now</a>


			<p
				style="font-size: 13px; margin: 20px 0 0px 0; padding: 10px; color: #9d9d9d;">

				The sender of this email is registered with mycareerbugs.com and
				using mycareerbugs.com services. The responsibility of checking the
				authenticity of offers/correspondence lies with you.</p>

			<p
				style="font-size: 12px; margin: 0px 0 20px 0; padding: 0 10px 10px 10px; color: #9d9d9d;">

				© 2019 My Career Bugs Job Portal Pvt Ltd</p>

		</aside>
	</div>
</body>
</html>