<?php
$this->title = $EmployerName;
use common\models\Likes;
use frontend\components\follow\FollowWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
use frontend\components\sidebar\SidebarWidget;
use frontend\components\searchpeople\SearchPeople;
use common\components\loadmore\LoadMore;
use common\models\LeaveComment;
$leavecomment = new LeaveComment();
$lik = new Likes();
$filepath = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/images/coverpic/' . $employer->coverpic;
if (file_exists($filepath) && ! empty($employer->coverpic)) {
    $cover_pic = $imageurl . 'images/coverpic/' . $employer->coverpic;
} else {
    $cover_pic = $imageurl . 'images/background-main.jpg';
}

if ($model->LogoId != 0) {
    $ph = $url . $model->logo->Doc;
} else {
    $ph = $url . 'images/user.png';
}
$cUserId = 0;
if (isset(Yii::$app->session['Employerid'])) {
    $lg = Yii::$app->session['EmployerDP'];
	$cUserId  = Yii::$app->session['Employerid'];
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $lg = Yii::$app->session['EmployeeDP'];
	$cUserId  = Yii::$app->session['Employeeid'];
} elseif (isset(Yii::$app->session['Campusid'])) {
    $lg = Yii::$app->session['CampusDP'];
	$cUserId  = Yii::$app->session['Campusid'];
} elseif (isset(Yii::$app->session['Teamid'])) {
    $lg = Yii::$app->session['TeamDP'];
	$cUserId  = Yii::$app->session['Teamid'];
}

?>



<!-- Begin page content -->
<style>
#wall_edit_profile {
	background: #f16b22;
	color: #fff;
	position: absolute;
	right: 15px;
	bottom: 55px;
	width: 134px;
}

.widget-like.company li {
	width: 7%;
	margin: 5px 2px 2px 8px;
}

.uploadFile {
	background:
		url('https://mycareerbugs.com/frontend/web/img/whitecam.png')
		no-repeat;
	height: 32px;
	width: 32px;
	overflow: hidden;
	cursor: pointer;
}

.pos {
	color: #fff;
	position: absolute;
	right: -25px;
	bottom: 55px;
}

body {
	margin: 0;
	padding: 0;
}

.picture-container {
	margin: 0 auto;
	overflow: hidden;
	height: 220px;
	position: relative;
}

.picture-container img {
	min-height: 100%;
	min-width: 100%;
}

.picture-container img {
	position: absolute;
}

.controls a {
	text-decoration: none;
	color: white;
	padding: 15px;
	display: inline-block;
}

.save {
	color: #fff;
	position: absolute;
	right: 150px;
	bottom: 65px;
	width: 134px;
	text-align: center;
}
</style>

<style>
#login_system {
	position: fixed;
	bottom: 0px;
	width: 100%;
	z-index: 99999;
	background: rgba(255, 255, 255, 0.92);
	height: 100vh;
}

.white {
	width: 100%;
	min-height: 270px;
	top: 95%;
	position: absolute;
	left: 50%;
	margin: -225px 0 0 -50%;
	background: #fff;
	padding: 20px 20px 20px 20px;
	border-top: 2px solid #f16b22;
}

#login_system .step.form {
	padding: 20px 0
}

#login_system .step.form img {
	width: 250px;
	margin: 0 auto;
	display: block
}

.wall_logo_1 {
	margin-bottom: 10px !Important
}

#head {
	padding: 0px;
	border-bottom: 1px solid #f16b22
}

#landing_page_banner_button .head_right {
	width: 35%
}
</style>



<style>
.navbar {
	min-height: 41px !important;
}

.select2-selection.select2-selection--single {
	height: 34px !important;
}

#wall_description1 .widget {
	margin: 0;
	box-shadow: 0 0 0 #000;
}

#wall_description1 {
	position: fixed;
	z-index: 999;
	bottom: 0px;
	left: 0px;
	width: 100%;
	margin-bottom: 0px;
}



#wall_description1 .widget-body {
	padding: 12px 12px 0px 12px;
}

.ui-autocomplete {
	width: 179.6px;
	display: block;
	background-color: white;
	margin-top: -19px;
}

#pf_shw_mb{display:none}

@media (max-width: 767px) {
#pf_shw_mb .cover.profile .cover-info {
    background: #6c146b !important;
    clear: both;
    height: auto;}
    
   #pf_shw_mb  .cover.profile .cover-info .cover-nav li a{color:#fff !important;}

#pf_shw_mb .cover.profile .cover-info .cover-nav {
    margin: 11px 0 0 0 !important;}

#pf_shw_mb{display:block}
}
</style>


<?php if(!isset($lg)){ ?>
<div class="header-top" id="head">
	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<a class="logo_top" href=" "> <img class="wall_logo_1"
					src="<?=$imageurl;?>careerimg/logo.png" alt="MCB Logo"
					style="width: 100%">
				</a>
			</div>
			<div class="col-lg-8  col-md-8 col-sm-6 col-xs- 2 main-nav">
				<!-- Main Navigation -->

				<div class="head_right">
					<ul style="margin: 20px 0 0 0;">
						<li><a class="btn-123"
							href="https://mycareerbugs.com/site/employersregister">Employer
								Zone</a></li>
						<li class=""><strong> Are you recruiting?</strong> <a
							href="https://mycareerbugs.com/content/help"> <span>How we can
									help</span>
						</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
</div>



<div class="page-content">
	<div id="login_system">
		<div class="white">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123"
								href="<?= Url::toRoute(['site/login'])?>"> Login </a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="step form">
				<div class="omb_login">
					<h6>Login</h6>

					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="email" class="form-control" name="AllUser[Email]"
									required placeholder="Email address">
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control"
									name="AllUser[Password]" required placeholder="Password">
							</div>

							<!-- <span class="help-block">Password error</span>  -->

							<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
					</div>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-3">
							<p class="center-left">
								Not Yet Register ? <a
									href="<?= Url::toRoute(['site/register'])?>" class="color">
									Register Now </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-3">
							<p class="omb_forgotPwd">
								<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
									password?</a>
							</p>
						</div>
					</div>
					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<span class="omb_spanOr">or</span>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
 <?php } ?>


<div id="pf_shw_mb" style="display:none"> 
<div class="cover profile" id="cover_photo_b">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
					<div class="profile_main" style="display: none !important">
						<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
					</div>
					<div class="name" style="display: none">
						<a href="#">My Career Bugs Wall</a>
					</div>
					<ul class="cover-nav" style="left: 0px; top: 0px; "> 
						<li><a href="<?= Url::toRoute([$page]);?>"> <i 	class="  fa fa-user"></i> My Profile</a></li>
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>">  My Activity</a></li>
						<li><a href="<?= Url::toRoute(['site/hirecandidate']);?>">  Hire candidate  </a></li> 
						<li><a href="<?= Url::toRoute(['wall/mcbwallpost']);?>"> MCB Wall Post </a></li>
				  	<?php if ($profile->UserTypeId == 3) {?>  
									<li><a href="<?= Url::toRoute(['wall/companyprofile']);?>"> Wall Post  </a></li>
								  <?php } ?>	
								
					</ul>
					<div class="form-group right_main" id="no_dis_mbl1">
						<?php echo SearchPeople::widget()?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

 <div class="form-group right_main" id="mobile_view_only">
 
 	<?php echo SearchPeople::widget()?>
			 
</div>  
</div>











<div class="page-content">
	<div class="cover profile">
		<div class=" ">
			<div class="image">
				<div class="picture-container">
					<img data-top="0" data-left="0" src="<?=$cover_pic?>"
						style="width: 100%; height: 430px;" class="show-in-modal"
						alt="people">
				</div>

			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="cover-info">
						<div class="profile_main">
							<img src="<?=$pimage;?>" alt="people" class="">
						</div>
						<div class="name">
							<a href="#"><?=$EmployerName;?></a>
						</div>

						<!--  <?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?>
                  <ul class="cover-nav">
                     <li  class="active"><a href="<?= Url::toRoute(['wall/companyprofile'])?>"><i class="fa fa-user"></i> Profile </a></li>
                     <li  class=""><a href="#"><i class="fa fa-user-plus"></i> Follow </a></li>  
                        <li  class=""><a href="#"><i class="fa fa-envelope"></i> Leave a comment </a></li>                              
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-fa fa-commenting-o"></i> View Comments</a></li>
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-clipboard"></i> Job Post</a></li>
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-sticky-note-o"></i> Wall Post</a></li>
                  </ul>
                 <?php } ?> -->


						<ul class="cover-nav">
							<li class="active">
							<?php if($employer->UserId == $cUserId){ 
								$url = Url::base().'/'. str_replace(' ','',$employer->Name).'-'.$employer->UserId; ?> 
							<a
								href="<?php echo $url;?>"><i
									class="fa fa-user"></i> Profile </a>
									
							<?php }else{?>		
							<a
								href="#"><i
									class="fa fa-user"></i> Profile </a>
							<?php }?>
							</li>
							<!--<li  class=""><a href="#"><i class="fa fa-user-plus"></i> Follow </a></li>  
                        <li  class=""><a href="#"><i class="fa fa-envelope"></i> Leave a comment </a></li>-->
							<li class="">
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
							<a href="<?= Url::toRoute(['site/yourpost']);?>"><i
									class="fa fa-clipboard"></i> Job Post</a>
							<?php }else{ ?>	
							<a href="<?= Url::toRoute(['site/jobsearch','company' => $employer->UserId]);?>"><i
									class="fa fa-clipboard"></i> Job Post</a>
							<?php }?>
							
									</li>
							
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
							<li class="">
							<a href="<?= Url::toRoute(['wall/companyprofile']);?>"><i
									class="fa fa-sticky-note-o"></i> Wall Post</a>
							</li>
							<li class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i
									class="fa fa-fa fa-commenting-o"></i> View Comments</a></li>
									
							<?php }else{?>
							<li class="">
							<a href="<?= Url::toRoute(['wall/searchcompany','userid' => $employer->UserId])?>"><i
									class="fa fa-sticky-note-o"></i> Wall Post</a>
							</li>
							<li class="">
							<a href="#"
								onclick="if(isMobile==true){$('#mobileleavecomment').slideToggle();}else{$('#leavecomment').slideToggle();}$(this).parent().toggleClass('active');$('#quickupdate').toggle();$('.profile').toggleClass('active');">
									<i class="fa fa-envelope"> </i> Write a Review
							</a>		
							</li>	
							<?php }?>
							
							
									
						</ul>

					</div>
				</div>
				<div class="col-md-3">
					
					<div class="widget no_shadow" style="margin: 10px 0 0 0">
						<div class="action-buttons">
							<div class="row">
								<div class="col-md-12">
									<?php //echo '<pre>'; print_r($cUserId); print_r($id);die;?>
									<?php if($user_loggenin && $cUserId != $id){ ?>
									<?php
									echo FollowWidget::widget([
										'profile' => $employer,
										'empid' => $cUserId
									]);
									?>
									
<!-- 									<div class="half-block-left"> -->
									<!-- 										<a href="#" class="btn btn-azure btn-block"><i -->
									<!-- 											class="fa fa-user-plus"></i> Follow</a> -->
									<!-- 									</div> -->
									<div class="half-block-right">
										<a href="javascript:;" data-toggle="modal"
											data-target="#myModalmessage" class="btn btn-azure btn-block">
											<i class="fa fa-envelope"> </i> Message
										</a>
									</div>
				  <?php }?>	
				  <?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
                   <a href="" class="save">Save Changes</a> <a
										href="<?= Url::toRoute(['site/companyprofileeditpage'])?>"
										class="btn btn-azure btn-block" id="wall_edit_profile"> <i
										class="fa fa-pencil-square-o"></i> Edit Profile
									</a>
									<div class="controls">
										<img
											src="https://mycareerbugs.com/frontend/web/img/whitecam.png"
											class="pos" alt="edcvr">
									</div>
                  <?php } ?>
			
                </div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Begin page content -->
	<div class="page-content like_pae">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="widget">
						<div class="widget-header">
							<h3 class="widget-caption">Statistics</h3>
						</div>
						<div class="section bordered-sky">
							<p>
					  
  <span class="badge"><?=count($lik->getTotalfollow($id));?></span>
									<i class="  fa fa-thumbs-up"></i> Followers 
							</p>
							<p>
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
								<a href="<?= Url::toRoute(['wall/companyprofile'])?>"><span class="badge"><?=count($allpost);?></span>
									<i class=" fa fa-comments"></i> Posts</a>
							<?php }else{?>		
								<a href="<?= Url::toRoute(['wall/searchcompany','userid' => $employer->UserId])?>"><span class="badge"><?=count($allpost);?></span>
									<i class=" fa fa-comments"></i> Posts</a>							
							<?php }?>
							</p>
							<p>
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
								<a href="<?= Url::toRoute(['site/yourpost']);?>"><span
									class="badge"><?=$totalactive;?> </span> <i class=" fa fa-comments"></i> Active
									Jobs</a>
							<?php }else{?>		
								<a href="<?= Url::toRoute(['site/jobsearch','company' => $employer->UserId]);?>"><span
									class="badge"><?=$totalactive;?> </span> <i class=" fa fa-comments"></i> Active
									Jobs</a>
							<?php }?>		
							</p>
						 
						</div>
					</div>
					<div class="widget">

						<div class="widget-header">

							<h3 class="widget-caption">Basic Skill</h3>

						</div>

						<div class="basic_skill">

              <?php

            if ($skill) {

                foreach ($skill as $sk => $sval) {

                    ?>

              <p><?=$sval;?></p>

              <?php
                }
            }

            ?>

                </div>

					</div>

				</div>
				<div class="col-md-9 padding-left">
					<div class="row">
						<!-- left posts-->
						<div class="col-md-12">
							
							<div id="leavecomment" style="display: none;">
								<div class="widget-header">
									<h3 class="widget-caption"> Write Review  </h3>
								</div>
								<div class="widget">
									<div class="resume-box">                              
									<?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'row',
                'enctype' => 'multipart/form-data'
            ],
            'action' => Url::toRoute([
                'wall/searchcandidate',
                'userid' => $employer->UserId
            ])
        ]);
    ?>                              <div class="form-group"> 
                                    	<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>                              
                                    </div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">                                    
											<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                                 
										</div>
									</div>                             
									 <?php ActiveForm::end(); ?>                           
									 </div>
								</div>
							</div>

						
						
						
						
							<div class="widget-header">
								<h3 class="widget-caption">Description</h3>
							</div>
							<div class="row">
								<div class="col-md-12" id="allcompanypost">
									<div class="box box-widget">
										<div class="box-header with-border">
											<div class="user-block">
												<span class="description"><?php echo  html_entity_decode($employer->CompanyDesc); ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- left posts-->
						<div class="col-md-12">
							<div class="widget-header">
								<h3 class="widget-caption"><?=$employer->EntryType;?> Details</h3>
							</div>
							<div class="row">
								<div class="col-md-12" id="allcompanypost">
									<div class="box box-widget">
										<div class="box-header with-border">
											<div class="job-short-detail">


												<dl>
                                    <?php if($employer->EntryType == 'HR'){ ?>
                           <dt>Name</dt>
													<dd><?php echo  $employer->Name; ?> </dd>

													<dt>Email</dt>
													<dd><?php echo  $employer->Email; ?></dd>
                           <?php }else{ ?>
                                    <dt>Company Type</dt>
													<dd><?php echo  $employer->industry->IndustryName; ?> </dd>

													<dt>Company Email</dt>
													<dd><?php echo  $employer->Email; ?></dd>
                           <?php } ?>
 
                                    <dt>Address:</dt>
													<dd><?php echo  $employer->Address; ?> </dd>

													<dt>Mobile Number</dt>
													<dd> <?php echo  $employer->MobileNo; ?>    </dd>


													<dt style="display: none;">Contact Number</dt>
													<dd style="display: none;"> <?php echo  $employer->ContactNo; ?>   </dd>

													<dt style="display: none;">Contact Person</dt>
													<dd style="display: none;"> <?php echo  $employer->ContactPerson; ?>   </dd>

													<dt>City:</dt>
													<dd><?php echo  $employer->City; ?> </dd>

													<dt>State:</dt>
													<dd><?php echo  $employer->State; ?> </dd>

													<dt>Country:</dt>
													<dd><?php echo  $employer->Country; ?>  </dd>

													<dt>Pincode</dt>
													<dd> <?php echo  $employer->PinCode; ?>  </dd>
												</dl>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<!-- left posts-->
						<div class="col-md-12">
							<div class="widget-header">
								<h3 class="widget-caption">Followers</h3>
							</div>
							<div class="row">
								<div class="col-md-12" id="allcompanypost">
									<div class="box box-widget">
										<div class="box-header with-border">
											<ul class=" ">
										<?php
											$followers = $lik->getTotalfollow($employer->UserId);
												
											if ($followers) {

												foreach ($followers as $fk => $fvalue) {

													if ($fvalue->likeFrom->UserTypeId == 2 || $fvalue->likeFrom->UserTypeId == 5) {

														if ($fvalue->likeFrom->PhotoId != 0) {

															$ll = $url . $fvalue->likeFrom->photo->Doc;
														} else {

															$ll = $imageurl . 'images/user.png';
														}
													} else {

														if ($fvalue->likeFrom->LogoId != 0) {

															$ll = $url . $fvalue->likeFrom->logo->Doc;
														} else {

															$ll = $imageurl . 'images/user.png';
														}
													}

													if ($fvalue->likeFrom->UserTypeId == 2) {
														$link = 'searchcandidate';
													} elseif ($fvalue->likeFrom->UserTypeId == 3) {
														$link = 'searchcompany';
													} elseif ($fvalue->likeFrom->UserTypeId == 4) {
														$link = 'searchcampus';
													} else {
														$link = 'searchteam';
													}

													?>

												  <li><a
																					href="<?= Url::toRoute(['/wall/'.$link,'userid'=>$fvalue->likeFrom->UserId])?>">
																						<img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a>
																				</li>

											   <?php
												}
											}

											?>               
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row">
						<!-- left posts-->
						<div class="col-md-12">
							<div class="widget-header">
								<h3 class="widget-caption">Following</h3>
							</div>
							<div class="row">
								<div class="col-md-12" id="allcompanypost">
									<div class="box box-widget">
										<div class="box-header with-border">
											<ul class=" ">
										<?php //echo $employer->UserId;die;
											$followers = $lik->getMyfollow($employer->UserId);
												
											if ($followers) {

												foreach ($followers as $fk => $fvalue) {

													if ($fvalue->likeTo->UserTypeId == 2 || $fvalue->likeTo->UserTypeId == 5) {

														if ($fvalue->likeTo->PhotoId != 0) {

															$ll = $url . $fvalue->likeTo->photo->Doc;
														} else {

															$ll = $imageurl . 'images/user.png';
														}
													} else {

														if ($fvalue->likeTo->LogoId != 0) {

															$ll = $url . $fvalue->likeTo->logo->Doc;
														} else {

															$ll = $imageurl . 'images/user.png';
														}
													}

													if ($fvalue->likeTo->UserTypeId == 2) {
														$link = 'searchcandidate';
													} elseif ($fvalue->likeTo->UserTypeId == 3) {
														$link = 'searchcompany';
													} elseif ($fvalue->likeTo->UserTypeId == 4) {
														$link = 'searchcampus';
													} else {
														$link = 'searchteam';
													}

													?>

												  <li><a
																					href="<?= Url::toRoute(['/wall/'.$link,'userid'=>$fvalue->likeTo->UserId])?>">
																						<img src="<?=$ll;?>" alt="image"><?=$fvalue->likeTo->Name;?></a>
																				</li>

											   <?php
												}
											}

											?>               
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="widget">
								<div class="widget-header">
									<h3 class="widget-caption">
										Reviews 
									</h3>
								</div>
								<div class="widget-comments">
									<div class="row">
										<div class="col-md-12">
										     <?php
                                                $lv = new LeaveComment();
                                                $allcomment = $lv->getComment($employer->UserId);
                                                if ($allcomment) {
                                                    foreach ($allcomment as $ck => $cval) {
                                                        if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) {
                                                            $ll = ($cval->commentFrom->PhotoId != 0) ? $url . $cval->commentFrom->photo->Doc : '/backend/web/imageupload/user.png';
                                                        } elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) {
                                                            $ll = ($cval->commentFrom->LogoId != 0) ? $url . $cval->commentFrom->logo->Doc : '/backend/web/imageupload/user.png';
                                                        }
                                                        if ($cval->commentFrom->UserTypeId == 2) {
                                                            $link = 'searchcandidate';
                                                        } elseif ($cval->commentFrom->UserTypeId == 3) {
                                                            $link = 'searchcompany';
                                                        } elseif ($cval->commentFrom->UserTypeId == 4) {
                                                            $link = 'searchcampus';
                                                        } else {
                                                            $link = 'searchteam';
                                                        }
                                                        ?>                                    <ul class=" ">
                                        												<li><a
                                        													href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>">
                                        														<img src="<?=$ll;?>" alt="image">                                          <?=$cval->commentFrom->Name;?>                                          </a></li>
                                        												<li class="comments-block">
                                        													<p>                                             <?=$cval->Comment;?>                                          </p>
                                        												</li>
                                        											</ul>                                    <?php
                                                    }
                                                }
                                                ?> 
									
										</div>
									</div>
								</div>
							</div>
					
				</div>
				<div class="widget" style="display: none;">
					<div class="widget-header">
						<h3 class="widget-caption">
							Employee</span>
						</h3>
					</div>
					<div class="widget-know">
						<ul class=" ">
							<li><a href=""> <img
									src="<?=$imageurl;?>careerimg/Friends/guy-6.jpg" alt="image">
									Rohit Jaiswal <span>Fresher</span></a></li>
							<li><a href=""> <img
									src="<?=$imageurl;?>careerimg/Friends/guy-6.jpg" alt="image">
									Rohit Jaiswal <span>Retail Head at Atlas Brands P Ltd</span></a>
							</li>
							<li><a href=""> <img src="careerimg/Friends/guy-6.jpg"
									alt="image"> Rohit Jaiswal <span>Retail Head at Atlas Brands P
										Ltd</span></a></li>
							<li><a href=""> <img src="careerimg/Friends/guy-6.jpg"
									alt="image"> Rohit Jaiswal <span>Retail Head at Atlas Brands P
										Ltd</span></a></li>
							<li><a href=""> <img src="careerimg/Friends/guy-6.jpg"
									alt="image"> Rohit Jaiswal <span>Retail Head at Atlas Brands P
										Ltd</span></a></li>
							<li><a href=""> <img src="careerimg/Friends/guy-6.jpg"
									alt="image"> Rohit Jaiswal <span>Retail Head at Atlas Brands P
										Ltd</span></a></li>
							<li><a href=""> <img src="careerimg/Friends/guy-6.jpg"
									alt="image"> Rohit Jaiswal <span>Retail Head at Atlas Brands P
										Ltd</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModalmessage" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">  
					 <?php
    $url_info = explode('/', Yii::$app->request->url);
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'row',
            'enctype' => 'multipart/form-data'
        ],
        'action' => Url::toRoute([
            '/' . $url_info[1]
        ])
    ]);
    ?>               
					 <div class="form-group">                  
						<?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>               
						</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">                     
							<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                  
						</div>
					</div>              
				 	<?php ActiveForm::end(); ?>            
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>

<script>
$(document).ready(function () {
		$('.save').hide();
		var img = $('.picture-container img'); 
		img.attr('style', 'top:<?= $employer->data_top?>px;left:<?= $employer->data_lft?>px');
		var y1 = $('.picture-container').height();
    	var y2 = img.height();
    	var x1 = $('.picture-container').width();
    	var x2 = img.width();
    	var desktop_start_x=0;
    	var desktop_start_y=0;
    	var mobile_start_x= -200;
    	var mobile_start_y= -200;
		$('.save').click(function(event){
	            event.preventDefault();
	            var t = img.position().top,
				l = img.position().left;
				img.attr('top', t);
	            img.attr('left', l);
	            img.draggable({ disabled: true });
				$('.save').hide(); 
				var cmid =  "<?= $employer->UserId ?>";
				$.ajax({url:"<?= Url::toRoute(['site/covloc'])?>?imgtp="+t+'&imglf='+l+'&cmid='+cmid,
				success:function(result)
				{
					  $('#allcompanypost').html(result);
					  $('#myModalpostsearch').modal('hide');
				}
			});
		})
		$('.pos').click(function(event){
			event.preventDefault();
			$('.save').show();
			  img.draggable({ 
			  	disabled: false,
			  	scroll: false,
			  	axis: 'y, x',
			  	cursor : 'move',
    		  	drag: function(event, ui) {
                     if(ui.position.top >= 0)
                      {
                          ui.position.top = 0;
                      }
                      if(ui.position.top <= y1 - y2)
                      {
                          ui.position.top = y1 - y2;
                      }
                      if (ui.position.left >= 0) {
                      	ui.position.left = 0;
                      };
                      if(ui.position.left <= x1 - x2)
                      {
                          ui.position.left = x1 - x2;
                      }
    		 	}
		});
	});
});

		</script>