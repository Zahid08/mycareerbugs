<?php 
use yii\helpers\Url;

$value = $model;

   if ($value->job->JobStatus == 0) {
   $status = 'Open';
   $background = 'green';
   } else {
   $status = 'Closed';
   $background = 'red';
   }
   ?>
<style>
.height_set {height: 36px;overflow: hidden;}
</style>

<div class="item-list">
   <div class="col-sm-12 add-desc-box">
      <div class="applied_job">
         <p>Applied Job
         </p>
      </div>
      <span class="deletejob"
         onclick="deleteappliedjob(<?=$value->JobId;?>,<?=Yii::$app->session['Campusid'];?>);">
      <i class="fa fa-trash">
      </i> Delete
      </span>
      <div class="applied_job" style="background: <?=$background;?>;top: 50px;">
         <p>
            <?=$status;?>
         </p>
      </div>
       
      <div class="add-details">
         <a
            href="<?= Url::base().'/campus-post/'.$value->job->Slug;?>">
            <h5
               class="add-title"><?=$value->job->JobTitle;?></h5>
         </a>
         <div class="info">
            <span class="category"><?=$value->job->position->Position;?></span>
            - <span class="item-location"><i class="fa fa-map-marker"></i> <?=$value->job->Location;?></span>
            <br> <span> <strong><?=$value->job->CompanyName;?></strong>
            </span>
         </div>
   <!--     <div class="info bottom">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">Experience : </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category"><?php if($value->job->Experience!='Fresher'){ echo $value->job->Experience.' Year';}else{echo $value->job->Experience;}?></span>
            </div>
         </div> -->
         <div class="info bottom" style="display:none">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">Keyskills : </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category">
               <?php
                  $jskill = '';
                  foreach ($value->job->jobRelatedSkills as $k => $v) {
                  $jskill .= $v->skill->Skill . ' , ';
                  }
                  echo $jskill;
                  ?>
               </span>
            </div>
         </div>
         
     
            <div class="info bottom">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">  Walkin </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category"> <?=date('dS M Y',strtotime($value->job->WalkinFrom)).' - '.date('dS M Y',strtotime($value->job->WalkinTo));?> </span>
            </div>
         </div>
         
            <div class="info bottom">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">  Venue </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category"><?=$value->job->Venue;?></span>
            </div>
         </div>
         
         <div class="info bottom">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">  Designation </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category"><?=$value->job->Designation;?></span>
            </div>
         </div>
         
       
             <div class="info bottom">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">Job Description </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category height_set"> <?=htmlspecialchars_decode($value->job->JobDescription);?></span>
            </div>
         </div>
         
            
             
          
         
             <div class="info bottom">
            <div class="col-sm-3 col-xs-3">
               <span class="styl">Salary Range </span>
            </div>
            <div class="col-sm-9 col-xs-9 left-text">
               <span class="category"><?=$value->job->Salary;?> Lakhs</span>
            </div>
         </div>
         
         <div class="info bottom">
            <span class="category" style="text-align: right">    Posted By <?=$value->job->employer->Name.' ('.date('d M Y, h:i A',strtotime($value->job->OnDate)).')';?></span>
         </div>
      </div>
   </div>
</div>