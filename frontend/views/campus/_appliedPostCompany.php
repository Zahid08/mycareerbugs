<?php use yii\helpers\Url; 
$value = $model;
$uploadUrl='/backend/web/';

?>
<div class="item-list">
   <div class="col-sm-12 add-desc-box">
      
	  <?php
		$emp_name = preg_replace('/\s+/', '', $value->user->Name);
        $url = Yii::$app->urlManager->createAbsoluteUrl('') . $emp_name . "-" . $value->user->UserId;
	  ?>
	  
		<div class="profile-content payment_list">
                                <div class="card">
                                     <p class="last_update_main" style="font-size:16px; margin-bottom:15px"><strong style="color:#f16b22"> Job Title :</strong><b> <?=$value->campusPost->JobTitle;?></b></p>
                                      <div class="clear"> </div>
                                    <div class="firstinfo">
									
										<?php 
											if($value->user->photo)
											{
												$doc=$uploadUrl.$value->user->photo->Doc;
											}
											else
											{
												$doc=$imageurl.'images/user.png';
											}
										?>
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive">
                                        <div class="profileinfo">  
                                             
                                            <h1> <?=$value->user->Name;?></h1>
											
											<small>From <?=$value->user->City;?>, <?=$value->user->State;?></small>
											
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														
														<li>Contact Person: <?=$value->user->ContactPerson;?>  </li>
														<li>Contact Number:  <?=$value->user->ContactNo;?> </li>
														
													  </ul>
                                                </div>
												  
												 </div>
											</div>
												
													
													 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->user->UpdatedDate));?></p>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p><br/>
													
										</div>
                                    </div>
		 
									 <div class="contact_me">
									 <a href="<?=$url;?>" target="_blank" class="btn-default" type="button" ><i class="fa fa-eye"></i> View Profile</a>
									  
									 </div>
                                </div>
                            </div>
							
							
					
   
   </div>
</div>