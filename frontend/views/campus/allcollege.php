<?php
$this->title = 'Job By Campus';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\AllUser;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
$user=new AllUser();
?>
<!-- start main wrapper --> 
	<div id="wrapper">
		
		 <div class="spacer-5"></div> 
		    <div class="container" id="company_list_pagination">
			<div class="clear"></div>

			   <div class="col-md-12">
			    <a href="#/" style="float: left;">  <?=$state;?>  </a>
			    </div>
			    <?php
			    $campus=$user->getCamp($state);
			    foreach($campus as $ckk=>$cvall)
			    {
			   ?>
				  <div class="col-md-2 col-sm-12 col-xs-12" onclick="campusdetail(<?=$cvall->UserId;?>);"> 
					<div class="company_show_logo">  <a href="#/">  <?=$cvall->CollegeName;?>  </a> </div> 	 
				 </div>
			   <?php
			    }
				
			    ?>
			   </div>
				    
			</div>
		
		 <div class="spacer-5"></div> 
		 
		
		
		<div class="testimony">
			<div class="container">
				<h1><?=$peoplesayblock->Heading;?></h1>
				<?=htmlspecialchars_decode($peoplesayblock->Content);?>
					
			</div>
			<div id="sync2" class="owl-carousel">
				<?php
				foreach($allfeedback as $fk=>$fvalue)
				{
					if($fvalue->docDetail)
					{
						$doc=$url.$fvalue->docDetail->Doc;
					}
					else
					{
						$doc=$imageurl.'images/user.png';
					}
				?>
				<div class="testimony-image">
					<img src="<?=$doc;?>" class="img-responsive" alt="testimony" style="height: 150px;width: 150px;"/>
				</div>
				<?php
				}
				?>
			</div>
			
			<div id="sync1" class="owl-carousel">
				<?php
				foreach($allfeedback as $fk=>$fvalue1)
				{
					?>
				<div class="testimony-content container">
					<?=htmlspecialchars_decode($fvalue1->Message);?>
					<p>
						<?php echo $fvalue1->Name.','.$fvalue1->Designation.','.$fvalue1->Companyname;?>
					</p>
					<div class="media-testimony">
						<?php
						if($fvalue1->Twitterlink!='')
						{
						?>
						<a href="<?=$fvalue1->Twitterlink;?>" target="blank"><i class="fa fa-twitter twit"></i></a>
						<?php
						}
						if($fvalue1->LinkedinLink!='')
						{
						?>
						<a href="<?=$fvalue1->LinkedinLink;?>" target="blank"><i class="fa fa-linkedin linkedin"></i></a>
						<?php
						}
						if($fvalue1->Facebooklink!='')
						{
						?>
						<a href="<?=$fvalue1->Facebooklink;?>" target="blank"><i class="fa fa-facebook fb"></i></a>
						<?php
						}
						?>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
		

<!-----myModal_email------>
<div class="modal fade" id="myModal_college" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">College Detail</h4>
        </div>
		
		<div class="row main" id="collegedetail">
		
		</div>
	 </div>  
	</div>
</div>
<!-----myModal_email------>

<script type="text/javascript">
	 function campusdetail(userid) {
        $.ajax({url:"<?= Url::toRoute(['campus/campusdetail'])?>?userid="+userid,
		  success:function(results)
		  {
			  $('#collegedetail').html(results);
			  $('#myModal_college').modal('show');
		  }
		});
     }
</script>