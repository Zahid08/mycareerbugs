<?php
$this->title = 'Student List';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use common\models\AllUser;
use common\models\Plan;
use common\models\PlanAssign;
$plan=new Plan();
$planassign=new PlanAssign();
$allplan=$plan->getAllplan();

if(isset(Yii::$app->session['Employerid']))
{
$isassign=$planassign->isAssign(Yii::$app->session['Employerid']);
}
else
{
$isassign=array();
}

if(isset($_POST['course']))
{
		  $crs=$_POST['course'];
}
else
{
		  $crs='';
}

if(isset($_POST['specialization']))
{
		  $spe=$_POST['specialization'];
}
else
{
		  $spe='';
}

if(isset($_POST['studentname']) && $_POST['studentname']!='')
{
		  $studentname=$_POST['studentname'];
}
else
{
		  $studentname='';
}
?>

<div id="wrapper"><!-- start main wrapper -->
		 
		  <div class="headline job_head">
       <div class=" container"><!-- start headline section -->
		<div class="row">
			<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 top-main bg-full margin_auto"> 
			   <h2 class="banner_heading">  <span>Search </span>  Student </h2>
			 
				<div class="sticky">
					<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form']]); ?>
                        <div class="group-sm group-top">
                         
						 <div class="group-item col-md-4 col-xs-6">
                            <div class="form-group">
							  <input type="text" name="studentname" class="form-control" placeholder="Student Name" value="<?=$studentname;?>"/>
							       
                            </div>
                          </div>
						 
						 <div class="group-item col-md-4 col-xs-12"> 
							<div class="form-group">
							  <select id="form-filter-location" name="course" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value=" ">Course</option>
                                <?php
								if($course)
								{
								   foreach($course as $comk=>$comval)
								   {
                                       $getCourseName=\common\models\NaukariQualData::findOne($comval->CourseId);

								   ?>
								   <option value="<?=$getCourseName->id;?>" <?php if($crs==$getCourseName->id) echo "selected";?>><?=$getCourseName->name;?></option>
								   <?php
								   }
								}
								?>
                              </select>
							</div>
                          </div>
						  
                          <div class="group-item col-md-3 col-xs-6">
                            <div class="form-group">
                              <select id="form-filter-location" name="specialization" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">Specialization</option>
								<?php
								if($spec)
								{
								   foreach($spec as $sk=>$sval)
								   {
                                       $specilizaitons=\common\models\NaukariSpecialization::findOne($sval->SpecializationId);

								   ?>
								   
								   <option value="<?=$specilizaitons->id;?>" <?php if($spe==$specilizaitons->id) echo "selected";?>><?=$specilizaitons->name;?></option>
								   <?php
								   }
								}
								?>
                              </select> 
                            </div>
                          </div>
						   
                          <div class=" group-item reveal-block reveal-lg-inline-block col-md-1 col-xs-12">
							<?= Html::submitButton('Search', ['name'=>'studentsearch','class' => 'btn btn-primary element-fullwidth']) ?>
                          </div>
						  
                        </div>
                      <?php ActiveForm::end(); ?>
					 </div>
						</div>
						 
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
	</div>
	
		  <div class="inner_page">
			<div class="container">
		
<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="right-side">
		  <div class="row">
		  <div class="width-13">
							  <a class="brdr orange_bg new_style" href="#/">    
										  <img class="img-responsive center-block " alt="" src="<?=($campusdetail->LogoId!=0)?$url.$campusdetail->logo->Doc:'/images/user.png';?>" style="width: 60px; margin-bottom:10px;height: 60px;"><?=$campusdetail->CollegeName;?> <br/> (<?=$campusdetail->State;?> , <?=$campusdetail->City;?>)  </a>
		  </div>
		  </div>
		  <style>  .orange_bg{  background: #333 !Important;
    color: #fff !important;
    display: block;
    padding: 20px;
    text-align: center;}</style>
		   
		   <div class="panel-group" id="accordion1" >
				 <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#expgender"><h4 class="panel-title">
                             Gender
                        </h4></a>
                    </div>
                    <div id="expgender" class="panel-collapse collapse">
                        <div class="panel-body"> 
						            <?php
									$gender=array();
								if(isset($_GET['gender']))
								{
								$gender=explode(",",$_GET['gender']);
								}
								?>
						            <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="camcgender" value="Male" <?php if(in_array('Male',$gender)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Male   </label>
								      </div>	 
							       </span>
								 
								     <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="camcgender" value="Female" <?php if(in_array('Female',$gender)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Female  </label>
								      </div>	 
							       </span>
								    
                        </div>
                    </div>
                </div>
				 
				
				 
				
                <div class="panel panel-default" >
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#expexperience"><h4 class="panel-title">
                             Experience
                        </h4></a>
                    </div>
                    <div id="expexperience" class="panel-collapse collapse">
                        <div class="panel-body">
							  <input type="hidden" id="campusid" value="<?=$_GET['userid'];?>" />
							  <input type="hidden" id="specid" value="<?=$_GET['specid'];?>" />
                           <?php
						   $experience='';
								if(isset($_GET['experience']))
								{
								$experience=$_GET['experience'];
								}
								?>
						    <select class="form-control" id="camcexperience"  onchange="localStorage.campusstudent='expexperience';campusstudentsearch();">
										<option value="">Experience</option>
										<option value="0-1" <?php if($experience=='0-1') echo "selected";?>>  Below 1 Year</option>
											<?php
											for($fr=1;$fr<=30;$fr=$fr+1)
											{
												$frn=$fr+1;
										?>
										<option value="<?=$fr.'-'.$frn;?>" <?php if($experience==$fr.'-'.$frn) echo "selected";?>><?=$fr.'-'.$frn;?> Years</option>
															   <?php
											}
											?>
							</select>
                        </div>
                    </div>
                </div>
				
				<div class="panel panel-default" >
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#expshowprofile"><h4 class="panel-title">
                             Show Profiles
                        </h4></a>
                    </div>
                    <div id="expshowprofile" class="panel-collapse collapse">
                        <div class="panel-body">
                           <?php
								if(isset($_GET['cresume']))
								{
								$showprofile=$_GET['cresume'];
								}
								else
								{
										$showprofile='';
								}
								?>
						         <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="campusceresume" value="1" <?php if($showprofile==1) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> With resume only 
									</label>
								  </div>	 
							     </span>
								 
                        </div>
                    </div>
                </div>
				
				
                
            </div>
		
		
		
		
		
		
		
		
		<div class="spacer-5"></div>
 
                           
                               <div class="widget-heading"><span class="title"> Our Plans  </span></div> 
                          <div id="orange_payment_block">     
								  <?php
		 if($allplan)
		 {
			foreach($allplan as $key=>$pland)
			{
				?>
									   <div class="widget_inner">
											   <div class="panel price panel-red">
												<div class="panel-body text-center">
													<p class="lead" ><strong><?=$pland->PlanName;?></strong></p>
												</div> 
												<ul class="list-group list-group-flush text-center width-half"> 
											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
											   <li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li> 
												</ul>
												<div class="panel-footer">
													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
												</div>
											</div>
									   <!-- /PRICE ITEM -->
								       </div>
									   <?php
			}
		 }
		 ?>  
							</div>
                   	</div>
					
					 
					  
					 
					 <div class="col-lg-9  col-md-9 col-sm-9 col-xs-12" id="mobile_design">
							   <b><?=$specialization->course->CourseName;?> -> <?=$specialization->Specialization;?></b>
					 <div class="pannel_header margin_top">
                       
                        <div class="width-13">
						
							 <div class="checkbox">
							    <label> <input type="checkbox" id="empchk" value="" onclick="if($(this).prop('checked')==true){$('.empch').prop('checked',true);}else{$('.empch').removeAttr('checked');}">
								<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> 
							    </label>
							  </div>					     
							</div>
							
					      <div class="width-14" style="display: none;">
							 <div class="checkbox">
							    <label> 
								  Sms
							    </label>
							  </div>					     
							</div>
						  <div class="width-14">
							 <div class="checkbox" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0) { ?>onclick="if($('.empch').prop('checked')==true){mailtoemp('myModal_email');}else{alert('Please check the checkbox to send mail');}" <?php }elseif(isset(Yii::$app->session['Campusid'])){ ?> onclick="if($('.empch').prop('checked')==true){mailtoemp('myModal_email');}else{alert('Please check the checkbox to send mail');}" <?php }else{ ?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php }?>>
							    <label>  
								 Mail
							    </label>
							  </div>	
							</div>
							
							
							 <div class="width-14">
							 <div class="checkbox" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0) { ?>onclick="if($('.empch').prop('checked')==true){download();}else{alert('Please check the checkbox to download');}" <?php } elseif(isset(Yii::$app->session['Campusid'])){ ?> onclick="if($('.empch').prop('checked')==true){download();}else{alert('Please check the checkbox to download');}"  <?php } else{ ?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php }?>>
							    <label> 
								  Download  
							    </label>
							  </div>	
							
							</div>
                             
					    <div class="width-10" id="select_per_page">	
                            <div class="form-groups">
							<div class="col-sm-8">
						      <label>Result Per page </label>
							 </div>
						<div class="col-sm-4">
                              <?php
							if(isset($_GET['perpage']))
							{
							$perpage=$_GET['perpage'];
							}else{$perpage=10;}
							?>
                              <select id="form-filter-location" data-minimum-results-for-search="Infinity" class="form-control23 select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="getresultperpagecampus(this.value,'allstudent');">
								<option value="10" <?php if($perpage==10) echo "selected='selected'";?>>10</option>
                                <option value="20" <?php if($perpage==20) echo "selected='selected'";?>>  20  </option>
                                <option value="30" <?php if($perpage==30) echo "selected='selected'";?>>  30 </option>
                                <option value="40" <?php if($perpage==40) echo "selected='selected'";?>>  40  </option>
								<option value="50" <?php if($perpage==50) echo "selected='selected'";?>>  50  </option> 
                              </select> 
                            </div>
							 </div>
                          </div>
						
						<div class="pannel_header" style="margin: 0px;">
							<span id="searchdivcam" style="margin-left: 10px;font-size: 14px;color: #f16b22;"></span>
						</div>
					 </div>
					 
                     <?php
                     if($model)
                     {
                        foreach($model as $mk=>$mval)
                        {
							  if($mval->photo)
										{
											$doc=$url.$mval->photo->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
                    ?>
					  <div class="col-md-12 col-sm-12 col-xs-12">
					 <div class="profile-content payment_list">
                                <div class="card">
										<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
                                    <div class="firstinfo">
										<input type="hidden" class="empid" value="<?=$mval->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$mval->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive">
                                        <div class="profileinfo">
                                            <h1> <?=$mval->Name;?> </h1>
											<small>From : <?=$mval->City;?> , <?=$mval->State;?></small>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-12 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">			 
														<li>About Me: <?=$mval->AboutYou;?></li> 
													  </ul>
                                                </div>
												  
												 </div>
											</div> 
												   <div class="profile-skills">
                                                    <?php
													if($mval->empRelatedSkills)
													{
													foreach($mval->empRelatedSkills as $ask=>$asv)
                                                    {
                                                        ?>
                                                        <span> <?=$asv->skill->Skill;?> </span>
                                                        <?php
                                                    }
                                                    }
                                                    ?>
													</div> 
													
													 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($mval->UpdatedDate));?></p>
													 <!--<p class="last_update_main">Shortlisted by 2 Recruiters recently</p>-->
										</div>
                                    </div>
		 
									 <div class="contact_me">
                                        <?php
										$availablecv=$isassign->CVDownload-$isassign->UseCVDownload;
										if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
												  $na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
										if($mval->CVId!=0)
										{
										?>
									  	<a href="<?=$url.$mval->cV->Doc;?>" download="<?=$name.'.'.$ext;?>" class="btn-default"> <i class="fa fa-download"></i>  Resume  </a>
										<?php
										}
										else
										{
										?>
										<a class="btn-default"> <i class="fa fa-download"></i>No Resume  </a>
										<?php
										}
										}
										elseif(isset(Yii::$app->session['Campusid'])){
										 $na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
										if($mval->CVId!=0)
										{
										?>
									  	<a href="<?=$url.$mval->cV->Doc;?>" download="<?=$name.'.'.$ext;?>" class="btn-default"> <i class="fa fa-download"></i>  Resume  </a>
										<?php
										}
										else
										{
										?>
										<a class="btn-default"> <i class="fa fa-download"></i>No Resume  </a>
										<?php
										}
										}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#myModal_buy_a_plan"> <i class="fa fa-download"></i> Resume   </a>
										<?php
										}
										$availableemail=$isassign->TotalEmail-$isassign->UseEmail;
										$availableviewcontact=$isassign->ViewContact-$isassign->UseViewContact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$mval->Email;?>');" <?php } elseif(isset(Yii::$app->session['Campusid'])){ ?> onclick="mailtosemp('myModal_email','<?=$mval->Email;?>');" <?php }else{?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$mval->UserId])?>" target="_blank" onclick="viewcontact();" <?php } elseif(isset(Yii::$app->session['Campusid'])){ ?> href="<?= Url::toRoute(['site/customerdetail','UserId'=>$mval->UserId])?>" target="_blank" <?php } else{ ?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							 
				      </div>
				<?php
                        }
                     }
					 else
					 {
							  echo "No Student registered in this category";
					 }
                     ?>

			 <!-- Modal -->
  <div class="modal fade" id="myModal_buy_a_plan" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h4 class="modal-title">Buy a Plan</h4>
        </div>
									    <?php
		 if($allplan)
		 {
			foreach($allplan as $key=>$pland)
			{
				?>
									   <div class="widget_inner">
											   <div class="panel price panel-red">
												<div class="panel-body text-center">
													<p class="lead" ><strong><?=$pland->PlanName;?></strong></p>
												</div> 
												<ul class="list-group list-group-flush text-center width-half"> 
											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
											   <li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li> 
												</ul>
												<div class="panel-footer">
													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
												</div>
											</div>
									   <!-- /PRICE ITEM -->
								       </div>
									   <?php
			}
		 }
		 ?>  	  
			   </div>  
			   </div>
		 	</div>
			<!-- Modal -->		 				
							<?php
							echo LinkPager::widget([
								'pagination' => $pages,
							]);
							?>
                        </div>
      </div>
		    </div>
		<div class="border"></div>
        
<!-----myModal_email------>
<div class="modal fade" id="myModal_email" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mail To Student</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<div class="form-group" style="display: none;">
		<?= $form->field($mailmodel, 'EmployeeId')->textInput(['id'=>'EmployeeId','maxlength' => true])->label(false) ?>
		</div>
		<div class="form-group">
		<?= $form->field($mailmodel, 'Subject')->textInput(['maxlength' => true,'Placeholder'=>'Subject'])->label(false) ?>
		</div>
		<div class="form-group">
		<?= $form->field($mailmodel, 'JobId')->dropDownList($allpost)->label(false) ?>
		</div>
								  <div class="col-md-12 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Description*  </label>
										
								<?= $form->field($mailmodel, 'MailText')->widget(TinyMce::className(), [
									'options' => ['rows' => 6],'class'=>'form-control textarea-small',
									'language' => 'en_CA',
									'clientOptions' => [
										'plugins' => [
											"advlist autolink lists link charmap print preview anchor",
											"searchreplace visualblocks code fullscreen",
											"insertdatetime media table  paste spellchecker"
										],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								])->label(false);?>
                              </div> 
							   
							  </div>
							  
							  <div class="col-md-12 col-sm-12 col-xs-12">
							
									<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green']) ?>
								</div>
								</div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
			   </div>  
			   </div>
		 	</div>
<!-----myModal_email------>

<script type="text/javascript">
		function mailtoemp(id) {
			var email='';
			$('.profile-content .empch:checkbox:checked').each(function(){
				email+=$(this).next().find('.empid').val()+'|';
			});
			$('#EmployeeId').val(email);
			$('#'+id).modal('show');
        }
		function mailtosemp(id,email) {
			$('#EmployeeId').val(email);
			$('#'+id).modal('show');
        }
		
		function download() {
		  <?php
		  if(isset(Yii::$app->session['Campusid']))
		  {
					?>
					var i=0;
			$('.profile-content .empch:checkbox:checked').each(function(){
					i++;
					if (i<=totaldownload) {
                        eid+=$(this).next().find('.employeeid').val()+',';
                    }
			});
			window.location.href="<?= Url::toRoute(['site/exportcandidate'])?>?Eid="+eid;
					<?php
		  }
		  else
		  {
					?>
            var eid='';
			var totaldownload=<?=$isassign->TotalDownload-$isassign->UseDownload;?>;
			if (totaldownload>0) {
		  
		    var i=0;
			$('.profile-content .empch:checkbox:checked').each(function(){
					i++;
					if (i<=totaldownload) {
                        eid+=$(this).next().find('.employeeid').val()+',';
                    }
			});
			window.location.href="<?= Url::toRoute(['site/exportcandidate'])?>?Eid="+eid;
			}
			else
			{
					alert("Your data plan is already used");
			}
			<?php
		  }
		  ?>
        }
		
		function cvdownload() {
            $.ajax({url:"<?= Url::toRoute(['site/cvdownload'])?>",
				   success:function(results)
				   {
					
				   }
			});
        }
		
		function viewcontact() {
            $.ajax({url:"<?= Url::toRoute(['site/contactview'])?>",
				   success:function(results)
				   {
					
				   }
			});
        }
</script>