<?php
$this->title = 'Applied Job';
// var_dump($model);
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\widgets\ListView;
?>
<style>.deletejob{top: 26px;}</style>
<div id="wrapper">
   <!-- start main wrapper -->
   <div class="inner_page">
      <div class="container">
         <div class="col-lg-9  col-md-9 col-sm-9 col-xs-12" id="mobile_design">
            <h4>
               <i class="glyphicon glyphicon-briefcase">
               </i>   You Have applied 
               <?=Yii::$app->session['NoofjobAppliedc']; ?> Job
            </h4>
            <?php
                Pjax::begin(['id' => 'postcomments-pjax']);
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => ['class' => 'item'],
                    'itemView' => '_appliedJob',
                    'summary' => '',
                    'emptyText' => 'No Posts Found',
                ]);
                Pjax::end();
               ?>
         </div>
         <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="right-side">
            <div class="job-oppening-title-right mar_less">
               <h4>Top Jobs</h4>
            </div>
            <div class="rsw">
               <aside>
                  <div class="widget">
                     <ul class="related-post">
                        <?php
                           if ($topjob) {
                           foreach ($topjob as $tkey => $tvalue) {
                           ?>
                        <li><a
                           href="
                           <?= Url::base().'/campus-post/'.$tvalue->Slug;?>"> 
                           <?=$tvalue->CompanyName;?>  
                           </a>
                           <span>
                           <i class="fa fa-suitcase">
                           </i>Designation:  
                           <?=$tvalue->Designation;?>
                           </span>
                           <span>
                           <i class="fa fa-calendar">
                           </i>Place:  
                           <?=$tvalue->Location;?> 
                           </span>
                           <span>
                           <i class="fa fa-clock-o">
                           </i>Post Time: 
                           <?=date('h:i A',strtotime($tvalue->OnDate));?> 
                           </span>
                        </li>
                        <?php
                           }
                           }
                           ?>
                     </ul>
                  </div>
               </aside>
            </div>
            <div class="clearfix">
            </div>
            <!--adban_block main -->
            <div class="adban_block">
               <img src="<?=$imageurl;?>images/adban_block/ban.jpg">
            </div>
            <!-- adban_block  main -->
         </div>
      </div>
   </div>
   <div class="border"></div>
</div>