<?php

echo $this->title = 'Campus Post - My Career Bugs';

//var_dump($model);

use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

use yii\widgets\LinkPager;

use common\models\Plan;
use common\models\States;
use common\models\Cities;

use common\models\PlanAssign;

$plan=new Plan();

$allplan=$plan->getAllplan();


$planassign=new PlanAssign();

$isassign = $planassign->isAssign(Yii::$app->session['Employerid']);
$daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView',$isassign->plan->PlanId,date("Y-m-d"));
//echo $isassign->plan->total_daily_view_contact."==";
//echo $daily_Contact;die('=');
$availableviewcontact = $isassign->plan->total_daily_view_contact-$daily_Contact;
$contact_limit_reached="myModal_buy_a_plan";
if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $isassign->plan->total_daily_email<=$daily_Contact) {
	$contact_limit_reached="daily_limit_reach";
}
$url = '/backend/web/';

?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<style>
.inner_page{padding-top:25px;}  
#desc32{height:50px; overflow:hidden; margin-bottom:10px;}
#orange_payment_block{display:none;}
.btn-default.purple-1{background:#6c146b}
.btn-default.purple-1:hover{color:#fff;}
</style>

<div id="wrapper"><!-- start main wrapper -->

		 

	

		  <div class="inner_page">

			<div class="container">

		

<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="right-side"> 

	 <h4>Job Filter</h4>
	 <div class="col-md-122">
		<a href="<?= Url::toRoute(['campus/campuspost']);?>">Clear </a>
	</div>
	 
	 
	 
            <div class="panel-group" id="accordion">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <h4 class="panel-title">

                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne21">

							 Latest</a>

                        </h4>

                    </div>

                    <div id="collapseOne21" class="panel-collapse collapse">

                        <div class="panel-body">
								<?php
        if (isset($_GET['latest'])) {
            $latest = $_GET['latest'];
        } else {
            $latest = '';
        }

        ?>
								<select class="form-control" id="latest" onchange="getcampussearch(this.value);">
										<option value="">Select</option>
										<option value="1"
											<?php if($latest==1) echo "selected='selected'";?>>1 Days</option>

										<option value="3"
											<?php if($latest==3) echo "selected='selected'";?>>3 Days</option>

										<option value="7"
											<?php if($latest==7) echo "selected='selected'";?>>7 Days</option>

										<option value="15"
											<?php if($latest==15) echo "selected='selected'";?>>15 Days</option>

										<option value="30"
											<?php if($latest==30) echo "selected='selected'";?>>30 Days</option>

									</select>

								</div>

                    </div>
 </div>
 
  </div>
 

            <div class="panel-group" id="accordion">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <h4 class="panel-title">

                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">

							 Location</a>

                        </h4>

                    </div>

                    <div id="collapseOne" class="panel-collapse collapse">

                        <div class="panel-body">
							<?php
								$stateId = "";
								if($_GET['state']){
									$stateData = States::find()
										   ->select('StateId')
										   ->where(['StateName' => $_GET['state']])
										   ->one();
									$stateId = $stateData->StateId;   
								}?>
                            <select class="form-control bfh-states states" id="state" data-country="US" data-state="CA" onchange="getcampussearch(this.value);"><option value="">Select</option></select>
							<br/>
							<?php $cityList = Cities::getCityList($stateId);?>
							<select style="width:100%" class="form-control" id="city" onchange="getcampussearch(this.value);">
								<option value="">Select City</option>
								<?php foreach ($cityList as $cityId => $CityName) { ?>
								<option <?=((isset($_GET['city']) && $_GET['city'] == $CityName)?"selected":"");?> value="<?=$CityName;?>"><?=$CityName;?></option>
								<?php  } ?>
							</select>
                        </div>
						
						

                    </div>
 </div>
 
 
     <div class="panel panel-default">
 
                    <div class="panel-heading">

                        <h4 class="panel-title">

                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">

							 College Name</a>

                        </h4>

                    </div>

                    <div id="collapse2" class="panel-collapse collapse">

                        <div class="panel-body">
							
							<?php

    if (isset($_GET['collegeby'])) {

        $collegeBy = explode(",", $_GET['collegeby']);
    } else {

        $collegeBy = array();
    }

    foreach ($collegeList as $collegeId => $collegeName) {
		
        ?>

				<span>

										<div class="checkbox">

											<label> <input type="checkbox" class="collegeby"
												value="<?=$collegeId;?>" onclick="getcampussearch(this.value);" 
												<?php if(in_array($collegeId,$collegeBy)) echo "checked";?> college="<?=$collegeName;?>">

												<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>

												<a><?=$collegeName;?> </a>

											</label>

										</div>

									</span>

				<?php
    }

    ?>
                          
                        </div>

                    </div>
                     </div>      
                    
                        <div class="panel panel-default">
                            
                            
                    <div class="panel-heading">

                        <h4 class="panel-title">

                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">

							 Education</a>

                        </h4>

                    </div>

                    <div id="collapse3" class="panel-collapse collapse">

                        <div class="panel-body">

                            <select id="education-course" class="questions-category form-control" tabindex="-1" aria-hidden="true" name="CampusPost[Qualification]"  onchange="getcampussearch(this.value);">
								<option value="">Select College</option>
								<?php foreach($NaukariQualData as $key1 => $value1) { ?>
									<option  <?=((isset($_GET['course']) && $_GET['course'] == $value1->name)?"selected":"");?> value="<?=$value1->name;?>"><?=$value1->name;?></option>
								<?php }?>
							</select> 
                        </div>
                        <div class="panel-body">

                            <select id="education-specialization" class="questions-category form-control" tabindex="-1"  aria-hidden="true" name="CampusPost[Specialization]"  onchange="getcampussearch(this.value);">
								<option value="">Select Specialization</option>
								<?php foreach($naukari_specialization as $value) { ?>
									<option  <?=((isset($_GET['specialization']) && $_GET['specialization'] == $value->name)?"selected":"");?> value="<?=$value->name;?>"><?=$value->name;?></option>
								<?php }?>
							</select>

                        </div>

                    </div>
                </div>

            </div>

		

		

		<div class="spacer-5"></div>

 

                           
<!-- <div class="widget-heading"><span class="title"> Our Plans  </span></div>  -->
                             

                          <div id="orange_payment_block">     
  
								  <?php

		 if($allplan)

		 {

			foreach($allplan as $key=>$pland)

			{

				?>

									   <div class="widget_inner">

											   <div class="panel price panel-red">

												<div class="panel-body text-center">

													<p class="lead" ><strong><?=$pland->PlanName;?></strong></p>

												</div> 

												<ul class="list-group list-group-flush text-center width-half"> 

											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>

											   <li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li> 

												</ul>

												<div class="panel-footer">

													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>

												</div>

											</div>

									   <!-- /PRICE ITEM -->

								       </div>

									   <?php

			}

		 }

		 ?>

		 

							</div>

							

                   	</div>

					

					 

					 <div class="col-lg-9  col-md-9 col-sm-9 col-xs-12" id="mobile_design">



					 <div class="pannel_header margin_top" style="margin-bottom:0px;">

					 

					        

					    <div class="width-10" id="select_per_page">	

                            <div class="form-groups">

							<div class="col-sm-8">

						      <label style="padding-top:12px;">Result Per page </label>

							 </div>

						<div class="col-sm-4">

                            <?php

							if(isset($_GET['perpage']))

							{

							$perpage=$_GET['perpage'];

							}else{$perpage=10;}

							?>

                              <select id="form-filter-location" name="location" data-minimum-results-for-search="Infinity" class="form-control" tabindex="-1" aria-hidden="true" onchange="getresultperpage(this.value,'campuspost');">

								<option value="10" <?php if($perpage==10) echo "selected='selected'";?>>10</option>

                                <option value="20" <?php if($perpage==20) echo "selected='selected'";?>>  20  </option>

                                <option value="30" <?php if($perpage==30) echo "selected='selected'";?>>  30 </option>

                                <option value="40" <?php if($perpage==40) echo "selected='selected'";?>>  40  </option>

								<option value="50" <?php if($perpage==50) echo "selected='selected'";?>>  50  </option> 

                              </select> 

                            </div>

							 </div>

                          </div>

					 </div>

					 <div class="pannel_header" style="margin: 0px;">

							<span id="searchdivcampuss" style="margin-left: 10px;font-size: 14px;color: #f16b22;"></span>

						</div>

                    <?php
                    //echo "<pre>";print_r($model);die();
                    if($model)

                    {

                        foreach($model as $key=>$value)

                        {
							
							
                        ?>

					  <div class="col-md-12 col-sm-12 col-xs-12">

					 <div class="profile-content payment_list">

                                <div class="card">

                                    <div class="firstinfo">
										<?php if(!empty($value->campus->photo->Doc)){?>
											<img src="<?=$url;?><?=$value->campus->photo->Doc;?>" alt="" class="img-circle img-responsive">
										<?php }else{?>
											<img src="<?=$imageurl . 'images/user.png';?>" alt="" class="img-circle img-responsive">
										<?php }?>
                                    	
                                        <div class="profileinfo">

                                            <h1><?=$value->JobTitle;?></h1>

											<small><?=$value->campus->CollegeName;?></small> -  <?=$value->City;?>  

											<div class="spcae1"></div>											

											<div class="rows">
											    	 <div class="col-md-12 col-sm-12 col-xs-12" id="desc32">  <?=$value->JobDescription;?></div> 
											    
											        <div class="col-md-6 col-sm-6 col-xs-12"> 
											        
											        	<ul class="commpany_desc">	
											        		<li>No of Candidates: <?=$value->NoofVacancy;?> Candidate   </li> 
											        			<?php if(!empty($value->specialization)){ ?>
															<li>Specialization:  <?=$value->specialization;?>   </li>
														<?php } ?>
											        	</ul>
											        </div>
											        
											         <div class="col-md-6 col-sm-6 col-xs-12"> 
											        
											        	<ul class="commpany_desc">	
											        		<li>Venue : <?=$value->Venue;?>   </li>  
											            	 
											        	</ul>
											        </div>
											        <div class="clear"></div>
											        
	 <style>#desc32 p{font-size:12px} #desc32 p br{display:none}</style>
										
											</div> 

												   <div class="profile-skills">

                                                    <?php
													
													
                                                   ?>
                                                   <?php if(!empty($value->course)){ ?>
															<span><?=$value->course;?>   </span>
														<?php } ?>
													</div> 

                             

													 <p class="last_update">Job Post: <?=date('d-m-Y',strtotime($value->OnDate));?></p>

													 <p class="last_update_main" style="display:none">Shortlisted by 0 Recruiters recently</p>

										</div>

                                    </div>

		 	 </div>

									 <div class="contact_me">

                                        <?php

                                        //$availableviewcontact=$isassign->ViewContact-$isassign->UseViewContact;
										$link = Url::toRoute([
											'campus/postdetail',
											'JobId' => $value->CampusPostId
										]);
										$InterestedLink = Url::toRoute([
											'campus/appliedCampus',
											'JobId' => $value->CampusPostId
										]);
										 ?>
<?php if($value->campusPostApplied){?>
	<a href="javascript:void(0);" class="btn-default purple-1"><i class="fa fa-check"></i> Applied </a>
<?php }else{?>
	<a href="<?= Url::toRoute(['campus/appliedcampus','CampusPostId'=>$value->CampusPostId])?>" class="btn-default">Interested</a>
<?php }?>
<a href="<?=$link?>" class="btn-default" target="_blank">Post Detail</a>
<a <?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['campus/collegeprofile','UserId'=>$value->campus->UserId])?>" target="_blank" onclick="viewcontact();" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-user"></i>  College  Profile </a>
   </div>

                            </div>

							 

				      </div>

				<?php

                        }

                    }else{
						echo '<br/><div class="text-left"><h5>No campus post found for this search. Please try another phrase. </h5></div>';
						
						
					}	

                    ?>

			 <!-- Modal -->

  <div class="modal fade" id="myModal_buy_a_plan" role="dialog">

    <div class="modal-dialog">

      <div class="modal-content">

	     <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">×</button>

          <h4 class="modal-title">Buy a Plan</h4>

        </div>

									    <?php

		 if($allplan)

		 {

			foreach($allplan as $key=>$pland)

			{

				?>

									   <div class="widget_inner">

											   <div class="panel price panel-red">

												<div class="panel-body text-center">

													<p class="lead" ><strong><?=$pland->PlanName;?></strong></p>

												</div> 

												<ul class="list-group list-group-flush text-center width-half"> 

											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>

											   <li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>

												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li> 

												</ul>

												<div class="panel-footer">

													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>

												</div>

											</div>

									   <!-- /PRICE ITEM -->

								       </div>

									   <?php

			}

		 }

		 ?>

		 

			   </div>  

			   </div>

		 	</div>

			<!-- Modal -->

            <div class="col-md-12 col-sm-12 col-xs-12">

							<?php

							echo LinkPager::widget([

								'pagination' => $pages,

							]);

							?>

            </div>

                        </div>

      </div>

		    </div>

		<div class="border"></div>
<div class="modal fade" id="daily_limit_reach" role="dialog">
    			<div class="modal-dialog">
      				<div class="modal-content">
	     				<div class="modal-header">
          					<button type="button" class="close" data-dismiss="modal">&times;</button>
          					<h4 class="modal-title">You have reached your daily limit.</h4>
        				</div>  
			   		</div>  
			   </div>
		 	</div>
<script type="text/javascript">
	$(document).ready(function() {
		selectpositionnew($("#crole").val());
	});
	$(document).on('change', '.skillby', function() {
		getcampussearch();
	});
    function viewcontact() {

            $.ajax({url:"<?= Url::toRoute(['site/contactview'])?>",

				   success:function(results)

				   {

					

				   }

			});

        }
        function selectpositionnew(pid){
		
	    $.ajax({
	    	url:mainurl+"campus/skills?roleid="+pid,
            type : 'GET',
            success:function(results)
            {
                $('#jobbyskill').html(results);
            }
        });
	}

	var data = [{id: '',text: 'Select Specialization'}];
	var Ldata = [{id: '',text: 'Select Course'}];
	<?php 
	$is_otrher = 0;
	foreach ($naukari_specialization as $key => $value) { 
		if($value->name=='Other' && $is_otrher == 1){
			continue;
		}
		?>
		var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
		data.push(d);
	<?php if($value->name=='Other'){$is_otrher = 1;} } ?>

	<?php foreach ($NaukariQualData as $key1 => $value1) { ?>
		var ld = {id: '<?=$value1->name?>',text: '<?=$value1->name?>'};
		Ldata.push(ld);
	<?php }  ?>
	$("#education-specialization1").select2({
  		data: data,
  		maximumSelectionLength: 5
	});
	$("#education-course1").select2({
  		data: Ldata
	});
	<?php if (isset($_GET['course'])) { ?>
		$("#education-course").val("<?=$_GET['course']?>");
		
	<?php } ?>
	<?php if (isset($_GET['specialization'])) { ?>
		$("#education-specialization").val("<?=$_GET['specialization']?>");
		
	<?php } ?>
	function getcampussearch() {
    var state=$('#state').val();
    var city=$('#city').val();
    var latest=$('#latest').val();
    var role = $('#crole').val();
    var course =$('#education-course').val();
    var specialization =$('#education-specialization').val();
    var skill = $('.skillby:checked').map(function () {
						return this.value;
					}).get();
	var collegeIds = $('.collegeby:checked').map(function () {
					return this.value;
				}).get();
    window.location.href='/campus'+"/campuspost?state="+state+"&city="+city+"&course="+course+"&specialization="+specialization+"&skill="+skill+"&latest="+latest+"&collegeby="+collegeIds;
	}
	//setTimeout(function(){
		var searchresult = "";
		<?php if (isset($_GET['state']) && !empty($_GET['state'])) { ?>
				searchresult+=' > '+"<?=$_GET['state']?>";
				$("#state").val("<?=$_GET['state']?>");
			<?php } ?>
		<?php if (isset($_GET['city']) && !empty($_GET['city'])) { ?>
				searchresult+=' > '+"<?=$_GET['city']?>";
				$("#state").val("<?=$_GET['city']?>");
			<?php } ?>	
		<?php if (isset($_GET['course']) && !empty($_GET['course'])) { ?>
				searchresult+=' > '+"<?=$_GET['course']?>";
			<?php } ?>
			<?php if (isset($_GET['specialization']) && !empty($_GET['specialization'])) { ?>
				searchresult+=' > '+"<?=$_GET['specialization']?>";
			<?php } ?>
			<?php if (isset($_GET['skill']) && !empty($_GET['skill'])) { ?>
				searchresult+=' > '+"<?=$_GET['skill']?>";
			<?php } ?>
			<?php if (isset($_GET['latest']) && !empty($_GET['latest'])) { ?>
				searchresult+=' > '+$('#latest option:selected').text();
			<?php } ?>
			<?php if (isset($_GET['collegeby']) && !empty($_GET['collegeby'])) { ?>
				searchresult+=' > '+$('.collegeby:checked').map(function() {return $(this).attr('college');}).get().join(',')
				
			<?php } ?>
			if(searchresult!=""){
				searchresult = "Results "+searchresult;
			}
			$('#searchdivcampuss').html(searchresult);
	//},2000);
</script>