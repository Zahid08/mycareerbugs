<?php
$this->title = 'Campus Profile';
$csrfToken = Yii::$app->request->getCsrfToken();
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">
	<div id="wrapper"><!-- start main wrapper -->
		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
			   <div class="col-md-2 col-sm-2 col-xs-12">
			                 <div class="user-profile">
                                    <img src="<?=Yii::$app->session['CampusDP'];?>" alt="" class="img-responsive center-block ">
                                    <h3><?=$campus->CollegeName;?></h3>
                                </div> 
			      	</div>
		         <div class="col-md-10 col-sm-10 col-xs-12"> 
                            <div class="job-short-detail">
                                <div class="heading-inner">
                                    <p class="title"> College Details</p>
									
				<a href="" data-toggle="modal" style="right: 40% !important;" data-target="#myModal_collegepic" class="uploadcv">  <i class="fa fa-image orange"></i> College Picture</a>
									<?php
									$na=explode(" ",$campus->Name);
									$name=$na[0];
									$extar=explode(".",$campus->presentation->Doc);
									$ext=$extar[1];
									?>
				<a href="<?=($campus->PresentaionId!=0)?$url.$campus->presentation->Doc:'#/';?>"  download="<?=($campus->PresentaionId!=0)?$name.'-Presentation.'.$ext:'';?>" class="uploadcv">  <i class="fa fa-file-pdf-o orange"></i>Presentation</a>
				<a href="<?= Url::toRoute(['campus/campusprofileedit'])?>">  <i class="fa fa-pencil-square-o orange"></i> Edit Profile</a>
                                </div>
								
								
								
                                <dl>
                                    <dt>About College</dt>
				                    <dd><?php echo nl2br($campus->CompanyDesc); ?></dd>
                                    <dt>College Name</dt>
                                    <dd><?=$campus->CollegeName;?></dd>

                                    <dt> Company Email </dt>
                                    <dd><?=$campus->Email;?></dd>
									<?php
									$address=explode(",",$campus->Address);
									?>
                                    <dt>Address1:</dt>
                                    <dd><?=trim($address[0]);?></dd>
									
									<dt>Address2:</dt>
                                    <dd><?=trim($address[1]);?></dd>
									 
									 <dt>Website:</dt>
                                    <dd><?=$campus->CollegeWebsite;?></dd>
									
									 <dt> Mobile Number  </dt>
                                    <dd> <?=$campus->MobileNo;?>   </dd> 
									
									 <dt>  Contact Number</dt>
                                    <dd> <?=$campus->ContactNo;?> </dd>

                                    <dt>City:</dt>
                                    <dd><?=$campus->City;?></dd>

                                    <dt>State:</dt>
                                    <dd><?=$campus->State;?></dd>

                                    <dt>Country:</dt>
                                    <dd><?=$campus->Country;?></dd>
									
				<dt>Pincode</dt>
                                    <dd><?=$campus->PinCode;?></dd>
				    <dt>Course Provided</dt>
				    <dd>
					<?php
					$i=0;
					foreach($campuscourse as $ck=>$cval)
					{
						$i++;
						?>
						<div style="width: 100%;height: auto;float: left;margin-top: 20px;"><span style="width: 20%;float: left;">Course <?=$i;?> :</span> <span style="padding: 5px;border: 1px solid #f16b22;float: left;"><?=$cval['course'];?></span></div>
						<div style="width: 100%;height: auto;float: left;margin-top: 10px;">
							<span style="width: 20%;float: left;">Specialization :</span>
						<?php
						foreach($campuscourse[$ck]['spec'] as $sp=>$sval)
						{
						?>
						<span style="padding: 5px;border: 1px solid #f16b22;"><?=$sval;?></span>
						<?php
						}
						?>
						</div><br/>
					<?php
					}
					?>
				    </dd>
					
                                </dl>
                            </div>
 
                        </div>
						 
	 <div class="clearfix"> </div>
	
						
				<div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="resume-box" style="display: none;">
                                <div class="heading-inner">
                                    <p class="title">  Collage Description </p>
                                </div>
                                <div class="row education-box">
                                    
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="degree-info">
                                            <p><?php echo nl2br($campus->CompanyDesc);?></p>
                                             </div>
                                    </div>
                                </div>
                                
                            </div>

							<div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Dean / Director / Principal </p>
                                </div>
                                <div class="row education-box">
							
									<?php
									if($campus->PhotoId!=0)
									{
										$campusphoto=$url.$campus->photo->Doc;
									?>
									<div class="col-xs-12 col-md-2 col-sm-2">
									<img style="" src="<?=$url.$campus->photo->Doc;?>"  alt="" class="img-responsive center-block ">
									</div>
									<?php
									}
									?>
									<div class="col-xs-12 col-md-2 col-sm-2">
										<?=$campus->Dean;?>
									</div>
                                    <div class="col-xs-12 col-md-10 col-sm-10">
										<label>About Me : </label>
                                        <div class="degree-info">
                                            <p><?php echo nl2br($campus->AboutYou);?></p>
											 
                                             </div>
											 
                                    </div>
                                </div>
                                
                            </div>
							
							
							
							
								<!--<div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Collage pictures  </p>
                                </div>
                                <div class="row education-box">
                                     
                                </div>
                                
                            </div>-->
                        </div>
						 
						 <div class="clearfix"> </div>
  </div>
            </div>
       </div>
		<div class="border"></div>
		
<div class="modal add-resume-modal" id="myModal_collegepic" tabindex="-1" role="dialog" aria-labelledby="">
            <div class="modal-dialog modal-md" role="document" style="width: 650px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">College Picture</h4>
                    </div>
					 <div class="modal-body">
						<div class="row education-box">
<div class="col-xs-12 col-md-12 col-sm-12">
		<?php
				if($campus->collegepics)
				{
					?>
            <div class="col-sm-10 margin_0auto">
			   <div class="col-xs-12" id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div class="col-sm-12" id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel1">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
									<?php
									foreach($campus->collegepics as $key=>$value)
									{
									?>
                                    <div class="item <?=($key==0?'active':'');?>" data-slide-number="<?=$key;?>">
                                        <img src="<?=$url.$value->pic->Doc;?>" >
									</div>
									<?php
									}
									?>

                                <!-- Carousel nav -->
                                <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Slider-->
        </div>
		 <div class="col-sm-12" id="slider-thumbs">
                <!-- Bottom switcher of slider -->
			  <ul class="hide-bulletss">
				<?php
					foreach($campus->collegepics as $key=>$value)
					{
				?>
                    <li class="col-sm-3"> 
					    <a class="thumbnail" id="carousel-selector-<?=$key;?>"> <img src="<?=$url.$value->pic->Doc;?>"> </a>
                    </li>
				<?php
					}
					?>
                </ul>
            </div>
		 <?php
				}
				?>
</div>
									 </div>
					 </div>
					<div class="modal-footer">
				
                    </div>
                </div>
            </div>
	</div>