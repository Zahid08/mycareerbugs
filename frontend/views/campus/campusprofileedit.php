<?php
$this->title = 'Edit Profile';
$csrfToken = Yii::$app->request->getCsrfToken();
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Cities;
use common\models\States;
use common\models\Specialization;
use common\models\NaukariSpecialization;

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

$url = '/backend/web/';

?>



<style>
.editcontainer {
	width: 20px;
	height: 20px;
	padding: 4px;
	float: right;
	cursor: pointer;
}

.select-wrapper {
	background: url(images/edit.png) no-repeat;
	background-size: cover;
	display: block;
	position: relative;
	width: 15px;
	height: 15px;
}

#image_src {
	width: 15px;
	height: 15px;
	margin-right: 100px;
	opacity: 0;
	filter: alpha(opacity = 0); /* IE 5-7 */
}
</style>

<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">

<div id="wrapper">
	<!-- start main wrapper -->

	<div class="inner_page second">

		<div class="container">

			<div id="profile-desc">

                <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

			    <div class="col-md-2 col-sm-2 col-xs-12">

					<div class="user-profile">

						<img src="<?=Yii::$app->session['CampusDP'];?>"
							alt="<?php echo $campus->Name ;?>"
							class="img-responsive center-block ">

						<div class="editcontainer">

							<span class="select-wrapper"> <input type="file"
								name="AllUser[LogoId]" id='image_src' />

							</span>

						</div>



						<h3>
							<input type="text" class="form-control" required
								name="AllUser[CollegeName]" id="Name"
								value="<?php echo $campus->CollegeName ;?>" placeholder="Name">
						</h3>
					</div>
				</div>

				<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="job-short-detail" id="edit_profile_page">
						<div class="heading-inner">
							<p class="title">Campus details</p>
									<?= Html::submitButton('<i class="fa fa-floppy-o orange"></i> Save Changes') ?>
                                </div>
						<dl>
							<dt style="display: none;">Coordinator Name</dt>
							<dd style="display: none;">
								<input type="text" class="form-control" name="AllUser[Name]"
									id="Name" required placeholder="Coordinator Name"
									value="<?=$campus->Name;?>" />
							</dd>
							<dt>Coordinator Contact No</dt>
							<dd>
								<input type="text" class="form-control"
									placeholder="Coordinator Contact Nos"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" class="form-control"
									required name="AllUser[MobileNo]" id="MobileNo" maxlength="10"
									value="<?=$campus->MobileNo;?>" />
							</dd>
							
							<?php 
							 $states = States::getStateList();
							
							?>
							<dt>State:</dt>
							<dd>
									
					<?php $state=$campus->State;$city=$campus->City;?>
				   <select
									class="questions-category states form-control"
									tabindex="0" aria-hidden="true" id="stateId"
									name="AllUser[State]" required>
									<option value="">Select State</option>
									<?php foreach ($states as $key => $s) : ?>
										<?php $isSelected = $s == $state ? 'selected' : ''?>
										<option value="<?php echo $s?>" <?php echo $isSelected?>> <?php echo $s?> </option>
									<?php endforeach;?>
									
								</select>							
							</dd>
							<dt>City:</dt>
							<dd>
								<select
									class="questions-category cities form-control"
									tabindex="0" aria-hidden="true" id="cityId"
									name="AllUser[City]" required>

									<option value="">Select City</option>

								</select>
							</dd>
							
							<dt>Country:</dt>
							<dd>
								<input type="text" class="form-control" name="Alluser[Country]"
									id="country" placeholder="Somewere at Antarctica "
									value="<?=$campus->Country;?>">
							</dd>

							<?php
                            $address = explode(",", $campus->Address);
                            ?>
                                     <dt>Address1:</dt>
							<dd>
								<input type="text" class="form-control" name="AllUser[Address1]"
									required placeholder="Address 1"
									value="<?=trim($address[0]);?>" />
							</dd>
							<dt>Address2:</dt>
							<dd>
								<input type="text" class="form-control" name="AllUser[Address2]"
									placeholder="Address 1" value="<?=trim($address[1]);?>" />
							</dd>
							<dt>Pincode:</dt>
							<dd>
								<input type="text" class="form-control" name="AllUser[PinCode]"
									required placeholder="Pincode"
									onkeypress="return numbersonly(event)" maxlength="6"
									value="<?=$campus->PinCode;?>" />
							</dd>



							<dt>About College :</dt>

							<dd>
								<textarea class="form-control textarea-small"
									name="AllUser[CompanyDesc]" required><?=$campus->CompanyDesc;?></textarea>
							</dd>

							<dt>College Website :</dt>

							<dd>
								<input type="text" class="form-control"
									name="AllUser[CollegeWebsite]" required
									placeholder=" College  Website"
									value="<?=$campus->CollegeWebsite;?>" />
							</dd>
							<dt>College Contact No</dt>
							<dd>
								<input type="text" class="form-control"
									name="AllUser[ContactNo]" required
									placeholder="College  Contact Nos"
									onkeypress="return numbersonly(event)" maxlength="11"
									value="<?=$campus->ContactNo;?>" />
							</dd>

							<dt>Dean / Director / Principal</dt>
							<dd>
								<input type="text" class="form-control" name="AllUser[Dean]"
									required placeholder=" Dean / Director / Principal "
									value="<?=$campus->Dean;?>" />
							</dd>

							<dt>About You</dt>
							<dd>
								<textarea class="form-control textarea-small"
									name="AllUser[AboutYou]"><?=$campus->AboutYou;?></textarea>
							</dd>
							<dt>Upload Your Photo</dt>
							<dd>
							    <div style="width: 100%; height: auto; float: left;">
                                    <?php if ($campus->photo) { ?>
                                        <div style="width: 110px; float: left; border: 1px solid #333; padding: 5px; margin: 5px; position: relative;" id="collegepic">
										    <img src="<?=$url.$campus->photo->Doc;?>" style="width: 100px;" />
										    <div style="position: absolute; z-index: 99999; background: red; color: #fff; font-size: 9px; padding: 2px; height: 15px; right: 0px; top: 0px; padding-top: 0px; margin-top: 0px;"	
										    onclick="delCamppic(<?=$campus->PhotoId;?>);">X</div>
									    </div>
                                        
                                    <?php  }  ?>
                                    </div>
                                    <div class="input-group">

									<label class="btn btn-default btn-file design-1"> Browse <input
										type="file" name="AllUser[PhotoId]" accept="image/*"
										style="display: none;"
										onchange="$(this).parent().next().html($(this).val());">

									</label> <span></span>
									<p>Supported Formats: .jpg, .png, .gif. Limit 200KB</p>
								</div>
							</dd>

							<dt>College Picture</dt>
							<dd>
								<div style="width: 100%; height: auto; float: left;">
                                    <?php
                                    if ($campus->collegepics) {
                                        foreach ($campus->collegepics as $key => $value) {
                                        ?>
                                            <div style="width: 110px; float: left; border: 1px solid #333; padding: 5px; margin: 5px; position: relative;" id="collegepic<?=$value->CollegePicId;?>">
    										    <img src="<?=$url.$value->pic->Doc;?>" style="width: 100px; height: 100px;" />
    										    <div style="position: absolute; z-index: 99999; background: red; color: #fff; font-size: 9px; padding: 2px; height: 15px; right: 0px; top: 0px; padding-top: 0px; margin-top: 0px;"	onclick="delpics(<?=$value->CollegePicId;?>);">X</div>
    									    </div>
                                        <?php
                                        }
                                    }
                                    ?>

                                    </div>

								<div class="input-group">

									<label class="btn btn-default btn-file design-1"> Browse <input
										type="file" name="CollegePic[PicId][]" accept="image/*"
										style="display: none;"
										onchange="$(this).parent().next().html($(this).val());">

									</label> <span></span>



									<div id="morepicbox"></div>

									<p>Supported Formats: .jpg, .png, .gif. Limit 200KB</p>

									<input type="button" value="+Add More" class="btn btn-primary"
										onclick='AddMore()'/>

								</div>

							</dd>





							<dt>Upload Presentations</dt>

							<dd>

								<div class="input-group">

									<label class="btn btn-default btn-file design-1"> Browse <input
										type="file" id="presentationid" name="AllUser[PresentaionId]"
										accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,

text/plain, application/pdf"
										style="display: none;"
										onchange="$(this).parent().next().html($(this).val());">

									</label> <span></span>

									<p class="help-block-error"></p>

									<p>Supported Formats: .ppt,.pptx, .pps, .pdf. Limit 5MB</p>

								</div>

							</dd>

						</dl>
						
						<div class="row" >
						
							<div class="col-md-12" style="margin-top:30px;">
								<!--New Code-->
							<?php if(!empty($campuscourse)){
									$courseList = array();
									foreach($campuscourse as $courseRec){
										if(!empty($courseRec->SpecializationId)){
											$courseList[$courseRec->CourseId][] = $courseRec->SpecializationId;
										}else{
											$courseList[$courseRec->CourseId] = $courseRec->SpecializationId;
										}
										
									}
									$key = 0;
									foreach($courseList as $courseId => $specializationId){
								?>
								<div class="form-group course-specialization">
									
									<label for="email" class="col-sm-2 control-label">Courses
										Provided </label>
									
									<div class="col-sm-3">
										<select name="AllUser[Course][<?=$key;?>][course]" required id="CampusCourseId-<?=$key;?>"
												class="form-control campus-course" count="<?=$key;?>">
												<option value="">Select Course</option>
												<?php foreach($naukari_specialization as $course){?>
													<option value="<?=$course->id;?>" <?=(($courseId == $course->id)?"selected":"");?>><?=$course->name;?></option>
												<?php }?>
												</select>
									</div>
									<label for="email" class="col-sm-2 control-label">Specializations
										</label>
										
										<div class="col-sm-4">
											<?php $specializationData = NaukariSpecialization::getSpecializationList();?>
											<select name="AllUser[Course][<?=$key;?>][specialization][]" multiple="multiple"
													id="SpecializationId-<?=$key;?>" class="form-control campus-specialization">
													<?php if(!empty($specializationData)){
															foreach($specializationData as $specialId => $specialVal){
														?>
														<option value="<?=$specialId;?>" <?=(in_array($specialId, $specializationId)?"selected":"")?>><?=$specialVal;?></option>
													<?php }
													}?>
												</select> 
										</div>
									
								</div>
								<br style="clear:both"/><br/>
									<?php $key++;}
								}else{?>
								<div class="form-group course-specialization">
									<label for="email" class="col-sm-2 control-label">Courses
										Provided </label>
									
									<div class="col-sm-3">
										<select name="AllUser[Course][0][course]" required id="CampusCourseId-0"
												class="form-control campus-course" count="0">
												<option value="">Select Course</option>
												<?php foreach($naukari_specialization as $course){?>
													<option value="<?=$course->id;?>"><?=$course->name;?></option>
												<?php }?>
												</select>
									</div>
									<label for="email" class="col-sm-2 control-label">Specializations
										</label>
										
										<div class="col-sm-4">
											<select name="AllUser[Course][0][specialization][]" multiple="multiple"
													id="SpecializationId-0" class="form-control campus-specialization">
												</select> 
										</div>
								</div>
							<?php }?>
							
								<div id="add-more-course-specialization">
								
								</div>
								<br style="clear:both"/>
								<div class="form-group">
									<label for="email" class="col-sm-2 control-label">
										<a href="javascript:void(0)" id="add-course-specialization" class="btn btn-primary">+ Add More</a>
									</label>
									
								</div>
							</div>
						
						</div>

					<?php /*

						<div class="row" style="margin-bottom: 100px;">
							<div class="col-md-6">
								<div class="form-group">
									<label for="email" class="cols-sm-2 control-label">Courses
										Provided </label>
									<div class="cols-sm-10">
										<div class="input-group full">
											<select name="AllUser[Course]" id="CampusCourseId"
												class="form-control">
												<option value="">Select Course</option>
                                        <?php  
                                        $courseId = [];
                                
                                        foreach ($course as $_course){ ?>
                                        <option value="<?=$_course->CourseId;?>"><?=$_course->CourseName;?></option>
                                        <?php    //$courseId[] = $course->CourseId;
                                        
                                        // foreach ($naukari_specialization as $ckey => $cval) {
                                        //     $isSelected = in_array($cval->id, $courseId) ? 'selected' : 'sd';
                                           // echo $isSelected;?>
                                        <!--<option value="<?php //echo $cval->id;?>" <?php //echo $isSelected?>><?php //echo $cval->name;?></option>-->
                                        <?php } ?>
                                     </select>
										</div>
									</div>
								</div>
								
								

								<div class="form-group">
									<label for="email" class="cols-sm-2 control-label">Specializations
									</label>
									<div class="cols-sm-10">
										<div class="input-group full">
											<select name="AllUser[Specialization]" multiple="multiple"
												id="SpecializationId" class="form-control bfh-states">
												<?php 
												$specializationId = [];
												foreach ($campuscourse as $course){
												    $specializationId[] = $course->SpecializationId;
												}
												foreach ($NaukariQualData as $ckey => $cval) { 
												    $isSelected = in_array($cval->id, $specializationId) ? 'selected' : 'dsds';
												    //echo $isSelected;?>
                                                	<option value="<?=$cval->id;?>" <?= $isSelected?>><?=$cval->name;?></option>
                                                <?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							

							<div class="col-md-2" style="text-align: center;">
								<input type="button" value="ADD" id="addcourse"
									class="btn btn-primary" style="margin-top: 50px;" />
							</div>

							<div class="col-md-4">
								<label>You Have Chosse :</label><br />
								<div
									style="background-color: #fffab2; border: 1px solid #ebe480; height: 118px; overflow-x: hidden; overflow-y: auto; padding: 3px 0 2px 5px; width: 200px;"
									id="choosedval">
                                <?php
                                $cr = 0;
                                $allcmap = '';
                                foreach ($campuscourse as $ck => $cval) {
                                    if ($cval != '') {
                                        $allcmap .= '|' . $cval->CourseId . '-' . $cval->SpecializationId;
                                        if ($cr != $cval->CourseId) {
                                            ?>
								  <div class="catlabel" id="<?=$cval->CourseId;?>"><?=$cval->course->CourseName;?><div
											class="catclose" onclick="closecat(<?=$cval->CourseId?>);">X</div>
									</div>
								  <?php
                                        }
                                        $cr = $cval->CourseId;
                                        ?>
								  <div
										class="subcatlabel subcat<?=$cval->SpecializationId;?> cat<?=$cval->CourseId;?>"
										id="<?=$cval->CourseId.'.'.$cval->SpecializationId;?>"><?=$cval->specialization->Specialization;?><div
											class="catclose"
											onclick="closesubcat(<?=$cval->SpecializationId;?>,<?=$cval->CourseId?>);">X</div>
									</div>
								  <?php
                                    }
                                }
                                ?>
                            </div>
								<input type="hidden" name="AllUser[AllC]" id="allcmap"
									value="<?=$allcmap;?>" />
							</div>
						</div>
					
					*/?>
					</div>
				</div>
                 <?php ActiveForm::end(); ?>
						 
	 <div class="clearfix"></div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<div class="border"></div>
<script type="text/javascript">

	$(document).on('change', '.campus-course', function(){
		var value = $(this).val();
		var count = $(this).attr('count');
		 $.ajax({
	    	url:mainurl+"campus/specializationlist?courseId="+value,
            type : 'GET',
			dataType: 'JSON',
            success:function(results)
            {
				var specidata = [{id: '',text: 'Select Specialization'}];
				$.each(results, function(key, value) {
					var ds = {id: key,text: value};
					specidata.push(ds);
				});
                $("#SpecializationId-"+count).select2({
					data: specidata,
					maximumSelectionLength: 5
				});
            }
        });
		return false;
	});
	
	$(".campus-specialization").select2({
		maximumSelectionLength: 5
	});
    
	$("#SpecializationId").select2({
  		//data: Ldata,
  		maximumSelectionLength: 5
	});
    function AddMore(){
    	var addmore = "<br/><label class='btn btn-default btn-file design-1'>Browse";  
    	addmore += "<input type='file' name='CollegePic[PicId][]'";  
    	addmore += "accept='image' style='display:none;'";  
    	addmore += "onchange='$(this).parent().next().html($(this).val());'></label><span></span>";
    	$("#morepicbox").append(addmore);
    }
  setTimeout(function(){
		var loc = new locationInfo();
		$('#stateId').val('<?=$state;?>');
		var loc1='<?=$state;?>';
		 var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');
        if(stateId != ''){
        	loc.getCities(stateId);
        }
		setTimeout(function(){
			$('#cityId').val('<?=$city;?>');
		},2000);
	},5000);
	
	$(document).on('click', '#add-course-specialization', function(){
		var count = $('.course-specialization').length;
		//count = parseInt(count)+1;
		var options = $('#CampusCourseId-0').html();
		var html = '<div class="form-group course-specialization"><label for="email" class="col-sm-2 control-label">Courses Provided </label><div class="col-sm-3"><select name="AllUser[Course]['+count+'][course]" required id="CampusCourseId-'+count+'" class="form-control campus-course" count="'+count+'">'+options+'</select></div><label for="email" class="col-sm-2 control-label">Specializations </label><div class="col-sm-4"><select name="AllUser[Course]['+count+'][specialization][]" multiple="multiple" id="SpecializationId-'+count+'" class="form-control campus-specialization"></select></div></div><br style="clear:both"/><br style="clear:both"/>';
		$('#add-more-course-specialization').append(html);
		$('#CampusCourseId-'+count+' option:selected').removeAttr('selected');
		$(".campus-specialization").select2({
			maximumSelectionLength: 5
		});
		
	});
</script>