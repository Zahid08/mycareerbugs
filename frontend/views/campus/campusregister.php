<?php
$this->title = 'Campus Register';

$csrfToken = Yii::$app->request->getCsrfToken();

use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use common\models\NaukariSpecialization;

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

$url = '/backend/web/';

?>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	
	
	<style>

#first_sec {padding: 0;margin: 0 0 15px 0;background: #f2f2f2; clear:both; overflow:hidden;}
#first_sec .page-head {color: #fff;background: #6d136a;text-align: left;padding: 10px 20px;}
#campus_left .form-control {width: 78%;float: right;}
.form-horizontal .form-group {width: 100%;}
.form-horizontal .control-label {    padding: 13px 0 0 0;    line-height:13px;}
	
	
	 
.find_a_job{display:none;}	  
	 
@media only screen and (max-width: 767px) { 
.form-horizontal .form-group {margin: 0 0 10px 0;}
#blank_0{clear:both; overflow:hidden;height:5px;}
.select2.select2-container.select2-container--default{float:right;}

}
	</style>
	
<div id="wrapper">
	<!-- start main wrapper -->
	<div class="headline_inner" style="display:none">
		<div class=" container">
			<!-- start headline section -->
			<div class="row">
				<h2>Register with us to showcase your college</h2>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- end headline section -->
	</div>
	<div class="inner_page" style="padding-top:30px">
		<div class="container">
			<div class="row main">
				<div class="xs-12 col-sm-12 main-center" id="campus_left">

				 

                        <?php
$form = ActiveForm::begin([
                            'options' => [
                                'id' => 'campus_register_form',
                                'class' => 'form-horizontal',
                                'enctype' => 'multipart/form-data'
                            ]
                        ]);
                        ?>

						<?php
    if (isset(Yii::$app->session['SocialName']) && Yii::$app->session['SocialName'] != '') {
        $name = 'value="' . Yii::$app->session['SocialName'] . '" readonly';
    } else {
        $name = '';
    }
    if (isset(Yii::$app->session['SocialEmail']) && Yii::$app->session['SocialEmail'] != '') {
        $email = 'value="' . Yii::$app->session['SocialEmail'] . '" readonly';
    } else {
        $email = '';
    }
    ?>


<div id="first_sec">
					    	<h5 class="page-head">Login Details</h5>

<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="name">Email</label>
<input type="email" class="form-control" required name="AllUser[Email]" id="Email"placeholder="" <?=$email;?> />
									</div></div>




<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="password">Password</label>
<input type="password" required
									class="form-control" name="AllUser[Password]" id="password"
									placeholder="" autocomplete="off" />
									</div></div>

					 
 

<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">Confirm Password</label>
<input type="password" required
									class="form-control"
									onblur="return ConfirmPassword(this.value);" name="confirm"
									id="confirmpassword" placeholder="" />
									</div></div>

					 
	</div>

  
					
					
<div id="first_sec">
					    	<h5 class="page-head">College / University Details</h5>

					 
<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="email">Co-ordinator Name  </label>
  <input type="text"
									class="form-control" required name="AllUser[Name]" id="Name"
									placeholder="" <?=$name;?> /> 
									</div></div>





<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="email">
							Contact No  </label>
 <input type="text"
									class="form-control" placeholder=""
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" class="form-control"
									required name="AllUser[MobileNo]" id="MobileNo" maxlength="11" /> 
									</div></div>




<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="email">
						College Contact No </label>

<input type="text" required
									class="form-control" name="AllUser[ContactNo]"
									placeholder=""
									onkeypress="return numbersonly(event)" maxlength="11" />
									</div></div>




 









<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">State</label>
 	<select class="questions-category states form-control"
								tabindex="0" aria-hidden="true" name="AllUser[State]" required
								id="stateId">
								<option value="">Select State</option>
							</select>
				 				
								
				</div></div>

	 
	 
	 
	 

<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">City</label>
 	<select class="questions-category cities form-control "
								name="AllUser[City]" required tabindex="0" aria-hidden="true"
								id="cityId">
								<option value="">Select </option>
							</select>
				 				
								
				</div></div>

 
 
 
 

<div class="col-md-4">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">Pincode</label>
  <input type="text" required
									class="form-control" name="AllUser[PinCode]"
									placeholder="" onkeypress="return numbersonly(event)"
									maxlength="6" />
				 				
								
				</div></div>

 
  
  

<div class="col-md-6">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">Co-ordinator
							College  Name</label>
 <input type="text"
								class="form-control" required name="AllUser[CollegeName]"
								id="CollegeName" placeholder="" />
								
								
									</div></div>




 


<div class="col-md-6">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm"> College Website Url  </label>
 <input type="text" 	class="form-control" name="AllUser[CollegeWebsite]" 	placeholder="" required />
								
								
									</div></div>



<div class="col-md-6">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">Address </label>
 <input type="text"
									class="form-control" name="AllUser[Address1]"
									placeholder="Address 1" required />
								
								
									</div></div>




<div class="col-md-6">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">Address 2 </label>
   <input type="text"
									class="form-control" name="AllUser[Address2]"
									placeholder="Address 2" />
								
								
									</div></div>

				 



					
					

<div class="col-md-12">	
					<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">About College</label>
  	<textarea class="form-control textarea-small"
									name="AllUser[CompanyDesc]" required></textarea>
				 				
								
				</div></div>

  

 
		








					<!--New Code-->
					<div class="form-group course-specialization">
						
						<label for="email" class="col-sm-2 control-label">Courses
							Provided </label>
						
						<div class="col-sm-3">
							<select name="AllUser[Course][0][course]" required id="CampusCourseId-0"
									class="form-control campus-course" count="0">
									<option value="">Select Course</option>
									<?php if($naukari_specialization): foreach($naukari_specialization as $course){?>
										<option value="<?=$course->id;?>"><?=$course->name;?></option>
									<?php }endif;?>
									</select>
						</div>
						<div id="blank_0"> </div>
						<label for="email" class="col-sm-1 control-label">Specializations
							</label>
							<?php $specializationList = NaukariSpecialization::getSpecializationList();?>
							<div class="col-sm-4">
								<select name="AllUser[Course][0][specialization][]" multiple="multiple"
										id="SpecializationId-0" class="form-control campus-specialization">
										<option value="">Select Specialization</option>
										<?php foreach($specializationList as $specializationId => $specialization){?>
										<option value="<?=$specializationId;?>"><?=$specialization;?></option>
									<?php }?>
									</select> 
							</div>
						
					</div>
					
					<div id="add-more-course-specialization">
					
					</div>
				
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">
							<a href="javascript:void(0)" id="add-course-specialization" class="btn btn-primary">+ Add More</a>
						</label>
						
					</div>
					
					<div class="clear"></div>		 




 

					 
	</div>



				 
  

					<div class="form-group" style="display: none;">

						<label for="confirm" class="cols-sm-2 control-label"> Country </label>

						<div class="input-group full">

							<select class="questions-category countries form-control"
								tabindex="0" aria-hidden="true" required name="AllUser[Country]"
								id="countryId">

								<option value="">Select City</option>

								<option value="India" countryid="101" selected="selected">India</option>

							</select>

						</div>

					</div>

			


			



<div id="first_sec">
					    	<h5 class="page-head">Dean / Director / Principal  Details</h5></h5>

<div class="col-md-12">	
				
 	<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">Name  </label>
   <input type="text" required
									class="form-control" name="AllUser[Dean]"
									placeholder="" />
				 				
								
				</div></div>



<div class="col-md-12">	
				
 	<div class="form-group field-alluser-name required has-success">
<label class="control-label" for="confirm">About  </label>
  	<textarea class="form-control textarea-small"
									name="AllUser[AboutYou]"></textarea>
				 				
								
				</div></div>



			   

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Upload Photo </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<label class="btn btn-default btn-file design-1"> Browse <input
									type="file" name="AllUser[PhotoId]" accept="image/*"
									style="display: none;" class="image"
									onchange="$(this).parent().next().html($(this).val());">
								</label> <span></span>

								<p class="help-block-error"></p>

								<p>Supported Formats: .jpg, .png ; Limit 200KB</p>

							</div>

						</div>

					</div>

					</div>

 


					<h2>College Display</h2>







					<div class="form-group">

						<label for="confirm" class="cols-sm-2 control-label"> College Logo
						</label>

						<div class="cols-sm-10">

							<div class="input-group">

								<label class="btn btn-default btn-file design-1"> Browse <input
									type="file" class="image" name="AllUser[LogoId]"
									accept="image/*" style="display: none;"
									onchange="$(this).parent().next().html($(this).val());">

								</label> <span></span>

								<p class="help-block-error"></p>

								<p>Supported Formats: .jpg, .png ;  Limit 10KB</p>

							</div>



						</div>

					</div>







					<div class="form-group">

						<label for="confirm" class="cols-sm-2 control-label">College
							Picture </label>

						<div class="cols-sm-10">

							<div class="input-group">

								<label class="btn btn-default btn-file design-1"> Browse <input
									type="file" class="image" name="CollegePic[PicId][]"
									accept="image/*" style="display: none;"
									onchange="$(this).parent().next().html($(this).val());">

								</label> <span></span>
								<p class="help-block-error"></p>
								<div id="morepicbox"></div>

								<p>Supported Formats: .jpg, .png ;  Limit 200KB</p>

								<input type="button" value="+Add More" class="btn btn-primary"
									onclick='AddMore()'/>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label"> Upload
							Presentations </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<label class="btn btn-default btn-file design-1"> Browse <input
									type="file" id="presentationid" name="AllUser[PresentaionId]"
									accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,text/plain, application/pdf"
									style="display: none;"
									onchange="$(this).parent().next().html($(this).val());">

								</label> <span></span>
								<p class="help-block-error"></p>
								<p>Supported Formats: .ppt, .pptx, .pps, .pdf ; Limit 5MB</p>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label"><input
							type="checkbox" id="check_box" class=""> </label>
						<div class="cols-sm-10 info">I agreed to the Terms and Conditions
							governing the use of My Career Bugs. I have reviewed the default
							Mailer &amp; Communications settings. Register Now</div>
					</div>

					<div class="form-group ">
						<button type="submit"
							class="btn btn-primary btn-lg btn-block login-button"
							onclick="if(!$('#check_box').is(':checked')){alert('Accept Terms and condition');return false;}">Submit</button>

					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="border"></div>
</div>
<!-- end main wrapper -->
<script>
 
        $(document).ready(function(){
  $('.need_help span' ).click(function(){
			$('.need_help').animate({right:0});  
				});});
			$('.closed a').click(function(){
				$('.need_help').animate({right:-270}); 
				return true
	});
	</script>
	
	
<script type="text/javascript">
    function AddMore(){
        var addmore = "<br/><label class='btn btn-default btn-file design-1'>Browse <input ";
        	addmore += "type='file' name='CollegePic[PicId][]'";
        	addmore += "accept='image' class='image' style='display:";
        	addmore += "none;' onchange='$(this).parent().next().html($(this).val());'></label><span></span>";
        	addmore += "<p class='help-block-error'></p>";
        $("#morepicbox").append(addmore);
        imagechk();
    }
    
    function imagechk() {
        $('.image').on( 'change', function() {
           var myfile= $( this ).val();
           var ext = myfile.split('.').pop().toLowerCase();
           if(ext=="png" || ext=="jpg" || ext=="jpeg" || ext=="gif"){
               //alert(ext);
           } else{
        	$(this).parent().next().next('.help-block-error').html('Please provide image in valide format');
              $( this ).val('');$(this).parent().next().html('');
           }
        });
    }
    var data = [{id: '',text: 'Select course'}];
	var Ldata = [{id: '',text: 'Select Specialization'}];
	<?php
    $is_otrher = 0;
    if($naukari_specialization){
    foreach ($naukari_specialization as $key => $value) {
    if ($value->name == 'Other' && $is_otrher == 1) {
        continue;
    }
    ?>
	var d = {id: '<?=$value->id?>',text: '<?=$value->name?>'};
	data.push(d);
		<?php
    if ($value->name == 'Other') {
        $is_otrher = 1;
    }
}
}
?>
	
	var pid = 1;
	/*$(".campus-course").select2({
  		data: data,
	});*/
	
	$(document).on('change', '.campus-course', function(){
		var value = $(this).val();
		var count = $(this).attr('count');
		$.ajax({
	    	url:mainurl+"campus/specializationlist?courseId="+value,
            type : 'GET',
			dataType: 'JSON',
            success:function(results)
            {
				console.log(results);
				var specidata = [{id: '',text: 'Select Specialization'}];
				$.each(results, function(key, value) {
					var ds = {id: key,text: value};
					specidata.push(ds);
				});
				
                $("#SpecializationId-"+count).select2({
					data: specidata,
					maximumSelectionLength: 5
				});
            },
			error: function(jqXHR, exception) {
				if (jqXHR.status === 0) {
					alert('Not connect.\n Verify Network.');
				} else if (jqXHR.status == 404) {
					alert('Requested page not found. [404]');
				} else if (jqXHR.status == 500) {
					alert('Internal Server Error [500].');
				} else if (exception === 'parsererror') {
					alert('Requested JSON parse failed.');
				} else if (exception === 'timeout') {
					alert('Time out error.');
				} else if (exception === 'abort') {
					alert('Ajax request aborted.');
				} else {
					alert('Uncaught Error.\n' + jqXHR.responseText);
				}
			}
        });
		return false;
	});
	
	$(".campus-specialization").select2({
		data: Ldata,
		maximumSelectionLength: 5
	});
	
	
	
	$(document).on('click', '.pr_cat_close', function() {
		var cat = $(this).parent().find('input').val();
		$( "input.parent_cat[value='"+cat+"']" ).parent().remove();
		//console.log($(".subcat_"+cat));
		//$(".subcat_"+cat).remove();
		$(this).parent('.catlabel').remove();
	});
	$(document).on('click', '.sub_cat_close', function() {
		
		$(this).parent('.subcatlabel').remove();
	});
	$( "#campus_register_form" ).submit(function( event ) {
  		var cat = {};
  		$('.catlabel').each(function (index, value) {
				cat[$(this).attr('id')] = $(this).find('input').val();
		});
		$('.subcatlabel').each(function (index, value) {
			var id_ary = $(this).attr('id').split('_');
			if(cat['pr_'+id_ary[2]]){
				var subcat = $(this).find('.subcat').val().split('_');
				cat['pr_'+id_ary[2]] = cat['pr_'+id_ary[2]]+","+subcat[2];
			}
		});
		$("#alluser-education").val(JSON.stringify(cat));
	});
	
	
	$(document).on('click', '#add-course-specialization', function(){
		var count = $('.course-specialization').length;
	      count = parseInt(count)+1;
		var options = $('#CampusCourseId-0').html();
		var html = '<div class="form-group course-specialization"><label for="email" class="col-sm-2 control-label">Courses Provided </label><div class="col-sm-3"><select name="AllUser[Course]['+count+'][course]" required id="CampusCourseId-'+count+'" class="form-control campus-course" count="'+count+'">'+options+'</select></div><label for="email" class="col-sm-1 control-label">Specializations </label><div class="col-sm-4"><select name="AllUser[Course]['+count+'][specialization][]" multiple="multiple" id="SpecializationId-'+count+'" class="form-control campus-specialization"></select></div></div>';
		$('#add-more-course-specialization').append(html);
		$('#CampusCourseId-'+count+' option:selected').removeAttr('selected');
		$(".campus-specialization").select2({
			data: Ldata,
			maximumSelectionLength: 5
		});
		
	});

</script>