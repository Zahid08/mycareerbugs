<?php

$this->title = 'Campus Zone';

$csrfToken = Yii::$app->request->getCsrfToken();

use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

?>



<div id="wrapper"><!-- start main wrapper -->

	 

	

		<div class="headline_inner_campus">

				<div class="rowd"> 

			       <div class=" container"><!-- start headline section --> 

						 <h2>Campus Zone</h2>

						<div class="clearfix"></div>

					</div>

			</div><!-- end headline section -->

    	</div>

	

		 

		<div class="inner_page">

			<div class="container"> 

			   <div class="row">

					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="campus_left">  

					 <h2>Make your Campus Visible</h2>

					  <div class="advertise_your_post"> 

							 <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">

							  <img src="<?=$imageurl;?>images/business.png">

								  <p>Presence in Campus Search  </p>

								

							  </div>	

								  <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 new">

								<img src="<?=$imageurl;?>images/employee.png">

								 <p>Campus Drives  </p>

								

							  </div>	

								<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">

								  <img src="<?=$imageurl;?>images/campus.png">

								  <p>Placement Brochure    </p> 

							  </div> 

							  

							   <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 new">

								<img src="<?=$imageurl;?>images/employee.png">

								 <p>Campus Jobs    </p>

								

							  </div>

							  <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">

								  <img src="<?=$imageurl;?>images/campus.png">

								  <p>Placement Prep Tests      </p> 

							  </div> 

							  

							   

							  

							   <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12 new">

								<img src="<?=$imageurl;?>images/employee.png">

								 <p>Campus Contact Us  </p>

								

							  </div>

		               </div>

					   <div class="spacer-1"></div>

					   

					    <h2> Campuses Contact Us</h2>

						<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>

						

						<div class="form-group">

							<label for="name" class="cols-sm-2 control-label">   College Name   </label>

							<div class="cols-sm-10">

								<div class="input-group">

									<span class="input-group-addon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>

									<input type="text" class="form-control" name="CampusContact[CollegeName]" required id="name" placeholder="College Name">
									<input type="hidden" name="CampusContact[City]" id="city_new">

								</div>

							</div>

						</div>

						<div class="form-group">

							<label for="name" class="cols-sm-2 control-label"> City:    </label>

							<div class="cols-sm-10">

								<div class="input-group full">

								 <select id="city_old" name="CampusContact[Cityold]" required class="form-control bfh-states"><option value="">Select City from List</option><optgroup value="-1" class="optgry" label="-----Top Metropolitan Cities-----" style="width:154px;"></optgroup><option value="2">Ahmedabad</option><option value="3">Bengaluru / Bangalore</option><option value="4">Chandigarh</option><option value="5">Chennai</option><option value="6">Delhi</option><option value="8">Hyderabad / Secunderabad</option><option value="9">Kolkata</option><option value="10">Mumbai</option><option value="12">Pune</option><optgroup value="-1" class="optgry" label="-----Andhra Pradesh-----" style="width:154px;"></optgroup><option value="14">Anantapur</option><option value="15">Guntakal</option><option value="16">Guntur</option><option value="17">Hyderabad / Secunderabad</option><option value="240">Kadapa</option><option value="18">Kakinada</option><option value="241">Karimnagar</option><option value="19">Kurnool</option><option value="20">Nellore</option><option value="21">Nizamabad</option><option value="22">Rajahmundry</option><option value="24">Tirupati</option><option value="25">Vijayawada</option><option value="26">Visakhapatnam</option><option value="27">Warangal</option><option value="28">Andhra Pradesh - Other</option><optgroup value="-1" class="optgry" label="-----Arunachal Pradesh-----" style="width:154px;"></optgroup><option value="30">Itanagar</option><option value="31">Arunachal Pradesh - Other</option><optgroup value="-1" class="optgry" label="-----Assam-----" style="width:154px;"></optgroup><option value="33">Guwahati</option><option value="34">Silchar</option><option value="35">Assam - Other</option><optgroup value="-1" class="optgry" label="-----Bihar-----" style="width:154px;"></optgroup><option value="37">Bahgalpur</option><option value="38">Patna</option><option value="39">Bihar - Other</option><optgroup value="-1" class="optgry" label="-----Chhattisgarh-----" style="width:154px;"></optgroup><option value="41">Bhillai</option><option value="42">Bilaspur</option><option value="242">Korba</option><option value="43">Raipur</option><option value="44">Chhattisgarh - Other</option><optgroup value="-1" class="optgry" label="-----Goa-----" style="width:154px;"></optgroup><option value="46">Panjim / Panaji</option><option value="48">Vasco Da Gama</option><option value="49">Goa - Other</option><optgroup value="-1" class="optgry" label="-----Gujarat-----" style="width:154px;"></optgroup><option value="51">Ahmedabad</option><option value="52">Anand</option><option value="53">Ankleshwar</option><option value="55">Bharuch</option><option value="56">Bhavnagar</option><option value="57">Bhuj</option><option value="58">Gandhinagar</option><option value="59">Gir</option><option value="60">Jamnagar</option><option value="61">Kandla</option><option value="243">Nadiad</option><option value="62">Porbandar</option><option value="63">Rajkot</option><option value="64">Surat</option><option value="65">Vadodara / Baroda</option><option value="66">Valsad</option><option value="67">Vapi</option><option value="68">Gujarat - Other</option><optgroup value="-1" class="optgry" label="-----Haryana-----" style="width:154px;"></optgroup><option value="70">Ambala</option><option value="244">Bhiwani</option><option value="202">Chandigarh</option><option value="71">Chandigarh</option><option value="72">Faridabad</option><option value="73">Gurgaon</option><option value="74">Hisar</option><option value="75">Karnal</option><option value="76">Kurukshetra</option><option value="77">Panipat</option><option value="245">Rewari</option><option value="78">Rohtak</option><option value="246">Sonepat</option><option value="247">Yamuna Nagar</option><option value="79">Haryana - Other</option><optgroup value="-1" class="optgry" label="-----Himachal Pradesh-----" style="width:154px;"></optgroup><option value="81">Dalhousie</option><option value="82">Dharmasala</option><option value="83">Kulu/Manali</option><option value="84">Shimla</option><option value="85">Himachal Pradesh - Other</option><optgroup value="-1" class="optgry" label="-----Jammu &amp; Kashmir-----" style="width:154px;"></optgroup><option value="87">Jammu</option><option value="88">Srinagar</option><option value="89">Jammu and Kashmir - Other</option><optgroup value="-1" class="optgry" label="-----Jharkhand-----" style="width:154px;"></optgroup><option value="91">Bokaro</option><option value="92">Dhanbad</option><option value="93">Jamshedpur</option><option value="94">Ranchi</option><option value="95">Jharkhand - Other</option><optgroup value="-1" class="optgry" label="-----Karnataka-----" style="width:154px;"></optgroup><option value="98">Belgaum</option><option value="99">Bellary</option><option value="97">Bengaluru / Bangalore</option><option value="100">Bidar</option><option value="101">Dharwad</option><option value="102">Gulbarga</option><option value="103">Hubli</option><option value="104">Kolar</option><option value="105">Mangalore</option><option value="106">Mysoru / Mysore</option><option value="248">Shimoga</option><option value="107">Karnataka - Other</option><optgroup value="-1" class="optgry" label="-----Kerala-----" style="width:154px;"></optgroup><option value="249">Alappuzha</option><option value="109">Calicut / Kozhikode</option><option value="110">Cochin / Ernakulam / Kochi</option><option value="112">Kannur</option><option value="250">Kasaragod</option><option value="114">Kollam</option><option value="115">Kottayam</option><option value="251">Malappuram</option><option value="117">Palakkad / Palghat</option><option value="252">Pathanamthitta</option><option value="119">Thrissur</option><option value="120">Trivandrum</option><option value="121">Kerala - Other</option><optgroup value="-1" class="optgry" label="-----Madhya Pradesh-----" style="width:154px;"></optgroup><option value="123">Bhopal</option><option value="124">Gwalior</option><option value="125">Indore</option><option value="126">Jabalpur</option><option value="253">Sagar</option><option value="254">Satna</option><option value="127">Ujjain</option><option value="128">Madhya Pradesh - Other</option><optgroup value="-1" class="optgry" label="-----Maharashtra-----" style="width:154px;"></optgroup><option value="130">Ahmednagar</option><option value="255">Amravati</option><option value="131">Aurangabad</option><option value="132">Jalgaon</option><option value="133">Kolhapur</option><option value="134">Mumbai</option><option value="135">Mumbai Suburbs</option><option value="136">Nagpur</option><option value="256">Nanded</option><option value="137">Nasik</option><option value="138">Navi Mumbai</option><option value="139">Pune</option><option value="257">Sangli</option><option value="258">Satara</option><option value="140">Solapur</option><option value="141">Maharashtra - Other</option><optgroup value="-1" class="optgry" label="-----Manipur-----" style="width:154px;"></optgroup><option value="143">Imphal</option><option value="144">Manipur - Other</option><optgroup value="-1" class="optgry" label="-----Meghalaya-----" style="width:154px;"></optgroup><option value="146">Shillong</option><option value="147">Meghalaya - Other</option><optgroup value="-1" class="optgry" label="-----Mizoram-----" style="width:154px;"></optgroup><option value="149">Aizawal</option><option value="150">Mizoram - Other</option><optgroup value="-1" class="optgry" label="-----Nagaland-----" style="width:154px;"></optgroup><option value="152">Dimapur</option><option value="153">Nagaland - Other</option><optgroup value="-1" class="optgry" label="-----Orissa-----" style="width:154px;"></optgroup><option value="259">Berhampur</option><option value="155">Bhubaneshwar</option><option value="156">Cuttack</option><option value="157">Paradeep</option><option value="158">Puri</option><option value="159">Rourkela</option><option value="160">Orissa - Other</option><optgroup value="-1" class="optgry" label="-----Punjab-----" style="width:154px;"></optgroup><option value="162">Amritsar</option><option value="163">Bathinda</option><option value="164">Chandigarh</option><option value="260">Hoshiarpur</option><option value="165">Jalandhar</option><option value="166">Ludhiana</option><option value="167">Mohali</option><option value="168">Pathankot</option><option value="169">Patiala</option><option value="170">Punjab - Other</option><optgroup value="-1" class="optgry" label="-----Rajasthan-----" style="width:154px;"></optgroup><option value="172">Ajmer</option><option value="261">Alwar</option><option value="262">Bhilwara</option><option value="263">Bikaner</option><option value="173">Jaipur</option><option value="174">Jaisalmer</option><option value="175">Jodhpur</option><option value="176">Kota</option><option value="177">Udaipur</option><option value="178">Rajasthan - Other</option><optgroup value="-1" class="optgry" label="-----Sikkim-----" style="width:154px;"></optgroup><option value="180">Gangtok</option><option value="181">Sikkim - Other</option><optgroup value="-1" class="optgry" label="-----Tamil Nadu-----" style="width:154px;"></optgroup><option value="183">Chennai</option><option value="184">Coimbatore</option><option value="185">Cuddalore</option><option value="264">Dindigul</option><option value="186">Erode</option><option value="187">Hosur</option><option value="265">Karur</option><option value="188">Madurai</option><option value="189">Nagercoil</option><option value="266">Namakkal</option><option value="190">Ooty</option><option value="191">Salem</option><option value="192">Thanjavur</option><option value="267">Theni</option><option value="193">Tirunalveli</option><option value="194">Trichy</option><option value="195">Tuticorin</option><option value="196">Vellore</option><option value="268">Villupuram</option><option value="197">Tamil Nadu - Other</option><optgroup value="-1" class="optgry" label="-----Tripura-----" style="width:154px;"></optgroup><option value="199">Agartala</option><option value="200">Tripura - Other</option><optgroup value="-1" class="optgry" label="-----Union Territories-----" style="width:154px;"></optgroup><option value="203">Dadra &amp; Nagar Haveli - Silvassa</option><option value="204">Daman &amp; Diu</option><option value="205">Delhi</option><option value="206">Pondicherry</option><optgroup value="-1" class="optgry" label="-----Uttar Pradesh-----" style="width:154px;"></optgroup><option value="208">Agra</option><option value="209">Aligarh</option><option value="210">Allahabad</option><option value="211">Bareilly</option><option value="212">Faizabad</option><option value="213">Ghaziabad</option><option value="214">Gorakhpur</option><option value="273">Greater Noida</option><option value="269">Jhansi</option><option value="215">Kanpur</option><option value="216">Lucknow</option><option value="217">Mathura</option><option value="218">Meerut</option><option value="219">Moradabad</option><option value="220">Noida</option><option value="270">Saharanpur</option><option value="221">Varanasi / Banaras</option><option value="222">Uttar Pradesh - Other</option><optgroup value="-1" class="optgry" label="-----Uttaranchal-----" style="width:154px;"></optgroup><option value="224">Dehradun</option><option value="225">Roorkee</option><option value="226">Uttaranchal - Other</option><optgroup value="-1" class="optgry" label="-----West Bengal-----" style="width:154px;"></optgroup><option value="228">Asansol</option><option value="271">Burdwan</option><option value="229">Durgapur</option><option value="230">Haldia</option><option value="272">Howrah</option><option value="231">Kharagpur</option><option value="232">Kolkata</option><option value="233">Siliguri</option><option value="234">West Bengal - Other</option><option value="99999">Other</option></select>

								</div>

							</div>

						</div>

						

						



						<div class="form-group">

							<label for="email" class="cols-sm-2 control-label">College Address  </label>

							<div class="cols-sm-10">

								<div class="input-group">

									<span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>

									<input type="text" class="form-control" name="CampusContact[CollegeAddress]" required id="email" placeholder="College Address">

								</div>

							</div>

						</div>

						

						

 

	                  <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">Courses Provided    </label>

							<div class="cols-sm-10">

								<div class="input-group full">

									 <select name="CampusContact[Course][]" multiple="multiple" id="CourseId"  class="form-control bfh-states">

										<?php

										if($course)

										{

										foreach($course as $ke=>$val)

										{

										?>

										<option value="<?=$val->CourseName;?>"><?=$val->CourseName;?></option>

										<?php

										}

										}

										?>

									 </select>

								</div>

							</div>

						</div>

						

 

                      <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">  Contact Person</label>

							<div class="cols-sm-10">

								<div class="input-group">

									<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>

									<input type="text" class="form-control" name="CampusContact[ContactPerson]" required placeholder=" Contact Person"  />

								</div>

							</div>

						</div> 

						 

						

						<div class="form-group">

							<label for="email" class="cols-sm-2 control-label">  Contact No</label>

							<div class="cols-sm-10">

								<div class="input-group">

									<span class="input-group-addon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>

									<input type="text" class="form-control" name="CampusContact[ContactNo]" required placeholder="Contact No" onkeypress="return numbersonly(event)" maxlength="10">

								</div>

							</div>

						</div> 

						

						

						 <div class="form-group">

							<label for="email" class="cols-sm-2 control-label">College Email</label>

							<div class="cols-sm-10">

								<div class="input-group">

									<span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>

									<input type="text" class="form-control" name="CampusContact[CollegeEmail]" required placeholder="College Email">

								</div>

							</div>

						</div>



						<div class="form-group ">

							<input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block login-button" />

						</div>

						 

					<?php ActiveForm::end(); ?>

					   

					</div>

					 <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="campus_right"> 

					    <h2>Campus Login</h2>

						<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>

									<div class="input-group">

										<span class="input-group-addon"><i class="fa fa-user"></i></span>

										<input type="email" class="form-control" name="AllUser[Email]" required placeholder="Email address">

									</div>

									<span class="help-block"></span>

														

									<div class="input-group">

										<span class="input-group-addon"><i class="fa fa-lock"></i></span>

										<input type="password" class="form-control" name="AllUser[Password]" required placeholder="Password">

									</div>

									  <span class="help-block"> &nbsp;  </span> 

									 <!-- <span class="help-block">Password error</span>  -->



									<input type="submit" class="btn btn-primary btn-lg btn-block login-button" value="Log In" >

						<?php ActiveForm::end(); ?>

								<div class="row">

									<div class="col-xs-6">

										  <p class="center-left"> Not Yet Register ?  <a href="<?= Url::toRoute(['campus/campusregister'])?>" class="color"> Register Now </a></p>

									</div>

									<div class="col-xs-6">

										<p class="omb_forgotPwd">

											<a href="<?= Url::toRoute(['campus/forgotpassword'])?>">Forgot password?</a>

										</p>

									</div>

									

									

									<a href="<?= Url::toRoute(['campus/studentregister'])?>" ><input type="button" class="btn btn-primary btn-lg btn-block login-button" value="Student Register" ></a>

						   </div>

					</div>

			   </div> 

            </div> 

		    </div>

		 

		

		

		<div class="border"></div>
		<script type="text/javascript">
			$(document).on('change', '#city_old', function() {
			    
				$("#city_new").val($('#city_old option:selected').text());
			});
		</script>