<?php

$this->title = 'Post Job';

$csrfToken = Yii::$app->request->getCsrfToken();

use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

use dosamigos\tinymce\TinyMce;

$state= $empd['State'];

$city=$empd['City'];



?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<div id="wrapper"><!-- start main wrapper -->

	 	<div class="headline_inner">

				 
			       <div class=" container"><!-- start headline section --> 

						 <h2>  Create Campus Post </h2>

						<div class="clearfix"></div>

				 

			</div><!-- end headline section -->

    	</div>

	

		 

		<div class="inner_page">

			<div class="container">

						<div class="container">

			<div class="row main">

				 

				<div class="xs-12 col-sm-12 main-center">

					 <?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>

					 <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Co-Ordinator Name  </label>

                                        <input type="text" name="CampusPost[Name]"  value="<?=$empd->Name;?>" required placeholder="" class="form-control">

                                    </div>

                                </div>

                               
								

								  

								  <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Mobile Number:</label>

                                        <input type="text" maxlength="10" onkeypress="return numbersonly(event)" onblur="return IsMobileno(this.value);" id="MobileNo" placeholder="" value="<?=$empd->MobileNo;?>" name="CampusPost[Phone]" class="form-control" required>

                                    </div>

                                </div>


	                     <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Landline Number:</label>

                                        <input type="text" maxlength="11" onkeypress="return numbersonly(event)" onblur="return IsLandLineno(this.value);" id="LandLineNo" placeholder="" name="CampusPost[LandLine]" value="<?=$empd->ContactNo;?>" class="form-control" required>

                                    </div>

                                </div>
								 

								  <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Email Address*</label>

                                        <input type="email" placeholder="" name="CampusPost[Email]" value="<?=$empd->Email;?>" required class="form-control">

                                    </div>

                                </div>

								  

							 

								   

								 <div class="col-md-4 col-sm-4 col-xs-12">

								     <div class="form-group">

                                        <label>State</label>

										 <select class="questions-category states form-control" tabindex="-1" aria-hidden="true"  id="stateId" name="CampusPost[State]" required>

										<option value="">Select State</option>

										</select>  

								     </div>

							     </div>

											  <div class="col-md-4 col-sm-4 col-xs-12">

											   <div class="form-group">

												<label>City</label>

												<select class="questions-category cities form-control" tabindex="-1" aria-hidden="true"  id="cityId" name="CampusPost[City]" required>

												<option value="">Select City</option>

											</select>

												</div>

										   </div>

								

										  <div class="clearfix"></div>

                             
								

	  

										  <div class="clearfix"></div>

                                

									  <div class="">

                                    <!-- <div class="form-group">

                                        <label>Key Skill</label>

                                        <input type="text" placeholder=" " name="CampusPost[RawSkill]" id="skills" required class="form-control">

										<div id="allskill" style="width: 100%; margin-top: 5px; height: 25px; padding: 3px;font-size:12px; color: #fff;">

										</div>

										<input type="hidden" id="skillid" name="CampusPost[KeySkill]" />

                                    </div> -->

                                </div>

								

								 <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Number of student  </label>

                                       <select class="questions-category form-control" tabindex="-1" aria-hidden="true" name="CampusPost[NoofVacancy]">

											<option value="10"> 10 - 20</option>

											<option value="20">20 - 30 </option>

											<option value="30"> 30 - 40 </option>

											<option value="40">40 +</option>  

                                        </select> 

                                    </div>

                                </div>

								<div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Course </label>

                                       <select id="education-course" class="questions-category form-control" tabindex="-1" aria-hidden="true" name="CampusPost[Qualification]">
                                        </select> 

                                    </div>

                                </div>
                                
                                
                                
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Specialization  </label>

                                       <select id="education-specialization" class="questions-category form-control" tabindex="-1" multiple="multiple" aria-hidden="true" name="CampusPost[Specialization][]">
                                        </select> 

                                    </div>

                                </div>
                                
                         <div class="clear"></div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Venue  </label>
                                             <input type="text" placeholder="" name="CampusPost[Venue]" required class="form-control">
                                     
                                    </div>

                                </div>
                                
                                
                                  <div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Date </label>
 	<input type="text" class="form-control walkindate" 	id="WalkinTo" name="CampusPost[WalkinTo]" readonly 	style="cursor: pointer; background: #fff;" autocomplete="off" 	placeholder="" />
                                     
                                    </div>

                                </div>
                                
                                
                                 <div class="clear"></div>
                                
                                
                                
                                
                                
                                <div class="col-md-6 col-sm-6 col-xs-12" style="display:none">

                                    <div class="form-group">

                                        <label>Role  </label>

                                       <select name="CampusPost[role]" class="form-control" onchange="selectpositionnew(this.value)" id="crole"><option value="">Select</option>
						   <?php
						   foreach($allcategory as $rk=>$rvalue)
						   {
						   ?>
						   <option value="<?=$rvalue->Position;?>" <?php if($role1==$rvalue->PositionId) echo "selected='selected'";?>><?=$rvalue->Position;?></option>
						   <?php
						   }
						   ?>
						   </select> 

                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12" style="display:none">

                                    <div class="form-group">

                                        <label>Job By Skill  </label>

                                       <div class="panel-body" id="jobbyskill" style="max-height: 200px;overflow: auto;">
							
							
							
			</div>
                                    </div>

                                </div>
								 <!--<div class="clearfix height-20"></div>

                                 <h5 class="page-head">  Add more</h5>

										  

								  

										  <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">

                                        <label>Key Skill</label>

                                        <input type="text" placeholder=" " class="form-control">

                                    </div>

                                </div>

								

								 <div class="col-md-6 col-sm-6 col-xs-12">

                                    <div class="form-group">

                                        <label>Number of student  </label>

                                        <select class="questions-category form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">

											<option value="0"> 10</option>

											<option value="1">20 </option>

											<option value="2"> 30</option>

											<option value="3">40 +</option>  

                                        </select> 

                                    </div>

                                </div>-->


	 <div class="col-xs-12">

                                    <div class="form-group">

                                        <label>Post Title</label>

                                        <input type="text" placeholder=" " name="CampusPost[JobTitle]" required class="form-control">

                                    </div>

                                </div>



								  <div class="col-md-12 col-sm-12 col-xs-12">

								    <div class="form-group"><label>Post Description*  </label>

								 <?= $form->field($model, 'JobDescription')->widget(TinyMce::className(), [

									'options' => ['rows' => 6],'class'=>'form-control textarea-small',

									'language' => 'en_CA',

									'clientOptions' => [

										'plugins' => [

											"advlist autolink lists link charmap print preview anchor",

											"searchreplace visualblocks code fullscreen",

											"insertdatetime media table  paste spellchecker"

										],

										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"

									]

								])->label(false);?>

                              </div> 

							   

							  </div>

							<div class="col-md-12 col-sm-12 col-xs-12">

							

							<div class="form-group">

							<?= Html::submitButton('SUBMIT', ['class' => 'btn btn-default btn-green']) ?>

						</div>

						</div>

                            <?php ActiveForm::end(); ?>

				</div>

			</div>

		</div>



      </div>

		    </div>

<div class="border"></div>


<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script>


<script type="text/javascript">

		setTimeout(function(){

		var loc = new locationInfo();

		$('#stateId').val('<?=$state;?>');

		var loc1='<?=$state;?>';

		 var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');

        if(stateId != '' || !stateId){

        loc.getCities(stateId);

        }

		

		setTimeout(function(){$('#cityId').val('<?=$city;?>');},2000);

		

	},8000);

	var data = [{id: '',text: 'Select Specialization'}];
	var Ldata = [{id: '',text: 'Select Course'}];
	<?php 
	$is_otrher = 0;
	foreach ($naukari_specialization as $key => $value) { 
		if($value->name=='Other' && $is_otrher == 1){
			continue;
		}
		?>
		var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
		data.push(d);
	<?php if($value->name=='Other'){$is_otrher = 1;} } ?>

	<?php foreach ($NaukariQualData as $key1 => $value1) { ?>
		var ld = {id: '<?=$value1->name?>',text: '<?=$value1->name?>'};
		Ldata.push(ld);
	<?php }  ?>
	$("#education-specialization").select2({
  		data: data,
  		maximumSelectionLength: 5
	});
	$("#education-course").select2({
  		data: Ldata
	});
	function selectpositionnew(pid){
		
	    $.ajax({
	    	url:mainurl+"campus/skills?roleid="+pid,
            type : 'GET',
            success:function(results)
            {
                $('#jobbyskill').html(results);
            }
        });
	}
</script>