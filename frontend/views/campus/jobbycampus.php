<?php
$this->title = 'Job By Campus';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\AllUser;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl() .'/';
$url='/backend/web/';

if(isset($_POST['course']))

{

	 $crs=$_POST['course'];

}

else

{

	 $crs='';

}

if(isset($_POST['location']))

{

	 $loc=$_POST['location'];

}

else

{

	 $loc='';

}

if(isset($_POST['specialization']))

{

	 $spec=$_POST['specialization'];

}

else

{

	 $spec='';

}

?>

<!-- start main wrapper --> 

	<div id="wrapper">

	 

	 <div class="headline job_head">

       <div class=" container"><!-- start headline section -->

		<div class="row">

			<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 top-main bg-full margin_auto"> 

			   <h2 class="banner_heading">  <span>Search </span>  Campus </h2>

			 

				<div class="sticky">

					<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form']]); ?>

                        <div class="group-sm group-top">

                         

						 <div class="group-item col-md-4 col-xs-12"> 

							<div class="form-group">

							  <select id="form-filter-location" name="course" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                <option value=" ">Course</option>

                                <?php

								if($course)

								{

								   foreach($course as $comk=>$comval)

								   {

								   ?>

								   <option value="<?=$comval->CourseId;?>" <?php if($crs==$comval->CourseId) echo "selected";?>><?=$comval->CourseName;?></option>

								   <?php

								   }

								}

								?>

                              </select>

							</div>

                          </div>

						  

							<div class="group-item col-md-4 col-xs-6">

                            <div class="form-group">

                              <select id="form-filter-location" name="location" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                <option value=" ">Location</option>

                                <?php

								if($states)

								{

								   foreach($states as $sk=>$sval)

								   {

								   ?>

								   <option value="<?=$sval->StateName;?>" <?php if($loc==$sval->StateName) echo "selected";?>><?=$sval->StateName;?></option>

								   <?php

								   }

								}

								?>

                              </select>

							       

                            </div>

                          </div>

						  

						  

                          <div class="group-item col-md-3 col-xs-6">

                            <div class="form-group">

                              <select id="form-filter-location" name="specialization" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                <option value="">Specialization</option>

								<?php

								if($specialization)

								{

								   foreach($specialization as $sk=>$sval)

								   {

								   ?>

								   

								   <option value="<?=$sval->SpecializationId;?>" <?php if($spec==$sval->SpecializationId) echo "selected";?>><?=$sval->Specialization;?></option>

								   <?php

								   }

								}

								?>

                              </select> 

                            </div>

                          </div>

						   

                          <div class=" group-item reveal-block reveal-lg-inline-block col-md-1 col-xs-12">

							<?= Html::submitButton('Search', ['name'=>'campussearch','class' => 'btn btn-primary element-fullwidth']) ?>

                          </div>

						  

                        </div>

                      <?php ActiveForm::end(); ?>

					 </div>

						</div>

						 

						<div class="clearfix"></div>

					</div>

			</div><!-- end headline section -->

	</div>

	 

		 

		  

		

		 <div class="spacer-5"></div> 

		    <div class="container" id="company_list_pagination">

			

			<style>

			

			</style>

			       <div class="col-xs-12" > 

		         	        <ul class="pagination">

								<li class="active no_display"><a href="#">Top Companies</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'A'])?>">A</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'B'])?>">B</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'C'])?>">C</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'D'])?>">D</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'E'])?>">E</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'F'])?>">F</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'G'])?>">G</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'H'])?>#">H</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'I'])?>">I</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'J'])?>">J</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'K'])?>">K</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'L'])?>">L</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'M'])?>">M</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'N'])?>">N</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'O'])?>">O</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'P'])?>">P</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'Q'])?>">Q</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'R'])?>">R</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'S'])?>">S</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'T'])?>">T</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'U'])?>">U</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'V'])?>">V</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'W'])?>">W</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'X'])?>">X</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'Y'])?>">Y</a></li>

								<li><a href="<?= Url::toRoute(['campus/jobbycampus','by'=>'Z'])?>">Z</a></li>

							  </ul>

						</div>	  

						

						

						

							  <div class="clear"></div>

							  

		     <?php

			 if(isset(Yii::$app->request->post()['campussearch']) || isset(Yii::$app->request->get()['by']))

        {

		   if($companylist)

			 {

			   ?>

			   <div class="col-md-12">

			   <?php

			   foreach($companylist as $ck=>$cval)

			   {

					if($state!=$cval->State)

					{

						 ?>

			   </div>

			   <div class="col-md-12">

			   <div class="col-md-12">

			    <a href="#/" style="float: left;">  <?=$cval->State;?>  </a>

			    </div>

			   <?php

					}

					$state=$cval->State;

					?>

				  <div class="col-md-2 col-sm-12 col-xs-12" onclick="campusdetail(<?=$cval->UserId;?>);"> 

					<div class="company_show_logo">  <a href="#/">  <?=$cval->CollegeName;?>  </a> </div> 	 

				  </div>

			   

			   <?php

			   }

			   ?>

			   </div>

			<?php

			 }

			 else

			 {

			   echo "No campus founnd";

			 }

		}

		else

		{

			 if($companylist)

			 {

			   foreach($companylist as $ck=>$cval)

			   {

					?>

			   <div class="col-md-12">

			   <div class="col-md-12">

			    <a href="#/" style="float: left;">  <?=$cval->State;?>  </a>

			    </div>

			    <?php

			    $campus=$cval->getCamp($cval->State);

				$s=0;

			    foreach($campus as $ckk=>$cvall)

			    {

					$s++;

					if($s<5)

					{

			   ?>

				  <div class="col-md-2 col-sm-12 col-xs-12" onclick="campusdetail(<?=$cvall->UserId;?>);"> 

					<div class="company_show_logo">  <a href="#/">  <?=$cvall->CollegeName;?>  </a> </div> 	 

				 </div>

			   <?php

					}

			    }

				if(count($campus)>4)

				{

					?>

					<div class="col-md-2 col-sm-12 col-xs-12"> 

						 <div class="company_show_logo">  <a href="<?= Url::toRoute(['campus/allcollege','State'=>$cval->State])?>">  See More  </a> </div> 	 

					</div>

					<?php

				}

			    ?>

			   </div>

			   <?php

			   }

			 }

			 else

			 {

			   echo "No campus founnd";

			 }

		}

			 ?>

				    

			</div>

		

		 <div class="spacer-5"></div> 

		 

		

		

		<div class="testimony">

			<div class="container">

				<h1><?=$peoplesayblock->Heading;?></h1>

				<?=htmlspecialchars_decode($peoplesayblock->Content);?>

					

			</div>

			<div id="sync2" class="owl-carousel">

				<?php

				foreach($allfeedback as $fk=>$fvalue)

				{

					if($fvalue->docDetail)

					{

						$doc=$url.$fvalue->docDetail->Doc;

					}

					else

					{

						$doc=$imageurl.'images/user.png';

					}

				?>

				<div class="testimony-image">

					<img src="<?=$doc;?>" class="img-responsive" alt="testimony" style="height: 150px;width: 150px;"/>

				</div>

				<?php

				}

				?>

			</div>

			

			<div id="sync1" class="owl-carousel">

				<?php

				foreach($allfeedback as $fk=>$fvalue1)

				{

					?>

				<div class="testimony-content container">

					<?=htmlspecialchars_decode($fvalue1->Message);?>

					<p>

						<?php echo $fvalue1->Name.','.$fvalue1->Designation.','.$fvalue1->Companyname;?>

					</p>

					<div class="media-testimony">

						<?php

						if($fvalue1->Twitterlink!='')

						{

						?>

						<a href="<?=$fvalue1->Twitterlink;?>" target="blank"><i class="fa fa-twitter twit"></i></a>

						<?php

						}

						if($fvalue1->LinkedinLink!='')

						{

						?>

						<a href="<?=$fvalue1->LinkedinLink;?>" target="blank"><i class="fa fa-linkedin linkedin"></i></a>

						<?php

						}

						if($fvalue1->Facebooklink!='')

						{

						?>

						<a href="<?=$fvalue1->Facebooklink;?>" target="blank"><i class="fa fa-facebook fb"></i></a>

						<?php

						}

						?>

					</div>

				</div>

				<?php

				}

				?>

			</div>

		</div>

		

		

		

	

			

		<?php

						if(!isset(Yii::$app->session['Employeeid']) && !isset(Yii::$app->session['Employerid']) && !isset(Yii::$app->session['Campusid']) && !isset(Yii::$app->session['Teamid']))

						{

						?>

		<div class="advertise_your_post">

		    <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">

		   	        <img src="<?=$imageurl;?>images/job_seekers.png">

					 

                    <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">  Register  </a>

				  </div>	



                            <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">

				  <img src="<?=$imageurl;?>images/team_orange.png">

		   	          

                    <a href="<?= Url::toRoute(['team/index'])?>">Register  </a>

				  </div>	





		           <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">

				  <img src="<?=$imageurl;?>images/company.png">

		   	          

                    <a href="<?= Url::toRoute(['site/employersregister'])?>">Register  </a>

				  </div>	

 



				       <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">

				  	  <img src="<?=$imageurl;?>images/campus_register.png">

		   	         

                    <a href="<?= Url::toRoute(['campus/campusregister'])?>">Register  </a>

				  </div>	

		</div>

		<?php

						}

						?>

<!-----myModal_email------>

<div class="modal fade" id="myModal_college" role="dialog">

    <div class="modal-dialog">

      <div class="modal-content">

	     <div class="modal-header">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">College Detail</h4>

        </div>

		

		<div class="row main" id="collegedetail">

		

		</div>

	 </div>  

	</div>

</div>

<!-----myModal_email------>



<script type="text/javascript">

	 function campusdetail(userid) {
	 	window.location.href = "<?= Url::toRoute(['campus/campusdetail'])?>?userid="+userid;
        /*$.ajax({url:"<= Url::toRoute(['campus/campusdetail'])?>?userid="+userid,

		  success:function(results)

		  {

			  $('#collegedetail').html(results);

			  $('#myModal_college').modal('show');

		  }

		});*/

     }

</script>