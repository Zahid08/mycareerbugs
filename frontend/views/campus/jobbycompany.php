<?php
$this->title = 'Job By Company MCB';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?> <meta name="keywords" content="Mycareer bugs, MCB, All companies list, Job By Company" />   <meta name="description" content="Hunt for a place in your dream Company. Search and Apply to Openings with Top Employers." /> 
<!-- start main wrapper --> 
	<div id="wrapper">
	 
	 
		 <div class="headline_inner">
				<div class="row"> 
			       <div class=" container"><!-- start headline section --> 
						 <h2> Browse Jobs by Companies </h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	</div>
		  
		
		 <div class="spacer-5"></div> 
		    <div class="container" id="company_list_pagination">
			
			<style>
			
			</style>
			       <div class="col-xs-12" > 
		         	        <ul class="pagination">
								<li class="active no_display"><a href="#">Top Companies</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'A'])?>">A</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'B'])?>">B</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'C'])?>">C</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'D'])?>">D</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'E'])?>">E</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'F'])?>">F</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'G'])?>">G</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'H'])?>#">H</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'I'])?>">I</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'J'])?>">J</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'K'])?>">K</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'L'])?>">L</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'M'])?>">M</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'N'])?>">N</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'O'])?>">O</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'P'])?>">P</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'Q'])?>">Q</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'R'])?>">R</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'S'])?>">S</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'T'])?>">T</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'U'])?>">U</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'V'])?>">V</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'W'])?>">W</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'X'])?>">X</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'Y'])?>">Y</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobbycompany','by'=>'Z'])?>">Z</a></li>
							  </ul>
						</div>	  
						
						
						
							  <div class="clear"></div>
							  
		     <?php
			 if($companylist)
			 {
			   foreach($companylist as $ck=>$cval)
			   {
					?>
				  <div class="col-md-2 col-sm-12 col-xs-12"> 
					<div class="company_show_logo">  <a href="<?= Url::toRoute(['site/jobsearch','indexsearch'=>'1','keyname'=>$cval->CompanyName,'indexlocation'=>'','experience'=>'','salary'=>''])?>">  <?=$cval->CompanyName;?>  </a> </div> 	 
				 </div>
			   <?php
			   }
			 }
			 else
			 {
			   echo "No company founnd";
			 }
			 ?>

			</div>
		
		 <div class="spacer-5"></div> 
		 
		
		
		<div class="testimony">
			<div class="container">
				<h1><?=$peoplesayblock->Heading;?></h1>
				<?=htmlspecialchars_decode($peoplesayblock->Content);?>
					
			</div>
			<div id="sync2" class="owl-carousel">
				<?php
				foreach($allfeedback as $fk=>$fvalue)
				{
					if($fvalue->docDetail)
					{
						$doc=$url.$fvalue->docDetail->Doc;
					}
					else
					{
						$doc=$imageurl.'images/user.png';
					}
				?>
				<div class="testimony-image">
					<img src="<?=$doc;?>" class="img-responsive" alt="MCB Testimonial Profile" style="height: 150px;width: 150px;"/>
				</div>
				<?php
				}
				?>
			</div>
			
			<div id="sync1" class="owl-carousel">
				<?php
				foreach($allfeedback as $fk=>$fvalue1)
				{
					?>
				<div class="testimony-content container">
					<?=htmlspecialchars_decode($fvalue1->Message);?>
					<p>
						<?php echo $fvalue1->Name.','.$fvalue1->Designation.','.$fvalue1->Companyname;?>
					</p>
					<div class="media-testimony">
						<?php
						if($fvalue1->Twitterlink!='')
						{
						?>
						<a href="<?=$fvalue1->Twitterlink;?>" target="blank"><i class="fa fa-twitter twit"></i></a>
						<?php
						}
						if($fvalue1->LinkedinLink!='')
						{
						?>
						<a href="<?=$fvalue1->LinkedinLink;?>" target="blank"><i class="fa fa-linkedin linkedin"></i></a>
						<?php
						}
						if($fvalue1->Facebooklink!='')
						{
						?>
						<a href="<?=$fvalue1->Facebooklink;?>" target="blank"><i class="fa fa-facebook fb"></i></a>
						<?php
						}
						?>
					</div>
				</div>
				<?php
				}
				?>
			</div>
		</div>
		
		
		
	
			
		<?php
						if(!isset(Yii::$app->session['Employeeid']) && !isset(Yii::$app->session['Employerid']) && !isset(Yii::$app->session['Campusid']) && !isset(Yii::$app->session['Teamid']))
						{
						?>
		<div class="advertise_your_post">
		    <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
		   	        <img src="<?=$imageurl;?>images/job_seekers.png" alt="MCB Job Seekers">
					 
                    <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">  Register  </a>
				  </div>	

                            <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
				  <img src="<?=$imageurl;?>images/team_orange.png" alt="MCB Team Register">
		   	          
                    <a href="<?= Url::toRoute(['team/index'])?>">Register  </a>
				  </div>	


		           <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
				  <img src="<?=$imageurl;?>images/company.png"  alt="MCB Employers Register">
		   	          
                    <a href="<?= Url::toRoute(['site/employersregister'])?>">Register  </a>
				  </div>	
 

				       <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
				  	  <img src="<?=$imageurl;?>images/campus_register.png" alt="MCB Campus Register">
		   	         
                    <a href="<?= Url::toRoute(['campus/campusregister'])?>">Register  </a>
				  </div>	
		</div>
		<?php
						}
						?>