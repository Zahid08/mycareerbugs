<?php
$this->title = 'Job Edit';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;use dosamigos\tinymce\TinyMce;
use common\models\NaukariSpecialization;
use common\models\NaukariQualData;
use common\models\States;
use common\models\Cities;

$naukariQualList = NaukariQualData::getCourseList();
$naukariSpecList = NaukariSpecialization::getSpecializationList();

$stateList = States::getStateList();

$state= $allpost->State;

$city = $allpost->City;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
  
	<div id="wrapper"><!-- start main wrapper -->

		<div class="headline_inner">
				<div class="row"> 
			       <div class=" container"><!-- start headline section --> 
						 <h2>Job Edit</h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	</div>
	
		 
		<div class="inner_page">
			<div class="container">
						<div class="container">
			<div class="row main">
				 
				<div class="xs-12 col-sm-12 main-center">
					<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Co-Ordinator Name  </label>
                                        <input type="text" name="CampusPost[Name]" required placeholder="" class="form-control" value="<?=$allpost->Name;?>">
                                    </div>
                                </div>
								<div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Mobile Number:</label>

                                        <input type="text" maxlength="10" onkeypress="return numbersonly(event)" onblur="return IsMobileno(this.value);" id="MobileNo" placeholder="" value="<?=$allpost->Phone;?>" name="CampusPost[Phone]" class="form-control" required>

                                    </div>

                                </div>
								 <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Land Line:</label>
                                        <input type="text" maxlength="11" onkeypress="return numbersonly(event)" onblur="return IsLandLineno(this.value);" id="MobileNo" placeholder="" value="<?=$allpost->LandLine;?>" name="CampusPost[LandLine]" class="form-control" required>
                                    </div>
                                </div>
								
								
								<div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Email Address*</label>
                                        <input type="email" placeholder="" name="CampusPost[Email]" value="<?=$allpost->Email;?>" required class="form-control">
                                    </div>
                                </div>
								
								 <div class="col-md-4 col-sm-4 col-xs-12">
								     <div class="form-group">
                                        <label>State</label>
                                         <select class="questions-category states form-control" tabindex="-1" aria-hidden="true"  id="stateId" name="CampusPost[State]" required>

										<option value="">Select State</option>
										<?php foreach($stateList as $stateId => $stateName){?>
											<option value="<?=$stateId;?>" <?=(($stateName == $allpost->State)?"selected":"");?> ><?=$stateName;?></option>
										<?php }?>
										</select> 
										  
								     </div>
							     </div>
								 
								 <div class="col-md-4 col-sm-4 col-xs-12">
									   <div class="form-group">
										<label>City</label>
										<?php 
											$stateData = States::find()->select(['StateId'])
																->where([
																	'StateName' => $allpost->State
																])
																->one();
											$cityList = array();	
										
											if(isset($stateData->StateId)){
												$cityList = Cities::getCityList($stateData->StateId);
											}					
											
										?>
										
										<select class="questions-category cities form-control" tabindex="-1" aria-hidden="true"  id="cityId" name="CampusPost[City]" required>

												<option value="">Select City</option>
												<?php foreach($cityList as $cityId => $cityName){?>
													<option value="<?=$cityId;?>" <?=(($cityName == $allpost->City)?"selected":"");?> ><?=$cityName;?></option>
												<?php }?>
											</select>
										</div>
								   </div>
									 <div class="clearfix"></div><div class="clearfix"></div>
									 
									<div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Number of student  </label>
                                       <select class="questions-category form-control" tabindex="-1" aria-hidden="true" name="CampusPost[NoofVacancy]">
											<option value="10" <?php if($allpost->NoofVacancy=='10') echo "selected"; ?>> 10 -20</option>
											<option value="20" <?php if($allpost->NoofVacancy=='20') echo "selected"; ?>>20 - 30</option>
											<option value="30" <?php if($allpost->NoofVacancy=='30') echo "selected"; ?>> 30 - 40</option>
											<option value="40" <?php if($allpost->NoofVacancy=='40') echo "selected"; ?>>40 +</option>  
                                        </select> 
                                    </div>
                                </div>
								
								<div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Course </label>

                                       <select id="education-course" class="questions-category form-control" tabindex="-1" aria-hidden="true" name="CampusPost[Qualification]">
										<option value="">Select Course</option>
										<?php foreach($naukariQualList as $courseName){?>
											<option value="<?=$courseName;?>" <?=(($courseName == $allpost->course)?"selected":"");?> ><?=$courseName;?></option>
										<?php }?>
                                        </select> 

                                    </div>

                                </div>
								
								<div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Specialization  </label>

                                       <select id="education-specialization" class="questions-category form-control" tabindex="-1" multiple="multiple" aria-hidden="true" name="CampusPost[Specialization][]">
									   <option value="">Select Specialization</option>
											<?php 
												$specialized = array();
												if(!empty($allpost->specialization)){
													$specialized = explode(',',$allpost->specialization);
												}
												
											?>
											<?php foreach($naukariSpecList as $specializationId => $specialization){?>
											<option value="<?=$specialization;?>" <?=(in_array($specialization, $specialized)?"selected":"");?>><?=$specialization;?></option>
										<?php }?>
                                        </select> 

                                    </div>

                                </div>
								   
								 <div class="clear"></div>
								 
								 <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Venue</label>
                                        
										<input type="text" class="form-control" value="<?=$allpost->Venue;?>" id="Venue" name="CampusPost[Venue]" />
                                    </div>
                                </div>
								<div class="col-md-4 col-sm-4 col-xs-12">

                                    <div class="form-group">

                                        <label>Date </label>
										<input type="text" class="form-control walkindate" 	id="WalkinTo" name="CampusPost[WalkinTo]" readonly value="<?=$allpost->WalkinTo;?>"	style="cursor: pointer; background: #fff;" autocomplete="off" 	placeholder="" />
                                     
                                    </div>

                                </div>
								  <div class="clear"></div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Title</label>
                                        <input type="text" placeholder=" " name="CampusPost[JobTitle]" required class="form-control" value="<?=$allpost->JobTitle;?>">
                                    </div>
                                </div>
								 <div class="clearfix height-20"></div>
                                
								  <div class="col-md-12 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Description*  </label>
								 <?= $form->field($allpost, 'JobDescription')->widget(TinyMce::className(), [
									'options' => ['rows' => 6],'class'=>'form-control textarea-small',
									'language' => 'en_CA',
									'clientOptions' => [
										'plugins' => [
											"advlist autolink lists link charmap print preview anchor",
											"searchreplace visualblocks code fullscreen",
											"insertdatetime media table  paste spellchecker"
										],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								])->label(false);?>
                              </div> 
							   
							  </div>
							<div class="col-md-12 col-sm-12 col-xs-12">
							
							<div class="form-group">
							<?= Html::submitButton('UPDATE', ['class' => 'btn btn-default btn-green']) ?>
						</div>
						</div>
                           <?php ActiveForm::end(); ?>
					
				</div>
			</div>
		</div>

      </div>
		    </div>
		<div class="border"></div>
        </div>
		
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script>


<script type="text/javascript">
$(document).ready(function(){
	setTimeout(function(){

		var loc = new locationInfo();

		$('#stateId').val('<?=$state;?>');

		var loc1='<?=$state;?>';

		 var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');

        if(stateId != '' || !stateId){

        loc.getCities(stateId);

        }

		

		setTimeout(function(){$('#cityId').val('<?=$city;?>');},1000);

		

	},2000);
	
	$("#education-specialization").select2({
		maximumSelectionLength: 5
	});
	$("#education-course").select2({
  		
	});
	
});


</script>		