<?php
$this->title = 'Campus - Job search - My Career Bugs';

use yii\helpers\Url;

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

$url = '/backend/web/';

use yii\widgets\LinkPager;

use common\models\AppliedJob;
use common\models\States;
use common\models\Cities;
use common\models\PostJob;

$appliedjob = new AppliedJob();
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
?>

<div id="wrapper">
	<!-- start main wrapper -->
	<div class="job_search">
		<!-- Start Recent Job -->
		<div class="container">
			<div class="row">
				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="right-side">
					<h4>Job Filter</h4>
					<div class="col-md-122">
						<a href="<?= Url::toRoute(['campus/jobsearch']);?>">Clear </a>
					</div>
					<div class="panel-group" id="accordion">
						<div class="panel panel-default">
						<input type="hidden" id="is_campus_search" value="1"/>
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion"
										href="#collapseOne12">Latest</a>
								</h4>
							</div>
							<div id="collapseOne12" class="panel-collapse collapse in">
								<div class="panel-body">
								<?php
								if (isset($_GET['latest'])) {
									$latest = $_GET['latest'];
								} else {
									$latest = '';
								}

								?>
								<select class="form-control" id="latest" onchange="getsearch();">
										<option value="">Select</option>
										<option value="1"
											<?php if($latest==1) echo "selected='selected'";?>>1 Days</option>

										<option value="3"
											<?php if($latest==3) echo "selected='selected'";?>>3 Days</option>

										<option value="7"
											<?php if($latest==7) echo "selected='selected'";?>>7 Days</option>

										<option value="15"
											<?php if($latest==15) echo "selected='selected'";?>>15 Days</option>

										<option value="30"
											<?php if($latest==30) echo "selected='selected'";?>>30 Days</option>

									</select>

								</div>

							</div>

						</div>
						
						


						<div class="panel panel-default">

							<div class="panel-heading">

								<h4 class="panel-title">

									<a data-toggle="collapse" data-parent="#accordion"
										href="#collapseOne"> Location</a>

								</h4>

							</div>

							<div id="collapseOne" class="panel-collapse collapse">

								<div class="panel-body">
						<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
										<?php 
						   	
							$url = Url::toRoute(['site/getpreferredlocation']);

							$jbpst = new PostJob();
							echo $form->field($jbpst, 'Location')->widget(Select2::classname(), [
								'data' => ArrayHelper::map(Cities::find()->all(), 'CityId', 'CityName'),
								'language' => 'en',
								'options' => ['placeholder' => 'Select Cities ...', 'onchange' => 'localStorage.jobsearch="collapseOne";getsearch();', 'id'=>'state'],
								'pluginOptions' => [
								    'allowClear' => true,
									'multiple' => false,
									'maximumSelectionLength' => 5,
									'ajax' => [
										'url' => $url ,
										'dataType' => 'json' ,
										'data' => new JsExpression( 'function(params) { return {q:params.term, page:params.page || 1}; }' )
									] ,
									'escapeMarkup' => new JsExpression ( 'function (markup) { return markup; }' ) ,
									'templateResult' => new JsExpression ( 'function(product) { console.log(product);return product.CityName; }' ) ,
									'templateSelection' => new JsExpression ( 'function (subject) { return subject.CityName; }' ) ,
								],
							])->label(false);;?>
                      
							<?php ActiveForm::end(); ?>
								</div>
							</div>
						</div>




						<div class="panel panel-default">

							<div class="panel-heading">

								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapseSeven"><h4 class="panel-title">Job By Company</h4></a>

							</div>

							<div id="collapseSeven" class="panel-collapse collapse">

								<div class="panel-body">

				<?php

    if (isset($_GET['companyby'])) {

        $jobbycompany = explode(",", $_GET['companyby']);
    } else {

        $jobbycompany = array();
    }

    foreach ($allcompany as $ck => $cvalue) {

        ?>

				<span>

										<div class="checkbox">

											<label> <input type="checkbox" class="companyby"
												value="<?=$cvalue->CompanyName;?>"
												<?php if(in_array($cvalue->CompanyName,$jobbycompany)) echo "checked";?>>

												<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>

												<a><?=$cvalue->CompanyName;?> </a>

											</label>

										</div>

									</span>

				<?php
    }

    ?>

			</div>

							</div>

						</div>
						
						<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><h4 class="panel-title">
                             Salary
                        </h4></a>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body" style="max-height: 200px;overflow: auto;">
								<?php
								if(isset($_GET['salaryrange']) && $_GET['salaryrange']!='')
								{
								$salaryrange=explode(",",$_GET['salaryrange']);
								}
								else
								{
										$salaryrange=array();
								}
								?>
						            <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="0 - 1.5" <?php if(in_array('0 - 1.5',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">0 - 1.5 Lakh </a>  </label>
								      </div>	 
							       </span>
								 
								     <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="1.5 - 3" <?php if(in_array('1.5 - 3',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">1.5 - 3 Lakh</a>  </label>
								      </div>	 
							       </span>
								    
								       <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="3 - 6" <?php if(in_array('3 - 6',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">3 - 6 Lakh</a></label>
								      </div>	 
							       </span>
								   
								   
								       <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="6 - 10" <?php if(in_array('6 - 10',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">6 - 10 Lakh</a></label>
								      </div>	 
							       </span>
								  
								       <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="10 - 15" <?php if(in_array('10 - 15',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">10 - 15 Lakh</a></label>
								      </div>	 
							       </span>
								   
								    
								<span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="15 - 25" <?php if(in_array('15 - 25',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">15 - 25 Lakh</a>   </label>
								      </div>	 
							       </span>
								<span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="Above 25" <?php if(in_array('Above 25',$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">Above 25 Lakh</a></label>
								      </div>	 
							       </span> 
							
                        </div>
                    </div>
                </div>
				
				 <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><h4 class="panel-title">
                             Job Type
                        </h4></a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body" style="max-height: 200px;overflow: auto;">
                           <?php
								if(isset($_GET['jobtype']) && $_GET['jobtype']!='')
								{
								$jobtype=explode(",",$_GET['jobtype']);
								}
								else
								{
										$jobtype=array();
								}
								?>
								<span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Full Time" <?php if(in_array('Full Time',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Full Time
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Part Time" <?php if(in_array('Part Time',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Part Time
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Fresher" <?php if(in_array('Fresher',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Fresher
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Walkin" <?php if(in_array('Walkin',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Walkin
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Internship" <?php if(in_array('Internship',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Internship
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Contract" <?php if(in_array('Contract',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Contract
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Commission" <?php if(in_array('Commission',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Commission
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Temporary" <?php if(in_array('Temporary',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Temporary
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Volunter" <?php if(in_array('Volunter',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Volunter
									</label>
								  </div>	 
							     </span>
								
                        </div>
                    </div>
                </div>
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#expcollapseSix"><h4 class="panel-title">
                             Industry
                        </h4></a>
                    </div>
                    <div id="expcollapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
				<?php
				if(isset($_GET['JobCategoryId']) && $_GET['JobCategoryId']!='')
								{
								$industryval=$_GET['JobCategoryId'];
								}
								else
								{
								$industryval='';
								}
				?>
				 <select class="form-control bfh-states" id="industry" onchange="localStorage.jobsearch='expcollapseSix';getsearch();">
								<option value="">Select</option>
								<?php
								if($industry)
								{
								foreach($industry as $lk=>$lv)
								{
								?>
								<option value="<?=$lv->IndustryId;?>" <?php if($industryval==$lv->IndustryId) echo 'selected';?>><?=$lv->IndustryName;?></option>
								<?php
								}
								}
								?>
							</select>
				
			</div>
		    </div>
		</div>
				
		<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#educollapseSix"><h4 class="panel-title">
                             Education
                        </h4></a>
                    </div>
                    <div id="educollapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
				<?php
				if(isset($_GET['education']) && $_GET['education']!='')
								{
								$education=$_GET['education'];
								}
								else
								{
								$education='';
								}
								if(isset($_GET['nak_course']) && $_GET['nak_course']!='')
								{
								$nak_course=$_GET['nak_course'];
								}
								else
								{
								$nak_course='';
								}
								if(isset($_GET['qualification']) && $_GET['qualification']!='')
								{
								$qualification_rec =$_GET['qualification'];
								}
								else
								{
								$qualification_rec='';
								}
				
				?>
					<select class="form-control" style="margin-bottom:10px" id="qualification"  onchange="localStorage.jobsearch='educollapseSix';getsearch();">
					<option value="">Select Qualification</option> 
					<?php
					// if($course)
					foreach($qualifications as $key1=>$value1){?>
						<option value="<?=$value1->name;?>" <?php if($qualification_rec==$value1->name) echo "selected='selected'";?>><?=$value1->name;?></option>
					<?php }?>	
					</select>
				
					<select class="form-control" style="margin-bottom:10px" id="nak_course"  onchange="localStorage.jobsearch='educollapseSix';getsearch();">
					<option value="">Select Course</option> 
					<?php
					// if($course)
					$is_otrher = 0;
					foreach($naukari_specialization as $key1=>$value1)
											{
												if($value1->name=='Other' && $is_otrher == 1){
													continue;
												}
											?>
											<option value="<?=$value1->name;?>" <?php if($nak_course==$value1->name) echo "selected='selected'";?>><?=$value1->name;?></option>
											<?php
											if($value1->name=='Other'){$is_otrher = 1;}
											}
											?>
					</select>
					
				
				<select class="form-control" id="education"  onchange="localStorage.jobsearch='educollapseSix';getsearch();">
				<option value="">Select Specializations </option> 
				<?php
				// if($course)
				// {
				foreach($NaukariQualData as $key=>$value)
										{
										?>
										<option value="<?=$value->name;?>" <?php if($education==$value->name) echo "selected='selected'";?>><?=$value->name;?></option>
										<?php
										}
										?>
				</select>
			
				
			</div>
		    </div>
		</div>
		



						<div class="panel panel-default" style="display: none">

							<div class="panel-heading">

								<h4 class="panel-title">

									<a data-toggle="collapse" data-parent="#accordion"
										href="#collapseFour"> Reports</a>

								</h4>

							</div>

							<div id="collapseFour" class="panel-collapse collapse">

								<div class="panel-body"></div>

							</div>

						</div>

					</div>





					<div class="spacer-5"></div>

					<div class="widget-heading">
						<span class="title">Hot Categories </span>
					</div>



					<div class="spacer-5"></div>

					<div class="widget">

						<ul class="categories-module">

                                       <?php

                                    if ($hotcategory) {

                                        foreach ($hotcategory as $hkey => $hvalue) {

                                            ?>

								<li><a
								href="<?= Url::toRoute(['campus/jobsearch','JobCategoryId'=>$hvalue->JobCategoryId])?>"> <?=$hvalue->CategoryName;?> <span>(<?=$hvalue->cnt;?>)</span>
							</a></li>

								<?php
                                        }
                                    }

                                    ?>

                                    </ul>

					</div>

				</div>







				<div class="col-lg-9  col-md-9 col-sm-9 col-xs-12 jobsearch-continer" id="jobsearch-continer">
					<?= $this->render('jobsearch_ajax', array('alljob'=>$alljob,'pages'=>$pages)); ?>
				</div>





				<div class="clearfix"></div>

			</div>

		</div>
 
</div>
 


