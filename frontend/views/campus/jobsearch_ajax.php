<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use common\models\AppliedJob;
$appliedjob=new AppliedJob();

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

?>
						<div class="col-md-8">
						<div class="pannel_header margin_top" id="mbl-no-nd">
								<h4> Company Post</h4>
						</div>
						</div>
						
						<div class="pannel_header margin_top" style="margin: 0px;">
					<span id="searchdiv" style="font-size: 14px;color: #f16b22;">
										<?=(isset($_GET['company']))?'Search > '.$alljob[0]->employer->Name:'';?>
								</span>
						</div>
		 <?php
		if($alljob)
		{
		foreach($alljob as $jkey=>$jvalue)
		{
				if(isset(Yii::$app->session['Employeeid']))
				{
				$iscount=$appliedjob->getIsapplied($jvalue->JobId,Yii::$app->session['Employeeid']);
				}elseif(Yii::$app->session['Teamid'])
                {
						$iscount=$appliedjob->getIsapplied($jvalue->JobId,Yii::$app->session['Teamid']);
				}else{$iscount=0;}
		?>
		 <div class="item-list">
                <a href="<?= Url::base().'/campus-post/'.$jvalue->Slug;?>">
                <div class="col-sm-12 add-desc-box">
                  <div class="add-details">  
					<div class="company-img">



										<?php
		
        if ($jvalue->docDetail) {
			$url = "https://mycareerbugs.com/backend/web/";
            $doc = $url . $jvalue->docDetail->Doc;
        } else {

            $doc = $imageurl . 'images/user.png';
        }

        ?>



                                        <img src="<?=$doc;?>"
										class="img-responsive" alt="" style="border-radius: 200px;">



								</div>
								
<style>
    
.company-img{width:6%; float:left; margin-right:20px;}
.cont_rught{float:right; width:91%;}
.cont_rught h5{    margin: 0px;padding: 7px 0 0 0;line-height: 17px;font-size: 14px;font-weight: bold;}
.cont_rught .info{padding:0px;}
.height_set{height:36px; overflow:hidden; margin-bottom:5px;}
.category.height_set h1, .category.height_set h2, .category.height_set h3, .category.height_set h4, .category.height_set h5, .category.height_set h6{  margin:0px;padding:0 0 5px 0;  font-size: 14px;}
</style>								
								
								
								
			<div class="cont_rught">		
                      
                    <h5 class="add-title"><?=$jvalue->JobTitle;?></h5>
					<?php
					if($iscount==1)
					{
						?>
					<img src="<?=$imageurl;?>images/applied.png" class="applied"/>
					<?php
					}
					?>
                    <div class="info"> 
                      <span class="category"><?=$jvalue->position->Position;?></span> -
                      <span class="item-location"><i class="fa fa-map-marker"></i> <?=$jvalue->Location;?></span> <br>
                    <span> <strong><?=$jvalue->CompanyName;?></strong> </span>
					</div>
					
						</div>
						
					<div class="clear"></div>
					
					<?php
					if($jvalue->IsWalkin==1)
					{
				?>
				
	
 			
		
				
				
						<div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Walkin : </span>  
					    </div>
						<div class="col-sm-10 col-xs-8 left-text">
							<span class="category"><?=date('d M Y',strtotime($jvalue->WalkinFrom)).' - '.date('d M Y',strtotime($jvalue->WalkinTo));?></span>  
                      </div>
					 </div>
				<?php
					}
					?>
                    <div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Eligibility : </span>  
					    </div>
                    <div class="col-sm-10 col-xs-8 left-text">
							<?php
						if($jvalue->IsExp==0){
								$exp='Fresher';
						}
						elseif($jvalue->IsExp==1)
						{
								$exp=($jvalue->Experience!='')?$jvalue->Experience.' Year':'Experience';
						}
						elseif($jvalue->IsExp==2)
						{
								$exp='Both';
						}
						?>
							<span class="category"><?=$exp?></span> 
                      </div>
					 </div> 
					 
					 
					 </strong></p>
					 	
					 	 	
					 	
					  <div class="info bottom">
					     <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Designation : </span>  
					    </div>
					<div class="col-sm-10 col-xs-8 left-text">
							<span class="category">
							     
							    	
							  <?=$jvalue->Designation;?>
							</span>  
                      </div>
					 </div>
					 
					 	
					  <div class="info bottom" style="display:none">
					     <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Keyskills : </span>  
					    </div>
					<div class="col-sm-10 col-xs-8 left-text">
							<span class="category">
								<?php
								$jskill='';
								foreach($jvalue->jobRelatedSkills as $k=>$v)
								{
								$jskill.=$v->skill->Skill.' , ';
								}
								echo $jskill;
								?>
							</span>  
                      </div>
					 </div>
					
					   <div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Job Description : </span>  
					    </div>
                      <div class="col-sm-10 col-xs-8 left-text">
							<span class="category height_set"><?=htmlspecialchars_decode($jvalue->JobDescription);?></span>  
                      </div>
					 </div>
					 
					  <div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Salary Range </span>  
					    </div>
							<div class="col-sm-10 col-xs-8 left-text">
							<span class="category"><?=$jvalue->Salary;?> Lakhs</span>  
                      </div>
					 </div> 
					 
					<div class="info bottom"> 
						<span class="category" style="text-align:right"> <?=' ('.date('d M Y, h:i A',strtotime($jvalue->OnDate)).')';?></span> 
                    </div> 
                  </div>
                </div>
                </a>
              </div>
		<?php
		}
		}
		else
		{
		?>
		No Job Found In this Search Category
		<?php
		}
		?>
					 
					 <?php
							echo LinkPager::widget([
								'pagination' => $pages,
							]);
							?>
						<div class="spacer-2"></div>