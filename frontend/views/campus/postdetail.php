<?php
$this->title = $allpost->JobTitle;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>


	<div id="wrapper"><!-- start main wrapper -->
 
		<div class="headline_inner">
				<div class="row"> 
			       <div class=" container"><!-- start headline section --> 
						 <h2> <?=$allpost->JobTitle;?>   </h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	</div>
	
		 
		<div class="inner_page">
			<div class="container">
	 
					 <div class="row">
                   <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
				    <aside>
                                
                                <div class="company-detail">
                                    <div class="company-contact-detail">
                                        <table>
                                            <tbody><tr>
                                                <th>Name:</th>
                                                <td><?=$allpost->Name;?></td>
                                            </tr>
                                            <tr>
                                                <th>Email:</th>
                                                <td><?=$allpost->Email;?></td>
                                            </tr>
                                            <tr>
                                                <th>Phone:</th>
                                                <td> +91 <?=$allpost->Phone;?></td>
                                            </tr>
                                              <tr>
                                                <th>Landline Number:</th>
                                                <td> <?=$allpost->LandLine;?></td>
                                            </tr>
                                            
                                            <tr style="display:none">
                                                <th>Address:</th>
                                                <td> <?=$allpost->City.' ,'.$allpost->State;?></td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                            </aside>
                            
                         
                    </div>
                        <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                            <div class="single-job-page-2 job-short-detail">
                                <div class="heading-inner">
                                    <p class="title">Post Description</p>
                                </div>
                                <div class="job-desc job-detail-boxes">
                                    <p class="margin-bottom-20">
                                        <?=htmlspecialchars_decode($allpost->JobDescription);?>
                                    </p>
                                </div>

                            </div>
                        </div>
                        
                        
                        
                        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">     
                        
                        
                           	
                            <div class="widget">
                                <div class="widget-heading"><span class="title">Short Description</span></div>
                                <ul class="short-decs-sidebar">
                                     <li>
                                        <div>
                                            <h4>Venue :</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->Venue;?></div>
                                    </li>
                                  
                                    
                                  
                                    <li>
                                        <div>
                                            <h4>No Of Candidate:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->NoofVacancy;?></div>
                                    </li>
									

                                    
                                    									<li>
                                        <div>
                                            <h4>State:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->State;?></div>
                                    </li>
                                    	<li>
                                    
                                       <div>
                                            <h4>City:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->City;?></div>
                                    </li>
                                    
                                
                                	<li>
                                    
                                       <div>
                                            <h4>Date:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->WalkinTo;?></div>
                                    </li>
                                    
                                
                                									<li>
                                        <div>
                                            <h4>Course:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->course;?></div>
                                    </li>
									
									<li>
                                        <div>
                                            <h4>Specialization:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->specialization;?></div>
                                    </li>
                                   <?php if(!empty($allpost->jobRelatedSkills)){?>
                                    <li style="display:none">
                                        <div>
                                        <h4>Skill:</h4></div>
                                        <div><i class="fa fa-bars"></i>
                                        <?php
                                        $skill='';
                                        foreach($allpost->jobRelatedSkills as $k=>$v)
                                        {
                                        $skill.=$v->skill->Skill.' , ';
                                        }
                                        $skill=trim($skill," , ");
                                        echo $skill;
                                        ?>
                                        </div>
                                    </li>
								   <?php }?>
                                    <li>
                                        <div>
                                            <h4>Posted On:</h4></div>
                                        <div><i class="fa fa-calendar"></i><?=date('d M, Y',strtotime($allpost->OnDate));?> </div>
                                    </li>
                                    
                                </ul>
                            </div>
                        <?php
						if(Yii::$app->session['Campusid']){
											
						if($allpost->JobStatus==0)
						{?>
                            <div class="apply-job" onclick="campusclosestatus(<?=$allpost->CampusPostId;?>);">
                                  <a>  <i class="fa fa-trash-o"></i>Close Status</a>
                            </div> 
                        <?php
						}
						}else{
							$InterestedLink = Url::toRoute([
											'campus/appliedCampus',
											'JobId' => $allpost->CampusPostId
										]);
							?>
							<?php if($allpost->campusPostApplied){?> 
							  <div class="apply-job">	<a href="javascript:void(0);" class="btn-default"  style="background: #6c146b;><i class="fa fa-check"></i> Applied </a>  </div>
							<?php }else{?>	  <div class="apply-job">
								<a href="<?= Url::toRoute(['campus/appliedcampus','CampusPostId'=>$allpost->CampusPostId])?>" class="btn-default">Interested</a></div>
							<?php }?>
					<?php }?>
						
						
					 </div>	
                        
                        
                           
                </div>
      </div>
		    </div>
		<div class="border"></div>