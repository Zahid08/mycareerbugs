<?php

$this->title ='Studentlist';



use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

use yii\widgets\LinkPager;

if(isset($_POST['course']))

{

    $crs=$_POST['course'];

}

else

{

    $crs='';

}



if(isset($_POST['specialization']))

{

    $spec=$_POST['specialization'];

}

else

{

    $spec='';

}

?>



<div id="wrapper"><!-- start main wrapper -->

    <div class="headline job_head" style="display: none;">

        <div class=" container"><!-- start headline section -->

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 top-main bg-full margin_auto">

                    <h2 class="banner_heading">  <span>Search </span>  Student </h2>



                    <div class="sticky">

                        <?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form']]); ?>

                        <div class="group-sm group-top">



                            <div class="group-item col-md-4 col-xs-6">

                                <div class="form-group">

                                    <input type="text" name="studentname" class="form-control" placeholder="Student Name" />



                                </div>

                            </div>



                            <div class="group-item col-md-4 col-xs-12">

                                <div class="form-group">

                                    <select id="form-filter-location" name="course" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                        <option value=" ">Course</option>

                                        <?php

                                        if($course && !empty($allstudent))

                                        {

                                            foreach($course as $comk=>$comval)

                                            {

                                                ?>

                                                <option value="<?=$comval->courses->CourseId;?>" <?php if($crs==$comval->courses->CourseId) echo "selected";?>><?=$comval->courses->CourseName;?></option>

                                                <?php

                                            }

                                        }

                                        ?>

                                    </select>

                                </div>

                            </div>



                            <div class="group-item col-md-3 col-xs-6">

                                <div class="form-group">

                                    <select id="form-filter-location" name="specialization" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">

                                        <option value="">Specialization</option>

                                        <?php

                                        if($specialization && !empty($allstudent))

                                        {

                                            foreach($specialization as $sk=>$sval)

                                            {

                                                ?>



                                                <option value="<?=$sval->specializations->SpecializationId;?>" <?php if($spec==$sval->specializations->SpecializationId) echo "selected";?>><?=$sval->specializations->Specialization;?></option>

                                                <?php

                                            }

                                        }

                                        ?>

                                    </select>

                                </div>

                            </div>



                            <div class=" group-item reveal-block reveal-lg-inline-block col-md-1 col-xs-12">

                                <?= Html::submitButton('Search', ['name'=>'studentsearch','class' => 'btn btn-primary element-fullwidth']) ?>

                            </div>



                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>

                </div>



                <div class="clearfix"></div>

            </div>

        </div><!-- end headline section -->

    </div>





    <div class="inner_page">

        <div class="container">



            <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12" id="mobile_design">

                <div class="pannel_header margin_top">

                    <h4>Student List</h4>

                    <div class="width-10" id="select_per_page">

                        <div class="form-groups">

                            <div class="col-sm-8">

                                <label>Result Per page </label>

                            </div>

                            <div class="col-sm-4">

                                <?php

                                if(isset($_GET['perpage']))

                                {

                                    $perpage=$_GET['perpage'];

                                }else{$perpage=10;}

                                ?>

                                <select id="form-filter-location" data-minimum-results-for-search="Infinity" class="form-control23 select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="getresultperpagecampus(this.value,'studentlist');">

                                    <option value="10" <?php if($perpage==10) echo "selected='selected'";?>>10</option>

                                    <option value="20" <?php if($perpage==20) echo "selected='selected'";?>>  20  </option>

                                    <option value="30" <?php if($perpage==30) echo "selected='selected'";?>>  30 </option>

                                    <option value="40" <?php if($perpage==40) echo "selected='selected'";?>>  40  </option>

                                    <option value="50" <?php if($perpage==50) echo "selected='selected'";?>>  50  </option>

                                </select>

                            </div>

                        </div>

                    </div>

                </div>



                <?php

                if($allcourse && $allstudent)

                {

                    foreach($allcourse as $sk=>$sval)

                    {
                        $getCourseName=\common\models\NaukariQualData::findOne($sval->CourseId);
                        $cnt = 0;
                        if(isset($sval->student)){
                            $cnt = count($sval->student);
                        }

                        $specilizaitons=\common\models\NaukariSpecialization::findOne($sval->SpecializationId);
                        ?>

                        <div class="col-md-4 col-sm-12 col-xs-12">

                            <div class="profile-content payment_list">
                                <div class="clear"></div>
                                <div class="card">

                                    <div class="firstinfo">

                                        <img src="<?=($sval->campus->LogoId!=0)?$url.$sval->campus->logo->Doc:'/images/user.png';?>" alt="" class="img-circle img-responsive">

                                        <div class="profileinfo">

                                            <h1> <?=$sval->campus->Name;?> </h1>

                                            <!--<small>Annamalae University</small>-->

                                            <div class="spcae1"></div>



                                            <div class="row">

                                                <div class="col-md-12 col-sm-12 col-xs-12">

                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <ul class="commpany_desc">

                                                            <li>Course: <?=isset($getCourseName->name)?$getCourseName->name:'';?>   </li>

                                                            <li>Specialization:  <?=isset($specilizaitons->name)?$specilizaitons->name:'';?> </li>

                                                        </ul>

                                                    </div>



                                                </div>

                                            </div>



                                        </div>

                                    </div>



                                    <div class="contact_me">

                                        <a href="<?= Url::toRoute(['campus/allstudent','CourseId'=>$sval->CourseId,'specid'=>$sval->SpecializationId])?>" class="btn-default" type="button"> <i class="fa fa-envelope-o"></i>  View Detail   </a>

                                    </div>

                                </div>

                            </div>



                        </div>



                        <?php

                    }

                }

                ?>

            </div>



            <?php

            echo LinkPager::widget([

                'pagination' => $pages,

            ]);

            ?>

        </div>

    </div>





</div>