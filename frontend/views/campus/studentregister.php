<?php
$this->title = 'Campus Register';
$csrfToken = Yii::$app->request->getCsrfToken();
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<div id="wrapper"><!-- start main wrapper --> 
	
		<div class="headline_inner">
			
			       <div class=" container"><!-- start headline section --> 
				   	<div class="row"> 
						 <h2>  Register under your college</h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	</div>
	
		 
		<div class="inner_page">
			<div class="container">
		
  
			<div class="row main">
				 
				<div class="xs-12 col-sm-12 main-center" id="campus_left">  
				<h2>Education Information </h2>
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>
						
						<div class="form-group">
								<label for="confirm" class="cols-sm-2 control-label">  State  </label> 
									<div class="input-group full">
									   <select class="questions-category states form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="AllUser[State]" required id="stateId" onchange="getcollege(this.value);">
											 <option value="">Select State  </option>
										</select> 
								</div>
						</div>
						<div class="form-group" id="collegelist">
						</div>
						
						<div class="form-group" id="courselist">
						</div>
						
						<div class="form-group" id="speclist">
						</div>
						
						<div class="form-group ">
							<button type="submit" class="btn btn-primary btn-lg btn-block login-button">Submit</button>
						</div>
						 
					<?php ActiveForm::end(); ?>
				</div>
			</div>
      </div>
		    </div>
		<div class="border"></div>
	</div><!-- end main wrapper -->