<?php
$this->title = 'My Post';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<style>
.post_view{background: #f16b22;
    position: absolute !important;
    right: 0px;
    top: 57px;
    color: #fff;
    font-size: 12px;
    padding: 5px;
    height: 30px;
    z-index:999;
    width: 87px;
    text-align: center;
    line-height: 24px;
    border-radius: 5px 0 0 5px;}    
    
.post_view:hover {background: #6c146b;color:#fff;}

</style>
<div id="wrapper"><!-- start main wrapper -->

		<div class="inner_page" style="padding:25px 0 60px 0">
			<div class="container">

					 <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"  id="mobile_design">

					   <h4><i class="glyphicon glyphicon-briefcase"></i> My Post</h4>

		<?php
				foreach($allpost as $key=>$value)
				{
						if($value->JobStatus==0)
						{
								$status='Open';
								$background='green';
						}
						else
						{
								$status='Closed';
								$background='red';
						}
						//echo '<pre>';
						//print_r($value);die;
						?>
		<div class="item-list">
                <div class="col-sm-12 add-desc-box">
						<div class="applied_job" style="background: <?=$background;?>">
								<p><?=$status;?></p>
						</div>
						 <a href="<?= Url::toRoute(['campus/postdetail','JobId'=>$value->CampusPostId])?>" class="post_view">View Post</a>
						 
						<div class="edit_job">
								<a href="<?= Url::toRoute(['campus/jobedit','JobId'=>$value->CampusPostId])?>"><p><i class="fa fa-edit"></i> Edit Job</p></a>
						</div>
						 
                  <div class="add-details">
                    <a href="<?= Url::toRoute(['campus/postdetail','JobId'=>$value->CampusPostId])?>"><h5 class="add-title" style="margin-bottom: 4px;"><?=$value->JobTitle;?></h5>
                       <span style="font-size: 13px;">  <?=$value->Name;?>  </span>
                    </a>
					
					 <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">State & City : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?=$value->State;?> - <?=$value->City;?>  </span> </span>  
                      </div>
					 </div>
					 
					
					 <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Venue : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?=$value->Venue;?></span>  
                      </div>
					 </div>

					   <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Course : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?=$value->course;?></span>  
                      </div>
					 </div>
					 
					 
					 
					    <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Date : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text"> 
							<span class="category"><?=$value->WalkinTo;?></span>  
                      </div>
					 </div>
					 
					   <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Specialization : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?=$value->specialization;?></span>  
                      </div>
					 </div>
					 
					 <!--  <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Job Description : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category">   <?=substr(htmlspecialchars_decode($value->JobDescription),0,250);?>   </span>  
                      </div>
					 </div>  -->
					 
				
					 
					 
					  
					 
					<div class="info bottom"> 
						<span class="category" style="text-align:right">    Posted By <?=Yii::$app->session['CampusName'].' ('.date('d M Y',strtotime($value->OnDate)).')';?></span> 
                    </div> 
                   
                  </div>
                </div>
              </div>
 <?php
				}
				?>
 
  
                        </div>
      </div>
		    </div>
		 
		
		
		<div class="border"></div>