<?php
$this->title = 'FAQs';
//var_dump($model);
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl();
$url='/backend/web/';
?>
<div id="wrapper"><!-- start main wrapper -->
	 
	 
		 
		<div class="inner_page_extra_pages">
			<div class="container">
		
		<style>
		.inner_page_extra_pages{padding:20px 0 40px 0}
		  .tab_block h1{color:#f16b22; font-size:24px}
		  .tab_block h2{color:#f16b22; font-size:20px}
		  .faqHeader{  font-size:18px;  margin:20px 0 10px 0}
		  .tab-content{padding:10px}
		  .tab_block p{font-size:14px; line-height:25px; margin-left:20px }
 
    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'FontAwesome';
    content: "\f078";
        float: right;
        color: #F58723;
        font-size: 16px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-2deg);
        -moz-transform: rotate(-2deg);
        -ms-transform: rotate(-2deg);
        -o-transform: rotate(-2deg);
        transform: rotate(-2deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }

		</style>
		
<div class="tab_block">
   <h1> Freqyently asked questions    </h1>
   
   
    <div class="panel-group" id="accordion">
        <div class="faqHeader"> General  </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Can I get a job at my preferred location?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                   Off-course, you will get a job at your preferred locations because the collections of companies are available in different concerns at a different location. So you can search and get your preferred job at your preferred location.  
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Will I receive all job applications on my email id?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                  Yep, you can receive your preferred job applications on your email id.  This option will send all the job applications to the email id specified by you o receive the applications.  And the applications sent through the site will also be obtainable online in your response account.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Can I change my posted job?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                   You can always welcoming to change your details like your posted jobs, salary expectations and much more.  It is very easy and simple to change your posted job like just click on the settings and change your posted jobs and enter you’re preferred jobs in the given column. 
                </div>
            </div>
        </div>





        <div class="faqHeader"> Recruiters</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Why I need to hire you?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                  Hiring is the efficient and simplest form to hire the best employeer and deliver you to access lot of benefits. Every candidate considers new roles and screened via combination of proprietary algorithms and curation team in-house made up for past university admissions officers. 
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Why do I perceive various candidates every week? </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                   Intelligent hiring matches you active talent meaningfully and quickly. The candidates also opt into condensed job search and fast so you can stay in touch with them at the right moment. We also desire to keep the candidates based on the marketplace time and real-time shifts. 
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Any advice for successful recruiting?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                   The majority of the candidate you can contact will respond accepting interview request or beginning reason for whey they are not eager. Once, you involve with the candidate the biggest source of advice is to shift rapidly. The longer period you have to wait, offers and more companies will compete against. Besides, you should move forever as faster as the candidate comfortable with. 
                   
                </div>
            </div>
        </div>
         
       

        <div class="faqHeader">Team</div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
How can I move to another job with my team?</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                      You can register the details of your team on the job portal of career bugs. By the details you provided, you can find a better job.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Is there any unique feature to register team details?</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes, the career bugs team will offer you a separate page for the team. So, it is easy to register there and get the further job details.
                </div>
            </div>
        </div>



<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven1">Is there any procedure in job portal?</a>
                </h4>
            </div>
            <div id="collapseSeven1" class="panel-collapse collapse">
                <div class="panel-body">
                    Yes, you will find separate sections for the purpose to register with a team. The section will ask your personal info, team member details, educational qualification of team members, working experience and much more.
                </div>
            </div>
        </div>






    </div>
 

</div>
  
           </div>  
      </div>
 <div class="border"></div>