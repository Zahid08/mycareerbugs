<?php
$this->title = 'Are you recruiting? How can we help';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?> <meta name="keywords" content="My Career Bugs, Are you recruiting? How can we help, Recuriting help, " />   
<meta name="description" content="Facing blues while hiring? Let seasoned Industry Experts at My Career Bugs hire the best talents for you in the least TAT. Hiring Redefined!" />
<!-- start main wrapper --> 
	<div id="wrapper">
	    
	    <style>
	        .recruiting-top {position:relative; margin:0 0 0px 0;   background:url(https://mycareerbugs.com/images/recruiter_help.jpg) no-repeat; background-size:cover;
	        min-height:450px}
			.recruiting-top h2{position:absolute; color:#fff; top:31%; font-size:62px; font-weight:bold;left:0px; text-align:center; width:100%}
			
			
	    </style>
	 
	  <div class="recruiting-top">
	        <h2> Advertise your post with <br> My Career Bugs</h2>  
		  </div> 
		
		
		 <div class="step-to">
			<div class="container">
				<h1>Are you recruiting?</h1>
				<p>
					The most easy and simple ways to get the candidates by create an account in the website.  <br> The account is helps to the job seekers to identify your requirements.  <br> And make a profile of you for understand what kind of process is running in <br> your organization    and these all are helps    to get the more candidates for your company. 
				</p>
	
				<div class="step-spacer"></div>
				<div id="step-image">
					<div class="step-by-container">
						<div class="step-by">
							 <a href="login.html">
							<div class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-1.png" alt="MCb Create your account step 1">
								</div>
							</div>
							</a>
							<h5>Create an Account </h5>
						</div>
								
						<div class="step-by">
							 
							 <a href="login.html"><div class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-2.png"  alt="MCb Create your account step 2">
								</div>
							</div>	</a>
							<h5>Create your profile</h5>
						</div>
								
						<div class="step-by">
							 
							 <a href="login.html"><div class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-3.png" alt="MCb Create your account step 3">
								</div>
							</div>	</a>
							<h5>Post your Requirements</h5>
						</div>
								
						<div class="step-by">
							 
							 <a href="login.html"><div class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-5.png"  alt="MCb Create your account step 4">
								</div>
							</div>	</a>
							<h5>Get Candidates</h5>
						</div>
								
					</div>
				</div>
				<div class="step-spacer"></div>
			</div>
		</div>
		
		
		
		
		<div class="advertise" style="">
			<div class="container">
					<h1>Why advertise with My Career Bugs?</h1>
					<p>
						In the case of simply describing the job opportunities in your company, <br> you can also able to describe your company details and uniqueness to the visitors.  With that, <br> the job seekers can know the thorough details of your company.
					</p>

					<div class="counter clearfix">
						<div class="counter-container">
							<div class="counter-value">75%</div>
							<div class="line"></div>
							<p>More Applications  </p>
						</div>
			
						
						<div class="counter-container">
							<div class="counter-value">55%</div>
							<div class="line"></div>
							<p>More Interview</p>
						</div>
						
						<div class="counter-container">
							<div class="counter-value">90%</div>
							<div class="line"></div>
							<p>Right Candidates</p>
						</div>
						
						<div class="counter-container">
							<div class="counter-value">95%</div>
							<div class="line"></div>
							<p>More Hires</p>
						</div>
					</div>
				
			</div>
		</div>





  <div class="step-to">
			<div class="container">
				<h1>Search candidate for your company</h1>
				 
				<div class="step-spacer"></div> 
                 <div class="row main" id="register"> 
							<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
								  <h2>
								     <a  href="https://www.mycareerbugs.com/site/hirecandidate" > 
								         <img class="normal" src="<?=$imageurl;?>images/job_seekers.png"  alt="MCB Search Candidate">
								   	  <img class="img_white" src="<?=$imageurl;?>images/job_seekers_white.png" alt="MCB Search Candidate"> 
								    </a> 
								  </h2> 
							</div> 
                              <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
								  <h2>
								     <a  href="https://www.mycareerbugs.com/team/findteam" > 
								      <img class="normal" src="<?=$imageurl;?>images/team_orange.png" alt="MCB Find Team">
								   	  <img class="img_white" src="<?=$imageurl;?>images/team_white.png" alt="MCB Find Team"> 
								    </a> 
								  </h2> 
							</div> 
                             <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
								  <h2>
								     <a  href="https://www.mycareerbugs.com/campus/campuszone" > 
								      <img class="normal" src="<?=$imageurl;?>images/campus_register.png" alt="MCB Campus Search">
								   	  <img class="img_white" src="<?=$imageurl;?>images/campus_register_white.png" alt="MCB Campus Search"> 
								    </a> 
								  </h2> 
							</div>
 
						</div> 
				<div class="step-spacer"></div>
			</div>
		</div>
