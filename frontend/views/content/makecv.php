<?php
$this->title = 'Profile';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='https://www.mycareerbugs.com/backend/web/';
use common\models\AllUser;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\EmployeeSkill;
use common\models\Skill;
use common\models\NaukariQualData;
$alluser=new AllUser();
?>
<div id="wrapper"><!-- start main wrapper -->

		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
		         <div class="col-md-10 col-sm-10 col-xs-12"> 
                            <div class="job-short-detail">
                                <div class="heading-inner">
                                       <h3 class="title" style="line-height: 23px;font-size: 22px;font-weight: bold;color: #000;margin: 12px 0  5px 0; display: inline-block;padding: 0 0 5px 0;"><?=$profile->Name;?></h3>	
							 
									</div>
                                </div>
								
							  <h3 class="title" style="line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;">Profile Details </h3>	
								
                                <dl>
                                   
						 <p style="margin: 0 0 10px 0;padding:0 0 2px 0"><span style="font-weight:bold">About Me: </span>  
						 <?php if($profile->UserTypeId == 5){?>
						 
						 <?=$profile->TeamDesc;?> </p> 	

						 <?php }else{?>	
						  <?=$profile->AboutYou;?> </p> 	
						 <?php }?>
 
 
    <p style="margin:12px 0 0 0;padding:0 0 2px 0"><span style="font-weight:bold">Prefered Job Location: </span>  <?=$profile->getPreferedJobLocation();?>  </p> 
     <p style="margin:0px 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Language Known: </span>   <?=$profile->language;?> </p> 
   <p style="margin:0 0 14px 0 ;padding:0 0 2px 0"><span style="font-weight:bold">Gender: </span><?=$profile->Gender;?></p>
 
 
 
   <p style="margin:10px 0 0 0;padding:0 0 2px 0"><span style="font-weight:bold">Phone: </span>+91 <?=$profile->MobileNo;?></p> 
        <p style="margin:0px 0 0 0;padding:0 0 2px 0"><span style="font-weight:bold">Whatsapp no: </span>+91 <?=$profile->whatsappno;?>  </p> 
   <p style="margin:0px;padding:0 0 10px 0"><span style="font-weight:bold">Email: </span><?=$profile->Email;?></p> 

 
 
  
            <p style="margin:10px 0 0 0;padding:0 0 2px 0"><span style="font-weight:bold">Address: </span><?=$profile->Address;?></p> 
            <p style="margin:0px;padding:0 0 2px 0"><span style="font-weight:bold">Country: </span><?=$profile->Country;?></p> 
            <p style="margin:0px;padding:0 0 2px 0"><span style="font-weight:bold">State: </span><?=$profile->State;?></p> 
            <p style="margin:0px;padding:0 0 2px 0"><span style="font-weight:bold">City: </span><?=$profile->City;?></p>  
            <p style="margin:0 0 0px 0 ;padding:0 0 2px 0"><span style="font-weight:bold">Pincode: </span><?=$profile->PinCode;?></p>
  
                                </dl>
                            </div>
 
                        </div>
						 
	 <div class="clearfix"> </div>
	
						
				<div class="col-md-12 col-sm-12 col-xs-12">
				    
				    <h3 class="title" style="line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;">  Interested Area </h3>	
					
				<?php if($profile->CollarType == "white"){?>
						<p style="margin: 10px 0 0 0;padding:0 0 2px 0px"><span style="font-weight:bold"> Job Category: </span> 
						<?php $CategoryIds1 = ArrayHelper::map(UserJobCategory::find()->where(['user_id' => $profile->UserId])->all(), 'category_id', 'category');
						
						$CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $profile->UserId
																])->all(), 'category_id', 'category_id');
						$expCategory = ArrayHelper::map(WhiteCategory::find()->where([
												'id' => $CategoryIds
											])->all(), 'name', 'name');
						
						$jobCategory = array_merge($CategoryIds1, $expCategory);
						?>
						<?php echo (!empty($jobCategory)?implode(', ',$jobCategory):"");?></p> 
						
						<p style="margin: 0px 0 0 0;padding:0 0 2px 0px"><span style="font-weight:bold"> Role: </span> 
						<?php $userRole1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'role_id', 'role'); 
							  $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $profile->UserId,
																	])->all(), 'role', 'role');
							 $userRole = array_merge($userRole1, $expRoles);
						?>
																	
																	
																	
						<?php echo (!empty($userRole)?implode(', ',$userRole):"");?></p> 
						
						<p style="margin: 0px 0 0 0;padding:0 0 2px 0px"><span style="font-weight:bold"> Skills: </span> 
						<?php $jobSkills1 = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'skill_id', 'skill');
							 $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $profile->UserId,
																	])->all(), 'skill', 'skill');
							 $jobSkills = array_merge($jobSkills1, $expSkills);										
						?>
						<?php echo (!empty($jobSkills)?implode(', ',$jobSkills):"");?></p>
						
						

				<?php }else{?>	
					
					
				 <p style="margin: 10px 0 0 0;padding:0 0 2px 0px"><span style="font-weight:bold"> Role: </span> <?=$profile->getEducationRoles();?> </p>  
 
 
 <p style="margin: 0 0 10px 0;padding:0 0 2px 0px"><span style="font-weight:bold"> Skills: </span> 	<?php
												$skill='';
												foreach($profile->empRelatedSkills as $ask=>$asv)
												{
														$skill.=$asv->skill->Skill.', ';
												}
												$skill=trim(trim($skill),",");
												?>
                                            <span><?=$skill;?></span> </p>			
							
				<?php } ?>						
							
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <h3 class="title" style="line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;">Educational Information  
                                    <span style="display: none;"><?=($profile->educations[0]->DurationTo);?></span></h3>
                                </div>
								
								
                                <div class="row education-box"> 
<p style="margin: 0px 0 2px 0;padding:0 0 2px 15px"><span style="font-weight:bold"> Higest Qualification: </span>
  <?=$profile->educations[0]->nqual->name;?>
</p> 	
<p style="margin: 0 0 0 0;padding:0 0 2px 15px"><span style="font-weight:bold"> Course: </span> <?=$profile->educations[0]->ncourse;?> </p> 	

<p style="margin: 0 0 0 0;padding:0 0 2px 15px"><span style="font-weight:bold"> Specialization: </span>  <?=$profile->educations[0]->specialization_id;?> </p> 	



<p style="margin: 0 0 0 0;padding:0 0 2px 15px"><span style="font-weight:bold"> University / College: </span>   <?=$profile->educations[0]->University;?> </p> 	

<p style="margin: 0 0 0 0;padding:0 0 2px 15px"><span style="font-weight:bold"> Pass out Year: </span> <?=$profile->educations[0]->DurationTo;?> </p>  



 
                                </div>
                            
                          
                                
                                
								 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4> 	</h4>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
											
                                             </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						 
						 <div class="clearfix"> </div>
						 
						 
						 
				<?php
				if($profile->experiences)
				{
						foreach($profile->experiences as $k=>$val)
						{
				?>
				<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <h3  class="title" style="line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;">  Work Experience  <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span></h3>
                                </div>
                              
                                
                                <p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">  Company name : </span> <?=$val->CompanyName;?> </p>  
                                <p style="margin: 0 0 2px 0;padding:0 0 2px 0px"><span style="font-weight:bold">Position : </span> <?=$val->PositionName;?> </p> 
								<?php if($profile->CollarType == 'white'){?>
								
								<p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Job Category: </span> <?=$val->whitecategory->name;?> </p> 
								<p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Role : </span> 
								<?php $expRole = ArrayHelper::map(ExperienceWhiteRole::find()->where([
														'user_id' => $profile->UserId,
														'experience_id' => $val->ExperienceId
													])->all(), 'role_id', 'role'); ?>
													
								<?php echo (!empty($expRole)?implode(', ',$expRole):"");?> </p> 
								<p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Skills : </span> 
								<?php $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $profile->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'skill_id', 'skill');?>
								<?php echo (!empty($expSkills)?implode(', ',$expSkills):"");?> 									
								</p> 
								
								<?php }else{?>
                                <p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Role : </span> <?=$val->position->Position;?> </p>  
								
								<?php 
										/*Get Experience Skill*/
										$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
											'IsDelete' => 0,
											'SkillRoleId' => $val->position->PositionId,
											'UserId' => $profile->UserId,
											'ExperienceId' => $val->ExperienceId
										])->all(), 'Employeeskillid', 'SkillId');
										
										$skills = ArrayHelper::map(Skill::find()->where([
											'IsDelete' => 0,
											'SkillId' => $employeeSkill
										])->all(), 'SkillId', 'Skill');
										
									?>
								<p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Skills : </span> <?php echo $skills ? implode(", ",$skills) : "";?> </p>  
								<?php }?>
                               <p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Industry : </span>
                                <?php
											if($val->industry)
											{
											?>
                                            <span><?=$val->industry->IndustryName;?></span>
											<?php
											}
											?>
											</p>
											
							  <p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Experience : </span>
							  <?php
												$expy=$val->Experience.' Years';
												?>
                                            <span> <?=$expy;?></span>
                                            </p>
                             <p style="margin: 0 0 2px 0;padding:0 0 2px 0"><span style="font-weight:bold">Salary : </span> <?=$val->Salary.' Lakh';?> </p>
                                            
                              
								
								  
                            </div>
                        </div>
	
	
	

			  <?php
						}
				}
				?>
				
				
				
						 <div class="col-md-12 col-sm-12 col-xs-12">	
		   <div class="resume-box">
				<div class="heading-inner">
					<h3  class="title" style="line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;"> Team Members </h3>
				</div>
		
		<div class="row education-box">
			<div class="col-md-12 col-sm-12 col-xs-12">	
			<?php if($profile->teams){
							 foreach($profile->teams as $tk=>$tval){ ?>
							<div style="width:45%; float:left; padding-right:10px;">		
								<?php	$qualification = NaukariQualData::find()
													   ->select('name')
													   ->where(['id' => $tval->MemberQualification])
													   ->one();
										$course = NaukariQualData::find()
													   ->select('name')
													   ->where(['id' => $tval->MemberCourse])
													   ->one(); 
										 ?>

										   <h5> <?=$tval->MemberName;?>  </h5> 
											<p> <?=(isset($qualification->name)?$qualification->name:"");?>  </p> 
											<p> <?=(isset($course->name)?$course->name:"");?>  </p>  
											<p> <?=$tval->MemberSalary.' Lakh';?>  </p> 										
							</div>
						 <?php } ?>
			
						<?php }?>

				</div>

					</div>

				</div>

			  </div>
            </div>
       </div>
		 
		
		
		
		<div class="border"></div>
</div>
