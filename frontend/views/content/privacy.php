<?php
$this->title = 'Privacy Policy';
//var_dump($model);
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl();
$url='/backend/web/';
?>
<div id="wrapper"><!-- start main wrapper -->
	 
	 
		 
		<div class="inner_page_extra_pages">
			<div class="container">
		
		<style>
		.inner_page_extra_pages{padding:20px 0 40px 0}
		  .tab_block h1{color:#f16b22; font-size:22px}
		  .tab_block h3{  font-size:18px;}
		  .tab-content{padding:10px}
		  .tab_block p{font-size:14px; line-height:25px; margin-left:20px }
		</style>
		
<div class="tab_block">
   <h1>Privacy Policy    </h1>
 
 
 <h3>Introduction</h3>

<p>We are committed to protect the valuable information you share with us through this information. This privacy policy is centered around the premise to safeguard and protect the data gathered on the My Career Bugs Job Portal Pvt Ltd (MCB) website (hereinafter referred to as the “website”). The Policy is applicable to the website and does not apply to any other information or website. The policy might be updated from time to time and we request you to go through the policy before accessing our website.You may be required to provide personally identifiable information at several different points on our website. By accepting the policy at the time of registration, you expressly approve and consent to our collection, storage, use and disclosure of your personal information as described in this Policy and terms and conditions.
</p>
<h3>Personal Identifiable Information Instruction</h3>

<p>In order to operate the Site in an efficient and effective manner and provide users with information on job postings and services that may be of interest to them, MCB may collect personal information, including contact information (such as an email address), from its users. In addition, this information allows us to provide users with private and secure areas to post and modify their original content on our site (e.g., jobs and CVs). We also automatically track certain information based upon your behavior on the Site and use this information to do internal research on our users' demographics, interests, and behavior to better understand, protect and serve users in general. This information may include the URL that a user has just come from (whether this URL is on our Site or not), the URL a user goes to next (whether this URL is on our Site or not), a user’s computer browser information, and their IP address.
<br>
You agree that MCB may use your personal information to contact you and deliver information that is targeted to your interests (such as relevant job postings and services). By accepting this Privacy Policy, you expressly agree to receive this information. You may opt out of such communications at anytime.
<br>
If the user voluntarily display or distribute personal identifiable information (such as their email address, contact number, image, or CV), that information can be collected and used by others. In such events, the user may receive unsolicited messages from third parties for which MCB is not responsible.
<br>
MCB may also disclose specific user information when we determine that such disclosure is necessary to comply with the law, to cooperate with or seek assistance from law enforcement or to protect the interests or safety of MCB or other users of the Site. In addition, personal information we have collected may be passed on to a third party in the event of a transfer of ownership or assets or a bankruptcy of MCB.
</p>

<h3>Cookies Instructions</h3>

<p>Our web pages utilize “cookies” and other tracking technologies. A "cookie" is a small text file that may be used, for example, to collect information about Web site activity. At MCB, we use cookies only for the protection and convenience of our users. Cookies enable us to serve secure pages to our users without asking them to sign in repeatedly. If a user's system is idle for more than an hour, however, the cookie will expire, forcing the user to sign in again to continue their session. This prevents unauthorized access to the user's information while they are away from their computer. You may have the ability to reset your browser to refuse all cookies or to indicate when a cookie is being sent; however,  you will need to re-enter your original user ID and password to gain access to certain parts of the website.

<p>Third-Party Cookies: In the course of serving advertisements to this site, our third-party advertisers may place or recognize a unique "cookie" on your browser.
</p>

<h3>Third Party Advertisement Instructions </h3> 

<p>We often use third-party advertising companies to serve ads when you visit our Website. These companies may use information about you and your visits to this and other Web sites in order to provide advertisements on this site and other sites about goods and services of interest to you.
</p>
<h3>Security Instructions</h3>

<p>MCB cannot ensure that all of your private communications and other personal identifiable information will never be disclosed in ways not otherwise described in this Privacy Policy. Although we are committed to protecting your sensitive information, you should not expect, that your personal identifiable information or private communications will always remain private. As a responsible user of the website, you understand and agree that you assume all responsibility and risk for your use of the Site, the internet generally, and the documents you post or access and for your conduct on and off the website.</p>

<p>The Company does not encourage to share www.mycareerbugs.com account access with different entities, individuals & third parties. You are not authorized to share your password or other account access information with any other party, temporarily or permanently, and breach of this obligation may amount to suspension of your MCB account and MCB Services.
</p>
<p>MCB site prohibit the sharing of login/passwords with different entities/individuals. In the event of any breach of security or unauthorized use of your login ID / Account, you must notify MCB immediately. MCB will not be liable for any loss caused by any unauthorized use of your login ID / Account. You may be liable for the losses to MCB if any due to such unauthorized use. 
</p>
<h3>CV Privacy Options</h3>

<p>As soon as you upload your CV on MCB, we put your CV in front of hundreds and thousands of employers with open positions each day. We automatically create a registration for you using the email address and password you supply after you post your CV. The same email address and password can be used to access the website, update existing CV, search and apply for jobs in the future. 
<br>
Job seekers are concerned about their information privacy when it comes to posting their CV on the Internet. We give the most visibility to your CV to our broadest employer base by making your CV searchable in our CV Database.
</p>

<h3>Fraud Detection</h3>

<p>Unfortunately, online frauds are continuing concerns for every business on the internet. Fraudulent emails might be circulated to our users demanding security deposits, refundable fees, nominal charges, personal information in the name of MCB but they are in fact sent by imposters. MCB does not charge any sort of fees from its users nor sending any such fraudulent emails. If you receive any such email requesting your personal information or fees, of any sort please do not respond and immediately contact My Career Bugs at info@mycareerbugs.com.</p>

<h3>Contact Us</h3>

<p>If you have any questions or suggestions regarding our privacy policy, please contact us at: info@mycareerbugs.com</p>




 
<!--
<p>We are  My Career Bugs affiliated provide the complete online privacy and understand the exact protection and personal information. Our website sharing details are completely safe and guaranteed genuine information useful for the job seekers from our website. We deliver the smooth browsing experience Career Bugs to gather all the information related to job search and enable you to get relevant information to encounter your needs. 
</p>
<h3>Information security</h3>
<p>
We take focus on security measures to secure against unauthorized one to access or to disclosure, data destruction or unauthorized alteration. We specially avoid accessing personal information to users who need to know certain details to develop, improve or operate services. 
</p>
<h3>Updating information</h3>
<p>
We keep the appropriate and updating mechanisms for your personal identifying details for various services. 
<br>
We also deliver the details only to the trusted and reliable partners and we respond legal process, court orders and subpoenas or to exercise or establish legal rights or protect against legal claims. 
</p>

<h3>Terms and conditions</h3>

<p>
Our terms and conditions suitable for both job seekers and recruiters and make you to know all the essential details. In job seekers, you can check out job mail, resume writing, recruiter connection, resume display, My Career Bugs certifications, application by non-registered users, My Career Bugs toolbar, background check, RSS, jobs on the move, etc. For recruiters terms and conditions like hot vacancies, insta hire, e-apps, resdex, resume showcase, job gallery, manual shortlisting, best places to work, job search, hot vacancies subscriptions, etc. We give the guarantee for using My Career Bugs content for derivative and motive without before written consent strictly prohibited. We also utilize technological means to prohibit robots etc from move slowly the scraping content and website. We undertake the right services, which suit to you and make best practices. We focus on the guidelines, rules, byelaws, regulations and notifications. 
</p>-->



</div>
  
           </div>  
      </div>
 <div class="border"></div>