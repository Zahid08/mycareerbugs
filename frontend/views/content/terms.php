<?php
$this->title = 'Terms and Condition';
//var_dump($model);
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl();
$url='/backend/web/';
?>
<div id="wrapper"><!-- start main wrapper -->
	 
	 
		 
		<div class="inner_page_extra_pages">
			<div class="container">
		
		<style>
		.inner_page_extra_pages{padding:20px 0 40px 0}
		  .tab_block h1{color:#f16b22; font-size:22px}
		  .tab_block h2{  font-size:18px;}
		  .tab-content{padding:10px}
		  .tab_block p{font-size:14px; line-height:25px; margin-left:20px }
		</style>
		
<div class="tab_block">
   <h1>Terms & Conditions </h1>
    

<p><strong>ACCESS TO THE SITE INDICATES YOUR AGREEMENT TO ALL THE TERMS & CONDITIONS MENTIONED BELOW. SO PLEASE READ THE TERMS & CONDITIONS CAREFULLY BEFORE PROCEEDING FURTHER.</strong></p>

<p>My Career Bugs Job Portal Pvt. Ltd. does not encourage any job seekers to pay any money or share personal information like credit card or debit card details, CVV number, important personal documents, etc to anyone. We do not charge any sort of registration fee, refundable fees, document processing fees, security deposits or fees of any sort for the services rendered. In case of receipt of any such emails, letters or calls demanding money, we recommend you to do your thorough inquiry before responding to any such mail letters or calls. Beware of Scams. Be Alert. </p>

<p>If you already are a victim of fraud, we encourage you to report the fraudulent activity to your local police and contact us at query@mycareerbugs.com.</p>

<p>You can bring to our notice any such susceptible job postings by emailing us at query@mycareerbugs.com. We will take all the measurable steps for your safety.</p>

<h2>Legal Information</h2>

<p>The following mentioned Terms & Conditions (‘T&C’) are applicable upon all members who is gaining access to and use of  ‘My Career Bugs Job Portal’ (hereinafter referred to as “My Career Bugs” or ‘MCB’) websites, content, social media, blogs, etc and/or who are availing the recruitment services and other related services offered by MCB in India. Failure to meet or comply with the stated provisions, the person/organization is prohibited from using the MCB website or any services offered by them. </p>

<p>This Agreement is made between the Company and the people gaining access to the MCB website, the website visitor and/or registered user. The Company reserves the right to change these terms & conditions at any time and include changing the website, which may eliminate or discontinue any content or feature of the website, restrict the hours of availability or limiting the amount of use permitted of any services on the website.</p>

<h2>Changes to Website Terms & Conditions</h2>

<p>Any changes to the Website will be effective immediately upon notice, which we may communicate by any means including, without limitation, posting on the site or via electronic mail. Your use of the site after such notice will be deemed acceptance of such changes. We request you to review this Agreement periodically to ensure familiarity with the most current version.</p>

<h2>Trademarks & Copyrights</h2>

<p> c 2019 My Career Bugs All Rights Reserved </p>

<p>The Intellectual Property of MCB includes MCB Website, designs, text, graphics, images, video, information, logos, icons, software and other MCB content including all pages within and all codes related thereto. MCB prohibits re-printing or re-publishing any part of these materials or code on these pages or anywhere on the site in any form without the express written permission of the Company except as is necessary to view the page on your desktop screens, laptop, tablet, smartphone or any such viewable devices. All materials and content on the Site are for personal and non-commercial purposes. Any use of materials and contents on the Site for which you receive remuneration of any sort, whether in money or otherwise, is commercial use and is strictly prohibited.</p>

</p>
<p> The site is owned and operated by My Career Bugs Job Portal Pvt. Ltd. Unauthorized use of the materials on the site may violate copyright, trademark, patent and other laws and is prohibited. Your agreement to this T&C is an acceptance that you do not acquire any ownership rights by using the site.</p>

</p>
<p> According to copyright law in India, Copyright for websites subsists in a work the instant it is created. Hence, the moment MCB Website was created, we acquired copyright in it. We have a copyright notice at the bottom of the website for external parties to be aware of our possession of website copyright. It is shown below in the following manner - “c 2019 My Career Bugs All Rights Reserved”</p>
 </p>
<h2>Applicable Laws & Jurisdiction</h2>

<p>The T&C shall be governed by and interpreted in accordance with the laws of India. The applicable laws are the Companies Act, 2013 and Information & Technology Act, 2000 and Income Tax Act, 1961. The Company is headquartered in Kolkata, India.</p>

<h2>Key Definitions</h2>
  
<p>- “Job Seeker” means a person who is gaining access to this site to hunt for a job or in any other capacity except as an employer. <br>
- “Employer” means a person or any organization that is gaining access to MCB Site to post a job or utilize MCB’s services for any reason related to the purpose of finding and hiring talent for employment.<br>
- “Employer Materials” includes any brochures, emails, sample job postings, website content, career fair material, audios, videos, photographs, logos, trademarks, service marks, domain names, documents or other materials provided by Employer, if any, for use in connection with the services.<br>
- “Services” means any services provided or posted on MCB Website by My Career Bugs or its agents.<br>
- “You” means the person or entity on behalf of whom you are accessing this website that is agreeing to these Terms & Conditions.<br>
- “User” refers to any individual or entity that uses any aspect of this site.<br>
- “MCB Materials” includes any materials, action-plan, implementation methods, audios, videos, images, content or other Intellectual property rights that have been used during the provision of such services.<br>
- “Website/Site” means any website under MCB’s control, whether partial or otherwise and includes such site content, materials and all services provided by them.<br>
- “Document” means any sort of posting to MCB’s Site, Like job Postings, Resumes, Curriculum Vitae, banners, blogs, etc.<br>
- “Content” includes all text, graphics, design, and programming used to develop and build this site.<br>
- “Text” includes all text - editorial, navigational and instructional - on every page of the site.<br>
- “Graphics” includes all logos, buttons and other graphical elements on the site. Paid advertising banners are not a part of graphics.<br>
- “Design” includes the color combinations and the page layout of the site.<br>
- “Programming” includes both client-side code and server-side code used to develop this Website.<br>
- “Post” means information that you submit, publish or display on MCB Website. <br>
</p>



<h2>Eligibility </h2>

<p>You must be 18 years of age and above to use mycareerbugs.com in any manner, whatsoever. By visiting mycareerbugs.com you acknowledge to the Company that you are 18 years of age or above and you abide by all the terms and conditions laid down herein.  In addition to that, you pledge to use mycareerbugs.com in a manner consistent with all applicable laws and regulations and in a responsible manner whatsoever.</p>

<h2>License to Use by Job Seekers</h2>

<p>MCB grants Job seekers a limited, terminable, non-exclusive right to access and use the MCB Site for personal use only. The use of the Site is for the purpose of seeking employment opportunities for oneself. MCB permits you to download site materials only for your personal, noncommercial use. The users are solely responsible for all content contained in the document and posted on the MCB Site by them. They are solely responsible for any consequences arising therefrom. In the event of a breach of any Terms & Conditions, MCB reserves the right to suspend or terminate the access at any time.</p>

<h2>License to Use by Employers</h2>

<p>MCB grants employers a limited, terminable, non-exclusive right to access and use the MCB Site for their internal business only. The use of the Site is for the purpose of seeking candidates for employment. MCB permits you to download site materials only for your personal, noncommercial use which is directly connected to using the site for searching and recruiting talents and candidates for the purpose of employment. The employers are solely responsible for all content contained in the document and posted on MCB Site by them. They are solely responsible for any consequences arising therefrom. In the event of breach of any Terms & Conditions, MCB reserves the right to suspend or terminate the access at any time.</p>

<h2>Registration Information</h2>

<p>While gaining access to MCB Site, Users agree to provide accurate, current and complete information about you as may be requested by any registration forms on the site; and maintain and regularly update the registration data, and any other information you provide to MCB, to keep it accurate, current and complete. </p>

<p>The Company will not disclose to any third party your name, address, email address or contact number without your prior consent, except to the extent necessary or appropriate to comply with applicable laws or in legal proceedings where such information is relevant.</p>

<h2>Secure Your Passwords</h2>
<p>The Company does not encourage to share mycareerbugs.com account access with different entities, individuals & third parties. You are not authorized to share your password or other account access information with any other party, temporarily or permanently, and breach of this obligation may amount to disablement of your MCB account and MCB Services.</p>
<p>MCB site Terms & Conditions prohibit the sharing of login/passwords with different entities/individuals. In the event of any breach of security or unauthorized use of your login ID / Account, you must notify MCB immediately. MCB will not be liable for any loss caused by any unauthorized use of your login ID / Account. You may be liable for the losses to MCB if any due to such unauthorized use.</p> 

<h2>Data Privacy & SMS Alerts</h2>

<p>In respect of ‘alerts’ service being introduced by MCB, the candidate consents to the use of his email address, contact number and other personal information by MCB to forward relevant job alerts and other information as and when they become available.</p>

<h2>MCB Services </h2>
<p>The job postings on the website, resume database and other features of mycareerbugs.com may be used only for lawful purposes by job seekers and employers looking for jobs & employees respectively. Your use of MCB Services is also subject to any other contracts you may have with MCB. In case of any conflict between these T&C and any contract you have with MCB, the terms of your contract will prevail.</p>
<h2>Use of Website</h2>
 
 
 
 <p>The Company prohibits Users to do any of the following activities -    <br>
1- Users are prohibited to transmit, post, distribute, store or destroy material and MCB Content, in violation of any applicable law or regulation, or which is misleading in any manner whatsoever, or contains videos, photographs or images of another person, or harms the minor is anyway, or contains sexual and exploitative materials, or impersonates another person.<br>
2- Users are prohibited to post incorrect or misleading resumes like uploading more than one copy of the same to the public at any one time or apply for any job on behalf of another party;<br>
3- Users are prohibited to carry out any activities which shall create liability for Us or cause Us to lose (in whole or in part) the services of Our internet service providers ("ISPs") or other suppliers;<br>
4- Users must defer any contact from an employer to any agent, agency, or another third party;<br>
5- Users must not gain access to another party account, solicit passwords or PIN from other Users;<br>
6- Post any jobs on MCB Site for any competitor of MCB or posting jobs or other content that contains links to any site competitive with MCB;<br>
7- Users are refrained from posting jobs or content on MCB Site that contain any hyperlinks, email addresses, HTML Tags, "hidden" keywords, repeated keywords or any keywords that are irrelevant to the job or are otherwise misleading;<br>
8- Posting jobs for modeling or talent scouting positions on MCB Site is strictly prohibited;;<br>
9- Users are prohibited to use MCB Site Resume Database inappropriately for any purpose other than as an employer seeking employees;<br>
10- Submitting false, inaccurate or incomplete biographical information of self or any information which does not belong to the user itself is discouraged by MCB;<br>
11- Users shall not post any content or jobs that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource; or contains any trojan horses, worms, time bombs, cancelbots, easter eggs or other computer programming routines that may damage, detrimentally interfere with, diminish the value of, surreptitiously intercept or expropriate any system, data or personal information;<br>
12- MCB prohibits sending unsolicited emails, junk mail, chain letters, spamming and faxes or making unsolicited phone calls regarding promotions and/or advertising  of products or services to a user of any MCB Site;<br>
13- Users shall not engage in any sort of advertisements at MCB Site;<br>
14- Posting jobs or content that contain multiple job requirements in a single job-post on MCB Site is not allowed;<br>
15- Post jobs or content on MCB Site seeking money, periodic payment in any form.<br>
16- MCB Site should not be used for offering services like visa processing facilities, etc.;<br>
17- Aggregate, copy or duplicate in any manner any of the MCB Content or information available from MCB Site including expired job postings, other than as permitted by these Terms; <br>
18- Frame or link to any of MCB Content or information available from any MCB Site.<br>
19- Use the MCB Services for any unlawful purpose or any illegal activity, or post or submit any content, resume or job posting that is defamatory, libelous, implicitly or explicitly offensive, vulgar, obscene, threatening, abusive, hateful, racist, discriminatory, of a menacing character or likely to cause annoyance, inconvenience, embarrassment, anxiety or could cause harassment to any person or include any links to pornographic, indecent or sexually explicit material of any kind, as determined by MCB’s discretion.<br>
20- The Company also specifically prohibits any use/insertion of any of the User’s URL, weblink, email address, and website while posting any job on mycareerbugs.com and all Users agree not to use/insert any of the User’s URL, weblink, email address, and website while posting any job on mycareerbugs.com. </p>
<p>On nonconformance by users of any of the aforesaid terms, MCB may in its sole discretion, restrict or terminate the offending User's ability to access the MCB Site(s) and determine whether to take any other actions or to remove or request the removal of the User Content. MCB has no liability or responsibility to Users for performance or nonperformance of such activities. Additionally, MCB has the right to initiate appropriate proceedings against the offending user as per law. </p>

<h2>Access Third-Party Link on MCB Websites at Your Own Risk</h2>

<p>The Site may contain links to third party websites. MCB provides these links not to endorse the contents of third party websites but solely for the convenience of our users. MCB is not responsible for the content of linked third-party sites and does not vouch for the accuracy of materials or content posted on such third-party websites. Your access to linked third-party websites is at your own risk.</p>


<h2></h2>Return and Refund policy</h2>
<p> Users are assigned to recognize the terms of this Return and Refund Policy.
If you do not agree to the terms comprised in this Return and Refund Policy, you are instructed not to accept the Terms of Use and may forthwith leave and quit using the Platforms. 
The terms contained in this Return and Refund Policy shall be accepted without modification and you agree to be bound by the terms comprised here.
As there is no return or refund policy available with us.
</p>

<h2>You Question, We Answer</h2>

<p>Contact us for any questions or comments pertaining to the use of MCB Site at Feedback. We appreciate your response and would love to get back on them.</p>

<h2>Trademark complaint</h2>

<p>MCB acknowledges the intellectual property of others. If you feel that your Trademark has been infringed, You can write to MCB at info@mycareerbugs.com</p>

<h2>Exclusion of Liability</h2>

<p>MCB is not responsible or liable for any direct, indirect, general, special, punitive, incidental, indirect or consequential damages; loss of use; loss of data; loss caused by a virus; loss of income or profit; loss of or damage to property; claims of third parties; or other losses of any kind or character that might occur in connection with the use of the site or any website with which they are linked. MCB is not liable for any such damages even if the user has been informed of such damages in advance.</p>


<h2>Make Your Grievances Known to Us</h2>

<p>In accordance with Information Technology Act, 2000 and rules made thereunder, the name and contact details of the Compliance Officer are provided below:</p>

<p>The Compliance Officer <br>
Mr. Rohit Kumar Jaiswal <br>
Contact: 8240369924 <br>
Address: Cf 318 Sector 1 saltlake <br>
Email: info@mycareerbugs.com
</p>









		 
	 
     
  </div>
</div>
  
           </div>  
      </div>
 <div class="border"></div>
		  