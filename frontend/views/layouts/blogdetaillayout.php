<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use common\models\Industry;
use common\components\loader\Loader;
$indu = new Industry();

AppAsset::register($this);
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	
	<?php if(isset($this->params['meta_description']) && !empty($this->params['meta_description'])){?>	
<meta name="description" content="<?=$this->params['meta_description'];?>"/>
<?php }?>

<?php if(isset($this->params['meta_keywords']) && !empty($this->params['meta_keywords'])){?>	
<meta name="keywords" content="<?=$this->params['meta_keywords'];?>"/>
<?php }?>
<meta property="og:site_name" content="https://www.localhost"/>
<meta property="og:locale" content="en_US"/>

<?php if(isset($this->params['og_title']) && !empty($this->params['og_title'])){?>	
<meta property="og:title" content="<?=$this->params['og_title'];?>"/>
<?php }?>
<?php if(isset($this->params['og_description']) && !empty($this->params['og_description'])){?>	
<meta property="og:description" content="<?=$this->params['og_description'];?>"/>
<?php }?>
<?php if(isset($this->params['og_url']) && !empty($this->params['og_url'])){?>	
<meta property="og:url" content="<?=$this->params['og_url'];?>"/>
<?php }?>
<?php if(isset($this->params['og_image']) && !empty($this->params['og_image'])){?>	
<meta property="og:image" content="<?=$this->params['og_image'];?>"/>
<?php }?>
	
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	
	
	
<!-- Bootstrap core CSS -->
	<?php $this->head() ?>
<link href="<?=$imageurl;?>css/style_main.css" rel="stylesheet">

<link rel="shortcut icon" href="<?=$imageurl;?>img/icons/favicon.png" />
<link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="img/icons/apple-touch-icon-72.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="img/icons/apple-touch-icon-114.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="img/icons/apple-touch-icon-144.png">
<!-- Map -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<!-- Map -->
<style>
.widget {
	border: none !important;
}

html, body {
	font-size: 14px !important;
}

.post-title {
	padding: inherit !important;
	font-weight: normal !important;
}

h4 {
	font-size: 18px !important;
}

.blogp  p {
	color: #595959;
	font-size: 12px;
	line-height: 1.7;
	padding-top: 8px;
}
</style>
</head>
<body data-main='false' id='item'>
  <?php $this->beginBody() ?>
   <a href='#' id='to-top'><i class='fa fa-chevron-up'></i></a>
	<!---------------------------------------------- Header End ---------------------------------------------->

	<header id='blogheader'>

		<div class='top-nav-wrapper'>
			<div class='container section' id='top-nav' name='Top Nav'>
				<div class='blogwidget LinkList' id='LinkList111'>
					<div class='widget-content'>
						<ul class='clearfix'>
							<li><a href='#' target='_blank'><i class='fa fa-facebook'></i></a></li>
							<li><a href='#' target='_blank'><i class='fa fa-twitter'></i></a></li>
							<li><a href='#' target='_blank'><i class='fa fa-google-plus'></i></a></li>
							<li><a href='#' target='_blank'><i class='fa fa-youtube'></i></a></li>
							<li><a href='#' target='_blank'><i class='fa fa-flickr'></i></a></li>
							<li><a href='#' target='_blank'><i class='fa fa-instagram'></i></a></li>
						</ul>
						<div class='clear'></div>
						<span class='widget-item-control'> <span
							class='item-control blog-admin'> <a class='quickedit' href='#'> <img
									alt='' height='18' src="<?=$imageurl;?>images/blog_logo.png"
									width='18' />
							</a>
						</span>
						</span>
						<div class='clear'></div>
					</div>
				</div>
				<div class='blogwidget LinkList' data-version='1' id='LinkList123'>
					<i class='fa fa-bars top-nav-icon'></i>
					<div class='widget-content'>
						<ul class='clearfix'>
							<li><a href='<?= Url::toRoute(['site/index'])?>' target="_blank"><i
									class='fa fa-home icon'></i>Job Search</a></li>

							<li><a href='https://www.localhost/wall/index'><i
									class='fa fa-send icon'></i>MCB Wall</a></li>
						</ul>
						<div class='clear'></div>
						<span class='widget-item-control'> <span
							class='item-control blog-admin'> <a class='quickedit' href='#'> <img
									alt='' height='18' src="<?=$imageurl;?>images/blog_logo.png"
									width='18' />
							</a>
						</span>
						</span>
						<div class='clear'></div>
					</div>
				</div>
			</div>
		</div>




		<div class='logo-container'>
			<div class='container section' id='logo-container' name='Logo'>
				<div class='widget1 Header' id='Header1'>
					<div id='header-inner'>
						<a href='#'>
							<h1>
								<img id='Header1_headerimg'
									src="<?=$imageurl;?>images/blog_logo.png" width='350px; ' />
							</h1>
						</a>
					</div>
				</div>
			</div>
		</div>






		<div class='main-nav-wrapper'>
			<div class='container clearfix'>
				<div id='mobile-main-nav-btn'>
					<i class='fa fa-bars'></i> Main menu
				</div>

				<div class='section' id='main-nav' name='Main Navbar'>
					<div class='widget LinkList' id='LinkList222'>
						<button id='mobile-close-btn' type='button'>close</button>
						<div class='widget-content'>
							<ul id='main-menu'>
								<li><a href='#'> home</a></li>
								<li><a href='#'> Blogging</a></li>
								<li><a href='#'> how to</a></li>
								<li><a href='#'> recipes</a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
	</header>

	<!---------------------------------------------- Header End ---------------------------------------------->

	<!---------------------- Container Start ------------------------>
<?php
if (Yii::$app->session->hasFlash('error') || Yii::$app->session->hasFlash('success')) {
    if (Yii::$app->session->hasFlash('error')) {
        $flashimage = $imageurl . "images/error.png";
    } elseif (Yii::$app->session->hasFlash('success')) {
        $flashimage = $imageurl . "images/success.png";
    }
    ?>
		  <div class="modal add-resume-modal" style="display: block;"
		id="alertbox" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="input-group image-preview form-group "
						style="width: 100%; height: auto; text-align: center;">
						<img src="<?=$flashimage;?>" style="width: 70px;" />
                            <?= Alert::widget() ?>
							<input type="button" value="OK" aria-hidden="true"
							data-dismiss="alert" class="btn btn-lg btn-primary btn-block"
							style="width: 100px; margin: auto;"
							onclick="$('#alertbox').hide();$('.modal-backdrop').hide();" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-backdrop  in"></div>
<?php
}
?>
<?php echo Loader::widget()?>
        <?= $content ?>
<!---------------------- Container End ------------------------>







	<div style="height: 20px; background: #e9eaed"></div>




	<!---------------------- Footer Start ------------------------>
	<div id="footer">
		<!-- Footer -->
		<div class="container">
			<!-- Container -->
			<div class="row">
				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- Text Widget -->
					<?php
    $aboutus = common\models\FooterAboutus::find()->one();
    ?>
						<h6 class="widget-title"><?=$aboutus->Heading;?></h6>
					<div class="textwidget">
							<?=htmlspecialchars_decode($aboutus->Content);?>
						</div>
				</div>
				<!-- Text Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12  footer-widget">
					<!-- Footer Menu Widget -->
					<h6 class="widget-title">Hot Categories</h6>
					<div class="footer-widget-nav">
						<ul>
								 <?php
        $htcat = $indu->getHotcategory();
        if ($htcat) {
            foreach ($htcat as $hk => $hval) {
                ?>
								<li><a
								href="<?= Url::toRoute(['site/jobsearch','JobCategoryId'=>$hval->IndustryId])?>"><?=$hval->IndustryName;?></a></li>
							  <?php
            }
        }
        ?>
							</ul>
					</div>
				</div>
				<!-- Footer Menu Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- Recent Tweet Widget -->
						<?php
						$thirdblock = common\models\FooterThirdBlock::find()->one();
    ?>
						<h6 class="widget-title"><?=$thirdblock->Heading;?></h6>
					<div class="footer-widget-nav">
						<ul>
							<li><a href="<?= Url::toRoute(['site/index']);?>">Home</a></li>
							<li><a href="<?= Url::toRoute(['site/jobsearch']);?>">Job Search</a></li>
							<li><a href="<?= Url::toRoute(['site/employerslogin']);?>">Post a
									Job</a></li>
							<li><a href="<?= Url::toRoute(['site/login']);?>">Post a Resume</a></li>
							<li><a href="<?= Url::toRoute(['site/feedback']);?>">Feedback</a></li>
							<li><a href="<?= Url::toRoute(['content/privacy']);?>">Privacy
									Policy</a></li>
							<li><a href="<?= Url::toRoute(['content/terms']);?>">Terms &
									Condition</a></li>
							<li><a href="<?= Url::toRoute(['content/faq']);?>">Faq</a></li>
							<li><a href="<?= Url::toRoute(['site/blog']);?>" target="_blank">Blog</a></li>
						</ul>
					</div>
				</div>
				<!-- Recent Tweet Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- News Leter Widget -->
					<div id="contact_details">
					<?php
    $contactus = common\models\FooterContactus::find()->one();
    ?>
					  <h6 class="widget-title"><?=$contactus->Heading;?></h6> 
					     <?=htmlspecialchars_decode($contactus->Content);?>
					  </div>
					<h6 class="widget-title sig1">Singin For news Letter</h6>

					<div class="single-widget">
						<input autocomplete="off" type="email" id="news_email" type="text"
							class="col-xs-15 subscribeInputBox"
							placeholder="Your email address">
						<button
							class="col-xs-7 btn btn-theme-secondary subscribeBtn reset-padding"
							onclick="newsletter();">Subscribe</button>
						<div class="clearfix"></div>
					</div>

					<p>Register now to get updates on latest Vacancy.</p>

				</div>
				<!-- News Leter Widget -->
				<div class="clearfix"></div>
			</div>

			<div id="footer-btm">
				<!-- Footer -->
				<div class="container">
					<!-- Container -->
					<div class="row">
					 <?php
    $socialicon = common\models\SocialIcon::find()->one();
    ?>
					   <ul class="list-inline social-buttons">
						 <?php
    if ($socialicon->IsTwitter == 1) {
        ?>
                        <li><a href="<?=$socialicon->TwitterLink;?>"
								target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php
    }
    if ($socialicon->IsFacebook == 1) {
        ?>
                        <li><a href="<?=$socialicon->FacebookLink;?>"
								target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php
    }
    if ($socialicon->IsLinkedin == 1) {
        ?>
                        <li><a href="<?=$socialicon->LinkedinLink;?>"
								target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php
    }
    if ($socialicon->IsGoogleplus == 1) {
        ?>
                        <li><a href="<?=$socialicon->GoogleplusLink;?>"
								target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<?php
    }
    ?>
                    </ul>

					</div>
				</div>
				<!-- Container -->
			</div>




			<div class="developed_by_e-developed_technology">
				<div class="row">
					<div class="col-md-6">
			   <?php
    $copyright = common\models\FooterCopyright::find()->one();
    echo htmlspecialchars_decode($copyright->Content);
    ?>
					
			   </div>
					<div class="col-md-6">
			   <?php
	$dblock = common\models\FooterDevelopedblock::find()->one();
    echo htmlspecialchars_decode($dblock->Content);
    ?>
					
			   </div>
				</div>
			</div>

		</div>
		<!-- Container -->
	</div>
	<!-- Footer -->

	<!---------------------- Footer End ------------------------->

<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>