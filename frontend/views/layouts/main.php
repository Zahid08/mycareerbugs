<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\models\Messages;
use common\models\AppliedJob;
use common\models\Industry;
$mess = new Messages();
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
$apl = new AppliedJob();
$indu = new Industry();
use common\models\Notification;
use common\models\CompanyWallPost;
use common\components\loader\Loader;
$notification = new Notification();
$campusnotification = $notification->getcampusnot();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?>, For Career Direction, use our exclusive Interview Guide and unlock several Job Opportunities </title>


<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


<meta name="keywords"
	content="Kolkata job, MCB Wall Post, MCB post, wall post,  Need Jobs, Online Jobs, Marketing Jobs, Bpo Jobs, It Jobs, Hr Jobs, Marketing Jobs,  Job in India, job in kolkata, job vacancies, job openings, find jobs,  My career bugs, Mycareer bugs, freshers jobs, experienced jobs" />
<meta name="description"
	content="My Career Bugs, MCB, is one of the fastest growing job portal in India which provides high quality curated job opportunities to all it's users. We believe in building career together and providing the right career direction to help you accomplish your future goals." />

<meta name="keywords" content="MCB" />
<script type="application/ld+json">             {                "@context" : "http://schema.org",                "@type" : "Organization",                "name" : "My Career Bugs",                "url" : "https://www.localhost",                "sameAs" : [ "https://www.facebook.com/mycareerbugs", "https://twitter.com/mycareerbugs", 			   "https://in.linkedin.com/jobs/my-career-bugs-jobs"  ]              }       </script>

<link rel="shortcut icon" href="<?=$imageurl;?>images/icons/favicon.png" />
<!-- Map -->
<script
	src="https://maps.googleapis.com/maps/api/js?&key=AIzaSyD9v82zwcqWdBEkhRQXmJt33FKcDfHRg-A"></script>
<!-- Map -->
	 <?php
if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Campusid']) || isset(Yii::$app->session['Teamid'])) {
    ?>
  <link href="<?=$imageurl;?>css/after_login_css.css" rel="stylesheet">
  <?php
}
?>
    <?php $this->head() ?>
    
     <style>
    
    .small-23.ds.home_3{display:none}
    
    @media (max-width: 767px){
        .small-23.ds.home_3{display:block; padding:0px;padding: 0;margin: 0 2px 0 0;width: 30px;}
        .small-23.ds.home_3 a{color: #fff;background-color: #f16b22;border-color: #f16b22;border: 1px solid transparent;white-space: nowrap;padding: 4px 7px 1px 6px;font-size: 12px;
        line-height: 1.42857143;border-radius: 4px; line-height: 19px; font-size: 18px;display: block;} 
    }
    
    </style>
</head>
<body>
    <style>
        .navbar-default .navbar-nav>li>a {
        padding: 15px 14px 10px 15px!important;
        }
    </style>
    <div id="no-charge" style="text-align:center;width:100%; background:#6d136a; color:#fff; line-height:34px;font-size:14px"> 
    mycareerbugs.com never charge for a job, registration or an interview in exchange of money. </div>
    
<?php $this->beginBody() ?>
<!--------------------------------------------------------------------------------------------- 
----------------------------------------- Start Header  ---------------------------------------
---------------------------------------------------------------------------------------------->
	<a href="<?= Url::toRoute(['wall/index'])?>" class="wall"><img
		src="<?=$imageurl;?>images/career_wall.png"> </a>
	<!--Sidebar_contact-->
	<div class="need_help">
		<span style="cursor: pointer" onclick="openNav2()"> <img
			src="<?=$imageurl;?>images/need_help.png"></span>

		<div class="contact_form" id="right_side_form">
			<h2 class="banner_heading">
				How can we<small> Help you ? </small>
			</h2>
			<div class="closed">
				<a href="javascript:void(0)"><i class="fa fa-times"
					aria-hidden="true"></i></a>
			</div>
			<input type="text" class="form-control" name="name" id="contact_name"
				placeholder="Name"> <input type="text" class="form-control"
				name="name" id="contact_number"
				onblur="contactvlidation(this.value);" placeholder="Contact Number"
				maxlength="10" onkeypress="return numbersonly(event)"> <input
				type="text" class="form-control" name="contact_email"
				id="contact_email" placeholder="Email Id"
				onblur="return checkemail();"> <select id="form-filter-location"
				data-minimum-results-for-search="Infinity"
				class="contact_from form-control select2-hidden-accessible"
				tabindex="-1" aria-hidden="true">
				<option value="1">Select</option>
				<option value="1">Fresher</option>
				<option value="1">Experience</option>
				<option value="2">Company</option>
				<option value="3">Consultancy</option>
				<option value="4">Collage / Institute</option>
				<option value="4">Others</option>
			</select>
			<textarea class="form-control textarea1" id="contact_message"
				placeholder="Message"></textarea>
			<button class="btn btn-lg btn-primary btn-block" type="button"
				onclick="contactfrom();">Send</button>
		</div>
	</div>
	<!--Sidebar_contact-->

	<!--top_nav-->
	<div id="mySidenav1" class="top_nav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav1()">&times;</a>
		<!-- Main Navigation -->
		<div class="navbar navbar-default " role="navigation">
			<div class="collapse navbar-collapse" style="display: block">
				<img class="responsive_logo"
					src="<?=$imageurl;?>images/responsive_logo.jpg">
				<ul class="nav navbar-nav">
					<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
					<li><a href="<?= Url::toRoute(['site/hirecandidate'])?>"> Hire
							Candidate</a></li>
					<li><a href="<?= Url::toRoute(['team/findteam'])?>"> Hire Team</a></li>
					<li><a href="<?= Url::toRoute(['site/login'])?>">Post a Resume</a></li>
					<li><a href="<?= Url::toRoute(['job-by-company'])?>"> Job By
							company </a></li>
					<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Companies
							Walk-In-Inerview </a></li>
					<li><a href="<?= Url::toRoute(['site/login'])?>"> Upload CV </a></li>
					<li><a href="<?= Url::toRoute(['site/register'])?>"> Sign up</a></li>
					<li><a href="<?= Url::toRoute(['site/login'])?>"> Employer Login</a></li>
					<li><a href="<?= Url::toRoute(['site/login'])?>"> Login</a></li>
					<div class="clearfix"></div>
				</ul>
		     
				 	  
			<?php
			$socialicon = common\models\SocialIcon::find()->one();
?>
					   <ul class="list-inline social-buttons">
						 <?php
    if ($socialicon->IsTwitter == 1) {
        ?>
                        <li><a href="<?=$socialicon->FacebookLink;?>"
						target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php
    }
    if ($socialicon->IsFacebook == 1) {
        ?>
                        <li><a href="<?=$socialicon->TwitterLink;?>"
						target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php
    }
    if ($socialicon->IsLinkedin == 1) {
        ?>
                        <li><a href="<?=$socialicon->LinkedinLink;?>"
						target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php
    }
    if ($socialicon->IsGoogleplus == 1) {
        ?>
                        <li><a href="<?=$socialicon->GoogleplusLink;?>"
						target="_blank"><i class="fa fa-instagram"></i></a></li>
						<?php
    }
    ?>
                    </ul>
			</div>
		</div>
	</div>
	<!--top_nav-->


	<!--sidebar search-->
	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div class="group-sm group-top" id="top_side_fixed">
			<h2 class="banner_heading">
				Find a <span>Job </span> You Will <span> Love </span>
			</h2>
			<div class="spacer1"></div>
					 <?php $form = ActiveForm::begin(['action' => Url::toRoute(['site/index']), 'options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form']]); ?>
					 <div class="group-item col-md-12 col-xs-12">
				<div class="form-group">
					<input type="text" class="form-control" name="keyname"
						id="keyname1" placeholder="Job title, skills, or company">
				</div>
			</div>
			<div class="group-item col-md-12 col-xs-12">
				<div class="form-group">
					<input type="text" class="form-control" name="indexlocation"
						id="indexlocation1" placeholder="Location">
				</div>
			</div>
			<div class="group-item col-md-12 col-xs-6">
				<div class="form-group">
					<select id="form-filter-experience" name="experience"
						data-minimum-results-for-search="Infinity"
						class="form-control select2-hidden-accessible" tabindex="-1"
						aria-hidden="true">
						<option value="">Experience</option>
						<option value="Fresher">Fresher</option>
						<option value="0-1">Below 1 Year</option>
					    <?php
                            for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
                                $frn = $fr + 1;
                                ?>
                                 <option value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                <?php } ?>
						  </select>
				</div>
			</div>

			<div class="group-item col-md-12 col-xs-6">
				<div class="form-group">
					<select id="form-filter-salary" name="salary"
						data-minimum-results-for-search="Infinity"
						class="form-control select2-hidden-accessible" tabindex="-1"
						aria-hidden="true">
						<option value="">Salary</option>
						<option value="0 - 1.5">0 - 1.5 Lakh</option>
						<option value="1.5 - 3">1.5 - 3 Lakh</option>
						<option value="3 - 6">3 - 6 Lakh</option>
						<option value="6 - 10">6 - 10 Lakh</option>
						<option value="10 - 15">10 - 15 Lakh</option>
						<option value="15 - 25">15 - 25 Lakh</option>
						<option value="Above 25">Above 25 Lakh</option>
						<option value="Negotiable">Negotiable</option>
					</select>
				</div>
			</div>

			<div
				class=" group-item reveal-block reveal-lg-inline-block col-md-12 col-xs-12">
						<?= Html::submitButton('Search', ['name'=>'indexsearch','class' => 'btn btn-primary element-fullwidth']) ?>
					  </div>
					  <?php ActiveForm::end(); ?>
            </div>
	</div>
	<!--sidebar search-->
	<!-------------------------------------  before login-------------------------->

<?php
if (! isset(Yii::$app->session['Employeeid']) || ! isset(Yii::$app->session['Employerid']) || ! isset(Yii::$app->session['Campusid']) || ! isset(Yii::$app->session['Teamid'])) {
    ?>
 <!-- Head Top-->
	<div id="head">
		<div class="container">
			<div class="row">
				<!--mobile -->
				<div class="col-xs-2 col-sm-1  menu_humb">
					<div class="  _login">
						<a href="#" onclick="openNav1()"> <span class="fa fa-bars"></span></a>
					</div>

				</div>
				<!--mobile -->
				<div class="col-lg-4  col-md-4 col-sm-6 col-xs-8 top-main">
					<!-- logo -->
					<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs"
						rel="home"> <img class="main-logo"
						src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs" />
					</a>
				</div>
				<!-- logo -->
				<div class="col-lg-8  col-md-8 col-sm-6 col-xs- 2 main-nav">
					<!-- Main Navigation -->

					<div class="head_right">
						<ul>
							<li class=""><strong> Are you recruiting?</strong> <a
								href="<?= Url::toRoute(['content/help'])?>"> <span>How we can
										help</span>
							</a></li>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/companypostofline'])?>"> Post a Job - It's Free</a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->

				</div>
				<div class="col-xs-2  col-sm-2  ds">
					<div class="dropdown _login">
						<a href="<?= Url::toRoute(['site/login'])?>"> <span
							class="fa fa-user"></span></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- Main Navigation -->
		</div>
	</div>
	<!-- Head -->


	<!-- Start main header Menu -->
	<div id="header">
		<div class="find_a_job">
			<span style="cursor: pointer" onclick="openNav()"> <img
				src="<?=$imageurl;?>images/find-icon.png"></span>
		</div>
		<div class="container">
			<!-- container -->
			<div class="row">
				<div class="col-lg-7  col-md-7 col-sm-7 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav">
								<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
								<li><a href="<?= Url::toRoute(['site/hirecandidate'])?>"> Hire
										Candidate</a></li>
								<li><a href="<?= Url::toRoute(['team/findteam'])?>"> Hire Team</a></li>
								<li><a href="<?= Url::toRoute(['site/jobseekersregister'])?>">Post
										a Resume</a></li>
								<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Walk-in Jobs</a></li>
								<!--<li><a href="<?= Url::toRoute(['site/companypostofline'])?>">Create-->
								<!--		New Post</a></li>-->

							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->

				<div class="col-lg-5  col-md-5 col-sm-5 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse no_pad">
							<ul class="nav navbar-nav right_algn">
								<li><a href="<?= Url::toRoute(['team/index'])?>"> <i
										class="fa fa-sign-in" aria-hidden="true"></i>&nbsp Team
								</a></li>
								<li><a href="<?= Url::toRoute(['campus/campuszone'])?>"> <i
										class="fa fa-sign-in" aria-hidden="true"></i>&nbsp Campus Zone
								</a></li>
								<li><a href="<?= Url::toRoute(['site/register'])?>"> <i
										class="fa fa-sign-in" aria-hidden="true"></i>&nbsp Sign up
								</a></li>
								<li><a href="<?= Url::toRoute(['site/login'])?>" class="log"> <i
										class="fa fa-sign-in" aria-hidden="true"></i> &nbsp Login
								</a></li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->
				<div class="clearfix"></div>
			</div>

		</div>
		<!-- container -->
	</div>
	<!-- end main header -->
<?php
} else {
    if (isset(Yii::$app->session['Employeeid'])) {
        ?>
  <!-- Head  Start -->
	<div id="head">
		<div class="container">
			<div class="row">
				<!-- logo -->
				<div class="col-lg-4  col-md-4 col-sm-5 col-xs-6  no_pad top-main">
					<a href="<?= Url::toRoute(['site/index'])?>" title="Career Bugs"
						rel="home"> <img class="main-logo"
						src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
					</a>
				</div>
				<!-- logo -->
				<!--mobile -->
				<div class="col-xs-6 no_pad  rt_flt">
					<div class="small-23   ds">
						<!-- user -------------------------->
						<div class="dropdown">
							<button class="btn btn-primary  after_login_image"
								onclick="openNav12()" type="button">
								<img src="<?=Yii::$app->session['EmployeeDP'];?>"
									style="width: 25px; height: 25px;" alt=""
									class="after_login_profile_photo">
							</button>
							</ul>
						</div>
					</div>
					<div class="small-23  ds">
						<!-- Notification --------------------->
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-bell mbl"></span>
							</button> 
											 <?php
        $alnotification = [];
        if (isset(Yii::$app->view->params['employeenotification']))
            $alnotification = Yii::$app->view->params['employeenotification'];
        ?>
								<ul class="dropdown-menu">
								   <?php

        if ($alnotification) {

            foreach ($alnotification as $nk => $nval) {
                if ($nk < 3) {
                    if ($nval->ModelType == get_class(new CompanyWallPost())) {
                        $route = [
                            'wall/post-view',
                            'id' => $nval->JobId
                        ];
                    } else {
                        $route = [
                            'site/jobdetail',
                            'JobId' => $nval->JobId,
                            'Nid' => $nval->NotificationId
                        ];
                    }
                    ?>
								    <li class=""><a target="_blank" href="<?= Url::toRoute($route)?>">
										<span class="notiLabel"><img
											src="<?=($nval->job->employer->UserTypeId==2 || $nval->job->employer->UserTypeId==5)?(($nval->job->employer->PhotoId!=0)?$url.$nval->job->employer->photo->Doc:$imageurl."images/user.png"):(($nval->job->employer->LogoId!=0)?$url.$nval->job->employer->logo->Doc:$imageurl."images/user.png");?>"
											style="width: 43px; height: 43px;" /></span> <span
										class="notiLabel"><?=$nval->job->employer->Name;?></span> <span
										class="notiCount"><?=$nk+1;?></span>
										<p>
											<span class="noti_Description fullWidth"><?=$nval->job->JobTitle;?> at <?=$nval->job->Location;?> , <?=$nval->job->City;?></span><span
												class="status"><?=date("F j, Y, g:i a",strtotime($nval->OnDate));?></span>
										</p>
								</a></li>
									<?php
                }
            }
            ?>
								   <li class=""><a
									href="<?= Url::toRoute(['site/candidatenotification'])?>"><span
										class="notiLabel">View All</span></a></li>
								   <?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No
											Notification</span></a></li>
								<li class=""><a
									href="<?= Url::toRoute(['site/candidatenotification'])?>"><span
										class="notiLabel">View All</span></a></li>
								   <?php
        }
        ?>
								</ul>
						</div>
					</div>
					<div class="small-23  ds">
						<!-- Message -------------------------------->
						<div class="dropdown">
									<?php
        $messagelist = $mess->getMessage(Yii::$app->session['Employeeid']);
        $messagelist1 = $mess->getMessagecount(Yii::$app->session['Employeeid']);
        $mlink = (count($messagelist1) == 0) ? Url::toRoute([
            'message/index'
        ]) : '';
        ?>
									 <button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-envelope-o mbl"></span>
							</button>
							<ul class="dropdown-menu">
								   <?php
        if ($messagelist) {
            $mc = 0;
            foreach ($messagelist as $mk => $mval) {
                if ($mc < 3) {
                    ?>
									 <li class=""><a
									href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
										<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
											style="width: 43px; height: 43px;" /></span> <span
										class="notiLabel"><?=$mval['Name'];?></span>
										<p>
											<span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span
												class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
										</p>
								</a></li>
									 <?php
                }
                $mc ++;
            }
            ?>
									  <li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
										class="notiLabel">View All Message</span></a></li>
									  <?php
        } else {
            ?>
									<li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
									<?php
        }
        ?>
							   </ul>

						</div>

					</div>


	                            <div class="small-23  ds home_3"> 
            						<div class="dropdown">  
            							<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"><span class="fa fa-home mbl"></span></a>	
            						 
            							</div>
								</div>
				</div>
				<!--mobile -->
				<!-- Main Navigation -->
				<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav">
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav">
								<li class="no-need"><a
									href="<?= Url::toRoute(['site/jobbycompany'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-file-text orange"></b> <br> Company List
								</a></li>
								<li class="no-need">
							 <?php
        $msgcount = count($messagelist1);

        ?>
							   <a href="#" class="dropdown-toggle brdr orange_bg new_style"
									data-toggle="dropdown"> <b class="fa fa-envelope orange"></b>
										<br>   <span class="total total-message"><?=$msgcount;?></span>
								</a>
									<ul class="dropdown-menu">
								   <?php
        if ($messagelist) {
            $mc = 0;
            foreach ($messagelist as $mk => $mval) {
                if ($mc < 3) {
                    ?>
									 <li class=""><a
											href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
												<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
													style="width: 43px; height: 43px;" /></span> <span
												class="notiLabel"><?=$mval['Name'];?></span>
												<p>
													<span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span
														class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
												</p>
										</a></li>
									 <?php
                }
                $mc ++;
            }
            ?>
									  <li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
												class="notiLabel">View All Message</span></a></li>
									  <?php
        } else {
            ?>
									<li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
									<?php
        }
        ?>
							   </ul>

								</li>
								<li class="no-need">
							  <?php
        $alnotification = [];
        if (isset(Yii::$app->view->params['employeenotification'])) {
            $alnotification = Yii::$app->view->params['employeenotification'];
            $empnotcount = count(Yii::$app->view->params['employeenotification']);
            ?>
								<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
									data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>
										 <span class="total"><?=$empnotcount;?></span></a>

									<ul class="dropdown-menu">
								   <?php
            if ($alnotification) {
                foreach ($alnotification as $nk => $nval) {
                    if ($nk < 3) {
                        if ($nval->ModelType == get_class(new CompanyWallPost())) {
                            $route = [
                                'wall/post-view',
                                'id' => $nval->JobId
                            ];
                        } else {
                            $route = [
                                'site/jobdetail',
                                'JobId' => $nval->JobId,
                                'Nid' => $nval->NotificationId
                            ];
                        }
                        ?>
								    <li class=""><a target="_blank"
											href="<?= Url::toRoute($route)?>">
												<span class="notiLabel"><img
													src="<?=($nval->job->employer->UserTypeId==2 || $nval->job->employer->UserTypeId==5)?(($nval->job->employer->PhotoId!=0)?$url.$nval->job->employer->photo->Doc:$imageurl."images/user.png"):(($nval->job->employer->LogoId!=0)?$url.$nval->job->employer->logo->Doc:$imageurl."images/user.png");?>"
													style="width: 43px; height: 43px;" /></span> <span
												class="notiLabel"><?=$nval->job->employer->Name;?></span> <span
												class="notiCount"><?=$nk+1;?></span>
												<p>
													<span class="noti_Description fullWidth"><?=$nval->job->JobTitle;?> at <?=$nval->job->Location;?> , <?=$nval->job->City;?></span><span
														class="status"><?=date("F j, Y, g:i a",strtotime($nval->OnDate));?></span>
												</p>
										</a></li>
									<?php
                    }
                }
                ?>
								   <li class=""><a
											href="<?= Url::toRoute(['site/candidatenotification'])?>"><span
												class="notiLabel">View All</span></a></li>
								   <?php
            } else {
                ?>
								   <li class=""><a href="#"><span class="notiLabel">No
													Notification</span></a></li>
										<li class=""><a
											href="<?= Url::toRoute(['site/candidatenotification'])?>"><span
												class="notiLabel">View All</span></a></li>
								   <?php
            }
            ?>
								</ul>
								<?php
        }
        ?>
							</li>
								<li class="no-need prfl_img"><a href="javascript:void(0)"
									class="brdr orange_bg new_style" onclick="openNav12()"> <img
										style="width: 43px; height: 43px;"
										src="<?=Yii::$app->session['EmployeeDP'];?>" alt=""
										class="img-responsive center-block "><!--<?=Yii::$app->session['EmployeeName'];?>--></a>
								</li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- Head  End -->

	<!-- Right sidebar start Menu  -->
	<div id="mySidenav12" class="sidenav12">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a>
		<div class="side_bar-main">
			<img style="width: 80px; height: 80px;"
				src="<?=Yii::$app->session['EmployeeDP'];?>" alt="" class="">
			<h2>  <?=Yii::$app->session['EmployeeName'];?>  </h2>
			<hr class="height-2">
			<ul>

				<li class=""><a href="<?= Url::toRoute(['wall/candidateprofile'])?>"> My 	Profile</a></li>
						 
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB 	Wall Post</a></li>
				<li class=""><a href="<?= Url::toRoute(['/myactivity'])?>"> My Activities</a></li>
				
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/jobsearch'])?>"> Job 	Search </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/recentjob'])?>"> Recent Jobs </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin 	Jobs </a></li>


				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/bookmarkjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Employeeid']);?></a></li>
				<li class=""><a href="<?= Url::toRoute(['site/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobApplied']; ?>    </a></li>
				
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/editprofile'])?>">
						Edit Profile</a></li>
				<li class=""><a
					href="<?= Url::toRoute(['site/employeechangepassword'])?>"> Change
						Password </a></li>
						
			<?php
        if (isset(Yii::$app->session['CampusStudent'])) {
            ?>
			<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Company Job
						For Campus</a></li>
			<?php
        }
        ?>
			<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/employeelogout'])?>">
						Logout </a></li>
			</ul>

		</div>
	</div>
	<!-- Right sidebar end Menu  -->

	<!-- header start Menu  -->
	<div id="header">
		<div class="find_a_job">
			<span style="cursor: pointer" onclick="openNav()"> <img
				src="<?=$imageurl;?>images/find-icon.png"></span>
		</div>
		<div class="container">
			<!-- container -->
			<div class="row">
				<div class="col-lg-10  col-md-8 col-sm-8 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav float-left">
								<li><a href="<?= Url::toRoute(['site/userdashboard'])?>">Dashboard</a></li>
								<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB wall
										Post</a></li>
								<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
								<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent Jobs</a></li>
								<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin Jobs </a></li>
								<li><a href="<?= Url::toRoute(['site/appliedjob'])?>"> Applied
										Jobs <span class="total"><?=Yii::$app->session['NoofjobApplied']; ?></span>
								</a></li>
								<li><a href="<?= Url::toRoute(['site/bookmarkjob'])?>"> Bookmark
										Jobs <span class="total"><?=$apl->getTotalbookmark(Yii::$app->session['Employeeid']);?></span>
								</a></li>
										<?php
        if (isset(Yii::$app->session['CampusStudent'])) {
            ?>
			<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Company Job
										For Campus</a></li>
			<?php
        }
        ?>
									  </ul>
						</div>
						<!--/.nav-collapse -->
					</div>

					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->

				<div class="clearfix"></div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- header end Menu  -->  
  <?php
    } elseif (isset(Yii::$app->session['Employerid'])) {
        $totalcan = $apl->getTotalcandidate(Yii::$app->session['Employerid']);
        $totalcampus = $apl->getTotalcampus(Yii::$app->session['Employerid']);
        ?>
  <!-- Head  Start -->
	<div id="head">
		<div class="container">
			<div class="row">
				<!-- logo -->
				<div class="col-lg-4  col-md-4 col-sm-5 col-xs-6  no_pad top-main">
					<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs"
						rel="home"> <img class="main-logo"
						src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
					</a>
				</div>
				<!-- logo -->
				<!--mobile -->
				<div class="col-xs-6 no_pad  rt_flt">
					<div class="small-23   ds">
						<!-- user -------------------------->
						<div class="dropdown">
							<button class="btn btn-primary  after_login_image"
								onclick="openNav12()" type="button">
								<img src="<?=Yii::$app->session['EmployerDP'];?>"
									style="width: 25px; height: 25px;" alt=""
									class="after_login_profile_photo">
							</button>
							</ul>
						</div>
					</div>
					<div class="small-23  ds">
						<!-- Notification --------------------->
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-bell mbl"></span>
							</button> 
											<?php
        $empnotification = [];
        if (isset(Yii::$app->view->params['employernotification']))
            $empnotification = Yii::$app->view->params['employernotification'];
        ?>
								<ul class="dropdown-menu">
								   <?php
        if ($empnotification) {
            foreach ($empnotification as $nk1 => $nval1) {
                if ($nk1 < 3) {
                    $exp = '';
                    $link = 'searchcandidate';
                    if ($nval1->user->experiences) {
                        $exp = 'Having' . $nval1->user->experiences[0]->Experience . ' Year Experience';
                    } elseif ($nval1->user->UserTypeId == 4) {
                        $exp = 'Company';
                        $link = 'campusdetail';
                    } else {
                        $exp = $nval1->user->educations[0]->course->name;
                    }
                    $ctext = '';
                    if ($nval1->user->CampusId != 0) {
                        $ctext = ' ( University - ' . $nval1->user->campus->CollegeName . ')';
                    }
                    ?>
								     <li class=""><a target="_blank"
									href="<?= Url::toRoute(['wall/'.$link,'userid'=>$nval1->UserId])?>">
										<span class="notiLabel"><img
											src="<?=($nval1->user->UserTypeId==2 || $nval1->user->UserTypeId==5)?(($nval1->user->PhotoId!=0)?$url.$nval1->user->photo->Doc:$imageurl."images/user.png"):(($nval1->user->LogoId!=0)?$url.$nval1->user->logo->Doc:$imageurl."images/user.png");?>"
											style="width: 43px; height: 43px;" /></span> <span
										class="notiLabel"><?=$nval1->user->Name.$ctext;?></span> <span
										class="notiCount"><?=$nk1+1;?></span>
										<p>
											<span class="noti_Description fullWidth">  <?=$exp;?> , <?=$nval1->user->City;?></span><span
												class="status"><?=date("F j, Y, g:i a",strtotime($nval1->OnDate));?></span>
										</p>
								</a></li>
									<?php
                }
            }
            ?>
								   <li class=""><a
									href="<?= Url::toRoute(['site/empnotification'])?>"><span
										class="notiLabel">View All</span></a></li>
								   <?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No
											Notification</a></li>
								<li class=""><a
									href="<?= Url::toRoute(['site/empnotification'])?>"><span
										class="notiLabel">View All</span></a></li>
								   <?php
        }
        ?>
								</ul>
						</div>
					</div>
					<div class="small-23  ds">
						<!-- Message -------------------------------->
						<div class="dropdown">
								    <?php
        $messagelist = $mess->getMessage(Yii::$app->session['Employerid']);
        $messagelist1 = $mess->getMessagecount(Yii::$app->session['Employerid']);
        $msgcnt = count($messagelist1);
        ?>
									 <button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-envelope mbl"></span>
							</button>

							<ul class="dropdown-menu">
								   <?php
        if ($messagelist) {
            $mc = 0;
            foreach ($messagelist as $mk => $mval) {
                if ($mc < 3) {
                    ?>
									 <li class=""><a
									href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
										<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
											style="width: 43px; height: 43px;" /></span> <span
										class="notiLabel"><?=$mval['Name'];?></span>
										<p>
											<span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span
												class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
										</p>
								</a></li>
									 <?php
                }
                $mc ++;
            }
            ?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
										class="notiLabel">View All Message</span>
										<p>
											<span class="status"></span>
										</p></a></li>
									<?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
        }
        ?>
									 </ul>

						</div>
					</div>

                            <div class="small-23  ds home_3"> 
            						<div class="dropdown">  
            							<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"><span class="fa fa-home mbl"></span></a>	 
            							</div>
								</div>
								
				</div>
				<!--mobile -->

				<!-- Main Navigation -->
				<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav">
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav">
							    
							 	<li class="no-need"><a
									href="<?= Url::toRoute(['/wall/mcbwallpost'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-home orange" style="font-size: 18px;line-height: 32px; display: block;"></b> <br>  
								</a></li>
								
								
								<li class="no-need"><a
									href="<?= Url::toRoute(['campus/jobbycampus'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-university orange"></b> <br> 
								</a></li>
								<li class="no-need"><a href="#" data-toggle="dropdown"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-envelope orange"></b> <br> 
										<span
										class="total total-message"><?=$msgcnt;?></span>
								</a>
									<ul class="dropdown-menu">
								    <?php
        if ($messagelist) {
            $mc = 0;
            foreach ($messagelist as $mk => $mval) {
                if ($mc < 3) {
                    ?>
									 <li class=""><a
											href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
												<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
													style="width: 43px; height: 43px;" /></span> <span
												class="notiLabel"><?=$mval['Name'];?></span>
												<p>
													<span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span
														class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
												</p>
										</a></li>
									 <?php
                }
                $mc ++;
            }
            ?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
												class="notiLabel">View All Message</span>
												<p>
													<span class="status"></span>
												</p></a></li>
									<?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
        }
        ?>
									 </ul></li>
								<li class="no-need">
							  <?php
        $empnotification = [];
        if (isset(Yii::$app->view->params['employernotification'])) {
            $empnotification = Yii::$app->view->params['employernotification'];
            $emprnotcount = count(Yii::$app->view->params['employernotification']);
            echo $emprnotcount;exit;
            ?>
								<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
									data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>  <span class="total totalempnot"><?=$emprnotcount;?></span></a>
									<ul class="dropdown-menu">
								    <?php
            if ($empnotification) {
                foreach ($empnotification as $nk1 => $nval1) {
                    if ($nk1 < 3) {
                        $exp = '';
                        $link = 'customerdetail';
                        if ($nval1->user->experiences) {
                            $exp = 'Having ' . $nval1->user->experiences[0]->Experience . ' Year Experience';
                        } elseif ($nval1->user->UserTypeId == 4) {
                            $exp = 'Company ';
                            $link = 'campusdetail';
                        } else {
                            $exp = $nval1->user->educations[0]->course->name;
                        }
                        $ctext = '';
                        if ($nval1->user->CampusId != 0) {
                            $ctext = ' ( University - ' . $nval1->user->campus->CollegeName . ')';
                        }
                        ?>
								     <li class=""><a target="_blank"
											href="<?= Url::toRoute(['site/'.$link,'UserId'=>$nval1->UserId,'Nid'=>$nval1->EmpnId])?>">
												<span class="notiLabel"><img
													src="<?=($nval1->user->UserTypeId==2 || $nval1->user->UserTypeId==5)?(($nval1->user->PhotoId!=0)?$url.$nval1->user->photo->Doc:$imageurl."images/user.png"):(($nval1->user->LogoId!=0)?$url.$nval1->user->logo->Doc:$imageurl."images/user.png");?>"
													style="width: 43px; height: 43px;" /></span> <span
												class="notiLabel"><?=$nval1->user->Name.$ctext;?></span> <span
												class="notiCount"><?=$nk1+1;?></span>
												<p>
													<span class="noti_Description fullWidth">  <?=$exp;?> , <?=$nval1->user->City;?></span><span
														class="status"><?=date("F j, Y, g:i a",strtotime($nval1->OnDate));?></span>
												</p>
										</a></li>
									<?php
                    }
                }
                ?>
								   <li class=""><a
											href="<?= Url::toRoute(['site/empnotification'])?>"><span
												class="notiLabel">View All</span></a></li>
								   <?php
            } else {
                ?>
								   <li class=""><a href="#"><span class="notiLabel">No
													Notification</a></li>
										<li class=""><a
											href="<?= Url::toRoute(['site/empnotification'])?>"><span
												class="notiLabel">View All</span></a></li>
								   <?php
            }
            ?>
								</ul>
								<?php
        }
        ?>
							</li>
								<li class="no-need prfl_img"><a href="javascript:void(0)"
									class="brdr orange_bg new_style" onclick="openNav12()"> <img
										style="width: 43px; height: 43px;"
										src="<?=Yii::$app->session['EmployerDP'];?>" alt=""
										class="img-responsive center-block "><!--<?php echo Yii::$app->session['EmployerName']; ?>  --></a>
								</li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- Head  End -->



	<!-- Right sidebar start Menu  -->
	<div id="mySidenav12" class="sidenav12">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a>
		<div class="side_bar-main">
			<img style="width: 80px; height: 80px;"
				src="<?=Yii::$app->session['EmployerDP'];?>" alt="" class="">
			<h2>  <?php echo Yii::$app->session['EmployerName']; ?>  </h2>
	 <?php $emp_name = preg_replace('/\s+/', '', Yii::$app->session['EmployerName']); ?>
	   <hr class="height-2">
			<ul>
				<li class=""><a
					href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".Yii::$app->session['Employerid']?>">
						My Profile</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
						Wall Post</a></li>
				<li class=""><a
					href="<?= Url::toRoute(['wall/companyprofile/'])?><?=Yii::$app->session['EmployeeName'];?>">
						MCB Profile</a></li>
				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['site/companyprofileeditpage'])?>"> Edit
						Profile</a></li>
				<li class=""><a
					href="<?= Url::toRoute(['site/employerchangepassword'])?>"> Change
						Password </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/hirecandidate'])?>">
						Hire Candidate </a></li>
				<li class=""><a href="<?= Url::toRoute(['team/findteam'])?>"> Hire
						Team </a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/campuspost'])?>">
						Campus Post </a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/jobbycampus'])?>">
						Campus List </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/candidateapplied'])?>">Applied Candidate  <?=$totalcan;?></a></li>
				<li class=""><a href="<?= Url::toRoute(['site/campusapplied'])?>">Applied Campus  <?=$totalcampus;?></a></li>
				<li class=""><a href="<?= Url::toRoute(['site/companypost'])?>">
						Create Post </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"> My
						Post Jobs </a></li>
				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['site/candidatebookmarked'])?>"> Bookmark
						Jobs </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/candidateapplied'])?>">
						Applied Jobs</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/employerlogout'])?>">
						Logout </a></li>
			</ul>
			<div class="start_a_project">
				<a href="<?= Url::toRoute(['site/companypost'])?>"> Post a new job</a>
			</div>
		</div>
	</div>
	<!-- Right sidebar end Menu  -->





	<!-- header start Menu  -->
	<div id="header">
		<div class="find_a_job">
			<a href="<?= Url::toRoute(['site/hirecandidate'])?>"><span
				style="cursor: pointer"> <img src="<?=$imageurl;?>images/find-icon.png"></span> </a>
		</div>
		<div class="container">
			<!-- container -->
			<div class="row">
				<div class="col-lg-10  col-md-8 col-sm-8 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav float-left">
								<li><a href="<?= Url::toRoute(['site/empdashboard'])?>">Dashboard</a></li>
								<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB Wall
										Post</a></li>
								<li><a href="<?= Url::toRoute(['site/hirecandidate'])?>">Hire
										Candidate</a></li>
								<li><a href="<?= Url::toRoute(['team/findteam'])?>">Hire Team</a></li>
								<li><a href="<?= Url::toRoute(['campus/campuspost'])?>"> Campus
										Post </a></li>
								<!--<li><a href="<?= Url::toRoute(['campus/jobbycampus'])?>">  Campus List    </a></li> -->
								<li><a href="<?= Url::toRoute(['site/candidateapplied'])?>">
										Applied Candidate <span class="total"><?=$totalcan;?></span>
								</a></li>
								<li><a href="<?= Url::toRoute(['site/campusapplied'])?>">
										Applied Campus <span class="total"><?=$totalcampus;?></span>
								</a></li>
								<li><a href="<?= Url::toRoute(['site/yourpost'])?>"> My Post
										Jobs </a></li>
								<!--<li><a href="<?= Url::toRoute(['site/companypost'])?>"> Create-->
								<!--		New Post </a></li>-->

							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>

					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->

				<div class="clearfix"></div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- header end Menu  -->  
  <?php
    } elseif (isset(Yii::$app->session['Campusid'])) {
        ?>
   <div id="head">
		<div class="container">
			<div class="row">
				<!--mobile -->
				<div class="col-xs-2 menu_humb extra_mar">
					<button class="btn btn-primary dropdown-toggle"
						onclick="openNav1()">
						<small class="fa fa-bars mbl"></small>
					</button>
				</div>
				<!--mobile -->
				<div class="col-lg-4  col-md-4 col-sm-5 col-xs-6  no_pad top-main">
					<!-- logo -->
					<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs"
						rel="home"> <img class="main-logo"
						src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
					</a>
				</div>
				<!-- logo -->

				<!--mobile -->
				<div class="col-xs-3 no_pad  rt_flt">
					<div class="small-23   ds">
						<!-- user -->
						<div class="dropdown">
							<button class="btn btn-primary  after_login_image"
								onclick="openNav12()" type="button">
								<img src="<?=Yii::$app->session['CampusDP'];?>"
									style="width: 25px; height: 25px;" alt=""
									class="after_login_profile_photo">
							</button>

							</ul>
						</div>
					</div>



					<!-- Notification -->
					<div class="small-23  ds">
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-bell mbl"></span>
							</button>

							<ul class="dropdown-menu" id="ntfctn">
														<?php
        if ($campusnotification) {
            foreach ($campusnotification as $nk1 => $nval1) {
                if ($nk1 < 3) {
                    ?>
								     <li class=""><a target="_blank" href="<?=$nval1['link'];?>">
										<span class="notiLabel"><img src="<?=$nval1['image'];?>"
											style="width: 43px; height: 43px;" /></span> <span
										class="notiLabel"><?=$nval1['name'];?></span> <span
										class="notiCount"><?=$nk1+1;?></span>
										<p>
											<span class="noti_Description fullWidth">  <?=$nval1['title'];?> , <?=$nval1['address'];?></span><span
												class="status"><?=date("F j, Y, g:i a",strtotime($nval1['date']));?></span>
										</p>
								</a></li>
									<?php
                }
            }
            ?>
								   <li class=""><a
									href="<?= Url::toRoute(['site/campusnotification'])?>"><span
										class="notiLabel">View All</span></a></li>
								   <?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No
											Notification</a></li>
								<li class=""><a
									href="<?= Url::toRoute(['site/campusnotification'])?>"><span
										class="notiLabel">View All</span></a></li>
								   <?php
        }
        ?>
														
												 </ul>
						</div>
					</div>

				</div>
				<!--mobile -->


				<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav">

								<!-- It will display when customer paid  for that -->
								<li class="no-need"><a
									href="<?= Url::toRoute(['campus/createpost'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-file-text orange"></b> <br> Campus Post
								</a></li>
								<!-- It will display when customer paid  for that -->



								<!-- It will display when customer paid  for that -->
								<li class="no-need" style="display: none;"><a
									href="<?= Url::toRoute(['site/hirecandidate'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-file-text orange"></b> <br> Candidate List
								</a></li>
								<!-- It will display when customer paid  for that -->

								<li class="no-need"><a href="#"
									class="dropdown-toggle brdr  orange_bg new_style"
									data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>
										 <span class="total totalempnot"><?=count($campusnotification);?></span></a>
									<ul class="dropdown-menu">
								   <?php
        if ($campusnotification) {
            foreach ($campusnotification as $nk1 => $nval1) {
                if ($nk1 < 3) {
                    ?>
								     <li class=""><a target="_blank" href="<?=$nval1['link'];?>">
												<span class="notiLabel"><img src="<?=$nval1['image'];?>"
													style="width: 43px; height: 43px;" /></span> <span
												class="notiLabel"><?=$nval1['name'];?></span> <span
												class="notiCount"><?=$nk1+1;?></span>
												<p>
													<span class="noti_Description fullWidth">  <?=$nval1['title'];?> , <?=$nval1['address'];?></span><span
														class="status"><?=date("F j, Y, g:i a",strtotime($nval1['date']));?></span>
												</p>
										</a></li>
									<?php
                }
            }
            ?>
								   <li class=""><a
											href="<?= Url::toRoute(['site/campusnotification'])?>"><span
												class="notiLabel">View All</span></a></li>
								   <?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No
													Notification</a></li>
										<li class=""><a
											href="<?= Url::toRoute(['site/campusnotification'])?>"><span
												class="notiLabel">View All</span></a></li>
								   <?php
        }
        ?>
								</ul></li>

								<li class="no-need prfl_img"><a href="javascript:void(0)"
									class="brdr orange_bg new_style" onclick="openNav12()"> <img
										style="width: 43px; height: 43px;"
										src="<?=Yii::$app->session['CampusDP'];?>" alt=""
										class="img-responsive center-block "><!--<?php echo Yii::$app->session['CampusName']; ?> --> </a>
									<ul class="dropdown-menu">
										<li class=""><a
											href="<?= Url::toRoute(['campus/campusprofile'])?>"> <b
												class="fa fa-user"></b> My Profile
										</a></li>
										<li class=""><a
											href="<?= Url::toRoute(['campus/campusprofileedit'])?>"><b
												class="fa fa-pencil-square-o"></b> Edit Profile</a></li>
										<li class=""><a
											href="<?= Url::toRoute(['campus/changepwd'])?>"> <b
												class="fa fa-lock"></b> Change Password
										</a></li>
										<li class=""><a
											href="<?= Url::toRoute(['campus/campuslogout'])?>"> <b
												class="fa fa-power-off"></b> Log Out
										</a></li>
									</ul></li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- Main Navigation -->
		</div>
	</div>


	<div id="mySidenav12" class="sidenav12">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a>

		<div class="side_bar-main">
			<img style="width: 83px; height: 83px;"
				src="<?=Yii::$app->session['CampusDP'];?>" alt="" class="">
			<h2><?php echo Yii::$app->session['CampusName']; ?></h2>
			<hr class="height-2">

			<ul>
				<li class=""><a href="<?= Url::toRoute(['wall/campusprofile'])?>">
						MCB Profile</a></li>
<!-- 				<li class=""><a href="< ?= Url::toRoute(['campus/campusprofile'])?>"> -->
<!-- 						My Profile</a></li> -->
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
						Wall Post </a></li>
				

				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['campus/campusprofileedit'])?>"> Edit
						Profile</a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/changepwd'])?>">
						Change Password </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/jobsearch'])?>"> Job
						Search </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/yourpost'])?>"> My
						Post Jobs </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/bookmarkedjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Campusid']);?></a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobAppliedc'];?>    </a></li>
				<li><a href="<?= Url::toRoute(['campus/studentlist'])?>">Student
						List</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/campuslogout'])?>">
						Logout </a></li>
			</ul>

			<div class="start_a_project">
				<a href="<?= Url::toRoute(['campus/createpost'])?>"> Post a new job</a>
			</div>

		</div>
	</div>

	<div id="header">
		<!-- start main header -->
		<div class="find_a_job">
			<span style="cursor: pointer" onclick="openNav()"> <img
				src="<?=$imageurl;?>images/find-icon.png"></span>
		</div>
		<div class="container">
			<!-- container -->
			<div class="row">
				<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav float-left">
								<li><a href="<?= Url::toRoute(['campus/dashboard'])?>">Dashboard</a></li>
								<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB wall
										Post</a></li>
								<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Search
										Company Post</a></li>
								<li><a href="<?= Url::toRoute(['campus/appliedcompany'])?>">Applied Company</a></li>
								<li><a href="<?= Url::toRoute(['campus/appliedjob'])?>"> Applied
										Job Post<span class="total"><?=Yii::$app->session['NoofjobAppliedc'];?></span>
								</a></li>
								<li><a href="<?= Url::toRoute(['campus/yourpost'])?>">My Post</a></li>
								<li><a href="<?= Url::toRoute(['campus/createpost'])?>">Create
										Campus Post</a></li>
								<li><a href="<?= Url::toRoute(['campus/studentlist'])?>">Student
										List</a></li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->

				<div class="clearfix"></div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- end main header -->
   <?php
    } elseif (isset(Yii::$app->session['Teamid'])) {
        ?>
   <div id="head">
		<div class="container">
			<div class="row">
				<!-- logo -->
				<div class="col-lg-4  col-md-4 col-sm-5 col-xs-6  no_pad top-main">
					<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs"
						rel="home"> <img class="main-logo"
						src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
					</a>
				</div>
				<!-- logo -->

				<!--mobile -->
				<div class="col-xs-6 no_pad  rt_flt">
					<div class="small-23   ds">
						<!-- user -->
						<div class="dropdown">
							<button class="btn btn-primary  after_login_image"
								onclick="openNav12()" type="button">
								<img src="<?=Yii::$app->session['TeamDP'];?>"
									style="width: 25px; height: 25px;" alt=""
									class="after_login_profile_photo">
							</button>

							</ul>
						</div>
					</div>



					<!-- Notification -->
					<div class="small-23  ds">
						<div class="dropdown">
							<button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-bell mbl"></span>
							</button>

							<ul class="dropdown-menu" id="ntfctn">
								<!--<li class=""><a target="_blank" href="#"><span class="notiLabel">Job Recommendations</span> <span class="notiCount">08</span> <p><span class="noti_Description fullWidth">HR Manager Position at Innofied Solution, Kolkata</span><span class="status"></span></p></a></li> 
														 <li class=""><a target="_blank" href="#"><span class="notiLabel">Job Recommendations</span> <span class="notiCount">08</span> <p><span class="noti_Description fullWidth">HR Manager Position at Innofied Solution, Kolkata</span><span class="status"></span></p></a></li> -->

							</ul>
						</div>
					</div>

					<div class="small-23  ds">
						<!-- Message -------------------------------->
						<div class="dropdown">
									<?php
        $messagelist = $mess->getMessage(Yii::$app->session['Teamid']);
        $messagelist1 = $mess->getMessagecount(Yii::$app->session['Teamid']);
        $msgcnt = count($messagelist1);

        ?>
									 <button class="btn btn-primary dropdown-toggle" type="button"
								data-toggle="dropdown">
								<span class="fa fa-envelope mbl"></span>
							</button>
							<ul class="dropdown-menu">
								   <?php
        if ($messagelist) {
            $mc = 0;
            foreach ($messagelist as $mk => $mval) {
                if ($mc < 3) {
                    ?>
									 <li class=""><a
									href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
										<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
											style="width: 43px; height: 43px;" /></span> <span
										class="notiLabel"><?=$mval['Name'];?></span>
										<p>
											<span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span
												class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
										</p>
								</a></li>
									 <?php
                }
                $mc ++;
            }
            ?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
										class="notiLabel">View All Message</span>
										<p>
											<span class="status"></span>
										</p></a></li>
									<?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
        }
        ?>
									 </ul>

						</div>
					</div>
                              <div class="small-23  ds home_3"> 
            						<div class="dropdown"> 
            							<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"><span class="fa fa-home mbl"></span></a>	 
            							</div>
								</div>
								
								
				</div>
				<!--mobile -->


				<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav">

								<!-- It will display when customer paid  for that -->
								<li class="no-need"><a
									href="<?= Url::toRoute(['site/jobbycompany'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-file-text orange"></b> <br> Company List
								</a></li>
								<!-- It will display when customer paid  for that -->



								<!-- It will display when customer paid  for that -->
								<li class="no-need"><a href="#"
									class="dropdown-toggle  orange_bg new_style"
									data-toggle="dropdown"> <b class="fa fa-envelope orange"></b> <br>Message
										<span class="total total-message"><?=$msgcnt;?></span></a>

									<ul class="dropdown-menu">
								   <?php
        if ($messagelist) {
            $mc = 0;
            foreach ($messagelist as $mk => $mval) {
                if ($mc < 3) {
                    ?>
									 <li class=""><a
											href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
												<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
													style="width: 43px; height: 43px;" /></span> <span
												class="notiLabel"><?=$mval['Name'];?></span>
												<p>
													<span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span
														class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
												</p>
										</a></li>
									 <?php
                }
                $mc ++;
            }
            ?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
												class="notiLabel">View All Message</span>
												<p>
													<span class="status"></span>
												</p></a></li>
									<?php
        } else {
            ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
        }
        ?>
									 </ul></li>
								<!-- It will display when customer paid  for that -->

								<li class="no-need"><a href="#"
									class="dropdown-toggle brdr  orange_bg new_style"
									data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>
										
										 <span class="total">0</span></a>
									<ul class="dropdown-menu">
										<li>No Notification</li>
									</ul></li>

								<li class="no-need prfl_img"><a href="javascript:void(0)"
									class="brdr orange_bg new_style" onclick="openNav12()"> <img
										style="width: 43px; height: 43px;"
										src="<?=Yii::$app->session['TeamDP'];?>" alt=""
										class="img-responsive center-block "><!--<?php echo Yii::$app->session['TeamName']; ?> --> </a>
									<ul class="dropdown-menu">
										<li class=""><a
											href="<?= Url::toRoute(['team/teamprofile'])?>"> <b
												class="fa fa-user"></b> My Profile
										</a></li>
										<li class=""><a
											href="<?= Url::toRoute(['team/teameditprofile'])?>"><b
												class="fa fa-pencil-square-o"></b> Edit Profile</a></li>
										<li class=""><a href="<?= Url::toRoute(['team/changepwd'])?>">
												<b class="fa fa-lock"></b> Change Password
										</a></li>
										<li class=""><a href="<?= Url::toRoute(['team/teamlogout'])?>">
												<b class="fa fa-power-off"></b> Log Out
										</a></li>
									</ul></li>
							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<!-- Main Navigation -->
		</div>
	</div>


	<div id="mySidenav12" class="sidenav12">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a>

		<div class="side_bar-main">
			<img style="width: 83px; height: 83px;"
				src="<?=Yii::$app->session['TeamDP'];?>" alt="" class="">
			<h2><?php echo Yii::$app->session['TeamName']; ?></h2>
			<hr class="height-2">

			<ul>

				<li class=""><a href="<?= Url::toRoute(['wall/teamprofile'])?>"> My
						Profile</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
						Wall Post</a></li>
				<li class=""><a href="<?= Url::toRoute(['/myactivity'])?>"> My Activities </a></li>
			
				<li class=""><a href="<?= Url::toRoute(['site/jobsearch'])?>"> Job
						Search </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/recentjob'])?>">
						Recent Jobs </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin
						Jobs </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['team/bookmarkedjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Teamid']);?></a></li>
				<li class=""><a href="<?= Url::toRoute(['team/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobAppliedt'];?>    </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['team/editprofile'])?>">
						Edit Profile</a></li>
				<li class=""><a href="<?= Url::toRoute(['team/changepwd'])?>">
						Change Password </a></li>
				<hr class="height-2">
			
			
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['team/teamlogout'])?>">
						Logout </a></li>
			</ul>
		</div>
	</div>

	<div id="header">
		<!-- start main header -->
		<div class="find_a_job">
			<span style="cursor: pointer" onclick="openNav()"> <img
				src="<?=$imageurl;?>images/find-icon.png"></span>
		</div>
		<div class="container">
			<!-- container -->
			<div class="row">
				<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="navbar navbar-default " role="navigation">
						<div class="collapse navbar-collapse">
							<ul class="nav navbar-nav float-left">
								<li><a href="<?= Url::toRoute(['team/dashboard'])?>">Dashboard</a></li>
								<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB Wall
										Post</a></li>
								<li><a href="<?= Url::toRoute(['/site/teamjobsearch'])?>">Team Jobs</a></li>
								<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
								<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent Jobs</a></li>
								<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin Jobs </a></li>
								<li><a href="<?= Url::toRoute(['team/appliedjob'])?>"> Applied
										Jobs <span class="total"><?=Yii::$app->session['NoofjobAppliedt'];?></span>
								</a></li>
								<li><a href="<?= Url::toRoute(['team/bookmarkedjob'])?>">
										Bookmark Jobs <span class="total"><?=$apl->getTotalbookmark(Yii::$app->session['Teamid']);?></span>
								</a></li>

							</ul>
						</div>
						<!--/.nav-collapse -->
					</div>
					<div class="clear"></div>
				</div>
				<!-- Main Navigation -->

				<div class="clearfix"></div>
			</div>
		</div>
		<!-- container -->
	</div>
	<!-- end main header -->
   <?php
    }
}
?>
  
 <!---------------------------------------------- Header End ---------------------------------------------->




	<!---------------------- Container Start ------------------------>
<?php
if (Yii::$app->session->hasFlash('error') || Yii::$app->session->hasFlash('success')) {
    if (Yii::$app->session->hasFlash('error')) {
        $flashimage = $imageurl . "images/error.png";
    } elseif (Yii::$app->session->hasFlash('success')) {
        $flashimage = $imageurl . "images/success.png";
    }
    $pagearr = array(
        'login',
        'campusregister',
        'jobseekersregister',
        'employersregister',
        'campuszone'
    );
    ?>
		  <div class="modal add-resume-modal" style="display: block;"
		id="alertbox" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="input-group image-preview form-group "
						style="width: 100%; height: auto; text-align: center;">
						<img src="<?=$flashimage;?>" style="width: 70px;" />
                            <?= Alert::widget() ?>
							<input type="button" value="OK" aria-hidden="true"
							data-dismiss="alert" class="btn btn-lg btn-primary btn-block"
							style="width: 100px; margin: auto;"
							onclick="$('#alertbox').hide();$('.modal-backdrop').hide();<?php if(!in_array(Yii::$app->controller->action->id,$pagearr)){?>location.reload();<?php }?>" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-backdrop  in"></div>
<?php
}
?>
<?php echo Loader::widget()?>
        <?= $content ?>
<!---------------------- Container End ------------------------>


	<!---------------------- Footer Start ------------------------>

	<div id="footer">
		<!-- Footer -->
		<div class="container">
			<!-- Container -->
			<div class="row">
				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- Text Widget -->
					<?php
					$aboutus = common\models\FooterAboutus::find()->one();
    ?>
						<h6 class="widget-title"><?=$aboutus->Heading;?></h6>
					<div class="textwidget">
							<?=htmlspecialchars_decode($aboutus->Content);?>
						</div>
				</div>
				<!-- Text Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12  footer-widget">
					<!-- Footer Menu Widget -->
					<h6 class="widget-title">Hot Categories</h6>
					<div class="footer-widget-nav">
						<ul>
								 <?php
        $htcat = $indu->getHotcategory();
        if ($htcat) {
            foreach ($htcat as $hk => $hval) {
                $string=Yii::$app->myfunctions->clean(trim($hval->IndustryName));
                ?>
								<li><a
								href="<?= Url::toRoute(['site/jobsearch/'.$string])?>"><?=$hval->IndustryName;?></a></li>
							  <?php
            }
        }
        ?>
							</ul>
					</div>
				</div>
				<!-- Footer Menu Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- Recent Tweet Widget -->
						<?php
						$thirdblock = common\models\FooterThirdBlock::find()->one();
    ?>
						<h6 class="widget-title"><?=$thirdblock->Heading;?></h6>
					<div class="footer-widget-nav">
						<ul>
							<li><a href="<?= Url::toRoute(['site/index']);?>">Home</a></li>
					 
						 	<li><a href="<?= Url::toRoute(['site/walkin']);?>"> Walkin Jobs</a></li>
									<li><a href="<?= Url::toRoute(['site/recentjob']);?>"> Recent Jobs</a></li>	
									
									
							<li><a href="<?= Url::toRoute(['site/login']);?>">Post a Resume</a></li>
					 
							<li><a href="<?= Url::toRoute(['content/privacy']);?>">Privacy
									Policy</a></li>
							<li><a href="<?= Url::toRoute(['content/terms']);?>">Terms &
									Condition</a></li>
							<li><a href="<?= Url::toRoute(['content/faq']);?>">Faq</a></li>
							<li><a href="<?= Url::toRoute(['site/blog']);?>" target="_blank">Blog</a></li>
						</ul>
					</div>
				</div>
				<!-- Recent Tweet Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- News Leter Widget -->
					<div id="contact_details">
					<?php
					$contactus = common\models\FooterContactus::find()->one();
    ?>
					  <h6 class="widget-title"><?=$contactus->Heading;?></h6> 
					     <?=htmlspecialchars_decode($contactus->Content);?>
					  </div>
					<h6 class="widget-title sig1">Singin For news Letter</h6>

					<div class="single-widget">
						<input autocomplete="off" type="email" id="news_email" type="text"
							class="col-xs-15 subscribeInputBox"
							placeholder="Your email address">
						<button
							class="col-xs-7 btn btn-theme-secondary subscribeBtn reset-padding"
							onclick="newsletter();">Subscribe</button>
						<div class="clearfix"></div>
					</div>

					<p>Register now to get updates on latest Vacancy.</p>

				</div>
				<!-- News Leter Widget -->
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div id="footer-btm">
		<!-- Footer -->
		<div class="container">
			<!-- Container -->
			<div class="row">
					 <?php
					 $socialicon = common\models\SocialIcon::find()->one();
    ?>
					   <ul class="list-inline social-buttons">
						 <?php
    if ($socialicon->IsFacebook == 1)  {
        ?>
                        <li><a href="<?=$socialicon->FacebookLink;?>"
						target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php
    }
    if ($socialicon->IsTwitter == 1) {
        ?>
                        <li><a href="<?=$socialicon->TwitterLink;?>"
						target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php
    }
    if ($socialicon->IsLinkedin == 1) {
        ?>
                        <li><a href="<?=$socialicon->LinkedinLink;?>"
						target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php
    }
    if ($socialicon->IsGoogleplus == 1) {
        ?>
                        <li><a href="<?=$socialicon->GoogleplusLink;?>"
						target="_blank"><i class="fa fa-instagram"></i></a></li>
						<?php
    }
    ?>
                    </ul>

			</div>
		</div>
		<!-- Container -->
	</div>




	<div class="developed_by_e-developed_technology">
		<div class="row">
			<div class="col-md-6">
			   <?php
	$copyright = common\models\FooterCopyright::find()->one();
    echo htmlspecialchars_decode($copyright->Content);
    ?>
					
			   </div>
			<div class="col-md-6">
			   <?php
	$dblock = common\models\FooterDevelopedblock::find()->one();
    echo htmlspecialchars_decode($dblock->Content);
    ?>
					
			   </div>
		</div>
	</div>

	</div>
	<!-- Container -->
	</div>
	<!-- Footer -->

	<!---------------------- Footer End ------------------------->
<?php $this->endBody() ?>
<!-- Menu Script -->
	<script>
	   function openNav12() {
		document.getElementById("mySidenav12").style.width = "250px";
	   } 
	function closeNav12() {
		document.getElementById("mySidenav12").style.width = "0";
	}
	
	<?php
if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Campusid']) || isset(Yii::$app->session['Teamid'])) {?>
	
	$(document).ready(function(){
		
		setInterval(function(){ 
			$.ajax({url:mainurl+"site/getmessagecount",dataType:'HTML',
               success:function(result)
            	{
					$('.total-message').html(result);
            	}
        	});
		}, 5000);
	});
	
<?php }?>	
	$(function(){
	  var searchresult='';
	  <?php
if (Yii::$app->controller->action->id == 'jobsearch') {
    if (isset($_GET['indexsearch']) && $_GET['indexsearch'] == 1) {
        if (isset($_GET['keyname']) && $_GET['keyname'] != '') {
            ?>
		  var keyname='<?=$_GET['keyname'];?>';
		  searchresult+=' > '+keyname;
		  <?php
        }
        if (isset($_GET['indexlocation']) && $_GET['indexlocation'] != '') {
            ?>
		  var indexlocation='<?=$_GET['indexlocation'];?>';
		  searchresult+=' > '+indexlocation;
		  <?php
        }
        if (isset($_GET['experience']) && $_GET['experience'] != '') {
            ?>
		  var experience='<?=$_GET['experience'];?>';
		  searchresult+=' > '+experience+' Year';
		  <?php
        }
        if (isset($_GET['salary']) && $_GET['salary'] != '') {
            ?>
		  var salary='<?=$_GET['salary'];?>';
		  searchresult+=' > '+salary+' Lakh';
		  <?php
        }
    } else if (isset($_GET['alljobsearch']) && $_GET['alljobsearch'] == 1) {
        if (isset($_GET['keyword']) && $_GET['keyword'] != '') {
            ?>
		  var popular='<?=$_GET['ctname'];?>';
		  searchresult+=' > '+popular;
		  <?php
        }
    } else {
        if (isset($_GET['role']) && $_GET['role'] != '') {
            ?>
		  var role=$('#position option:selected').text();
		  searchresult+=' > '+role;
		  <?php
        }
        if (isset($_GET['state']) && $_GET['state'] != '') {
            ?>
		  var state='<?=$_GET['state'];?>';searchresult+=' > '+state;
		 setTimeout(function(){ $('#state').val('<?=$_GET['state'];?>');},2000);
		  <?php
        }
        if (isset($_GET['salaryrange']) && $_GET['salaryrange'] != '') {
            ?>
		  var salaryrange='<?=$_GET['salaryrange'];?> lakh';
		  searchresult+=' > '+salaryrange;
		  <?php
        }
        if (isset($_GET['jobtype']) && $_GET['jobtype'] != '') {
            ?>
		  var jobtype='<?=$_GET['jobtype'];?>';
		  searchresult+=' > '+jobtype;
		  <?php
        }
        if (isset($_GET['skillby']) && $_GET['skillby'] != '') {
            ?>
		  var skill = $('#collapseSix input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+skill;
		  <?php
        }
        if (isset($_GET['companyby']) && $_GET['companyby'] != '') {
            ?>
		  var companyby = $('#collapseSeven input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+companyby;
		  <?php
        }
        if (isset($_GET['latest']) && $_GET['latest'] != '') {
            ?>
		  var latest =$('#latest option:selected').text();
		  searchresult+=' > '+latest;
		  <?php
        }
        if (isset($_GET['JobCategoryId']) && $_GET['JobCategoryId'] != '') {
            ?>
		  var latest =$('#industry option:selected').text();
		  searchresult+=' > '+latest;
		  <?php
        }
        if (isset($_GET['experience']) && $_GET['experience'] != '') {
            ?>
		  var experience=$('#collapsenine input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+experience;
		  <?php
        }
        if (isset($_GET['expno']) && $_GET['expno'] != '') {
            ?>
		  var experience='<?=$_GET['expno'];?>';
		  searchresult+=' > '+experience+' Year';
		  <?php
        }
        if (isset($_GET['education']) && $_GET['education'] != '') {
            ?>
			var education=$('#educollapseSix option:selected').text();
			searchresult+=' > '+education;
			<?php
        }
    }
    ?>
		 if(!$('#'+localStorage.jobsearch).hasClass('in'))
		 {
		 $('a[href="#'+localStorage.jobsearch+'"]').click();
		 }
		 <?php
}
?>
		
		
		setTimeout(function(){
		 if(searchresult!='')
		 {
		 $('#searchdiv').html('Search '+searchresult);
		 }
		 },1000);
		 
		 
		 
		 
		 var csearchresult='';
	  <?php
$stype = '';
if (Yii::$app->controller->action->id == 'hirecandidate') {

    if (isset($_GET['state']) && $_GET['state'] != '') {
        if ($_GET['stype'] == 'fresher') {
            ?>
		  setTimeout(function(){ $('#fstate').val('<?=$_GET['state'];?>');var state='<?=$_GET['state'];?>';searchresult+=' > '+state;},2000);
		  <?php
        } else {
            ?>
		 setTimeout(function(){ $('#state').val('<?=$_GET['state'];?>');var state='<?=$_GET['state'];?>';searchresult+=' > '+state;},2000);
		  <?php
        }
    }
    if (isset($_GET['education']) && $_GET['education'] != '') {
        if ($_GET['stype'] == 'fresher') {
            ?>
			var education=$('#fceducation option:selected').text();
			searchresult+=' > '+education;
			<?php
        } else {
            ?>
			var education=$('#ceducation option:selected').text();
			searchresult+=' > '+education;
			<?php
        }
    }

    if (isset($_GET['latest']) && $_GET['latest'] != '') {
        if ($_GET['stype'] == 'fresher') {
            ?>
			var fcfreshness=$('#fcfreshness option:selected').text();
			searchresult+=' > '+fcfreshness;
			<?php
        } else {
            ?>
			var fcfreshness=$('#cfreshness option:selected').text();
			searchresult+=' > '+fcfreshness;
			<?php
        }
    }

    if (isset($_GET['gender']) && $_GET['gender'] != '') {
        if ($_GET['stype'] == 'fresher') {
            ?>
			var gender=$('#collapseTwo input:checkbox:checked').map(function() {
			  return $(this).val().split().join();
		  }).get();
			searchresult+=' > '+gender;
			<?php
        } else {
            ?>
			var gender=$('#expcollapseTwoo input:checkbox:checked').map(function() {
			  return $(this).val().split().join();
		  }).get();
			searchresult+=' > '+gender;
			<?php
        }
    }

    if (isset($_GET['cresume']) && $_GET['cresume'] != '') {
        ?>
			searchresult+=' > with resume';
			<?php
    }

    if (isset($_GET['salaryrange']) && $_GET['salaryrange'] != '') {
        ?>
		  var salaryrange='<?=$_GET['salaryrange'];?> lakh';
		  searchresult+=' > '+salaryrange;
		  <?php
    }
    if (isset($_GET['experience']) && $_GET['experience'] != '') {
        ?>
		  var jobtype='<?=$_GET['experience'];?> Years';
		  searchresult+=' > '+jobtype;
		  <?php
    }
    if (isset($_GET['role']) && $_GET['role'] != '') {
        ?>
		  var jobtype=$('#crole option:selected').text();
		  searchresult+=' > '+jobtype;
		  <?php
    }
    if (isset($_GET['skill']) && $_GET['skill'] != '') {
        ?>
		  var skill = $('#expcollapseSeven input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+skill;
		  <?php
    }
    if (isset($_GET['industry']) && $_GET['industry'] != '') {
        ?>
		  var industry = $('#expcollapseSix input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+industry;
		  <?php
    }
    if ($_GET['stype'] == 'fresher') {
        $stype = 'Fresher';
    } else {
        $stype = 'Experience';
    }

    ?>
		  if(!$('#'+localStorage.candidate).hasClass('in'))
		 {
		 $('a[href="#'+localStorage.candidate+'"]').click();
		 }
		  <?php
}
?>
		
		setTimeout(function(){
		 if(searchresult!='')
		 {
		 $('#searchdivc').html('<?=$stype;?> '+searchresult);
		 }
		 },2500);
		
		<?php
if (Yii::$app->controller->action->id == 'campuspost') {
    if (isset($_GET['state']) && $_GET['state'] != '') {
        ?>
		  var state='<?=$_GET['state'];?>';
		 setTimeout(function(){$('#state').val('<?=$_GET['state'];?>');},2000);
		 searchresult+=' > '+state;
		  <?php
    }
}
?>
		setTimeout(function(){
		 if(searchresult!='')
		 {
		 $('#searchdivcampus').html('Search '+searchresult);
		 }
		 },1000);
		
		<?php
if (Yii::$app->controller->action->id == 'findteam') {
    if (isset($_GET['state']) && $_GET['state'] != '') {
        ?>
		  var state='<?=$_GET['state'];?>';
		 setTimeout(function(){$('#state').val('<?=$_GET['state'];?>');},2000);
		 searchresult+=' > '+state;
		  <?php
    }
    if (isset($_GET['salaryrange']) && $_GET['salaryrange'] != '') {
        ?>
		  var salaryrange='<?=$_GET['salaryrange'];?> lakh';
		  searchresult+=' > '+salaryrange;
		  <?php
    }
    if (isset($_GET['skill']) && $_GET['skill'] != '') {
        ?>
		  var skill = $('#collapseSeven input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+skill;
		  <?php
    }
    if (isset($_GET['industry']) && $_GET['industry'] != '') {
        ?>
		  var industry = $('#collapseSix input:checkbox:checked').map(function() {
			  return $(this).next().next().html().split().join();
		  }).get();
		  searchresult+=' > '+industry;
		  <?php
    }
    ?>
		   if(!$('#'+localStorage.team).hasClass('in'))
		 {
		 $('a[href="#'+localStorage.team+'"]').click();
		 }
		 <?php
}
?>
		setTimeout(function(){
		 if(searchresult!='')
		 {
		 $('#searchdivteam').html('Search '+searchresult);
		 }
		 },1000);
		 
	<?php
if (Yii::$app->controller->action->id == 'allstudent') {
    if (isset($_GET['gender']) && $_GET['gender'] != '') {
        ?>
		  var gender='<?=$_GET['gender'];?>';
		 searchresult+=' > '+gender;
		  <?php
    }
    if (isset($_GET['experience']) && $_GET['experience'] != '') {
        ?>
		  var experience=$('#camcexperience').val();
		  searchresult+=' > '+experience+' Years';
		  <?php
    }
    if (isset($_GET['cresume']) && $_GET['cresume'] != '') {
        ?>
			searchresult+=' > with resume';
			<?php
    }
    ?>
		   if(!$('#'+localStorage.campusstudent).hasClass('in'))
		 {
		 $('a[href="#'+localStorage.campusstudent+'"]').click();
		 }
		 <?php
}
?>
		setTimeout(function(){
		 if(searchresult!='')
		 {
		 $('#searchdivcam').html('Search '+searchresult);
		 }
		 },1000);
		
		var urll=window.location.href;
		urll=urll.split('#');

		if(urll[1]=='jobalertbox')
		{
		 $('#myModal_jobalert').modal('show');
		}
		
		
	});
	
	</script>


	<!-- Menu Script -->
</body>
</html>
<?php $this->endPage() ?>