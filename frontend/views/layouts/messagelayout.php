<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppmAsset;
use common\widgets\Alert;
use common\models\Messages;
use common\models\AppliedJob;
use common\models\Industry;
$mess=new Messages();
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
$apl=new AppliedJob();
$indu=new Industry();

use common\models\Notification;
use common\components\loader\Loader;
$notification=new Notification();
$campusnotification=$notification->getcampusnot();

AppmAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <style>.footer-widget-nav li{text-align:left;}#contact_details p{text-align:left;}</style>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?=$imageurl;?>images/icons/favicon.png"/>
	
	 <?php
 if(isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Campusid']) || isset(Yii::$app->session['Teamid']))
 {
  ?>
  <link href="<?=$imageurl;?>css/after_login_css.css" rel="stylesheet">
  <?php
 }
 ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--------------------------------------------------------------------------------------------- 
----------------------------------------- Start Header  ---------------------------------------
---------------------------------------------------------------------------------------------->
<a href="<?= Url::toRoute(['wall/index'])?>" class="wall" ><img src="<?=$imageurl;?>images/career_wall.png">   </a>
<!--Sidebar_contact--> 
	<div class="need_help">
		 <span style="cursor:pointer" onclick="openNav2()"> <img src="<?=$imageurl;?>images/need_help.png"></span> 
		  
			<div class="contact_form" id="right_side_form">
			   <h2 class="banner_heading">How we can <small> Help you ? </small>     </h2>  
			   <div class="closed">
			      <a href="javascript:void(0)"><i class="fa fa-times" aria-hidden="true"></i></a>
			   </div> 
				<input type="text" class="form-control" name="name" id="contact_name" placeholder="Name">  
				<input type="text" class="form-control" name="name" id="contact_number" onblur="contactvlidation(this.value);" placeholder="Contact Number" maxlength="10" onkeypress="return numbersonly(event)"> 
				<input type="text" class="form-control" name="contact_email" id="contact_email" placeholder="Email Id" onblur="return checkemail();"> 
				  <select id="form-filter-location" data-minimum-results-for-search="Infinity" class="contact_from form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
					<option value="1">Select  </option>
					<option value="1">Fresher  </option>
					<option value="1">Experience  </option>
					<option value="2">Company  </option>
					<option value="3">Consultancy </option>
					<option value="4"> Collage / Institute  </option>
					<option value="4">  Others  </option> 
				  </select> 
		         <textarea class="form-control textarea1" id="contact_message" placeholder="Message"></textarea>
				 <button class="btn btn-lg btn-primary btn-block" type="button" onclick="contactfrom();">Send</button>
			  </div>  
	 </div> 
 <!--Sidebar_contact-->
		 
  <!--top_nav--> 
<div id="mySidenav1" class="top_nav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav1()">&times;</a> 
		  <!-- Main Navigation --> 
		   <div class="navbar navbar-default " role="navigation">  
		   <div class="collapse navbar-collapse" style="display:block"> 
		        <img class="responsive_logo" src="<?=$imageurl;?>images/responsive_logo.jpg">
			      <ul class="nav navbar-nav"> 
						<li><a  href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
						<li><a  href="<?= Url::toRoute(['site/hirecandidate'])?>">   Hire Candidate</a></li>  
						<li><a  href="<?= Url::toRoute(['team/findteam'])?>">  Hire Team</a></li> 	
						<li><a  href="<?= Url::toRoute(['site/login'])?>">Post a Resume</a></li>    
						<li><a  href="<?= Url::toRoute(['campus/jobbycompany'])?>">   Job By company  </a></li>
						<li><a  href="">   Companies Walk-In-Inerview   </a></li>
						<li><a  href="<?= Url::toRoute(['site/login'])?>"  >    Upload CV </a></li>
						<li><a  href="<?= Url::toRoute(['site/login'])?>">    Campus Login</a></li>  
						<li><a  href="<?= Url::toRoute(['site/login'])?>">    Employer Login</a></li>  
						<li><a  href="<?= Url::toRoute(['site/login'])?>"  >   Login</a></li> 
		        <div class="clearfix"></div> 
		      </ul>
		     
				 	  
			<?php
			$socialicon = common\models\SocialIcon::find()->one();
					 ?>
					   <ul class="list-inline social-buttons">
						 <?php
						 if($socialicon->IsTwitter==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->TwitterLink;?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php
						 }
						 if($socialicon->IsFacebook==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->FacebookLink;?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php
						 }
						 if($socialicon->IsLinkedin==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->LinkedinLink;?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php
						 }
						 if($socialicon->IsGoogleplus==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->GoogleplusLink;?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<?php
						 }
						 ?>
                    </ul>
	</div>	 
</div> 		 
</div>
<!--top_nav-->
  
  
<!--sidebar search--> 
<div id="mySidenav" class="sidenav">
   <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		  <div class="group-sm group-top" id="top_side_fixed">
					 <h2 class="banner_heading">Find a <span>Job </span> You Will <span>  Love </span> </h2>
					 <div class="spacer1"></div>
					 <?php $form = ActiveForm::begin(['action' => Url::toRoute(['site/index']), 'options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form']]); ?>
					 <div  class="group-item col-md-12 col-xs-12">
						<div class="form-group"> 
						<input type="text" class="form-control" name="keyname" id="keyname1"  placeholder="Job title, skills, or company"> 
						</div>
					  </div> 
					  <div  class="group-item col-md-12 col-xs-12">
						<div class="form-group"> 
						<input type="text" class="form-control" name="indexlocation" id="indexlocation1" placeholder="Location"> 
						</div>
					  </div> 
					  <div   class="group-item col-md-12 col-xs-6">
						<div class="form-group">
						  <select id="form-filter-location" name="experience" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
							<option value="">Experience</option>
                               <option value="Fresher">  Fresher</option>
					    <option value="0-1">  Below 1 Year</option>
					    <?php
					    for($fr=1;$fr<=30;$fr=$fr+1)
					    {
							$frn=$fr+1;
					?>
                                            <option value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                           <?php
					    }
					    ?>
						  </select> 
						</div>
					  </div>
					  
					  <div  class="group-item col-md-12 col-xs-6">
						<div class="form-group">
						  <select id="form-filter-location" name="salary" id="salary" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                <option value="">Salary</option>
                                <option value="0 - 1.5"> 0 - 1.5 Lakh  </option>
                                <option value="1.5 - 3"> 1.5 - 3 Lakh </option>
                                <option value="3 - 6"> 3 - 6 Lakh  </option>
								<option value="6 - 10"> 6 - 10 Lakh   </option>
								<option value="10 - 15"> 10 - 15 Lakh   </option>
								
								<option value="15 - 25"> 15 - 25 Lakh  </option>
								<option value="Above 25">Above 25 Lakh</option>
                              </select> 
						</div>
					  </div>
					   
					  <div class=" group-item reveal-block reveal-lg-inline-block col-md-12 col-xs-12">
						<?= Html::submitButton('Search', ['name'=>'indexsearch','class' => 'btn btn-primary element-fullwidth']) ?>
					  </div>
					  <?php ActiveForm::end(); ?>
            </div>   
	   </div> 
  <!--sidebar search-->
<!-------------------------------------  before login-------------------------->

<?php
if(!isset(Yii::$app->session['Employeeid']) && !isset(Yii::$app->session['Employerid']) && !isset(Yii::$app->session['Campusid']) && !isset(Yii::$app->session['Teamid']))
{
 ?>
 <!-- Head Top--> 
<div id="head">
  <div class="container">
			  <div class="row">  
			          <!--mobile -->
						<div class="col-xs-2 col-sm-1  menu_humb">
							<div class="  _login">
								 <a href="#"  onclick="openNav1()">	<span class="fa fa-bars"></span></a>
								</div>
							 
						</div>		
						<!--mobile -->	 
					<div class="col-lg-4  col-md-4 col-sm-6 col-xs-7 top-main"><!-- logo -->
						<a  href="<?= Url::toRoute(['site/index'])?>"  title="My Career Bugs" rel="home">
							<img class="main-logo" src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs" />
						</a>
					</div><!-- logo --> 
					<div class="col-lg-8  col-md-8 col-sm-6 col-xs- 2 main-nav"><!-- Main Navigation -->
					
                       <div class="head_right"> 
						       <ul>  
								   <li class=""> <strong> Are you recruiting?</strong> <a  href="<?= Url::toRoute(['content/help'])?>"> 
									  <span>How we can help</span>  </a></li>  
								   <li><a class="btn-123"  href="<?= Url::toRoute(['site/employersregister'])?>"   >Employer Zone</a></li>
						       </ul>
                      </div><!--/.nav-collapse -->
					
					</div>  
		    <div class="col-xs-3  col-sm-2  ds"> 
			    <div class="dropdown _login">
				 <a  href="<?= Url::toRoute(['site/login'])?>" >	<span class="fa fa-user"></span></a>
				</div>	
		    </div> 	
			<div class="clear"></div>
		</div><!-- Main Navigation --> 
  </div>
</div> 
  <!-- Head -->   

  
<!-- Start main header Menu -->
 <div id="header">
		 <div class="find_a_job">
		     <span style="cursor:pointer" onclick="openNav()"> <img src="<?=$imageurl;?>images/find-icon.png"></span> 
         </div>
			<div class="container"><!-- container -->
			  <div class="row"> 
					  <div class="col-lg-7  col-md-7 col-sm-7 col-xs-12 main-nav"><!-- Main Navigation --> 
					   <div class="navbar navbar-default " role="navigation"> 
                       <div class="collapse navbar-collapse"> 
						<ul class="nav navbar-nav"> 
								<li><a   href="<?= Url::toRoute(['site/jobsearch'])?>" >Job Search</a></li>
								<li><a   href="<?= Url::toRoute(['site/hirecandidate'])?>" >   Hire Candidate</a></li>
								<li><a  href="<?= Url::toRoute(['team/findteam'])?>">  Hire Team</a></li>
								<li><a   href="<?= Url::toRoute(['site/jobseekersregister'])?>" >Post a Resume</a></li>
								<li><a  href="<?= Url::toRoute(['campus/jobbycompany'])?>">   Job by Company</a></li>
								
						</ul>
                       </div><!--/.nav-collapse -->  
                      </div>  
						<div  class="clear"></div>
					</div><!-- Main Navigation -->
					
					<div class="col-lg-5  col-md-5 col-sm-5 col-xs-12 main-nav"><!-- Main Navigation --> 
					   <div class="navbar navbar-default " role="navigation"> 
                        <div class="collapse navbar-collapse no_pad"> 
						 <ul class="nav navbar-nav right_algn"> 
						      <li><a   href="<?= Url::toRoute(['team/index'])?>" > <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp  Team</a></li>
									 <li><a   href="<?= Url::toRoute(['campus/campuszone'])?>" > <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp  Campus Zone</a></li>  
								 <li><a   href="<?= Url::toRoute(['site/register'])?>" > <i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp  Sign up</a></li>  
								<li><a   href="<?= Url::toRoute(['site/login'])?>"  class="log" > <i class="fa fa-sign-in" aria-hidden="true"></i>  &nbsp Login</a></li>
						</ul>
                    </div><!--/.nav-collapse --> 
                  </div> 
						<div  class="clear"></div>
					</div><!-- Main Navigation -->
					<div class="clearfix"></div>
				</div>
				 
			</div><!-- container --> 
	</div>
<!-- end main header -->
<?php
}
else
{
if(isset(Yii::$app->session['Employeeid']))
  {
  ?>
  <!-- Head  Start -->    
 <div id="head">
  <div class="container">
			  <div class="row">  
			    <!-- logo --> 
					<div class="col-lg-4  col-md-4 col-sm-5 col-xs-7  no_pad top-main">
						<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs" rel="home">
							<img class="main-logo" src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
						</a>
					</div>
				<!-- logo -->
	    <!--mobile -->
			<div class="col-xs-5 no_pad  rt_flt" >   
				 		      <div class="small-23   ds"> <!-- user -------------------------->
								   <div class="dropdown">
									 <button class="btn btn-primary  after_login_image"   onclick="openNav12()" type="button"  > 
										<img  src="<?=Yii::$app->session['EmployeeDP'];?>" style="width: 25px;height: 25px;" alt="" class="after_login_profile_photo">
									</button>    
											</ul>
										</div>
								</div>    
								 <div class="small-23  ds"> <!-- Notification --------------------->
								   <div class="dropdown">
									 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
										<span class="fa fa-bell mbl"></span></button> 
											 <?php
								$alnotification=[];
								if(isset(Yii::$app->view->params['employeenotification']))
								$alnotification=Yii::$app->view->params['employeenotification'];
								?>
								<ul class="dropdown-menu">
								   <?php
								   if($alnotification)
								   {
								   foreach($alnotification as $nk=>$nval)
								   {
									if($nk<3)
									{
								   ?>
								    <li class=""><a target="_blank" href="<?= Url::toRoute(['site/jobdetail','JobId'=>$nval->JobId,'Nid'=>$nval->NotificationId])?>">
									<span class="notiLabel"><img src="<?=($nval->job->employer->UserTypeId==2 || $nval->job->employer->UserTypeId==5)?(($nval->job->employer->PhotoId!=0)?$url.$nval->job->employer->photo->Doc:"$imageurl.images/user.png"):(($nval->job->employer->LogoId!=0)?$url.$nval->job->employer->logo->Doc:"$imageurl.images/user.png");?>" style="width: 43px;height: 43px;" /></span>
									<span class="notiLabel"><?=$nval->job->employer->Name;?></span> <span class="notiCount"><?=$nk+1;?></span> <p><span class="noti_Description fullWidth"><?=$nval->job->JobTitle;?> at <?=$nval->job->Location;?> , <?=$nval->job->City;?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($nval->OnDate));?></span></p></a></li>
									<?php
								   }
								   }
								   ?>
								   <li class=""><a href="<?= Url::toRoute(['site/candidatenotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Notification</span></a></li><li class=""><a href="<?= Url::toRoute(['site/candidatenotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   ?>
								</ul>
										</div>  
									</div>  
								<div class="small-23  ds"> <!-- Message -------------------------------->	
								   <div class="dropdown">
									<?php
									$messagelist=$mess->getMessage(Yii::$app->session['Employeeid']);
									$messagelist1=$mess->getMessagecount(Yii::$app->session['Employeeid']);
									$mlink=(count($messagelist1)==0)?Url::toRoute(['message/index']):'';
									?>
									 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
										<span class="fa fa-envelope-o mbl"></span>
									 </button>
								<ul class="dropdown-menu">
								   <?php
								   if($messagelist)
								   {
									$mc=0;
									foreach($messagelist as $mk=>$mval)
									{
									 if($mc<3)
									 {
									 ?>
									 <li class="">
									  <a href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									  <span class="notiLabel"><img src="<?=$mval['Photo'];?>" style="width: 43px;height: 43px;" /></span>
									  <span class="notiLabel"><?=$mval['Name'];?></span> <p><span class="noti_Description fullWidth">
									  <?php if($mval['IsView']){?>
											<?=$mval['Last'].$mval['Message'];?>
											<?php }else{?>
											<strong><?=$mval['Last'].$mval['Message'];?></strong>
											<?php }?>
									  </span><span class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span></p>
									  </a>
									 </li>
									 <?php
									 }
									 $mc++;
									}
									?>
									  <li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span class="notiLabel">View All Message</span></a></li>
									  <?php
								   }
								   else
								   {
									?>
									<li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
									<?php
								   }
								   ?>
							   </ul>

									</div>  
									
								</div>
									  
					</div>		
				<!--mobile -->	
				<!-- Main Navigation -->   				
					<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav">
					   <div class="navbar navbar-default " role="navigation"> 
                       <div class="collapse navbar-collapse"> 
				           <ul class="nav navbar-nav">   
							<li class="no-need"> 
							   <a href="<?= Url::toRoute(['campus/jobbycompany'])?>" class="dropdown-toggle  orange_bg new_style">  <b class="fa fa-file-text orange"></b> <br> Company List </a>
							</li>   		
							<li class="no-need">
							 <?php
							 $msgcount=count($messagelist1);
							 
							 ?>
							   <a href="#" class="dropdown-toggle brdr orange_bg new_style" data-toggle="dropdown">  <b class="fa fa-envelope-o orange"></b> <br>   Message  <span class="total"><?=$msgcount;?></span> </a>
							   <ul class="dropdown-menu">
								   <?php
								   if($messagelist)
								   {
									$mc=0;
									foreach($messagelist as $mk=>$mval)
									{
									 if($mc<3)
									 {
									 ?>
									 <li class="">
									  <a href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									  <span class="notiLabel"><img src="<?=$mval['Photo'];?>" style="width: 43px;height: 43px;" /></span>
									  <span class="notiLabel"><?=$mval['Name'];?></span> <p><span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span></p>
									  </a>
									 </li>
									 <?php
									 }
									 $mc++;
									}
									?>
									  <li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span class="notiLabel">View All Message</span></a></li>
									  <?php
								   }
								   else
								   {
									?>
									<li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
									<?php
								   }
								   ?>
							   </ul>
							   
							</li>   
						     <li class="no-need">
							  <?php
								$alnotification=[];
								if(isset(Yii::$app->view->params['employeenotification']))
								{
								$alnotification=Yii::$app->view->params['employeenotification'];
								$empnotcount=count(Yii::$app->view->params['employeenotification']);
								?>
								<a href="#" class="dropdown-toggle brdr  orange_bg new_style" data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>  Notification <span class="total"><?=$empnotcount;?></span></a> 
								
								<ul class="dropdown-menu">
								   <?php
								   if($alnotification)
								   {
								   foreach($alnotification as $nk=>$nval)
								   {
									if($nk<3)
									{
								   ?>
								    <li class=""><a target="_blank" href="<?= Url::toRoute(['site/jobdetail','JobId'=>$nval->JobId,'Nid'=>$nval->NotificationId])?>">
									<span class="notiLabel"><img src="<?=($nval->job->employer->UserTypeId==2 || $nval->job->employer->UserTypeId==5)?(($nval->job->employer->PhotoId!=0)?$url.$nval->job->employer->photo->Doc:"$imageurl.images/user.png"):(($nval->job->employer->LogoId!=0)?$url.$nval->job->employer->logo->Doc:"$imageurl.images/user.png");?>" style="width: 43px;height: 43px;" /></span>
									<span class="notiLabel"><?=$nval->job->employer->Name;?></span> <span class="notiCount"><?=$nk+1;?></span> <p><span class="noti_Description fullWidth"><?=$nval->job->JobTitle;?> at <?=$nval->job->Location;?> , <?=$nval->job->City;?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($nval->OnDate));?></span></p></a></li>
									<?php
								   }
								   }
								   ?>
								   <li class=""><a href="<?= Url::toRoute(['site/candidatenotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Notification</span></a></li>
								   <li class=""><a href="<?= Url::toRoute(['site/candidatenotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   ?>
								</ul>
								<?php
								}
								?>
							</li> 
							<li class="no-need prfl_img">
								<a href="javascript:void(0)" class="brdr orange_bg new_style"  onclick="openNav12()">    
   								<img style="width: 43px;height: 43px;" src="<?=Yii::$app->session['EmployeeDP'];?>"  alt="" class="img-responsive center-block "><?=Yii::$app->session['EmployeeName'];?></a>  
							</li> 
				      </ul> 
                </div><!--/.nav-collapse --> 
               </div> 
			 <div class="clear"></div>
		  </div> 
         <!-- Main Navigation -->   					
		<div class="clear"></div>
	</div> 
  </div>
 </div>
<!-- Head  End -->     

<!-- Right sidebar start Menu  -->  
 <div id="mySidenav12" class="sidenav12">
 <a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a> 
  <div class="side_bar-main">
     <img style="width: 80px;height: 80px;" src="<?=Yii::$app->session['EmployeeDP'];?>" alt="" class=""> 
	 <h2>  <?=Yii::$app->session['EmployeeName'];?>  </h2>
	   <hr class="height-2"> 
		<ul>

			<li class=""><a href="<?= Url::toRoute(['site/profilepage'])?>">  My Profile</a></li> 
			<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">  MCB Wall Post</a></li>  
			<li class=""><a href="<?= Url::toRoute(['wall/candidateprofile'])?>">  MCB Profile</a></li>  
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['site/editprofile'])?>">  Edit Profile</a></li>	
			<li class=""><a href="<?= Url::toRoute(['site/employeechangepassword'])?>">  Change Password  </a></li>			
			<hr class="height-2">
			<li class=""><a href="<?= Url::toRoute(['site/recentjob'])?>"> Recent Job </a></li>  
			<li class=""><a href="<?= Url::toRoute(['site/jobsearch'])?>">  Job Search    </a></li>  
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['site/bookmarkjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Employeeid']);?></a></li> 
			<li class=""><a href="<?= Url::toRoute(['site/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobApplied']; ?>    </a></li>
			<?php
			if(isset(Yii::$app->session['CampusStudent']))
			{
			 ?>
			<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>" >Company Job For Campus</a></li>
			<?php
			}
			?>
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['site/employeelogout'])?>">  Logout </a></li>  
		 </ul> 
	   
  </div>
 </div>   
<!-- Right sidebar end Menu  -->  	   

<!-- header start Menu  -->  
       <div id="header">
		   <div class="find_a_job">
				 <span style="cursor:pointer" onclick="openNav()"> <img src="<?=$imageurl;?>images/find-icon.png"></span> 
			 </div> 
					<div class="container"><!-- container -->
						<div class="row">  
							    <div class="col-lg-10  col-md-8 col-sm-8 col-xs-12 main-nav"><!-- Main Navigation --> 
					              <div class="navbar navbar-default " role="navigation"> 
								   <div class="collapse navbar-collapse"> 
									  <ul class="nav navbar-nav float-left">  
										<li><a href="<?= Url::toRoute(['site/userdashboard'])?>">Dashboard</a></li>  
										<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB Wall Post  </a></li>
										<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li> 
										<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent Jobs</a></li>
										<li> <a href="<?= Url::toRoute(['site/appliedjob'])?>"> Applied Jobs  <span class="total"><?=Yii::$app->session['NoofjobApplied']; ?></span>  </a></li>  
										<li><a href="<?= Url::toRoute(['site/bookmarkjob'])?>">  Bookmark Jobs <span class="total"><?=$apl->getTotalbookmark(Yii::$app->session['Employeeid']);?></span>     </a></li>
										<?php
			if(isset(Yii::$app->session['CampusStudent']))
			{
			 ?>
			<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>" >Company Job For Campus</a></li>
			<?php
			}
			?>
									  </ul>
									</div><!--/.nav-collapse --> 
								   </div>
								   
									<div  class="clear"></div>
								</div><!-- Main Navigation -->
					
					<div class="clearfix"></div>
				</div>
			</div><!-- container --> 
		</div>
<!-- header end Menu  -->  
  <?php
  }
  elseif(isset(Yii::$app->session['Employerid']))
  {
   $totalcan=$apl->getTotalcandidate(Yii::$app->session['Employerid']);
   $totalcampus=$apl->getTotalcampus(Yii::$app->session['Employerid']);
  ?>
  <!-- Head  Start -->    
 <div id="head">
  <div class="container">
			  <div class="row">  
			    <!-- logo --> 
					<div class="col-lg-4  col-md-4 col-sm-5 col-xs-7  no_pad top-main">
						<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs" rel="home">
							<img class="main-logo" src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
						</a>
					</div>
				<!-- logo -->
	    <!--mobile -->
			<div class="col-xs-5 no_pad  rt_flt" >   
				 		      <div class="small-23   ds"> <!-- user -------------------------->
								   <div class="dropdown">
									 <button class="btn btn-primary  after_login_image"   onclick="openNav12()" type="button"  > 
										<img  src="<?=Yii::$app->session['EmployerDP'];?>" style="width: 25px;height: 25px;" alt="" class="after_login_profile_photo">
									</button>    
											</ul>
										</div>
								</div>    
								 <div class="small-23  ds"> <!-- Notification --------------------->
								   <div class="dropdown">
									 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
										<span class="fa fa-bell mbl"></span></button> 
											<?php
								$empnotification=[];
								if(isset(Yii::$app->view->params['employernotification']))
								$empnotification=Yii::$app->view->params['employernotification'];
								?>
								<ul class="dropdown-menu">
								   <?php
								   if($empnotification)
								   {
								   foreach($empnotification as $nk1=>$nval1)
								   {
									if($nk1<3)
									{
										$exp='';$link='customerdetail';
										if($nval1->user->experiences)
										{
											$exp='Having'.$nval1->user->experiences[0]->Experience.' Year Experience'; 
										}
										elseif($nval1->user->UserTypeId==4)
										{
											$exp='Company';$link='campusdetail';
										}
										else
										{
											 $exp=$nval1->user->educations[0]->course->CourseName;
										}
								   ?>
								     <li class=""><a target="_blank" href="<?= Url::toRoute(['site/'.$link,'UserId'=>$nval1->UserId,'Nid'=>$nval1->EmpnId])?>">
									<span class="notiLabel"><img src="<?=($nval1->user->UserTypeId==2 || $nval1->user->UserTypeId==5)?(($nval1->user->PhotoId!=0)?$url.$nval1->user->photo->Doc:"$imageurl.images/user.png"):(($nval1->user->LogoId!=0)?$url.$nval1->user->logo->Doc:"$imageurl.images/user.png");?>" style="width: 43px;height: 43px;" /></span>
									<span class="notiLabel"><?=$nval1->user->Name;?></span> <span class="notiCount"><?=$nk1+1;?></span> <p><span class="noti_Description fullWidth">  <?=$exp;?> , <?=$nval1->user->City;?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($nval1->OnDate));?></span></p></a></li>
									<?php
									}
								   }
								   ?>
								   <li class=""><a href="<?= Url::toRoute(['site/empnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Notification</a></li>
								   <li class=""><a href="<?= Url::toRoute(['site/empnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   ?>
								</ul>
										</div>  
									</div>  
								<div class="small-23  ds"> <!-- Message -------------------------------->	
								   <div class="dropdown">
								    <?php
									 $messagelist=$mess->getMessage(Yii::$app->session['Employerid']);
									 $messagelist1=$mess->getMessagecount(Yii::$app->session['Employerid']);
									 $msgcnt=count($messagelist1);
									 ?>
									 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
										<span class="fa fa-envelope-o mbl"></span></button>
									
									 <ul class="dropdown-menu">
								   <?php
								   if($messagelist)
								   {
									$mc=0;
									foreach($messagelist as $mk=>$mval)
									{
									 if($mc<3)
									 {
									 ?>
									 <li class="">
									  <a href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									  <span class="notiLabel"><img src="<?=$mval['Photo'];?>" style="width: 43px;height: 43px;" /></span>
									  <span class="notiLabel"><?=$mval['Name'];?></span> <p><span class="noti_Description fullWidth">
									  <?php if($mval['IsView']){?>
										<?=$mval['Last'].$mval['Message'];?>
										<?php }else{?>
										<strong><?=$mval['Last'].$mval['Message'];?></strong>
										<?php }?>
									  </span><span class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span></p>
									  </a>
									 </li>
									 <?php
									 }
									 $mc++;
									}
									?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span class="notiLabel">View All Message</span> <p><span class="status"></span></p></a></li>
									<?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
								   }
								   ?>
									 </ul>
									 
										</div>  
								</div>
									  
					</div>		
				<!--mobile -->	

				<!-- Main Navigation -->   				
					<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav">
					   <div class="navbar navbar-default " role="navigation"> 
                       <div class="collapse navbar-collapse"> 
				           <ul class="nav navbar-nav">   
							<li class="no-need"> 
							   <a href="<?= Url::toRoute(['campus/jobbycampus'])?>" class="dropdown-toggle  orange_bg new_style">  <b class="fa fa-file-text orange"></b> <br>  Campus List </a>
							</li>   		
							<li class="no-need">
							 
							   <a href="#" data-toggle="dropdown" class="dropdown-toggle  orange_bg new_style">  <b class="fa fa-envelope-o orange"></b> <br>   Message  <span class="total"><?=$msgcnt;?></span> </a>
							   <ul class="dropdown-menu">
								    <?php
								   if($messagelist)
								   {
									$mc=0;
									foreach($messagelist as $mk=>$mval)
									{
									 if($mc<3)
									 {
									 ?>
									 <li class="">
									  <a href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									  <span class="notiLabel"><img src="<?=$mval['Photo'];?>" style="width: 43px;height: 43px;" /></span>
									  <span class="notiLabel"><?=$mval['Name'];?></span> <p><span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span></p>
									  </a>
									 </li>
									 <?php
									 }
									 $mc++;
									}
									?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span class="notiLabel">View All Message</span> <p><span class="status"></span></p></a></li>
									<?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
								   }
								   ?>
									 </ul>
							  
							</li>   
						     <li class="no-need">
							  <?php
								$empnotification=[];
								if(isset(Yii::$app->view->params['employernotification']))
								{
								 $empnotification=Yii::$app->view->params['employernotification'];
								 $emprnotcount=count(Yii::$app->view->params['employernotification']);
								?>
								<a href="#" class="dropdown-toggle brdr  orange_bg new_style" data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>  Notification <span class="total totalempnot"><?=$emprnotcount;?></span></a> 
								<ul class="dropdown-menu">
								    <?php
								   if($empnotification)
								   {
								   foreach($empnotification as $nk1=>$nval1)
								   {
									if($nk1<3)
									{
										$exp='';$link='customerdetail';
										if($nval1->user->experiences)
										{
											$exp='Having '.$nval1->user->experiences[0]->Experience.' Year Experience'; 
										}
										elseif($nval1->user->UserTypeId==4)
										{
											$exp='Company ';$link='campusdetail';
										}
										else
										{
											 $exp=$nval1->user->educations[0]->course->CourseName;
										}
								   ?>
								    <li class=""><a target="_blank" href="<?= Url::toRoute(['site/'.$link,'UserId'=>$nval1->UserId,'Nid'=>$nval1->EmpnId])?>">
									<span class="notiLabel"><img src="<?=($nval1->user->UserTypeId==2 || $nval1->user->UserTypeId==5)?(($nval1->user->PhotoId!=0)?$url.$nval1->user->photo->Doc:"$imageurl.images/user.png"):(($nval1->user->LogoId!=0)?$url.$nval1->user->logo->Doc:"$imageurl.images/user.png");?>" style="width: 43px;height: 43px;" /></span>
									<span class="notiLabel"><?=$nval1->user->Name;?></span> <span class="notiCount"><?=$nk1+1;?></span> <p><span class="noti_Description fullWidth">  <?=$exp;?> , <?=$nval1->user->City;?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($nval1->OnDate));?></span></p></a></li>
									<?php
									}
								   }
								   ?>
								   <li class=""><a href="<?= Url::toRoute(['site/empnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Notification</a></li>
								   <li class=""><a href="<?= Url::toRoute(['site/empnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   ?>
								</ul>
								<?php
									}
									?>
							</li> 
							<li class="no-need prfl_img">
								<a href="javascript:void(0)" class="brdr orange_bg new_style"  onclick="openNav12()">    
   								<img style="width: 43px;height: 43px;" src="<?=Yii::$app->session['EmployerDP'];?>"  alt="" class="img-responsive center-block "><?php echo Yii::$app->session['EmployerName']; ?>  </a>  
							</li> 
				      </ul> 
                </div><!--/.nav-collapse --> 
               </div> 
			 <div class="clear"></div>
		  </div> 
         <!-- Main Navigation -->   					
		<div class="clear"></div>
	</div> 
  </div>
 </div>
<!-- Head  End -->     

 
 
<!-- Right sidebar start Menu  -->  
 <div id="mySidenav12" class="sidenav12">
 <a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a> 
  <div class="side_bar-main">
     <img style="width: 80px;height: 80px;" src="<?=Yii::$app->session['EmployerDP'];?>" alt="" class=""> 
	 <h2>  <?php echo Yii::$app->session['EmployerName']; ?>  </h2>
	 <?php $emp_name = preg_replace('/\s+/', '', Yii::$app->session['EmployerName']); ?>
	   <hr class="height-2"> 
		<ul> 
			<li class=""><a href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".Yii::$app->session['Employerid']?>>">  My Profile</a></li> 
		 	<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB Wall Post  </a></li>
			<li class=""><a href="<?= Url::toRoute(['wall/companyprofile'])?>">  MCB Profile</a></li>  
			
				
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['site/companyprofileeditpage'])?>">  Edit Profile</a></li>	
			<li class=""><a href="<?= Url::toRoute(['site/employerchangepassword'])?>">  Change Password  </a></li>			
			<hr class="height-2">  
			<li class=""><a href="<?= Url::toRoute(['site/hirecandidate'])?>">  Hire Candidate    </a></li>
			<li class=""><a href="<?= Url::toRoute(['team/findteam'])?>">  Hire Team    </a></li>
			<li class=""><a href="<?= Url::toRoute(['campus/campuspost'])?>">  Campus Post    </a></li>
			<li class=""><a href="<?= Url::toRoute(['campus/jobbycampus'])?>">  Campus List    </a></li>
			<hr class="height-2">
			<li class=""><a href="<?= Url::toRoute(['site/candidateapplied'])?>">Applied Candidate  <?=$totalcan;?></a></li>
			<li class=""><a href="<?= Url::toRoute(['site/campusapplied'])?>">Applied Campus  <?=$totalcampus;?></a></li> 
			<li class=""><a href="<?= Url::toRoute(['site/companypost'])?>"> Create Post  </a></li> 
			<li class=""><a href="<?= Url::toRoute(['site/yourpost'])?>">  Your Job Post  </a></li> 
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['site/candidatebookmarked'])?>">  Bookmark Jobs </a></li> 
			<li class=""><a href="<?= Url::toRoute(['site/candidateapplied'])?>">  Applied Jobs</a></li>  
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['site/employerlogout'])?>">  Logout </a></li>  
		 </ul> 
	   <div class="start_a_project">
	  <a href="<?= Url::toRoute(['site/companypost'])?>">  Post a new job</a>
	</div> 
  </div>
 </div>   
<!-- Right sidebar end Menu  -->  	   
	    
		
		
		

<!-- header start Menu  -->  
       <div id="header">
		   <div class="find_a_job">
				 <a href="<?= Url::toRoute(['site/hirecandidate'])?>"><span style="cursor:pointer"> <img src="<?=$imageurl;?>images/find-icon.png"></span> </a>
			 </div> 
					<div class="container"><!-- container -->
						<div class="row">  
							    <div class="col-lg-10  col-md-8 col-sm-8 col-xs-12 main-nav"><!-- Main Navigation --> 
					              <div class="navbar navbar-default " role="navigation"> 
								   <div class="collapse navbar-collapse"> 
									  <ul class="nav navbar-nav float-left">
										<li><a href="<?= Url::toRoute(['site/empdashboard'])?>">Dashboard</a></li> 
                                        <li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB Wall Post  </a></li> 
										<li><a href="<?= Url::toRoute(['site/hirecandidate'])?>">Hire Candidate</a></li>
										<li><a href="<?= Url::toRoute(['team/findteam'])?>">Hire Team</a></li>
										<li><a href="<?= Url::toRoute(['campus/campuspost'])?>">  Campus Post    </a></li>
										<!--<li><a href="<?= Url::toRoute(['campus/jobbycampus'])?>">  Campus List    </a></li>-->
										<li> <a href="<?= Url::toRoute(['site/candidateapplied'])?>"> Applied Candidate <span class="total"><?=$totalcan;?></span> </a></li>
										<li> <a href="<?= Url::toRoute(['site/campusapplied'])?>"> Applied Campus <span class="total"><?=$totalcampus;?></span> </a></li>
										<li><a href="<?= Url::toRoute(['site/yourpost'])?>">  Your Post Job    </a></li>
										<li><a href="<?= Url::toRoute(['site/companypost'])?>">  Create New Post    </a></li>
										
									  </ul>
									</div><!--/.nav-collapse --> 
								   </div>
								   
									<div  class="clear"></div>
								</div><!-- Main Navigation -->
					
					<div class="clearfix"></div>
				</div>
			</div><!-- container --> 
		</div>
<!-- header end Menu  -->  
  <?php
  }
  elseif(isset(Yii::$app->session['Campusid']))
  {
   ?>
   <div id="head">
  <div class="container">
			  <div class="row"> 
			            <!--mobile -->
							<div class="col-xs-2 menu_humb extra_mar">
							 <button class="btn btn-primary dropdown-toggle" onclick="openNav1()"> 
		                  	<small class="fa fa-bars mbl"></small></button> 
						</div>		
						<!--mobile -->	 
					<div class="col-lg-4  col-md-4 col-sm-5 col-xs-7  no_pad top-main"><!-- logo -->
						<a href="<?= Url::toRoute(['site/index'])?>" title="My Career Bugs" rel="home">
							<img class="main-logo" src="<?=$imageurl;?>images/logo.png" alt="My Career Bugs">
						</a>
					</div><!-- logo --> 
					
			   <!--mobile -->
				<div class="col-xs-3 no_pad  rt_flt" > 
							  <div class="small-23   ds"><!-- user -->
							   <div class="dropdown">
								 <button class="btn btn-primary  after_login_image"   onclick="openNav12()" type="button"  > 
									<img  src="<?=Yii::$app->session['CampusDP'];?>" style="width: 25px;height: 25px;" alt="" class="after_login_profile_photo">
								</button>   
									 
										</ul>
									</div>
								</div>  
				 		
					
					
					<!-- Notification -->
									 <div class="small-23  ds"> 
									   <div class="dropdown">
										 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
											<span class="fa fa-bell mbl"></span></button>
												 
												 <ul class="dropdown-menu" id="ntfctn">
														<?php
								   if($campusnotification)
								   {
								   foreach($campusnotification as $nk1=>$nval1)
								   {
									if($nk1<3)
									{
								   ?>
								     <li class=""><a target="_blank" href="<?=$nval1['link'];?>">
									<span class="notiLabel"><img src="<?=$nval1['image'];?>" style="width: 43px;height: 43px;" /></span>
									<span class="notiLabel"><?=$nval1['name'];?></span> <span class="notiCount"><?=$nk1+1;?></span> <p><span class="noti_Description fullWidth">  <?=$nval1['title'];?> , <?=$nval1['address'];?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($nval1['date']));?></span></p></a></li>
									<?php
									}
								   }
								   ?>
								   <li class=""><a href="<?= Url::toRoute(['site/campusnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Notification</a></li>
								   <li class=""><a href="<?= Url::toRoute(['site/campusnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   ?>
														
												 </ul>
											</div>  
										</div>

								</div>		
								<!--mobile -->	 

								
					<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav"><!-- Main Navigation -->   
					   <div class="navbar navbar-default " role="navigation"> 
                       <div class="collapse navbar-collapse"> 
				           <ul class="nav navbar-nav"> 
						
							<!-- It will display when customer paid  for that -->		
							<li class="no-need"> 
							   <a href="<?= Url::toRoute(['campus/createpost'])?>" class="dropdown-toggle  orange_bg new_style">  <b class="fa fa-file-text orange"></b> <br>   Campus Post </a>
							</li>  
						<!-- It will display when customer paid  for that -->	

						
						
						<!-- It will display when customer paid  for that -->		
							<li class="no-need" style="display: none;"> 
							   <a href="<?= Url::toRoute(['site/hirecandidate'])?>" class="dropdown-toggle  orange_bg new_style">  <b class="fa fa-file-text orange"></b> <br>   Candidate List</a>
							</li>  
						<!-- It will display when customer paid  for that -->	

						<li class="no-need">
								<a href="#" class="dropdown-toggle brdr  orange_bg new_style" data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>  Notification <span class="total"><?=count($campusnotification);?></span></a> 
								<ul class="dropdown-menu">
								   <?php
								   if($campusnotification)
								   {
								   foreach($campusnotification as $nk1=>$nval1)
								   {
									if($nk1<3)
									{
								   ?>
								     <li class=""><a target="_blank" href="<?=$nval1['link'];?>">
									<span class="notiLabel"><img src="<?=$nval1['image'];?>" style="width: 43px;height: 43px;" /></span>
									<span class="notiLabel"><?=$nval1['name'];?></span> <span class="notiCount"><?=$nk1+1;?></span> <p><span class="noti_Description fullWidth">  <?=$nval1['title'];?> , <?=$nval1['address'];?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($nval1['date']));?></span></p></a></li>
									<?php
									}
								   }
								   ?>
								   <li class=""><a href="<?= Url::toRoute(['site/campusnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Notification</a></li>
								   <li class=""><a href="<?= Url::toRoute(['site/campusnotification'])?>"><span class="notiLabel">View All</span></a></li>
								   <?php
								   }
								   ?>
								</ul>
							</li>
							
							<li class="no-need prfl_img">
								<a href="javascript:void(0)" class="brdr orange_bg new_style"  onclick="openNav12()">    
   								<img style="width: 43px;height: 43px;" src="<?=Yii::$app->session['CampusDP'];?>"  alt="" class="img-responsive center-block "><?php echo Yii::$app->session['CampusName']; ?>  </a> 
								<ul class="dropdown-menu"> 
									<li class=""><a href="<?= Url::toRoute(['campus/campusprofile'])?>"> <b class="fa fa-user"></b>  My Profile</a></li> 
									<li class=""><a href="<?= Url::toRoute(['campus/campusprofileedit'])?>"><b class="fa fa-pencil-square-o"></b>   Edit Profile</a></li>  
									<li class=""><a href="<?= Url::toRoute(['campus/changepwd'])?>"> <b class="fa fa-lock"></b> Change Password  </a></li> 
									<li class=""><a href="<?= Url::toRoute(['campus/campuslogout'])?>"> <b class="fa fa-power-off"></b> Log Out  </a></li>  
								</ul>
							</li> 
				      </ul> 
                </div><!--/.nav-collapse --> 
               </div> 
						<div class="clear"></div>
					</div>  
			<div class="clear"></div>
		</div><!-- Main Navigation --> 
  </div>
</div>


<div id="mySidenav12" class="sidenav12">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a>

 <div class="side_bar-main">
     <img style="width: 83px;height: 83px;" src="<?=Yii::$app->session['CampusDP'];?>" alt="" class=""> 
	 <h2><?php echo Yii::$app->session['CampusName']; ?></h2>
	<hr class="height-2">
	
		<ul>
			 
			<li class=""><a href="<?= Url::toRoute(['campus/campusprofile'])?>">  My Profile</a></li> 
			
			<li class=""><a href="<?= Url::toRoute(['wall/campusprofile'])?>"> MCB Profile</a></li>  
			<li class=""><a href="<?= Url::toRoute(['wall/careerbugwall'])?>">   MCB Wall Post</a></li>  
				<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['campus/campusprofileedit'])?>">  Edit Profile</a></li>	
			<li class=""><a href="<?= Url::toRoute(['campus/changepwd'])?>">  Change Password  </a></li>			
			<hr class="height-2">  
			<li class=""><a href="<?= Url::toRoute(['campus/jobsearch'])?>">  Job Search    </a></li>  
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['campus/yourpost'])?>">  Your Job Post  </a></li> 
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['campus/bookmarkedjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Campusid']);?></a></li> 
			<li class=""><a href="<?= Url::toRoute(['campus/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobAppliedc'];?>    </a></li>
			<li><a href="<?= Url::toRoute(['campus/studentlist'])?>" >Student List</a></li>
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['campus/campuslogout'])?>">  Logout </a></li>  
		 </ul>

	<div class="start_a_project">
	  <a href="<?= Url::toRoute(['campus/createpost'])?>">  Post a new job</a>
	</div>
	
 </div>
</div>   
	   
 	   <div id="header"><!-- start main header -->
		   <div class="find_a_job">
		     <span style="cursor:pointer" onclick="openNav()"> <img src="<?=$imageurl;?>images/find-icon.png"></span> 
         </div>
					<div class="container"><!-- container -->
						<div class="row">  
							  <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 main-nav"><!-- Main Navigation --> 
							   <div class="navbar navbar-default " role="navigation"> 
							   <div class="collapse navbar-collapse">  
								<ul class="nav navbar-nav float-left">
									<li><a href="<?= Url::toRoute(['campus/dashboard'])?>">Dashboard</a></li>  
									
									<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">  MCB Wall Post</a></li> 
									
									<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Search Company Post</a></li>  
									<li><a href="#">Applied  Company</a></li> 
									<li> <a href="<?= Url::toRoute(['campus/appliedjob'])?>"> Applied Job Post<span class="total"><?=Yii::$app->session['NoofjobAppliedc'];?></span>  </a></li> 
									<li><a href="<?= Url::toRoute(['campus/yourpost'])?>">My Post</a></li>  
									<li><a href="<?= Url::toRoute(['campus/createpost'])?>" >Create  Campus Post</a></li>
									<li><a href="<?= Url::toRoute(['campus/studentlist'])?>" >Student List</a></li>
								</ul>
						</div><!--/.nav-collapse --> 
					   </div> 
						<div  class="clear"></div>
					</div><!-- Main Navigation --> 
					
					<div class="clearfix"></div>
				</div>
			</div><!-- container --> 
		</div><!-- end main header -->
   <?php
  }
  elseif(isset(Yii::$app->session['Teamid']))
  {
   ?>
   <div id="head">
  <div class="container">
			  <div class="row"> 
			           <!-- logo --> 
					<div class="col-lg-4  col-md-4 col-sm-5 col-xs-7  no_pad top-main">
						<a href="<?= Url::toRoute(['site/index'])?>" title="Career Bugs" rel="home">
							<img class="main-logo" src="<?=$imageurl;?>images/logo.png" alt="Career Bugs">
						</a>
					</div>
				<!-- logo -->
					
			   <!--mobile -->
				<div class="col-xs-5 no_pad  rt_flt" > 
							  <div class="small-23   ds"><!-- user -->
							   <div class="dropdown">
								 <button class="btn btn-primary  after_login_image"   onclick="openNav12()" type="button"  > 
									<img  src="<?=Yii::$app->session['TeamDP'];?>" style="width: 25px;height: 25px;" alt="" class="after_login_profile_photo">
								</button>   
									 
										</ul>
									</div>
								</div>  
				 		
					
					
					<!-- Notification -->
									 <div class="small-23  ds"> 
									   <div class="dropdown">
										 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
											<span class="fa fa-bell mbl"></span></button>
												 
												 <ul class="dropdown-menu" id="ntfctn">
														<!--<li class=""><a target="_blank" href="#"><span class="notiLabel">Job Recommendations</span> <span class="notiCount">08</span> <p><span class="noti_Description fullWidth">HR Manager Position at Innofied Solution, Kolkata</span><span class="status"></span></p></a></li> 
														 <li class=""><a target="_blank" href="#"><span class="notiLabel">Job Recommendations</span> <span class="notiCount">08</span> <p><span class="noti_Description fullWidth">HR Manager Position at Innofied Solution, Kolkata</span><span class="status"></span></p></a></li> -->
														
												 </ul>
											</div>  
										</div>
									 
										<div class="small-23  ds"> <!-- Message -------------------------------->	
								   <div class="dropdown">
									<?php
									 $messagelist=$mess->getMessage(Yii::$app->session['Teamid']);
									 $messagelist1=$mess->getMessagecount(Yii::$app->session['Teamid']);
									 $msgcnt=count($messagelist1);
									 
									 ?>
									 <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
										<span class="fa fa-envelope-o mbl"></span></button>
									 <ul class="dropdown-menu">
								   <?php
								   if($messagelist)
								   {
									$mc=0;
									foreach($messagelist as $mk=>$mval)
									{
									 if($mc<3)
									 {
									 ?>
									 <li class="">
									  <a href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									  <span class="notiLabel"><img src="<?=$mval['Photo'];?>" style="width: 43px;height: 43px;" /></span>
									  <span class="notiLabel"><?=$mval['Name'];?></span> <p><span class="noti_Description fullWidth">
									  <?php if($mval['IsView']){?>
											<?=$mval['Last'].$mval['Message'];?>
											<?php }else{?>
											<strong><?=$mval['Last'].$mval['Message'];?></strong>
											<?php }?>
									  </span><span class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span></p>
									  </a>
									 </li>
									 <?php
									 }
									 $mc++;
									}
									?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span class="notiLabel">View All Message</span> <p><span class="status"></span></p></a></li>
									<?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
								   }
								   ?>
									 </ul>
									 
										</div>  
								</div>

								</div>		
								<!--mobile -->	 

								
					<div class="col-lg-8  col-md-8 col-sm-7	 col-xs-7  main-nav"><!-- Main Navigation -->   
					   <div class="navbar navbar-default " role="navigation"> 
                       <div class="collapse navbar-collapse"> 
				           <ul class="nav navbar-nav"> 
						
							<!-- It will display when customer paid  for that -->		
							<li class="no-need"> 
							   <a href="<?= Url::toRoute(['campus/jobbycompany'])?>" class="dropdown-toggle  orange_bg new_style">  <b class="fa fa-file-text orange"></b> <br>   Company List </a>
							</li>  
						<!-- It will display when customer paid  for that -->	

						
						
						<!-- It will display when customer paid  for that -->		
							<li class="no-need">
							 
							   <a href="#" class="dropdown-toggle  orange_bg new_style" data-toggle="dropdown">  <b class="fa fa-envelope orange"></b> <br>Message <span class="total"><?=$msgcnt;?></span></a>
							   
									 <ul class="dropdown-menu">
								   <?php
								   if($messagelist)
								   {
									$mc=0;
									foreach($messagelist as $mk=>$mval)
									{
									 if($mc<3)
									 {
									 ?>
									 <li class="">
									  <a href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									  <span class="notiLabel"><img src="<?=$mval['Photo'];?>" style="width: 43px;height: 43px;" /></span>
									  <span class="notiLabel"><?=$mval['Name'];?></span> <p><span class="noti_Description fullWidth"><?=$mval['Last'].$mval['Message'];?></span><span class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span></p>
									  </a>
									 </li>
									 <?php
									 }
									 $mc++;
									}
									?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span class="notiLabel">View All Message</span> <p><span class="status"></span></p></a></li>
									<?php
								   }
								   else
								   {
								   ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
								   }
								   ?>
									 </ul>
							
							</li>  
						<!-- It will display when customer paid  for that -->	

						<li class="no-need">
								 
								<a href="#" class="dropdown-toggle brdr  orange_bg new_style" data-toggle="dropdown"><b class="fa fa-bell orange"></b> <br>  Notification <span class="total">0</span></a> 
								<ul class="dropdown-menu">
								   <li>No Notification</li>
								</ul>
							</li>
							
							<li class="no-need prfl_img">
								<a href="javascript:void(0)" class="brdr orange_bg new_style"  onclick="openNav12()">    
   								<img style="width: 43px;height: 43px;" src="<?=Yii::$app->session['TeamDP'];?>"  alt="" class="img-responsive center-block "><?php echo Yii::$app->session['TeamName']; ?>  </a> 
								<ul class="dropdown-menu"> 
									<li class=""><a href="<?= Url::toRoute(['team/teamprofile'])?>"> <b class="fa fa-user"></b>  My Profile</a></li> 
									<li class=""><a href="<?= Url::toRoute(['team/teameditprofile'])?>"><b class="fa fa-pencil-square-o"></b>   Edit Profile</a></li>  
									<li class=""><a href="<?= Url::toRoute(['team/changepwd'])?>"> <b class="fa fa-lock"></b> Change Password  </a></li> 
									<li class=""><a href="<?= Url::toRoute(['team/teamlogout'])?>"> <b class="fa fa-power-off"></b> Log Out  </a></li>  
								</ul>
							</li> 
				      </ul> 
                </div><!--/.nav-collapse --> 
               </div> 
						<div class="clear"></div>
					</div>  
			<div class="clear"></div>
		</div><!-- Main Navigation --> 
  </div>
</div>


<div id="mySidenav12" class="sidenav12">
<a href="javascript:void(0)" class="closebtn" onclick="closeNav12()">&times;</a>

 <div class="side_bar-main">
     <img style="width: 83px;height: 83px;" src="<?=Yii::$app->session['TeamDP'];?>" alt="" class=""> 
	 <h2><?php echo Yii::$app->session['TeamName']; ?></h2>
	<hr class="height-2">
	
		<ul>
			 
			<li class=""><a href="<?= Url::toRoute(['team/teamprofile'])?>">  My Profile</a></li> 
			<li class=""><a href="<?= Url::toRoute(['wall/careerbugwall'])?>">  Career Wall</a></li>  
			<li class=""><a href="<?= Url::toRoute(['wall/teamprofile'])?>">  My Career Wall</a></li>  
				<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['team/editprofile'])?>">  Edit Profile</a></li>	
			<li class=""><a href="<?= Url::toRoute(['team/changepwd'])?>">  Change Password  </a></li>			
			<hr class="height-2">  
			<li class=""><a href="<?= Url::toRoute(['site/jobsearch'])?>">  Job Search    </a></li>
			<li class=""><a href="<?= Url::toRoute(['site/recentjob'])?>">  Recent Job    </a></li>  
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['team/bookmarkedjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Teamid']);?></a></li> 
			<li class=""><a href="<?= Url::toRoute(['team/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobAppliedt'];?>    </a></li>  
			<hr class="height-2"> 
			<li class=""><a href="<?= Url::toRoute(['team/teamlogout'])?>">  Logout </a></li>  
		 </ul>
 </div>
</div>   
	   
 	   <div id="header"><!-- start main header -->
		   <div class="find_a_job">
		     <span style="cursor:pointer" onclick="openNav()"> <img src="<?=$imageurl;?>images/find-icon.png"></span> 
         </div>
					<div class="container"><!-- container -->
						<div class="row">  
							  <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 main-nav"><!-- Main Navigation --> 
							   <div class="navbar navbar-default " role="navigation"> 
							   <div class="collapse navbar-collapse">  
								<ul class="nav navbar-nav float-left">
									<li><a href="<?= Url::toRoute(['team/dashboard'])?>">Dashboard</a></li>  
									<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB Wall Post</a></li>  
									<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Team Jobs</a></li>
									<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
									<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent Jobs</a></li>
									<li> <a href="<?= Url::toRoute(['team/appliedjob'])?>"> Applied Jobs  <span class="total"><?=Yii::$app->session['NoofjobAppliedt'];?></span>  </a></li> 
									<li><a href="<?= Url::toRoute(['team/bookmarkedjob'])?>">  Bookmark Jobs  <span class="total"><?=$apl->getTotalbookmark(Yii::$app->session['Teamid']);?></span>    </a></li> 
									
								</ul>
						</div><!--/.nav-collapse --> 
					   </div> 
						<div  class="clear"></div>
					</div><!-- Main Navigation --> 
					
					<div class="clearfix"></div>
				</div>
			</div><!-- container --> 
		</div><!-- end main header -->
   <?php
  }
}
?>
  
 <!---------------------------------------------- Header End ----------------------------------------------> 
  
  
  
 
 
 
<!---------------------- Container Start ------------------------>
<?php
if(Yii::$app->session->hasFlash('error') || Yii::$app->session->hasFlash('success'))
{
	 if(Yii::$app->session->hasFlash('error'))
	 {
		  $flashimage=$imageurl."images/error.png";
	 }
	 elseif(Yii::$app->session->hasFlash('success'))
	 {
		  $flashimage=$imageurl."images/success.png";
	 }
	 ?>
		  <div class="modal add-resume-modal" style="display: block;" id="alertbox" tabindex="-1" role="dialog" aria-labelledby="">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="input-group image-preview form-group " style="width: 100%;height: auto;text-align: center;">
							  <img src="<?=$flashimage;?>" style="width: 70px;"/>
                            <?= Alert::widget() ?>
							<input type="button" value="OK" aria-hidden="true" data-dismiss="alert" class="btn btn-lg btn-primary btn-block" style="width: 100px;margin: auto;" onclick="$('#alertbox').hide();$('.modal-backdrop').hide();"/>
                        </div>
                    </div>
                </div>
            </div>
          </div>
		  <div class="modal-backdrop  in">
		  </div>
<?php
}
?>
<?php echo Loader::widget()?>
        <?= $content ?>
<!---------------------- Container End ------------------------>


<!---------------------- Footer Start ------------------------>

<div id="footer"><!-- Footer -->
			<div class="container"><!-- Container -->
				<div class="row">
					<div class="col-md-3 footer-widget"><!-- Text Widget -->
					<?php
					$aboutus = common\models\FooterAboutus::find()->one();
					?>
						<h6 class="widget-title"><?=$aboutus->Heading;?></h6>
						<div class="textwidget">
							<?=htmlspecialchars_decode($aboutus->Content);?>
						</div>
					</div><!-- Text Widget -->
					
					<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12  footer-widget"><!-- Footer Menu Widget -->
						<h6 class="widget-title">Hot Categories</h6>
						<div class="footer-widget-nav">
							<ul>
								  <?php
							   $htcat=$indu->getHotcategory();
							  if($htcat)
							  {
								foreach($htcat as $hk=>$hval)
								{
								?>
								<li><a href="<?= Url::toRoute(['site/jobsearch','JobCategoryId'=>$hval->IndustryId])?>"><?=$hval->IndustryName;?></a></li>
							  <?php
								}
							  }
							  ?>
							</ul>
						</div>
					</div><!-- Footer Menu Widget -->
					
					<div class="col-md-3 footer-widget"><!-- Recent Tweet Widget -->
						<?php
						$thirdblock = common\models\FooterThirdBlock::find()->one();?>
						<h6 class="widget-title"><?=$thirdblock->Heading;?></h6>
						<div class="footer-widget-nav">
							<ul>
								<li><a href="<?= Url::toRoute(['site/index']);?>">Home</a></li>
								<li><a href="<?= Url::toRoute(['site/jobsearch']);?>">Job Search</a></li>
								<li><a href="<?= Url::toRoute(['site/employerslogin']);?>">Post a Job</a></li>
								<li><a href="<?= Url::toRoute(['site/login']);?>">Post a Resume</a></li>
							     <li><a href="<?= Url::toRoute(['site/feedback']);?>">Feedback</a></li>
								 <li><a href="<?= Url::toRoute(['content/privacy']);?>" >Privacy Policy</a></li>
								 <li><a href="<?= Url::toRoute(['content/terms']);?>" >Terms & Condition</a></li>
								 <li><a href="<?= Url::toRoute(['content/faq']);?>" >Faq</a></li>
                                 <li><a href="<?= Url::toRoute(['site/blog']);?>" target="_blank">Blog</a></li>
							</ul>
						</div>
					</div><!-- Recent Tweet Widget -->

					<div class="col-md-3 footer-widget"><!-- News Leter Widget -->
					<div id="contact_details">
					<?php
					$contactus = common\models\FooterContactus::find()->one();
					?>
					  <h6 class="widget-title"><?=$contactus->Heading;?></h6> 
					     <?=htmlspecialchars_decode($contactus->Content);?>
					  </div>
						<h6 class="widget-title sig1">Singin For news Letter</h6>
							
							<div class="single-widget"> 
										<input autocomplete="off" type="email" id="news_email" type="text" class="col-xs-15 subscribeInputBox" placeholder="Your email address">
										<button class="col-xs-7 btn btn-theme-secondary subscribeBtn reset-padding" onclick="newsletter();">Subscribe</button> 
								 <div class="clearfix"></div>
					      	</div>

					  <p>Register now to get updates on latest Vacancy.</p>

					</div><!-- News Leter Widget -->
					<div class="clearfix"></div>
				</div>

				<div id="footer"><!-- Footer -->
			<div class="container"><!-- Container -->
				<div class="row">
					 <?php
					 $socialicon = common\models\SocialIcon::find()->one();
					 ?>
					   <ul class="list-inline social-buttons">
						 <?php
						 if($socialicon->IsTwitter==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->TwitterLink;?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php
						 }
						 if($socialicon->IsFacebook==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->FacebookLink;?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php
						 }
						 if($socialicon->IsLinkedin==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->LinkedinLink;?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php
						 }
						 if($socialicon->IsGoogleplus==1)
						 {
						 ?>
                        <li><a href="<?=$socialicon->GoogleplusLink;?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<?php
						 }
						 ?>
                    </ul>
				 
				</div>
			</div><!-- Container -->
		</div>
		
		
		
		
		<div class="developed_by_e-developed_technology">
	      <div class="row">
			 <div class="col-md-6">
			   <?php
			   $copyright = common\models\FooterCopyright::find()->one();
					echo htmlspecialchars_decode($copyright->Content);?>
					
			   </div>
			  <div class="col-md-6">
			   <?php
			   $dblock = common\models\FooterDevelopedblock::find()->one();
					echo htmlspecialchars_decode($dblock->Content);?>
					
			   </div>
		   </div>
		</div>
				
			</div><!-- Container -->
</div><!-- Footer -->

<!---------------------- Footer End ------------------------->
<?php $this->endBody() ?>
<script>
  function openNav12() {
		document.getElementById("mySidenav12").style.width = "250px";
	   } 
	function closeNav12() {
		document.getElementById("mySidenav12").style.width = "0";
	}
$(document).ready(function(){
	<?php
	if(isset($_GET['from']) && isset($_GET['to']))
	{
	?>
	message(<?=$_GET['from'];?>,<?=$_GET['to'];?>);
	<?php
	}
	else
	{
	 $messageto=Yii::$app->view->params['Messageto'];
	 $messagefrom=Yii::$app->view->params['Messagefrom'];
	?>
	message(<?=$messagefrom;?>,<?=$messageto;?>);
	<?php
	}
	?>
  
	$('#Messages').keypress(function(e){
			 var unicode=e.charCode? e.charCode : e.keyCode;
			  if (unicode==13) {
				  var value = $('#Messages').val();
				  
					if(value != ""){
						messagesubmit(localStorage.empid,localStorage.userid);
					}else{
						alert('Please enter message');
					}
              }
			});
	$(document).on('click', '#messagesb', function(){
		var value = $('#Messages').val();
		if(value != ""){
			messagesubmit(localStorage.empid,localStorage.userid);
		}else{
			alert('Please enter message');
		}
		
	});		
});
function message(empid,userid) {
 if (isMobile==true) {
   $(".ms-menu").hide();
 }
 
 localStorage.empid=empid;localStorage.userid=userid;
    $.ajax({url:"<?= Url::toRoute(['message/getmessagelist']);?>?empid="+localStorage.empid+"&userid="+localStorage.userid,
		   success:function(results)
		   {
			$('#messagebox').html(results);
			 if (isMobile==true) {
			 $("#ms-menu-trigger").click(function(){
				 $(".ms-menu").toggle("300");
			 });
			 }
		   }
	});
	$("#messagebox").animate({ scrollTop: $('#messagebox').height()+350}, 1000);
	setTimeout(function(){message(localStorage.empid,localStorage.userid);},60000);
}

function messagesubmit(from,to) {
    var Messages=encodeURIComponent(JSON.stringify($('#Messages').val()));
	if (Messages!='') {
        $.ajax({url:"<?= Url::toRoute(['message/messageadd']);?>?Messages="+Messages+"&from="+from+"&to="+to,
			   success:function(result)
			   {
				var res=JSON.parse(result);
				if (res==1) {
                   message(from,to);$('#Messages').val('').focus();
                }
			   }
		})
    }
}
</script> 
</body>
</html>
<?php $this->endPage() ?>
