<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppwAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;
use common\components\loader\Loader;

AppwAsset::register($this);
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>"> 
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="My Career Bugs Wall,  mycareer bugs wall, mcb wall, MCB wall, wall, job wall, job post wall, Mcb wall post, Kolkata job, Kolkata  jobs, mcb wall, wall post, Job Post,  job post, Need Jobs, Online Jobs, Marketing Jobs, Bpo Jobs, It Jobs, Hr Jobs, Marketing Jobs,  Job in India, job in kolkata, job vacancies, job openings, find jobs,  freshers jobs, experienced jobs" /> 
    <meta name="author" content="">
	 

	 
	 
	 
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Bootstrap core CSS -->
	<?php $this->head() ?>
    <link rel="shortcut icon" href="<?=$imageurl;?>careerimg/icons/favicon.png"/>
	<link rel="apple-touch-icon" href="<?=$imageurl;?>careerimg/icons/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=$imageurl;?>careerimg/icons/apple-touch-icon-72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=$imageurl;?>careerimg/icons/apple-touch-icon-114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=$imageurl;?>careerimg/icons/apple-touch-icon-144.png">

<style>
@media (max-width: 767px) {
body{ padding:45px 0 0 0;}
#head{position:fixed; top:0px; left:0px; z-index:99999999}

}

</style>



</head>
<body>
  <?php $this->beginBody() ?>
 <!---------------------------------------------- Header start ---------------------------------------------->
 <!---------------------------------------------- Header End ----------------------------------------------> 
<!---------------------- Container Start ------------------------>
<?php
if(Yii::$app->session->hasFlash('error') || Yii::$app->session->hasFlash('success'))
{
	 if(Yii::$app->session->hasFlash('error'))
	 {
		  $flashimage=$imageurl."images/error.png";
	 }
	 elseif(Yii::$app->session->hasFlash('success'))
	 {
		  $flashimage=$imageurl."images/success.png";
	 }
	 ?>
		  <div class="modal add-resume-modal" style="display: block;" id="alertbox" tabindex="-1" role="dialog" aria-labelledby="">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="input-group image-preview form-group " style="width: 100%;height: auto;text-align: center;">
							  <img src="<?=$flashimage;?>" style="width: 70px;"/>
                            <?= Alert::widget() ?>
							<input type="button" value="OK" aria-hidden="true" data-dismiss="alert" class="btn btn-lg btn-primary btn-block" style="width: 100px;margin: auto;" onclick="$('#alertbox').hide();$('.modal-backdrop').hide();"/>
                        </div>
                    </div>
                </div>
            </div>
          </div>
		  <div class="modal-backdrop  in">
		  </div>
<?php
}
?>
<?php echo Loader::widget()?>
        <?= $content ?>
<!---------------------- Container End ------------------------>
   
  
   

<!---------------------- Footer Start ------------------------>
<div class="developed_by_e-developed_technology" id="careerwall_lading_copyright">
	      <div class="row">
			 <div class="col-md-6">
			   <p>	2019 © <a href="https://www.mycareerbugs.com">My Career Bugs</a>. All Rights Reserved.</p>
			   </div>
			  <div class="col-md-6">
			   <a href="http://www.e-developedtechnology.com" target="_blank">Designed by <span> E-developed Technology </span></a>
			   </div>
		   </div>
		</div>
		
		

<!---------------------- Footer End ------------------------->

<?php $this->endBody() ?>

<script>
$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});


$('.messages a').click(function(){
   $('.form').animate({height: "toggle", opacity: "toggle"}, "slow");
});

</script>
</body>
</html>
<?php $this->endPage() ?>