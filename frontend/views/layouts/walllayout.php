<?php

/* @var $this \yii\web\View */
/* @var $content string */
use common\models\AppliedJob;
use common\models\FooterAboutus;
use common\models\Industry;
use common\models\Messages;
use common\models\Notification;
use common\widgets\Alert;
use frontend\assets\AppwAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\searchpeople\SearchPeople;
use common\components\loader\Loader;
$indu = new Industry();
$mess = new Messages();
$apl = new AppliedJob();
$notification = new Notification();
$campusnotification = $notification->getcampusnot();

if(!isset(Yii::$app->session['EmployerDP'])){
  Yii::$app->session['EmployerDP']="/images/user.png";
}
AppwAsset::register($this);
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" prefix="og: http://ogp.me/ns#">
<head>
<title><?= Html::encode($this->title) ?></title>
<?php if(isset($this->params['meta_description']) && !empty($this->params['meta_description'])){?>	
<meta name="description" content="<?=$this->params['meta_description'];?>"/>
<?php }?>

<meta property="og:site_name" content="https://www.localhost"/>
<meta property="og:locale" content="en_US"/>

<?php if(isset($this->params['og_title']) && !empty($this->params['og_title'])){?>	
<meta property="og:title" content="<?=$this->params['og_title'];?>" />
<?php }?>
<?php if(isset($this->params['og_description']) && !empty($this->params['og_description'])){?>	
<meta property="og:description" content="<?=$this->params['og_description'];?>"/>
<?php }?>
<?php if(isset($this->params['og_url']) && !empty($this->params['og_url'])){?>	
<meta property="og:url" content="<?=$this->params['og_url'];?>"/>
<?php }?>
<?php if(isset($this->params['og_image']) && !empty($this->params['og_image'])){?>	
<meta property="og:image" content="<?=$this->params['og_image'];?>"/>
<?php }?>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">

<meta name="keywords"
	content="My career bugs wall, mcb wall, wall, job wall, Mcb wall post">
<meta name="description"
	content="Interface to Trending jobs. Walk-Ins! Top Employers. Get Registered! Hiring Trends." /> 
  
  
  
	<?= Html::csrfMetaTags() ?>
    
<!-- Bootstrap core CSS -->
	<?php $this->head() ?>

    <link rel="shortcut icon"
	href="<?=$imageurl;?>careerimg/icons/favicon.png" />
<link rel="apple-touch-icon"
	href="<?=$imageurl;?>careerimg/icons/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="<?=$imageurl;?>careerimg/icons/apple-touch-icon-72.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="<?=$imageurl;?>careerimg/icons/apple-touch-icon-114.png">
<link rel="apple-touch-icon" sizes="144x144"
	href="<?=$imageurl;?>careerimg/icons/apple-touch-icon-144.png">
<style>

.menu_block, .navbar{    position: relative;z-index: 9;}
    
#peoplesearch{border-radius:100px !important;margin: 0 0 0 5px;}
.src1{border-radius:100px !important;}
@media (max-width: 767px) {
body{ padding:45px 0 0 0;}
#head{position:fixed; top:0px; left:0px; z-index:99999999}

}

</style>

 <?php
if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Campusid']) || isset(Yii::$app->session['Teamid'])) {
    ?>
  <link href="<?=$imageurl;?>css/after_login_css.css" rel="stylesheet">
  <?php
}
?>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>.wall {
    position: fixed;
    right: 5px;
    bottom: 5px;
    z-index: 999;
    display:none;
    text-align: center;
    color: #fff;
    background: none;
    border: 0px;
    border-radius: 0 6px 6px 0;}
    .home_mbl-1{display:none}
   @media (max-width: 767px){ .home_mbl-1{display:block}
       .top_user_message.home_mbl-1 .fa.fa-home{line-height: 27px;font-size: 17px;}
   }
    </style>
</head>
<body>
   <!-- <div id="no-charge" style="text-align:center;width:100%; background:#6d136a; color:#fff; line-height:32px;font-size:12px"> 
    mycareerbugs.com never charge for a job, registration or an interview in exchange of money. </div> -->
    
    
    <a href="https://www.localhost/wall/index" class="wall"><img src="https://mycareerbugs.com/images/career_wall.png"> </a>
    
  <?php $this->beginBody() ?>
<!---------------------------------------------- Header start ---------------------------------------------->
	<div class="directory-info-row fixed-pos">
		<ul class="social-links">
			<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
			<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
			<li><a href="#" title="Blog"><i class="fa fa-bold"></i> </a></li>
			<li><a href="#" title="Blog"><i class="fa fa-envelope"></i> </a></li>
		</ul>
	</div> 
									
<?php
if (isset(Yii::$app->session['Employeeid'])) {
    ?>
    <div class="header-top" id="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
					<a class="logo_top" href="<?= Url::toRoute(['wall/mcbwallpost'])?>">
						<img src="<?=$imageurl;?>careerimg/logo.png" alt="Logo">
					</a>
				</div>
				<div class="col-lg-8  col-md-8 col-sm-8 col-xs-12 main-nav">
					<!-- Main Navigation -->
					<div class="top_user">
						<a href="#" class="dropdown-toggle brdr orange_bg new_style"
							onclick="openNav()"> <img style="width: 33px; height: 33px;"
							src="<?=Yii::$app->session['EmployeeDP'];?>" alt=""
							class="img-responsive center-block "> <span><?php echo Yii::$app->session['EmployeeName']; ?></span></a>
					</div>


					<div class="top_user_notification">
						<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown">
						  <?php
    $alnotification = [];
    if (isset(Yii::$app->view->params['employeenotification'])) {
        $alnotification = Yii::$app->view->params['employeenotification'];
        $empnotcount = count(Yii::$app->view->params['employeenotification']);
    }
    ?>
						 <b class="fa fa-bell orange"></b> <span class="not"> 
								<span class="total"><?=$empnotcount;?></span>
						</span>
						</a>
						<ul class="dropdown-menu">
								   <?php
    if ($alnotification) {
        foreach ($alnotification as $nk => $nval) {
            if ($nk < 5) {
                ?>
								    <li class="" style="<?php if ($nval->IsView==1){echo "background-color:#80808036";}?>"> <a target="_blank"
								href="<?= Url::base().'/wall-post/'.$nval->wallPost->Slug.'?Nid='.$nval->NotificationId;?>">
									<span class="notiLabel"><img
										src="<?=($nval->wallPost->employer->UserTypeId==2 || $nval->wallPost->employer->UserTypeId==5)?(($nval->wallPost->employer->PhotoId!=0)?$url.$nval->wallPost->employer->photo->Doc:"$imageurl.images/user.png"):(($nval->wallPost->employer->LogoId!=0)?$url.$nval->wallPost->employer->logo->Doc:$imageurl."images/user.png");?>"
										style="width: 33px; height: 33px;" /></span> <span
									class="notiLabel"><?=$nval->wallPost->employer->Name;?></span> 
									<!--<span class="notiCount"><?=$nk+1;?></span>-->
									<p>
										<span class="noti_Description fullWidth"><?=$nval->wallPost->PostTitle;?> at <?=$nval->wallPost->Locationat;?></span><span
											class="status"><?=date("F j, Y, g:i a",strtotime($nval->OnDate));?></span>
									</p>
							</a></li>
									<?php
            }
        }
        ?>
								   <li class=""><a
								href="<?= Url::toRoute(['site/candidatenotification'])?>"><span
									class="notiLabel">View All</span></a></li>
								   <?php
    } else {
        ?>
								   <li class=""><a href="#"><span class="notiLabel">No
										Notification</span></a></li>
							<li class=""><a
								href="<?= Url::toRoute(['site/candidatenotification'])?>"><span
									class="notiLabel">View All</span></a></li>
								   <?php
    }
    ?>
								</ul>
					</div>
							  <?php
    $messagelist = $mess->getMessage(Yii::$app->session['Employeeid']);
    $messagelist1 = $mess->getMessagecount(Yii::$app->session['Employeeid']);
    $msgcount = count($messagelist1);
    ?>
					 <div class="top_user_message">

						<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown"> <b class="fa fa-envelope"></b> <span
							class="not">  <span class="total total-message"><?=$msgcount;?></span></span>
						</a>
						<ul class="dropdown-menu">
								   <?php
    if ($messagelist) {
        foreach ($messagelist as $mk => $mval) {
            if ($mc < 3) {
                ?>
									 <li class=""><a
								href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
										style="width: 33px; height: 33px;" /></span> <span
									class="notiLabel"><?=$mval['Name'];?></span>
									<p>
										<span class="noti_Description fullWidth">
										<?php if($mval['IsView']){?>
											<?=$mval['Last'].$mval['Message'];?>
											<?php }else{?>
											<strong><?=$mval['Last'].$mval['Message'];?></strong>
											<?php }?>
										</span><span
											class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
									</p>
							</a></li>
									 <?php
            }
            $mc ++;
        }
        ?>
									<li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
									class="notiLabel">View All Message</span>
									<p>
										<span class="status"></span>
									</p></a></li>
									<?php
    } else {
        ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
    }
    ?>
							   </ul>

					</div>


            	 <div class="top_user_message home_mbl-1"> 
						<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>" class="dropdown-toggle brdr  orange_bg new_style" style="font-size: 15px;"> <b class="fa fa-home"></b>     
						</a> 
				</div>
				<!--/.nav-collapse -->
				
						<div class="form-group right_main" id="no_dis_mbl1" style="margin-right:6px;     border-radius: 100px;">
						<?php echo SearchPeople::widget()?>
					</div>

			</div>
		</div>
	</div>
	</div>
	</div>

	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

		<div class="side_bar-main">
			<img style="width: 80px; height: 80px;"
				src="<?=Yii::$app->session['EmployeeDP'];?>" alt="" class="">
			<h2>  <?=Yii::$app->session['EmployeeName'];?></h2>
			<hr class="height-2">

			<ul>

				<li class=""><a href="<?= Url::toRoute(['wall/candidateprofile'])?>"> My
						Profile</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB Wall Post</a></li>
				<li class=""><a href="<?= Url::toRoute(['/myactivity'])?>"> My Activities</a></li>
						
							<hr class="height-2">
				<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
				<li><a href="<?= Url::toRoute(['site/matchingjobs'])?>">Matching Job</a></li>
				<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent Jobs</a></li>
				<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin Jobs </a></li>
				
					<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/bookmarkjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Employeeid']);?></a></li>
				<li class=""><a href="<?= Url::toRoute(['site/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobApplied']; ?>    </a></li>
				 
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/editprofile'])?>">
						Edit Profile</a></li>
				<li class=""><a href="<?= Url::toRoute(['site/employeechangepassword'])?>"> Change Password </a></li>
			
			
			<?php
    if (isset(Yii::$app->session['CampusStudent'])) {
        ?>
			<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Company Job
						For Campus</a></li>
			<?php
    }
    ?>
			<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/employeelogout'])?>">
						Logout </a></li>
			</ul>



			<div class="start_a_project">
				<a href="<?= Url::toRoute(['site/jobsearch'])?>"> Applied for new
					job</a>
			</div>

		</div>
	</div>


	<div class="menu_block" id="after_log">
		<div class="container">
			<div class="row">
				<div class="  position_relative">
					<div class="col-md-12">
						<nav class="navbar navbar-white navbar-fixed-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed"
									data-toggle="collapse" data-target="#navbar"
									aria-expanded="false" aria-controls="navbar">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
							</div>
							<div id="navbar" class="navbar-collapse collapse"
								aria-expanded="false">
								<ul class="nav navbar-nav float-left">
									<li><a href="<?= Url::toRoute(['site/userdashboard'])?>">Dashboard</a></li>
									<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
											Wall Post</a></li>
									<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
									<li><a href="<?= Url::toRoute(['site/matchingjobs'])?>">Matching Job</a></li>
									<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent
											Jobs</a></li>
									<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin Jobs
									</a></li>
									<li><a href="<?= Url::toRoute(['site/appliedjob'])?>"> Applied
											Jobs <span class="total"><?=Yii::$app->session['NoofjobApplied']; ?></span>
									</a></li>
									<li><a href="<?= Url::toRoute(['site/bookmarkjob'])?>">
											Bookmark Jobs <span class="total"><?=$apl->getTotalbookmark(Yii::$app->session['Employeeid']);?></span>
									</a></li>
										<li><a href="<?= Url::toRoute(['/wall/watchlist'])?>">
											Watchlist Jobs  
									</a></li>
										<?php
    if (isset(Yii::$app->session['CampusStudent'])) {
        ?>
			<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Company Job
											For Campus</a></li>
			<?php
    }
    ?>
								</ul>
							</div>
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php
} elseif (isset(Yii::$app->session['Employerid'])) {

    $totalcan = $apl->getTotalcandidate(Yii::$app->session['Employerid']);
    $totalcampus = $apl->getTotalcampus(Yii::$app->session['Employerid']);
    ?>
<div class="header-top" id="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
					<a class="logo_top" href="<?= Url::toRoute(['wall/mcbwallpost'])?>">
						<img src="<?=$imageurl;?>careerimg/logo.png" alt="Logo">
					</a>
				</div>

				<div class="col-lg-8  col-md-8 col-sm-8 col-xs-12 main-nav">
					<!-- Main Navigation -->

					<div class="top_user">
						<a href="#" class="dropdown-toggle brdr orange_bg new_style"
							onclick="openNav()"> <img style="width: 33px; height: 33px;"
							src="<?=Yii::$app->session['EmployerDP'];?>" alt=""
							class="img-responsive center-block "> <span><?=Yii::$app->session['EmployerName'];?> </span></a>
					</div>


					<div class="top_user_notification">
						<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown">
						  <?php
    $empnotification = [];
    if (isset(Yii::$app->view->params['employernotification'])) {
        $empnotification = Yii::$app->view->params['employernotification'];
        $emprnotcount = count(Yii::$app->view->params['employernotification']);
    }
    ?>
						 <b class="fa fa-bell orange"></b> <span class="not"> 
								<span class="total"><?=$emprnotcount;?></span>
						</span>
						</a>

						<ul class="dropdown-menu">
								   <?php
    if ($empnotification) {
        foreach ($empnotification as $nk1 => $nval1) {
            if ($nk1 < 3) {
                $exp = '';
                $link = 'searchcandidate';
                if ($nval1->user->experiences) {
                    $exp = 'Having' . $nval1->user->experiences[0]->Experience . ' Year Experience';
                } elseif ($nval1->user->UserTypeId == 4) {
                    $exp = 'Company';
                    $link = 'campusdetail';
                } else {
                    $exp = isset($nval1->user->educations[0]->course->CourseName) ? $nval1->user->educations[0]->course->CourseName : '';
                }
                ?>
								     <li class=""><a target="_blank"
								href="<?=($nval1->Type=="Like" || $nval1->Type=="Comment")?'#':Url::toRoute(['wall/'.$link,'userid'=>$nval1->UserId])?>">
									<span class="notiLabel"><img
										src="<?=($nval1->user->UserTypeId==2 || $nval1->user->UserTypeId==5)?(($nval1->user->PhotoId!=0)?$url.$nval1->user->photo->Doc:$imageurl."images/user.png"):(($nval1->user->LogoId!=0)?$url.$nval1->user->logo->Doc:$imageurl."images/user.png");?>"
										style="width: 33px; height: 33px;" /></span> <span
									class="notiLabel"><?=$nval1->user->Name;?> <?php if($nval1->Type=="Like"){ echo 'like your job post';}else if($nval1->Type=="Comment"){echo 'commented on your job post';}?></span>
									<p>
										<span class="noti_Description fullWidth">  <?=$exp;?> , <?=$nval1->user->City;?></span><span
											class="status"><?=date("F j, Y, g:i a",strtotime($nval1->OnDate));?></span>
									</p>
							</a></li>
									<?php
            }
        }
        ?>
								   <li class=""><a
								href="<?= Url::toRoute(['site/empnotification'])?>"><span
									class="notiLabel">View All</span></a></li>
								   <?php
    } else {
        ?>
								   <li class=""><a href="#"><span class="notiLabel">No
										Notification</span></a></li>
							<li class=""><a
								href="<?= Url::toRoute(['site/empnotification'])?>"><span
									class="notiLabel">View All</span></a></li>
								   <?php
    }
    ?>
								</ul>
					</div>

					<div class="top_user_message">
					   <?php
    $messagelist = $mess->getMessage(Yii::$app->session['Employerid']);
    $messagelist1 = $mess->getMessagecount(Yii::$app->session['Employerid']);
    $msgcnt = count($messagelist1);

    ?>
						 <a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown"> <b class="fa fa-envelope"></b> <span
							class="not">  <span class="total total-message"><?=$msgcnt;?></span></span>
						</a>
						<ul class="dropdown-menu">
								   <?php
    if ($messagelist) {
        foreach ($messagelist as $mk => $mval) {
            if ($mc < 3) {
                ?>
									 <li class=""><a
								href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
										style="width: 33px; height: 33px;" /></span> <span
									class="notiLabel"><?=$mval['Name'];?></span>
									<p>
										<span class="noti_Description fullWidth">
										<?php if($mval['IsView']){?>
											<?=$mval['Last'].$mval['Message'];?>
											<?php }else{?>
											<strong><?=$mval['Last'].$mval['Message'];?></strong>
											<?php }?>
										</span><span
											class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
									</p>
							</a></li>
									 <?php
            }
            $mc ++;
        }
        ?>
									  <li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
									class="notiLabel">View All Message</span></a></li>
									  <?php
    } else {
        ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
    }
    ?>
									 </ul>

					</div>
					
					
					   <div class="top_user_message home_mbl-1" id="no_dis_mbl1"> 
				 
									<a href="<?= Url::toRoute(['campus/jobbycampus'])?>"
									class="dropdown-toggle  orange_bg new_style"> <b
										class="fa fa-university orange"></b> <br>  
								</a>
						</a> 
				</div>
				
		 
						 <div class="top_user_message home_mbl-1"> 
						<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>" class="dropdown-toggle brdr  orange_bg new_style" style="font-size: 15px;"> 
						<b class="fa fa-home"></b>   
						</a> 
				</div>
				
				
					<div class="form-group right_main" id="no_dis_mbl1" style="margin-right:6px;     border-radius: 100px;">
						<?php echo SearchPeople::widget()?>
					</div>
					
					
					

				</div>
				<!--/.nav-collapse -->

			</div>
		</div>
	</div>
	</div>
	</div>






	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

		<div class="side_bar-main">
			<img style="width: 80px; height: 80px;"
				src="<?=Yii::$app->session['EmployerDP'];?>" alt="" class="">
			<h2><?=Yii::$app->session['EmployerName'];?> </h2>
	 <?php $emp_name = preg_replace('/\s+/', '', Yii::$app->session['EmployerName']); ?>
	<hr class="height-2">
			<ul>
				<li class=""><a
					href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".Yii::$app->session['Employerid']?>">
						My Profile</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
						Wall Post</a></li>
				<li class=""><a
					href="<?= Url::toRoute(['wall/companyprofile/'])?><?=Yii::$app->session['EmployeeName'];?>">
						MCB Profile</a></li>
				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['site/companyprofileeditpage'])?>"> Edit
						Profile</a></li>
				<li class=""><a
					href="<?= Url::toRoute(['site/employerchangepassword'])?>"> Change
						Password </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/hirecandidate'])?>">
						Hire Candidate </a></li>
				<li class=""><a href="<?= Url::toRoute(['team/findteam'])?>"> Hire
						Team </a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/campuspost'])?>">
						Campus Post </a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/jobbycampus'])?>">
						Campus List </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/candidateapplied'])?>">Applied Candidate <?=$totalcan;?></a></li>
				<li class=""><a href="<?= Url::toRoute(['site/campusapplied'])?>">Applied Campus  <?=$totalcampus;?></a></li>
				<li class=""><a href="<?= Url::toRoute(['site/companypost'])?>">
						Create Post </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"> My
						Post Jobs </a></li>
				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['site/candidatebookmarked'])?>"> Bookmark
						Jobs </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/candidateapplied'])?>">
						Applied Jobs</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/employerlogout'])?>">
						Logout </a></li>

			</ul>



			<div class="start_a_project">
				<a href="<?= Url::toRoute(['site/companypost'])?>"> Post a new Job</a>
			</div>

		</div>
	</div>

	<div class="menu_block" id="after_log">
		<div class="container">
			<div class="row">
				<div class="  position_relative">
					<div class="col-md-12">
						<nav class="navbar navbar-white navbar-fixed-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed"
									data-toggle="collapse" data-target="#navbar"
									aria-expanded="false" aria-controls="navbar">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
							</div>
							<div id="navbar" class="navbar-collapse collapse"
								aria-expanded="false">
								<ul class="nav navbar-nav float-left">
									<li><a href="<?= Url::toRoute(['site/empdashboard'])?>">Dashboard</a></li>
									<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>">MCB Wall
											Post</a></li>
									<li><a href="<?= Url::toRoute(['site/hirecandidate'])?>">Hire
											Candidate</a></li>
									<li><a href="<?= Url::toRoute(['team/findteam'])?>">Hire Team</a></li>
									<li><a href="<?= Url::toRoute(['campus/campuspost'])?>"> Campus
											Post </a></li>
									<li><a href="<?= Url::toRoute(['site/candidateapplied'])?>">
											Applied Candidate <span class="total"><?=$totalcan;?></span>
									</a></li>
									<li><a href="<?= Url::toRoute(['site/campusapplied'])?>">
											Applied Campus <span class="total"><?=$totalcampus;?></span>
									</a></li>
									<li><a href="<?= Url::toRoute(['site/yourpost'])?>"> My Post
											Jobs </a></li>
									<li><a href="<?= Url::toRoute(['site/companypost'])?>"> Create
											New Post </a></li>
								</ul>
							</div>
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php
} elseif (isset(Yii::$app->session['Campusid'])) {
	 
    $messagelist = $mess->getMessage(Yii::$app->session['Campusid']);
    $messagelist1 = $mess->getMessagecount(Yii::$app->session['Campusid']);
    $msgcnt = count($messagelist1);

    ?>
    <div class="header-top" id="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
					<a class="logo_top" href="<?= Url::toRoute(['wall/mcbwallpost'])?>">
						<img src="<?=$imageurl;?>careerimg/logo.png" alt="Logo">
					</a>
				</div>

				<div class="col-lg-8  col-md-8 col-sm-8 col-xs-12 main-nav">
					<!-- Main Navigation -->

					<div class="top_user">
						<a href="#" class="dropdown-toggle brdr orange_bg new_style"
							onclick="openNav()"> <img style="width: 33px; height: 33px;"
							src="<?=Yii::$app->session['CampusDP'];?>" alt=""
							class="img-responsive center-block "> <span><?=Yii::$app->session['CampusName'];?></span></a>
					</div>


					<div class="top_user_notification">
						<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown"> <b class="fa fa-bell orange"></b> <span
							class="not">  
							<span class="total"><?=count($campusnotification);?></span></span>
						</a>
						<ul class="dropdown-menu">
								<?php
    if ($campusnotification) {
        foreach ($campusnotification as $nk1 => $nval1) {
            if ($nk1 < 3) {
                ?>
								     <li class=""><a target="_blank" href="<?=$nval1['link'];?>">
									<span class="notiLabel"><img src="<?=$nval1['image'];?>"
										style="width: 33px; height: 33px;" /></span> <span
									class="notiLabel"><?=$nval1['name'];?></span> <span
									class="notiCount"><?=$nk1+1;?></span>
									<p>
										<span class="noti_Description fullWidth">  <?=$nval1['title'];?> , <?=$nval1['address'];?></span><span
											class="status"><?=date("F j, Y, g:i a",strtotime($nval1['date']));?></span>
									</p>
							</a></li>
									<?php
            }
        }
        ?>
								   <li class=""><a
								href="<?= Url::toRoute(['site/campusnotification'])?>"><span
									class="notiLabel">View All</span></a></li>
								   <?php
    } else {
        ?>
								   <li class=""><a href="#"><span class="notiLabel">No
										Notification</a></li>
							<li class=""><a
								href="<?= Url::toRoute(['site/campusnotification'])?>"><span
									class="notiLabel">View All</span></a></li>
								   <?php
    }
    ?>
							</ul>
					</div>
				
					<div class="top_user_message">
						<a href="<?= Url::toRoute(['message/index']);?>" class="orange_bg">
							<b class="fa fa-envelope"></b> <span class="not"> <span class="total total-message"><?=$msgcnt;?></span>
						</a>
					</div>


	             <div class="top_user_message home_mbl-1"> 
						<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>" class="dropdown-toggle brdr  orange_bg new_style"> <b class="fa fa-home"></b>   
						</a> 
				</div>
				
				
				   <div class="top_user_message home_mbl-1"> 
						<a href="<?= Url::toRoute(['/site/mcbwallpost'])?>" class="dropdown-toggle brdr  orange_bg new_style"> <b class="fa fa-home"></b>   
						</a> 
				</div>
				
				
				 <div class="form-group right_main" id="no_dis_mbl1" style="margin-right:6px;     border-radius: 100px;">
						<?php echo SearchPeople::widget()?>
					</div>
				
				</div>
				<!--/.nav-collapse -->
         	
			</div>
		</div>
	</div>
	</div>
	</div>

	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

		<div class="side_bar-main">
			<img style="width: 80px; height: 80px;"
				src="<?=Yii::$app->session['CampusDP'];?>" alt="" class="">
			<h2><?=Yii::$app->session['CampusName'];?> </h2>
			<hr class="height-2">
			<ul>

				<li class=""><a href="<?= Url::toRoute(['campus/campusprofile'])?>">
						My Profile</a></li>
				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['wall/mcbwallpost/'])?><?=Yii::$app->session['CampusName'];?>">
						MCB Wall Post</a></li>
				<li class=""><a href="<?= Url::toRoute(['wall/campusprofile'])?>">
						MCB Profile</a></li>
				<hr class="height-2">
				<li class=""><a
					href="<?= Url::toRoute(['campus/campusprofileedit'])?>"> Edit
						Profile</a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/changepwd'])?>">
						Change Password </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/jobsearch'])?>"> Job
						Search </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/yourpost'])?>"> My
						Post Jobs </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/bookmarkedjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Campusid']);?></a></li>
				<li class=""><a href="<?= Url::toRoute(['campus/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobAppliedc'];?>    </a></li>
				<li><a href="<?= Url::toRoute(['campus/studentlist'])?>">Student
						List</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['campus/campuslogout'])?>">
						Logout </a></li>
			</ul>



			<div class="start_a_project">
				<a href="<?= Url::toRoute(['campus/createpost'])?>"> Post a new Job</a>
			</div>

		</div>
	</div>

	<div class="menu_block" id="after_log">
		<div class="container">
			<div class="row">
				<div class="  position_relative">
					<div class="col-md-12">
						<nav class="navbar navbar-white navbar-fixed-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed"
									data-toggle="collapse" data-target="#navbar"
									aria-expanded="false" aria-controls="navbar">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
							</div>
							<div id="navbar" class="navbar-collapse collapse"
								aria-expanded="false">
								<ul class="nav navbar-nav float-left">
									<li><a href="<?= Url::toRoute(['campus/dashboard'])?>">Dashboard</a></li>
									<li><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
											Wall Post</a></li>
									<li><a href="<?= Url::toRoute(['campus/jobsearch'])?>">Search
											Company Post</a></li>
									<li><a href="<?= Url::toRoute(['campus/appliedcompany'])?>">Applied Company</a></li>
									<li><a href="<?= Url::toRoute(['campus/appliedjob'])?>">
											Applied Job Post<span class="total"><?=Yii::$app->session['NoofjobAppliedc'];?></span>
									</a></li>
									<li><a href="<?= Url::toRoute(['campus/yourpost'])?>">My Post</a></li>
									<li><a href="<?= Url::toRoute(['campus/createpost'])?>">Create
											Campus Post</a></li>
									<li><a href="<?= Url::toRoute(['campus/studentlist'])?>">Student
											List</a></li>
								</ul>
							</div>
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php
} elseif (isset(Yii::$app->session['Teamid'])) {
    ?>
    <div class="header-top" id="head">
		<div class="container">
			<div class="row">
				<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
					<a class="logo_top" href="<?= Url::toRoute(['wall/mcbwallpost'])?>">
						<img src="<?=$imageurl;?>careerimg/logo.png" alt="Logo">
					</a>
				</div>

				<div class="col-lg-8  col-md-8 col-sm-8 col-xs-12 main-nav">
					<!-- Main Navigation -->

					<div class="top_user">
						<a href="#" class="dropdown-toggle brdr orange_bg new_style"
							onclick="openNav()"> <img style="width: 33px; height: 33px;"
							src="<?=Yii::$app->session['TeamDP'];?>" alt=""
							class="img-responsive center-block "> <span><?php echo Yii::$app->session['TeamName']; ?></span></a>
					</div>



					<div class="top_user_message">

						<a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown"> <b class="fa fa-bell"></b> <span
							class="not">  <span class="total"></span></span>
						</a>
						<ul class="dropdown-menu">
						</ul>
					</div>

					<div class="top_user_message">
					   <?php
    $messagelist = $mess->getMessage(Yii::$app->session['Teamid']);
    $messagelist1 = $mess->getMessagecount(Yii::$app->session['Teamid']);
    $msgcnt = count($messagelist1);

    ?>
						 <a href="#" class="dropdown-toggle brdr  orange_bg new_style"
							data-toggle="dropdown"> <b class="fa fa-envelope"></b> <span
							class="not">  <span class="total"><?=$msgcnt;?></span></span>
						</a>
						<ul class="dropdown-menu">
								   <?php
    if ($messagelist) {
        foreach ($messagelist as $mk => $mval) {
            if ($mc < 3) {
                ?>
									 <li class=""><a
								href="<?= Url::toRoute(['message/index','from'=>$mval['From'],'to'=>$mval['To']])?>">
									<span class="notiLabel"><img src="<?=$mval['Photo'];?>"
										style="width: 33px; height:33px;" /></span> <span
									class="notiLabel"><?=$mval['Name'];?></span>
									<p>
										<span class="noti_Description fullWidth">
										<?php if($mval['IsView']){?>
											<?=$mval['Last'].$mval['Message'];?>
											<?php }else{?>
											<strong><?=$mval['Last'].$mval['Message'];?></strong>
											<?php }?>
										</span><span
											class="status"><?=date("F j, Y, g:i a",strtotime($mval['OnDate']));?></span>
									</p>
							</a></li>
									 <?php
            }
            $mc ++;
        }
        ?>
									  <li class=""><a href="<?= Url::toRoute(['message/index'])?>"><span
									class="notiLabel">View All Message</span></a></li>
									  <?php
    } else {
        ?>
								   <li class=""><a href="#"><span class="notiLabel">No Message</span></a></li>
								   <?php
    }
    ?>
									 </ul>

					</div>
					
						 <div class="top_user_message home_mbl-1"> 
						<a href="<?= Url::toRoute(['site/job-by-company'])?>" class="dropdown-toggle brdr  orange_bg new_style" style="font-size: 15px;">
						    <b class="fa fa-briefcase orange"></b>     
						</a> 
				</div>
					
					
						 <div class="top_user_message home_mbl-1"> 
						<a href="<?= Url::toRoute(['wall/mcbwallpost'])?>" class="dropdown-toggle brdr  orange_bg new_style" style="font-size: 15px;"> <b class="fa fa-home"></b>     
						</a> 
				</div>
							<div class="form-group right_main" id="no_dis_mbl1" style="margin-right:6px;     border-radius: 100px;">
						<?php echo SearchPeople::widget()?>
					</div>

				</div>
				<!--/.nav-collapse -->

			</div>
		</div>
	</div>
	</div>
	</div>

	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

		<div class="side_bar-main">
			<img style="width: 80px; height: 80px;"
				src="<?=Yii::$app->session['TeamDP'];?>" alt="" class="">
			<h2>  <?=Yii::$app->session['TeamName'];?>  </h2>
			<hr class="height-2">
			<ul>

				<li class=""><a href="<?= Url::toRoute(['team/teamprofile'])?>"> My
						Profile</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['wall/mcbwallpost'])?>"> MCB
						Wall Post</a></li>
				<!--<li class=""><a
					href="<?= Url::toRoute(['wall/teamprofile/'])?>/<?=Yii::$app->session['TeamName'];?>">
						MCB Profile</a></li>-->
				<li class=""><a href="<?= Url::toRoute(['/myactivity'])?>"> My Activities</a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['team/editprofile'])?>">
						Edit Profile</a></li>
				<li class=""><a href="<?= Url::toRoute(['team/changepwd'])?>">
						Change Password </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['site/jobsearch'])?>"> Job
						Search </a></li>
				<li class=""><a href="<?= Url::toRoute(['site/recentjob'])?>">
						Recent Jobs </a></li>
				<li><a href="<?= Url::toRoute(['site/walkin'])?>"> Walkin Jobs </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['team/bookmarkedjob'])?>">  Bookmark Jobs <?=$apl->getTotalbookmark(Yii::$app->session['Teamid']);?></a></li>
				<li class=""><a href="<?= Url::toRoute(['team/appliedjob'])?>">  Applied Jobs  <?=Yii::$app->session['NoofjobAppliedt'];?>    </a></li>
				<hr class="height-2">
				<li class=""><a href="<?= Url::toRoute(['team/teamlogout'])?>">
						Logout </a></li>
			</ul>



			<div class="start_a_project">
				<a href="<?= Url::toRoute(['site/jobsearch'])?>"> Applied for new
					job</a>
			</div>

		</div>
	</div>


	<div class="menu_block" id="after_log">
		<div class="container">
			<div class="row">
				<div class="  position_relative">
					<div class="col-md-12">
						<nav class="navbar navbar-white navbar-fixed-top">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed"
									data-toggle="collapse" data-target="#navbar"
									aria-expanded="false" aria-controls="navbar">
									<span class="sr-only">Toggle navigation</span> <span
										class="icon-bar"></span> <span class="icon-bar"></span> <span
										class="icon-bar"></span>
								</button>
							</div>
							<div id="navbar" class="navbar-collapse collapse"
								aria-expanded="false">
								<ul class="nav navbar-nav float-left">
									<li><a href="<?= Url::toRoute(['team/dashboard'])?>">Dashboard</a></li>
									<li><a href="<?= Url::toRoute(['site/index'])?>">My Career Bugs</a></li>
									<li><a href="<?= Url::toRoute(['site/teamjobsearch'])?>">Team Jobs</a></li>
									<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
									<li><a href="<?= Url::toRoute(['site/matchingjobs'])?>">Matching Job</a></li>
									<li><a href="<?= Url::toRoute(['site/recentjob'])?>">Recent
											Jobs</a></li>
									<li><a href="<?= Url::toRoute(['team/appliedjob'])?>"> Applied
											Jobs <span class="total"><?=Yii::$app->session['NoofjobAppliedt'];?></span>
									</a></li>
									<li><a href="<?= Url::toRoute(['team/bookmarkedjob'])?>">
											Bookmark Jobs <span class="total"><?=$apl->getTotalbookmark(Yii::$app->session['Teamid']);?></span>
									</a></li>
										<li><a href="<?= Url::toRoute(['/wall/watchlist'])?>">
											Watchlist Jobs 
									</a></li>
								</ul>
							</div>
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>

 <!---------------------------------------------- Header End ---------------------------------------------->

	<!---------------------- Container Start ------------------------>
<?php
if (Yii::$app->session->hasFlash('error') || Yii::$app->session->hasFlash('success')) {
    if (Yii::$app->session->hasFlash('error')) {
        $flashimage = $imageurl . "images/error.png";
    } elseif (Yii::$app->session->hasFlash('success')) {
        $flashimage = $imageurl . "images/success.png";
    }
    ?>
		  <div class="modal add-resume-modal" style="display: block;"
		id="alertbox" tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="input-group image-preview form-group "
						style="width: 100%; height: auto; text-align: center;">
						<img src="<?=$flashimage;?>" style="width: 70px;" />
                            <?= Alert::widget() ?>
							<input type="button" value="OK" aria-hidden="true"
							data-dismiss="alert" class="btn btn-lg btn-primary btn-block"
							style="width: 100px; margin: auto;"
							onclick="$('#alertbox').hide();$('.modal-backdrop').hide();" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-backdrop  in"></div>
<?php
}
?>
<?php echo Loader::widget()?>
        <?= $content ?>
<!---------------------- Container End ------------------------>




	<!---------------------- Footer Start ------------------------>
	<div id="footer">
		<!-- Footer -->
		<div class="container">
			<!-- Container -->
			<div class="row">
				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- Text Widget -->
					<?php
    $aboutus = FooterAboutus::find()->one();
    ?>
						<h6 class="widget-title"><?=$aboutus->Heading;?></h6>
					<div class="textwidget">
							<?=htmlspecialchars_decode($aboutus->Content);?>
						</div>
				</div>
				<!-- Text Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12  footer-widget">
					<!-- Footer Menu Widget -->
					<h6 class="widget-title">Hot Categories</h6>
					<div class="footer-widget-nav">
						<ul>
								 <?php
        $htcat = $indu->getHotcategory();
        if ($htcat) {
            foreach ($htcat as $hk => $hval) {
                ?>
								<li><a
								href="<?= Url::toRoute(['site/jobsearch','JobCategoryId'=>$hval->IndustryId])?>"><?=$hval->IndustryName;?></a></li>
							  <?php
            }
        }
        ?>
							</ul>
					</div>
				</div>
				<!-- Footer Menu Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- Recent Tweet Widget -->
						<?php
						$thirdblock = common\models\FooterThirdBlock::find()->one();
    ?>
						<h6 class="widget-title"><?=$thirdblock->Heading;?></h6>
					<div class="footer-widget-nav">
						<ul>
							<li><a href="<?= Url::toRoute(['site/index']);?>">Home</a></li>
						
						 	<li><a href="<?= Url::toRoute(['site/walkin']);?>"> Walkin Jobs</a></li>
									<li><a href="<?= Url::toRoute(['site/recentjob']);?>"> Recent Jobs</a></li>	
						 
							<li><a href="<?= Url::toRoute(['site/login']);?>">Post a Resume</a></li>
							<li><a href="<?= Url::toRoute(['site/feedback']);?>">Feedback</a></li>
							<li><a href="<?= Url::toRoute(['content/privacy']);?>">Privacy
									Policy</a></li>
							<li><a href="<?= Url::toRoute(['content/terms']);?>">Terms &
									Condition</a></li>
							<li><a href="<?= Url::toRoute(['content/faq']);?>">Faq</a></li>
							<li><a href="<?= Url::toRoute(['site/blog']);?>" target="_blank">Blog</a></li>
						</ul>
					</div>
				</div>
				<!-- Recent Tweet Widget -->

				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 footer-widget">
					<!-- News Leter Widget -->
					<div id="contact_details">
					<?php
    $contactus = common\models\FooterContactus::find()->one();
    ?>
					  <h6 class="widget-title"><?=$contactus->Heading;?></h6> 
					     <?=htmlspecialchars_decode($contactus->Content);?>
					  </div>
					<h6 class="widget-title sig1">Singin For news Letter</h6>

					<div class="single-widget">
						<input autocomplete="off" type="email" id="news_email" type="text"
							class="col-xs-15 subscribeInputBox"
							placeholder="Your email address">
						<button
							class="col-xs-7 btn btn-theme-secondary subscribeBtn reset-padding"
							onclick="newsletter();">Subscribe</button>
						<div class="clearfix"></div>
					</div>

					<p>Register now to get updates on latest Vacancy.</p>

				</div>
				<!-- News Leter Widget -->
				<div class="clearfix"></div>
			</div>

			<div id="footer">
				<!-- Footer -->
				<div class="container">
					<!-- Container -->
					<div class="row">
					 <?php
    $socialicon = common\models\SocialIcon::find()->one();
    ?>
					   <ul class="list-inline social-buttons">
						 <?php
    if ($socialicon->IsTwitter == 1) {
        ?>
                        <li><a href="<?=$socialicon->TwitterLink;?>"
								target="_blank"><i class="fa fa-twitter"></i></a></li>
						<?php
    }
    if ($socialicon->IsFacebook == 1) {
        ?>
                        <li><a href="<?=$socialicon->FacebookLink;?>"
								target="_blank"><i class="fa fa-facebook"></i></a></li>
						<?php
    }
    if ($socialicon->IsLinkedin == 1) {
        ?>
                        <li><a href="<?=$socialicon->LinkedinLink;?>"
								target="_blank"><i class="fa fa-linkedin"></i></a></li>
						<?php
    }
    if ($socialicon->IsGoogleplus == 1) {
        ?>
                        <li><a href="<?=$socialicon->GoogleplusLink;?>"
								target="_blank"><i class="fa fa-google-plus"></i></a></li>
						<?php
    }
    ?>
                    </ul>

					</div>
				</div>
				<!-- Container -->
			</div>




			<div class="developed_by_e-developed_technology">
				<div class="row">
					<div class="col-md-6">
			   <?php
    $copyright = common\models\FooterCopyright::find()->one();
    echo htmlspecialchars_decode($copyright->Content);
    ?>
					
			   </div>
					<div class="col-md-6">
			   <?php
	$dblock = common\models\FooterDevelopedblock::find()->one();
    echo htmlspecialchars_decode($dblock->Content);
    ?>
					
			   </div>
				</div>
			</div>

		</div>
		<!-- Container -->
	</div>
	<!-- Footer -->

	<!---------------------- Footer End ------------------------->

<?php $this->endBody() ?>

<script type="text/javascript">
 function initAutocomplete() {
		var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
      }

</script>
	<!-- Map -->
	<script
		src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete&key=AIzaSyD9v82zwcqWdBEkhRQXmJt33FKcDfHRg-A"></script>
	<!-- Map -->
	<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
<?php
if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Campusid']) || isset(Yii::$app->session['Teamid'])) {
    ?>
$(document).ready(function(){
		
		setInterval(function(){ 
			$.ajax({url:mainurl+"site/getmessagecount",dataType:'HTML',
               success:function(result)
            	{
					$('.total-message').html(result);
            	}
        	});
		}, 5000);
	});
<?php }?>	
$(function(){
 <?php
$sp = array(
    'searchcandidate',
    'searchcompany',
    'searchteam',
    'searchcampus'
);
if (in_array(Yii::$app->controller->action->id, $sp)) {
    if (isset($_GET['lc']) && $_GET['lc'] == 1) {
        ?>
 if(isMobile==true){$('#mobileleavecomment').slideToggle();}else{$('#leavecomment').slideToggle();}$('.comm').toggleClass('active');$('#quickupdate').toggle();$('.profile').toggleClass('active');
 <?php
    }
}
?>
});

/*------------ Show / Hide Text ------------*/
$(document).ready(function() {
	var showChar = 475;
	var ellipsestext = "...";
	var moretext = "Show more";
	var lesstext = "less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
/*------------------------------------------*/
</script>
</body>
</html>
<?php $this->endPage() ?>