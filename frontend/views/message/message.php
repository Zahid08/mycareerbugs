<?php
$this->title = 'Message';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';

if(isset(Yii::$app->session['Employerid']))
{
    $empid=Yii::$app->session['Employerid'];
}
elseif(isset(Yii::$app->session['Employeeid']))
{
    $empid=Yii::$app->session['Employeeid'];
    
}
elseif(isset(Yii::$app->session['Teamid']))
{
    $empid=Yii::$app->session['Teamid'];
}
?>

<div id="wrapper"><!-- start main wrapper -->
	 
    <!-- Begin page content -->
    <div class="container page-content">
    <div class="row">
      <div class="tile tile-alt" id="messages-main">
      <div class="ms-menu">
          <div class="list-group lg-alt">
			<?php
            if($allfriend)
            {
                foreach($allfriend as $key=>$value)
                {
            ?>
              <a class="list-group-item media" href="#" onclick="message(<?=$value['From'];?>,<?=$value['To'];?>);">
                  <div class="pull-left">
                      <img src="<?=$value['Photo'];?>" alt="" class="img-avatar">
                  </div>
                  <div class="media-body">
                      <small class="list-group-item-heading"><?=$value['Name'];?></small>
                      <small class="list-group-item-text c-gray"><?=$value['Position'];?></small>
					  <small class="list-group-item-text icon" onclick="return delefrnd(<?=$value['From'];?>,<?=$value['To'];?>);"><i class="fa fa-close"></i></small>
                  </div>
              </a>
              <?php
                }
            }
			?>
			
          </div>
      </div>

      <div class="ms-body">
         <div id="messagebox"></div> 

		<div class="msb-reply">
              <textarea placeholder="What's on your mind..." id="Messages"></textarea>
              <button id="messagesb"><i class="fa fa-paper-plane-o"></i></button>
          </div>
      </div>
      
      
      
      </div>
    </div>
    </div>
</div>

<script type="text/javascript">
  function clearchat(from,to,empid)
  {
	var r=confirm("Are you sure you want to clear chat?");
	if (r==true) {
  $.ajax({url:"<?= Url::toRoute(['message/clearchat'])?>?from="+from+"&to="+to+"&empid="+empid,
			success:function(result)
			{
				 message(from,to);$('#Messages').focus();
			}
	 });
	}
  }
  
  function delefrnd(from,to) {
    var r=confirm("Are you sure you want to Delete chat?");
	if (r==true) {
  $.ajax({url:"<?= Url::toRoute(['message/delchat'])?>?from="+from+"&to="+to+"&empid="+<?=$empid;?>,
			success:function(result)
			{
				 location.reload();
			}
	 });
	}
  }
</script>