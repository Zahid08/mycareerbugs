<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';

if(isset(Yii::$app->session['Employerid']))
{
    $empid=Yii::$app->session['Employerid'];
    $mylogo=($from->LogoId!=0)?$url.$from->logo->Doc:$imageurl.'images/user.png';
    
}
elseif(isset(Yii::$app->session['Employeeid']))
{
    $empid=Yii::$app->session['Employeeid'];
    $mylogo=($from->PhotoId!=0)?$url.$from->photo->Doc:$imageurl.'images/user.png';
    
}
elseif(isset(Yii::$app->session['Teamid']))
{
    $empid=Yii::$app->session['Teamid'];
    $mylogo=($from->PhotoId!=0)?$url.$from->photo->Doc:$imageurl.'images/user.png';
}
elseif(isset(Yii::$app->session['Campusid']))
{
    $empid=Yii::$app->session['Campusid'];
    $mylogo=($from->PhotoId!=0)?$url.$from->photo->Doc:$imageurl.'images/user.png';
}

if($to->UserTypeId==2 || $to->UserTypeId==5)
{
    $logo=($to->PhotoId!=0)?$url.$to->photo->Doc:$imageurl.'images/user.png';
}
else
{
    $logo=($to->LogoId!=0)?$url.$to->logo->Doc:$imageurl.'images/user.png';
}

?>
 
        <div class="action-header clearfix">
              <div class="visible-xs" id="ms-menu-trigger">
                  <i class="fa fa-bars"></i>
              </div>
              
              <div class="pull-left hidden-xs">
                  <img src="<?=$logo;?>" alt="" class="img-avatar m-r-10">
                  <div class="lv-avatar pull-left">
                      
                  </div>
                  <span><?=$to->Name;?></span>
              </div>
               
              <ul class="ah-actions actions">
                  <li>
                      <a href="#" onclick="clearchat(<?=$from->UserId;?>,<?=$to->UserId;?>,<?=$empid;?>);">
                          <i class="fa fa-trash"></i>
                      </a>
                  </li>
                 
              </ul>
          </div>
          
        <?php
        if($messagelist)
        {
            foreach($messagelist as $mk=>$mv)
            {
                if($mv['MessageFrom']==$empid)
                {
        ?>
        <div class="message-feed media right">
              <div class="pull-right">
                  <img src="<?=$mylogo;?>" alt="" class="img-avatar">
              </div>
              <div class="media-body">
                  <div class="mf-content">
                     <?=$mv['Message'];?>
                  </div>
                  <small class="mf-date"><i class="fa fa-clock-o"></i><?=date('d/m/Y',strtotime($mv['OnDate'])).' at '.date('h:i A',strtotime($mv['OnDate']));?></small>
              </div>
        </div>
        <?php
        }
        else
        {
            
            ?>
            <div class="message-feed media">
              <div class="pull-left">
                  <img src="<?=$logo;?>" alt="" class="img-avatar">
              </div>
              <div class="media-body">
                  <div class="mf-content">
                     <?=$mv['Message'];?>
                  </div>
                  <small class="mf-date"><i class="fa fa-clock-o"></i><?=date('d/m/Y',strtotime($mv['OnDate'])).' at '.date('h:i A',strtotime($mv['OnDate']));?></small>
              </div>
        </div>
        <?php
        }
        ?>
        <?php
            }
        }
        ?>
          
<div style="height:60px;"></div>

           