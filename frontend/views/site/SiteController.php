<?php
namespace frontend\controllers;

use common\models\AllUser;
use common\models\AppliedJob;
use common\models\AppliedJobRequest;
use common\models\Blog;
use common\models\BlogComment;
use common\models\CampNotification;
use common\models\Cities;
use common\models\City;
use common\models\CompanyWallPost;
use common\models\ContactUs;
use common\models\Course;
use common\models\Documents;
use common\models\Education;
use common\models\EmpNotification;
use common\models\EmployeeSkill;
use common\models\Experience;
use common\models\Feedback;
use common\models\FooterAboutus;
use common\models\FooterContactus;
use common\models\FooterCopyright;
use common\models\FooterDevelopedblock;
use common\models\FooterThirdBlock;
use common\models\HrContact;
use common\models\Industry;
use common\models\JobAlert;
use common\models\JobCategory;
use common\models\JobRelatedSkill;
use common\models\JobRelevant;
use common\models\JobShift;
use common\models\LanguagesOpts;
use common\models\Likes;
use common\models\Location;
use common\models\Mail;
use common\models\Messages;
use common\models\NaukariQualData;
use common\models\NaukariSpecialization;
use common\models\NewsLetter;
use common\models\Notification;
use common\models\OtherSalaryDetail;
use common\models\PeoplesayBlock;
use common\models\Plan;
use common\models\PlanAssign;
use common\models\PlanRecord;
use common\models\Position;
use common\models\PostJob;
use common\models\Skill;
use common\models\SocialIcon;
use common\models\SpecialQualification;
use common\models\States;
use common\models\Total;
use common\models\WhiteCategory;
use common\models\WhiteSkill;
use common\models\WhiteRole;
use common\models\PostJobWSkill;
use common\models\PostJobWRole;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\BlogCategory;
use common\models\BlogLikeActivity;


use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\SignupForm;

use Yii;
use kartik\mpdf\Pdf;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\db\Expression;
use yii2tech\spreadsheet\Spreadsheet;
use common\models\Feed;
$basepath = str_replace('frontend', 'backend', Yii::$app->basePath);

Yii::$app->params['uploadPath'] = $basepath . '/imageupload/';

/**
 * Site controller
 */
class SiteController extends Controller

{

    /**
     *
     * @inheritdoc
     *
     */
    public function behaviors()

    {
        return [

            'access' => [

                'class' => AccessControl::className(),

                'only' => [
                    'logout',
                    'signup'
                ],

                'rules' => [

                    [

                        'actions' => [
                            'profile',
                            'view',
                            'reqcad',
                            'signup',
                            'index',
                            'jobseach',
                            'employersregister',
                            'companyprofileeditpage',
                            'resetpassword',
                            'applyjob',
                            'error',
                            'get-course-list',
                            'jobalert'
                        ],

                        'allow' => true,

                        'roles' => [
                            '?'
                        ]
                    ],

                    [

                        'actions' => [
                            'logout',
                            'error'
                        ],

                        'allow' => true,

                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ],

            'verbs' => [

                'class' => VerbFilter::className(),

                'actions' => [

                    'logout' => [
                        'post'
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * @inheritdoc
     *
     */
    public function actions()

    {
        return [
            'auth' => [

                'class' => 'yii\authclient\AuthAction',

                'successCallback' => [
                    $this,
                    'oAuthSuccess'
                ],

                'successUrl' => Url::to([
                    'usersocial'
                ])
            ],

            'captcha' => [

                'class' => 'yii\captcha\CaptchaAction',

                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ]
        ];
    }

    public function oAuthSuccess($client)
    {

        // get user data from client
        $userAttributes = $client->getUserAttributes();

        // var_dump($userAttributes);

        // die();
		
		

        if ($_GET['authclient'] == 'facebook') {

            $name = $userAttributes['name'];

            $id = $userAttributes['id'];

            $email = $userAttributes['email'];

            $gender = $userAttributes['gender'];
			
        } else if ($_GET['authclient'] == 'google') {

            $name = $userAttributes['name'];

            $id = $userAttributes['id'];

            $email = $userAttributes['email'];

        } else if ($_GET['authclient'] == 'linkedin') {

            $email = $userAttributes['email'];

            $name = $userAttributes['first_name'] . ' ' . $userAttributes['last_name'];
        }

        $client = $_GET['authclient'];

        $session = Yii::$app->session;

        $session->open();

        $userlist = new AllUser();

        $count = $userlist->find()
            ->where([
            'Email' => $email,
            'IsDelete' => 0,
            'VerifyStatus' => 1
        ])
            ->andWhere([
            '!=',
            'UserTypeId',
            4
        ])
            ->count();
		
        if ($count > 0) {

            $rest = $userlist->find()
                ->where([
                'Email' => $email,
                'IsDelete' => 0,
                'VerifyStatus' => 1
            ])
                ->andWhere([
                '!=',
                'UserTypeId',
                4
            ])
                ->one();

            if ($rest->UserTypeId == 2) {

                Yii::$app->session['EmployeeEmail'] = $rest->Email;

                Yii::$app->session['Employeeid'] = $rest->UserId;

                Yii::$app->session['EmployeeName'] = $rest->Name;

                if ($rest->PhotoId != 0) {

                    $url = '/backend/web/';

                    $pimage = $url . $rest->photo->Doc;
                } else {

                    $pimage = '/images/user.png';
                }

                $appliedjob = new AppliedJob();

                $noofjobapplied = $appliedjob->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsDelete' => 0
                ])
                    ->count();

                Yii::$app->session['NoofjobApplied'] = $noofjobapplied;

                Yii::$app->session['EmployeeDP'] = $pimage;
            } elseif ($rest->UserTypeId == 3) {

                Yii::$app->session['Employerid'] = $rest->UserId;

                Yii::$app->session['EmployerName'] = $rest->Name;

                Yii::$app->session['EmployerEmail'] = $email;

                $employerone = $userlist->find()
                    ->where([
                    'UserId' => $rest->UserId
                ])
                    ->one();

                if ($employerone->LogoId != 0) {

                    $url = '/backend/web/';

                    $pimage = $url . $employerone->logo->Doc;
                } else {

                    $pimage = '/images/user.png';
                }

                Yii::$app->session['EmployerDP'] = $pimage;
            } elseif ($rest->UserTypeId == 5) {

                Yii::$app->session['Teamid'] = $rest->UserId;

                Yii::$app->session['TeamName'] = $rest->Name;

                Yii::$app->session['TeamEmail'] = $rest->Email;

                if ($rest->PhotoId != 0) {

                    $url = '/backend/web/';

                    $pimage = $url . $rest->photo->Doc;
                } else {

                    $pimage = '/images/user.png';
                }

                $appliedjob = new AppliedJob();

                $noofjobapplied = $appliedjob->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Teamid'],
                    'AppliedJob.IsDelete' => 0
                ])
                    ->count();

                Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;

                Yii::$app->session['TeamDP'] = $pimage;
            }
        } else {
			Yii::$app->cache->flush();
		
			/*Save User Details*/
			if ($_GET['authclient'] == 'facebook' && !empty($userAttributes['id'])) {
				$model = new AllUser();
				$model->EntryType = 'Fresher';
				$model->UserTypeId = 2;
				$model->Ondate = date('Y-m-d');
				$model->VerifyKey = 'CB' . time();
				$model->VerifyStatus = 1;
				$model->Country = 'India';
				$model->fb_id = $userAttributes['id']; 
				$model->IsExp = 0;
				$model->Name = $name;
				$model->Email = $email;
				$model->Gender = "N/A";
				$model->Password = "N/A";
				$model->Address = "N/A";
				$model->State = "N/A";
				$model->City = "N/A";
				$model->PinCode = "0";
				$model->MobileNo = "0";
				$model->Password = "N/A";
				$model->language = "N/A";
				
				if($model->save()){
					 
					Yii::$app->session['Employeeid'] = $model->UserId;

                    Yii::$app->session['EmployeeName'] = $model->Name;

                    Yii::$app->session['EmployeeEmail'] = $model->Email;
					
                    Yii::$app->session['SocialEmail'] = $model->Email;
					
				}else{
					echo "<pre>User : ";
					print_r($model->getErrors());
					die();
				}
				
				
			}else if($_GET['authclient'] == 'google' && !empty($userAttributes['id'])){
				$model = new AllUser();
				$model->EntryType = 'Fresher';
				$model->UserTypeId = 2;
				$model->Ondate = date('Y-m-d');
				$model->VerifyKey = 'CB' . time();
				$model->VerifyStatus = 1;
				$model->Country = 'India';
				$model->google_id = $userAttributes['id']; 
				$model->IsExp = 0;
				$model->Name = $name;
				$model->Email = $email;
				$model->Gender = "N/A";
				$model->Password = "N/A";
				$model->Address = "N/A";
				$model->State = "N/A";
				$model->City = "N/A";
				$model->PinCode = "0";
				$model->MobileNo = "0";
				$model->ConfirmPassword = "N/A";
				$model->language = "N/A";
				
				if($model->save()){
					 
					Yii::$app->session['Employeeid'] = $model->UserId;

                    Yii::$app->session['EmployeeName'] = $model->Name;

                    Yii::$app->session['EmployeeEmail'] = $model->Email;
					
                    Yii::$app->session['SocialEmail'] = $model->Email;
					
				}else{
					echo "<pre>User : ";
					print_r($model->getErrors());
					die();
				}
				
			}
			
			return $this->redirect([
                    'wall/mcbwallpost'
                ]);
		
           /* Yii::$app->session['SocialEmail'] = $email;

            Yii::$app->session['SocialName'] = $name;

            Yii::$app->session['SocialGender'] = $gender;*/
        }

        // do some thing with user data. for example with $userAttributes['email']
    }

    public function actionUsersocial()

    {

        // echo Yii::$app->session['referrer'];
        if (isset(Yii::$app->session['SocialEmail'])) {

            return $this->redirect([
                'editprofile'
            ]);
        } else {

             return $this->redirect([
                    'wall/mcbwallpost'
                ]);
        }
    }

    public function setPassword($password1)

    {
        $password1 = "admin";

        $pass = $this->password_hash = Yii::$app->security->generatePasswordHash($password1);

        echo $pass;
        die();
    }

    public function actionIndex($id = NULL)
    {
        $ttl = Total::find()->where([
            'id' => 1
        ])->one();

        $ttl->updateCounters([
            'totalvisit' => 1
        ]);

        $postjob = new PostJob();
        $jobalert = new JobAlert();
        $user = new AllUser();
        $allpost = CompanyWallPost::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

       $allhrs = AllUser::find()
        ->select(['AllUser.*', 'COUNT(PostJob.EmployerId) AS total_jobs'])
        ->join('LEFT JOIN', PostJob::tableName(), 'PostJob.EmployerId=AllUser.UserId')
        ->where([
            'PostJob.IsDelete' => 0,
            'AllUser.EntryType' => 'HR'
        ])
        ->groupBy('PostJob.EmployerId')
        ->orderBy(['total_jobs' => SORT_DESC])
        
            ->limit(10)
            ->all();

        if (isset(Yii::$app->session['Employeeid'])) {

            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])->one();
        }

        if (isset(Yii::$app->session['Campusid'])) {

            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {

            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $postfor = array(
                'Team',
                'Both'
            );
        } else {

            $postfor = array(
                'Candidate',
                'Team',
                'Both'
            );
        }

        // Recent Job
        if (isset(Yii::$app->request->post()['indexsearch'])) {

            $keyname = Yii::$app->request->post()['keyname'];

            $indexlocation = Yii::$app->request->post()['indexlocation'];

            $experience = Yii::$app->request->post()['experience'];

            $salary = Yii::$app->request->post()['salary'];

            $sklist = array();

            $skilllist = Skill::find()->where([
                'like',
                'Skill',
                $keyname
            ])->all();

            foreach ($skilllist as $sk => $sv) {

                $sklist[] = $sv->SkillId;
            }

            if ($salary != '') {

                $salaryrange = str_replace(' Lakhs', "", $salary);
            } else {
                $salaryrange = '';
            }
            return $this->redirect([
                'jobsearch',
                'indexsearch' => 1,
                'keyname' => $keyname,
                'indexlocation' => $indexlocation,
                'experience' => $experience,
                'salary' => $salary
            ]);
        } elseif ($jobalert->load(Yii::$app->request->post())) {

            if ($jobalert->save(false)) {

                Yii::$app->session->setFlash('success', "Successfully jobalert applied");

                return $this->redirect([
                    'index'
                ]);
            } else {

                Yii::$app->session->setFlash('error', "There is some error please try again");

                return $this->redirect([
                    'index'
                ]);
            }
        } elseif ($user->load(Yii::$app->request->post())) {

            $docmodel = new Documents();
            $image = UploadedFile::getInstance($user, 'CVId');
            if ($image) {
                $image_id = $docmodel->imageUpload($image, 'cv');
            } else {
                $image_id = 0;
            }
            $user->CVId = $image_id;
            if ($user->save(false)) {
                Yii::$app->session->setFlash('success', "Successfully CV uploaded");
                return $this->redirect([
                    'index'
                ]);
            } else {
                Yii::$app->session->setFlash('error', "There is some error please try again");
                return $this->redirect([
                    'index'
                ]);
            }
        } else {
            $alljob = $postjob->find()
                ->where([
                'IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor,
                'TopJob' => 1
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ]);
        }
        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);
        if (isset(Yii::$app->request->get()['perpage'])) {
            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {
            $pages->defaultPageSize = 5;
        }
        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        // Top Job
        $topjob = PostJob::getTopJob($postfor);
        // hot categories
        $hotcategory = PostJob::getHotCategoryJob($postfor);
        // top job Opening
        $topjobopening = PostJob::getTopJob($postfor, PostJob::OPENING);

        // companies who posted job

        $companylogo = $postjob->find()
            ->select([
            'CompanyName',
            'LogoId',
            'Website',
            'EmployerId'
        ])
            ->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor
        ])
            ->orderBy([
            'JobId' => SORT_DESC
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        $allemployer = AllUser::find()->select([
            'Name',
            'HiringCompaniesLogoId',
            'UserId'
        ])
            ->where([
            'UserTypeId' => 3,
            'IsDelete' => 0,
            'EntryType' => 'Company',
            'ShowInFront' => 1
        ])
            ->all();

        $peoplesayblock = new PeoplesayBlock();

        $pb = $peoplesayblock->find()->one();

        // all feedback

        $fdbk = new Feedback();

        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        // walkin job

        $today = date('Y-m-d');

        $walkin = PostJob::find()->where([
            'IsWalkin' => 1,
            'PostFor' => $postfor,
            'IsDelete' => 0,
            'JobStatus' => 0
        ])
            ->andWhere([
            ">=",
            "WalkinTo",
            $today
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        $allindustry = Industry::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'IndustryName' => 'SORT_ASC'
        ])
            ->all();
        $allcity = City::find()->where([
            'ShowInFront' => 1
        ])
            ->limit(7)
            ->all();

        $mostpopular = Industry::find()->where([
            'ShowInFront' => 1,
            'IsDelete' => 0
        ])
            ->limit(6)
            ->all();

        $mostpopular1 = Industry::find()->where([
            'TopCompany' => 1,
            'IsDelete' => 0
        ])
            ->limit(6)
            ->all();

        $topconsultancy = AllUser::find()->where([
            'IsDelete' => 0,
            'UserTypeId' => 3,
            'EntryType' => 'Consultancy',
            'ShowInFront' => 1
        ])
            ->limit(10)
            ->all();

        $topcompany = array();

        foreach ($mostpopular1 as $mk => $mval) {

            $topcompany[$mval->IndustryId]['category'] = $mval->IndustryName;

            $topcompany[$mval->IndustryId]['categoryid'] = $mval->IndustryId;

            $cmp = PostJob::find()->where([
                'JobStatus' => 0,
                'IsDelete' => 0,
                'JobCategoryId' => $mval->IndustryId,
                'PostFor' => $postfor
            ])
                ->groupBy([
                'CompanyName'
            ])
                ->all();

            foreach ($cmp as $ck => $cval) {

                $topcompany[$mval->IndustryId]['companyname'][$ck] = $cval->CompanyName;
            }
        }

        $this->layout = 'main';
     
        return $this->render('index', [
            'alljob' => $alljob,
            'pages' => $pages,
            'topjob' => $topjob,
            'hotcategory' => $hotcategory,
            'allcompany' => $companylogo,
            'peoplesayblock' => $pb,
            'allfeedback' => $allfeedback,
            'topjobopening' => $topjobopening,
            'jobalert' => $jobalert,
            'allemployers' => $allemployer,
            'walkin' => $walkin,
            'allindustry' => $allindustry,
            'allcity' => $allcity,
            'mostpopular' => $mostpopular,
            'topcompany' => $topcompany,
            'topconsultancy' => $topconsultancy,
            'user' => $user,
            'allpost' => $allpost,
            'allhrs' => $allhrs
        ]);
    }

    public function actionCities()

    {

        // contactus block
        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;
		
		$postfor = array(
                'Candidate',
                'Both'
            );

		$cityData = ArrayHelper::map(PostJob::find()->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
			'PostFor' => $postfor
        ])->groupBy(['Location'])->all(), 'Location', 'Location');
		$cityList = array();
		if(!empty($cityData)){
			foreach($cityData as $city){
				if(!empty($city) && $city != 'None'){
					$cNames = explode(',', $city);
					if(!empty($cNames)){
						foreach($cNames as $city){
							$cityList[$city] = $city;
						}
					}
				}
			}
		}
		return $this->render('cities', [
            'allcity' => $cityList
        ]);
    }

    public function actionAlljob()
    {
        $postjob = new PostJob();
        $id = '';
        $keyword = '';
        if (isset($_GET['key'])) {
            $id = $_GET['key'];
        }
        if (isset($_GET['keyword'])) {
            $keyword = $_GET['keyword'];
        }

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        if ($keyword == 'popular') {

            $catname = Industry::find()->where([
                'IndustryId' => $id
            ])->one();

            // $alljob=$postjob->find()->where(['JobCategoryId'=>$id,'IsDelete'=>0,'JobStatus'=>0,'PostFor'=>'Candidate'])->orderBy(['OnDate'=>SORT_DESC]);

            $ctname = $catname->IndustryName;
        } elseif ($keyword == 'employer') {

            // $alljob=$postjob->find()->where(['EmployerId'=>$id,'IsDelete'=>0,'JobStatus'=>0,'PostFor'=>'Candidate'])->orderBy(['OnDate'=>SORT_DESC]);

            $ud = AllUser::find()->where([
                'UserId' => $id,
                'IsDelete' => 0,
                'UserTypeId' => 3
            ])->one();

            $ctname = $ud->Name;
        } elseif ($keyword == 'company') {

            // $alljob=$postjob->find()->where(['CompanyName'=>$id,'IsDelete'=>0,'JobStatus'=>0,'PostFor'=>'Candidate'])->orderBy(['OnDate'=>SORT_DESC]);

            $ctname = $id;
        } elseif ($keyword == 'city') {

            $in = Cities::find()->where([
                'CityName' => $id
            ])
                ->one();

            $ctname = $id;
            $id = $in->CityId;
			
            //$ctname = $id;
        } elseif ($keyword == 'industry') {

            $in = Industry::find()->where([
                'IndustryId' => $id
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->one();

            $ctname = $in->IndustryName;
        }

        // $pages = new Pagination(['totalCount' => $alljob->count()]);

        // $alljob = $alljob->offset($pages->offset)

        // ->limit($pages->limit)

        // ->all();

        // return $this->render('alljob',['model'=>$alljob,'pages' => $pages]);
		//echo $ctname;
		//echo $id;die;
		return $this->redirect(['jobsearch/'.Yii::$app->myfunctions->clean(trim($ctname))]);
        // return $this->redirect([
        //     'jobsearch',
        //     'alljobsearch' => 1,
        //     'keyword' => $keyword,
        //     'ID' => $id,
        //     'ctname' => $ctname
        // ]);
    }

    /**
     *
     * Displays homepage.
     *
     *
     *
     * @return mixed
     *
     */
    public function actionRecentjob()
    {
        $postjob = new PostJob();
        $jobalert = new JobAlert();
        if (isset(Yii::$app->session['Campusid'])) {
            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {
            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {
            $postfor = [
                'Team',
                'Both'
            ];
        } else {
            $postfor = [
                'Candidate',
                'Team',
                'Both'
            ];
        }

        // Recent Job

        $keyname = '';
        $indexlocation = '';
        $experience = '';
        $salary = '';
        $post = \Yii::$app->request->post();
        /*
         * echo "<pre>";
         * print_r(\Yii::$app->request->post());
         * die();
         */
        if ($post) {
            $keyname = Yii::$app->request->post()['keyname'];
            $indexlocation = Yii::$app->request->post()['indexlocation'];
            $experience = Yii::$app->request->post()['experience'];
            $salary = Yii::$app->request->post()['salary'];
			//echo '<pre>';
			//print_r($salary);die;
			if ($keyname != '') {
				
			   $roleJobId = ArrayHelper::map(PostJobWRole::find()->where([
                'or', ['LIKE', 'role', $keyname]
				])->all(), 'postjob_id', 'postjob_id');
				
				$skillJobId = ArrayHelper::map(PostJobWSkill::find()->where([
				'or', ['LIKE', 'skill', $keyname]
                ])->all(), 'postjob_id', 'postjob_id');
				
				$jobIds = array_merge($roleJobId, $skillJobId);
				
					
			}
            $sklist = [];
            $skilllist = [];
            if ($keyname != '') {
                $skilllist = Skill::find()->where([
                    'like',
                    'Skill',
                    $keyname
                ])->all();
            }
            foreach ($skilllist as $sk => $sv) {
                $sklist[] = $sv->SkillId;
            }
            if ($salary != '') {
                $salaryrange = str_replace(' Lakhs', "", $salary);
            } else {
                $salaryrange = '';
            }
            $alljob = $postjob->find()
                ->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor,
                //'TopJob' => 1
            ])
                ->joinWith([
                'jobRelatedSkills'
            ])
                ->andFilterWhere([
                'or',
                [
                    'like',
                    'JobTitle',
                    $keyname
                ],

                [
                    'like',
                    'CompanyName',
                    $keyname
                ],
				[
                    'PostJob.JobId' => $jobIds
                ],
				[
                    'JobRelatedSkill.SkillId' => $sklist
                ]
            ])
				 ->andFilterWhere([
				 'or',
					[
						'like',
						'Location',
						$indexlocation
					]
				 
				 ])
                ->andFilterWhere([
                'Experience' => $experience
            ])->andFilterWhere([
               'or',
				[
					"LIKE",
					"Salary",
					$salaryrange
				]
			])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ]); 

			
			
			
			
			
            
        } elseif ($jobalert->load(Yii::$app->request->post())) {
            if ($jobalert->save(false)) {
                Yii::$app->session->setFlash('success', "Successfully jobalert applied");
                return $this->redirect([
                    'index'
                ]);
            } else {
                Yii::$app->session->setFlash('error', "There is some error please try again");
                return $this->redirect([
                    'index'
                ]);
            }
        } else {

            $alljob = $postjob->find()
                ->where([
                'IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor,
                //'TopJob' => 1
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ]);
			/*echo '<pre>';
			print_r($postfor);die;*/
        }
		
		/*$alljob = $alljob->andWhere([ "or",
				[">=",
				"WalkinTo",
				$today],
				["WalkinTo" => NULL]
			]);*/

        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }

        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // Top Job

        $topjob = $postjob->find()
            ->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        // hot categories

        $hotcategory = $postjob->find()
            ->select([
            'count(*) as cnt',
            'Industry.IndustryName as CategoryName',
            'Industry.IndustryId as JobCategoryId'
        ])
            ->joinWith([
            'jobCategory'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor
        ])
            ->groupBy([
            'PostJob.JobCategoryId'
        ])
            ->all();

        // top job

        $topjobopening = $postjob->find()
            ->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'TopJob' => 1,
            'PostFor' => $postfor
        ])
            ->all();

        // contactus block

        // companies who posted job

        $companylogo = $postjob->find()
            ->select([
            'CompanyName',
            'LogoId',
            'Website'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        $allemployer = AllUser::find()->select([
            'Name',
            'LogoId'
        ])
            ->where([
            'UserTypeId' => 3,
            'IsDelete' => 0
        ])
            ->all();

        $peoplesayblock = new PeoplesayBlock();

        $pb = $peoplesayblock->find()->one();

        // all feedback

        $fdbk = new Feedback();

        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
				->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        $this->layout = 'main';
        /*
         * echo "<pre>";
         * print_r($alljob);
         * die();
         */
        return $this->render('recentjob', [
            'alljob' => $alljob,
            'pages' => $pages,
            'topjob' => $topjob,
            'hotcategory' => $hotcategory,
            'allcompany' => $companylogo,
            'peoplesayblock' => $pb,
            'allfeedback' => $allfeedback,
            'topjobopening' => $topjobopening,
            'jobalert' => $jobalert,
            'allemployers' => $allemployer,
            'keyname' => $keyname,
            'indexlocation' => $indexlocation,
            'experience' => $experience,
            'salary' => $salary
        ]);
    }

    public function actionWalkin()
    {
        $postjob = new PostJob();
        $jobalert = new JobAlert();
        if (isset(Yii::$app->session['Campusid'])) {
            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {

            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $postfor = array(
                'Team',
                'Both'
            );
        } else {

            $postfor = array(
                'Candidate',
                'Team',
                'Both'
            );
        }

        // Recent Job

        $keyname = '';
        $indexlocation = '';
        $experience = '';
        $salary = '';
		
        if (isset(Yii::$app->request->post()['indexsearch'])) {
			
            $keyname = trim(Yii::$app->request->post()['keyname']);

            $indexlocation = Yii::$app->request->post()['indexlocation'];
			if(!empty($indexlocation)){
				$indexlocation = explode(',',$indexlocation);
			}

            $experience = Yii::$app->request->post()['experience'];

            $salary = Yii::$app->request->post()['salary'];

            $sklist = array();
			if(!empty($keyname)){
				$skilllist = Skill::find()->where([
					'like',
					'Skill',
					$keyname
				])->all();
				foreach ($skilllist as $sk => $sv) {

					$sklist[] = $sv->SkillId;
				}
			}
            
            if ($salary != '') {
                $salaryrange =  str_replace(' Lakhs', "", $salary);
            } else {
                $salaryrange = '';
            }
			$jobIds = array();
			if ($keyname != '') {
				
			   $roleJobId = ArrayHelper::map(PostJobWRole::find()->where([
                'or', ['LIKE', 'role', $keyname]
				])->all(), 'postjob_id', 'postjob_id');
				
				$skillJobId = ArrayHelper::map(PostJobWSkill::find()->where([
				'or', ['LIKE', 'skill', $keyname]
                ])->all(), 'postjob_id', 'postjob_id');
				
				$jobIds = array_merge($roleJobId, $skillJobId);
			}
			
		
            $alljob = $postjob->find()
                ->where([
                'IsWalkin' => 1,
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->joinWith([
                'jobRelatedSkills'
            ])
                ->andFilterWhere([
                'or',

                [
                    'like',
                    'JobTitle',
                    $keyname
                ],

                [
                    'like',
                    'CompanyName',
                    $keyname
                ],
				[
                    'PostJob.JobId' => $jobIds
                ],				
                [
                    'JobRelatedSkill.SkillId' => $sklist
                ]
            ])
             ->andFilterWhere([
               'or',
				[
					"LIKE",
					"Salary",
					$salaryrange
				]
			])
                /* ->andWhere("'$today' between `WalkinFrom` and `WalkinTo`") */
                ->andFilterWhere([
				'or',
				[
                "OR LIKE",
                "Location",
                $indexlocation
				]
				
            ]) 
            ->andWhere([
                ">=",
                "WalkinTo",
                date('Y-m-d')
            ])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ]);	
            
    		if($experience=='Both'){
                    $alljob->andFilterWhere(['IsExp' =>2]);
            }else{
                      $alljob->andFilterWhere(['Experience' => $experience]);
            }
//             echo "<pre>";
// 		print_r($alljob->createCommand()->rawSql);die;
			
        } elseif ($jobalert->load(Yii::$app->request->post())) {

            if ($jobalert->save(false)) {

                Yii::$app->session->setFlash('success', "Successfully jobalert applied");

                return $this->redirect([
                    'index'
                ]);
            } else {

                Yii::$app->session->setFlash('error', "There is some error please try again");

                return $this->redirect([
                    'index'
                ]);
            }
        } else {

            if(isset(Yii::$app->request->get()['filtertoday'])){
                
                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])
                ->andWhere([
                    ">=",
                    "WalkinTo",
                    date('Y-m-d')
                ])
                ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } 
            else if(isset(Yii::$app->request->get()['filtertomorrow'])){
                
                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])

                ->andWhere([
                    ">=",
                    "WalkinTo",
                    date('Y-m-d', strtotime(date('Y-m-d'). ' + 1 days'))
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } 
            else if(isset(Yii::$app->request->get()['filterweek'])){

                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])

                ->andWhere([
                    ">=",
                    "WalkinTo",
                    date('Y-m-d')
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } 
            else if(isset(Yii::$app->request->get()['filtermonth'])){
                
                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])

                ->andWhere([
                    ">=",
                    "WalkinTo",
                    date("Y-m-01", strtotime(date('Y-m-d')))
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } 
            else if(isset(Yii::$app->request->get()['filternextmonth'])){
                
                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])
                ->andWhere([
                    ">=",
                    "WalkinTo",
                    date('Y-m-d', strtotime("+1 months", strtotime(date("Y-m-01", strtotime(date('Y-m-d'))))))
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } 
            else if(isset(Yii::$app->request->get()['filterdate'])){

                $today = Yii::$app->request->get()['filterdate'];

                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])

                ->andWhere([
                    ">=",
                    "WalkinTo",
                    $today
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            }
            else{
               
                $alljob = PostJob::find()->where([
                    'IsWalkin' => 1,
                    'PostFor' => $postfor,
                    'IsDelete' => 0,
                    'JobStatus' => 0
                ])

                ->andWhere([
                    ">=",
                    "WalkinTo",
                    date('Y-m-d')
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            }
			
        }

        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }
		/*echo '<pre>'; print_r($jobIds);
		print_r($alljob->createCommand()->rawSql);
        die();*/ 
        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
		/*echo '<pre>';
		print_r($alljob);die;*/			
        // Top Job

        $topjob = $postjob->find()
            ->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        // hot categories

        $hotcategory = $postjob->find()
            ->select([
            'count(*) as cnt',
            'Industry.IndustryName as CategoryName',
            'Industry.IndustryId as JobCategoryId'
        ])
            ->joinWith([
            'jobCategory'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor
        ])
            ->groupBy([
            'PostJob.JobCategoryId'
        ])
            ->all();

        // top job

        $topjobopening = $postjob->find()
            ->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'TopJob' => 1,
            'PostFor' => $postfor
        ])
            ->all();

        // companies who posted job

        $companylogo = $postjob->find()
            ->select([
            'CompanyName',
            'LogoId',
            'Website'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        $allemployer = AllUser::find()->select([
            'Name',
            'LogoId'
        ])
            ->where([
            'UserTypeId' => 3,
            'IsDelete' => 0
        ])
            ->all();

        $peoplesayblock = new PeoplesayBlock();

        $pb = $peoplesayblock->find()->one();

        // all feedback

        $fdbk = new Feedback();

        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        $this->layout = 'main';
		if(!empty($indexlocation)){
			$indexlocation = implode(',', $indexlocation);
		}
        return $this->render('walkin', [
            'alljob' => $alljob,
            'pages' => $pages,
            'topjob' => $topjob,
            'hotcategory' => $hotcategory,
            'allcompany' => $companylogo,
            'peoplesayblock' => $pb,
            'allfeedback' => $allfeedback,
            'topjobopening' => $topjobopening,
            'jobalert' => $jobalert,
            'allemployers' => $allemployer,
            'keyname' => $keyname,
            'indexlocation' => $indexlocation,
            'experience' => $experience,
            'salary' => $salary
        ]);
    }

    public function actionTopjob()

    {
        $postjob = new PostJob();

        if (isset(Yii::$app->session['Campusid'])) {

            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {

            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $postfor = array(
                'Team',
                'Both'
            );
        } else {

            $postfor = array(
                'Candidate',
                'Team',
                'Both'
            );
        }

        $alljob = PostJob::find()->where([
            'PostFor' => $postfor,
            'IsDelete' => 0,
            'JobStatus' => 0,
            'TopJob' => 1
        ])->orderBy([
            'OnDate' => SORT_DESC
        ]);

        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }

        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // companies who posted job

        $companylogo = $postjob->find()
            ->select([
            'CompanyName',
            'LogoId',
            'Website'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        $allemployer = AllUser::find()->select([
            'Name',
            'LogoId'
        ])
            ->where([
            'UserTypeId' => 3,
            'IsDelete' => 0
        ])
            ->all();

        $peoplesayblock = new PeoplesayBlock();

        $pb = $peoplesayblock->find()->one();

        // all feedback

        $fdbk = new Feedback();

        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // top job

        $topjobopening = $postjob->find()
            ->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'TopJob' => 1,
            'PostFor' => $postfor
        ])
            ->all();

        return $this->render('topjob', [
            'alljob' => $topjobopening
        ]);
    }

    public function actionKeysearch()

    {
        if (isset(Yii::$app->session['Campusid'])) {

            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {

            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $postfor = array(
                'Team',
                'Both'
            );
        } else {

            $postfor = array(
                'Candidate'
            );
        }

        $searchTerm = Yii::$app->request->get()['term'];

        $allskill = Skill::find()->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'Skill',
            $searchTerm
        ])
            ->all();

        $alljob = PostJob::find()->select([
            'JobTitle',
            'JobId'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->andWhere([
            'LIKE',
            'JobTitle',
            $searchTerm
        ])
            ->all();

        $allcom = PostJob::find()->select([
            'CompanyName',
            'JobId'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->andWhere([
            'LIKE',
            'CompanyName',
            $searchTerm
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        $data = array();

        foreach ($allskill as $key => $value) {

            $data[] = array(
                'id' => $value->SkillId,
                'value' => $value->Skill
            );
        }

        foreach ($alljob as $key => $value) {

            $data[] = array(
                'id' => $value->JobId,
                'value' => $value->JobTitle
            );
        }

        foreach ($allcom as $key => $value) {

            $data[] = array(
                'id' => $value->JobId,
                'value' => $value->CompanyName
            );
        }

        echo json_encode($data);
    }

    public function actionLocationsearch()

    {
        $searchTerm = Yii::$app->request->get()['term'];

        $alllocation = PostJob::find()->select([
            'Location',
            'JobId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'Location',
            $searchTerm
        ])
            ->groupBy([
            'Location'
        ])
            ->all();

        $data = array();

        foreach ($alllocation as $key => $value) {

            $data[] = array(
                'id' => $value->JobId,
                'value' => $value->Location
            );
        }

        echo json_encode($data);
    }

    public function actionCandidatekeysearch()

    {
        $searchTerm = Yii::$app->request->get()['term'];

        $allUser = AllUser::find()->select([
            'Name',
            'UserId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'Name',
            $searchTerm
        ])
            ->orderBy([
            'Name' => 'SORT_ASC'
        ])
            ->all();

        $allindustry = Industry::find()->select([
            'IndustryName',
            'IndustryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'IndustryName',
            $searchTerm
        ])
            ->orderBy([
            'IndustryName' => 'SORT_ASC'
        ])
            ->all();

        $alledu = Education::find()->select([
            'HighestQualification',
            'EducationId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'HighestQualification',
            $searchTerm
        ])
            ->all();

        $data = array();

        foreach ($allindustry as $key => $value) {

            $data[] = array(
                'id' => $value->IndustryId,
                'value' => $value->IndustryName
            );
        }

        foreach ($alledu as $key => $value) {

            $data[] = array(
                'id' => $value->EducationId,
                'value' => $value->HighestQualification
            );
        }
        foreach ($allUser as $key => $value) {

            $data[] = array(
                'id' => $value->UserId,
                'value' => $value->Name
            );
        }

        echo json_encode($data);
    }

    public function actionCandidatelocation()

    {
        $searchTerm = Yii::$app->request->get()['term'];

        $alledu = AllUser::find()->select([
            'State',
            'UserId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'State',
            $searchTerm
        ])
            ->groupBy([
            'State'
        ])
            ->all();

        $data = array();

        foreach ($alledu as $key => $value) {

            $data[] = array(
                'id' => $value->UserId,
                'value' => $value->State
            );
        }

        echo json_encode($data);
    }

    public function actionKeywordssearch()
    {
        if (isset(Yii::$app->session['Campusid'])) {
            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {
            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {
            $postfor = array(
                'Team',
                'Both'
            );
        } else {
            $postfor = array(
                'Candidate'
            );
        }
        $searchTerm = Yii::$app->request->get()['term'];
        $allskill = Skill::find()->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'Skill',
            $searchTerm
        ])
            ->all();

        $allrole = Position::find()->where([
            'IsDelete' => 0
        ])
            ->andWhere([
            'LIKE',
            'Position',
            $searchTerm
        ])
            ->all();

        $alljob = PostJob::find()->select([
            'JobId',
            'JobTitle'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->andWhere([
            'LIKE',
            'JobTitle',
            $searchTerm
        ])
            ->all();

        $allcom = PostJob::find()->select([
            'CompanyName',
            'JobId'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->andWhere([
            'LIKE',
            'CompanyName',
            $searchTerm
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        $data = array();

        foreach ($allskill as $key => $value) {
            $data[] = array(
                'id' => $value->SkillId,
                'value' => $value->Skill,
                'type' => 'Skill'
            );
        }

        foreach ($allrole as $key => $value) {
            $data[] = array(
                'id' => $value->PositionId,
                'value' => $value->Position,
                'type' => 'Position'
            );
        }
        foreach ($alljob as $key => $value) {
            $data[] = array(
                'id' => $value->JobId,
                'value' => $value->JobTitle,
                'type' => 'PostJob'
            );
        }
        foreach ($allcom as $key => $value) {
            $data[] = array(
                'id' => $value->JobId,
                'value' => $value->CompanyName,
                'type' => 'PostJob'
            );
        }

        echo json_encode($data);
    }

    public function actionJobdetail()
    {
		
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;
        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        $this->layout = 'layout';
        $JobId = Yii::$app->request->get()['JobId'];
        $postjob = new PostJob();
		
		$slug = (isset(Yii::$app->request->get()['alias'])?Yii::$app->request->get()['alias']:"");
		
        if (isset(Yii::$app->request->get()['Nid'])) {
            $nid = Yii::$app->request->get()['Nid'];
            $notification = Notification::find()->where([
                'NotificationId' => $nid
            ])->one();
            $notification->IsView = 1;
            $notification->save(false);
        }
        // no of view
        $postdetails = PostJob::find()->where([
            //'JobId' => $JobId,
			'Slug' => $slug,
            'JobStatus' => 0
        ])->one();
		
		if(empty($postdetails)){
			return $this->redirect([
						'jobsearch'
					]);
		}
		
		$postdetails->TotalView = $postdetails->TotalView + 1;
        $postdetails->save(false);
        $postdetail = $postjob->find()
            ->where([
            'JobId' => $postdetails->JobId,
            'JobStatus' => 0
        ])
            ->one();
        // simmilar post
		
        $sklist = array();
        foreach ($postdetail->jobRelatedSkills as $sk => $sv) {
            $sklist[] = $sv->SkillId;
        }
		
		$white_skills = ArrayHelper::map(PostJobWSkill::find()->where([
					'postjob_id' => $postdetails->JobId
				])->all(), 'skill_id', 'skill_id');
				
		if(!empty($white_skills)){
			$postjob_ids = ArrayHelper::map(PostJobWSkill::find()->where([
					'skill_id' => $white_skills])->andWhere(['not in','postjob_id',$postdetails->JobId])->limit(6)->all(), 'postjob_id', 'postjob_id');
				
			$simmilarpost = PostJob::find()->where([
				'PostJob.JobStatus' => 0,
				'PostJob.IsDelete' => 0,
				'PostJob.JobId' => $postjob_ids
			])
				->groupBy([
				'PostJob.JobId'
			])
				->orderBy([
				'PostJob.onDate' => SORT_DESC
			])
			->limit(6)
			->all();	
			
			
		}else{
			$simmilarpost = PostJob::find()->where([
				'PostJob.JobStatus' => 0,
				'PostJob.IsDelete' => 0
			])
				->joinWith([
				'jobRelatedSkills'
			])
				->andWhere([
				'JobRelatedSkill.SkillId' => $sklist
			])
				->groupBy([
				'PostJob.JobId'
			])
				->orderBy([
				'PostJob.onDate' => SORT_DESC
			])
				->all();
		}
				
        
        // simmilar post
        $alluser = new AllUser();
        if (isset(Yii::$app->session['Employeeid'])) {
            $alluser = $alluser->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])
                ->one();
        }
        $mailmodel = new Mail();
        /*
         * if(isset($_GET['share']) && $_GET['share']=='fb'){
         * return $this->render('fb_share_jobdetail',['mailmodel'=>$mailmodel,'allpost'=>$postdetail,'simmilar'=>$simmilarpost,'alluser'=>$alluser]);
         * }
         */
		/*Set OG Tags*/ 
		if(!empty($postdetail)){
			$this->view->params['meta_description'] = substr(strip_tags($postdetail->JobDescription),0,150);
			$title =$postdetail->JobTitle;
			if(!empty($postdetail->Location) && $postdetail->Location!='Null'){
				$title .= ', '.$postdetail->Location;
			}
			if(!empty($postdetail->Experience)){
				$title .= ', '.$postdetail->Experience.' Year';
			}
			$this->view->params['og_title'] = $title;
			$this->view->params['og_description'] = substr(strip_tags($postdetail->JobDescription),0,150);
			$jobUrl = Url::base().'/job/'.$postdetail->Slug;
			$this->view->params['og_url'] = $jobUrl;
			if ($postdetail->docDetail) {
				$url = Url::base().'/backend/web/';
				$doc = $url . $postdetail->docDetail->Doc;
				$this->view->params['og_image'] = $doc;
			}
			
		}
		
        return $this->render('jobdetail', [
            'mailmodel' => $mailmodel,
            'allpost' => $postdetail,
            'simmilar' => $simmilarpost,
            'alluser' => $alluser
        ]);
    }

    public function actionDeletecv($userid)
    {
        $alluser = AllUser::find()->where([
            'UserId' => $userid
        ])->one();

        $alluser->CVId = 0;

        $alluser->save(false);

        echo json_encode(1);
    }

    /**
     *
     * Logs in a user.
     *
     *
     *
     * @return mixed
     *
     */

    /*
     * public function actionLogin()
     *
     * {
     *
     * if (!Yii::$app->user->isGuest) {
     *
     * return $this->goHome();
     *
     * }
     *
     *
     *
     * $model = new LoginForm();
     *
     * if ($model->load(Yii::$app->request->post()) && $model->login()) {
     *
     * return $this->goBack();
     *
     * } else {
     *
     * return $this->render('login', [
     *
     * 'model' => $model,
     *
     * ]);
     *
     * }
     *
     * }
     */

    /**
     *
     * Logs out the current user.
     *
     *
     *
     * @return mixed
     *
     */
    public function actionLogout()

    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     *
     * Displays contact page.
     *
     *
     *
     * @return mixed
     *
     */
    public function actionContact()

    {
        $model = new ContactUs();

        $contactname = Yii::$app->request->get()['contact_name'];

        $contactnumber = Yii::$app->request->get()['contact_number'];

        $contactemail = Yii::$app->request->get()['contact_email'];

        $contactfrom = Yii::$app->request->get()['contactfrom'];

        $contactmessage = Yii::$app->request->get()['contact_message'];

        $model->ContactNumber = $contactnumber;

        $model->Email = $contactemail;

        $model->ContactFrom = $contactfrom;

        $model->Message = $contactmessage;

        $model->Name = $contactname;

        $model->OnDate = date('Y-m-d');

        if ($model->save(false)) {

            $msg = 'Thank you for contacting us. We will respond to you as soon as possible.';

            $to = $model->Email;

            $from = Yii::$app->params['adminEmail'];

            $subject = "Thank you";

            $html = "<html>

               <head>

               <title>Thank you</title>

               </head>

               <body>

               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#fff;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:250px;'></td>

                                   </tr>

                                   <tr>

                                       <td style='height:30px'></td>

                                   </tr>

                           <tr>

                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to 
                                       <a href='https://mycareerbugs.com' target='_blank'> My Career Bugs </a>

               <br><br>

               <span style='font-size:16px;line-height:1.5'>

                 <h3> Dear  $contactname, </h3>

Thank you for contacting us. We will respond to you as soon as possible.! <br>

Contact Number:  8240369924   

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

            $mail = new ContactForm();

            $mail->sendEmail($to, $from, $html, $subject);
        } else {

            $msg = 'There was an error sending email.';
        }

        echo json_encode($msg);
    }

    public function actionNewsletter()

    {
        $model = new NewsLetter();

        $news_email = Yii::$app->request->get()['news_email'];

        $cnt = $model->find()
            ->where([
            'Email' => $news_email
        ])
            ->one();

        if ($cnt) {

            $msg = "You have already signup for News Letter with this email";
        } else {

            $model->Email = $news_email;

            $model->IsDelete = 0;

            $model->OnDate = date('Y-m-d');

            if ($model->save(false)) {

                $to = $model->Email;

                $from = Yii::$app->params['adminEmail'];

                $subject = "News Letter";

                $html = "<html>

               <head>

               <title>News Letter</title>

               </head>

               <body>

               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:200px;'></td>

                                   </tr>

                                   <tr>

                                       <td style='height:30px'></td>

                                   </tr>

                           <tr>

                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='https://mycareerbugs.com' target='_blank'> My Career Bugs </a>

               <br><br>

               <span style='font-size:16px;line-height:1.5'>

                 <h3> Dear , $to</h3>

Thank you for connecting with us. <br>

Contact Number: 8240369924   

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                $msg = "Successfully Signup For News Letter";
            } else {

                $msg = "There is some error please try again";
            }
        }

        echo json_encode($msg);
    }

    /**
     *
     * Displays about page.
     *
     *
     *
     * @return mixed
     *
     */
    public function actionAbout()

    {
        return $this->render('about');
    }

    /**
     *
     * Signs user up.
     *
     *
     *
     * @return mixed
     *
     */
    public function actionSignup()

    {
        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {

            if ($user = $model->signup()) {

                if (Yii::$app->getUser()->login($user)) {

                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [

            'model' => $model
        ]);
    }

    /**
     *
     * Requests password reset.
     *
     *
     *
     * @return mixed
     *
     */
    public function actionRequestPasswordReset()

    {
        $model = new PasswordResetRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {

                Yii::$app->session->setFlash('success', 'An email has been sent to your mail-id. Please click on the link provided in the mail to reset your password.

In case you do not receive your password reset email shortly, Please check your spam folder also..');

                return $this->goHome();
            } else {

                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [

            'model' => $model
        ]);
    }

    /**
     *
     * Resets password.
     *
     *
     *
     * @param string $token
     *
     * @return mixed
     *
     *
     */

    // ------------------- ALL USER(Employers) Start -------------------//
    public function actionEmployerslogin()

    {
        if (! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Employeeid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $model = new AllUser();

            if ($model->load(Yii::$app->request->post())) {

                $password = md5($model->Password);

                $rest = $model->find()
                    ->where([
                    'Email' => $model->Email,
                    'Password' => $password,
                    'UserTypeId' => 3,
                    'IsDelete' => 0
                ])
                    ->one();

                if ($rest) {

                    if ($rest->VerifyStatus == 0) {

                        Yii::$app->session->setFlash('error', 'Please Check your mail for Email Verification.');

                        return $this->render('employerslogin', [
                            'model' => $model
                        ]);
                    } else {

                        $rest = $model->find()
                            ->where([
                            'Email' => $model->Email,
                            'Password' => $password,
                            'UserTypeId' => 3,
                            'IsDelete' => 0
                        ])
                            ->one();

                        $session = Yii::$app->session;

                        $session->open();

                        Yii::$app->session['Employerid'] = $rest->UserId;

                        Yii::$app->session['EmployerName'] = $rest->Name;

                        Yii::$app->session['EmployerEmail'] = $model->Email;

                        $employerone = $model->find()
                            ->where([
                            'UserId' => $rest->UserId
                        ])
                            ->one();

                        if ($employerone->LogoId != 0) {

                            $url = '/backend/web/';

                            $pimage = $url . $employerone->logo->Doc;
                        } else {

                            $pimage = '/images/user.png';
                        }

                        Yii::$app->session['EmployerDP'] = $pimage;

                        return $this->render('companyprofile', [
                            'employer' => $employerone
                        ]);
                    }
                } else {

                    Yii::$app->session->setFlash('error', "Wrong Email Or Password, Try Again!");

                    return $this->render('employerslogin', [
                        'model' => $model
                    ]);
                }
            } else {

                return $this->render('employerslogin', [
                    'model' => $model
                ]);
            }
        } else {

            if (isset(Yii::$app->session['Employerid'])) {

                return $this->redirect([
                    'companyprofile'
                ]);
            } else {

                return $this->redirect([
                    'index'
                ]);
            }
        }
    }

    public function actionEmployersregister()
    {
        if (! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Employeeid'])) {
            $cn = new FooterContactus();
            $footercontact = $cn->find()->one();
            Yii::$app->view->params['footercontact'] = $footercontact;
            // second block
            $allhotcategory = JobCategory::getHotJobCategory();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $model = new AllUser();

            $allindustry = ArrayHelper::map(Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all(), 'IndustryId', 'IndustryName');

            $docmodel = new Documents();

            if ($model->load(Yii::$app->request->post())) {
                
                if(!isset($model->captcha) && AllUser::TYPE_COMPANY == 3) {
                    
                    Yii::$app->session->setFlash('error', "Captcha field is mandatory.");

                    return $this->redirect([
                        'employersregister'
                    ]);
                }

                $count = $model->find()
                    ->where([
                    'Email' => $model->Email,
                    'IsDelete' => 0
                ])
                    ->count();

                if ($count > 0) {

                    Yii::$app->session->setFlash('error', "This Mail-id Already Exist.");

                    return $this->redirect([
                        'employersregister'
                    ]);
                } else {

                    // var_dump(Yii::$app->request->post());

                    $model->UserTypeId = AllUser::TYPE_COMPANY;
                    $model->Password = md5($model->Password);
                    $vkey = 'CB' . time();
                    $model->VerifyKey = $vkey;
                    $model->VerifyStatus = 1;
                    $model->Ondate = date('Y-m-d');
                    $logoid = 0;
                    if ($docmodel->load(Yii::$app->request->post())) {
                        $docmodel->image = \yii\web\UploadedFile::getInstance($docmodel, 'Doc');
                        if ($docmodel->image) {
							$logoid = Documents::handleImageUpload($docmodel, 'Doc', 'LogoId');
                            /*$image = UploadedFile::getInstance($docmodel, 'Doc');
                            $ext = end((explode(".", $image->name)));
                            $ppp = Yii::$app->security->generateRandomString();
                            $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";
                            $docmodel->Doc = 'imageupload/' . $ppp . ".{$ext}";
                            $docmodel->OnDate = date('Y-m-d');
                            $docmodel->From = 'photo';
                            if ($docmodel->save(false)) {
                                $docmodel->image->saveAs(Yii::$app->params['uploadPath'] . $ppp . '.' . $docmodel->image->extension);
                                $logoid = $docmodel->DocId;
                                $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
                                $thumb = $furl . 'thumb_' . $logoid . '.' . $ext;
                                if (file_exists($thumb)) {
                                    $document = Documents::find()->where([
                                        'DocId' => $logoid
                                    ])->one();
                                    $document->Doc = 'imageupload/thumb_' . $logoid . '.' . $ext;
                                    $document->save(false);
                                }
                            } else {
                                var_dump($docmodel->getErrors());
                                $logoid = 0;
                            }*/
                        }else{
							$logoid = 0;
						}
                    }
                    $model->LogoId = $logoid;
                    if ($model->save(false)) {
                        $model->assignFreePlan();
                    }
                    $name = $model->Name;
                    $to = $model->Email;
                    $from = Yii::$app->params['adminEmail'];
                    $subject = "Registration Success";

                    $html = "<html>

               <head>

               <title>Registration Success</title>

               </head>

               <body>

               <table style='width:600px;height:auto;margin:auto;font-family:arial;color:#fff;background:#fff;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:300px;'></td>

                                   </tr>

                                   <tr>

                                       <td style='height:10px'></td>

                                   </tr>

                           <tr>

                                        <td style='font-size:18px'>
                                        <h2 style='width:560px;font-weight:normal;background:#f16b22;padding:40px 20px;margin:auto; border-radius:10px'>
                                        Welcome to <a href='https://mycareerbugs.com' target='_blank' style='color: #6d136a; text-align: center; text-decoration: underline;'>
                                        My Career Bugs </a>

               <br>

               <span style='font-size:16px;line-height:1.5'>

                 <h3 style='margin: 18px 0 10px 0;font-size: 35px;'> Dear  $name, </h3>

                    Congratulations! You have been registered successfully on My Career Bugs!!  <br/> No 1 Job Portal <br/>

                    Contact Number:  8240369924   

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                    $mail = new ContactForm();

                    $mail->sendEmail($to, $from, $html, $subject);

                    // Yii::$app->session->setFlash('success', 'Check your email for EmailId Verification.');

                    $session = Yii::$app->session;

                    $session->open();

                    Yii::$app->session['Employerid'] = $model->UserId;

                    Yii::$app->session['EmployerName'] = $model->Name;

                    Yii::$app->session['EmployerEmail'] = $model->Email;

                    if ($model->LogoId != 0) {

                        $url = '/backend/web/';

                        $pimage = $url . $model->logo->Doc;
                    } else {

                        $pimage = '/images/user.png';
                    }

                    Yii::$app->session['EmployerDP'] = $pimage;

                    return $this->redirect([
                        'thankyou'
                    ]);
                }
            } else {

                return $this->render('employersregister', [
                    'model' => $model,
                    'industry' => $allindustry,
                    'docmodel' => $docmodel
                ]);
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionEmployersverifryemail($vkey)

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $alluser = new AllUser();

        $userone = $alluser->find()
            ->where([
            'VerifyKey' => $vkey
        ])
            ->one();

        if ($userone) {

            if ($userone->VerifyStatus == 0) {

                $userone->VerifyStatus = 1;

                $userone->save(false);

                $name = $userone->Name;

                $to = $userone->Email;

                $from = Yii::$app->params['adminEmail'];

                $subject = "Registration Success";

                $html = "<html>

               <head>

               <title>Registration Success</title>

               </head>

               <body>

               <table style='width:600px;height:auto;margin:auto;font-family:arial;color:#fff;background:#fff;text-align:center'>

                <tbody><tr>

                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:300px;'></td>

                   </tr>

                   <tr>

                       <td style='height:10px'></td>

                   </tr>

           <tr>

           <td style='font-size:18px'><h2 style='width:560px;font-weight:normal;background:#f16b22;padding:40px 20px;margin:auto;'>Welcome to <a href='https://mycareerbugs.com' target='_blank' style='color: #6d136a; text-align: center; text-decoration: underline;'> My Career Bugs </a>

               <br> 

               <span style='font-size:16px;line-height:1.5'>

                  <h3 style='margin: 18px 0 10px 0;font-size: 35px;'> Dear  $name, </h3>

                    Congratulations! You have been registered successfully on My Career Bugs!!! <br/> No 1 Job Portal <br/>

                Contact Number: 8240369924    

                   

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                Yii::$app->session->setFlash('success', 'Account Verified Successfully, Now You Can Login');
            } else {

                Yii::$app->session->setFlash('error', 'You are already Verified Your Account');
            }
        } else {

            Yii::$app->session->setFlash('error', 'Please check your activation link');
        }

        return $this->redirect([
            'employerslogin'
        ]);
    }

    public function actionEmployeeverifyemail($vkey)

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $alluser = new AllUser();

        $userone = $alluser->find()
            ->where([
            'VerifyKey' => $vkey
        ])
            ->one();

        if ($userone) {

            if ($userone->VerifyStatus == 0) {

                $userone->VerifyStatus = 1;

                $userone->save(false);

                $name = $userone->Name;

                $to = $userone->Email;

                $from = Yii::$app->params['adminEmail'];

                $subject = "Registration Success";

                $html = "<html>

               <head>

               <title>Registration Success</title>

               </head>

               <body>

             <table style='width:600px;height:auto;margin:auto;font-family:arial;color:#fff;background:#fff;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:300px;'></td>

                                   </tr>

                                   <tr>

                                       <td style='height:10px'></td>

                                   </tr>

                           <tr>

                                       <td style='font-size:18px'><h2 style='width:560px;font-weight:normal;background:#f16b22;padding:40px 20px;margin:auto; border-radius:10px'>Welcome to <a href='https://mycareerbugs.com' target='_blank' style='color: #6d136a; text-align: center; text-decoration: underline;'> My Career Bugs </a>

               <br>

               <span style='font-size:16px;line-height:1.5'>

                  <h3 style='margin: 18px 0 10px 0;font-size: 35px;'> Dear  $name, </h3>

                    Congratulations! You have been registered successfully on My Career Bugs!!! <br/> No 1 Job Portal <br/>

                    Contact Number:   8240369924   

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                Yii::$app->session->setFlash('success', 'Account Verified Successfully, Now You Can Login');
            } else {

                Yii::$app->session->setFlash('error', 'You are already Verified your Account');
            }
        } else {

            Yii::$app->session->setFlash('error', 'Wrong Activation link');
        }

        return $this->redirect([
            'login'
        ]);
    }

    public function actionCompanyprofile()

    {
        $emp_name = preg_replace('/\s+/', '', Yii::$app->session['EmployerName']);
        return $this->redirect([
            $emp_name . "-" . Yii::$app->session['Employerid']
        ]);
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $model = new AllUser();

            $employerid = Yii::$app->session['Employerid'];

            $employerone = $model->find()
                ->where([
                'UserId' => $employerid
            ])
                ->one();

            return $this->render('companyprofile', [
                'employer' => $employerone
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionMyprofile()
    {
        $this->layout = 'walllayout';
        $message = new Messages();

        $url_info = explode('/', Yii::$app->request->url);
        $id = '';
        $cmp_name = '';
        if (isset($url_info[1]) && ! empty($url_info[1])) {
            $companyary = explode('-', $url_info[1]);
            if (! empty($companyary)) {
                $cnt = count($companyary);
                $id = $companyary[$cnt - 1];
            }
            if (isset($companyary[0]) && ! empty($companyary[0])) {
                $cmp_name = $companyary[0];
            }
        }

        if (empty($id) || empty($cmp_name)) {
            throw new \yii\web\NotFoundHttpException();
            exit();
        }
        $pimage = '';
        $Employerid = '';
        $EmployerName = '';
        $sess_id = '';
        $user_loggenin = false;
        $user_loggenin_new = true;
        $employerone = array();
        if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Campusid']) || isset(Yii::$app->session['Teamid'])) {
            $user_loggenin = true;
        }
		
		
        if (isset(Yii::$app->session['Employerid'])) {
            $Employerid = Yii::$app->session['Employerid'];
            $sess_id = $Employerid;
        } elseif (isset(Yii::$app->session['Employeeid'])) {
            $Employerid = Yii::$app->session['Employeeid'];
            $sess_id = $Employerid;
        } elseif (isset(Yii::$app->session['Campusid'])) {
            $Employerid = Yii::$app->session['Campusid'];
            $sess_id = $Employerid;
        } elseif (isset(Yii::$app->session['Teamid'])) {
            $Employerid = Yii::$app->session['Teamid'];
            $sess_id = $Employerid;
        }
		
		if (! empty($id)) {
           $Employerid = $id;
        }


        

        if (! empty($Employerid)) {
            $model = new AllUser();
            $employerid = $Employerid;
            $employerone = $model->find()
                ->where([
                'UserId' => $employerid
            ])
                ->one();
            // echo "<pre>";print_r($employerone); die();
            if (count((array) $employerone) == 0) {
                throw new \yii\web\NotFoundHttpException();
                exit();
            }

            $EmployerName = $employerone->Name;
            // echo $employerone->UserTypeId;die('=');
            if ($employerone->UserTypeId == 2) {
                if ($employerone->PhotoId != 0) {
                    $url = '/backend/web/';
                    $pimage = $url . $employerone->photo->Doc;
                } else {
                    $pimage = $url . '/images/user.png';
                }
            } elseif ($employerone->UserTypeId == 3) {
                if ($employerone->LogoId != 0) {
                    $url = '/backend/web/';
                    $pimage = $url . $employerone->logo->Doc;
                } else {
                    $pimage = $url . '/images/user.png';
                }
                // echo $pimage;die('==');
            } elseif ($employerone->UserTypeId == 5) {

                if ($employerone->PhotoId != 0) {

                    $url = '/backend/web/';

                    $pimage = $url . $employerone->photo->Doc;
                } else {

                    $pimage = $url . '/images/user.png';
                }
            }
        }

        if ($user_loggenin_new) {

            $allnotification = array();

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => $Employerid,
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => $Employerid,
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $allpost = CompanyWallPost::find()->where([
                'IsDelete' => 0,
                'IsHide' => 0,
                'EmployerId' => $Employerid
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();

            $totalactive = PostJob::find()->where([
                'EmployerId' => $Employerid,
                'JobStatus' => 0,
                'IsDelete' => 0
            ])->count();

            $allskill = PostJob::find()->where([
                'EmployerId' => $Employerid,
                'JobStatus' => 0,
                'IsDelete' => 0,
                'IsHide' => 0
            ])->all();

            $skill = array();

            foreach ($allskill as $key => $value) {

                foreach ($value->jobRelatedSkills as $k => $v) {

                    $skill[$v->SkillId] = $v->skill->Skill;
                }
            }
            if ($message->load(\Yii::$app->request->post())) {
                $message->OnDate = date('Y-m-d H:i:s');
                $message->MessageFrom = $Employerid;
                $message->MessageTo = $id;
                $message->IsDelete = 0;
                $message->IsView = 0;
                if ($message->save()) {
                    \Yii::$app->session->setFlash('success', 'Message sent successfully');
                } else {
                    // var_dump($comment->getErrors());
                    \Yii::$app->session->setFlash('error', 'There is some error please try again.');
                }
                return $this->redirect([
                    '/' . $url_info[1]
                ]);
            }
            return $this->render('myprofile', [
                'sess_id' => $sess_id,
                'id' => $id,
                'user_loggenin' => $user_loggenin,
                'EmployerName' => $EmployerName,
                'pimage' => $pimage,
                'skill' => $skill,
                'totalactive' => $totalactive,
                'allpost' => $allpost,
                'employer' => $employerone,
                'message' => $message
            ]);
        } else {
            $this->layout = 'layout';
            return $this->render('myprofile_logout', [
                'pimage' => $pimage,
                'EmployerName' => $EmployerName,
                'id' => $id,
                'user_loggenin' => $user_loggenin,
                'employer' => $employerone
            ]);
        }
    }

    public function actionCompanyprofileeditpage()
    {
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;
        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        $this->layout = 'layout';
        $model = new AllUser();
        $allindustry = ArrayHelper::map(Industry::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'IndustryName' => 'SORT_ASC'
        ])
            ->all(), 'IndustryId', 'IndustryName');
        $docmodel = new Documents();
        $employerid = Yii::$app->session['Employerid'];
        $employerone = $model->find()
            ->where([
            'UserId' => $employerid
        ])
            ->one();
        $oldlogo = $employerone->LogoId;
        if (!empty(Yii::$app->request->post()) && $employerone->load(Yii::$app->request->post())) {
			
            $logoid = $oldlogo;
            if ($docmodel->load(Yii::$app->request->post())) {
               $docmodel->image = \yii\web\UploadedFile::getInstance($docmodel, 'Doc');
			   
                if ($docmodel->image) {
					$employerone->LogoId = Documents::handleImageUpload($docmodel, 'Doc', 'LogoId');
                    /*$image = UploadedFile::getInstance($docmodel, 'Doc');
                    $ext = end((explode(".", $image->name)));
                    $ppp = Yii::$app->security->generateRandomString();
                    $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";
                    $docmodel->Doc = 'imageupload/' . $ppp . ".{$ext}";
                    $docmodel->OnDate = date('Y-m-d');
                    $docmodel->From = 'photo';
                    if ($docmodel->save(false)) {
						//echo Yii::$app->params['uploadPath'] . $ppp . '.' . $docmodel->image->extension;die;
                        $docmodel->image->saveAs(Yii::$app->params['uploadPath'] . $ppp . '.' . $docmodel->image->extension);
                        $logoid = $docmodel->DocId;
                        $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
                        $thumb = $furl . 'thumb_' . $logoid . '.' . $ext;
                        if (file_exists($thumb)) {
                            $document = Documents::find()->where([
                                'DocId' => $logoid
                            ])->one();
                            $document->Doc = 'imageupload/thumb_' . $logoid . '.' . $ext;
                            $document->save(false);
                        }
					} else {
                        $logoid = 0;
                    }*/
                }
            }
            if (isset($_FILES['coverpic'])) {
                $uploads_dir = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/images/coverpic';
                $tmp_name = $_FILES["coverpic"]["tmp_name"];
                $name = $_FILES["coverpic"]["name"];
                move_uploaded_file($tmp_name, "$uploads_dir/$name");
                $employerone->coverpic = $name;
            }
           
			//$employerone->PhotoId = 
			$stateId = Yii::$app->request->post()['AllUser']['State'];
			$cityId = Yii::$app->request->post()['AllUser']['City'];
			$employerone->State = States::findOne(['StateId' => $stateId])->StateName;
            $employerone->City = City::findOne(['CityID' => $cityId])->CityName;;
            $employerone->save(false);
            if ($employerone->LogoId != 0) {
                $url = '/backend/web/';
                $pimage = $url . $employerone->logo->Doc;
            } else {
                $pimage = '/images/user.png';
            }
            Yii::$app->session['EmployerDP'] = $pimage;
            $emp_name = preg_replace('/\s+/', '', Yii::$app->session['EmployerName']);
            Yii::$app->session->setFlash('success', 'Profile Updated Successfully.');
            return $this->redirect([
                $emp_name . "-" . Yii::$app->session['Employerid']
            ]);
        } else {

            return $this->render('editcompanyprofilepage', [
                'employer' => $employerone,
                'industry' => $allindustry,
                'docmodel' => $docmodel
            ]);
        }
    }

    public function actionCovloc()

    {
        $imgtoppo = Yii::$app->request->get()['imgtp'];
        $imglfppo = Yii::$app->request->get()['imglf'];
        $cmid = Yii::$app->request->get()['cmid'];
        $alusr = AllUser::find()->where([
            'UserId' => $cmid
        ])->one();
        $alusr->data_top = $imgtoppo;
        $alusr->data_lft = $imglfppo;
        $alusr->update(false);

        return true;
    }

    public function actionYourpost()

    {
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';
			
			$postType = (isset(Yii::$app->request->get()['type'])?Yii::$app->request->get()['type']:"");

            $postjob = new PostJob();

            $allpost = $postjob->find()
                ->where([
                'EmployerId' => Yii::$app->session['Employerid']
            ])
			 ->andFilterWhere([
				'PostFor' => $postType
			 ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();

            return $this->render('yourpost', [
                'allpost' => $allpost
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionPostdetail($JobId, $pf)

    {
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            $allcourse = Course::find()->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $postjob = new PostJob();

            $allpost = $postjob->find()
                ->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'JobId' => $JobId
            ])
                ->one();

            return $this->render('postdetail', [
                'allpost' => $allpost,
                'pf' => $pf,
                'course' => $allcourse
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionClosestatus()

    {
        $jobid = Yii::$app->request->get()['jobid'];

        $postjob = new PostJob();

        $pdetail = $postjob->find()
            ->where([
            'JobId' => $jobid
        ])
            ->one();

        $pdetail->JobStatus = 1;

        if ($pdetail->save(false)) {

            $msg = "Job Closed Successfully";
        } else {

            $msg = "There is some error please try again";
        }

        echo json_encode($msg);
    }

    public function actionCompanypost()
    {
        if (isset(Yii::$app->session['Employerid'])) {
            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;
            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }
 $this->layout = 'layout';
            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end
            return $this->render('companypost');
        } else {
            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionPostajob($pf)
    {
		Yii::$app->cache->flush();
        if (isset(Yii::$app->session['Employerid'])) {
            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }
            Yii::$app->view->params['employeenotification'] = $allnotification;
            // notification for employer
            $allntemp = array();
            if (isset(Yii::$app->session['Employerid'])) {
                $empnot = new EmpNotification();
                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;
            // notification end
            $this->layout = 'layout';
            $postajob = new PostJob();
            $position = new Position();
            $jobcategory = new JobCategory();
            $whiteCategory = new WhiteCategory();
            $docmodel = new Documents();
            $alluser = new Alluser();
            $employerd = $alluser->find()
                ->select([
                'MobileNo',
                'Email',
                'Name',
                'City',
                'State',
                'Country',
                'CompanyDesc'
            ])
                ->where([
                'UserId' => Yii::$app->session['Employerid']
            ])
                ->one();
            $allposition = $position->find()
                ->where("IsDelete=0")
                ->all();
            // $allcategory=$jobcategory->find()->where("IsDelete=0")->all();
            $allcategory = Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all();
            $locationar = Location::find()->all();
            $education = Yii::$app->request->get()['education'];
            $education = Course::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'CourseId' => 'SORT_ASC'
            ])
                ->all();
			$whiteCategoryList =  ArrayHelper::map(WhiteCategory::find()->where([
                'status' => 1
            ])
                ->orderBy([
                'name' => 'SORT_ASC'
            ])
                ->all(), 'id', 'name');	
				
            $allcourse = Course::find()->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();
            if ($postajob->load(Yii::$app->request->post())) {
                $postajob->State = $employerd->State;
                $postajob->City = $employerd->City;
                $postajob->Country = $employerd->Country;
                $postajob->CompanyName = $employerd->Name;
                $date = date('Y-m-d');
                $totaljob = PostJob::find()->where([
                    'EmployerId' => Yii::$app->session['Employerid'],
                    'date(OnDate)' => $date
                ])->count();
                if ($totaljob < 10) {
                    $logo_id = 0;
                    if ($docmodel->load(Yii::$app->request->post())) {
                        $docmodel->image = \yii\web\UploadedFile::getInstance($docmodel, 'Doc');
                        if ($docmodel->image) {
							$logo_id = Documents::handleImageUpload($docmodel, 'Doc', 'LogoId');
                            /*$image = UploadedFile::getInstance($docmodel, 'Doc');
                            $ext = end((explode(".", $image->name)));
                            $ppp = Yii::$app->security->generateRandomString();
                            $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";
                            $docmodel->Doc = 'imageupload/' . $ppp . ".{$ext}";
                            $docmodel->OnDate = date('Y-m-d');
                            $docmodel->From = 'CompanyLogo';
                            if ($docmodel->save(false)) {
                                $logo_id = $docmodel->DocId;
                                $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));
                                $thumb = $furl . 'thumb_' . $logo_id . '.' . $ext;
                                if (file_exists($thumb)) {
                                    $document = Documents::find()->where([
                                        'DocId' => $logo_id
                                    ])->one();
                                    $document->Doc = 'imageupload/thumb_' . $logo_id . '.' . $ext;
                                    $document->save(false);
                                }
                            } else {
                                $logo_id = 0;
                            }*/
                        }
                    }
					
                    $postajob->LogoId = $logo_id;
                    $postajob->EmployerId = Yii::$app->session['Employerid'];
                    $postajob->OnDate = date('Y-m-d H:i:s');
                    $reff = 'MC' . date('dm') . rand(9, 999);
                    $postajob->RefId = $reff;
					
					/*Create Slug*/
					$slug = Yii::$app->request->post()['PostJob']['JobTitle'];
					$slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
					$slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
					$slug = preg_replace('~[^-\w]+~', '', $slug);
					$slug = trim($slug, '-');
					$slug = preg_replace('~-+~', '-', $slug);
					$slug = strtolower($slug);
					
					/*check slug*/
					 $count = PostJob::find()
								->where([
									'Slug' => $slug
								])
								->count();
					if($count > 0){
						$postajob->Slug = $slug.'-'.rand(9, 999);
					}else{
						$postajob->Slug = $slug;
					}
					
					
                    if ($pf == 0) {
                        $postajob->PostFor = 'Candidate';
                    } else {
                        $postajob->PostFor = 'Campus';
                    }
                    if (isset(Yii::$app->request->post()['PostJob']['PostFor'])) {
                        $postajob->PostFor = Yii::$app->request->post()['PostJob']['PostFor'];
                    }
                    if ($postajob->PostFor == 'Team') {
                        $postajob->NoofVacancy = Yii::$app->request->post()['PostJob']['NoofVacancyt'];
                    }
                    if ($postajob->IsWalkin == 1) {
                        if ($postajob->WalkinFrom == '') {
                            $postajob->WalkinFrom = date('Y-m-d');
                        }
                        if ($postajob->WalkinTo == '') {
                            $postajob->WalkinTo = date('Y-m-d');
                        }
                    }

                    if ($postajob->IsExp != 2) {
                        if ($postajob->Experience == 'Fresher') {
                            $postajob->IsExp = 0;
                        } else {
                            $postajob->IsExp = 1;
                        }
                    }

                    $altemail = '';
                    $altemailar = Yii::$app->request->post()['PostJob']['AltEmail'];

                    if ($altemailar) {
                        foreach ($altemailar as $ak => $aval) {
                            $altemail .= $aval . '|';
                        }
                    }
                    $altemail = trim($altemail, "|");
                    $postajob->AltEmail = $altemail;
                    $postajob->ContactPerson = Yii::$app->request->post()['PostJob']['ContactPerson'];
                    $postajob->JobShift = Yii::$app->request->post()['PostJob']['JobShift'];
                    $postajob->OtherSalary = Yii::$app->request->post()['PostJob']['OtherSalary'];
                    $postajob->JobSpecification = Yii::$app->request->post()['PostJob']['JobSpecification'];
                    $postajob->TechnicalGuidance = Yii::$app->request->post()['PostJob']['TechnicalGuidance'];
                    // $postajob->SpecialQualification = Yii::$app->request->post()['PostJob']['SpecialQualification'];
                    if (! empty(Yii::$app->request->post()['PostJob']['SpecialQualification'])) {
                        $postajob->SpecialQualification = implode(',', Yii::$app->request->post()['PostJob']['SpecialQualification']);
                    }

                    if (! empty(Yii::$app->request->post()['PostJob']['course'])) {
                        $postajob->course = implode(',', Yii::$app->request->post()['PostJob']['course']);
                    }
					
					if (! empty(Yii::$app->request->post()['PostJob']['Qualifications'])) {
						$postajob->qualifications = implode(',', Yii::$app->request->post()['PostJob']['Qualifications']);
					}
					
                    $blk = Yii::$app->request->post()['PostJob']['Location'];
                    if (! empty($blk)) {
                        $postajob->Location = implode(",", $blk);
                    } else {
                        $postajob->Location = 'None';
                    }
					
					if (empty(Yii::$app->request->post()['PostJob']['PositionId'])) {
						$postajob->PositionId = 108;
					}
					$postajob->CollarType =  Yii::$app->request->post()['PostJob']['CollarType'];
					if (Yii::$app->request->post()['PostJob']['CollarType'] == "white") {
						$postajob->WhiteCategoryId = Yii::$app->request->post()['PostJob']['WhiteCategory'];
					}else{
						$postajob->WhiteCategoryId = "";
					}	
					
					
					$postajob->LandlineNo = Yii::$app->request->post()['PostJob']['LandlineNo'];
					/*echo '<pre>';
					print_r(Yii::$app->request->post()['PostJob']);die;*/
					
                    if ($postajob->save(false)) {
                        $val = 1;
                        Yii::$app->session->setFlash('success', 'Job Post Successfully having reference no .' . $reff);
                        $rawskill = Yii::$app->request->post()['PostJob']['KeySkill'];
                        if(!empty($rawskill) && Yii::$app->request->post()['PostJob']['CollarType'] == 'blue'){
							foreach ($rawskill as $slk) {
								$skil = new JobRelatedSkill();
								$skil->JobId = $postajob->JobId;
								$skil->SkillId = $slk;
								$skil->OnDate = date('Y-m-d');
								$skil->save(false);
								if ($postajob->PostFor == 'Candidate' || $postajob->PostFor == 'Both') {
									$userlist = AllUser::find()->where([
										'AllUser.IsDelete' => 0,
										'VerifyStatus' => 1,
										'UserTypeId' => 2,
										'EmployeeSkill.SkillId' => $slk
									])
										->joinWith([
										'skilldetail'
									])
										->all();

									if ($userlist) {
										foreach ($userlist as $uk => $uval) {
											$nt = new Notification();
											$nt->JobId = $postajob->JobId;
											$nt->UserId = $uval->UserId;
											$nt->IsView = 0;
											$nt->OnDate = date('Y-m-d H:i:s');
											$nt->save(false);
										}
									}
								}

								if ($postajob->PostFor == 'Campus') {
									// student under campus
									$userlist1 = AllUser::find()->where([
										'AllUser.IsDelete' => 0,
										'VerifyStatus' => 1,
										'UserTypeId' => 2,
										'EmployeeSkill.SkillId' => $slk
									])
										->andWhere([
										'!=',
										'CampusId',
										0
									])
										->joinWith([
										'skilldetail'
									])
										->all();
									if ($userlist1) {
										foreach ($userlist1 as $uk => $uval) {
											$nt = new Notification();
											$nt->JobId = $postajob->JobId;
											$nt->UserId = $uval->UserId;
											$nt->IsView = 0;
											$nt->OnDate = date('Y-m-d H:i:s');
											$nt->save(false);
										}
									}
									// student under campus
									// campuslist
									$campuslist = AllUser::find()->where([
										'IsDelete' => 0,
										'VerifyStatus' => 1,
										'UserTypeId' => 4
									])->all();
									if ($campuslist) {
										foreach ($campuslist as $ck => $cval) {
											$nt = new Notification();
											$nt->JobId = $postajob->JobId;
											$nt->UserId = $cval->UserId;
											$nt->IsView = 0;
											$nt->OnDate = date('Y-m-d H:i:s');
											$nt->save(false);
										}
									}
								}
							}
						}else if(Yii::$app->request->post()['PostJob']['CollarType'] == 'white'){
							
							$whiteRole = Yii::$app->request->post()['PostJob']['WhiteRole'];
							$whiteSkills = Yii::$app->request->post()['PostJob']['WhiteSkills'];
							if(!empty($whiteRole)){
								foreach($whiteRole as $value){
									if($value != ""){
										$whiteRole = WhiteRole::find()->where([
												'id' => $value
											])->one();
										if(!empty($whiteRole)){
											$wRole = new PostJobWRole();
											$wRole->postjob_id = $postajob->JobId;
											$wRole->user_id = Yii::$app->session['Employerid'];
											$wRole->role_id = $value;
											$wRole->role = $whiteRole->name;
											$wRole->on_date = date('Y-m-d');
											$wRole->save(false);	
										}
									}
								}
								/*Notifications are pending*/
							}
							
							if(!empty($whiteSkills)){
								foreach($whiteSkills as $value){
									if($value != ""){
										$whiteSkillRec = WhiteSkill::find()->where([
												'id' => $value
											])->one();
										if(!empty($whiteSkillRec)){
											$wSkill = new PostJobWSkill();
											$wSkill->postjob_id = $postajob->JobId;
											$wSkill->user_id = Yii::$app->session['Employerid'];
											$wSkill->skill_id = $value;
											$wSkill->skill = $whiteSkillRec->name;
											$wSkill->on_date = date('Y-m-d');
											$wSkill->save(false);	
											/*Send Notification For User*/
											if ($postajob->PostFor == 'Candidate' || $postajob->PostFor == 'Both') {
												$userlist = UserWhiteSkill::find()->where([
													'skill_id' => $value,
													'AllUser.IsDelete' => 0,
													'AllUser.VerifyStatus' => 1,
													'AllUser.UserTypeId' => 2,	
												])->andWhere([
													'LIKE', 'AllUser.PreferredLocation', $blk
												])->joinWith([
												'user'
												])->GroupBy([
													'user_id'
												])->all();
												
												
												if ($userlist) {
													foreach ($userlist as $uk => $uval) {
														$nt = new Notification();
														$nt->JobId = $postajob->JobId;
														$nt->UserId = $uval->user_id;;
														$nt->IsView = 0;
														$nt->OnDate = date('Y-m-d H:i:s');
														$nt->save(false);
													}
												}
										}

										if ($postajob->PostFor == 'Campus') {
											// student under campus
											$userlist1 = UserWhiteSkill::find()->where([
													'skill_id' => $value,
													'AllUser.IsDelete' => 0,
													'AllUser.VerifyStatus' => 1,
													'AllUser.UserTypeId' => 2,	
												])->andWhere([
												'!=',
												'AllUser.CampusId',
												0
												])->andWhere([
													'LIKE', 'AllUser.PreferredLocation', $blk
												])->joinWith([
												'user'
												])->GroupBy([
												'user_id'
												])->all();
											
											if ($userlist1) {
												foreach ($userlist1 as $uk => $uval) {
													$nt = new Notification();
													$nt->JobId = $postajob->JobId;
													$nt->UserId = $uval->user_id;
													$nt->IsView = 0;
													$nt->OnDate = date('Y-m-d H:i:s');
													$nt->save(false);
												}
											}
											// student under campus
											// campuslist
											$campuslist = AllUser::find()->where([
												'IsDelete' => 0,
												'VerifyStatus' => 1,
												'UserTypeId' => 4
											])->andWhere([
													'LIKE', 'AllUser.PreferredLocation', $blk
												])->all();
											if ($campuslist) {
												foreach ($campuslist as $ck => $cval) {
													$nt = new Notification();
													$nt->JobId = $postajob->JobId;
													$nt->UserId = $cval->UserId;
													$nt->IsView = 0;
													$nt->OnDate = date('Y-m-d H:i:s');
													$nt->save(false);
												}
											}
										}
											
											
											
										}
									}
								}
								/*Notifications are pending*/
							}
						}					
						
                    } else {
                        $val = 0;
                        Yii::$app->session->setFlash('error', 'There is some error, Please try again');
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Your job post limit exceed, Please choose A plan or Contact My Career Bugs');
                }
                return $this->redirect([
                    'yourpost'
                ]);
            } else {
                $NaukariQualData = NaukariSpecialization::find()->all();
                $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3")->all();
				$topQualifications  = NaukariQualData::getHighestQualificationList();
                return $this->render('postajob', [
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'topQualifications' => $topQualifications,
                    'model' => $postajob,
                    'position' => $allposition,
                    'jobcategory' => $allcategory,
                    'empd' => $employerd,
                    'pf' => $pf,
                    'location' => $locationar,
                    'docmodel' => $docmodel,
                    'course' => $allcourse,
                    'education' => $education,
					'whiteCategories' => $whiteCategoryList
                ]);
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionSkill()
    {
        $skill = new Skill();

        $searchTerm = Yii::$app->request->get()['term'];

        if (isset(Yii::$app->request->get()['roleid'])) {

            $roleid = Yii::$app->request->get()['roleid'];

            $allskill = $skill->find()
                ->where([
                'IsDelete' => 0,
                'PositionId' => $roleid
            ])
                ->andWhere([
                'LIKE',
                'Skill',
                $searchTerm
            ])
                ->all();
        } else {

            $allskill = $skill->find()
                ->where([
                'IsDelete' => 0
            ])
                ->andWhere([
                'LIKE',
                'Skill',
                $searchTerm
            ])
                ->all();
        }

        $data = array();

        foreach ($allskill as $key => $value) {

            $data[] = array(
                'id' => $value->SkillId,
                'value' => $value->Skill
            );
        }

        echo json_encode($data);
    }

    public function actionGeteducationskills()
    {
        $data = [];
        $data['status'] = 'NOK';
        $roleId = Yii::$app->request->get('id');
        $html = '';
        $html .= '<div class="form-group field-experience-skills">
                    <label class="control-label">Skills</label>
                <div id="experience-skills">';
        if ($roleId) {
            $allskills = Skill::find()->where([
                'IsDelete' => 0,
                'PositionId' => $roleId
            ])->all();
            if (! empty($allskills)) {
                $data['status'] = 'OK';
                foreach ($allskills as $key => $skill) {
                    $html .= '<label class="modal-radio"><input type="checkbox" name="Education[SkillId][' . $roleId . '][]" value="' . $skill->SkillId . '" tabindex="3"><i></i><span> ' . $skill->Skill . ' </span></label>';
                }
            } else {
                $html .= 'No Skills Found';
            }
        }
        $html .= '<p class="help-block help-block-error"></p> </div>';
        $data['result'] = $html;
        return $this->asJson($data);
    }

    public function actionGetskills()
    {
        $data = [];
        $data['status'] = 'NOK';
        $roleId = Yii::$app->request->get('id');
        $html = '';
        $html .= '<div class="form-group field-experience-skills">
                    <label class="control-label">Skills</label>
                <div id="experience-skills">';
        if (!empty($roleId) && $roleId != "undefined") {
            $allskills = Skill::find()->where([
                'IsDelete' => 0,
                'PositionId' => $roleId
            ])->all();
            if (! empty($allskills)) {
                $data['status'] = 'OK';
                foreach ($allskills as $key => $skill) {
                    $html .= '<label class="modal-radio"><input type="checkbox" name="Experience[skills][' . $roleId . '][]" value="' . $skill->SkillId . '" tabindex="3"><i></i><span> ' . $skill->Skill . ' </span></label>';
                }
            } else {
                $html .= 'No Skills Found';
            }
        }
        $html .= '<p class="help-block help-block-error"></p> </div>';
        $data['result'] = $html;
        return $this->asJson($data);
    }
	
	 public function actionGetexperienceskills()
    {
        $data = [];
        $data['status'] = 'NOK';
        $roleId = Yii::$app->request->get('id');
        $count = Yii::$app->request->get('count');
		if(empty($count)){
			$count = 0;
		}
        $html = '';
        $html .= '<div class="form-group field-experience-skills">
                    <label class="control-label">Skills</label>
                <div id="experience-skills">';
        if ($roleId) {
            $allskills = Skill::find()->where([
                'IsDelete' => 0,
                'PositionId' => $roleId
            ])->all();
            if (! empty($allskills)) {
                $data['status'] = 'OK';
                foreach ($allskills as $key => $skill) {
                    $html .= '<label class="modal-radio"><input type="checkbox" name="Experience['.$count.'][skills][' . $roleId . '][]" value="' . $skill->SkillId . '" tabindex="3"><i></i><span> ' . $skill->Skill . ' </span></label>';
                }
            } else {
                $html .= 'No Skills Found';
            }
        }
        $html .= '<p class="help-block help-block-error"></p> </div>';
        $data['result'] = $html;
        return $this->asJson($data);
    }

    public function actionGetselectedskills()
    {
        $data = [];
        $data['status'] = 'NOK';
        $roleId = Yii::$app->request->get('id');
        $userId = Yii::$app->request->get('userId');
        $name = Yii::$app->request->get('name');
        $existCheck = Yii::$app->request->get('existCheck', true);
        $html = '';
        $html .= '<div class="form-group field-experience-skills">
                    <div id="experience-skills">';
        if ($roleId) {
            $allskills = Skill::find()->where([
                'IsDelete' => 0,
                'PositionId' => $roleId
            ])->all();
            if (! empty($allskills)) {
                $data['status'] = 'OK';
                foreach ($allskills as $key => $skill) {
                    $isSelected = EmployeeSkill::find()->where([
                        'SkillRoleId' => $roleId,
                        'SkillId' => $skill->SkillId,
                        'UserId' => $userId
                    ])->exists();
                    $selected = $isSelected ? 'checked' : '';
                    $selected = $existCheck == 'false' ? '' : $selected;
                    $html .= '<label class="modal-radio"><input type="checkbox" ' . $selected . ' name="AllUser[' . $name . '][]" value="' . $skill->SkillId . '" tabindex="3"><i></i><span> ' . $skill->Skill . ' </span></label>';
                }
            } else {
                $html .= 'No Skills Found';
            }
        }
        $html .= '<p class="help-block help-block-error"></p> </div>';
        $data['result'] = $html;
        return $this->asJson($data);
    }

    public function actionSkilldata()
    {
        // Check By Vishal
        $allskill = [];
        $roleid = Yii::$app->request->get()['roleid'];
        if (isset(Yii::$app->request->get()['roleid'])) {
            $roleid = Yii::$app->request->get()['roleid'];
            $allskill = Skill::find()->where([
                'IsDelete' => 0,
                'PositionId' => $roleid
            ])->all();
        }
        $data['status'] = 'NOK';

        if (! empty($allskill)) {
            $data['status'] = 'OK';
            foreach ($allskill as $key => $value) {
                $data[] = array(
                    'id' => $value->SkillId,
                    'value' => $value->Skill
                );
            }
        }

        echo json_encode($data);
    }

    public function actionSpecialskill()

    {
        $searchTerm = Yii::$app->request->get()['term'];

        $allsk = SpecialQualification::find()->where([
            'LIKE',
            'Special',
            $searchTerm
        ])->all();

        $data = array();

        foreach ($allsk as $key => $value) {

            $data[] = array(
                'id' => $value->SpecialId,
                'value' => $value->Special
            );
        }

        echo json_encode($data);
    }

    public function actionJobshift()

    {
        $searchTerm = Yii::$app->request->get()['term'];

        $allsk = JobShift::find()->where([
            'LIKE',
            'JobShift',
            $searchTerm
        ])->all();

        $data = array();

        foreach ($allsk as $key => $value) {

            $data[] = array(
                'id' => $value->JobShiftId,
                'value' => $value->JobShift
            );
        }

        echo json_encode($data);
    }

    public function actionOthersalary()

    {
        $searchTerm = Yii::$app->request->get()['term'];

        $allsk = OtherSalaryDetail::find()->where([
            'LIKE',
            'Other',
            $searchTerm
        ])->all();

        $data = array();

        foreach ($allsk as $key => $value) {

            $data[] = array(
                'id' => $value->OtherSalaryId,
                'value' => $value->Other
            );
        }

        echo json_encode($data);
    }

    public function actionEmployerlogout()

    {
        $session = Yii::$app->session;

        $session->open();

        unset(Yii::$app->session['Employerid']);

        unset(Yii::$app->session['EmployerName']);

        unset(Yii::$app->session['EmployerEmail']);

        unset(Yii::$app->session['EmployerDP']);

        unset(Yii::$app->session['SocialEmail']);

        unset(Yii::$app->session['SocialName']);

        return $this->redirect([
            'index'
        ]);
    }

    public function actionTeamjobsearch()
    {
        // notification for employee
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $postjob = new PostJob();

        $postfor = array(
            'Team',
            'Both'
        );

        // All Job

        if (isset(Yii::$app->request->get()['indexsearch'])) {
            $keyname = Yii::$app->request->get()['keyname'];
            $indexlocation = Yii::$app->request->get()['indexlocation'];
            $experience = Yii::$app->request->get()['experience'];
            $salary = Yii::$app->request->get()['salary'];
            $education = Yii::$app->request->get()['education'];
            $sklist = array();

            $skilllist = Skill::find()->where([
                'like',
                'Skill',
                $keyname
            ])->all();

            foreach ($skilllist as $sk => $sv) {

                $sklist[] = $sv->SkillId;
            }

            if ($salary != '') {

                $salaryrange = str_replace(' Lakhs', "", $salary);
            } else {
                $salaryrange = '';
            }

            $alljob = $postjob->find()
                ->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->joinWith([
                'jobRelatedSkills'
            ])
                ->andFilterWhere([
                'or',

                [
                    'like',
                    'JobTitle',
                    $keyname
                ],

                [
                    'like',
                    'CompanyName',
                    $keyname
                ],

                [
                    'JobRelatedSkill.SkillId' => $sklist
                ]
            ])
                ->andFilterWhere([
                'Location' => $indexlocation,
                'Experience' => $experience,
                'Salary' => $salaryrange
            ])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ]);
        } else if (isset(Yii::$app->request->get()['alljobsearch'])) {

            $keyword = Yii::$app->request->get()['keyword'];

            $id = Yii::$app->request->get()['ID'];

            if ($keyword == 'popular') {

                $catname = Industry::find()->where([
                    'IndustryId' => $id
                ])->one();

                $alljob = $postjob->find()
                    ->where([
                    'JobCategoryId' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } elseif ($keyword == 'employer') {

                $alljob = $postjob->find()
                    ->where([
                    'EmployerId' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } elseif ($keyword == 'company') {

                $alljob = $postjob->find()
                    ->where([
                    'CompanyName' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } elseif ($keyword == 'city') {

                $alljob = $postjob->find()
                    ->where([
                    'City' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } elseif ($keyword == 'industry') {

                $alljob = $postjob->find()
                    ->where([
                    'PostJob.IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor,
                    'IndustryId' => $id
                ])
                    ->joinWith([
                    'employer'
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'PostJob.OnDate' => SORT_DESC
                ]);
            }
        } else {

            if (isset(Yii::$app->request->get()['JobCategoryId'])) {
                $jobcategoryid = Yii::$app->request->get()['JobCategoryId'];
            } else {

                $jobcategoryid = '';
            }

            $latest = '';
            $role = '';
            $state = '';
            $jobtype = array();
            $skillby = array();
            $companyby = array();

            if (isset(Yii::$app->request->get()['latest'])) {
                $latest = Yii::$app->request->get()['latest'];
            }

            if (isset(Yii::$app->request->get()['role'])) {
                $role = Yii::$app->request->get()['role'];
            }

            if (isset(Yii::$app->request->get()['state'])) {
                $state = Yii::$app->request->get()['state'];
            }

            if (isset(Yii::$app->request->get()['education'])) {
                $education = Yii::$app->request->get()['education'];
            } else {
                $education = '';
            }

            if (isset(Yii::$app->request->get()['salaryrange']) && Yii::$app->request->get()['salaryrange'] != '') {
                $salary = explode(",", Yii::$app->request->get()['salaryrange']);
            } else {
                $salary = '';
            }

            if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {
                $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
            }

            if (isset(Yii::$app->request->get()['skillby']) && Yii::$app->request->get()['skillby'] != '') {

                $skillby = explode(",", Yii::$app->request->get()['skillby']);
            }

            if (isset(Yii::$app->request->get()['companyby']) && Yii::$app->request->get()['companyby'] != '') {

                $companyby = explode(",", Yii::$app->request->get()['companyby']);
            }

            if (isset(Yii::$app->request->get()['company']) && Yii::$app->request->get()['company'] != '') {

                $company = Yii::$app->request->get()['company'];
            } else {

                $company = '';
            }

            if (isset(Yii::$app->request->get()['experience']) && Yii::$app->request->get()['experience'] != '') {

                if (Yii::$app->request->get()['expno'] != '') {

                    $expone = Yii::$app->request->get()['expno'];
                } else {

                    $expone = '';
                }

                $experience = explode(',', Yii::$app->request->get()['experience']);
            } else {

                $experience = array();

                $expone = '';
            }

            $ddate = date('Y-m-d', strtotime("-$latest days"));

            $alljob = $postjob->find()
                ->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->andFilterWhere([
                'or',
                [
                    'like',
                    'Location',
                    $state
                ]
            ])
                ->andFilterWhere([
                'JobCategoryId' => $jobcategoryid,
                'PositionId' => $role,
                'JobType' => $jobtype,
                'Salary' => $salary,
                'SpecialQualification' => $education,
                'JobRelatedSkill.SkillId' => $skillby,
                'CompanyName' => $companyby,
                'EmployerId' => $company,
                'IsExp' => $experience,
                'Experience' => $expone
            ])
                ->andFilterWhere([
                '>=',
                'DATE( PostJob.OnDate )',
                $ddate
            ])
                ->joinWith([
                'jobRelatedSkills'
            ])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ]);
        }

         if (isset(Yii::$app->request->get()['nak_course']) && Yii::$app->request->get()['nak_course'] != '') {
			
            $alljob = $alljob->andFilterWhere([
                'or',
                [
                    'like',
                    'PostJob.course',
                    Yii::$app->request->get()['nak_course']
                ]
            ]);
        }
		
		 if (isset(Yii::$app->request->get()['education']) && Yii::$app->request->get()['education'] != '') {
			
            $alljob = $alljob->andFilterWhere([
                'or',
                [
                    'like',
                    'PostJob.SpecialQualification',
                    Yii::$app->request->get()['education']
                ]
            ]);
        }
		
		if (isset(Yii::$app->request->get()['qualification']) && Yii::$app->request->get()['qualification'] != '') {
			
            $alljob = $alljob->andFilterWhere([
				'or',

                [
                    'like',
                    'qualifications',
                    Yii::$app->request->get()['qualification']
                ]
            ]);
			
        }
		
		if (isset(Yii::$app->request->get()['collar_type']) && Yii::$app->request->get()['collar_type'] != '') {

                $collarType = Yii::$app->request->get()['collar_type'];
				
				if($collarType == 'white'){
					 $alljob = $alljob->andFilterWhere([
						'PostJob.CollarType' => $collarType
					]);
				}else{
					 $alljob = $alljob->andWhere([
						'or', ['IS', 'PostJob.CollarType', NULL], ['PostJob.CollarType' => $collarType]
					]);
					
				}
			}
			
			if (isset(Yii::$app->request->get()['category_id']) && Yii::$app->request->get()['category_id'] != '') {

                $categoryId = Yii::$app->request->get()['category_id'];
				
				$alljob = $alljob->andFilterWhere([
						'PostJob.WhiteCategoryId' => $categoryId
					]);
			}
			
			if (isset(Yii::$app->request->get()['collar_role']) && Yii::$app->request->get()['collar_role'] != '') {

                $collarRole = Yii::$app->request->get()['collar_role'];
				
				$roleJobId = ArrayHelper::map(PostJobWRole::find()->where([
					'role_id', $collarRole
				])->all(), 'postjob_id', 'postjob_id');
				
				$alljob = $alljob->andFilterWhere([
						'PostJob.JobId' => $roleJobId
					]);
					
			}
			
			if (isset(Yii::$app->request->get()['collar_skills']) && Yii::$app->request->get()['collar_skills'] != '') {

                $collarSkills = Yii::$app->request->get()['collar_skills'];
				
				$skillJobId = ArrayHelper::map(PostJobWSkill::find()->where([
					'skill_id' => $collarSkills
				])->all(), 'postjob_id', 'postjob_id');
				
				$alljob = $alljob->andFilterWhere([
						'PostJob.JobId' => $skillJobId
					]);
			}
        /*
         * echo "<pre>";
         * print_r($alljob->createCommand()->rawSql);
         * die();
         */
        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }

        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // echo "<pre>";print_r($alljob->createCommand()->getRawSql()); die();

        // position

        $position = new Position();

        $allposition = $position->find()
            ->where([
            'IsDelete' => 0
        ])
            ->all();

        // skill

        if (isset(Yii::$app->request->get()['role']))

            $PositionId = Yii::$app->request->get()['role'];

        $allskill = Skill::find()->where([
            'IsDelete' => 0,
            'PositionId' => $PositionId
        ])->all();

        // company

        $allcompany = PostJob::find()->select([
            'CompanyName'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0,
            'PostFor' => $postfor
        ])
            ->groupBy([
            'CompanyName'
        ])
            ->all();

        //

        // hot categories

        $hotcategory = $postjob->find()
            ->select([
            'count(*) as cnt',
            'Industry.IndustryName as CategoryName',
            'Industry.IndustryId as JobCategoryId'
        ])
            ->joinWith([
            'jobCategory'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor,
            'Industry.IsDelete' => 0
        ])
            ->groupBy([
            'PostJob.JobCategoryId'
        ])
            ->all();

        // location list

        $location = $postjob->find()
            ->select([
            'Location'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0
        ])
            ->groupBy([
            'Location'
        ])
            ->all();

        $allcategory = Industry::find()->where("IsDelete=0")
            ->orderBy([
            'IndustryName' => SORT_ASC
        ])
            ->all();

        $allcourse = Course::find()->where("IsDelete=0")
            ->orderBy([
            'CourseName' => 'SORT_ASC'
        ])
            ->all();

        $jbpst = new PostJob();

        $NaukariQualData = NaukariSpecialization::find()->all();

        $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3")->all();

        $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3")->all();
		$qualifications = NaukariQualData::find()->where("parent_id=0")->all();
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('jobsearch_ajax', array(
                'jbpst' => $jbpst,
                'alljob' => $alljob,
                'pages' => $pages,
				'type' => 'team'
            ));
        } else {
            return $this->render('jobsearch', [
                'NaukariQualData' => $NaukariQualData,
                'naukari_specialization' => $naukari_specialization,
				'qualifications' => $qualifications,
                'jbpst' => $jbpst,
                'alljob' => $alljob,
                'pages' => $pages,
                'hotcategory' => $hotcategory,
                'role' => $allposition,
                'allskill' => $allskill,
                'allcompany' => $allcompany,
                'location' => $location,
                'industry' => $allcategory,
                'course' => $allcourse,
                'education' => $education,
				'type' => 'team'
            ]);
        }
    }

    // ------------------- ALL USER(Employers) End -------------------//
    public function actionJobsearch($catname="")
    {
        echo "<pre>";
        print_r('asdasd');
        exit();

        // var_dump(Yii::$app->request->get()['alias']);die();
        // notification for employee
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $postjob = new PostJob();

        /*
         * if (isset(Yii::$app->session['Campusid'])) {
         *
         * $postfor = array(
         * 'Campus'
         * );
         * } elseif (isset(Yii::$app->session['Employeeid'])) {
         *
         * $postfor = array(
         * 'Candidate',
         * 'Both'
         * );
         * } elseif (isset(Yii::$app->session['Teamid'])) {
         *
         * $postfor = array(
         * 'Team',
         * 'Both'
         * );
         * } else {
         */

        $postfor = array(
            'Candidate',
            'Both'
        );
        // }

        // All Job

        if (isset(Yii::$app->request->get()['indexsearch'])) {
            $keyname = Yii::$app->request->get()['keyname'];
            echo "<pre>";
            print_r($keyname);
            exit();

            $indexlocation = Yii::$app->request->get()['indexlocation'];
            $experience = Yii::$app->request->get()['experience'];
            $salary = Yii::$app->request->get()['salary'];
            $education = Yii::$app->request->get()['education'];
            $sklist = array();
			
            $skilllist = Skill::find()->where([
                'like',
                'Skill',
                $keyname
            ])->all();

            foreach ($skilllist as $sk => $sv) {

                $sklist[] = $sv->SkillId;
            }

            if ($salary != '') {

                $salaryrange = str_replace(' Lakhs', "", $salary);
                $salaryrange1 = $salary;
            } else {
                $salaryrange = '';
				$salaryrange1 = '';
            }
            
            $jobcategoryid = '';
            if (isset(Yii::$app->request->get()['JobCategoryId'])) {

                $jobcategoryid = Yii::$app->request->get()['JobCategoryId'];
            } 
			
			$company = "";
			
            if(isset(Yii::$app->request->get()['alias']) && Yii::$app->request->get()['alias']!=null){
			    $string=Yii::$app->request->get()['alias'];
				/*Search Industry*/
			    $jobcategoryid = Industry::find()->where(['slug'=>$string,'IsDelete' => 0])->one();
			    $jobcategoryid = $jobcategoryid->IndustryId;
				if(empty($jobcategoryid)){
					$string = str_replace('-', ' ',$string);
					$userRecord = AllUser::find()->where(['Name'=>$string, 'EntryType' => ['Company','Consultancy', 'HR'],'IsDelete' => 0])->one();
					if(!empty($userRecord)){
						$company = $userRecord->UserId;
					}
				}
				/**/
				
				
				
			 //   var_dump("<pre>",$jobcategoryid->IndustryId);die();
			}

            $latest = '';
            $role = '';
            $state = '';
			$city = '';
			$collarType = '';
			$categoryId = '';
            $jobtype = array();
            $skillby = array();
            $companyby = array();
			
			if(isset(Yii::$app->request->get()['city']) && !empty(Yii::$app->request->get()['city'])){
				$city = Yii::$app->request->get()['city'];
				
			}

            if (isset(Yii::$app->request->get()['latest'])) {

                $latest = Yii::$app->request->get()['latest'];
            }

            if (isset(Yii::$app->request->get()['role'])) {

                $role = Yii::$app->request->get()['role'];
            }

            if (isset(Yii::$app->request->get()['indexlocation'])) {

                $state = Yii::$app->request->get()['indexlocation'];
            }

            if (isset(Yii::$app->request->get()['education'])) {

                $education = Yii::$app->request->get()['education'];
            } else {
                $education = '';
            }

            if (isset(Yii::$app->request->get()['salary']) && Yii::$app->request->get()['salary'] != '') {

                $salary = explode(",", Yii::$app->request->get()['salary']);
            } else {
                $salary = '';
            }

            if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {

                $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
            }

            if (isset(Yii::$app->request->get()['skillby']) && Yii::$app->request->get()['skillby'] != '') {

                $skillby = explode(",", Yii::$app->request->get()['skillby']);
            }

            if (isset(Yii::$app->request->get()['companyby']) && Yii::$app->request->get()['companyby'] != '') {

                $companyby = explode(",", Yii::$app->request->get()['companyby']);
            }
			
            if (isset(Yii::$app->request->get()['company']) && Yii::$app->request->get()['company'] != '') {

                $company = Yii::$app->request->get()['company'];
            } 
			
			if (isset(Yii::$app->request->get()['postby']) && Yii::$app->request->get()['postby'] != '') {

                $postBy = explode(",", Yii::$app->request->get()['postby']);
            } else {

                $postBy = '';
            }
            if (isset(Yii::$app->request->get()['experience']) && Yii::$app->request->get()['experience'] != '') {

                if (Yii::$app->request->get()['expno'] != '') {

                    $expone = Yii::$app->request->get()['expno'];
                } else {

                    $expone = '';
                }

                $experience = explode(',', Yii::$app->request->get()['experience']);
            } else {

                $experience = array();

                $expone = '';
            }
		
			
            //added by Manish
            $ddate = date('Y-m-d', strtotime("-$latest days"));
            $today = date('Y-m-d');
			
            //added by Manish
            $alljob = $postjob->find()
                ->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->andFilterWhere([
                'or',
				
                [
                    'like',
                    'Location',
                    $city
                ]
            ]);
            
            if(isset($keyname))
            {
                $alljob->andFilterWhere([
                'or',
				
                [
                    'like',
                    'JobTitle',
                    $keyname
                ],
                [
                    'IN',
                    'JobRelatedSkill.SkillId',
                    $keyname
                ],
                [
                    'like',
                    'CompanyName',
                    $keyname
                ]
            ]);  
            
           
          
            }
            
            if(!empty($experience))
            {
                
                if($experience[0] == 'Both'){
                $alljob->andFilterWhere(['PostJob.IsExp' => '2']);
                }else{
                     $alljob->andFilterWhere(['Experience' => $experience]);
                }
            }
            
                $alljob->andFilterWhere([

                'JobCategoryId' => $jobcategoryid,

                'PositionId' => $role,

                'JobType' => $jobtype,

                //'PostJob.Salary' => str_replace(" Lakhs","",$salary),
				
                'JobRelatedSkill.SkillId' => $skillby,

                'CompanyName' => $companyby,

                'EmployerId' => $company,

				
				'AllUser.EntryType' => $postBy
				
				
            ])
			 ->andFilterWhere(['or',
                ['PostJob.Salary' => str_replace(" Lakhs","",$salary)],
				['PostJob.Salary' => $salary],
            ])
                ->andFilterWhere([
                '>=',
                'DATE( PostJob.OnDate )',
                $ddate
            ])->andFilterWhere([
                'LIKE',
                'PostJob.Location',
                $state
            ])
			->joinWith([
                'jobRelatedSkills',
				'employer',
				'jobWhiteRoles',
				'jobWhiteSkills'
            ])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ]);


            $alljob = $alljob->andWhere([ "or",
				[">=",
				"WalkinTo",
				$today],
				["WalkinTo" => NULL]
			]);
			


			
			/*echo '<pre>';
			print_r($alljob->createCommand()->rawSql);die;*/
			
			 /*
         * echo "<pre>jj";
         * print_r($alljob->createCommand()->rawSql);
         * die();
         */
			
			if ($keyname != '') {
				
			   $roleJobId = ArrayHelper::map(PostJobWRole::find()->where([
                'or', ['LIKE', 'role', $keyname]
				])->all(), 'postjob_id', 'postjob_id');
				
				$skillJobId = ArrayHelper::map(PostJobWSkill::find()->where([
				'or', ['LIKE', 'skill', $keyname]
                ])->all(), 'postjob_id', 'postjob_id');
				
				$jobIds = array_merge($roleJobId, $skillJobId);
				//echo '<pre>';
				//print_r($skillJobId);die;
				
               $alljob = $alljob->orFilterWhere([
						'PostJob.JobId' => $jobIds
					]);
					
			}
			
			
        } else if (isset(Yii::$app->request->get()['alljobsearch'])) {

            $keyword = Yii::$app->request->get()['keyword'];
			
            $id = Yii::$app->request->get()['ID'];
            $ctname = isset(Yii::$app->request->get()['ctname'])?Yii::$app->request->get()['ctname']:"";
			
			

            if ($keyword == 'popular') {

                $catname = Industry::find()->where([
                    'IndustryId' => $id
                ])->one();

                $alljob = $postjob->find()
                    ->where([
                    'JobCategoryId' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } elseif ($keyword == 'employer') {

                $alljob = $postjob->find()
                    ->where([
                    'EmployerId' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
            } elseif ($keyword == 'company') {

                $alljob = $postjob->find()
                    ->where([
                    'CompanyName' => $id,
                    'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);
				
				
            } elseif ($keyword == 'city') {
				$alljob = $postjob->find()
                    ->where([
                   'IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor
                ]) ->andFilterWhere([
					'or',
					[
						'like',
						'Location',
						$ctname
					]
				])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ]);				
				
            } elseif ($keyword == 'industry') {

                $alljob = $postjob->find()
                    ->where([
                    'PostJob.IsDelete' => 0,
                    'JobStatus' => 0,
                    'PostFor' => $postfor,
                    'JobCategoryId' => $id
                ])
                    ->joinWith([
                    'employer'
                ])
                    ->groupBy([
                    'PostJob.JobId'
                ])
                    ->orderBy([
                    'PostJob.OnDate' => SORT_DESC
                ]);
				
            }
        } else {
			//echo '<pre>';
			//print_r(Yii::$app->request->get());
			$jobcategoryid = '';
            if (isset(Yii::$app->request->get()['JobCategoryId'])) {

                $jobcategoryid = Yii::$app->request->get()['JobCategoryId'];
            } 
			
			$company = "";
			
            if(isset(Yii::$app->request->get()['alias']) && Yii::$app->request->get()['alias']!=null){
			    $string=Yii::$app->request->get()['alias'];
				/*Search Industry*/
			    $jobcategoryid = Industry::find()->where(['slug'=>$string,'IsDelete' => 0])->one();
			    $jobcategoryid = $jobcategoryid->IndustryId;
				if(empty($jobcategoryid)){
					$string = str_replace('-', ' ',$string);
					$userRecord = AllUser::find()->where(['Name'=>$string, 'EntryType' => ['Company','Consultancy', 'HR'],'IsDelete' => 0])->one();
					if(!empty($userRecord)){
						$company = $userRecord->UserId;
					}
				}
				/**/
				
				
				
			 //   var_dump("<pre>",$jobcategoryid->IndustryId);die();
			}

            $latest = '';
            $role = '';
            $state = '';
			$city = '';
			$collarType = '';
			$categoryId = '';
            $jobtype = array();
            $skillby = array();
            $companyby = array();
			
			if(isset(Yii::$app->request->get()['city']) && !empty(Yii::$app->request->get()['city'])){
				$city = Yii::$app->request->get()['city'];
				
			}

            if (isset(Yii::$app->request->get()['latest'])) {

                $latest = Yii::$app->request->get()['latest'];
            }

            if (isset(Yii::$app->request->get()['role'])) {

                $role = Yii::$app->request->get()['role'];
            }

            if (isset(Yii::$app->request->get()['state'])) {

                $state = Yii::$app->request->get()['state'];
            }

            if (isset(Yii::$app->request->get()['education'])) {

                $education = Yii::$app->request->get()['education'];
            } else {
                $education = '';
            }

            if (isset(Yii::$app->request->get()['salaryrange']) && Yii::$app->request->get()['salaryrange'] != '') {

                $salary = explode(",", Yii::$app->request->get()['salaryrange']);
            } else {
                $salary = '';
            }

            if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {

                $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
            }

            if (isset(Yii::$app->request->get()['skillby']) && Yii::$app->request->get()['skillby'] != '') {

                $skillby = explode(",", Yii::$app->request->get()['skillby']);
            }

            if (isset(Yii::$app->request->get()['companyby']) && Yii::$app->request->get()['companyby'] != '') {

                $companyby = explode(",", Yii::$app->request->get()['companyby']);
            }
			
            if (isset(Yii::$app->request->get()['company']) && Yii::$app->request->get()['company'] != '') {

                $company = Yii::$app->request->get()['company'];
            } 
			
			if (isset(Yii::$app->request->get()['postby']) && Yii::$app->request->get()['postby'] != '') {

                $postBy = explode(",", Yii::$app->request->get()['postby']);
            } else {

                $postBy = '';
            }
            if (isset(Yii::$app->request->get()['experience']) && Yii::$app->request->get()['experience'] != '') {

                if (Yii::$app->request->get()['expno'] != '') {

                    $expone = Yii::$app->request->get()['expno'];
                } else {

                    $expone = '';
                }

                $experience = explode(',', Yii::$app->request->get()['experience']);
            } else {

                $experience = array();

                $expone = '';
            }

            $ddate = date('Y-m-d', strtotime("-$latest days"));
			
			$today = date('Y-m-d');
			
            $alljob = $postjob->find()
                ->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->andFilterWhere([
                'or',
				
                [
                    'like',
                    'Location',
                    $city
                ]
            ])
                ->andFilterWhere([

                'JobCategoryId' => $jobcategoryid,

                'PositionId' => $role,

                'JobType' => $jobtype,

                //'PostJob.Salary' => str_replace(" Lakhs","",$salary),
				
                'JobRelatedSkill.SkillId' => $skillby,

                'CompanyName' => $companyby,

                'EmployerId' => $company,

                'PostJob.IsExp' => $experience,

                'Experience' => $expone,
				
				'AllUser.EntryType' => $postBy
				
				
            ])
			 ->andFilterWhere(['or',
                ['PostJob.Salary' => str_replace(" Lakhs","",$salary)],
				['PostJob.Salary' => $salary],
            ])
                ->andFilterWhere([
                '>=',
                'DATE( PostJob.OnDate )',
                $ddate
            ])->andFilterWhere([
                'LIKE',
                'PostJob.Location',
                $state
            ])
			->joinWith([
                'jobRelatedSkills',
				'employer',
				'jobWhiteRoles',
				'jobWhiteSkills'
            ])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ]);
			
			
        }

        $alljob = $alljob->andWhere([ "or",
				[">=",
				"WalkinTo",
				$today],
				["WalkinTo" => NULL]
			]);
		
		if (isset(Yii::$app->request->get()['nak_course']) && Yii::$app->request->get()['nak_course'] != '') {
			
            $alljob = $alljob->andFilterWhere([
                'or',
                [
                    'like',
                    'PostJob.course',
                    Yii::$app->request->get()['nak_course']
                ]
            ]);
        }
		
		 if (isset(Yii::$app->request->get()['education']) && Yii::$app->request->get()['education'] != '') {
			
            $alljob = $alljob->andFilterWhere([
                'or',
                [
                    'like',
                    'PostJob.SpecialQualification',
                    Yii::$app->request->get()['education']
                ]
            ]);
        }
		
		if (isset(Yii::$app->request->get()['qualification']) && Yii::$app->request->get()['qualification'] != '') {
			
            $alljob = $alljob->andFilterWhere([
				'or',

                [
                    'like',
                    'qualifications',
                    Yii::$app->request->get()['qualification']
                ]
            ]);
			
        }
		
		if (isset(Yii::$app->request->get()['collar_type']) && Yii::$app->request->get()['collar_type'] != '') {

                $collarType = Yii::$app->request->get()['collar_type'];
				
				if($collarType == 'white'){
					 $alljob = $alljob->andFilterWhere([
						'PostJob.CollarType' => $collarType
					]);
				}else{
					 $alljob = $alljob->andWhere([
						'or', ['IS', 'PostJob.CollarType', NULL], ['PostJob.CollarType' => $collarType]
					]);
					
				}
			}
			
			if (isset(Yii::$app->request->get()['category_id']) && Yii::$app->request->get()['category_id'] != '') {

                $categoryId = Yii::$app->request->get()['category_id'];
				
				$alljob = $alljob->andFilterWhere([
						'PostJob.WhiteCategoryId' => $categoryId
					]);
			}
			
			if (isset(Yii::$app->request->get()['collar_role']) && Yii::$app->request->get()['collar_role'] != '') {

                $collarRole = Yii::$app->request->get()['collar_role'];
				
				$roleJobId = ArrayHelper::map(PostJobWRole::find()->where([
					'role_id' => $collarRole
				])->all(), 'postjob_id', 'postjob_id');
				
				$alljob = $alljob->andFilterWhere([
						'PostJob.JobId' => $roleJobId
					]);
					
			}
			
			if (isset(Yii::$app->request->get()['collar_skills']) && Yii::$app->request->get()['collar_skills'] != '') {

                $collarSkills = Yii::$app->request->get()['collar_skills'];
				
				$skillJobId = ArrayHelper::map(PostJobWSkill::find()->where([
					'skill_id' => $collarSkills
				])->all(), 'postjob_id', 'postjob_id');
				
				$alljob = $alljob->andFilterWhere([
						'PostJob.JobId' => $skillJobId
					]);
			}
		
		//echo '<pre>';
		//print_r(Yii::$app->request->get());die;
		
      
        //   echo "<pre>";
        //   print_r($alljob->createCommand()->rawSql);
        //   die();
        
        $pages = new Pagination([
            'totalCount' => $alljob->count()
        ]);
		

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }

        $alljob = $alljob->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
			
		//echo '<pre>';
		//print_r($alljob);

        //echo "<pre>";print_r($alljob->createCommand()->getRawSql()); die();
		//$allcandidate->createCommand()->getRawSql(); die;

        // position

        $position = new Position();

        $allposition = $position->find()
            ->where([
            'IsDelete' => 0
        ])
            ->all();

        // skill

        if (isset(Yii::$app->request->get()['role']))

            $PositionId = Yii::$app->request->get()['role'];

			$allskill = Skill::find()->where([
				'IsDelete' => 0,
				'PositionId' => $PositionId
			])->all();

        // company

        $allcompany = PostJob::find()->select([
            'CompanyName'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'PostJob.JobStatus' => 0,
            'PostJob.PostFor' => $postfor
        ]) ->joinWith([
			'employer',
			
		]) ->andFilterWhere([
			'AllUser.EntryType' => 'Company'
		])->groupBy([
            'CompanyName'
        ])->all();
			
		$allHR = PostJob::find()->select([
            'CompanyName'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'PostJob.JobStatus' => 0,
            'PostJob.PostFor' => $postfor
        ]) ->joinWith([
			'employer',
			
		]) ->andFilterWhere([
			'AllUser.EntryType' => 'HR'
		])->groupBy([
            'CompanyName'
        ])->all();	
		
		$allConsultancy = PostJob::find()->select([
            'CompanyName'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'PostJob.JobStatus' => 0,
            'PostJob.PostFor' => $postfor
        ]) ->joinWith([
			'employer',
			
		]) ->andFilterWhere([
			'AllUser.EntryType' => 'Consultancy'
		])->groupBy([
            'CompanyName'
        ])->all();	

        //

        // hot categories

        $hotcategory = $postjob->find()
            ->select([
            'count(*) as cnt',
            'Industry.IndustryName as CategoryName',
            'Industry.IndustryId as JobCategoryId'
        ])
            ->joinWith([
            'jobCategory'
        ])
            ->where([
            'PostJob.IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor,
            'Industry.IsDelete' => 0
        ])
            ->groupBy([
            'PostJob.JobCategoryId'
        ])
            ->all();

        // location list

        $location = $postjob->find()
            ->select([
            'Location'
        ])
            ->where([
            'IsDelete' => 0,
            'JobStatus' => 0
        ])
            ->groupBy([
            'Location'
        ])
            ->all();

        $allcategory = Industry::find()->where("IsDelete=0")
            ->orderBy([
            'IndustryName' => SORT_ASC
        ])
            ->all();

        $allcourse = Course::find()->where("IsDelete=0")
            ->orderBy([
            'CourseName' => 'SORT_ASC'
        ])
            ->all();
			
		$whiteCategoryList =  ArrayHelper::map(WhiteCategory::find()->where([
                'status' => 1
            ])
                ->orderBy([
                'name' => 'SORT_ASC'
            ])
                ->all(), 'id', 'name');	

        $jbpst = new PostJob();

        $NaukariQualData = NaukariSpecialization::find()->all();

        $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3")->all();		
		
		$qualifications = NaukariQualData::find()->where("parent_id=0")->all();
        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('jobsearch_ajax', array(
                'jbpst' => $jbpst,
                'alljob' => $alljob,
                'pages' => $pages
            ));
        } else {
            return $this->render('jobsearch', [
                'NaukariQualData' => $NaukariQualData,
                'naukari_specialization' => $naukari_specialization,
                'qualifications' => $qualifications,
                'jbpst' => $jbpst,
                'alljob' => $alljob,
                'pages' => $pages,
                'hotcategory' => $hotcategory,
                'role' => $allposition,
                'allskill' => $allskill,
                'allcompany' => $allcompany,
                'location' => $location,
                'industry' => $allcategory,
                'course' => $allcourse,
                'education' => $education,
				'whiteCategories' => $whiteCategoryList,
				'allConsultancy' => $allConsultancy,
				'allHR' => $allHR,
				'jobcategoryid' => $jobcategoryid
            ]);
        }
    }

    public function actionHirecandidate()
    {
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;
        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'walllayout';

        $state = '';
        $city = '';
        $jobtype = '';
        $industry = '';
        $skill = '';
        $salary = '';
        $education = '';
        $latest = '';
        $gender = '';
        $cresume = '';
        $role = '';
        $exp = '';

        if (isset(Yii::$app->session['Employerid'])) {

            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();

            $totalemail = $planassign->TotalEmail - $planassign->UseEmail;
        }

        $mailmodel = new Mail();

        if ($mailmodel->load(Yii::$app->request->post())) {
			
            if ($planassign->isExpired()) {
                $data = [];
                $data['message'] = 'Your Plan is Expired, Please Contact Administration';
                \Yii::$app->session->setFlash('error', $data['message']);
                return $this->redirect([
                    '/hirecandidate'
                ]);
            }

            $mailmodel->Type = 'Mail';

            $jobdet = PostJob::find()->where([
                'JobId' => $mailmodel->JobId
            ])->one();

            if ($jobdet->IsExp == 0) {

                $experience = $jobdet->Experience;
            } else if ($jobdet->IsExp == 1) {

                $experience = $jobdet->Experience . ' Years';
            } else if ($jobdet->IsExp == 2) {

                $experience = 'Both Fresher and Experience';
            }

            $location = $jobdet->Location;
			
			$slug = $jobdet->Slug;
			
            $mailmodel->Experience = $experience;

            $mailmodel->Location = $location;

            if ($mailmodel->save(false)) {

                $sender = AllUser::find()->where([
                    'UserId' => Yii::$app->session['Employerid']
                ])->one();

                $mailtext = $mailmodel->MailText;

                $email = trim($mailmodel->EmployeeId, "|");

                $to = explode("|", $email);

                $to_mail_userid = explode("|", trim(Yii::$app->request->post()['Mail']['UserId'], "|"));

                $allMailsUser = PlanRecord::find()->select(['UserId'])->where(['AND',['EmployerId' => Yii::$app->session['Employerid']],['type'=>'Email'],['PlanId' => $planassign->PlanId]])->all();
                $allMailUserArray = array_column($allMailsUser, 'UserId');
                
                $check=[];
                if(!empty($allMailUserArray)){
                    $check = array_values(array_diff($to,$allMailUserArray));
                }else{
                    $check = $to;
                }
                $checkDailyLimit = $planassign->getDailyLimit(count($to));
                if ($checkDailyLimit['status'] == 'NOK') {
                    \Yii::$app->session->setFlash('error', $checkDailyLimit['message']);
                    return $this->redirect([
                        '/hirecandidate'
                    ]);
                }


                $subject = $mailmodel->Subject;

                $senderemail = $sender->Email;

                $sendername = $sender->Name;

                $jobid = $mailmodel->JobId;

               
               $mailgo = ($totalemail > count($to)) ? count($to) : count($to) - $totalemail;
                $m = 0;

                if ($mailgo > 0) {

                    if (! empty($to_mail_userid)) {

                        $j = 0;

                        foreach ($to_mail_userid as $key => $value1) {
                            $planrecord_cnt = PlanRecord::find()->where([
                                'EmployerId' => Yii::$app->session['Employerid'],
                                'UserId' => $value1,
                                'Type' => 'Email',
                                'PlanId' => $planassign->PlanId
                            ])->count();
                           if (empty($planrecord_cnt)) {
                                	$planrecord = new PlanRecord();
        							$planrecord->PlanId = $planassign->PlanId;
        							$planrecord->EmployerId = $planassign->EmployerId;
        							$planrecord->Type = 'Email';
        							$planrecord->UserId = $value1;
        							$planrecord->Amount = 1;
        							$planrecord->UpdatedDate = date('Y-m-d H:i:s');
        							$planrecord->save(false);
        							$j ++;
                            }
						
                        }
                        $planassign->UseEmail = $planassign->UseEmail + $j;
                        $planassign->save(false);
                    }

                    foreach ($to as $key => $value) {
                        $m ++;
                        if ($m <= $mailgo) {
                            $receiver = AllUser::find()->select([
                                'Name'
                            ])
                                ->Where([
                                'Email' => $value
                            ])
                                ->one();
                            $receivername = $receiver->Name;

                            $html = "<html>

                    <head>

                    <title>$subject</title>

                    <style>

                    .im {

                    color:#4d4c4c!important;        

                    }

                    </style>

                    </head>

                    <body>

                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;line-height: 1.6;'>

                     <tbody><tr>

                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career bugs' alt='My Career bugs' style='margin-top:10px;width:200px;'></td>

                                        </tr>

                                       

                    <tr>

                                            <td><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:3%;margin:auto;color: #333;text-align: left;font-size:12px'>

                            The sender of this email is registered with mycareerbugs.com as $sendername. To respond back directly to the Employer, please click on Reply button, or send an email to $senderemail 

                    </h2>

                    </td>

                    </tr>

                    

                    <tr>

                    <td>

                    <table style='width:91%;height:auto;color:#4d4c4c;background:#fff;text-align:center;text-align: left;padding: 4%;padding-top: 0%;font-family: arial;font-size: 13px;margin-left: 4.5%;line-height: 1.8;'>

                   <tr>

                    <td><b>Experience required for the Job:</b> $experience </td>

                   </tr>

                   <tr>

                    <td><b>Job Location:</b> $location  <br/><br/></td>

                   </tr>

                   <tr>

                    <td>

                    <a href='https://mycareerbugs.com/job/$slug'><input type='button' value='Apply Now' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;border: none;'/></a>

                    <a href='mailto:$senderemail'><input type='button' value='Reply' style='padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: left;margin-left: 20px;border: none;'/></a>

                    </td>

                   </tr>

                   <tr>

                    <td><br/> <b>   Dear $receivername,</b> <br/><br/></td>

                   </tr>

                   <tr>

                    <td>

                    $mailtext

                    </td>

                   </tr>

                    </table>

                    </td>

                    </tr>

                    <tr>

                    <td width='275' bgcolor='#f4f8fe' style='border:1px solid #e7f0fe;font:bold 13px Arial,Helvetica,sans-serif;color:#5d6f8a;padding:10px;margin:0 auto'>Is this job relevant to you? <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;margin:0 5px;font-weight:bold' href='https://mycareerbugs.com/site/jobrelevant?type=yes&email=$value&JobId=$jobid'>Yes</a> <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;font-weight:bold' href='https://mycareerbugs.com/site/jobrelevant?type=no&email=$value&JobId=$jobid'>No</a></td>

                   </tr>

                    <tr>

                    <td>

                    <p style='font-size: 11px;color: #333;'>Your feedback would help us in sending you the most relevant job opportunities</p>

                    </td>

                    </tr>

                    <tr>

                                        <td style='border-bottom: 1px solid #ccc;'></td>

                    </tr>

                    <td>

                    <p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'><b>Disclaimer:</b></p>

                    </td>

                    </tr>

                    <tr>

                                        <td style='border-bottom: 1px solid #333;'></td>

                    </tr>

                    <tr>

                    <td><p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'>

                    The sender of this email is registered with <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>www.mycareerbugs.com</a> as  registered user using <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> services. The responsibility of checking the authenticity of offers/correspondence lies with you. If you consider the content of this email inappropriate or spam, you may: Forward this email to: compliance@mycareerbugs.com. <br/>

<b>Please Note:</b> This mail is a private message from the recruiter. We have enabled auto login for your convenience, you are strongly advised not to forward this email to protect your account from unauthorized access.<br/>

<b>Advisory:</b> Please do not pay any money to anyone who promises to find you a job. This could be in the form of a registration fee or document processing fee or visa charges or any other pretext. The money could be asked for upfront or it could be asked after trust has been built after some correspondence has been exchanged. Also please note that in case you get a job offer or a letter of intent without having been through an interview process it is probably a scam and you should contact compliance@mycareerbugs.com for advise.

                    </p></td>

                    </tr>

                     </tbody>

                    </table>

                    </body>

                    </html>";

                            $mail = new ContactForm();

                            $mail->sendEmail($value, $senderemail, $html, $subject);
                        }
                    }
                    Yii::$app->session->setFlash('success', "Mail sent Successfully");
                } else {
                    Yii::$app->session->setFlash('error', "Your Data plan is already Used");
                }
            } else {
                // var_dump($mailmodel->getErrors());
                Yii::$app->session->setFlash('error', "There is some error please try again");
            }

            return $this->redirect([
                'hirecandidate'
            ]);
        } else {
			/*echo  '<pre>';
			print_r(Yii::$app->request->post());
			die;*/
            $alluser = new AllUser();
            $keyname = '';
            $indexlocation = '';
            $experience = '';
            $category = '';
			$preferred_location = "";
            if (isset(Yii::$app->request->post()['candidatesearch'])) {
                $keyname = Yii::$app->request->post()['keysearch'];
                $indexlocation = Yii::$app->request->post()['location'];
                $experience = Yii::$app->request->post()['experience'];
                $category = Yii::$app->request->post()['category'];
                $allcandidate = $alluser->find()
                    ->where([
                    'AllUser.IsDelete' => 0,
                    'VerifyStatus' => 1,
                    'UserTypeId' => 2,
                    'IsApprove' => 1
                ])
                    ->joinWith([
                    'experiences',
                    'experiences.industry',
                    'educations'
                ])
                    ->andFilterWhere([
                    'or',
                    [
                        'like',
                        'AllUser.Name',
                        $keyname
                    ],
					[
                        'like',
                        'Industry.IndustryName',
                        $keyname
                    ],

                    [
                        'like',
                        'Education.HighestQualification',
                        $keyname
                    ],
					
					['like','AllUser.State',$indexlocation],
					['like','AllUser.City',$indexlocation]
                ])
                    ->orWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,State)'))
                    ->andFilterWhere([
                    // 'State' => $indexlocation,
					'Experience.Experience' => $experience,
                    'Experience.PositionId' => $category
                ])
                    ->groupBy([
                    'AllUser.UserId'
                ])
                    ->addParams([
                    ':cat_to_find' => $indexlocation
                ])
                    ->orderBy([
                    'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                ]);
            } elseif (isset(Yii::$app->request->get()['stype'])) {

                // fresher search

                if (Yii::$app->request->get()['stype'] == 'fresher') {

                    if (isset(Yii::$app->request->get()['state'])) {

                        $state = Yii::$app->request->get()['state'];
                    }
					
					if (isset(Yii::$app->request->get()['state'])) {

                        $city = Yii::$app->request->get()['city'];
                    }
					
					if (isset(Yii::$app->request->get()['preferred-location'])) {

                        $preferred_location = Yii::$app->request->get()['preferred-location'];
                    }

                    if (isset(Yii::$app->request->get()['education'])) {

                        $education = Yii::$app->request->get()['education'];
                    }

                    if (isset(Yii::$app->request->get()['specialization'])) {

                        $naukari_specialization = Yii::$app->request->get()['specialization'];
                    }

                    if (isset(Yii::$app->request->get()['course'])) {

                        $naukari_course = Yii::$app->request->get()['course'];
                    }

                    if (isset(Yii::$app->request->get()['latest'])) {

                        $latest = Yii::$app->request->get()['latest'];
                    }

                    if (isset(Yii::$app->request->get()['gender']) && Yii::$app->request->get()['gender'] != '') {

                        $gender = explode(",", Yii::$app->request->get()['gender']);
                    }

                    if (isset(Yii::$app->request->get()['cresume']) && Yii::$app->request->get()['cresume'] != '') {

                        $cresume = Yii::$app->request->get()['cresume'];
                    }

                    if (isset(Yii::$app->request->get()['language']) && Yii::$app->request->get()['language'] != '') {

                        $language = Yii::$app->request->get()['language'];

                        $language_ary = explode(',', $language);
                    }

                    $ddate = date('Y-m-d', strtotime("-$latest days"));

                    if ($cresume == 1) {

                        $allcandidate = $alluser->find()
                            ->where([
                            'AllUser.IsDelete' => 0,
                            'VerifyStatus' => 1,
                            'UserTypeId' => 2,
                            'IsExp' => 0,
                            'IsApprove' => 1
                        ])
                            ->joinWith([
                            'educations',
                            'skilldetail',
                            'experiences'
                        ])
                            ->andFilterWhere([

                            'State' => $state,

                            'Gender' => $gender
                        ])
                            ->
                        // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                        andWhere([
                            '!=',
                            'CVId',
                            0
                        ])
                            ->groupBy([
                            'AllUser.UserId'
                        ])
                            ->orderBy([
                            'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                        ]);
                    } else {

                        $allcandidate = $alluser->find()
                            ->where([
                            'AllUser.IsDelete' => 0,
                            'VerifyStatus' => 1,
                            'UserTypeId' => 2,
                            'IsExp' => 0,
                            'IsApprove' => 1
                        ])
                            ->joinWith([
                            'educations',
                            'skilldetail',
                            'experiences'
                        ])
                            ->andFilterWhere([

                            'State' => $state,
								
                            'Gender' => $gender
                        ])
                            ->
                        // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                        groupBy([
                            'AllUser.UserId'
                        ])
                            ->orderBy([
                            'AllUser.UserID' => SORT_DESC
                        ]);
                    }

                    if (! empty($education)) {

                        $allcandidate->andWhere([
                            'Education.highest_qual_id' => $education
                        ]);
                    }
					
					if (! isset($preferred_location) && !empty($preferred_location)) {

                        $allcandidate->andWhere([
                            'AllUser.PreferredLocation' => $preferred_location
                        ]);
                    }
                    if (isset(Yii::$app->request->get()['latest']) && ! empty(Yii::$app->request->get()['latest'])) {

                        $allcandidate->andFilterWhere([
                            '>=',
                            'DATE( AllUser.UpdatedDate )',
                            $ddate
                        ]);
                    }

                    if (isset($naukari_specialization) && ! empty($naukari_specialization)) {

                        $allcandidate->andFilterWhere([
                            'Education.specialization_id' => $naukari_specialization
                        ]);
                    }

                    if (isset($naukari_course) && ! empty($naukari_course)) {

                        $allcandidate->andFilterWhere([
                            'Education.course_id' => $naukari_course
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['skill']) && Yii::$app->request->get()['skill'] != '') {

                        $skill = explode(",", Yii::$app->request->get()['skill']);

                        $allcandidate->andFilterWhere([
                            'EmployeeSkill.SkillId' => $skill
                        ]);
                    }
					
					if (isset(Yii::$app->request->get()['state']) && Yii::$app->request->get()['state'] != '') {

                        $allcandidate->andFilterWhere([
                            'AllUser.State' => Yii::$app->request->get()['state']
                        ]);
                    }
					
					
					if (isset(Yii::$app->request->get()['city']) && Yii::$app->request->get()['city'] != '') {

                        $allcandidate->andFilterWhere([
                            'AllUser.City' => Yii::$app->request->get()['city']
                        ]);
                    }
					

                    if (isset(Yii::$app->request->get()['role']) && Yii::$app->request->get()['role'] != '') {

                        $role = Yii::$app->request->get()['role'];

                        $allcandidate->andFilterWhere([
                            'EmployeeSkill.SkillRoleId' => $role
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'Y') {

                        $allcandidate->andWhere([
                            '!=',
                            'AllUser.whatsappno',
                            " "
                        ]);
                    }

                    if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'N') {

                        $allcandidate->andWhere([
                            '=',
                            'AllUser.whatsappno',
                            " "
                        ]);
                    }
                } // fresher search

                // experience search

                else {
					
                    if (isset(Yii::$app->request->get()['state'])) {
                        $state = Yii::$app->request->get()['state'];
                    }
					
					if (isset(Yii::$app->request->get()['city'])) {

                        $city = Yii::$app->request->get()['city'];
                    }
					
					if (isset(Yii::$app->request->get()['preferred-location'])) {
                        $preferred_location = Yii::$app->request->get()['preferred-location'];
                    }
                    if (isset(Yii::$app->request->get()['education'])) {
                        $education = Yii::$app->request->get()['education'];
                    }

                    if (isset(Yii::$app->request->get()['latest'])) {
                        $latest = Yii::$app->request->get()['latest'];
                    }

                    if (isset(Yii::$app->request->get()['gender']) && Yii::$app->request->get()['gender'] != '') {
                        $gender = explode(",", Yii::$app->request->get()['gender']);
                    }

                    if (isset(Yii::$app->request->get()['salaryrange']) && Yii::$app->request->get()['salaryrange'] != '') {

                        $salary = explode(",", Yii::$app->request->get()['salaryrange']);
                    }

                    if (isset(Yii::$app->request->get()['jobtype']) && Yii::$app->request->get()['jobtype'] != '') {

                        $jobtype = explode(",", Yii::$app->request->get()['jobtype']);
                    }

                    if (isset(Yii::$app->request->get()['industry']) && Yii::$app->request->get()['industry'] != '') {

                        $industry = explode(",", Yii::$app->request->get()['industry']);
                    }

                    if (isset(Yii::$app->request->get()['skill']) && Yii::$app->request->get()['skill'] != '') {

                        $skill = explode(",", Yii::$app->request->get()['skill']);
                    }

                    if (isset(Yii::$app->request->get()['role']) && Yii::$app->request->get()['role'] != '') {

                        $role = Yii::$app->request->get()['role'];
                    }

                    if (isset(Yii::$app->request->get()['experience']) && Yii::$app->request->get()['experience'] != '') {

                        $exp = Yii::$app->request->get()['experience'];
                    }

                    if (isset(Yii::$app->request->get()['cresume']) && Yii::$app->request->get()['cresume'] != '') {

                        $cresume = Yii::$app->request->get()['cresume'];
                    }

                    if (isset(Yii::$app->request->get()['specialization'])) {

                        $naukari_specialization = Yii::$app->request->get()['specialization'];
                    }

                    if (isset(Yii::$app->request->get()['course'])) {

                        $naukari_course = Yii::$app->request->get()['course'];
                    }
					
					 

                    if (isset(Yii::$app->request->get()['language']) && Yii::$app->request->get()['language'] != '') {

                        $language = Yii::$app->request->get()['language'];

                        $language_ary = explode(',', $language);
                    }

                    $ddate = date('Y-m-d', strtotime("-$latest days"));
					
                    if ($role != '' && $exp != '') {
						
                        if ($cresume == 1) {

                            $allcandidate = $alluser->find()
                                ->where([
                                'AllUser.IsDelete' => 0,
                                'VerifyStatus' => 1,
                                'UserTypeId' => 2,
                                'IsExp' => 1,
                                'IsApprove' => 1
                            ])
                                ->joinWith([
                                'educations',
                                'skilldetail',
                                'experiences'
                            ])
                                ->andWhere([
                                '!=',
                                'CVId',
                                0
                            ])
                                ->andFilterWhere([
                                'State' => $state,
                                'City' => $city,
                                'Gender' => $gender,
                                //'PreferredLocation' => $preferred_location,
                                'Experience.IndustryId' => $industry,
                                //'Experience.PositionId' => $role,
                                //'EmployeeSkill.SkillRoleId' => $role,
                                'Experience.Experience' => $exp,
                                'EmployeeSkill.SkillId' => $skill
                            ])
                               
                            ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
							 ->
                            groupBy([
                                'AllUser.UserId'
                            ])
                                ->orderBy([
                                'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                            ]);
                        } else {

                            $allcandidate = $alluser->find()
                                ->where([
                                'AllUser.IsDelete' => 0,
                                'VerifyStatus' => 1,
                                'UserTypeId' => 2,
                                'IsExp' => 1,
                                'IsApprove' => 1
                            ])
                                ->joinWith([
                                'educations',
                                'skilldetail',
                                'experiences'
                            ])
                                ->andFilterWhere([
                                'State' => $state,
                                'City' => $city,
                               // 'PreferredLocation' => $preferred_location,
                                'Gender' => $gender,
                                'Experience.IndustryId' => $industry,
                                //'EmployeeSkill.SkillRoleId' => $role,
                                'Experience.Experience' => $exp,
                                'EmployeeSkill.SkillId' => $skill
                            ]) ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
                                ->
                            // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                            groupBy([
                                'AllUser.UserId'
                            ])
                                ->orderBy([
                                'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                            ]);
                        }
                    } else {
						//echo $role;die;
                        if ($exp != '') {
							
                            if ($cresume == 1) {
                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 2,
                                    'IsExp' => 1,
                                    'IsApprove' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences'
                                ])
                                    ->andWhere([
                                    '!=',
                                    'CVId',
                                    0
                                ])
                                    ->andFilterWhere([

                                    'State' => $state,
									'City' => $city,
                                    //'PreferredLocation' => $preferred_location,

                                    'Gender' => $gender,

                                    'Experience.IndustryId' => $industry,
                                    //'Experience.PositionId' => $role,

                                    //'EmployeeSkill.SkillRoleId' => $role,

                                    'Experience.Experience' => $exp,

                                    'EmployeeSkill.SkillId' => $skill
                                ])
								 ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
                                    ->
                                // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                                groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC, 
									'AllUser.UserId' => SORT_DESC,
									
                                    'Experience.Experience' => SORT_DESC
                                ]);
                            } else {

                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 2,
                                    'IsExp' => 1,
                                    'IsApprove' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences'
                                ])
                                    ->andFilterWhere([
                                    'State' => $state,
									'City' => $city,
                                    //'PreferredLocation' => $preferred_location,
                                    'Gender' => $gender,
                                    'Experience.IndustryId' => $industry,
                                    //'Experience.PositionId' => $role,
                                    //'EmployeeSkill.SkillRoleId' => $role,
                                    'Experience.Experience' => $exp,
                                    'EmployeeSkill.SkillId' => $skill
                                ])
                                    ->andFilterWhere([
                                    '>=',
                                    'DATE( AllUser.UpdatedDate )',
                                    $ddate
                                ])  ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
                                    ->groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC, 
									'AllUser.UserId' => SORT_DESC,
                                    'Experience.Experience' => SORT_DESC
                                ]);
                            }
                        } else {
							
                            if ($cresume == 1) {
                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 2,
                                    'IsExp' => 1,
                                    'IsApprove' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences'
                                ])
                                    ->andWhere([
                                    '!=',
                                    'CVId',
                                    0
                                ])
                                    ->andFilterWhere([

                                    'State' => $state,
									'City' => $city,
                                    //'PreferredLocation' => $preferred_location,

                                    'Gender' => $gender,

                                    'Experience.IndustryId' => $industry,
									//'Experience.PositionId' => $role,
                                    //'EmployeeSkill.SkillRoleId' => $role,

                                    // 'Experience.Experience'=>$exp,

                                    'EmployeeSkill.SkillId' => $skill
                                ])
								 ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
                                    ->
                                // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])
			
                                groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                                ]);
                            } else {
								
                                $allcandidate = $alluser->find()
                                    ->where([
                                    'AllUser.IsDelete' => 0,
                                    'VerifyStatus' => 1,
                                    'UserTypeId' => 2,
                                    'IsExp' => 1,
                                    'IsApprove' => 1
                                ])
                                    ->joinWith([
                                    'educations',
                                    'skilldetail',
                                    'experiences'
                                ])
                                    ->andFilterWhere([

                                    'State' => $state,
									'City' => $city,
                                    //'PreferredLocation' => $preferred_location,

                                    'Gender' => $gender,

                                    'Experience.IndustryId' => $industry,
									
                                    //'Experience.PositionId' => $role,

                                    //'EmployeeSkill.SkillRoleId' => $role,

                                    // 'Experience.Experience'=>$exp,

                                    'EmployeeSkill.SkillId' => $skill
                                ])
								 ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
                                    ->
                                // ->andFilterWhere(['>=', 'DATE( AllUser.UpdatedDate )', $ddate])

                                groupBy([
                                    'AllUser.UserId'
                                ])
                                    ->orderBy([
                                    'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                                ]);
                            }
                        }
                    }
                }

                // experience search
            } else {

                $allcandidate = $alluser->find()
                    ->where([
                    'AllUser.IsDelete' => 0,
                    'IsExp' => 0,
                    'VerifyStatus' => 1,
                    'UserTypeId' => 2,
                    'IsApprove' => 1
                ])
                    ->joinWith([
                    'educations',
					'skilldetail'					
                ])
                    ->andFilterWhere([

                    'State' => $state,
					'City' => $city,
                    //'PreferredLocation' => $preferred_location,

                    'Gender' => $gender,

                    'Experience.IndustryId' => $industry,

                    'EmployeeSkill.SkillId' => $skill
                ])
				 ->andFilterWhere(['Like', 'PreferredLocation', $preferred_location])
                    ->groupBy([
                    'AllUser.UserId'
                ])
                    ->orderBy([
                    'AllUser.OnDate' => SORT_DESC, 'AllUser.UserId' => SORT_DESC
                ]);
            }
            if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'Y') {

                $allcandidate->andWhere([
                    '!=',
                    'AllUser.whatsappno',
                    " "
                ]);
            }
            if (! empty($education)) {

                $allcandidate->andWhere([
                    'Education.highest_qual_id' => $education
                ]);
            }
			
			//'Experience.PositionId' => $role,

           //'EmployeeSkill.SkillRoleId' => $role,
			if(!empty($role)){
				$allcandidate->andWhere(
					['or',
					['Experience.PositionId' => $role],                           
					['EmployeeSkill.SkillRoleId' => $role] 
				]);
					
			}					
			
			
			if (! isset($preferred_location) && !empty($preferred_location)) {

                        $allcandidate->andWhere([
                            'LIKE', 'AllUser.PreferredLocation', $preferred_location
                        ]);
			}
			
            if (isset(Yii::$app->request->get()['whatsapp_contact']) && Yii::$app->request->get()['whatsapp_contact'] == 'N') {

                $allcandidate->andWhere([
                    '=',
                    'AllUser.whatsappno',
                    " "
                ]);
            }
            if (! empty($salary)) {
                $tmp_ary = array(
                    'or'
                );
                foreach ($salary as $key => $value) {
                    $tmp_ary[] = [
                        'Experience.Salary' =>
                        $value
                    ];
                }
				
                $allcandidate->andFilterWhere($tmp_ary);
            }

            if (isset(Yii::$app->request->get()['latest']) && ! empty(Yii::$app->request->get()['latest'])) {

                $allcandidate->andFilterWhere([
                    '>=',
                    'DATE( AllUser.UpdatedDate )',
                    $ddate
                ]);
            }

            if (isset($naukari_specialization) && ! empty($naukari_specialization)) {

                $allcandidate->andFilterWhere([
                    'Education.specialization_id' => $naukari_specialization
                ]);
            }

            if (isset($naukari_course) && ! empty($naukari_course)) {

                $allcandidate->andFilterWhere([
                    'Education.course_id' => $naukari_course
                ]);
            }

            if (isset(Yii::$app->request->get()['condidateage']) && Yii::$app->request->get()['condidateage'] != '') {

                $condidateage = Yii::$app->request->get()['condidateage'];

                $allcandidate->andFilterWhere([
                    '<=',
                    'AllUser.Age',
                    $condidateage
                ]);
            }

            if (isset(Yii::$app->request->get()['worktype']) && Yii::$app->request->get()['worktype'] != '') {

                $worktype = Yii::$app->request->get()['worktype'];

                $allcandidate->andFilterWhere([
                    'AllUser.WorkType' => $worktype
                ]);
            }
			
			/*Collar Type search*/
			if (isset(Yii::$app->request->get()['collar_type']) && Yii::$app->request->get()['collar_type'] != '') {

                $collarType = Yii::$app->request->get()['collar_type'];
				
				if($collarType == 'white'){
					 $allcandidate->andFilterWhere([
						'AllUser.CollarType' => $collarType
					]);
				}else{
					$allcandidate->andWhere([
						'or', ['IS', 'AllUser.CollarType', NULL], ['AllUser.CollarType' => $collarType]
					]);
					
				}
			}
			
			
			
			if (isset(Yii::$app->request->get()['category_id']) && Yii::$app->request->get()['category_id'] != '') {

                $categoryId = Yii::$app->request->get()['category_id'];
				$userIds1 = ArrayHelper::map(UserJobCategory::find()->where([
											'category_id' => $categoryId
										])->all(), 'user_id', 'user_id');
				
				$userIds2 = ArrayHelper::map(Experience::find()->where([
									'CategoryId' => $categoryId
								])->all(), 'UserId', 'UserId');
								
							
				if(!empty($userIds1) && !empty($userIds2)){
					$userIds = array_merge($userIds1, $userIds2);
					
				}elseif(!empty($userIds1) ){
					$userIds = $userIds1;
				}elseif(!empty($userIds2)){
					$userIds = $userIds2;
				}else{
					$userIds = array();
				}			
										
				if(!empty($userIds)){
					$allcandidate->andFilterWhere([
						'AllUser.UserId' => $userIds
					]);
				}
				
			}
			
			if (isset(Yii::$app->request->get()['collar_role']) && Yii::$app->request->get()['collar_role'] != '') {

                $collarRole = Yii::$app->request->get()['collar_role'];
				if(!empty($collarRole)){
					$collarRoleId = explode(',', $collarRole);
					$userIds1 = ArrayHelper::map(UserWhiteRole::find()->where([
											'role_id' => $collarRoleId
										])->all(), 'user_id', 'user_id');
										
					$userIds2 = ArrayHelper::map(ExperienceWhiteRole::find()->where([
											'role_id' => $collarRoleId
										])->all(), 'user_id', 'user_id');	
					if(!empty($userIds1) && !empty($userIds2)){
						$userIds = array_merge($userIds1, $userIds2);
						
					}elseif(!empty($userIds1) ){
						$userIds = $userIds1;
					}elseif(!empty($userIds2)){
						$userIds = $userIds2;
					}else{
						$userIds = array();
					}
										
					if(!empty($userIds)){
						$allcandidate->andFilterWhere([
							'AllUser.UserId' => $userIds
						]);
					}					
				}				
					
			}
			
			if (isset(Yii::$app->request->get()['collar_skills']) && Yii::$app->request->get()['collar_skills'] != '') {

                $collarSkills = Yii::$app->request->get()['collar_skills'];
				if(!empty($collarSkills)){
					$skillId = explode(',', $collarSkills);
					$userIds1 = ArrayHelper::map(UserWhiteSkill::find()->where([
											'skill_id' => $skillId
										])->all(), 'user_id', 'user_id');
					$userIds2 = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
											'skill_id' => $skillId
										])->all(), 'user_id', 'user_id');	
									
					if(!empty($userIds1) && !empty($userIds2)){
						$userIds = array_merge($userIds1, $userIds2);
						
					}elseif(!empty($userIds1) ){
						$userIds = $userIds1;
					}elseif(!empty($userIds2)){
						$userIds = $userIds2;
					}else{
						$userIds = array();
					}
					
					
					$allcandidate->andWhere([
						'AllUser.UserId' => $userIds
					]);				
				}
				
				
			}
			
			
			

            if (isset(Yii::$app->request->get()['preferred-location']) && Yii::$app->request->get()['preferred-location'] != '') {

                $PreferredLocation = explode(',', Yii::$app->request->get()['preferred-location']);

                foreach ($PreferredLocation as $pl) {

                    $allcandidate->andFilterWhere([
                        'OR',
                        [
                            'LIKE',
                            'AllUser.PreferredLocation',
                            $pl
                        ]
                    ]);
                }
            }
            if (isset($language_ary) && ! empty($language_ary)) {
                $tmp_ary = array(
                    'or'
                );
                foreach ($language_ary as $key => $value) {
                    $tmp_ary[] = [
                        'like',
                        'AllUser.language',
                        $value
                    ];
                }
                $allcandidate->andFilterWhere($tmp_ary);
            }
            //echo $allcandidate->createCommand()->getRawSql(); die;

            $pages = new Pagination([
                'totalCount' => $allcandidate->count()
            ]);

            if (isset(Yii::$app->request->get()['perpage'])) {

                $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
            } else {

                $pages->defaultPageSize = 10;
            }
			/*echo "<pre>";
             print_r($allcandidate->createCommand()->rawSql);
            die();/**/
			//echo $allcandidate->createCommand()->getRawSql(); die;
            $allcandidate = $allcandidate->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            //echo $allcandidate->createCommand()->getRawSql(); die;

            $allposition = Position::find()->select([
                'Position',
                'PositionId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'Position' => SORT_ASC
            ])
                ->all();

            $allindustry = Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all();

            $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();

            // skill

            $allskill = Skill::find()->where([
                'IsDelete' => 0
            ])->all();

            $allcourse = Course::find()->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();

            $allpost = ArrayHelper::map(PostJob::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'JobStatus' => 0,
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all(), 'JobId', 'JobTitle');

            $cities = Cities::find()->all();

            $naukari_specialization = NaukariSpecialization::find()->all();

            $language_opts = LanguagesOpts::find()->all();

            $plan = new Plan();

            $allplan = $plan->getAllplan();

            $planassign = new PlanAssign();

            $isassign = $planassign->isAssign(Yii::$app->session['Employerid']);

            if (Yii::$app->request->isAjax) {

                $salaryrange = array();

                return $this->renderPartial('candidate_filter_ajax', array(
                    'candidate' => $allcandidate,
                    'pages' => $pages,
                    'allplan' => $allplan,
                    'planassign' => $planassign,
                    'url' => $url,
                    'imageurl' => $imageurl,
                    'isassign' => $isassign,
                    'salaryrange' => $salaryrange
                ));				
				
            } else {
				$jbpst = new PostJob();
				$whiteCategories =  ArrayHelper::map(WhiteCategory::find()->where([
					'status' => 1
				])
					->orderBy([
					'name' => 'SORT_ASC'
				])
                ->all(), 'id', 'name');
                return $this->render('hirecandidate', [
                    'language_opts' => $language_opts,
                    'allplan' => $allplan,
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'candidate' => $allcandidate,
                    'pages' => $pages,
                    'allcategory' => $allposition,
                    'allindustry' => $allindustry,
                    'isassign' => $isassign,
                    'allskill' => $allskill,
                    'mailmodel' => $mailmodel,
                    'course' => $allcourse,
                    'allpost' => $allpost,
                    'cities' => $cities,
                    'keyname' => $keyname,
                    'indexlocation' => $indexlocation,
                    'experience' => $experience,
                    'category' => $category,
					'jbpst' => $jbpst,
					'whiteCategories' => $whiteCategories
                ]);
            }
        }
    }

    public function actionJobrelevant($type, $email, $JobId)
    {
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;
        $model = new JobRelevant();
        $model->Email = $email;
        $model->JobId = $JobId;
        $model->Type = $type;
        $model->save(false);
        if ($type == 'no') {
            return $this->render('jobsfeedback', [
                'type' => 'no'
            ]);
        }
        return $this->render('jobsfeedback', [
            'type' => 'yes'
        ]);
    }

   public function actionCvdownload()
    {
        $data = [];
        $data['status'] = 'OK';
        $userId = !empty($_GET['UserId']) ? $_GET['UserId'] : '';

        if(empty($userId))
        {
          echo json_encode(['status'=>'error','message'=>'something is wrong']);
          exit;
        }

        $planassign = PlanAssign::find()->where([
        'EmployerId' => Yii::$app->session['Employerid'],
        'IsDelete' => 0
        ])->one();

         if ($planassign->isExpired()) {
            $data['status'] = 'error';
            $data['message'] = 'Your plan has expired, Please Contact Admin';
            echo json_encode($data);
            exit;
        }

        $planRecordModel = Plan::find()->where(['PlanId'=>$planassign->PlanId])->select(['total_daily_cv_download'])->one();

        $todayusedCVDownload = PlanRecord::find()->where([
            'PlanId'=>$planassign->PlanId,
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'CVDownload',
            ])->count();

        if($planRecordModel->total_daily_cv_download > $todayusedCVDownload)
        {
          $planrecord_cnt = PlanRecord::isAlreadyRecord($planassign, $_GET['UserId'], 'CVDownload');
            if ($planrecord_cnt == 0) {
                PlanRecord::createNewPlanRecord($planassign, $_GET['UserId'],'CVDownload','UseCVDownload');
            }
        }else{

            $todayusedCVDownload = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'UserId'=> $userId,
                'Type' => 'CVDownload'
                
            ]);
            if(empty($todayusedCVDownload))
            {
                $data['status'] = 'error';
                $data['message'] = "You ";
                echo json_encode($data);
                exit;
            }
        }
        
        return $this->asJson($data);
        // $planassign = PlanAssign::find()->where([
        // 'EmployerId' => Yii::$app->session['Employerid'],
        // 'IsDelete' => 0
        // ])->one();
        // $planrecord_cnt = PlanRecord::find()->where([
        // 'EmployerId' => Yii::$app->session['Employerid'],
        // 'UserId' => $_GET['UserId'],
        // 'Type' => 'CVDownload',
        // 'PlanId' => $planassign->PlanId
        // ])->count();
        // if ($planrecord_cnt == 0) {
        // $planassign->UseCVDownload = $planassign->UseCVDownload + 1;
        // $planassign->save(false);
        // $planrecord = new PlanRecord();
        // $planrecord->PlanId = $planassign->PlanId;
        // $planrecord->EmployerId = $planassign->EmployerId;
        // $planrecord->Type = 'CVDownload';
        // $planrecord->Amount = 1;
        // $planrecord->UserId = $_GET['UserId'];
        // $planrecord->save(false);
        // }
    }

    public function actionWhatsappview()
    {
        $data = [];
        $planassign = PlanAssign::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'IsDelete' => 0
        ])->one();
        if ($planassign->isExpired()) {
            $data['status'] = 'NOK';
            Yii::$app->session->setFlash('error', 'Your plan has expired, Please Contact Admin');
            return $this->asJson($data);
        }
        $planrecord_cnt = PlanRecord::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'UserId' => $_GET['UserId'],
            'Type' => 'whatsappview',
            'PlanId' => $planassign->PlanId
        ])->count();
       // if ($planrecord_cnt == 0) {
            $planassign->UseWhatsApp = $planassign->UseWhatsApp + 1;
            $planassign->save(false);
            $planrecord = new PlanRecord();
            $planrecord->PlanId = $planassign->PlanId;
            $planrecord->EmployerId = $planassign->EmployerId;
            $planrecord->Type = 'whatsappview';
            $planrecord->Amount = 1;
            $planrecord->UserId = $_GET['UserId'];
			$planrecord->UpdatedDate = date('Y-m-d H:i:s');
            $planrecord->save(false);
       // }
        $data['status'] = 'OK';
        return $this->asJson($data);
    }

    public function actionContactview()
    {
        // $planassign = PlanAssign::find()->where([
        // 'EmployerId' => Yii::$app->session['Employerid'],
        // 'IsDelete' => 0
        // ])->one();
        // if ($planassign->isExpired()){

        // $data = [];
        // $data['status'] = 'NOK';
        // $data['message'] = 'Your Plan is Expired, Please Contant Administration';
        // return $this->asJson($data);
        // }
        // $planrecord_cnt = PlanRecord::isAlreadyRecord($planassign, $_GET['UserId'], 'ContactView');
        // if ($planrecord_cnt == 0) {
        // PlanRecord::createNewPlanRecord($planassign, $_GET['UserId'], 'ContactView');
        // }
    }

    public function actionExportcandidate($Eid)
    {
        $Eid = explode(",", trim($Eid, ","));

        $model = AllUser::find()->where([
            'IN',
            'UserId',
            $Eid
        ]);
       
        $planassign = PlanAssign::find()->where([
            'EmployerId' => Yii::$app->session['Employerid'],
            'IsDelete' => 0
        ])->one();
        
        $allExportUser = PlanRecord::find()->select(['UserId'])->where(['AND',['EmployerId' => Yii::$app->session['Employerid']],['type'=>'Download'],['PlanId' => $planassign->PlanId]])->all();
        $allExportUserArray = array_column($allExportUser, 'UserId');
        
        if(!empty($allExportUser)){
            $Eid = array_values(array_diff($Eid,$allExportUserArray));
        }
        
        $k = 0;
       
        $dailyDownloadLimit = $planassign->isDailyLimitExceedForDownload(count($Eid));
		
        if ($dailyDownloadLimit['status'] == 'NOK'){
            \Yii::$app->session->setFlash('error', $dailyDownloadLimit['message']);
            return $this->redirect([
                '/hirecandidate'
            ]);
        }
        if ($planassign->isExpired()) {
            $data = [];
            $data['message'] = 'Your Plan is Expired, Please Contact Administration';
            \Yii::$app->session->setFlash('error', $data['message']);
            return $this->redirect([
                '/hirecandidate'
            ]);
        }
        if (! empty($Eid) && ! empty($planassign)) {
            foreach ($Eid as $key => $value1) {
                $planrecord_cnt = PlanRecord::find()->where([
                    'EmployerId' => Yii::$app->session['Employerid'],
                    'UserId' => $value1,
                    'Type' => 'Download',
                    'PlanId' => $planassign->PlanId
                ])->count();
				
				if ($planrecord_cnt == 0) {
                    $planrecord = new PlanRecord();
                    $planrecord->PlanId = $planassign->PlanId;
                    $planrecord->EmployerId = $planassign->EmployerId;
                    $planrecord->Type = 'Download';
                    $planrecord->Amount = 1;
                    $planrecord->UserId = $value1;
					$planrecord->UpdatedDate = date('Y-m-d H:i:s');
                    $planrecord->save(false);
                    $k ++;
                }
            }
            $planassign->UseDownload = $planassign->UseDownload + $k;
            $planassign->save(false);
        }
		
		
        $exporter = new Spreadsheet([
            'dataProvider' => new ActiveDataProvider([
                'query' => $model
            ]),
            'columns' => [
                [
                    'attribute' => 'Name'
                ],
                [
                    'attribute' => 'Email'
                ],
                [
                    'attribute' => 'MobileNo'
                ],
                [
                    'attribute' => 'whatsappno'
                ],
                [
                    'attribute' => 'Address'
                ],
                [
                    'attribute' => 'State'
                ],
                [
                    'attribute' => 'City'
                ],
                [
                    'attribute' => 'PinCode'
                ],
                [
                    'header' => 'HighestQualification',
                    'value' => function ($data) {
                        return isset($data->educations[0]->course->CourseName) ? $data->educations[0]->course->CourseName : 'Not Set';
                    }
                ],
                [
                    'header' => 'University',
                    'value' => function ($data) {
                        return $data->educations[0]->University;
                    }
                ],
				[
                    'header' => 'Job Category',
                    'value' => function ($data) {
						$CategoryIds = ArrayHelper::map(UserJobCategory::find()->where(['user_id' => $data->UserId])->all(), 'category_id', 'category');
                        return (!empty($CategoryIds)?implode(', ',$CategoryIds):"");
                    }
                ],
				[
                    'header' => 'Role',
                    'value' => function ($data) {
						if($data->CollarType == "white"){
							$userRole = ArrayHelper::map(UserWhiteRole::find()->where([
														'user_id' => $data->UserId
													])->all(), 'role_id', 'role');
							return (!empty($userRole)?implode(', ',$userRole):"");						
						}else{
							return $data->getEducationRoles();
						}
                        //return $data->educations[0]->University;
                    }
                ],
				
                [
                    'header' => 'Skills',
                    'value' => function ($data) {
						
						if($data->CollarType == "white"){
							$skill_ary = ArrayHelper::map(UserWhiteSkill::find()->where([
													'user_id' => $data->UserId
												])->all(), 'skill_id', 'skill');
							return (!empty($skill_ary)?implode(', ',$skill_ary):"");					
						}else{
							$skill = '';
							foreach ($data->empRelatedSkills as $ask => $asv) {
								if (isset($asv->skill->Skill) && ! empty($asv->skill->Skill))
									$skill .= $asv->skill->Skill . ', ';
							}
							return $skill;
						}
                    }
                ],
				[
                    'header' => 'EntryType',
                    'value' => function ($data) {
                        if ($data->EntryType == "Experience" && $data->IsExp == 1) {
                            $entry = "Experience";
                        } else {
                            $entry = 'Fresher';
                        }
                        return $entry;
                    }
                ],
                [
                    'header' => 'LastSalary',
                    'value' => function ($data) {
                        if ($data->experiences) {
                            $count = count($data->experiences);
                            $salary = $data->experiences[$count - 1]->Salary . ' Lakh';
                        } else {
                            $salary = 'nil';
                        }
                        return $salary;
                    }
                ],
                [
                    'header' => 'Experience',
                    'value' => function ($data) {
                        if ($data->IsExp) {
                            $count = count($data->experiences);
                            $exp = $data->experiences[$count - 1]->Experience . ' Year';
                        } else {
                            $exp = 'nil';
                        }
                        return $exp;
                    }
                ]
            ]
        ]);
        return $exporter->send('candidate.xls');
    }

    public function actionJobseekersregisterBackup()

    {
        // echo "<pre>";print_r($_POST);die();
        if (! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Employeeid'])) {
            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';
            $skill = new Skill();
            $position = new Position();
            $course = new Course();
            $alluser = new AllUser();
            $docmodel = new Documents();
            $docmodel1 = new Documents();
            $allskill = $skill->find()
                ->where("IsDelete=0")
                ->all();

            $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();

            $allposition = $position->find()
                ->where("IsDelete=0")
                ->orderBy([
                'Position' => 'SORT_ASC'
            ])
                ->all();

            $allcourse = $course->find()
                ->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();

            $allindustry = ArrayHelper::map(Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all(), 'IndustryId', 'IndustryName');

            if ($alluser->load(Yii::$app->request->post())) {
                // echo "<pre>";print_r(Yii::$app->request->post());die();
                $count = $alluser->find()
                    ->where([
                    'Email' => $alluser->Email,
                    'IsDelete' => 0
                ])
                    ->count();

                if ($count > 0) {

                    Yii::$app->session->setFlash('error', "This Emailid Already Exist.");

                    return $this->redirect([
                        'jobseekersregister'
                    ]);
                } else {

                    $cv = UploadedFile::getInstance($alluser, 'CVId');

                    if ($cv) {

                        $cv_id = $docmodel->imageUpload($cv, 'CVId');
                    } else {

                        $cv_id = 0;
                    }

                    $alluser->CVId = $cv_id;

                    $photo_id = 0;

                    if ($docmodel1->load(Yii::$app->request->post())) {

                        $docmodel1->image = \yii\web\UploadedFile::getInstance($docmodel1, 'Doc');

                        if ($docmodel1->image) {

                            $ppp = Yii::$app->security->generateRandomString();

                            // $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";

                            $docmodel1->Doc = 'imageupload/' . $ppp . '.' . $docmodel1->image->extension;

                            $docmodel1->OnDate = date('Y-m-d');

                            $docmodel1->From = 'photo';

                            if ($docmodel1->save(false)) {
                                $docmodel1->image->saveAs(Yii::$app->params['uploadPath'] . $ppp . '.' . $docmodel1->image->extension);
                                $photo_id = $docmodel1->DocId;

                                $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));

                                $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;

                                if (file_exists($thumb)) {

                                    $document = Documents::find()->where([
                                        'DocId' => $photo_id
                                    ])->one();

                                    $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;

                                    $document->save(false);
                                }
                            } else {

                                $photo_id = 0;
                            }
                        }
                    }

                    // other course

                    if (isset(Yii::$app->request->post()['AllUser']['OtherCourse']) && Yii::$app->request->post()['AllUser']['OtherCourse'] != '') {

                        $ncourse = new Course();

                        $ncourse->CourseName = Yii::$app->request->post()['AllUser']['OtherCourse'];

                        $ncourse->OnDate = date('Y-m-d');

                        $ncourse->save(false);

                        $courseId = $ncourse->CourseId;
                    } else {

                        $courseId = Yii::$app->request->post()['AllUser']['CourseId'];
                    }

                    $alluser->UserTypeId = 2;

                    if (Yii::$app->request->post()['AllUser']['CompanyName'] != '') {

                        $alluser->EntryType = 'Experience';
                    } else {

                        $alluser->EntryType = 'Fresher';
                    }

                    // echo "<pre>";print_r(Yii::$app->request->post()['AllUser']['Language']);die();

                    $alluser->Password = md5(Yii::$app->request->post()['AllUser']['Password']);

                    if (! empty(Yii::$app->request->post()['AllUser']['Language'])) {

                        $alluser->language = implode(',', Yii::$app->request->post()['AllUser']['Language']);
                    }

                    $alluser->whatsappno = '91' . Yii::$app->request->post()['AllUser']['WhatsAppNo'];

                    $alluser->PhotoId = $photo_id;

                    $alluser->Ondate = date('Y-m-d');

                    $vkey = 'CB' . time();

                    $alluser->VerifyKey = $vkey;

                    $alluser->VerifyStatus = 1;
                    if (! empty(Yii::$app->request->post()['AllUser']['PreferredLocation'])) {
                        $alluser->PreferredLocation = implode(',', Yii::$app->request->post()['AllUser']['PreferredLocation']);
                    }

                    // register under campus

                    if (isset(Yii::$app->session['Universityid'])) {
                        $alluser->CampusId = Yii::$app->session['Universityid'];
                        $alluser->SpecId = Yii::$app->session['Specialization'];
                    }
                    // register under campus
                    // $alluser->Salary=Yii::$app->request->post()['AllUser']['Salary'];
                    $alluser->WorkType = Yii::$app->request->post()['AllUser']['WorkType'];

                    $alluser->Age = Yii::$app->request->post()['AllUser']['Age'];

                    $alluser->save(false);

                    if (isset(Yii::$app->session['Universityid'])) {
                        $campnot = new CampNotification();
                        $campnot->CampId = Yii::$app->session['Universityid'];
                        $campnot->UserId = $alluser->UserId;
                        $campnot->IsView = 0;
                        $campnot->OnDate = date('Y-m-d H:i:s');
                        $campnot->save(false);
                    }
                    // echo "<pre>";print_r($alluser);die();
                    // new skill
                    $skillidlist = '';

                    // $skilllist=trim(trim(Yii::$app->request->post()['AllUser']['RawSkill']),",");

                    $rawskill = Yii::$app->request->post()['AllUser']['SkillId'];
                    $skills_ary = array();
                    if (empty(Yii::$app->request->post()['AllUser']['SkillId'])) {
                        $rawskill = array();
                    } else {
                        $skill_s = new Skill();
                        $skills_res = $skill_s->find()
                            ->where([
                            'SkillId' => $rawskill
                        ])
                            ->all();
                        if (! empty($skills_res)) {
                            foreach ($skills_res as $keys => $values) {
                                $skills_ary[$values->SkillId] = $values->PositionId;
                            }
                        }
                    }
                    foreach ($rawskill as $rk => $skid) {
                        if ($skid != '') {
                            $skillidlist .= $skid;
                            $empskill = new EmployeeSkill();
                            if (isset($skills_ary[$skid])) {
                                $skillid_s = $skills_ary[$skid];
                            } else {
                                $skillid_s = 0;
                            }
                            $empskill->SkillRoleId = $skillid_s;
                            $empskill->SkillId = $skid;
                            $empskill->UserId = $alluser->UserId;
                            $empskill->OnDate = date('Y-m-d');
                            $empskill->save(false);
                            // var_dump($empskill->getErrors());
                        }
                    }

                    if (Yii::$app->request->post()['AllUser']['SkillId'] == '') {

                        $skil = $skillidlist;
                    } else {

                        $skil = Yii::$app->request->post()['AllUser']['SkillId'];
                    }

                    $education = new Education();

                    if (isset(Yii::$app->request->post()['AllUser']['otherSpecialization']) && ! empty(Yii::$app->request->post()['AllUser']['otherSpecialization'])) {

                        $education->specialization_id = Yii::$app->request->post()['AllUser']['otherSpecialization'];

                        $naukari_specialization = new NaukariSpecialization();

                        $naukari_specialization->name = Yii::$app->request->post()['AllUser']['otherSpecialization'];

                        $naukari_specialization->save();
                    } else {

                        $education->specialization_id = Yii::$app->request->post()['AllUser']['Specialization'];
                    }
                    $education->UserId = $alluser->UserId;
                    $education->HighestQualification = 'none';
                    $education->CourseId = $courseId;
                    $education->University = Yii::$app->request->post()['AllUser']['University'];
                    $education->PassingYear = Yii::$app->request->post()['AllUser']['DurationTo'];
                    $education->SkillId = $skil;
                    $education->DurationFrom = '';
                    // $education->highest_qual_id=Yii::$app->request->post()['AllUser']['HighestQualId'];;
                    // $education->course_id=Yii::$app->request->post()['AllUser']['CourseId'];;
                    if (! empty(Yii::$app->request->post()['AllUser']['BoardId'])) {
                        $education->board_id = Yii::$app->request->post()['AllUser']['BoardId'];
                    }
                    if (! empty(Yii::$app->request->post()['AllUser']['HighestQualId'])) {
                        $education->highest_qual_id = Yii::$app->request->post()['AllUser']['HighestQualId'];
                    }

                    if (! empty(Yii::$app->request->post()['AllUser']['CourseId'])) {
                        $education->course_id = Yii::$app->request->post()['AllUser']['CourseId'];
                    }

                    $education->DurationTo = Yii::$app->request->post()['AllUser']['DurationTo'];
                    $education->OnDate = date('Y-m-d');
                    // echo "<pre>";print_r($education);die();
                    $education->save(false);
                    // var_dump($education->getErrors());
                    $IsExp = 0;
                    foreach (Yii::$app->request->post()['AllUser']['CompanyName'] as $key => $vv) {
                        if (Yii::$app->request->post()['AllUser']['CompanyName'][$key] != '') {
                            $IsExp = 1;
                            // other position
                            $positionId = 0;
                            if (isset(Yii::$app->request->post()['AllUser']['OtherPosition'][$key]) && Yii::$app->request->post()['AllUser']['OtherPosition'][$key] != '') {
                                $nposition = new Position();
                                $nposition->Position = Yii::$app->request->post()['AllUser']['OtherPosition'][$key];
                                $nposition->OnDate = date('Y-m-d');
                                $nposition->save(false);
                                $positionId = $nposition->PositionId;
                            } else {
                                $positionId = Yii::$app->request->post()['AllUser']['PositionId'][$key];
                            }

                            $experience = new Experience();
                            $experience->UserId = $alluser->UserId;
                            $experience->CompanyName = Yii::$app->request->post()['AllUser']['CompanyName'][$key];
                            $experience->PositionId = $positionId;
                            $experience->PositionName = Yii::$app->request->post()['Experience']['PositionName'][$key];
                            $experience->IndustryId = Yii::$app->request->post()['AllUser']['CIndustryId'][$key];
                            $experience->YearFrom = Yii::$app->request->post()['AllUser']['YearFrom'][$key];
                            $experience->YearTo = Yii::$app->request->post()['AllUser']['YearTo'][$key];
                            if (! Yii::$app->request->post()['AllUser']['YearTo'][$key]) {
                                $experience->YearTo = 'Till Now';
                            }
                            $experience->Experience = Yii::$app->request->post()['AllUser']['Experience'][$key];
                            $experience->Salary = Yii::$app->request->post()['AllUser']['Salary'][$key];
                            if (! empty(Yii::$app->request->post()['AllUser']['SkillsId'])) {
                                $experience->skills = implode(',', Yii::$app->request->post()['AllUser']['SkillsId']);
                            }
                            $experience->OnDate = date('Y-m-d');
                            $experience->save(false);
                        }
                    }

                    if ($IsExp == 1) {

                        $uuser = AllUser::find()->where([
                            'UserId' => $alluser->UserId
                        ])->one();

                        $uuser->IsExp = $IsExp;

                        $uuser->save(false);
                    }

                    $name = $alluser->Name;

                    $to = $alluser->Email;

                    $from = Yii::$app->params['adminEmail'];

                    $subject = "Registration Success";

                    $html = "<html>

               <head>

               <title>Registration Success</title>

               </head>

               <body>

               <table style='width:600px;height:auto;margin:auto;font-family:arial;color:#fff;background:#fff;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='Mycareer Bugs' style='margin-top:10px;width:300px;'></td>

                                   </tr>

                                   <tr>

                                       <td style='height:10px'></td>

                                   </tr>

                           <tr>

                                    <td style='font-size:18px'><h2 style='width:560px;font-weight:normal;background:#f16b22;padding:40px 20px;margin:auto; border-radius:10px'>
                                    Welcome to <a href='https://mycareerbugs.com' target='_blank' style='color: #6d136a; text-align: center; text-decoration: underline;'> My Career Bugs </a>

               <br> 

               <span style='font-size:16px;line-height:1.5'>

                  <h3 style='margin: 18px 0 10px 0;font-size: 35px;'> Dear  $name, </h3>

                    Congratulations! You have been registered successfully on My Career Bugs!!    <br/> No 1 Job Portal <br/>

                    Contact Number:  +91 8240369924   

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                    $mail = new ContactForm();

                    $mail->sendEmail($to, $from, $html, $subject);

                    $session = Yii::$app->session;

                    $session->open();

                    Yii::$app->session['Employeeid'] = $alluser->UserId;

                    Yii::$app->session['EmployeeName'] = $alluser->Name;

                    Yii::$app->session['EmployeeEmail'] = $alluser->Email;

                    $pimage = $url . $alluser->photo->Doc;

                    $img = explode(".", $pimage);

                    $ext = $img[1];

                    $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));

                    $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;

                    if (file_exists($thumb)) {

                        $document = Documents::find()->where([
                            'DocId' => $photo_id
                        ])->one();

                        $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;

                        $document->save(false);
                    }

                    if ($alluser->PhotoId != 0) {

                        $url = '/backend/web/';

                        $pimage = $url . $alluser->photo->Doc;
                    } else {

                        $pimage = '/images/user.png';
                    }

                    Yii::$app->session['EmployeeDP'] = $pimage;

                    if (isset(Yii::$app->session['Universityid'])) {

                        Yii::$app->session['CampusStudent'] = Yii::$app->session['Universityid'];
                    }

                    unset(Yii::$app->session['University']);
                    unset(Yii::$app->session['State']);
                    unset(Yii::$app->session['Universityid']);
                    unset(Yii::$app->session['Course']);
                    unset(Yii::$app->session['Specialization']);

                    // Yii::$app->session->setFlash('success', "Account Created Successfully");

                    return $this->redirect([
                        'thankyou'
                    ]);
                }
            } else {

                $cities = Cities::find()->all();

                $naukari_specialization = NaukariSpecialization::find()->all();

                $language_opts = LanguagesOpts::find()->all();

                return $this->render('jobseekersregister_backup', [
                    'language_opts' => $language_opts,
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'skill' => $allskill,
                    'position' => $allposition,
                    'course' => $allcourse,
                    'industry' => $allindustry,
                    'docmodel' => $docmodel1,
                    'cities' => $cities
                ]);
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionJobseekersregister()
    {
        $this->layout = 'layout';
        $model = new AllUser();
        $experience = new Experience();
        $education = new Education();
        $post = \Yii::$app->request->post();
		
        if ($post) {
			Yii::$app->cache->flush();
			//echo "<pre>"; print_r($post);die();
			//$experience->load($post);
			//echo '<pre>';
			//print_r($post['Experience']);die();
			
            if ($model->load($post)) {
                $model->EntryType = !empty($post['Experience'][0]['CompanyName']) ? 'Experience' : 'Fresher';
                $model->UserTypeId = 2;
                $model->Password = md5($model->Password);
                //$model->language = is_array($model->language) ? implode(', ', $model->language) : $model->language;
                
                $model->language = is_array($post['AllUser']['language']) ? implode(', ', $post['AllUser']['language']) : $post['AllUser']['language'];
                
                $model->Ondate = date('Y-m-d');
                $model->VerifyKey = 'CB' . time();
                $model->VerifyStatus = 1;
                $model->PreferredLocation = is_array($model->PreferredLocation) ? implode(', ', $model->PreferredLocation) : $model->PreferredLocation;
                $model->Country = 'India';
                $model->State = States::findOne($model->State)->StateName;
                $model->City = City::findOne($model->City)->CityName;
                $model->PhotoId = Documents::handleImageUpload($model, 'photoFile', 'PhotoId');
                $model->CVId = Documents::handleImageUpload($model, 'cvFile', 'cv');
                $model->IsExp = 0;
				$model->CollarType = $post['AllUser']['CollarType'];

                if ($model->save()) {
                    if ($education->load($post)) {

                        if (in_array($post->Education->highest_qual_id, [
                            4,
                            5,
							6
                        ])) {
                            $education->CourseId = 0;
							//$education->board_id = $post['Education']['board_id'] ? $post['Education']['board_id'] : 0;
                        } else {
                            $education->CourseId = $post['Education']['CourseId'] ? $post['Education']['CourseId'] : 0;;
                        }
						$education->board_id = $post['Education']['board_id'] ? $post['Education']['board_id'] : 0;
                        $education->HighestQualification = $post['Education']['highest_qual_id'] ? Education::getQual($post['Education']['highest_qual_id']) : 'none';
                        $education->highest_qual_id = $post['Education']['highest_qual_id'] ? $post['Education']['highest_qual_id'] : 0;
                        $education->specialization_id = $education->specialization_id ? $education->specialization_id : 0;
                        $education->course_id = $education->CourseId ? $education->CourseId : 0;
                        $education->UserId = $model->UserId;
                        $education->DurationTo = $education->PassingYear;
                        $education->DurationFrom = '';
                        $education->OnDate = date('Y-m-d');
                        // $education->CourseId = Education::getCourseId( $education->CourseId );
                        foreach ($education->RoleId as $key => $role) {
                            if (isset($education->SkillId) && is_array($education->SkillId)) {
                                foreach ($education->SkillId[$role] as $key => $skill) {
                                    $educationSkill = new EmployeeSkill();
                                    $educationSkill->UserId = $model->UserId;
                                    $educationSkill->SkillRoleId = $role;
                                    $educationSkill->SkillId = $skill;
                                    $educationSkill->IsDelete = 0;
                                    $educationSkill->TypeId = EmployeeSkill::TYPE_EDUCATION;
                                    $educationSkill->OnDate = date('Y-m-d h:i:s');
                                    if (! $educationSkill->save()) {
                                        echo "<pre>EducationSkill : ";
                                        print_r($educationSkill->getErrors());
                                        die();
                                    }
                                }
                            }
                        }
                        if (! $education->save(false)) {
                            echo "<pre>Education : ";
                            print_r($education->getErrors());
                            die();
                        }
                    }
					/*Save White Collar Job Category, Skills, Role*/
					if(!empty($post['AllUser']['Category'])){
						foreach($post['AllUser']['Category'] as $data){
							if(!empty($data['WhiteCategory'])){
								$whiteCategory = WhiteCategory::find()->where([
													'id' => $data['WhiteCategory']
												])->one();
								if(!empty($whiteCategory)){
									$userJobCate = new UserJobCategory();
									$userJobCate->user_id = $model->UserId;
									$userJobCate->category_id = $data['WhiteCategory'];
									$userJobCate->category = $whiteCategory->name;
									$userJobCate->on_date = date('Y-m-d H:i:s');
									if(!$userJobCate->save()){
										echo "<pre>Job Category : ";
                                        print_r($userJobCate->getErrors());
                                        die();
									}
									/*Save White Role*/
									if(!empty($data['WhiteRole'])){
										foreach($data['WhiteRole'] as $whiteRoleId){
											if(!empty($whiteRoleId)){
												$whiteRoleData = WhiteRole::find()->where([
													'id' => $whiteRoleId
												])->one();
												if(!empty($whiteRoleData)){
													$userWhiteRole = new UserWhiteRole();
													$userWhiteRole->user_id = $model->UserId;
													$userWhiteRole->category_id = $data['WhiteCategory'];
													$userWhiteRole->role_id = $whiteRoleId;
													$userWhiteRole->role = $whiteRoleData->name;
													if(!$userWhiteRole->save()){
														echo "<pre>Role : ";
														print_r($userWhiteRole->getErrors());
														die();
													}
												}	
											}
										}
									}
									
								}
							}
						}
					}
					
					/*Save White Collar Skills*/
					if(!empty($post['AllUser']['WhiteSkills'])){
						foreach($post['AllUser']['WhiteSkills'] as $skillId){
							if(!empty($skillId)){
								$whiteSkillRec = WhiteSkill::find()->where([
												'id' => $skillId
											])->one();
								if(!empty($whiteSkillRec)){
									$userWSkill = new UserWhiteSkill();
									$userWSkill->user_id = $model->UserId;
									$userWSkill->skill_id = $skillId;
									$userWSkill->skill = $whiteSkillRec->name;
									$userWSkill->on_date = date('Y-m-d');
									if(!$userWSkill->save()){
										echo "<pre>Skills : ";
										print_r($userWSkill->getErrors());
										die();
									}	
								}
							}
						}
					}
					
					
                    if (!empty($post['Experience'])) {
						
						/*Update New Code*/
						
						foreach($post['Experience'] as $experienceData){
							if(!empty($experienceData['YearFrom'])){
								$experienceModel = new Experience();
								$experienceModel->UserId = $model->UserId;
								$experienceModel->CompanyName = $experienceData['CompanyName'];
								$experienceModel->PositionId = (isset($experienceData['PositionId']) && $experienceData['PositionId']) ? $experienceData['PositionId'] : 108;
								$experienceModel->IndustryId = (isset($experienceData['IndustryId']) && $experienceData['IndustryId']) ? $experienceData['IndustryId'] : 0;
								$experienceModel->PositionName = $experienceData['PositionName'];
								$experienceModel->YearFrom = $experienceData['YearFrom'];
								$experienceModel->YearTo = (isset($experienceData['YearTo']) && $experienceData['YearTo'] != '') ? $experienceData['YearTo'] : 'Till Now';
								if(isset($experienceData['CurrentWorking']) && !empty($experienceData['CurrentWorking'])){
									$experienceModel->YearTo = 'Till Now';
									
								}
								$experienceModel->Experience = $experienceData['Experience'];
								$experienceModel->Salary = $experienceData['Salary'];
								$experienceModel->skills = 1; // implode(',', $experience->skills[$key]);
								$experienceModel->CategoryId = $experienceData['Category'];
								$experienceModel->OnDate = date('Y-m-d');
								$experienceModel->CurrentWorking = (isset($experienceData['CurrentWorking'])?$experienceData['CurrentWorking']:0);
								if ($experienceModel->save()) {
									if($post['AllUser']['CollarType'] == 'blue'){
										if ($experienceData['PositionId']) {
											if(isset($experienceData['skills'][$experienceData['PositionId']]) && !empty($experienceData['skills'][$experienceData['PositionId']])){
												
												$skillData = $experienceData['skills'][$experienceData['PositionId']];
												foreach($skillData as $skill){
													$employeeSkill = new EmployeeSkill();
													$employeeSkill->UserId = $model->UserId;
													$employeeSkill->SkillRoleId = $experienceData['PositionId'];
													$employeeSkill->SkillId = $skill;
													$employeeSkill->OnDate = date('Y-m-d');
													$employeeSkill->TypeId = EmployeeSkill::TYPE_EXPERIENCE;
													$employeeSkill->ExperienceId = $experienceModel->ExperienceId;
													if (! $employeeSkill->save()) {
														echo "<pre>Employee Skill : ";
														print_r($employeeSkill->getErrors());
														die();
													}
												}
											}
											
										}	
									}elseif($post['AllUser']['CollarType'] == 'white'){
										if(!empty($experienceData['Category'])){
											/*Save Roles*/
											if(!empty($experienceData['WhiteRole'])){
												foreach($experienceData['WhiteRole'] as $wRoleId){
													$whiteRole = WhiteRole::find()->where([
															'id' => $wRoleId
														])->one();
													if(!empty($whiteRole)){
														$expWhiteRole = new ExperienceWhiteRole();
														$expWhiteRole->user_id = $model->UserId;
														$expWhiteRole->experience_id = $experienceModel->ExperienceId;
														$expWhiteRole->category_id = $experienceData['Category'];
														$expWhiteRole->role_id = $wRoleId;
														$expWhiteRole->role = $whiteRole->name;
														$expWhiteRole->save();
													}
												}
											}
										}
										
										/*Save Skills*/
										if(!empty($experienceData['WhiteSkills'])){
											foreach($experienceData['WhiteSkills'] as $wSkillId){
												$whiteSkill = WhiteSkill::find()->where([
														'id' => $wSkillId
													])->one();
												if(!empty($whiteSkill)){
													$expWhiteSkill = new ExperienceWhiteSkill();
													$expWhiteSkill->user_id = $model->UserId;
													$expWhiteSkill->experience_id = $experienceModel->ExperienceId;
													$expWhiteSkill->skill_id = $wSkillId;
													$expWhiteSkill->skill = $whiteSkill->name;
													$expWhiteSkill->save();
												}
											}
										}
									}
									$model->IsExp = 1;
									$model->updateAttributes([
										'IsExp'
									]);
								}else{
									echo "<pre>Employee Experience : ";
									print_r($experienceModel->getErrors());
									die();
								}
								
							}
							
						}
						
						/*if(!empty($experience->YearFrom)){
							foreach ($experience->YearFrom as $key => $yearFrom) {
                            $experienceModel = new Experience();
                            $experienceModel->UserId = $model->UserId;
                            $experienceModel->CompanyName = $experience->CompanyName[$key];
                            $experienceModel->PositionId = (isset($experience->PositionId[$key]) && $experience->PositionId[$key]) ? $experience->PositionId[$key] : 0;
                            $experienceModel->IndustryId = (isset($experience->IndustryId[$key]) && $experience->IndustryId[$key]) ? $experience->IndustryId[$key] : 0;
                            $experienceModel->PositionName = $experience->PositionName[$key];
                            $experienceModel->YearFrom = $experience->YearFrom[$key];
                            $experienceModel->YearTo = (isset($experience->YearTo[$key]) && $experience->YearTo[$key] != '') ? $experience->YearTo[$key] : 'Till Now';
                            $experienceModel->Experience = $experience->Experience[$key];
                            $experienceModel->Salary = $experience->Salary[$key];
                            $experienceModel->skills = 1; // implode(',', $experience->skills[$key]);
                            $experienceModel->OnDate = date('Y-m-d');

                            if ($experienceModel->PositionId) {
                                if ($experienceModel->save()) {

                                    foreach ($experience->PositionId as $key => $position) {
                                        if (isset($experience->skills) && is_array($experience->skills)) {
                                            foreach ($experience->skills[$position] as $key => $empSkill) {
                                                $employeeSkill = new EmployeeSkill();
                                                $employeeSkill->UserId = $model->UserId;
                                                $employeeSkill->SkillRoleId = $position;
                                                $employeeSkill->SkillId = $empSkill;
                                                $employeeSkill->OnDate = date('Y-m-d');
                                                $employeeSkill->TypeId = EmployeeSkill::TYPE_EXPERIENCE;
                                               $employeeSkill->ExperienceId = $experienceModel->ExperienceId;
                                                if (! $employeeSkill->save()) {
                                                    echo "<pre>Employee Skill : ";
                                                    print_r($employeeSkill->getErrors());
                                                    die();
                                                }
                                            }
                                        }
                                    }

                                    $model->IsExp = 1;
                                    $model->updateAttributes([
                                        'IsExp'
                                    ]);
                                } else {
                                    echo "<pre>Experience : ";
                                    print_r($experienceModel->getErrors());
                                    die();
                                }
                            }
                        }
						} */
                        
                    }
                    /**
                     * TODO Mail Functionality
                     */

                    Yii::$app->session['Employeeid'] = $model->UserId;

                    Yii::$app->session['EmployeeName'] = $model->Name;

                    Yii::$app->session['EmployeeEmail'] = $model->Email;
					
					if ($model->PhotoId != 0) {

                           $url = '/backend/web/';

                           $pimage = $url . $model->photo->Doc;
                     } else {

						$pimage = '/images/user.png';
					}
						//echo $pimage;die;
						Yii::$app->session['EmployeeDP'] = $pimage;

                   return $this->redirect([
                        'thankyou'
                    ]);
                } else {
                    echo "<pre>User : ";
                    print_r($model->getErrors());
                    die();
                }
            }
        }
        $naukari_specialization = NaukariSpecialization::find()->all();
        $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();
        $whiteCategories =  ArrayHelper::map(WhiteCategory::find()->where([
                'status' => 1
            ])
                ->orderBy([
                'name' => 'SORT_ASC'
            ])
                ->all(), 'id', 'name');
		
		return $this->render('jobseekersregister', compact('model', 'experience', 'education', 'whiteCategories'));
    }

    public function actionGetcity($stateId)
    {
        $data = [];
        $data['status'] = 'NOK';
        $models = Cities::find()->where([
            'StateId' => $stateId
        ])->all();
        if (! empty($models)) {
            $data['status'] = 'OK';
            $result = '';
            $result .= '<option value="">' . 'Select City' . '</option>';
            foreach ($models as $model) {
                $result .= '<option value="' . $model->CityId . '">' . $model->CityName . '</option>';
            }
        } else {
            $result .= '<option value="">No Result Found</option>';
        }
        $data['result'] = $result;
        return $this->asJson($data);
    }

    public function actionLogin()

    {
        if (! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Employeeid']) && ! isset(Yii::$app->session['Campusid']) && ! isset(Yii::$app->session['Teamid'])) {
            $allhotcategory = JobCategory::getHotJobCategory();
            Yii::$app->view->params['hotcategory'] = $allhotcategory;
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $model = new AllUser();

            if ($model->load(Yii::$app->request->post())) {

                $password = md5($model->Password);

                $rest = $model->find()
                    ->where([
                    'Email' => $model->Email,
                    'IsDelete' => 0
                ])
                    ->andWhere([
                    '!=',
                    'UserTypeId',
                    4
                ])
                    ->one();

                if ($rest) {

                    if ($password == $rest->Password) {

                        if ($rest->VerifyStatus == 0) {

                            Yii::$app->session->setFlash('error', 'Please Check your mail for Email Verification.');

                            return $this->render('login', [
                                'model' => $model
                            ]);
                        } else {

                            $this->layout = 'layout';

                            $rest = $model->find()
                                ->where([
                                'Email' => $model->Email,
                                'Password' => $password,
                                'IsDelete' => 0
                            ])
                                ->andWhere([
                                '!=',
                                'UserTypeId',
                                4
                            ])
                                ->one();

                            if ($rest->UserTypeId == 2) {

                                $session = Yii::$app->session;

                                $session->open();

                                Yii::$app->session['Employeeid'] = $rest->UserId;

                                Yii::$app->session['EmployeeName'] = $rest->Name;

                                Yii::$app->session['EmployeeEmail'] = $model->Email;

                                if ($rest->CampusId != 0) {

                                    Yii::$app->session['CampusStudent'] = $rest->CampusId;
                                }

                                if ($rest->PhotoId != 0) {

                                    $url = '/backend/web/';

                                    $pimage = $url . $rest->photo->Doc;
                                } else {

                                    $pimage = '/images/user.png';
                                }
								//echo $pimage;die;
                                Yii::$app->session['EmployeeDP'] = $pimage;

                                $appliedjob = new AppliedJob();

                                $noofjobapplied = $appliedjob->find()
                                    ->where([
                                    'Status' => 'Applied',
                                    'UserId' => Yii::$app->session['Employeeid'],
                                    'IsDelete' => 0
                                ])
                                    ->count();

                                Yii::$app->session['NoofjobApplied'] = $noofjobapplied;

                                if (isset(Yii::$app->request->get()['Job'])) {

                                    return $this->redirect([
                                        'job/'.Yii::$app->request->get()['Job']
                                    ]);
                                }elseif (isset(Yii::$app->request->get()['wall'])) {

                                    return $this->redirect([
                                        'wall/post-view/'.Yii::$app->request->get()['wall']
                                    ]);
                                } else {

                                     return $this->redirect([
										'wall/mcbwallpost'
									]);
                                }
                            } elseif ($rest->UserTypeId == 3) {

                                $session = Yii::$app->session;

                                $session->open();

                                Yii::$app->session['Employerid'] = $rest->UserId;

                                Yii::$app->session['EmployerName'] = $rest->Name;

                                Yii::$app->session['EmployerEmail'] = $model->Email;

                                $employerone = $model->find()
                                    ->where([
                                    'UserId' => $rest->UserId
                                ])
                                    ->one();

                                if ($employerone->LogoId != 0) {

                                    $url = '/backend/web/';

                                    $pimage = $url . $employerone->logo->Doc;
                                } else {

                                    $pimage = '/images/user.png';
                                }

                                Yii::$app->session['EmployerDP'] = $pimage;
								if (isset(Yii::$app->request->get()['wall'])) {

                                    return $this->redirect([
                                        'wall/post-view/'.Yii::$app->request->get()['wall']
                                    ]);
                                } else {
									return $this->redirect([
										'yourpost'
									]);
								}
                            } elseif ($rest->UserTypeId == 5) {

                                $session = Yii::$app->session;

                                $session->open();

                                Yii::$app->session['Teamid'] = $rest->UserId;

                                Yii::$app->session['TeamName'] = $rest->Name;

                                Yii::$app->session['TeamEmail'] = $rest->Email;

                                if ($rest->PhotoId != 0) {

                                    $url = '/backend/web/';

                                    $pimage = $url . $rest->photo->Doc;
                                } else {

                                    $pimage = '/images/user.png';
                                }

                                $appliedjob = new AppliedJob();

                                $noofjobapplied = $appliedjob->find()
                                    ->where([
                                    'Status' => 'Applied',
                                    'UserId' => Yii::$app->session['Teamid'],
                                    'AppliedJob.IsDelete' => 0
                                ])
                                    ->count();

                                Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;

                                Yii::$app->session['TeamDP'] = $pimage;

                                // return $this->redirect(['team/teamprofile']);
								if (isset(Yii::$app->request->get()['wall'])) {

                                    return $this->redirect([
                                        'wall/post-view/'.Yii::$app->request->get()['wall']
                                    ]);
                                } else {
									 return $this->redirect([
										'wall/mcbwallpost'
									]);
									
								}
                            }
                        }
                    } else {

                        Yii::$app->session->setFlash('error', "Wrong Password");

                        return $this->render('login', [
                            'model' => $model
                        ]);
                    }
                } else {

                    Yii::$app->session->setFlash('error', "Wrong Email");

                    return $this->render('login', [
                        'model' => $model
                    ]);
                }
            } else {

                return $this->render('login', [
                    'model' => $model
                ]);
            }
        } else {

            return $this->redirect([
                    'wall/mcbwallpost'
                ]);
        }
    }

    public function actionReqcad($id)

    {
        echo $id;
        exit();
    }

    public function actionEmployeelogout()

    {
        $session = Yii::$app->session;

        $session->open();

        unset(Yii::$app->session['Employeeid']);

        unset(Yii::$app->session['EmployeeName']);

        unset(Yii::$app->session['EmployeeEmail']);

        unset(Yii::$app->session['EmployeeDP']);

        unset(Yii::$app->session['SocialEmail']);

        unset(Yii::$app->session['SocialName']);

        return $this->redirect([
            'index'
        ]);
    }

    public function actionEmployerforgetpassword()

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $model = new AllUser();

        if ($model->load(Yii::$app->request->post())) {

            $email = Yii::$app->request->post('AllUser')['Email'];

            $chk = $model->find()
                ->where([
                'Email' => $email,
                'UserTypeId' => 3
            ])
                ->one();

            if ($chk) {

                $ucode = 'CB' . time();

                $chk->VerifyKey = $ucode;

                $chk->ForgotStatus = 0;

                $chk->save(false);

                // var_dump($chk->getErrors());

                $to = $email;

                $from = 'mycareerbugs@mycareerbugs.com';

                $subject = "Reset Password";

                $html = "<html>

               <head>

               <title>Create a new password</title>

               </head>

               <body>

               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#fff;text-align:center'>

                <tbody><tr>

                <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:250px;'></td>

                                   </tr> 

                           <tr>

                                       <td style='font-size:18px'>
                                        <div style='width: 600px; background: #f16b22; margin: 1em auto 0em; border-radius: 5px; padding: 2em 1em 3em 1em;'>
                                     
                                       <h2 style='text-align: center;font-size: 1.3em; color: #fff; margin-bottom: 10px; font-weight: 700;'>Welcome to <a href='https://mycareerbugs.com/' target='_blank' style='text-decoration:none;color: #6d136a;'> My Career Bugs </a>

               <br><br>
  <h2 style='text-align: center;font-size: 36px;color: #fff;margin: 10px 0;font-weight: 700;'>Forgot Password</h2>
  
               <span style='font-size:16px;line-height:1.5'>

               Please click in the link to create your new password

               <br/>

               <a href='https://mycareerbugs.com/site/resetpassword?ucode=$ucode' style='display:block;color:#fff; text-decoration:none; background: #580e56; border-bottom: 3px solid #6d136a; width:250px; margin:20px auto 0; padding:10px 0'>

               <span style='display: block; height:50 px;width:100px;color: #ffffff;text-decoration: none;font-size: 14px;text-align: center;font-family: 'Open Sans',Gill Sans,Arial,Helvetica,sans-serif;font-weight: bold;line-height: 45px;'>Reset My Password Here</span>

               </a>

              

               </span>

               </h2>
</div>
               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                Yii::$app->session->setFlash('success', 'An email has been sent your mail-id. Please click on the link provided in the mail to reset your password.

In case you do not receive your password reset email shortly, please check your spam folder also..');
            } else {

                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('employerforgetpassword', [
            'model' => $model
        ]);
    }

    public function actionResetpassword($ucode)

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $model = new AllUser();

        $chk = $model->find()
            ->where([
            'VerifyKey' => $ucode,
            'UserTypeId' => 3
        ])
            ->one();

        if ($model->load(Yii::$app->request->post())) {

            $chk->Password = md5($model->Password);

            $chk->save(false);

            Yii::$app->session->setFlash('success', 'You have successfully created a new password.');

            return $this->redirect([
                'employerslogin'
            ]);
        } else {

            if ($chk) {

                if ($chk->ForgotStatus == 0) {

                    $chk->ForgotStatus = 1;

                    $chk->save(false);

                    return $this->render('resetpassword', [
                        'ucode' => $ucode,
                        'model' => $model
                    ]);
                } else {

                    Yii::$app->session->setFlash('error', 'Your reset password link expired');

                    return $this->redirect([
                        'employerforgetpassword'
                    ]);
                }
            } else {

                Yii::$app->session->setFlash('success', 'Please give your registered EmailId for create a new password.');

                return $this->redirect([
                    'employerforgetpassword'
                ]);
            }
        }
    }

    public function actionEmployerchangepassword()

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $model = new AllUser();

        if ($model->load(Yii::$app->request->post())) {

            $cpass = md5(Yii::$app->request->post('AllUser')['CPassword']);

            $npass = Yii::$app->request->post('AllUser')['NPassword'];

            $UserId = Yii::$app->request->post('AllUser')['UserId'];

            $chk = $model->find()
                ->where([
                'UserId' => $UserId
            ])
                ->one();

            if ($chk->Password == $cpass) {

                $chk->Password = md5($npass);

                $chk->save(false);

                Yii::$app->session->setFlash('success', 'Password Has been Changed.');

                return $this->redirect([
                    'companyprofile'
                ]);
            } else {

                Yii::$app->session->setFlash('error', 'Incorrect Current Password.');

                return $this->render('employerchangepassword', [
                    'model' => $model
                ]);
            }
        } else {

            return $this->render('employerchangepassword', [
                'model' => $model
            ]);
        }
    }

    public function actionForgetpassword()

    {
        // contactus block
        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $model = new AllUser();

        if ($model->load(Yii::$app->request->post())) {

            $email = Yii::$app->request->post('AllUser')['Email'];

            $chk = $model->find()
                ->where([
                'Email' => $email,
                'UserTypeId' => 2
            ])
                ->one();

            if ($chk) {

                $ucode = 'CB' . time();

                $chk->VerifyKey = $ucode;

                $chk->ForgotStatus = 0;

                $chk->save(false);

                // var_dump($chk->getErrors());

                $to = $email;

                $from = 'mycareerbugs@mycareerbugs.com';

                $subject = "Reset Password";

                $html = "<html>

               <head>

               <title>Create a new password</title>

               </head>

               <body>

               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#fff;background:#fff;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:250px;'></td>

                                   </tr>
 

                           <tr>

                                       <td style='font-size:18px'>
                                       
                                        <div style='width: 600px; background: #f16b22; margin: 1em auto 0em; border-radius: 5px; padding: 2em 1em 3em 1em;'>
                                     
                                       <h2 style='text-align: center;font-size: 1.3em; color: #fff; margin-bottom: 10px; font-weight: 700;'>
                                    Welcome to <a href='https://mycareerbugs.com' target='_blank' style='text-decoration:none;color: #6d136a;'> My Career Bugs </a>

              <h2 style='text-align: center;font-size: 36px;color: #fff;margin: 10px 0;font-weight: 700;'>Forgot Password</h2>

               <span style='font-size:16px;line-height:1.5'>

               Please click in the link to create your new password

               <br/>

               <a href='https://mycareerbugs.com/site/resetemppassword?ucode=$ucode'  style='text-decoration:none;display:block;color:#fff;background: #580e56; border-bottom: 3px solid #6d136a; width:250px; margin:20px auto 0; padding:10px 0'>

               <span style='display: block; height:50 px;width:100px;color: #ffffff;text-decoration: none;font-size: 14px;text-align: center;font-family: 'Open Sans',Gill Sans,Arial,Helvetica,sans-serif;font-weight: bold;line-height: 45px;'>Reset Your Password</span>

               </a>

              

               </span>

               </h2>
</div>
               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                Yii::$app->session->setFlash('success', 'An email has been sent to your mail-id. Please click on the link provided in the mail to reset your password.

In case you do not receive your password reset email shortly, please check your spam folder also..');
            } else {
                $_POST = null;
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('forgetpassword', [
            'model' => $model
        ]);
    }

    public function actionResetemppassword($ucode)

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $model = new AllUser();

        $chk = $model->find()
            ->where([
            'VerifyKey' => $ucode,
            'UserTypeId' => 2
        ])
            ->one();

        if ($model->load(Yii::$app->request->post())) {

            $chk->Password = md5($model->Password);

            $chk->save(false);

            Yii::$app->session->setFlash('success', 'You have successfully created a new password.');

            return $this->redirect([
                'login'
            ]);
        } else {

            if ($chk) {

                if ($chk->ForgotStatus == 0) {

                    $chk->ForgotStatus = 1;

                    $chk->save(false);

                    return $this->render('resetemppassword', [
                        'ucode' => $ucode,
                        'model' => $model
                    ]);
                } else {

                    Yii::$app->session->setFlash('error', 'Your reset password link expired');

                    return $this->redirect([
                        'forgetpassword'
                    ]);
                }
            } else {

                Yii::$app->session->setFlash('error', 'Please give your registered EmailId for create a new password.');

                return $this->redirect([
                    'forgetpassword'
                ]);
            }
        }
    }

    public function actionEmployeechangepassword()

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $model = new AllUser();

        if ($model->load(Yii::$app->request->post())) {

            $cpass = md5(Yii::$app->request->post('AllUser')['CPassword']);

            $npass = Yii::$app->request->post('AllUser')['NPassword'];

            $UserId = Yii::$app->request->post('AllUser')['UserId'];

            $chk = $model->find()
                ->where([
                'UserId' => $UserId
            ])
                ->one();

            if ($chk->Password == $cpass) {

                $chk->Password = md5($npass);

                $chk->save(false);

                Yii::$app->session->setFlash('success', 'Password Has been Changed.');

                return $this->redirect([
                    'profilepage'
                ]);
            } else {

                Yii::$app->session->setFlash('error', 'Incorrect Current Password.');

                return $this->render('employeechangepassword', [
                    'model' => $model
                ]);
            }
        } else {

            return $this->render('employeechangepassword', [
                'model' => $model
            ]);
        }
    }

    public function actionRegister()

    {
        if (! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Employeeid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            return $this->render('register');
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionSearchcandidate()

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        return $this->render('searchcandidate');
    }

    public function actionProfilepage()

    {
        if (isset(Yii::$app->session['Employeeid'])) {

            $alluser = new AllUser();

            $profile = $alluser->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])
                ->one();
            $role = 0;
            if ($profile->empRelatedSkills) {
                foreach ($profile->empRelatedSkills as $ask => $asv) {
                    if ($asv->skill->position->Position) {
                        $role = $asv->skill->position->PositionId;
                    }
                    if (! empty($role)) {
                        break;
                    }
                }
            }
            $recentjob = PostJob::getRecentsJobs($role);

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $photo_id = $profile->PhotoId;

            if ($photo_id != 0) {

                $pimage = Yii::$app->params['uploadPath'] . $profile->photo->Doc;

                $img = explode(".", $pimage);

                $ext = $img[1];

                $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));

                $thumb = $furl . 'thumb_' . $photo_id . '.' . $ext;

                if (file_exists($thumb)) {

                    $document = Documents::find()->where([
                        'DocId' => $photo_id
                    ])->one();

                    $document->Doc = 'imageupload/thumb_' . $photo_id . '.' . $ext;

                    $document->save(false);
                }

                $profile = $alluser->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid']
                ])
                    ->one();

                $url = '/backend/web/';

                $pimage = $url . $profile->photo->Doc;
            } else {
                $pimage = '/images/user.png';
            }

            Yii::$app->session['EmployeeDP'] = $pimage;

            if ($profile->load(Yii::$app->request->post())) {

                $docmodel = new Documents();

                $image = UploadedFile::getInstance($profile, 'CVId');

                if ($image) {

                    $image_id = $docmodel->imageUpload($image, 'cv');
                } else {

                    $image_id = 0;
                }

                $profile->CVId = $image_id;

                if ($profile->save(false)) {

                    Yii::$app->session->setFlash('success', "Successfully CV uploaded");

                    return $this->redirect([
                        'profilepage'
                    ]);
                } else {

                    Yii::$app->session->setFlash('error', "There is some error please try again");

                    return $this->redirect([
                        'profilepage'
                    ]);
                }
            } else {

                return $this->render('profilepage', [
                    'profile' => $profile,
                    'alljob' => $recentjob
                ]);
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionEditprofile()
    {
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        if (isset(Yii::$app->session['Employeeid'])) {

            $alluser = new AllUser();
            $prefered_loc = array();
            $profile = $alluser->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])
                ->one();
            if (! empty($profile->PreferredLocation)) {
                $prefered_loc = explode(',', $profile->PreferredLocation);
            }

            if ($profile->PhotoId != 0) {

                $url = '/backend/web/';

                $pimage = $url . $profile->photo->Doc;
            } else {

                $pimage = $url . '/images/user.png';
            }

            $skill = new Skill();

            $position = new Position();

            $course = new Course();

            $docmodel = new Documents();

            $docmodel1 = new Documents();

            $NaukariQualData = NaukariQualData::find()->where([
                'parent_id' => 0
            ])->all();
            $allskill = $skill->find()
                ->where("IsDelete=0")
                ->all();
            $naukari_specialization = NaukariSpecialization::find()->all();
            $cities = Cities::find()->all();
            $allposition = $position->find()
                ->where("IsDelete=0")
                ->all();

            $allcourse = $course->find()
                ->where("IsDelete=0")
                ->all();

            $allindustry = ArrayHelper::map(Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all(), 'IndustryId', 'IndustryName');

            $cvid = $profile->CVId;

            $photoid = $profile->PhotoId;

            if ($profile->load(Yii::$app->request->post())) {
				
				/*echo '<pre>';
				print_r(Yii::$app->request->post());die;*/
                if (! empty($_POST['AllUser']['PreferredLocation'])) {
                    $profile->PreferredLocation = implode(',', $_POST['AllUser']['PreferredLocation']);
                }
                if (! empty(Yii::$app->request->post()['AllUser']['Language'])) {

                    $profile->language = implode(', ', Yii::$app->request->post()['AllUser']['Language']);
                }
                $profile->whatsappno = Yii::$app->request->post()['AllUser']['WhatsAppNo'];
                $cv = UploadedFile::getInstance($profile, 'CVId');

                if ($cv) {

                    $cv_id = $docmodel->imageUpload($cv, 'CVId');
                } else {

                    $cv_id = $cvid;
                }

                $photo_id = $photoid;

                if ($docmodel1->load(Yii::$app->request->post())) {
                    $docmodel1->image = \yii\web\UploadedFile::getInstance($docmodel1, 'Doc');

                    // die('123');
                    if ($docmodel1->image) {
						$photo_id = Documents::handleImageUpload($docmodel, 'Doc', 'PhotoId');	
                       /*
					   // $image=\yii\web\UploadedFile::getInstance($docmodel1, 'Doc');
                        // $DocId = $docmodel1->imageUpload($image,'photo');
                        // $ext = end((explode(".", $image->name)));

                        $ppp = Yii::$app->security->generateRandomString();

                        // $path = Yii::$app->params['uploadPath'] . $ppp.".{$ext}";

                        $docmodel1->Doc = 'imageupload/' . $ppp . '.' . $docmodel1->image->extension;

                        $docmodel1->OnDate = date('Y-m-d');

                        $docmodel1->From = 'photo';

                        if ($docmodel1->save(false)) {
                            $docmodel1->image->saveAs(Yii::$app->params['uploadPath'] . $ppp . '.' . $docmodel1->image->extension);

                            $photo_id = $docmodel1->DocId;
                        } else {

                            $photo_id = $photoid;
                        }
                        // die('123'); */
                    }
                }

                if (Yii::$app->request->post()['AllUser']['CompanyName'] != '') {

                    $profile->EntryType = 'Experience';
                } else {

                    $profile->EntryType = 'Fresher';
                }

                $profile->CVId = $cv_id;

                $profile->PhotoId = $photo_id;
				
                $profile->CollarType = Yii::$app->request->post()['AllUser']['CollarType'];
				
                $profile->save(false);

                if ($photo_id != 0) {

                    $profile = $alluser->find()
                        ->where([
                        'UserId' => Yii::$app->session['Employeeid']
                    ])
                        ->one();

                    $url = '/backend/web/';

                    $pimage = $url . $profile->photo->Doc;
                } else {

                    $pimage = $url . '/images/user.png';
                }

                Yii::$app->session['EmployeeDP'] = $pimage;

                $education = Education::find()->where([
                    'UserId' => $profile->UserId
                ])->one();
                
                if ($education) {
					if (! empty(Yii::$app->request->post()['AllUser']['Specialization'])) {
						$education->specialization_id = Yii::$app->request->post()['AllUser']['Specialization'];
					}
					if (! empty(Yii::$app->request->post()['AllUser']['HighestQualId'])) {
						$education->highest_qual_id = Yii::$app->request->post()['AllUser']['HighestQualId'];
					}
					if (! empty(Yii::$app->request->post()['AllUser']['CourseId'])) {
						$education->course_id = Yii::$app->request->post()['AllUser']['CourseId'];
					}
					
					if (! empty(Yii::$app->request->post()['AllUser']['board_id'])) {
						$education->board_id = Yii::$app->request->post()['AllUser']['board_id'];
					}						

                    $education->HighestQualification = $education->highest_qual_id ? Education::getQual($education->highest_qual_id) : 'none';

                    $education->CourseId = 0; // Yii::$app->request->post()['AllUser']['CourseId'];

                    $education->University = Yii::$app->request->post()['AllUser']['University'];
                    if (! empty(Yii::$app->request->post()['AllUser']['SkillId'])) {
                        $education->SkillId = implode(',', Yii::$app->request->post()['AllUser']['SkillId']);
                    }

                    $education->PassingYear = Yii::$app->request->post()['AllUser']['PassingYear'];

                    $education->DurationTo = Yii::$app->request->post()['AllUser']['PassingYear'];
					
                    $education->save(false);
                } else {

                    $education = new Education();

                    $education->UserId = $profile->UserId;
										
					$education->specialization_id = !empty(Yii::$app->request->post()['AllUser']['Specialization'])?Yii::$app->request->post()['AllUser']['Specialization']:0;
					
					$education->highest_qual_id = !empty(Yii::$app->request->post()['AllUser']['HighestQualId'])?Yii::$app->request->post()['AllUser']['HighestQualId']:0;
					
					$education->course_id = !empty(Yii::$app->request->post()['AllUser']['CourseId'])?Yii::$app->request->post()['AllUser']['CourseId']:0;

                    $education->CourseId = !empty(Yii::$app->request->post()['AllUser']['CourseId'])?Yii::$app->request->post()['AllUser']['CourseId']:0;
					
					$education->board_id = !empty(Yii::$app->request->post()['AllUser']['board_id'])?Yii::$app->request->post()['AllUser']['board_id']:0;

                    $education->PassingYear = Yii::$app->request->post()['AllUser']['PassingYear'];

                    $education->DurationTo = Yii::$app->request->post()['AllUser']['PassingYear'];

                    $education->University = !empty(Yii::$app->request->post()['AllUser']['University'])?Yii::$app->request->post()['AllUser']['University']:"";

                    if (! empty(Yii::$app->request->post()['AllUser']['SkillId'])) {
                        $education->SkillId = implode(',', Yii::$app->request->post()['AllUser']['SkillId']);
                    }

                    $education->OnDate = date('Y-m-d');

                    $education->save(false);
                }

                $IsExp = 0;
				
				/*Update Skills*/
				if(!empty(Yii::$app->request->post()['AllUser']['SkillRoleId'])){
					$skillRoleIds = Yii::$app->request->post()['AllUser']['SkillRoleId'];
					$skillIds = Yii::$app->request->post()['AllUser']['SkillId'];
					/*Delete All Experience Skills*/
					EmployeeSkill::deleteAll([
						'UserId' => $profile->UserId,
						'TypeId' => 0
					]);
					foreach($skillRoleIds as $skillRoleId){
						if(!empty($skillIds[$skillRoleId])){
							foreach($skillIds[$skillRoleId] as $eduSkillId){
								$empskill = new EmployeeSkill();
								$empskill->SkillRoleId = $skillRoleId;
								$empskill->SkillId = $eduSkillId;
								$empskill->UserId = $profile->UserId;
								$empskill->ExperienceId = 0;
								$empskill->TypeId = 0;
								$empskill->OnDate = date('Y-m-d');
								$empskill->save(false);
								
							}
							
						}					
					}
				}

				/*Save White Collar Job Category, Skills, Role*/
					if(!empty(Yii::$app->request->post()['AllUser']['Category'])){
						/*Delete All Experience Skills*/
						UserJobCategory::deleteAll([
							'user_id' => $profile->UserId
						]);
						UserWhiteRole::deleteAll([
							'user_id' => $profile->UserId
						]);
						foreach(Yii::$app->request->post()['AllUser']['Category'] as $data){
							if(!empty($data['WhiteCategory'])){
								$whiteCategory = WhiteCategory::find()->where([
													'id' => $data['WhiteCategory']
												])->one();
								if(!empty($whiteCategory)){
									$userJobCate = new UserJobCategory();
									$userJobCate->user_id = $profile->UserId;
									$userJobCate->category_id = $data['WhiteCategory'];
									$userJobCate->category = $whiteCategory->name;
									$userJobCate->on_date = date('Y-m-d H:i:s');
									if(!$userJobCate->save()){
										/*echo "<pre>Job Category : ";
                                        print_r($userJobCate->getErrors());
                                        die();*/
									}
									/*Save White Role*/
									if(!empty($data['WhiteRole'])){
										foreach($data['WhiteRole'] as $whiteRoleId){
											if(!empty($whiteRoleId)){
												$whiteRoleData = WhiteRole::find()->where([
													'id' => $whiteRoleId
												])->one();
												if(!empty($whiteRoleData)){
													$userWhiteRole = new UserWhiteRole();
													$userWhiteRole->user_id = $profile->UserId;
													$userWhiteRole->category_id = $data['WhiteCategory'];
													$userWhiteRole->role_id = $whiteRoleId;
													$userWhiteRole->role = $whiteRoleData->name;
													if(!$userWhiteRole->save()){
														/*echo "<pre>Role : ";
														print_r($userWhiteRole->getErrors());
														die();*/
													}
												}	
											}
										}
									}
									
								}
							}
						}
					}
					
					/*Save White Collar Skills*/
					if(!empty(Yii::$app->request->post()['AllUser']['WhiteSkills'])){
						UserWhiteSkill::deleteAll([
							'user_id' => $profile->UserId
						]);
						foreach(Yii::$app->request->post()['AllUser']['WhiteSkills'] as $skillId){
							if(!empty($skillId)){
								$whiteSkillRec = WhiteSkill::find()->where([
												'id' => $skillId
											])->one();
								if(!empty($whiteSkillRec)){
									$userWSkill = new UserWhiteSkill();
									$userWSkill->user_id = $profile->UserId;
									$userWSkill->skill_id = $skillId;
									$userWSkill->skill = $whiteSkillRec->name;
									$userWSkill->on_date = date('Y-m-d');
									if(!$userWSkill->save()){
										/*echo "<pre>Skills : ";
										print_r($userWSkill->getErrors());
										die();*/
									}	
								}
							}
						}
					}	
				
				
				/*Save & Update Experience Code*/
				if(!empty(Yii::$app->request->post()['AllUser']['Experience'])){
					$experienceData = Yii::$app->request->post()['AllUser']['Experience'];
					/*Delete All Experience Skills*/
					EmployeeSkill::deleteAll([
						'UserId' => $profile->UserId,
						'TypeId' => 1
					]);
					foreach($experienceData as $expData){
						if(!empty($expData['YearFrom']) && !empty($expData['CompanyName'])){
						
						if(!empty($expData['ExperienceId'])){
							$experience = Experience::find()->where([
                                'ExperienceId' => $expData['ExperienceId']
                            ])
                                ->one();															

                            $experience->CompanyName = $expData['CompanyName'];
                            $experience->PositionId = (!empty($expData['PositionId'])?$expData['PositionId']:0);
                            $experience->IndustryId = $expData['CIndustryId'];
                            $experience->Experience = $expData['Experience'];
                            $experience->PositionName = $expData['PositionName'];
                            $experience->YearFrom = $expData['YearFrom'];
                            $experience->YearTo = $expData['YearTo'];
                            $experience->Salary = $expData['Salary'];
                            $experience->CategoryId = $expData['Category'];
							$IsExp = 1;							
							if(isset($expData['CurrentWorking']) && !empty($expData['CurrentWorking'])){
								$experience->YearTo = 'Till Now';
								$experience->CurrentWorking = 1;								
							}else{
								$experience->CurrentWorking = 0;
							}							
                            $experience->save(false);
							
						}else{
							$experience = new Experience();
                            $experience->UserId = $profile->UserId;
                            $experience->CompanyName = $expData['CompanyName'];
                            $experience->PositionId = (!empty($expData['PositionId'])?$expData['PositionId']:0);
                            $experience->IndustryId = $expData['CIndustryId'];
                            $experience->YearFrom = $expData['YearFrom'];
                            $experience->YearTo = $expData['YearTo'];
                            if (! $expData['YearTo'] ) {
                                $experience->YearTo = 'Till Now';
                            }							
							if(isset($expData['CurrentWorking']) && !empty($expData['CurrentWorking'])){
								$experience->YearTo = 'Till Now';
								$experience->CurrentWorking = 1;								
							}else{
								$experience->CurrentWorking = 0;
							}							
                            $experience->Experience = $expData['Experience'];
                            $experience->CategoryId = $expData['Category'];
                            $experience->PositionName = $expData['PositionName'];
                            $experience->Salary = $expData['Salary'];
                            $experience->OnDate = date('Y-m-d');
                            $experience->skills = 1;
							$IsExp = 1;							
                            $experience->save(false);							
						}
						
						 $experienceId =  $experience->ExperienceId;
						 
						 /*Delete White Collar Role and Skills*/
						  ExperienceWhiteRole::deleteAll([
									'user_id' => $profile->UserId,
									'experience_id' => $experienceId
								]);
						  ExperienceWhiteSkill::deleteAll([
									'user_id' => $profile->UserId,
									'experience_id' => $experienceId
								]);		
								
						 if(Yii::$app->request->post()['AllUser']['CollarType'] == 'blue'){						 
							 /*Save Experience Skills*/
							 if(!empty($expData['ExperienceSkillId']) && !empty($expData['PositionId'])){
								 foreach($expData['ExperienceSkillId'] as $skill){
									$empskill = new EmployeeSkill();									
									$empskill->SkillRoleId = $expData['PositionId'];
									$empskill->SkillId = $skill;
									$empskill->UserId = $profile->UserId;
									$empskill->ExperienceId = $experienceId;									
									$empskill->TypeId = 1;
									$empskill->OnDate = date('Y-m-d');
									$empskill->save(false);
								 }								 
							 }						 
						 }elseif(Yii::$app->request->post()['AllUser']['CollarType'] == 'white'){
							 if(!empty($expData['Category'])){								
								/*Save Roles*/
								if(!empty($expData['WhiteRole'])){
									foreach($expData['WhiteRole'] as $wRoleId){
										$whiteRole = WhiteRole::find()->where([
												'id' => $wRoleId
											])->one();
										if(!empty($whiteRole)){
											$expWhiteRole = new ExperienceWhiteRole();
											$expWhiteRole->user_id = $profile->UserId;
											$expWhiteRole->experience_id = $experienceId;
											$expWhiteRole->category_id = $expData['Category'];
											$expWhiteRole->role_id = $wRoleId;
											$expWhiteRole->role = $whiteRole->name;
											$expWhiteRole->save();
										}
									}
								}
							}
							
							/*Save Skills*/
							if(!empty($expData['WhiteSkills'])){
								foreach($expData['WhiteSkills'] as $wSkillId){
									$whiteSkill = WhiteSkill::find()->where([
											'id' => $wSkillId
										])->one();
									if(!empty($whiteSkill)){
										$expWhiteSkill = new ExperienceWhiteSkill();
										$expWhiteSkill->user_id = $profile->UserId;
										$expWhiteSkill->experience_id = $experienceId;
										$expWhiteSkill->skill_id = $wSkillId;
										$expWhiteSkill->skill = $whiteSkill->name;
										$expWhiteSkill->save();
									}
								}
							}
							 
						 }						 
						}						 
					}					
				}
				
				
				
				
				
				//echo '<pre>';
				//print_r(Yii::$app->request->post());die;
                /*foreach (Yii::$app->request->post()['AllUser']['CompanyName'] as $key => $val) {

                    if (Yii::$app->request->post()['AllUser']['CompanyName'][$key] != '') {

                        $IsExp = 1;

                        if (isset(Yii::$app->request->post()['AllUser']['ExperienceId'][$key]) && Yii::$app->request->post()['AllUser']['ExperienceId'][$key] != '') {

                            $experience = Experience::find()->where([
                                'ExperienceId' => Yii::$app->request->post()['AllUser']['ExperienceId'][$key]
                            ])
                                ->one();

                            $experience->CompanyName = Yii::$app->request->post()['AllUser']['CompanyName'][$key];

                            $experience->PositionId = Yii::$app->request->post()['AllUser']['PositionId'][$key];

                            $experience->IndustryId = Yii::$app->request->post()['AllUser']['CIndustryId'][$key];

                            $experience->Experience = Yii::$app->request->post()['AllUser']['Experience'][$key];
                            $experience->PositionName = Yii::$app->request->post()['AllUser']['PositionName'][$key];
                            $experience->YearFrom = Yii::$app->request->post()['AllUser']['YearFrom'][$key];

                            $experience->YearTo = Yii::$app->request->post()['AllUser']['YearTo'][$key];

                            $experience->Salary = Yii::$app->request->post()['AllUser']['Salary'][$key];

                            $experience->save(false);
                        } else {

                            $experience = new Experience();

                            $experience->UserId = $profile->UserId;

                            $experience->CompanyName = Yii::$app->request->post()['AllUser']['CompanyName'][$key];

                            $experience->PositionId = Yii::$app->request->post()['AllUser']['PositionId'][$key];

                            $experience->IndustryId = Yii::$app->request->post()['AllUser']['CIndustryId'][$key];

                            $experience->YearFrom = Yii::$app->request->post()['AllUser']['YearFrom'][$key];

                            $experience->YearTo = Yii::$app->request->post()['AllUser']['YearTo'][$key];

                            if (! Yii::$app->request->post()['AllUser']['YearTo'][$key]) {

                                $experience->YearTo = 'Till Now';
                            }
                            $experience->Experience = Yii::$app->request->post()['AllUser']['Experience'][$key];
                            $experience->PositionName = Yii::$app->request->post()['AllUser']['PositionName'][$key];
                            $experience->Salary = Yii::$app->request->post()['AllUser']['Salary'][$key];
                            $experience->OnDate = date('Y-m-d');
                            $experience->save(false);
                        }
                    }
                }
                EmployeeSkill::deleteAll([
                    'UserId' => $profile->UserId
                ]);
                // new skill
                $rawskill = Yii::$app->request->post()['AllUser']['SkillId'];
			
                $skills_ary = array();
                if (empty($rawskill)) {
                    $rawskill = array();
                } else {
                    $skill_s = new Skill();
                    $skills_res = $skill_s->find()
                        ->where([
                        'SkillId' => $rawskill
                    ])
                        ->all();
                    if (! empty($skills_res)) {
                        foreach ($skills_res as $keys => $values) {
                            $skills_ary[$values->SkillId] = $values->PositionId;
                        }
                    }
                }
                // $rawskill=explode(",",Yii::$app->request->post()['AllUser']['RawSkill']);

                foreach ($rawskill as $rk => $skid) {

                    if (! empty($skid)) {
                        $skillidlist .= $skid;

                        $empskill = new EmployeeSkill();
                        if (isset($skills_ary[$skid])) {
                            $skillid_s = $skills_ary[$skid];
                        } else {
                            $skillid_s = 0;
                        }
                        $empskill->SkillRoleId = $skillid_s;

                        $empskill->SkillId = $skid;

                        $empskill->UserId = $profile->UserId;

                        $empskill->OnDate = date('Y-m-d');

                        $empskill->save(false);

                        // var_dump($empskill->getErrors());
                    }
                }
				*/

                if ($IsExp == 1) {

                    $uuser = AllUser::find()->where([
                        'UserId' => $profile->UserId
                    ])->one();

                    $uuser->IsExp = $IsExp;
                    $uuser->EntryType = "Experience";

                    $uuser->save(false);
                }

                // return $this->render('editprofile',['profile'=>$profile,'skill'=>$allskill,'position'=>$allposition,'course'=>$allcourse]);

                Yii::$app->session->setFlash('success', "Profile Updated Successfully");

                return $this->redirect([
                    '/wall/candidateprofile'
                    // 'profilepage'
                ]);
            } else {
                $profile->language = $profile->language ? explode(', ', $profile->language) : array();
				$whiteCategories =  ArrayHelper::map(WhiteCategory::find()->where([
					'status' => 1
				])
					->orderBy([
					'name' => 'SORT_ASC'
				])
                ->all(), 'id', 'name');
                return $this->render('editprofile', [
                    'prefered_loc' => $prefered_loc,
                    'cities' => $cities,
                    'naukari_specialization' => $naukari_specialization,
                    'NaukariQualData' => $NaukariQualData,
                    'profile' => $profile,
                    'skill' => $allskill,
                    'position' => $allposition,
                    'course' => $allcourse,
                    'industry' => $allindustry,
                    'docmodel' => $docmodel1,
					'whiteCategories' => $whiteCategories
                ]);
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionApplyjob($JobId)
    {
        $this->layout = "blanklayout";
        if (isset(Yii::$app->session['Employeeid'])) {
            $model = new AppliedJob();
            $jobdet = PostJob::find()->where([
                'JobId' => $JobId
            ])->one();
            $user = AllUser::find()->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])->one();
			
            if (isset(Yii::$app->request->post()['applyjobwith'])) {
                $altemail = explode("|", $jobdet->AltEmail);

                $docmodel = new Documents();
                $image = UploadedFile::getInstance($user, 'CVId');
                if ($image) {
                    $image_id = $docmodel->imageUpload($image, 'cv');
                } else {
                    $image_id = $user->CVId;
                }
                $user->CVId = $image_id;
                $user->save(false);
                $title = $jobdet->JobTitle;
                $name = $user->Name;
                if ($user && $user->CVId != 0) {
                    $na = explode(" ", $user->Name);
                    $name1 = $na[0];
                    $extar = explode(".", $user->cV->Doc);
                    $ext = $extar[1];
                    $url = 'https://www.mycareerbugs.com/backend/web/imageupload/';
                    $cvpath = $url . $user->cV->Doc;
                    $nn = $name1 . '.' . $ext;
                    $link = "<a style='width:260px; margin:10px auto 20px; background: #f16b22; font-size: 13px; font-weight: bold; text-align:center; color: #fff;padding: 7px 10px;font-family: Arial,Helvetica,sans-serif;text-decoration: none;display: block;'  href='$cvpath' class='cv' download='$nn'>$name Resume</a>";
                } else {
                    // get your HTML raw content without any layouts or scripts
                    $content = $this->renderPartial('makecv', [
                        'profile' => $user
                    ], true);
                    $ppp = preg_replace('/\s+/', '_', $user->Name . $user->UserId);
                    $ext = 'pdf';
                    $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";
                    // setup kartik\mpdf\Pdf component
                    $pdf = new Pdf([
                        // set to use core fonts only
                        'mode' => Pdf::MODE_CORE,
                        // A4 paper format
                        'format' => Pdf::FORMAT_A4,
                        // portrait orientation
                        'orientation' => Pdf::ORIENT_PORTRAIT,
                        // stream to browser inline
                        'filename' => $path,
                        'destination' => Pdf::DEST_FILE,
                        // your html content input
                        'content' => $content,
                        // format content from your own css file if needed or use the
                        // enhanced bootstrap css built by Krajee for mPDF formatting
                        'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
                        // any css to be embedded if required
                        'cssInline' => '.kv-heading-1{font-size:18px}',
                        // set mPDF properties on the fly
                        'options' => [
                            'title' => 'My Career Bugs'
                        ],
                        // call mPDF methods on the fly
                        'methods' => [
                            'SetHeader' => [
                                'My Career Bugs'
                            ],
                            'SetFooter' => [
                                '{PAGENO}'
                            ]
                        ]
                    ]);
                    echo $pdf->render($path, 'F');
                    $url = 'https://www.mycareerbugs.com/backend/web/imageupload/';
                    $cvpath = $url . $ppp . ".{$ext}";
                    $name = $user->Name;
                    $link = "<a style='width:260px; margin:10px auto 20px; background: #f16b22; font-size: 13px; font-weight: bold; text-align:center; color: #fff;padding: 7px 10px;font-family: Arial,Helvetica,sans-serif;text-decoration: none;display: block; ' href='$cvpath' class='cv'>$name Resume</a>";
                }
				
                foreach ($altemail as $ak => $av) {

                    // mail
                    $to = $av;
                    $from = Yii::$app->params['adminEmail'];
                    $subject = "Job title: $title Applied by $name";
                    $html = "<html>
                    <head>
                    <title>Job Applied </title>
                    <style>
                    .im {
                    color:#4d4c4c!important;        
                    }
                    </style>
                    </head>
                    <body>
                    <table style='width:660px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#fff;text-align:left'>
                     <tbody>
                     <tr>
                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin:10px auto 20px;width:260px;'></td>
                                        </tr>
                                <tr>
                                            <td>
                                            <div style='border:1px solid #cccccc; padding:10px'>
                                            
                                            <h2 style='font-size:16px;display:block;padding:24px 10px 0 0;margin:0 0 10px 0'>Dear,</h2>
                                            <p style='font-size:13px;padding:0 10px 10px 0;margin:0 0 20px 0; border-bottom:1px solid #cccccc'>
				You are receiving this mail because the candidate likes your Job post
				and  to be suitable for the following job opportunity posted on <a>www.mycareerbugs.com</a>, and you can directly view their profile.
			</p>
                                            
                                            <h2 style=' font-size:14px;  border-bottom: 2px solid #f16b22;text-align:center;  display:block;padding:10px ; margin:0 0 0px 0; color:#f16b22; font-weight:bold;'> 
                                             
                             $name is applied on your job $title
                               <br/>
                              $link
                   
                    </h2>
                    
                    
                    <p style='font-size: 12px; margin: 20px 0 0px 0; padding: 10px;color: #9d9d9d;'> The sender of this email is registered with <a href='https://mycareerbugs.com/'>mycareerbugs.com <a/> and using <a href='https://mycareerbugs.com/'>mycareerbugs.com <a/> services. The responsibility of checking the authenticity of offers/correspondence lies with you. </p>
                    <p style='font-size: 12px; margin: 0px 0 0px 0; padding: 10px;color: #9d9d9d;'> © 2019 My Career Bugs Job Portal Pvt Ltd </p>
                      
                    </div>
                    </td>
                    </tr>
                    <tr>
                    </tbody>
                    </table>
                    </body>
                    </html>";
                    $mail = new ContactForm();
                    $mail->sendEmail($to, $from, $html, $subject);
                    // mail
                }
            }

            if ($jobdet->PostFor == 'Candidate' || $jobdet->PostFor == 'Both') {

                $alreadyapplied = $model->find()
                    ->where([
                    'IsDelete' => 0,
                    'JobId' => $JobId,
                    'UserId' => Yii::$app->session['Employeeid']
                ])
                    ->one();

                if ($alreadyapplied) {

                    if ($alreadyapplied->Status == 'Bookmark') {

                        $alreadyapplied->Status = "Applied";

                        $alreadyapplied->save(false);

                        Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
                    } else {

                        Yii::$app->session->setFlash('error', "You have already applied for this Job.");
                    }
                } else {

                    $model->JobId = $JobId;

                    $model->UserId = Yii::$app->session['Employeeid'];

                    $model->OnDate = date("Y-m-d");

                    $model->Status = "Applied";

                    $model->save(false);

                    $jobdet = PostJob::find()->where([
                        'JobId' => $JobId
                    ])->one();

                    $empnt = new EmpNotification();

                    $empnt->EmpId = $jobdet->EmployerId;

                    $empnt->UserId = Yii::$app->session['Employeeid'];

                    $empnt->JobId = $JobId;

                    $empnt->Type = 'Applied';

                    $empnt->ForType = 'Candidate';

                    $empnt->IsView = 0;

                    $empnt->OnDate = date('Y-m-d H:i:s');

                    $empnt->save(false);

                    Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
                }
            } else if ($jobdet->PostFor == 'Campus' && $user->CampusId != 0) {

                $model->JobId = $JobId;

                $model->UserId = Yii::$app->session['Employeeid'];

                $model->OnDate = date("Y-m-d");

                $model->Status = "Applied";

                $model->save(false);

                $jobdet = PostJob::find()->where([
                    'JobId' => $JobId
                ])->one();

                $empnt = new EmpNotification();

                $empnt->EmpId = $jobdet->EmployerId;

                $empnt->UserId = Yii::$app->session['Employeeid'];

                $empnt->JobId = $JobId;

                $empnt->Type = 'Applied';

                $empnt->ForType = 'Candidate';

                $empnt->IsView = 0;

                $empnt->OnDate = date('Y-m-d H:i:s');

                $empnt->save(false);

                Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
            } else {

                Yii::$app->session->setFlash('error', "This job is not for you");
            }

            $noofjobapplied = $model->find()
                ->where([
                'Status' => 'Applied',
                'UserId' => Yii::$app->session['Employeeid'],
                'IsDelete' => 0
            ])
                ->count();

            Yii::$app->session['NoofjobApplied'] = $noofjobapplied;

            $jobdet = PostJob::find()->where([
                'JobId' => $JobId
            ])->one();

            if ($jobdet->IsWalkin == 1) {

                return $this->redirect([
                    'job/'.$jobdet->Slug
                ]);
            } else {

                return $this->redirect([
                    'job/'.$jobdet->Slug
                ]);
            }
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $jobdet = PostJob::find()->where([
                'JobId' => $JobId
            ])->one();

            $model = new AppliedJob();

            if ($jobdet->PostFor == 'Team' || $jobdet->PostFor == 'Both') {

                $alreadyapplied = $model->find()
                    ->where([
                    'IsDelete' => 0,
                    'JobId' => $JobId,
                    'UserId' => Yii::$app->session['Teamid']
                ])
                    ->one();

                if ($alreadyapplied) {

                    if ($alreadyapplied->Status == 'Bookmark') {

                        $alreadyapplied->Status = "Applied";

                        $alreadyapplied->save(false);

                        Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
                    } else {

                        Yii::$app->session->setFlash('error', "You have already applied for this Job.");
                    }
                } else {

                    $model->JobId = $JobId;

                    $model->UserId = Yii::$app->session['Teamid'];

                    $model->OnDate = date("Y-m-d");

                    $model->Status = "Applied";

                    $model->save(false);

                    $jobdet = PostJob::find()->where([
                        'JobId' => $JobId
                    ])->one();

                    $empnt = new EmpNotification();

                    $empnt->EmpId = $jobdet->EmployerId;

                    $empnt->UserId = Yii::$app->session['Teamid'];

                    $empnt->JobId = $JobId;

                    $empnt->Type = 'Applied';

                    $empnt->ForType = 'Team';

                    $empnt->IsView = 0;

                    $empnt->OnDate = date('Y-m-d H:i:s');

                    $empnt->save(false);

                    Yii::$app->session->setFlash('success', "You are successfully applied this Job.");
                }
            } else {

                Yii::$app->session->setFlash('error', "This job is not for you");
            }

            $noofjobapplied = $model->find()
                ->where([
                'Status' => 'Applied',
                'UserId' => Yii::$app->session['Teamid']
            ])
                ->count();

            Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;

            return $this->redirect([
                'job/'.$jobdet->Slug
            ]);
        } else {

            Yii::$app->session->setFlash('error', "Please Login to apply this job");
			$jobdet = PostJob::find()->where([
                'JobId' => $JobId
            ])->one();
            return $this->redirect([
                'login',
                'Job' => $jobdet->Slug
            ]);
        }
    }

    public function actionAppliedjob()

    {
        if (isset(Yii::$app->session['Campusid'])) {

            $postfor = array(
                'Campus'
            );
        } elseif (isset(Yii::$app->session['Employeeid'])) {

            $postfor = array(
                'Candidate',
                'Both'
            );
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $postfor = array(
                'Team',
                'Both'
            );
        } else {

            $postfor = array(
                'Candidate'
            );
        }

        // footer section

        // first block

        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $model = new AppliedJob();

        $appliedjobs = $model->find()->where([
            'IsDelete' => 0,
            'Status' => 'Applied',
            'UserId' => Yii::$app->session['Employeeid']
        ]);

        $pages = new Pagination([
            'totalCount' => $appliedjobs->count()
        ]);

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }

        $appliedjobs = $appliedjobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        // Top Job

        $topjob = PostJob::find()->where([
            'IsDelete' => 0,
            'Jobstatus' => 0,
            'PostFor' => $postfor
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(5)
            ->all();

        return $this->render('appliedjob', [
            'model' => $appliedjobs,
            'pages' => $pages,
            'topjob' => $topjob
        ]);
    }

    public function actionBookmark($JobId)

    {
        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        if (isset(Yii::$app->session['Employeeid'])) {

            $model = new AppliedJob();

            $alreadyapplied = $model->find()
                ->where([
                'IsDelete' => 0,
                'JobId' => $JobId,
                'UserId' => Yii::$app->session['Employeeid']
            ])
                ->one();

            if ($alreadyapplied) {

                if ($alreadyapplied->Status == 'Bookmark') {

                    Yii::$app->session->setFlash('error', "You have already bookmarked  this Job.");
                } else {

                    Yii::$app->session->setFlash('error', "You have already Applied  this Job.");
                }
            } else {

                $model->JobId = $JobId;

                $model->UserId = Yii::$app->session['Employeeid'];

                $model->OnDate = date("Y-m-d");

                $model->Status = "Bookmark";

                $model->save(false);

                $noofjobapplied = $model->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsDelete' => 0
                ])
                    ->count();

                Yii::$app->session['NoofjobApplied'] = $noofjobapplied;

                $jobdet = PostJob::find()->where([
                    'JobId' => $JobId
                ])->one();

                $empnt = new EmpNotification();

                $empnt->EmpId = $jobdet->EmployerId;

                $empnt->UserId = Yii::$app->session['Employeeid'];

                $empnt->JobId = $JobId;

                $empnt->Type = 'Bookmarked';

                $empnt->ForType = 'Candidate';

                $empnt->IsView = 0;

                $empnt->OnDate = date('Y-m-d H:i:s');

                $empnt->save(false);

                // var_dump($model->getErrors());

                Yii::$app->session->setFlash('success', "You have Bookmarked a job.");
            }

            return $this->redirect([
                'jobsearch'
            ]);
        } elseif (isset(Yii::$app->session['Teamid'])) {

            $model = new AppliedJob();

            $alreadyapplied = $model->find()
                ->where([
                'IsDelete' => 0,
                'JobId' => $JobId,
                'UserId' => Yii::$app->session['Teamid']
            ])
                ->one();

            if ($alreadyapplied) {

                if ($alreadyapplied->Status == 'Bookmark') {

                    Yii::$app->session->setFlash('error', "You have already bookmarked  this Job.");
                } else {

                    Yii::$app->session->setFlash('error', "You have already Applied  this Job.");
                }
            } else {

                $model->JobId = $JobId;

                $model->UserId = Yii::$app->session['Teamid'];

                $model->OnDate = date("Y-m-d");

                $model->Status = "Bookmark";

                $model->save(false);

                $noofjobapplied = $model->find()
                    ->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Teamid']
                ])
                    ->count();

                Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;

                $jobdet = PostJob::find()->where([
                    'JobId' => $JobId
                ])->one();

                $empnt = new EmpNotification();

                $empnt->EmpId = $jobdet->EmployerId;

                $empnt->UserId = Yii::$app->session['Teamid'];

                $empnt->JobId = $JobId;

                $empnt->Type = 'Bookmarked';

                $empnt->ForType = 'Team';

                $empnt->IsView = 0;

                $empnt->OnDate = date('Y-m-d H:i:s');

                $empnt->save(false);

                // var_dump($model->getErrors());

                Yii::$app->session->setFlash('success', "You have Bookmarked a job.");
            }

            return $this->redirect([
                'jobsearch'
            ]);
        } else {

            Yii::$app->session->setFlash('error', "Please Login to bookmark this job");

            return $this->redirect([
                'login'
            ]);
        }
    }

    public function actionBookmarkjob()

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'layout';

        $model = new AppliedJob();

        $appliedjobs = $model->find()->where([
            'IsDelete' => 0,
            'Status' => 'Bookmark',
            'UserId' => Yii::$app->session['Employeeid']
        ])->orderBy([
            'OnDate' => SORT_DESC
        ]);

        $pages = new Pagination([
            'totalCount' => $appliedjobs->count()
        ]);

        if (isset(Yii::$app->request->get()['perpage'])) {

            $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
        } else {

            $pages->defaultPageSize = 10;
        }

        $appliedjobs = $appliedjobs->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('bookmarkedjob', [
            'pages' => $pages,
            'model' => $appliedjobs
        ]);
    }

    public function actionDeletejob()

    {
        $JobId = Yii::$app->request->get()['jobid'];

        $userid = Yii::$app->request->get()['userid'];

        $applied = AppliedJob::find()->where([
            'JobId' => $JobId,
            'UserId' => $userid
        ])->one();

        $applied->IsDelete = 1;

        if ($applied->save(false)) {

            if (isset(Yii::$app->session['Employeeid'])) {

                $noofjobapplied = AppliedJob::find()->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsDelete' => 0
                ])->count();

                Yii::$app->session['NoofjobApplied'] = $noofjobapplied;
            }

            if (isset(Yii::$app->session['Teamid'])) {

                $noofjobapplied = AppliedJob::find()->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Teamid'],
                    'IsDelete' => 0
                ])->count();

                Yii::$app->session['NoofjobAppliedt'] = $noofjobapplied;
            }

            if (isset(Yii::$app->session['Campusid'])) {

                $noofjobapplied = AppliedJob::find()->where([
                    'Status' => 'Applied',
                    'UserId' => Yii::$app->session['Campusid'],
                    'IsDelete' => 0
                ])->count();

                Yii::$app->session['NoofjobAppliedc'] = $noofjobapplied;
            }

            echo json_encode(1);
        } else {

            echo json_encode(0);
        }
    }

    public function actionJobedit()

    {
		//Yii::$app->cache->flush();
		
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $JobId = Yii::$app->request->get()['JobId'];

            $pf = Yii::$app->request->get()['pf'];

            $postjob = new PostJob();

            $postdetail = $postjob->find()
                ->where([
                'JobId' => $JobId
            ])
                ->one();

            $position = new Position();

            $jobcategory = new JobCategory();

            $allposition = $position->find()
                ->where("IsDelete=0")
                ->all();

            // $allcategory=$jobcategory->find()->where("IsDelete=0")->all();

            $allcategory = Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all();

            $locationar = Location::find()->all();

            $allcourse = Course::find()->where("IsDelete=0")
                ->orderBy([
                'CourseName' => 'SORT_ASC'
            ])
                ->all();
				
			$whiteCategoryList =  ArrayHelper::map(WhiteCategory::find()->where([
                'status' => 1
            ])
                ->orderBy([
                'name' => 'SORT_ASC'
            ])
                ->all(), 'id', 'name');		
			
            $logoold = $postdetail->LogoId;

            $docmodel = new Documents();
			
			

            if ($postdetail->load(Yii::$app->request->post())) {
				/*echo '<pre>';
				print_r(Yii::$app->request->post());die;*/
                if (! empty($postdetail->Location) && is_array($postdetail->Location)) {
					$postdetail->Location = implode(',', $postdetail->Location);
                }
                $logo_id = $logoold;

                if ($docmodel->load(Yii::$app->request->post())) {
					Yii::$app->cache->flush();
                    $docmodel->image = \yii\web\UploadedFile::getInstance($docmodel, 'Doc');

                    if ($docmodel->image) {

						$logo_id = Documents::handleImageUpload($docmodel, 'Doc', 'LogoId');
						
                        /*$image = UploadedFile::getInstance($docmodel, 'Doc');

                        $ext = end((explode(".", $image->name)));

                        $ppp = Yii::$app->security->generateRandomString();

                        $path = Yii::$app->params['uploadPath'] . $ppp . ".{$ext}";

                        $docmodel->Doc = 'imageupload/' . $ppp . ".{$ext}";

                        $docmodel->OnDate = date('Y-m-d');

                        $docmodel->From = 'CompanyLogo';

                        if ($docmodel->save(false)) {

                            $logo_id = $docmodel->DocId;

                            $furl = str_replace('frontend', 'backend', (Yii::$app->params['uploadPath']));

                            $thumb = $furl . 'thumb_' . $logo_id . '.' . $ext;

                            if (file_exists($thumb)) {

                                $document = Documents::find()->where([
                                    'DocId' => $logo_id
                                ])->one();

                                $document->Doc = 'imageupload/thumb_' . $logo_id . '.' . $ext;

                                $document->save(false);
                            }
                        } else {

                            $logo_id = 0;
                        }*/
                    }
                }

                $postdetail->LogoId = $logo_id;

                $altemail = '';

                $altemailar = Yii::$app->request->post()['PostJob']['AltEmail'];

                foreach ($altemailar as $ak => $aval) {

                    $altemail .= $aval . '|';
                }

                $altemail = trim($altemail, "|");

                $postdetail->AltEmail = $altemail;
				
				if (! empty(Yii::$app->request->post()['PostJob']['SpecialQualification'])) {
					$postdetail->SpecialQualification = implode(',', Yii::$app->request->post()['PostJob']['SpecialQualification']);
				}else{
					$postdetail->SpecialQualification = "";
				}

				if (! empty(Yii::$app->request->post()['PostJob']['course'])) {
					$postdetail->course = implode(',', Yii::$app->request->post()['PostJob']['course']);
				}else{
					$postdetail->course = "";
				}
				
				if (! empty(Yii::$app->request->post()['PostJob']['Qualifications'])) {
					$postdetail->qualifications = implode(',', Yii::$app->request->post()['PostJob']['Qualifications']);
				}else{
					$postdetail->qualifications = "";
				}
				
				$postdetail->LandlineNo = Yii::$app->request->post()['PostJob']['LandlineNo'];
				$postdetail->ContactPerson = Yii::$app->request->post()['PostJob']['ContactPerson'];
				$postdetail->JobSpecification = Yii::$app->request->post()['PostJob']['JobSpecification'];
				$postdetail->TechnicalGuidance = Yii::$app->request->post()['PostJob']['TechnicalGuidance'];
				
				if(!empty(Yii::$app->request->post()['PostJob']['WhiteCategory'])){
					$postdetail->WhiteCategoryId = Yii::$app->request->post()['PostJob']['WhiteCategory'];
				}
				/*echo '<pre>';
				print_r(Yii::$app->request->post()['PostJob']);die;*/
				
				$postdetail->save(false);

                // var_dump($postdetail->getErrors());

               
                // special skill

                $speckill = Yii::$app->request->post()['PostJob']['SpecialQualification'];
				if(!empty($speckill)){
					foreach ($speckill as $rk => $rval) {
						if ($rval != '') {
							$sindskill = trim($rval);
							$snskill = new SpecialQualification();
							$cskill = $snskill->find()
								->where([
								'Special' => $sindskill
							])
								->one();
							if (! $cskill) {
								$snskill->Special = $sindskill;
								$snskill->save(false);
							}
						}
					}
				}
                

                // $snskill=new Course();

                // //$snskill->CourseName=Yii::$app->request->post()['PostJob']['SpecialQualification'];

                // $snskill->CourseId=Yii::$app->request->post()['PostJob']['SpecialQualification'];

                // //$snskill->OnDate=date('Y-m-d');

                // $snskill->save(false);

                // other

                $other = explode(",", Yii::$app->request->post()['PostJob']['OtherSalary']);
				if(!empty($other)){
					foreach ($other as $rk => $rval) {
						if ($rval != '') {
							$oindskill = trim($rval);
							$onskill = new OtherSalaryDetail();
							$cskill = $onskill->find()
								->where([
								'Other' => $oindskill
							])
								->one();

							if (! $cskill) {
								$onskill->Other = $oindskill;
								$onskill->save(false);
							}
						}
					}
				}
                

                // jobshift

                $jobshift = explode(",", Yii::$app->request->post()['PostJob']['JobShift']);
				if(!empty($jobshift)){
					foreach ($jobshift as $rk => $rval) {

                    if ($rval != '') {

                        $jindskill = trim($rval);

                        $jnskill = new JobShift();

                        $cskill = $jnskill->find()
                            ->where([
                            'JobShift' => $jindskill
                        ])
                            ->one();

                        if (! $cskill) {

                            $jnskill->JobShift = $jindskill;

                            $jnskill->save(false);
                        }
                    }
                }
				}
                

                // new skill			
				
				if(!empty(Yii::$app->request->post()['PostJob']['KeySkill'])){
					 $jobrelatedskill = new JobRelatedSkill();

					JobRelatedSkill::deleteAll([
						'JobId' => $JobId
					]);

					$skillData = Yii::$app->request->post()['PostJob']['KeySkill'];
						foreach($skillData as $skillVal){
							$jobrelatedskill = new JobRelatedSkill();
							$keySkillFind = $jobrelatedskill->find()
								->where([
								'JobId' => $JobId,
								'SkillId' => $skillVal
							])->one();
							if(empty($keySkillFind)){
								$jobrelatedskill->JobId = $JobId;
								$jobrelatedskill->SkillId = $skillVal;
								$jobrelatedskill->OnDate = date('Y-m-d');
								$jobrelatedskill->save(false);
							}
						}
					}
                /*$rawskill = explode(",", Yii::$app->request->post()['PostJob']['RawSkill']);

                foreach ($rawskill as $rk => $rval) {

                    if ($rval != '') {

                        $indskill = trim($rval);

                        $nskill = new Skill();

                        $cskill = $nskill->find()
                            ->where([
                            'Skill' => $indskill,
                            'IsDelete' => 0
                        ])
                            ->one();

                        if (! $cskill) {

                            $nskill->Skill = $indskill;

                            $nskill->OnDate = date('Y-m-d');

                            $nskill->save(false);

                            $skid = $nskill->SkillId;
                        } else {

                            $skid = $cskill->SkillId;
                        }

                        $jobrelatedskill = new JobRelatedSkill();

                        $jobrelatedskill->JobId = $JobId;

                        $jobrelatedskill->SkillId = $skid;

                        $jobrelatedskill->OnDate = date('Y-m-d');

                        $jobrelatedskill->save(false);
                    }
                }*/
				
				/*Updated White List Role & Skills*/
				$whiteRole = (!empty(Yii::$app->request->post()['PostJob']['WhiteRole'])?Yii::$app->request->post()['PostJob']['WhiteRole']:"");
				$whiteSkills = (!empty(Yii::$app->request->post()['PostJob']['WhiteSkills'])?Yii::$app->request->post()['PostJob']['WhiteSkills']:"");
					if(!empty($whiteRole)){
						PostJobWRole::deleteAll([
							'postjob_id' => $JobId,
							'user_id' => Yii::$app->session['Employerid']
						]);
						foreach($whiteRole as $value){
							if($value != ""){
								$whiteRole = WhiteRole::find()->where([
										'id' => $value
									])->one();
								if(!empty($whiteRole)){
									$wRole = new PostJobWRole();
									$wRole->postjob_id = $JobId;
									$wRole->user_id = Yii::$app->session['Employerid'];
									$wRole->role_id = $value;
									$wRole->role = $whiteRole->name;
									$wRole->on_date = date('Y-m-d');
									$wRole->save(false);	
								}
							}
						}
						/*Notifications are pending*/
					}
							
							if(!empty($whiteSkills)){
								PostJobWSkill::deleteAll([
									'postjob_id' => $JobId,
									'user_id' => Yii::$app->session['Employerid']
								]);
								foreach($whiteSkills as $value){
									if($value != ""){
										$whiteSkillRec = WhiteSkill::find()->where([
												'id' => $value
											])->one();
										if(!empty($whiteSkillRec)){
											$wSkill = new PostJobWSkill();
											$wSkill->postjob_id = $JobId;
											$wSkill->user_id = Yii::$app->session['Employerid'];
											$wSkill->skill_id = $value;
											$wSkill->skill = $whiteSkillRec->name;
											$wSkill->on_date = date('Y-m-d');
											$wSkill->save(false);	
										}
									}
								}
								/*Notifications are pending*/
							}
						
				

                Yii::$app->session->setFlash('success', 'Successfully Edited This Job.');
				//die($postdetail->Slug);
                return $this->redirect([
                    'postdetail?JobId='.$postdetail->JobId.'&pf=0'
                ]);
            } else {
				$NaukariQualData = NaukariSpecialization::find()->all();
                $naukari_specialization = NaukariQualData::find()->where("parent_id!=4 and parent_id!=5 and parent_id!=6 and id!=1 and id!=2 and id!=3")->all();
				$topQualifications  = NaukariQualData::getHighestQualificationList();
				$whiteCategoryRole = ArrayHelper::map(WhiteRole::find()->where([
										'status' => 1,
										'category_id' => $postdetail->WhiteCategoryId
										
									])
										->orderBy([
										'name' => 'SORT_ASC'
									])
										->all(), 'id', 'name');
				/*Get Selected Role Ids*/						
				$selectedWhiteRole = ArrayHelper::map(PostJobWRole::find()->where([
										'postjob_id' => $postdetail->JobId,
										'user_id' => $postdetail->EmployerId
									])
									  ->all(), 'role_id', 'role_id');	
									  
				/*Get Select White Collar Skills*/	
				$selectedWhiteSkills = ArrayHelper::map(PostJobWSkill::find()->where([
										'postjob_id' => $postdetail->JobId,
										'user_id' => $postdetail->EmployerId
									])->all(), 'skill_id', 'skill_id');
									
				$whiteSkills = 	ArrayHelper::map(WhiteSkill::find()->where([
										'id' => $selectedWhiteSkills,
										'status' => 1
									])->all(), 'id', 'name');				  
								
                return $this->render('jobedit', [
                    'allpost' => $postdetail,
					'NaukariQualData' => $NaukariQualData,
					'topQualifications' => $topQualifications,
					'naukari_specialization' => $naukari_specialization,
                    'position' => $allposition,
                    'jobcategory' => $allcategory,
                    'pf' => $pf,
                    'location' => $locationar,
                    'docmodel' => $docmodel,
                    'course' => $allcourse,
                    'whiteCategories' => $whiteCategoryList,
					'whiteCategoryRole' => $whiteCategoryRole,
					'selectedWhiteRole' => $selectedWhiteRole,
					'selectedWhiteSkills' => $selectedWhiteSkills,
					'whiteSkills' => $whiteSkills
                ]);
				
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionFeedback()

    {

        // footer section

        // first block
        $about = new FooterAboutus();

        $footerabout = $about->find()->one();

        Yii::$app->view->params['footerabout'] = $footerabout;

        // contactus block

        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // notification for employee

        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $docmodel = new Documents();

        $this->layout = 'layout';

        $feedback = new Feedback();

        if ($feedback->load(Yii::$app->request->post())) {

            $logo = UploadedFile::getInstance($feedback, 'Logo');

            if ($logo) {

                $logo_id = $docmodel->imageUpload($logo, 'Feedbackphoto');
            } else {

                $logo_id = 0;
            }

            $feedback->Logo = $logo_id;

            $feedback->IsDelete = 0;

            $feedback->OnDate = date('Y-m-d');

            if ($feedback->save(false)) {

                $to = $feedback->Email;

                $from = Yii::$app->params['adminEmail'];

                $subject = "Thank you for your feedback";

                $html = "<html>

               <head>

               <title>Feedback</title>

               </head>

               <body>

               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>

                <tbody><tr>

                                       <td><img src='https://mycareerbugs.com/images/logo.png' title='Career Bugs' alt='Career Bugs' style='margin-top:10px;width:200px;'></td>

                                   </tr>

                                   <tr>

                                       <td style='height:30px'></td>

                                   </tr>

                           <tr>

                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='https://mycareerbugs.com' target='_blank'> Career Bugs </a>

               <br><br>

               <span style='font-size:16px;line-height:1.5'>

                 <h3> Dear , $to</h3>

Thank you for your feedback

               <br/>

               </span>

               </h2>

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                Yii::$app->session->setFlash('success', "Thank you for your feedback");
            } else {

                Yii::$app->session->setFlash('error', "There is some error please try again");
            }

            return $this->render('feedback', [
                'feedback' => $feedback
            ]);
        } else {

            return $this->render('feedback', [
                'feedback' => $feedback
            ]);
        }
    }

    public function actionThankyou()

    {

        // notification for employee
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        return $this->render('thankyou');
    }

    public function actionEmpdashboard()
    {
        if (isset(Yii::$app->session['Employerid'])) {
            // second block
            $jobcategory = new JobCategory();
            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $postjob = new PostJob();

            $totaljob = $postjob->find()
                ->where([
                'EmployerId' => Yii::$app->session['Employerid']
            ])
                ->count();

            $totalactive = $postjob->find()
                ->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'JobStatus' => 0
            ])
                ->count();

            $candidateapplied = AppliedJob::find()->where([
                'PostJob.EmployerId' => Yii::$app->session['Employerid'],
                'Status' => 'Applied',
                'AppliedJob.IsDelete' => 0
            ])
                ->joinWith([
                'job'
            ])
                ->count();

            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();

            $todayusedContactView = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'ContactView'
            ])->count();

            $todayusedEmail = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'Email'
            ])->count();

            $todayusedCVDownload = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'CVDownload'
            ])->count();
            $todayusedDownload = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'Download'
            ])->count();

            $todayusedSMS = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'SMS'
            ])->count();

            $todayusedWapp = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'whatsappview'
            ])->count();

            $lastusedContactView = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'Type' => 'ContactView'
            ])
                ->andWhere([
                '<',
                'DATE(UpdatedDate)',
                date('Y-m-d')
            ])
                ->count();

            $lastusedEmail = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'Type' => 'Email'
            ])
                ->andWhere([
                '<',
                'DATE(UpdatedDate)',
                date('Y-m-d')
            ])
                ->count();

            $lastusedCVDownload = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'Type' => 'CVDownload'
            ])
                ->andWhere([
                '<=',
                'DATE(UpdatedDate)',
                date('Y-m-d')
            ])
                ->count();

            $lastusedSMS = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'Type' => 'SMS'
            ])
                ->andWhere([
                '<',
                'DATE(UpdatedDate)',
                date('Y-m-d')
            ])
                ->count();

            $candidatebookmarked = AppliedJob::find()->where([
                'PostJob.EmployerId' => Yii::$app->session['Employerid'],
                'Status' => 'Bookmark',
                'AppliedJob.IsDelete' => 0
            ])
                ->joinWith([
                'job'
            ])
                ->count();
	        $planassign = PlanAssign::find()->where(['EmployerId' => Yii::$app->session['Employerid'],'IsDelete' => 0])->one();
            return $this->render('employerdashboard', [
                'totaljob' => $totaljob,
                'totalactivejob' => $totalactive,
                'appliedjob' => $candidateapplied,
                'bookmarked' => $candidatebookmarked,
                'planassign' => $planassign,
                'todayusedContactView' => $todayusedContactView,
                'todayusedWapp' => $todayusedWapp,
                'todayusedEmail' => $todayusedEmail,
                'todayusedCVDownload' => $todayusedCVDownload,
                'todayusedSMS' => $todayusedSMS,
                'lastusedContactView' => $lastusedContactView,
                'lastusedEmail' => $lastusedEmail,
                'lastusedCVDownload' => $lastusedCVDownload,
                'todayusedDownload' => $todayusedDownload,
                'lastusedSMS' => $lastusedSMS,
                'planassign'=>$planassign
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionUserdashboard()

    {
        if (isset(Yii::$app->session['Employeeid'])) {

            $postfor = array(
                'Candidate',
                'Both'
            );

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $candidateapplied = AppliedJob::find()->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'Status' => 'Applied',
                'IsDelete' => 0
            ])->count();

            $user = Alluser::find()->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])->one();
            $role = 0;
            if ($user->empRelatedSkills) {
                foreach ($user->empRelatedSkills as $ask => $asv) {
                    if ($asv->skill->position->Position) {
                        $role = $asv->skill->position->PositionId;
                    }
                    if (! empty($role)) {
                        break;
                    }
                }
            }
            $candidatebookmarked = AppliedJob::find()->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'Status' => 'Bookmark',
                'IsDelete' => 0
            ])->count();

            $recentjob = PostJob::find()->where([
                'IsDelete' => 0,
                'Jobstatus' => 0,
                'PositionId' => $role
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(10)
                ->all();

            return $this->render('userdashboard', [
                'alljob' => $recentjob,
                'appliedjob' => $candidateapplied,
                'bookmarked' => $candidatebookmarked
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionCandidateapplied()

    {
		
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $postjob = new PostJob();

            $mailmodel = new Mail();

            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();

            $totalemail = $planassign->TotalEmail - $planassign->UseEmail;

            if ($mailmodel->load(Yii::$app->request->post())) {

                $mailmodel->Type = 'Mail';

                if ($mailmodel->save(false)) {

                    $sender = AllUser::find()->where([
                        'UserId' => Yii::$app->session['Employerid']
                    ])->one();

                    $mailtext = $mailmodel->MailText;

                    $email = trim($mailmodel->EmployeeId, "|");

                    $to = explode("|", $email);

                    $from = $sender->Name . ' <' . $sender->Email . '>';

                    $subject = $mailmodel->Subject;

                    $senderemail = $sender->Email;

                    $sendername = $sender->Name;

                    $mailgo = ($totalemail > count($to)) ? count($to) : count($to) - $totalemail;

                    $m = 0;

                    if ($mailgo > 0) {

                        $planassign->UseEmail = $planassign->UseEmail + $mailgo;

                        $planassign->save(false);

                        $planrecord = new PlanRecord();

                        $planrecord->PlanId = $planassign->PlanId;

                        $planrecord->EmployerId = $planassign->EmployerId;

                        $planrecord->Type = 'Email';

                        $planrecord->Amount = $mailgo;

                        $planrecord->save(false);

                        foreach ($to as $key => $value) {

                            $m ++;

                            if ($m <= $mailgo) {

                                $receiver = AllUser::find()->select([
                                    'Name'
                                ])
                                    ->Where([
                                    'Email' => $value
                                ])
                                    ->one();

                                $receivername = $receiver->Name;

                                $html = "<html>

                    <head>

                    <title>$subject</title>

                    <style>

                    .im {

                    color:#4d4c4c!important;        

                    }

                    </style>

                    </head>

                    <body>

                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;line-height: 1.6;'>

                     <tbody><tr>

                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='My career Bugs' alt='Mycareer Bugs' style='margin-top:10px;width:200px;'></td>

                                        </tr>

                                       

                    <tr>

                                            <td><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:3%;margin:auto;color: #333;text-align: left;font-size:12px'>

                            The sender of this email is registered with mycareerbugs.com as $sendername. To respond back directly to the Employer, please click on Reply button, or send an email to $senderemail 

                    </h2>

                    </td>

                    </tr>

                    

                    <tr>

                    <td>

                    <table style='width:91%;height:auto;color:#4d4c4c;background:#fff;text-align:center;text-align: left;padding: 4%;padding-top: 0%;font-family: arial;font-size: 13px;margin-left: 4.5%;line-height: 1.8;'>

                   <tr>

                    <td><br/> <b>   Dear $receivername,</b> <br/><br/></td>

                   </tr>

                   <tr>

                    <td>

                    $mailtext

                    </td>

                   </tr>

                    </table>

                    </td>

                    </tr>

                    <tr>

                                        <td style='border-bottom: 1px solid #ccc;'></td>

                    </tr>

                    <td>

                    <p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'><b>Disclaimer:</b></p>

                    </td>

                    </tr>

                    <tr>

                                        <td style='border-bottom: 1px solid #333;'></td>

                    </tr>

                    <tr>

                    <td><p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'>

                    The sender of this email is registered with <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> as Mycareer bugs Job Portal Private Limited (info@mycareerbugs.com, CF 318 Sector 1 saltlake) using <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> services. The responsibility of checking the authenticity of offers/correspondence lies with you. If you consider the content of this email inappropriate or spam, you may: Forward this email to: compliance@mycareerbugs.com. <br/>

<b>Please Note:</b> This mail is a private message from the recruiter. We have enabled auto login for your convenience, you are strongly advised not to forward this email to protect your account from unauthorized access.<br/>

<b>Advisory:</b> Please do not pay any money to anyone who promises to find you a job. This could be in the form of a registration fee or document processing fee or visa charges or any other pretext. The money could be asked for upfront or it could be asked after trust has been built after some correspondence has been exchanged. Also please note that in case you get a job offer or a letter of intent without having been through an interview process it is probably a scam and you should contact compliance@mycareerbugs.com for advise.

                    </p></td>

                    </tr>

                     </tbody>

                    </table>

                    </body>

                    </html>";

                                $mail = new ContactForm();

                                $mail->sendEmail($value, $from, $html, $subject);
                            }
                        }

                        Yii::$app->session->setFlash('success', "Mail sent Successfully");
                    } else {

                        Yii::$app->session->setFlash('error', "Your Data plan is already Used");
                    }
                } else {

                    // var_dump($mailmodel->getErrors());

                    Yii::$app->session->setFlash('error', "There is some error please try again");
                }

                return $this->redirect([
                    'candidateapplied'
                ]);
            } else {

                if (isset($_GET['JobId']) && $_GET['JobId'] > 0) {

                    $postfor = array(
                        'Candidate',
                        'Both'
                    );

                    $candidateapplied = AppliedJob::find()->where([
                        'AppliedJob.JobId' => $_GET['JobId'],
                        'PostJob.EmployerId' => Yii::$app->session['Employerid'],
                        'PostJob.PostFor' => $postfor,
                        'UserTypeId' => 2,
                        'Status' => 'Applied',
                        'AppliedJob.IsDelete' => 0
                    ])
                        ->joinWith([
                        'job',
                        'user'
                    ])
                        ->groupBy([
                        'AppliedJob.JobId',
                        'AppliedJob.UserId'
                    ])
                        ->orderBy([
                        'AppliedJob.OnDate' => SORT_DESC
                    ]);
                    $candidateapplied = AppliedJob::find()->where([
                        'JobId' => $_GET['JobId'],
                        'Status' => 'Applied'
                    ]);
                    $pages = new Pagination([
                        'totalCount' => $candidateapplied->count()
                    ]);

                    if (isset(Yii::$app->request->get()['perpage'])) {

                        $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
                    } else {

                        $pages->defaultPageSize = 10;
                    }

                    $candidateapplied = $candidateapplied->offset($pages->offset)
                        ->limit($pages->limit)
                        ->all();
                }

                $pj = Yii::$app->session['Employerid'];
                $sql = "SELECT * FROM PostJob AS jp LEFT JOIN AppliedJob AS jvp ON jvp.JobId = jp.JobId WHERE jvp.UserId= '" . $pj . "'  AND jp.`PostFor` IN ('Both', 'Candidate' )";
                $cmd = Yii::$app->db->createCommand($sql);
                $alljb = $cmd->queryAll();
                $sql = "SELECT * FROM PostJob  WHERE  EmployerId= '" . $pj . "'  AND  JobStatus = 0 AND  IsDelete = 0 AND `PostFor` IN ('Both', 'Candidate' )";
                $cmd = Yii::$app->db->createCommand($sql);
                $alljb = $cmd->queryAll();
                $allpost = PostJob::find()->where([
                    'JobStatus' => 0,
                    'IsDelete' => 0,
                    'PostFor' => $postfor
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
                $allplan = Plan::find()->where([
                    'IsDelete' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
                $language_opts = LanguagesOpts::find()->all();
                $naukari_specialization = NaukariSpecialization::find()->all();
                $NaukariQualData = NaukariQualData::find()->where([
                    'parent_id' => 0
                ])->all();

                $allposition = Position::find()->select([
                    'Position',
                    'PositionId'
                ])
                    ->where([
                    'IsDelete' => 0
                ])
                    ->orderBy([
                    'Position' => SORT_ASC
                ])
                    ->all();
                $allindustry = Industry::find()->where([
                    'IsDelete' => 0
                ])
                    ->orderBy([
                    'IndustryName' => 'SORT_ASC'
                ])
                    ->all();

                $allcourse = Course::find()->where("IsDelete=0")
                    ->orderBy([
                    'CourseName' => 'SORT_ASC'
                ])
                    ->all();

                $cities = Cities::find()->all();
                $allskill = Skill::find()->where([
                    'IsDelete' => 0
                ])->all();
                $planassign = new PlanAssign();

                $isassign = $planassign->isAssign(Yii::$app->session['Employerid']);

                // echo "<pre>";print_r($candidateapplied);die();
                if (isset($_GET['JobId']) && $_GET['JobId'] > 0) {

                    return $this->render('candidateapplied_new', [
                        'appliedjob' => $candidateapplied,
                        'pages' => $pages,
                        'mailmodel' => $mailmodel,
                        'allpost' => $allpost,
                        'language_opts' => $language_opts,
                        'allplan' => $allplan,
                        'naukari_specialization' => $naukari_specialization,
                        'NaukariQualData' => $NaukariQualData,
                        'allcategory' => $allposition,
                        'allindustry' => $allindustry,
                        'isassign' => $isassign,
                        'allskill' => $allskill,
                        'course' => $allcourse,
                        'cities' => $cities
                    ]);
                } else {
                    return $this->render('candidateapplied', [
                        'appliedjob' => $candidateapplied,
                        'pages' => $pages,
                        'mailmodel' => $mailmodel,
                        'allpost' => $alljb,
                        'language_opts' => $language_opts,
                        'allplan' => $allplan,
                        'naukari_specialization' => $naukari_specialization,
                        'NaukariQualData' => $NaukariQualData,
                        'allcategory' => $allposition,
                        'allindustry' => $allindustry,
                        'isassign' => $isassign,
                        'allskill' => $allskill,
                        'course' => $allcourse,
                        'cities' => $cities
                    ]);
                }
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionCampusapplied()

    {
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $postjob = new PostJob();

            $mailmodel = new Mail();

            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();

            $totalemail = $planassign->TotalEmail - $planassign->UseEmail;

            if ($mailmodel->load(Yii::$app->request->post())) {

                $mailmodel->Type = 'Mail';

                if ($mailmodel->save(false)) {

                    $sender = AllUser::find()->where([
                        'UserId' => Yii::$app->session['Employerid']
                    ])->one();

                    $mailtext = $mailmodel->MailText;

                    $email = trim($mailmodel->EmployeeId, "|");

                    $to = explode("|", $email);

                    $from = $sender->Name . ' <' . $sender->Email . '>';

                    $subject = $mailmodel->Subject;

                    $senderemail = $sender->Email;

                    $sendername = $sender->Name;

                    $mailgo = ($totalemail > count($to)) ? count($to) : count($to) - $totalemail;

                    $m = 0;

                    if ($mailgo > 0) {

                        $planassign->UseEmail = $planassign->UseEmail + $mailgo;

                        $planassign->save(false);

                        $planrecord = new PlanRecord();

                        $planrecord->PlanId = $planassign->PlanId;

                        $planrecord->EmployerId = $planassign->EmployerId;

                        $planrecord->Type = 'Email';

                        $planrecord->Amount = $mailgo;

                        $planrecord->save(false);

                        foreach ($to as $key => $value) {

                            $m ++;

                            if ($m <= $mailgo) {

                                $receiver = AllUser::find()->select([
                                    'Name'
                                ])
                                    ->Where([
                                    'Email' => $value
                                ])
                                    ->one();

                                $receivername = $receiver->Name;

                                $html = "<html>

                    <head>

                    <title>$subject</title>

                    <style>

                    .im {

                    color:#4d4c4c!important;        

                    }

                    </style>

                    </head>

                    <body>

                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;line-height: 1.6;'>

                     <tbody><tr>

                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career Bugs' alt='My Career Bugs' style='margin-top:10px;width:200px;'></td>

                                        </tr>

                                       

                    <tr>

                                            <td><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:3%;margin:auto;color: #333;text-align: left;font-size:12px'>

                            The sender of this email is registered with mycareerbugs.com as $sendername. To respond back directly to the Employer, please click on Reply button, or send an email to $senderemail 

                    </h2>

                    </td>

                    </tr>

                    

                    <tr>

                    <td>

                    <table style='width:91%;height:auto;color:#4d4c4c;background:#fff;text-align:center;text-align: left;padding: 4%;padding-top: 0%;font-family: arial;font-size: 13px;margin-left: 4.5%;line-height: 1.8;'>

                   <tr>

                    <td><br/> <b>   Dear $receivername,</b> <br/><br/></td>

                   </tr>

                   <tr>

                    <td>

                    $mailtext

                    </td>

                   </tr>

                    </table>

                    </td>

                    </tr>

                    <tr>

                                        <td style='border-bottom: 1px solid #ccc;'></td>

                    </tr>

                    <td>

                    <p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'><b>Disclaimer:</b></p>

                    </td>

                    </tr>

                    <tr>

                                        <td style='border-bottom: 1px solid #333;'></td>

                    </tr>

                    <tr>

                    <td><p style='font-size: 11px;color: #333;text-align: left;padding: 1%;'>

                    The sender of this email is registered with <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> as Cygnet Infotech Pvt. Ltd (rushad.shah@cygnetinfotech.com, 2 Manikyam Opp Samudra Annexes Off. C.G.Road,, AHMEDABAD, Gujarat - 380009) using <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a> services. The responsibility of checking the authenticity of offers/correspondence lies with you. If you consider the content of this email inappropriate or spam, you may: Forward this email to: compliance@mycareerbugs.com. <br/>

<b>Please Note:</b> This mail is a private message from the recruiter. We have enabled auto login for your convenience, you are strongly advised not to forward this email to protect your account from unauthorized access.<br/>

<b>Advisory:</b> Please do not pay any money to anyone who promises to find you a job. This could be in the form of a registration fee or document processing fee or visa charges or any other pretext. The money could be asked for upfront or it could be asked after trust has been built after some correspondence has been exchanged. Also please note that in case you get a job offer or a letter of intent without having been through an interview process it is probably a scam and you should contact compliance@mycareerbugs.com for advise.

                    </p></td>

                    </tr>

                     </tbody>

                    </table>

                    </body>

                    </html>";

                                $mail = new ContactForm();

                                $mail->sendEmail($value, $from, $html, $subject);
                            }
                        }

                        Yii::$app->session->setFlash('success', "Mail sent Successfully");
                    } else {

                        Yii::$app->session->setFlash('error', "Your Data plan is already Used");
                    }
                } else {

                    // var_dump($mailmodel->getErrors());

                    Yii::$app->session->setFlash('error', "There is some error please try again");
                }

                return $this->redirect([
                    'campusapplied'
                ]);
            } else {

                $candidateapplied = AppliedJob::find()->where([
                    'PostJob.EmployerId' => Yii::$app->session['Employerid'],
                    'PostJob.PostFor' => 'Campus',
                    'UserTypeId' => 4,
                    'Status' => 'Applied',
                    'AppliedJob.IsDelete' => 0
                ])
                    ->joinWith([
                    'job',
                    'user'
                ])
                    ->groupBy([
                    'AppliedJob.JobId',
                    'AppliedJob.UserId'
                ])
                    ->orderBy([
                    'AppliedJob.OnDate' => SORT_DESC
                ]);

                $pages = new Pagination([
                    'totalCount' => $candidateapplied->count()
                ]);

                if (isset(Yii::$app->request->get()['perpage'])) {

                    $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
                } else {

                    $pages->defaultPageSize = 10;
                }

                $candidateapplied = $candidateapplied->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

                return $this->render('campusapplied', [
                    'appliedjob' => $candidateapplied,
                    'pages' => $pages,
                    'mailmodel' => $mailmodel
                ]);
            }
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionCandidatebookmarked()

    {
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $postjob = new PostJob();

            $candidateapplied = AppliedJob::find()->where([
                'PostJob.EmployerId' => Yii::$app->session['Employerid'],
                'Status' => 'Bookmark',
                'AppliedJob.IsDelete' => 0
            ])
                ->joinWith([
                'job',
                'user'
            ])
                ->orderBy([
                'AppliedJob.OnDate' => SORT_DESC
            ]);

            $pages = new Pagination([
                'totalCount' => $candidateapplied->count()
            ]);

            if (isset(Yii::$app->request->get()['perpage'])) {

                $pages->defaultPageSize = Yii::$app->request->get()['perpage'];
            } else {

                $pages->defaultPageSize = 10;
            }

            $candidateapplied = $candidateapplied->offset($pages->offset)
                ->limit($pages->limit)
                ->all();

            return $this->render('candidateapplied', [
                'appliedjob' => $candidateapplied,
                'pages' => $pages
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionCampusnotification()

    {
        if (isset(Yii::$app->session['Campusid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            $this->layout = 'layout';

            $notification = new Notification();

            $campusnotification = $notification->getcampusnot();

            return $this->render('campusnotification', [
                'allnotification' => $campusnotification
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionEmpnotification()

    {
        if (isset(Yii::$app->session['Employerid'])) {

            // footer section

            // first block

            $about = new FooterAboutus();

            $footerabout = $about->find()->one();

            Yii::$app->view->params['footerabout'] = $footerabout;

            // contactus block

            $cn = new FooterContactus();

            $footercontact = $cn->find()->one();

            Yii::$app->view->params['footercontact'] = $footercontact;

            // second block

            $jobcategory = new JobCategory();

            $allhotcategory = $jobcategory->find()
                ->select([
                'CategoryName',
                'JobCategoryId'
            ])
                ->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->limit(7)
                ->all();

            Yii::$app->view->params['hotcategory'] = $allhotcategory;

            // copyright block

            $cp = new FooterCopyright();

            $allcp = $cp->find()->one();

            Yii::$app->view->params['footercopyright'] = $allcp;

            // developer block

            $dblock = new FooterDevelopedblock();

            $developerblock = $dblock->find()->one();

            Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

            // socialicon block

            $socialicon = new SocialIcon();

            $sicon = $socialicon->find()->one();

            Yii::$app->view->params['footersocialicon'] = $sicon;

            // third block

            $th = new FooterThirdBlock();

            $thirdblock = $th->find()->one();

            Yii::$app->view->params['footerthirdblock'] = $thirdblock;

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $allntemp1 = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid']
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();

            return $this->render('empnotification', [
                'allnotification' => $allntemp1
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionCandidatenotification()

    {
        if (isset(Yii::$app->session['Employeeid'])) {

            // notification for employee

            $allnotification = array();

            $notification = new Notification();
            if (isset(Yii::$app->session['Employeeid'])) {

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
				   ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            $this->layout = 'layout';

            $allnotification1 = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid']
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();

            return $this->render('candidatenotification', [
                'allnotification' => $allnotification1
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionBlog()

    {

        // notification for employee
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'bloglayout';

        $allblog = Blog::find()->where([
            'IsDelete' => 0
        ]) ->orderBy([
          'OnDate' => SORT_DESC
        ])->all();

        $allblogcategory = Blog::find()->select([
            'count(*) as cnt',
            'BlogCategory.BlogCategoryName as BlogCategoryName',
            'BlogCategory.BlogCategoryId as BlogCategoryId'
        ])
            ->joinWith([
            'blogCategory'
        ])
            ->where([
            'Blog.IsDelete' => 0
        ])
			->orderBy([
                'Blog.OnDate' => SORT_DESC
        ])
            ->groupBy([
            'Blog.BlogCategoryId'
        ])
            ->all();

        // recent post

        $recent = Blog::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(2)
            ->all();

        return $this->render('blog', [
            'allblog' => $allblog,
            'allblogcategory' => $allblogcategory,
            'recent' => $recent
        ]);
    }

    public function actionBlogcategory($catid = null )

    {

        // notification for employee
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'bloglayout';
	
		$categorySlug = $slug = (isset(Yii::$app->request->get()['category'])?Yii::$app->request->get()['category']:"");
				
		if(!empty($categorySlug)){
			 
			 $categoryDetail = BlogCategory::find()->where([
				'Slug' => $categorySlug
			 ])->one();
			
			if(!empty($categoryDetail)){
				$catid = $categoryDetail->BlogCategoryId;
			}else{
				return $this->redirect([
					'blog'
				]);
			}			
		}else{
			return $this->redirect([
                'blog'
            ]);
		}	
			
	
        $allblog = Blog::find()->where([
            'IsDelete' => 0,
            'Blog.BlogCategoryId' => $catid
        ]) ->orderBy([
                'OnDate' => SORT_DESC
            ])->all();

        $allblogcategory = Blog::find()->select([
            'count(*) as cnt',
            'BlogCategory.BlogCategoryName as BlogCategoryName',
            'BlogCategory.BlogCategoryId as BlogCategoryId'
        ])
            ->joinWith([
            'blogCategory'
        ])
            ->where([
            'Blog.IsDelete' => 0
        ])
			->orderBy([
                'Blog.OnDate' => SORT_DESC
        ])
            ->groupBy([
            'Blog.BlogCategoryId'
        ])
            ->all();

        // recent post

        $recent = Blog::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(2)
            ->all();

        return $this->render('blog', [
            'allblog' => $allblog,
            'allblogcategory' => $allblogcategory,
            'recent' => $recent
        ]);
    }

    public function actionBlogdetails($BlogId = null)

    {
		
        // notification for employee
        $allnotification = array();

        if (isset(Yii::$app->session['Employeeid'])) {

            $notification = new Notification();

            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
			->GroupBy([
				'JobId'
			])
                ->all();
        }

        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer

        $allntemp = array();

        if (isset(Yii::$app->session['Employerid'])) {

            $empnot = new EmpNotification();

            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }

        Yii::$app->view->params['employernotification'] = $allntemp;

        // notification end

        $this->layout = 'blogdetaillayout';
		
		$blogSlug = $slug = (isset(Yii::$app->request->get()['alias'])?Yii::$app->request->get()['alias']:"");
				
		if(!empty($blogSlug)){
			 $blogdetail = Blog::find()->where([
				'BlogSlug' => $blogSlug
			])->one();
			
			if(!empty($blogdetail)){
				$BlogId = $blogdetail->BlogId;
			}else{
				return $this->redirect([
					'blog'
				]);
			}

		}else{
			return $this->redirect([
                'blog'
            ]);
		}
		
       
        $allblog = Blog::find()->where([
            'IsDelete' => 0
        ])->all();

        $allblogcategory = Blog::find()->select([
            'count(*) as cnt',
            'BlogCategory.BlogCategoryName as BlogCategoryName',
            'BlogCategory.BlogCategoryId as BlogCategoryId'
        ])
            ->joinWith([
            'blogCategory'
        ])
            ->where([
            'Blog.IsDelete' => 0
        ])
            ->groupBy([
            'Blog.BlogCategoryId'
        ])
            ->all();

        // recent post

        $recent = Blog::find()->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(2)
            ->all();

        // you may also like

        $alsolike = Blog::find()->where([
            'BlogCategoryId' => $blogdetail->BlogCategoryId,
            'IsDelete' => 0
        ])
            ->andWhere("BlogId!=$BlogId")
            ->all();

        // allcomment

        $blogc = BlogComment::find()->where([
            'BlogId' => $BlogId,
            'IsDelete' => 0
        ])->all();

        $blogcomment = new BlogComment();



        if ($blogcomment->load(Yii::$app->request->post()) && $blogcomment->validate()) {

            $blogcomment->IsDelete = 0;

            $blogcomment->OnDate = date('Y-m-d');

            $blogcomment->BlogId = $BlogId;

            $blogcomment->Cid = 0;

            $blogcomment->save(false);

            return $this->redirect([
                'blogdetails',
                'BlogId' => $BlogId
            ]);
        } else {

            return $this->render('blogdetails', [
                'allblog' => $allblog,
                'blogdetail' => $blogdetail,
                'allblogcategory' => $allblogcategory,
                'recent' => $recent,
                'alsolike' => $alsolike,
                'model' => $blogcomment,
                'blogc' => $blogc
            ]);
        }
    }

    public function actionSearchblog()

    {
        $blogname = Yii::$app->request->get()['blogname'];

        $blog = Blog::find()->where([
            'like',
            'BlogName',
            $blogname
        ])->one();

        if ($blog) {

            $blogid = $blog->BlogId;
			$categorySlug = $blog->blogCategory->Slug;
			$postSlug = $blog->BlogSlug;
            return $this->redirect([
                '/blog/'.$categorySlug.'/'.$postSlug
            ]);
        } else {

            Yii::$app->session->setFlash('error', 'No blog found.');

            return $this->redirect([
                'blog'
            ]);
        }
    }

    public function actionCustomerdetail()

    {
        if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Campusid'])) {

            $planassign = PlanAssign::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'IsDelete' => 0
            ])->one();
            // Start // Vishal Sinha
            if ($planassign->isExpired()) {
                $data = [];
                $data['message'] = 'Your Plan is Expired, Please Contact Administration';
                \Yii::$app->session->setFlash('error', $data['message']);
                return $this->redirect([
                    '/hirecandidate'
                ]);
            }
            
            $planRecordModel = Plan::find()->where(['PlanId'=>$planassign->PlanId])->select(['total_daily_view_contact'])->one();

            $todayusedCVDownload = PlanRecord::find()->where([
            'PlanId'=>$planassign->PlanId,
                'EmployerId' => Yii::$app->session['Employerid'],
                'DATE(UpdatedDate)' => date('Y-m-d'),
                'Type' => 'CVDownload'
            ])->count();

            if($planRecordModel->total_daily_view_contact < $todayusedCVDownload)
            {
              $todayusedCVDownload = PlanRecord::find()->where([
                    'EmployerId' => Yii::$app->session['Employerid'],
                    'UserId'=> $userId,
                    'Type' => 'CVDownload'
                ]);
                if(empty($todayusedCVDownload))
                {
                    $data['message'] = 'You have reached your daily limit.';
                    \Yii::$app->session->setFlash('error', $data['message']);
                    return $this->redirect([
                        '/hirecandidate'
                    ]);
                }
            }
            
            $planrecord_cnt = PlanRecord::isAlreadyRecord($planassign, $_GET['UserId'], 'ContactView');
            if ($planrecord_cnt == 0) {
                PlanRecord::createNewPlanRecord($planassign, $_GET['UserId'], 'ContactView', 'UseViewContact');
            }
            // end

            $daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView', $planassign->plan->PlanId, date("Y-m-d"));
            $planrecord_cnt = PlanRecord::find()->where([
                'EmployerId' => Yii::$app->session['Employerid'],
                'UserId' => $_GET['UserId'],
                'Type' => 'ContactView',
                'PlanId' => $planassign->PlanId
            ])->count();
            $availableviewcontact = $planassign->plan->total_daily_view_contact - $daily_Contact;
            if ($availableviewcontact <= 0 && $planrecord_cnt == 0) {
                throw new \yii\web\NotFoundHttpException();
                exit();
            }

            if (isset(Yii::$app->request->get()['page']) && Yii::$app->request->get()['page'] == 'CampNotification') {
                if (isset(Yii::$app->request->get()['Nid'])) {
                    $nid = Yii::$app->request->get()['Nid'];
                    $notification = CampNotification::find()->where([
                        'Id' => $nid
                    ])->one();
                    $notification->IsView = 1;
                    $notification->save(false);
                }
            } else {
                if (isset(Yii::$app->request->get()['Nid'])) {
                    $nid = Yii::$app->request->get()['Nid'];
                    $notification = EmpNotification::find()->where([
                        'EmpnId' => $nid
                    ])->one();
                    $notification->IsView = 1;
                    $notification->save(false);
                }
            }
            // notification for employee
            $allnotification = array();
            if (isset(Yii::$app->session['Employeeid'])) {
                $notification = new Notification();
                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $userid = Yii::$app->request->get()['UserId'];

            $candidate = AllUser::find()->where([
                'UserId' => $userid,
                'IsDelete' => 0
            ])->one();

            return $this->render('candidatedetail', [
                'candidate' => $candidate
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionCampusdetail()

    {
        if (isset(Yii::$app->session['Employerid'])) {

            if (isset(Yii::$app->request->get()['Nid'])) {

                $nid = Yii::$app->request->get()['Nid'];

                $notification = EmpNotification::find()->where([
                    'EmpnId' => $nid
                ])->one();

                $notification->IsView = 1;

                $notification->save(false);
            }

            // notification for employee

            $allnotification = array();

            if (isset(Yii::$app->session['Employeeid'])) {

                $notification = new Notification();

                $allnotification = $notification->find()
                    ->where([
                    'UserId' => Yii::$app->session['Employeeid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
					->GroupBy([
					'JobId'
				])
                    ->all();
            }

            Yii::$app->view->params['employeenotification'] = $allnotification;

            // notification for employer

            $allntemp = array();

            if (isset(Yii::$app->session['Employerid'])) {

                $empnot = new EmpNotification();

                $allntemp = $empnot->find()
                    ->where([
                    'EmpId' => Yii::$app->session['Employerid'],
                    'IsView' => 0
                ])
                    ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                    ->all();
            }

            Yii::$app->view->params['employernotification'] = $allntemp;

            // notification end

            $this->layout = 'layout';

            $userid = Yii::$app->request->get()['UserId'];

            $candidate = AllUser::find()->where([
                'UserId' => $userid,
                'IsDelete' => 0
            ])->one();

            return $this->render('campusdetail', [
                'campus' => $candidate
            ]);
        } else {

            return $this->redirect([
                'index'
            ]);
        }
    }

    public function actionJobmatchyourprofile()

    {
        $this->layout = 'blanklayout';

        $date = date('Y-m-d');

        // $date='2017-05-03';

        $postfor = array(
            'Candidate',
            'Both'
        );

        $notification = Notification::find()->where([
            'date(Notification.OnDate)' => $date,
            'PostJob.JobStatus' => 0,
            'PostJob.PostFor' => $postfor,
            'AllUser.IsDelete' => 0,
            'AllUser.UserTypeId' => 2
        ])
            ->joinWith([
            'job',
            'user'
        ])
            ->groupBy([
            'UserId'
        ])
            ->all();

        foreach ($notification as $key => $value) {

            $ntdetail = Notification::find()->where([
                'date(Notification.OnDate)' => $date,
                'Notification.UserId' => $value->UserId,
                'PostJob.JobStatus' => 0,
                'PostJob.PostFor' => 'Candidate',
                'AllUser.IsDelete' => 0
            ])
                ->joinWith([
                'job',
                'user'
            ])
                ->groupBy([
                'Notification.JobId'
            ])
                ->all();

            $var = '';

            foreach ($ntdetail as $nk => $nv) {

                $skills = '';

                foreach ($nv->job->jobRelatedSkills as $s => $sv) {

                    $skills .= $sv->skill->Skill . ',';
                }

                $skills = trim($skills, ",");

                if ($nv->job->IsExp == 0) {

                    $exp = 'Fresher';
                } else if ($nv->job->IsExp == 1) {

                    $exp = $nv->job->Experience . ' Year';
                } else if ($nv->job->IsExp == 2) {

                    $exp = 'Both Candidate and Team';
                }

                $var .= '<tr style="background: #fff;border-bottom: 1px solid #333;"><td style="line-height: 1.6;font-size: 12px;padding: 2%"><b style="color: #f16b22;text-transform:uppercase;">' . $nv->job->JobTitle . '</b> ( ' . $exp . ' )<br/> ' . $nv->job->CompanyName . '<span style="color: #ccc;"> ( ' . $nv->job->Location . ',' . $nv->job->City . ' ) </span><br/><br/>' . substr($nv->job->Description, 0, 100) . ' <br/> Keyskills : <span style="color:#f16b22;">' . $skills . '</span> <br/>Role : <span style="color:#f16b22;">' . $nv->job->position->Position . '</span><a href="https://mycareerbugs.com/job/' . $nv->job->Slug . '"><input type="button" value="Apply" style="padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: right;margin-right: 5px;border: none;"/></a></td></tr>';
            }

            // mail

            $to = $value->user->Email;

            // $to='rohit.09012@gmail.com';

            $from = Yii::$app->params['adminEmail'];

            $subject = "Jobs Matching Your Profile";

            $name = $value->user->Name;

            $lastupdate = date('dS M Y', strtotime($value->user->UpdatedDate));

            $html = "<html>

                    <head>

                    <title>Jobs Matching Your Profile</title>

                    <style>

                    .im {

                    color:#4d4c4c!important;        

                    }

                    </style>

                    </head>

                    <body>

                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>

                     <tbody><tr>

                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='Careerbug' alt='Careerbug' style='margin-top:10px;width:200px;'></td>

                                        </tr>

                                       

                                <tr>

                                            <td><h2 style='width:85%;font-weight:normal;background:#f16b22;padding:3%;margin:auto;color: #fff;text-align: left;font-size:14px'>

                              Dear $name (Login ID : $to) , new job posted just now for you

                              <a href='https://mycareerbugs.com/site/editprofile'><input type='button' style='padding: 10px;color: #fff;background: #6c146b;font-size: 12px;float: right;margin-right: 5px;border: none;' value='Update Profile'></a>

                               

                    <br><br>

                    </h2>

                    </td>

                    </tr>

                    <tr>

                    <td>

                    <table style='width:100%;height:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;text-align: left;padding: 4%;padding-top: 0%;'>

                    $var

                    </table>

                    </td>

                    </tr>

                    <tr>

                    <td><p style='font-size: 11px;color: #333;'>

                                        You are receiving this mail because you have chosen to follow job updates by recruiters on <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a>.

                                        <br/><br/><br/>

                    This is a system-generated e-mail, please don't reply to this message. The jobs sent in this mail have been posted by the clients of <a href='https://mycareerbugs.com' style='color: #f16b22;' target='_blank'>mycareerbugs.com</a>.  Users are advised to research bonafides of advertisers independently. We recommend that you visit our <a href='https://mycareerbugs.com/content/terms' style='color: #f16b22;' target='_blank'>Terms & Conditions</a> and the Security Advice for more comprehensive information.



                    </p></td>

                    </tr>

                     </tbody>

                    </table>

                    </body>

                    </html>";

            $mail = new ContactForm();

            $mail->sendEmail($to, $from, $html, $subject);

            // mail
        }
    }

    public function actionDeletejobalert($alertid)
    {
        $alert = JobAlert::find()->where([
            'JobAlertId' => $alertid
        ])->one();
        if ($alert->delete()) {
            Yii::$app->session->setFlash('success', "Job alert deleted successfully");
        } else {
            Yii::$app->session->setFlash('error', "There is some error please try again");
        }
        return $this->redirect([
            'index'
        ]);
    }

    public function actionDailyjobalert()
    {
        $this->layout = 'blanklayout';
        $date = date('Y-m-d');
        // echo $date='2017-04-06';
        $dailyjobalert = JobAlert::find()->all();

        foreach ($dailyjobalert as $dk => $dval) {

            $jobalertid = $dval->JobAlertId;

            $allskill = explode(",", $dval->Keyword);
            $as = array();

            $location = $dval->Location;

            foreach ($allskill as $ak => $aval) {

                if ($aval != '') {

                    $as[] = trim($aval);
                }
            }

            $sklist = array();

            $skilllist = Skill::find()->where([
                'Skill' => $as
            ])->all();

            foreach ($skilllist as $sk => $sv) {

                $sklist[] = $sv->SkillId;
            }

            // $jobrelatedskill=JobRelatedSkill::find()->where(['SkillId'=>$sklist])->groupBy(['JobId'])->all();

            $var = '';

            // foreach($jobrelatedskill as $key=>$nv)

            // {

            // if(($nv->job->PostFor=='Candidate' || $nv->job->PostFor=='Both') && $nv->job->IsDelete==0 && $nv->job->JobStatus==0)

            // {

            // $skills='';

            // foreach($nv->job->jobRelatedSkills as $s=>$sv)

            // {

            // $skills.=$sv->skill->Skill.',';

            // }

            // $skills=trim($skills,",");

            //

            // if($nv->job->IsExp==0)

            // {

            // $exp='Fresher';

            // }

            // else if($nv->job->IsExp==1)

            // {

            // $exp=$nv->job->Experience.' Year';

            // }

            // else if($nv->job->IsExp==2)

            // {

            // $exp='Both Candidate and Team';

            // }

            // $var.='<tr style="background: #fff;border-bottom: 1px solid #333;"><td style="line-height: 1.6;font-size: 12px;padding: 2%"><b style="color: #f16b22;text-transform:uppercase;">'.$nv->job->JobTitle.'</b> ( '.$exp.' )<br/> '.$nv->job->CompanyName.'<span style="color: #ccc;"> ( '.$nv->job->Location.','.$nv->job->City.' ) </span><br/><br/>'.substr($nv->job->Description,0,100).' <br/> Keyskills : <span style="color:#f16b22;">'.$skills.'</span> <br/>Role : <span style="color:#f16b22;">'.$nv->job->position->Position.'</span><a href="http://mycareerbugs.com/site/jobdetail?JobId='.$nv->job->JobId.'"><input type="button" value="Apply" style="padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: right;margin-right: 5px;border: none;"/></a></td></tr>';

            // }

            // }

            $postfor = array(
                'Candidate',
                'Both'
            );

            $alljob = PostJob::find()->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor
            ])
                ->joinWith([
                'jobRelatedSkills'
            ])
                ->andFilterWhere([
                'or',

                [
                    'like',
                    'Designation',
                    $allskill
                ],

                [
                    'like',
                    'CompanyName',
                    $allskill
                ],

                [
                    'JobRelatedSkill.SkillId' => $sklist
                ]
            ])
                ->andFilterWhere([
                'Location' => $location
            ])
                ->groupBy([
                'PostJob.JobId'
            ])
                ->orderBy([
                'PostJob.OnDate' => SORT_DESC
            ])
                ->limit(6)
                ->all();

            foreach ($alljob as $key => $nv) {

                $skills = '';

                foreach ($nv->jobRelatedSkills as $s => $sv) {

                    $skills .= $sv->skill->Skill . ',';
                }

                $skills = trim($skills, ",");

                if ($nv->IsExp == 0) {

                    $exp = 'Fresher';
                } else if ($nv->IsExp == 1) {

                    $exp = $nv->Experience . ' Year';
                } else if ($nv->IsExp == 2) {

                    $exp = 'Both Candidate and Team';
                }

                $var .= '<tr style="background: #fff;border-bottom: 1px solid #333;"><td style="line-height: 1.6;font-size: 12px;padding: 2%"><b style="color: #f16b22;text-transform:uppercase;">' . $nv->JobTitle . '</b> ( ' . $exp . ' )<br/> ' . $nv->CompanyName . '<span style="color: #ccc;"> ( ' . $nv->Location . ',' . $nv->City . ' ) </span><br/><br/>' . substr($nv->Description, 0, 200) . ' <br/> Keyskills : <span style="color:#f16b22;">' . $skills . '</span> <br/>Role : <span style="color:#f16b22;">' . $nv->position->Position . '</span><a href="http://mycareerbugs.com/job/' . $nv->Slug . '"><input type="button" value="Apply" style="padding: 10px;color: #fff;background: #f16b22;font-size: 12px;float: right;margin-right: 5px;border: none;"/></a></td></tr>';

                $jobid = $nv->JobId;
            }

            // echo $dval->EmailId.'----'.$var.'<br/>';

            // mail

            if ($var != '') {

                $to = $dval->EmailId;

                $userdetail = AllUser::find()->where([
                    'Email' => $to
                ])->one();

                // $to='rohit.09012@gmail.com';

                if ($userdetail) {

                    $name = $userdetail->Name;

                    $lastupdate = date('dS M Y', strtotime($userdetail->UpdatedDate));
                } else {

                    $name = $to;

                    $lastupdate = '';
                }

                $from = Yii::$app->params['adminEmail'];

                $subject = "Jobs Matching Your Profile";

                $html = "<html>

                    <head>

                    <title>Jobs Matching Your Profile</title>

                    <style>

                    .im {

                    color:#4d4c4c!important;        

                    }

                    </style>

                    </head>

                    <body>

                    <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>

                     <tbody><tr>

                                            <td><img src='https://mycareerbugs.com/images/logo.png' title='My Career bugs' alt='My Career Bugs' style='margin-top:10px;width:200px;'></td>

                                        </tr>

                                       

                                <tr>

                                            <td><h2 style='width:85%;font-weight:normal;background:#f16b22;padding:3%;margin:auto;color: #fff;text-align: left;font-size:14px'>

                              Dear $name , new job posted just now for you

                              <a href='https://mycareerbugs.com/site/editprofile'><input type='button' style='padding: 10px;color: #fff;background: #6c146b;font-size: 12px;float: right;margin-right: 5px;border: none;' value='Update Profile'></a>

                               

                    <br><br>

                    </h2>

                    </td>

                    </tr>

                    <tr>

                    <td>

                    <table style='width:100%;height:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;text-align: left;padding: 4%;padding-top: 0%;'>

                    $var

                    </table>

                     <table style='width:100%;height:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center;text-align: left;padding: 4%;padding-top: 0%;margin-top: 0%;'>

                   <tr style='background: #fff;border-bottom: 1px solid #333;'>

                    <td style='line-height: 1.6;font-size: 12px;padding: 2%'>

                    Are these jobs relevant to you

                    <br/><a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;margin:0 5px;font-weight:bold' href='https://mycareerbugs.com/site/jobrelevant?type=yes&email=$to&JobId=$jobid'>Yes</a> | <a target='_blank' style='background:#5d6f8a;padding:5px;text-align:center;color:#fff!important;text-decoration:none;font-weight:bold' href='https://mycareerbugs.com/site/jobrelevant?type=no&email=$to&JobId=$jobid'>No</a> <br/>

                    

                    Looking for different kind of job?<br/>

                    <a href='http://mycareerbugs.com/site/index#jobalertbox'><input type='button' value='Create Another Job Alert' style='padding: 10px;background: #fff;font-size: 12px;float: right;margin-right: 5px;border: 1px solid #333;color: #333;'/></a>

                    </td>

                    <td style='line-height: 1.6;font-size: 12px;padding: 2%'>

                    <div style='width: 80%;height: auto;padding: 1px;background: #f2f2f2;'>

                    <p>Join Us On &nbsp;

                    <a href=''><img src='https://mycareerbugs.com/images/facebook.png' style='width: 25px'/></a>&nbsp;&nbsp;

                    <a href=''><img src='https://mycareerbugs.com/images/twitter.png' style='width: 25px;'/></a>&nbsp;&nbsp;

                    <a href=''><img src='https://mycareerbugs.com/images/linkedin.png' style='width: 25px;'/></a>&nbsp;&nbsp;

                    </p>

                    </div>

                    

                   Want to stop getting jobs for this alert?<br/>

                    <a href='http://mycareerbugs.com/site/deletejobalert?alertid=$jobalertid'><input type='button' value='Delete this search alert' style='padding: 10px;background: #fff;font-size: 12px;float: right;margin-right: 5px;border: 1px solid #333;color: #333;'/></a>

                    </td>

                   </tr>

                    </table>

                    </td>

                    </tr>

                    <tr>

                    <td><p style='font-size: 11px;color: #333;'>

                                        You are receiving this mail because you have chosen to follow job updates by recruiters on <a href='https://mycareerbugs.com' target='_blank' style='color: #f16b22;'>mycareerbugs.com</a>.

                                        <br/><br/><br/>

                    This is a system-generated e-mail, please don't reply to this message. The jobs sent in this mail have been posted by the clients of <a href='https://mycareerbugs.com' style='color: #f16b22;' target='_blank'>mycareerbugs.com</a>.  Users are advised to research bonafides of advertisers independently. We recommend that you visit our <a href='http://mycareerbugs.dmo/content/terms' style='color: #f16b22;' target='_blank'>Terms & Conditions</a> and the Security Advice for more comprehensive information.

        

                     <br/><br/>

                    If you dont want to recieve this mailer , <a href='https://mycareerbugs.com/site/unsubscribe?email=$to' style='color: #f16b22;' target='_blank'>Unsubscribe</a>

                    </p></td>

                    </tr>

                     </tbody>

                    </table>

                    </body>

                    </html>";

                $mail = new ContactForm();

                $mail->sendEmail($to, $from, $html, $subject);

                // mail
            }
        }
    }

    public function actionUnsubscribe($email)

    {
        $user = JobAlert::find()->where([
            'EmailId' => $email
        ])->one();

        $user->delete();

        Yii::$app->session->setFlash('success', "Unsubscribed successfully");

        return $this->redirect([
            'index'
        ]);
    }

    public function actionDownload()

    {
        $filesrc = Yii::$app->request->get()['filesrc'];
    }

    public function actionLocationinfo()
    {
        $result = [];
        $this->layout = 'blanklayout';
        $type = Yii::$app->request->get()['type'];
        if ($type == 'getStates') {
            $id = Yii::$app->request->get()['countryId'];
            $rest = States::find()->where([
                'CountryId' => $id
            ])
                ->asArray()
                ->all();
            if ($rest) {
                $result['status'] = 'success';
                $result['tp'] = 1;
                $result['msg'] = 'States fetched successfully';
                foreach ($rest as $ck => $cval) {
                    $result['result'][$cval['StateId']] = $cval['StateName'];
                }
            }
        } elseif ($type == 'getCities') {
            $id = Yii::$app->request->get()['stateId'];
            $rest = Cities::find()->where([
                'StateId' => $id
            ])
                ->asArray()
                ->all();
            if ($rest) {
                $result['status'] = 'success';
                $result['tp'] = 1;

                $result['msg'] = 'Cities fetched successfully';

                foreach ($rest as $ck => $cval) {

                    $result['result'][$cval['CityId']] = $cval['CityName'];
                }
            }
        }

        return $this->asJson($result);
        // echo json_encode($result);
    }

    public function actionTime()

    {
        echo date('Y-m-d h:i:s A');
    }

    public function actionSkills()

    {
        $PositionId = Yii::$app->request->get()['roleid'];
        $type = '';
        if (isset(Yii::$app->request->get()['type'])) {
            $type = Yii::$app->request->get()['type'];
        }

        // skill
        $allskill = [];
        if ($PositionId != '') {
            $allskill = Skill::find()->where([
                'IsDelete' => 0,
                'PositionId' => $PositionId
            ])->all();
        }

        if ((isset($type) && $type == 'candidate')) {

            $onclick = 'candidatefsearch()';

            $class = 'cskillby';
        } elseif (isset($type) && $type == 'ecandidate') {

            $onclick = 'candidatesearch()';

            $class = 'cskillby';
        } elseif (isset($type) && $type == 'tskillby') {

            $onclick = 'getteamsearch()';

            $class = 'tskillby';
        } else {

            $onclick = 'getsearch()';

            $class = 'skillby';
        }

        foreach ($allskill as $sk => $svalue) {

            ?>

<span>

	<div class="checkbox">

		<label> <input type="checkbox" class="<?php echo $class; ?>"
			value="<?=$svalue->SkillId;?>"> <span class="cr"><i
				class="cr-icon glyphicon glyphicon-ok"></i></span> <a><?=$svalue->Skill;?> </a>

		</label>

	</div>

</span>

<?php
        }

        die();
    }

    public function actionView($id = NULL)
    {
        die();
    }

    public function actionTestTo()
    {
        die('Working');
    }

    public function actionGetcourselist()
    {
        $id = \Yii::$app->request->get('id');
        $data = [];
        $data['status'] = 'NOK';
        $NaukariQualDatas = NaukariQualData::find()->where([
            "parent_id" => $id
        ])->all();
        $html = '';
        if (! empty($NaukariQualDatas)) {
            $data['status'] = 'OK';
			$boardData = array(4,5,6);
			if(in_array($id, $boardData)){
				$html .= '<option value="">' . 'Select Board' . '</option>';
			}else{
				$html .= '<option value="">' . 'Select Course' . '</option>';
			}            
            foreach ($NaukariQualDatas as $qualdata) {
                $html .= '<option value="' . $qualdata->id . '">' . $qualdata->name . '</option>';
            }
        } else {
            $html .= '<option value="">No Result Found</option>';
        }
        $data['result'] = $html;
        return $this->asJson($data);
    }

    public function actionGetcourse()
    {
        $return_ary = array();
        $NaukariQualData = NaukariQualData::find()->where("parent_id=" . $_POST['id'])->all();
        foreach ($NaukariQualData as $key => $value) {
            $return_ary[$value->id] = $value->name;
        }
        echo json_encode($return_ary);
        exit();
    }

    public function actionContacthr($id = NULL)
    {
        $HrContact = new HrContact();
        $count = $HrContact->find()
            ->where([
            'job_id' => Yii::$app->request->get()['job_id'],
            'candidate_id' => Yii::$app->request->get()['can_id']
        ])
            ->count();
        if ($count == 0) {
            $HrContact->job_id = Yii::$app->request->get()['job_id'];
            $HrContact->candidate_id = Yii::$app->request->get()['can_id'];
            $HrContact->created_date = date('Y-m-d H:i:s');
            $HrContact->save();
            $mail = new ContactForm();
            $to = Yii::$app->request->get()['tomail'];
            $from = Yii::$app->params['adminEmail'];
            $subject = 'Job Contact by candidate';
            $html = "<html>
               <head>
               <title>Contact from candidate</title>
               </head>
               <body>
               <table style='width:500px;height:auto;margin:auto;font-family:arial;color:#4d4c4c;background:#efefef;text-align:center'>
                <tbody><tr>
                                       <td><img src='http://mycareerbugs.com/images/logo.png' title='Career Bugs' alt='Career Bugs' style='margin-top:10px;width:200px;'></td>
                                   </tr>
                                   <tr>
                                       <td style='height:30px'></td>
                                   </tr>
                           <tr>
                                       <td style='font-size:18px'><h2 style='width:85%;font-weight:normal;background:#ffffff;padding:5%;margin:auto'>Welcome to <a href='http://mycareerbugs.com' target='_blank'> Mycareer Bugs </a>
               <br><br>

               <span style='font-size:16px;line-height:1.5'>

                 <h3> Dear  Employer, </h3>

                Candidate Contact your job profile. please finds below details<br>

                Name : " . Yii::$app->request->get()['username'] . "<br>

                Email : " . Yii::$app->request->get()['emp_email'] . "  

               <br/>

               </span>

               </h2>

               Thanks,<br>

               My Career Bugs

               </td>

               </tr>

               </tbody>

               </table>

               </body>

               </html>";

            $mail->sendEmail($to, $from, $html, $subject);
        }

        exit();
    }

    public function actionJobsharemail()
    {
        $wallPostId = $_POST['wallpostid'];

        $mail = $_POST['email'];
        $sub = $_POST['Subject'];
        $text = $_POST['text'];
        // $mail_form = new ContactForm();
        $to = $mail;
        $from = Yii::$app->params['adminEmail'];
        // $mail_form->sendEmail($to, $from, $text, $sub);
        $companyWallPost = CompanyWallPost::findOne($wallPostId);
        $view = '@app/../common/mail/jobpostshare.php';
        $companyWallPost->createEmail($view, $to, $from, $sub, $companyWallPost);

        die('1');
    }

    public function actionJobpostsharemail()
    {
        $wallPostId = $_POST['jobid'];

        $mail = $_POST['email'];
        $sub = $_POST['Subject'];
        $text = $_POST['text'];
        // $mail_form = new ContactForm();
        $to = $mail;
        $from = Yii::$app->params['adminEmail'];
        // $mail_form->sendEmail($to, $from, $text, $sub);
        $companyWallPost = PostJob::findOne($wallPostId);
        $view = '@app/../common/mail/jobpost.php';
        $companyWallPost->createEmail($view, $to, $from, $sub, $companyWallPost);

        die('1');
    }

    public function actionSharejob()
    {
        die('ppp');
    }

    public function actionJobplanrequst()
    {
        if (Yii::$app->session['Employerid']) {
            $model = new AppliedJobRequest();
            $cnt = $model->find()
                ->where([
                'user_id' => Yii::$app->session['Employerid']
            ])
                ->count();
            if ($cnt == 0) {
                $model->user_id = Yii::$app->session['Employerid'];
                $model->request_on = date('Y-m-d');
                if ($model->save()) {
                    echo '0';
                } else {
                    echo '1';
                }
            }
        }

        exit();
    }

    public function actionMyactivity()
    { // contactus block
        $cn = new FooterContactus();

        $footercontact = $cn->find()->one();

        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block

        $jobcategory = new JobCategory();

        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();

        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block

        $cp = new FooterCopyright();

        $allcp = $cp->find()->one();

        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block

        $dblock = new FooterDevelopedblock();

        $developerblock = $dblock->find()->one();

        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block

        $socialicon = new SocialIcon();

        $sicon = $socialicon->find()->one();

        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block

        $th = new FooterThirdBlock();

        $thirdblock = $th->find()->one();

        Yii::$app->view->params['footerthirdblock'] = $thirdblock;
        $connection = Yii::$app->getDb();

        if (isset(Yii::$app->session['Employerid'])) {
            $id = Yii::$app->session['Employerid'];
            $command = $connection->createCommand("select comment.* from AllUser as user left join CompanyWallPost as comment on user.UserId=comment.EmployerId where user.UserId=" . $id . " AND comment.EmployerId IS NOT NULL Order By comment.OnDate DESC");
            $cnt = $command->queryAll();
            $acitity = array();
            if (! empty($cnt)) {
                foreach ($cnt as $key => $value) {
                    $acitity[$value['OnDate']] = $value;
                }
            }
        } else if (isset(Yii::$app->session['Employeeid'])) {

            $id = Yii::$app->session['Employeeid'];
            $command = $connection->createCommand("select comment.* ,p.* from AllUser as user left join CommentToPost as comment on user.UserId=comment.EmpId left join CompanyWallPost as p on comment.PostId=p.WallPostId  where user.UserId=" . $id . " AND comment.EmpId IS NOT NULL Order By comment.OnDate DESC");
            $cnt = $command->queryAll();
            $command = $connection->createCommand("select lk.*,p.* from AllUser as user left join LikeToPost as lk on user.UserId=lk.EmpId left join CompanyWallPost as p on lk.PostId=p.WallPostId where user.UserId=" . $id . " AND lk.EmpId IS NOT NULL Order By lk.OnDate DESC");
            $lk = $command->queryAll();
            // echo "<pre>";print_r($cnt);
            $comments = array();
            $likes = array();
            if (! empty($cnt)) {
                foreach ($cnt as $key => $value) {
                    $comments[$value['OnDate']] = $value;
                }
            }
            if (! empty($lk)) {
                foreach ($lk as $key1 => $value1) {
                    $likes[strtotime($value1['OnDate'])] = $value1;
                }
            }
            $acitity = array_merge($comments, $likes);
        } else {
            return $this->redirect([
                'index'
            ]);
        }
        rsort($acitity);
        $this->layout = 'walllayout';
        // echo "<pre>";print_r($acitity);die();
        $dataProvider = new ActiveDataProvider([
            'query' => Feed::find()->where([
                'created_by_id' => $id
            ])->orderBy([
                'created_on' => SORT_DESC
            ])
        ]);
        return $this->render('myactivity', [
            'acitity' => $acitity,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionConsultancies()
    {
        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $Consultancies = Alluser::find()->where([
            'EntryType' => 'Consultancy'
        ]);
        if (isset($_GET['by']) && ! empty($_GET['by'])) {
            $Consultancies->andWhere([
                'like',
                'Name',
                $_GET['by'] . '%',
                false
            ]);
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $Consultancies
        ]);
        // echo "<pre>";print_r($Consultancies);die();
        $peoplesayblock = new PeoplesayBlock();
        $pb = $peoplesayblock->find()->one();

        // all feedback
        $fdbk = new Feedback();
        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();
        return $this->render('consultancieslist', [
            'peoplesayblock' => $pb,
            'Consultancies' => $Consultancies,
            'allfeedback' => $allfeedback,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionAllhr()
    {
        // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = JobCategory::getHotJobCategory();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
				->GroupBy([
					'JobId'
			])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        $Consultancies = Alluser::find()->where([
            'EntryType' => 'HR'
        ]);
        if (isset($_GET['by']) && ! empty($_GET['by'])) {
            $Consultancies->andWhere([
                'like',
                'Name',
                $_GET['by'] . '%',
                false
            ]);
            // $query = $Consultancies->createCommand()->rawSql;
            // echo "<pre>";
            // print_r($query);
            // die();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $Consultancies
        ]);
        // echo "<pre>";print_r($Consultancies);die();
        $peoplesayblock = new PeoplesayBlock();
        $pb = $peoplesayblock->find()->one();

        // all feedback
        $fdbk = new Feedback();
        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();
        return $this->render('allhrlist', [
            'peoplesayblock' => $pb,
            'Consultancies' => $Consultancies,
            'allfeedback' => $allfeedback,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionError()
    {
        $message = 'Something went Wrong';
        return $this->render('error', compact('message'));
    }

    public function actionFollow()
    {
        $data = [];
        $data['status'] = 'NOK';
        $userid = \Yii::$app->request->post('touserid');
        if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }
        $likes = Likes::find()->where([
            'LikeFrom' => $empid,
            'LikeTo' => $userid
        ])->one();
        if (empty($likes)) {
            $likes = new Likes();
            $likes->LikeFrom = $empid;
            $likes->LikeTo = $userid;
            $likes->IsLike = 1;
            if ($likes->save()) {
                $data['status'] = 'OK';
                $data['html'] = '<i class="fa fa-user-plus"> </i> UnFollow';
				$data['like'] = 1;				
            }
        } elseif ($likes->IsLike == 0) {
            $likes->LikeFrom = $empid;
            $likes->LikeTo = $userid;
            $likes->IsLike = 1;
            if ($likes->save()) {
                $data['status'] = 'OK';
                $data['html'] = '<i class="fa fa-user-plus"> </i> UnFollow';
				$data['like'] = 1;
            }
        } elseif ($likes->IsLike == 1) {
            $likes->LikeFrom = $empid;
            $likes->LikeTo = $userid;
            $likes->IsLike = 0;
            if ($likes->save()) {
                $data['status'] = 'OK';
                $data['html'] = '<i class="fa fa-user-plus"> </i> Follow';
				$data['like'] = 0;
            }
        }

        return $this->asJson($data);
    }

    public function actionGetpreferredlocation($page, $q = null, $id = null)
    {
        $limit = 5;
        $offset = ($page - 1) * $limit;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [
            'results' => [
                'id' => '',
                'text' => '',
                'qtyLeft' => '',
                'serialNumbers' => ''
            ]
        ];
        if (! is_null($q)) {
            $query = new \yii\db\Query();
            $query->select([
                'CityName as id',
                'CityName as CityName'
            ])
                ->from('Cities')
                ->where([
                'like',
                'CityName',
                $q.'%',
				false
            ]) ->groupBy([
                'CityName'
            ])
                ->offset($offset)
                ->limit($limit);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            $out['pagination'] = [
                'more' => ! empty($data) ? true : false
            ];
        } elseif ($id > 0) {
            $out['results'] = [
                'id' => $id,
                'text' => Cities::find()->where([
                    'CityId' => $id
                ])->one()->CityName
            ];
        }else{
			$query = new \yii\db\Query();
            $query->select([
                'CityName as id',
                'CityName as CityName'
            ])
                ->from('Cities')
                ->groupBy([
                'CityName'
            ])
			->orderBy(new Expression('rand()'))
                ->offset($offset)
                ->limit($limit);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            $out['pagination'] = [
                'more' => ! empty($data) ? true : false
            ];
		}
        return $out;
    }

	 public function actionGetwhiteskills($page, $q = null, $id = null)
    {
        $limit = 5;
        $offset = ($page - 1) * $limit;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [
            'results' => [
                'id' => '',
                'text' => '',
                'qtyLeft' => '',
                'serialNumbers' => ''
            ]
        ];
        if (! is_null($q)) {
            $query = new \yii\db\Query();
            $query->select([
                'id as id',
                'name as name'
            ])
                ->from('white_skills')
                ->where([
                'like',
                'name',
                $q.'%',
				false
            ])
                ->offset($offset)
                ->limit($limit);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            $out['pagination'] = [
                'more' => ! empty($data) ? true : false
            ];
        }else{
			$query = new \yii\db\Query();
            $query->select([
                'id as id',
                'name as name'
            ])
                ->from('white_skills')
                ->offset($offset)
				->orderBy([
					'name' => 'RAND()'
				])
                ->limit($limit);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
            $out['pagination'] = [
                'more' => ! empty($data) ? true : false
            ];
		} 
        return $out;
    }
	
	public function actionGetwhiteroles(){
		$categoryId = isset($_GET['category_id'])?$_GET['category_id']:"";
		if(!empty($categoryId)){
			$data = ArrayHelper::map(WhiteRole::find()->where([
                'status' => 1,
				'category_id' => $categoryId
            ])
                ->orderBy([
                'name' => 'SORT_ASC'
            ])
                ->all(), 'id', 'name');
			echo json_encode($data);	
		}	
		exit();	
	}
	
	
    /* 12/08/2019 */
    public function actionGetqualification()
    {
        $return_ary = array();
        $NaukariQualData = NaukariQualData::find()->where("parent_id=0")->all();
        foreach ($NaukariQualData as $key => $value) {
            $return_ary[$value->id] = $value->name;
        }
        echo json_encode($return_ary);
        exit();
    }
    public function actionJobalert()
    {
		// footer section

	// first block
	$about = new FooterAboutus();

	$footerabout = $about->find()->one();

	Yii::$app->view->params['footerabout'] = $footerabout;

	// contactus block

	$cn = new FooterContactus();

	$footercontact = $cn->find()->one();

	Yii::$app->view->params['footercontact'] = $footercontact;

	// second block

	$jobcategory = new JobCategory();

	$allhotcategory = $jobcategory->find()
		->select([
		'CategoryName',
		'JobCategoryId'
	])
		->where([
		'IsDelete' => 0
	])
		->orderBy([
		'OnDate' => SORT_DESC
	])
		->limit(7)
		->all();

	Yii::$app->view->params['hotcategory'] = $allhotcategory;

	// copyright block

	$cp = new FooterCopyright();

	$allcp = $cp->find()->one();

	Yii::$app->view->params['footercopyright'] = $allcp;

	// developer block

	$dblock = new FooterDevelopedblock();

	$developerblock = $dblock->find()->one();

	Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

	// socialicon block

	$socialicon = new SocialIcon();

	$sicon = $socialicon->find()->one();

	Yii::$app->view->params['footersocialicon'] = $sicon;

	// third block

	$th = new FooterThirdBlock();

	$thirdblock = $th->find()->one();

	Yii::$app->view->params['footerthirdblock'] = $thirdblock;

	// notification for employee

	$allnotification = array();

	if (isset(Yii::$app->session['Employeeid'])) {

		$notification = new Notification();

		$allnotification = $notification->find()
			->where([
			'UserId' => Yii::$app->session['Employeeid'],
			'IsView' => 0
		])
			->orderBy([
			'OnDate' => SORT_DESC
		])
			->GroupBy([
				'JobId'
		])
			->all();
	}

	Yii::$app->view->params['employeenotification'] = $allnotification;

	// notification for employer

	$allntemp = array();

	if (isset(Yii::$app->session['Employerid'])) {

		$empnot = new EmpNotification();

		$allntemp = $empnot->find()
			->where([
			'EmpId' => Yii::$app->session['Employerid'],
			'IsView' => 0
		])
			->orderBy([
			'OnDate' => SORT_DESC
		])
			->all();
	}

	Yii::$app->view->params['employernotification'] = $allntemp;
	$post = \Yii::$app->request->post();
	/*Save White Collar Job Category, Skills, Role*/
	$whiteCategories = WhiteCategory::find()->all();
	$data = array();
	$jobalert = new JobAlert();
	// notification end
	
	$education = new Education();
	
	return $this->render('jobalert', compact('data', 'jobalert', 'education', 'whiteCategories'));
	//return $this->render('jobalert', compact('model', 'experience', 'education', 'whiteCategories'));
        
    }
	public function actionGetmessagecount(){
		$message = new Messages();
		$empid = "";
		if (isset(Yii::$app->session['Employeeid'])) {
            $empid = Yii::$app->session['Employeeid'];
        } elseif (isset(Yii::$app->session['Employerid'])) {
            $empid = Yii::$app->session['Employerid'];
        } elseif (Yii::$app->session['Campusid']) {
            $empid = Yii::$app->session['Campusid'];
        } elseif (Yii::$app->session['Teamid']) {
            $empid = Yii::$app->session['Teamid'];
        }
		$msg = $message->getMessagecount($empid);
		echo count($msg);die;
	}
	
	public function actionJobbycompany()
    { // contactus block
        $cn = new FooterContactus();
        $footercontact = $cn->find()->one();
        Yii::$app->view->params['footercontact'] = $footercontact;

        // second block
        $jobcategory = new JobCategory();
        $allhotcategory = $jobcategory->find()
            ->select([
            'CategoryName',
            'JobCategoryId'
        ])
            ->where([
            'IsDelete' => 0
        ])
            ->orderBy([
            'OnDate' => SORT_DESC
        ])
            ->limit(7)
            ->all();
        Yii::$app->view->params['hotcategory'] = $allhotcategory;

        // copyright block
        $cp = new FooterCopyright();
        $allcp = $cp->find()->one();
        Yii::$app->view->params['footercopyright'] = $allcp;

        // developer block
        $dblock = new FooterDevelopedblock();
        $developerblock = $dblock->find()->one();
        Yii::$app->view->params['footerdeveloperblock'] = $developerblock;

        // socialicon block
        $socialicon = new SocialIcon();
        $sicon = $socialicon->find()->one();
        Yii::$app->view->params['footersocialicon'] = $sicon;

        // third block
        $th = new FooterThirdBlock();
        $thirdblock = $th->find()->one();
        Yii::$app->view->params['footerthirdblock'] = $thirdblock;

        // footer section
        // notification for employee
        $allnotification = array();
        if (isset(Yii::$app->session['Employeeid'])) {
            $notification = new Notification();
            $allnotification = $notification->find()
                ->where([
                'UserId' => Yii::$app->session['Employeeid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employeenotification'] = $allnotification;

        // notification for employer
        $allntemp = array();
        if (isset(Yii::$app->session['Employerid'])) {
            $empnot = new EmpNotification();
            $allntemp = $empnot->find()
                ->where([
                'EmpId' => Yii::$app->session['Employerid'],
                'IsView' => 0
            ])
                ->orderBy([
                'OnDate' => SORT_DESC
            ])
                ->all();
        }
        Yii::$app->view->params['employernotification'] = $allntemp;
        // notification end
        $postfor = array(
            'Candidate',
            'Both'
        );
        if (isset(Yii::$app->request->get()['by'])) {
            $by = Yii::$app->request->get()['by'];
            $jobbycompany = PostJob::find()->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
				'AllUser.EntryType' => 'Company',
                'PostFor' => $postfor
            ])
                ->andWhere([
                'LIKE',
                'CompanyName',
                $by . '%',
                false
            ])
			->joinWith([
				'employer'
			])
                ->orderBy([
                'CompanyName' => SORT_ASC
            ])
                ->groupBy([
                'CompanyName'
            ])
                ->all();
        } else {
            $jobbycompany = PostJob::find()->where([
                'PostJob.IsDelete' => 0,
                'Jobstatus' => 0,
                'PostFor' => $postfor,
				'AllUser.EntryType' => 'Company',
            ])
                ->orderBy([
                'CompanyName' => SORT_ASC
            ])->joinWith([
				'employer'
			])
                ->groupBy([
                'CompanyName'
            ])
                ->all();
        }

        $peoplesayblock = new PeoplesayBlock();
        $pb = $peoplesayblock->find()->one();

        // all feedback
        $fdbk = new Feedback();
        $allfeedback = $fdbk->find()
            ->where([
            'IsApprove' => 1
        ])
            ->orderBy([
            'OnDate' => 'SORT_DESC'
        ])
            ->all();
        return $this->render('jobbycompany', [
            'peoplesayblock' => $pb,
            'companylist' => $jobbycompany,
            'allfeedback' => $allfeedback
        ]);
    }

	/*Delete User Resume*/
	public function actionDeleteuserresume(){
		if (isset(Yii::$app->session['Employeeid'])) {
			$userId = Yii::$app->session['Employeeid'];
			if(!empty($userId)){
				$userData = AllUser::find()->where([
								'UserId' => $userId
							])->one();
				if(!empty($userData)){
					$userData->CVId = 0;
					$userData->update(false);
					echo true;
				}			
			}
		}
		exit();
	}
	
	/*Like & Dislike Blog*/
	public function actionBloglike(){
		$postData = Yii::$app->request->post();
		$flag = isset($postData['flag'])?$postData['flag']:"";
		$blogId = isset($postData['blog'])?$postData['blog']:"";
		if(!empty($blogId)){
			$ipAddress = "";
			$checkActivity = BlogLikeActivity::find()->where([
								'IpAddress' => $ipAddress,
								'Action' => $flag,
								'BlogId' => $blogId
							])->one();
			if(empty($checkActivity)){
				$blogActivity = new BlogLikeActivity();
				$blogActivity->IpAddress = $ipAddress;
				$blogActivity->BlogId = $blogId;
				$blogActivity->Action = $flag;
				$blogActivity->save(false);	
				/*Update on Blog Table*/	
				$blogData = Blog::find()->where([
								'BlogId' => $blogId
							])->one();
				if(!empty($blogData)){
					if($flag == 1){
						$blogData->BlogLike = $blogData->BlogLike + 1;
					}elseif($flag == 2){
						$blogData->BlogDislike = $blogData->BlogDislike + 1;
					}
					$blogData->update(false);
				}
			}	
			$data['flag'] = $flag;
			$blogData = Blog::find()->where([
								'BlogId' => $blogId
							])->one();
			if($flag == 1){				
				$data['count'] = $blogData->BlogLike;
			}else if($flag == 2){
				$data['count'] = $blogData->BlogDislike;
			}
			echo json_encode($data);	
		}
		exit();
	}
	
}

