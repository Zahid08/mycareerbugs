 <?php
 use yii\widgets\ActiveForm;
 use yii\helpers\Html;
 use yii\captcha\Captcha;
 ?>
 
  <?php $form = ActiveForm::begin(); ?>
					    <div class="form-group">
						   <?= $form->field($model, 'Name')->textInput(['maxlength' => true,'placeholder'=>'Name'])->label(false) ?>
						</div>
                        <div class="form-group">
						   <?= $form->field($model, 'Comment')->textarea(array('rows'=>3,'placeholder'=>'Comment'))->label(false);?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                  'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                              ])->label(false); ?>
                      </div>
						<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update <i class="icon-arrow-right14 position-right"></i>', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
                    <?php ActiveForm::end(); ?>