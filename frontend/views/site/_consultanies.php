<?php
use common\components\MyHelpers;
use yii\helpers\Url;
?>
 <?php
$url='/backend/web/';  
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$postvalue = $model;

if ($postvalue->LogoId != '' && $postvalue->LogoId > 0) {
    $logo = $url . $postvalue->logo->Doc;
} else {
    $logo = $imageurl . 'images/user.png';
}
?>
<div class="box box-widget consultancy_block">
	<div class="box-header with-border">
		<div class="user-block" style="min-height: 55px;">
			<img class="img-circle" src="<?=$logo?>" alt="HR Profile">
							<?php $emp_name = preg_replace('/\s+/', '', $postvalue->Name); ?>
							<span class="username"> <a
				href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".$postvalue->UserId?>"><?=$postvalue->Name;?></a>
			</span>

		</div>
		<span class="pull-left username text-muted"> Industry: Accountant </span>
		<span class="pull-right username text-muted"><?php echo MyHelpers::totalhrpostjob($postvalue->UserId) ?> posts</span>
		<div class="clear"></div>

		<p class="industry_nm">
			<a
				href="<?= Url::toRoute(['site/profilepage','userid'=>$postvalue->UserId])?>"
				data-pjax = 0
				class="btn btn-default btn-xs"><i class="fa fa-eye"></i> View
				Profile</a>
		</p>
	</div>
</div>