<?php 
$userId = "";
if (isset(Yii::$app->session['Employerid'])) {
	$userId = Yii::$app->session['Employerid'];
}?>
<?php if (strtotime($date) != strtotime(date('Y-m-d',strtotime($model->created_on))) || $date == null):?>
	<p class="new_alert_design1">Date : <?php echo date('Y-m-d',strtotime($model->created_on))?></p>
<?php endif;?>
	<div class="new_alert_design"
		style="background: #fff; margin-bottom: 0px;">
		<div class="box-header with-border">
			<div class="user-block">
				<a href="#"> 
					<img class="img-circle" src="<?php echo $model->getLikedImage($userId)?>"alt="User Image">
				</a> 
				<span class="username"><a href="<?php echo $model->getFeedLink($userId)?>"><?php echo $model->getLikedName($userId)?></a> </span> 
				<span class="description">
					<small style="font-size: 12px; color: #6c146b;"><?php echo $model->content?> - <?php echo $model->getSomeContent($userId);?></small>
					
				</span>
			</div>
		</div>
	</div>