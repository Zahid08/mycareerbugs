<?php
use common\models\Industry;
use common\models\Position;
use common\models\Experience;
use yii\helpers\Url;
?>
<div id="work-experience-source-container">
	
	<div class="rows">	    
	       
				<div class="col-md-4">  
				 	<?php echo $form->field($experience, '[0]CompanyName',['enableAjaxValidation' => false])->textInput(['maxlength'])?>
			   </div>
			   
			   	<div class="col-md-4">  
				 	<?php echo $form->field($experience, '[0]PositionName',['enableAjaxValidation' => false])->textInput(['maxlength'])?>
			   </div>
			   
			   
			   	<div class="col-md-4">  
				 	<?php echo $form->field($experience, '[0]IndustryId',['enableAjaxValidation' => false])->dropDownList(Industry::getIndustryLists(),['prompt' => ' Select an Industry   '])?>
			   </div>
			   
			   	<div class="col-md-4">
			   	    	<?php echo $form->field($experience, '[0]Experience',['enableAjaxValidation' => false])->dropDownList(Experience::getExperienceYear(),['prompt' => 'Select Experience'])?>
      
				    </div>
			   
			   
			   	<div class="col-md-4">  
			   		<?php echo $form->field($experience, '[0]Salary',['enableAjaxValidation' => false])->dropDownList(Experience::getSalaryOptions(),['prompt' => 'Select Salary'])?>

				</div>
				   
				<div class="col-md-2">
			   	     	<label for="YearFrom" id="" style="margin-right: 4% !important;" class="cols-sm-1 control-label"> Year </label>
			   	      
					  <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-calendar"
							aria-hidden="true"></i></span> <input type="text"
							class="form-control date" name="Experience[0][YearFrom]"
							id="YearFrom" readonly
							style="cursor: pointer; background: #fff; float: left; " autocomplete="off"
							placeholder="From" onkeypress="return numbersonly(event)" />
					</div></div>
			 
				    
				      <div class="col-md-2">
			   	     	<label for="YearFrom" id="" class="cols-sm-1 control-label"> &nbsp </label>
			 
				     
							<div class="input-group exp-to-date" style="">
							<span class="input-group-addon"><i class="fa fa-calendar"
						aria-hidden="true"></i></span> <input type="text"
						class="form-control date" name="Experience[0][YearTo]" id="YearTo"
						readonly style="cursor: pointer; background: #fff; float: left; "
						autocomplete="off" placeholder="To"
						onkeypress="return numbersonly(event);"
						onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />
			 
				
						</div>	
			
				</div>	
 
                        	<div class="clear"></div>	
                        	
                        	 
  
				<div class="col-md-4" style="margin-top:10px; margin-bottom:10px">
			   	     	<label for="YearFrom" id="" class="cols-sm-1 control-label"> &nbsp </label>    
			  
			 
			 	<span><input id="currentWorking" type="checkbox" value="1" name="Experience[0][CurrentWorking]">&nbsp;<span style="font-size:12px; color:##565656">I currently work here</span></span>
			 	 
			 	
		</div>
	
 	<div class="clear"></div>	
	 <div class="exp-blue-collar">
		<div class="col-md-12" style="padding-left:0px; padding-right:0px">
		  <div id="workexpdiv"> 
		  <div class="col-md-4">
			<?php echo $form->field($experience, '[0]PositionId',['enableAjaxValidation' => false])->dropDownList(Position::getPositionLists(),['data-id-count' => 0,'prompt' => '   Select Role   '])->label('Role')?>
		  
		  </div>
		  <div class="col-md-8">
			<div id="skill-container-0">
				<?php echo $form->field($experience, '[0]skills',['enableAjaxValidation' => false])->checkboxList([0 => 'Select Role First'],[
					'item' => function($index, $label, $name, $checked, $value) {
						$return = '<label class="modal-check">';
						$return .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" tabindex="3">';
						$return .= '<i></i>';
						$return .= '<span> ' . ucwords($label) . ' </span>';
						$return .= '</label>';
					return $return;
					}
					
				])->label('Skills')?>
			</div>
			
			</div>
		</div>
			<div class="clear"></div>	
		</div>
	</div>
	
	<div class="exp-white-collar" style="display:none">
		<div class="col-md-4">
			 <label class="control-label">Job Category</label>
			 <select class="questions-category form-control white_callar_exp_category" name="Experience[0][Category]" tabindex="0" aria-hidden="true" id="white_callar_exp0" data-id-count="0">
				 <option value="">Select Category</option>
				 <?php if(!empty($whiteCategories)){
						foreach($whiteCategories as $categoryId => $categoryName){?>
							<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
						<?php  }
						}?>
				 
			</select> 
		</div>
		<div class="col-md-4">
			<label class="control-label">Role</label>
			<select class="questions-category form-control" name="Experience[0][WhiteRole][]" id="white_callar_role_exp0" multiple="multiple"></select> 
		</div>
		<div class="col-md-4">
			<label class="control-label">Skills</label>
			<select class="questions-category form-control white_callar_skills" id="white_callar_skills_exp0" name="Experience[0][WhiteSkills][]" multiple="multiple"></select>		
								
		</div>
		
		<div class="clear"></div>	
	 	
	</div>
	
		<div class="clear"></div>	
	
	
	</div>
</div>
<script>

    //Change Date Options
    $(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true, yearRange: "-50:+0"});

function getCompanyNameField(count){
	var html = '<div class="col-md-4"><div class="form-group field-experience-companyname required">'+
		'<label class="control-label" for="experience-companyname-'+count+'">Company Name</label>'+
		'<input type="text" id="experience-'+count+'-companyname" class="form-control" name="Experience['+count+'][CompanyName]" 0="maxlength">'+
		'<p class="help-block help-block-error"></p>'+
		'</div></div>';
	return html;
}
function getPositionName(count){
	var html = '<div class="col-md-4"><div class="form-group field-experience-positionname">'+
		'<label class="control-label" for="experience-positionname-'+count+'">Position Name</label>'+
		'<input type="text" id="experience-'+count+'-positionname" class="form-control" name="Experience['+count+'][PositionName]" 0="maxlength">'+
		'<p class="help-block help-block-error"></p>'+
		'</div></div>';
	return html;
}
function getIndustry(count) {
	var options = $('#experience-0-industryid').html();
	var html = '<div class="col-md-4"><div class="form-group field-experience-industryid has-success">'+
		'<label class="control-label" for="experience-industryid-'+count+'">Industry</label>'+
		'<select id="experience-'+count+'-industryid" class="form-control" name="Experience['+count+'][IndustryId]" aria-invalid="false">'+
		options +
		'</select>'+
		'<p class="help-block help-block-error"></p>'+
		'</div></div>';
	return html;
}
function getRole(count){
	var options = $('#experience-0-positionid').html();
	var html = '<div class="exp-blue-collar"><div class="col-md-4"><div class="form-group field-experience-positionid required has-success">'+
            		'<label class="control-label" for="experience-positionid-'+count+'">Role</label>'+
            			'<select id="experience-0-positionid" class="form-control" name="Experience['+count+'][PositionId]" aria-invalid="false" data-id-count='+count+'>'+
            				options +
            			'</select>'+
            		'<p class="help-block help-block-error"></p>'+
            	'</div></div>'+
            	'<div class="col-md-8"><div id="skill-container-'+count+'"></div></div></div>';
	return html;
}

function getWhiteCollarOptions(count){
	var options = $('#white_callar_exp0').html();
	var html = '<div class="exp-white-collar" style="display:none"><div class="col-md-4"><label class="control-label">Job Category</label> <select class="questions-category form-control white_callar_exp_category" name="Experience['+count+'][Category]" id="white_callar_exp'+count+'" data-id-count="'+count+'">'+options+'</select></div><div class="col-md-4"><label class="control-label">Role</label><select class="questions-category form-control" name="Experience['+count+'][WhiteRole][]" id="white_callar_role_exp'+count+'" multiple=""></select></div><div class="col-md-4"><label class="control-label">Skills</label><select class="questions-category form-control white_callar_skills" id="white_callar_skills_exp'+count+'" name="Experience['+count+'][WhiteSkills][]" multiple=""></select></div><div class="clear"></div></div><div class="clear"></div>';
	return html;
}

function getExperience(count) {
	var options = $('#experience-0-experience').html();
	var html = '<div class="col-md-4"><div class="form-group field-experience-experience required has-success">'+
		'<label class="control-label" for="experience-experience-'+count+'">Experience</label>'+
		'<select id="experience-'+count+'-experience" class="form-control" name="Experience['+count+'][Experience]" aria-invalid="false">'+
		options +
		'</select>'+
		'<p class="help-block help-block-error"></p>'+
		'</div></div>';
	return html;
}
function getSalary (count) {
	var options = $('#experience-0-salary').html();
	var html = '<div class="col-md-4"><div class="form-group field-experience-salary required has-success">'+
		'<label class="control-label" for="experience-salary-'+count+'">Salary</label>'+
		'<select id="experience-'+count+'-salary" class="form-control" name="Experience['+count+'][Salary]" aria-invalid="false">'+
		options +
		'</select>'+
		'<p class="help-block help-block-error"></p>'+
		'</div></div>';
	return html;
}
var p=0;
function admoreexp() {
    console.log("come in addd more.");
		p++;
		
		var date='<div class="col-md-4"><div class="form-group"><label for="responsive_full" class="cols-sm-2 control-label">  Year    \
			</label><div class="cols-sm-5"><div class="input-group" id="responsive_full" style="width: 39%;float: left;">\
			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>\
			<input type="text" class="form-control date" name="Experience['+p+'][YearFrom]" readonly style="cursor: pointer;background: #fff; float: left;"\
			autocomplete="off"  placeholder="From" maxlength="4" onkeypress="return numbersonly(event)"/>\
			</div><div class="input-group" id="responsive_full1" style="width: 39%;float: left;">\
			<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>\
			<input type="text" class="form-control date" name="Experience['+p+'][YearTo]" readonly style="cursor: pointer;background: #fff; float: left;" autocomplete="off"  \
			placeholder="To"  maxlength="4" onkeypress="return numbersonly(event);" \
			onchange="if(new Date($(this).val()).getTime()<=new Date($(&#39;YearFrom&#39;).val()).getTime()){alert(&#39;Wrong Year Duration&#39;);$(this).val(&#39; &#39;);}"/>\
			</div></div></div></div><div class="clear"></div><div class="clear"></div>';
		
		var result = getCompanyNameField (p) + 
		getPositionName (p) +
		getIndustry (p) +
		getExperience (p) +
		getSalary (p) +
		date + getRole (p) + getWhiteCollarOptions(p);
		//var result = $('#workexpdiv').html();
		//var result = $('#workexpdiv').find('#roleskilllist').empty().html();
		//console.log(resultt);
		
    	$('#work-experience-source-container:last').append('<div class="clear"></div><hr/><div class="clear"></div><div id="workexpindiv'+p+'" ><span style="color:red;cursor:pointer;" onclick="delExp('+p+');">X</span><div class="clear"></div>'+result+'</div><div class="clear"></div>');
		$(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true, yearRange: "-50:+0"});
 		$('#workexpindiv'+p).find('#roleskilllist').empty();

 		var roleId = $('[id^=experience-positionid]').val();
		
		var collarType = $('.collar-type:checked').val();
		if(collarType == 'white'){
			$('.exp-white-collar').show(0);
			$('.exp-blue-collar').hide(0);
		}else{
			$('.exp-white-collar').hide(0);
			$('.exp-blue-collar').show(0);
		}
		
		$("#white_callar_role_exp"+p).select2({
			placeholder: "Select Role"
		});
		
		$(".white_callar_skills").select2({
			maximumSelectionLength: 5,
			placeholder: "Select Skills",
			ajax: {
				url: '<?=Url::toRoute(['getwhiteskills']);?>',
				dataType: 'json',
				type: "GET",
				data: function (params) {
					return {
						q: params.term,
						page: 1
					  }
				},
				processResults: function(data){
					return {
						results: $.map(data.results, function (item) {
							
							return {
								text: item.name,
								id: item.id
							}
						})
					};
				}
			 }
		});
		
		
		
 		//getSkillExperience(roleId,p);		
}
var baseUrl = "<?php echo Url::toRoute(['/'])?>";

$(document).ready( function(e){
	var roleId = $('[id^=experience-positionid]').val();
	var dataidcount = 0;
	getSkillExperience(roleId,dataidcount);
	
	$('#currentWorking').click(function(){
		if($(this).is(':checked')){
			$('.exp-to-date').hide(0);
		}else{
			$('.exp-to-date').show(0);
		}
		
	});
	
	$("#white_callar_role_exp0").select2({
  		placeholder: "Select Role"
	});
	
	$(document).on('change', '.white_callar_exp_category', function(){
		var value = $(this).val();
		var countC = $(this).attr('data-id-count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role_exp'+countC).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role_exp'+countC).append(option);
						});
					}
				}
			});
		}
		
	});
	
	$(".white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});	
	
});
$(document).on('change' , '[id^=experience-0-positionid]', function(e){
	var roleId = $(this).val();
	var dataidcount = $(this).attr('data-id-count');
	getSkillExperienceNew(roleId,dataidcount);
})
function delExp(cnt){
    $("#workexpindiv"+cnt).remove();
    p--;
}
function getSkillExperience(roleId,dataidcount){
	$.get(baseUrl+'site/getskills?id='+roleId, function(res){
		$('#skill-container-'+dataidcount).empty();
		$('#skill-container-'+dataidcount).append(res.result);
		
	});
}
function getSkillExperienceNew(roleId,dataidcount){
	$.get(baseUrl+'site/getexperienceskills?id='+roleId+'&count='+dataidcount, function(res){
		$('#skill-container-'+dataidcount).empty();
		$('#skill-container-'+dataidcount).append(res.result);
		
	});
}
// function fromDatePicker(count){
// 	var html = '<div class="col-md-6"> \
// 		<div class="form-group field-experience-yearfrom required"> \
//         	<label class="control-label" for="experience-yearfrom_'+count+'">Year</label> \
//         		<input type="text" id="experience-yearfrom_" name="Experience[YearFrom][]" placeholder="From" class="hasDatepicker"> \
//         		<p class="help-block help-block-error">Year From cannot be blank.</p> \
//         	</div>  \
//     	</div>';
// 	return html;
// }
// var count = 1;
// $(document).on('click', '#add-more-experience' , function(e){
// 	console.log(count);
// 	$('#work-experience-source-container:last').append(fromDatePicker(count));
// 	count++;
// });
// $(document).on('click','#experience-yearfrom_',function(){
//     $(this).datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true});
// });
// 	$(document).on('click', '#add-more-experience', function(e){
// 		e.preventDefault();
// 		var html = $('#work-experience-source-container').html();
// 		$(html).find('#experience-yearfrom').attr('id' , 'experience-yearfrom-1');
// 		$('#work-experience-source-container').after(html);
// 	});
</script>