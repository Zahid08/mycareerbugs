<?php
$this->title = 'Browse Job by HR - My Career Bugs';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\ListView;
use yii\widgets\Pjax;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
?>
<meta name="keywords" content="My Career Bugs, MCB, All HR list, Top HR, Top Recruiter, HR Recruiter" />
<meta name="description"  content="Hunt for a place in your dream Company. Search and Apply to Openings with Top Employers." />
	
	<style>
#consultancy_list_1 {
	clear: both;
	overflow: hidden;
}

#consultancy_list_1 .consultancy_block {
	width: 24%;
	margin-bottom: 20px;
	float: left;
	margin: 0 0.5%;
}

#consultancy_list_1 .img-circle {
	width: 80px;
	height: 80px;
	display: block;
	margin: 0 auto 10px;
	float: none;
}

#consultancy_list_1 .username {
	margin: 0 auto;
	text-align: center;
	margin-bottom: 5px;
}

#consultancy_list_1 .industry_nm {
	font-size: 12px;
	margin: 5px 0 0 0;
	text-align: center
}

#consultancy_list_1 .box-header.with-border {
	border-top: 0px solid #000 !important;
	border: 1px solid #ddd;
	margin-bottom: 10px;
}
</style>
	
<!-- start main wrapper -->
<div id="wrapper">
	<div class="headline_inner">
		<div class="row">
			<div class=" container">
				<!-- start headline section -->
				<h2>Browse Jobs by HR</h2>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- end headline section -->
	</div>
	<div class="spacer-5"></div>
	<div class="container" id="company_list_pagination">
	<?php

Pjax::begin([
    'id' => 'hrlist-pjax'
]);
?>
		<div class="col-xs-12">
			<ul class="pagination">
				<li class="active no_display"><a href="#">Top HR</a></li>
				<?php $ranges = range('A', 'Z'); foreach ($ranges as $range) {?>
				<li><a href="<?= Url::toRoute(['site/allhr','by'=>$range])?>"><?php echo $range?></a></li>
				<?php }?>
					<li><a href="<?= Url::toRoute(['site/all-hr'])?>">All</a></li>
			</ul>
		</div>

		<div class="clear"></div>
			<div id="consultancy_list_1">	
				<?php

    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => [
            'class' => 'item'
        ],
        'itemView' => '_consultanies',
        'summary' => '',
        'emptyText' => 'No Results Found'
        // 'pager' => [
        // 'class' => \kop\y2sp\ScrollPager::className(),
        // 'paginationSelector' => '.list-view .pagination',
        // 'triggerTemplate' => 'working'
        // ]
    ]);
    Pjax::end();
    ?>
    </div>
			</div>
</div>
<div class="spacer-5"></div>



<div class="testimony">
	<div class="container">
		<h1><?=$peoplesayblock->Heading;?></h1>
				<?=htmlspecialchars_decode($peoplesayblock->Content);?>
					
			</div>
	<div id="sync2" class="owl-carousel">
				<?php
    foreach ($allfeedback as $fk => $fvalue) {
        if ($fvalue->docDetail) {
            $doc = $url . $fvalue->docDetail->Doc;
        } else {
            $doc = $imageurl . '<?=$logo?>';
        }
        ?>
				<div class="testimony-image">
			<img src="<?=$doc;?>" class="img-responsive"
				alt="MCB Testimonial Profile" style="height: 150px; width: 150px;" />
		</div>
				<?php
    }
    ?>
			</div>

	<div id="sync1" class="owl-carousel">
				<?php
    foreach ($allfeedback as $fk => $fvalue1) {
        ?>
				<div class="testimony-content container">
					<?=htmlspecialchars_decode($fvalue1->Message);?>
					<p>
						<?php echo $fvalue1->Name.','.$fvalue1->Designation.','.$fvalue1->Companyname;?>
					</p>
			<div class="media-testimony">
						<?php
        if ($fvalue1->Twitterlink != '') {
            ?>
						<a href="<?=$fvalue1->Twitterlink;?>" target="blank"><i
					class="fa fa-twitter twit"></i></a>
						<?php
        }
        if ($fvalue1->LinkedinLink != '') {
            ?>
						<a href="<?=$fvalue1->LinkedinLink;?>" target="blank"><i
					class="fa fa-linkedin linkedin"></i></a>
						<?php
        }
        if ($fvalue1->Facebooklink != '') {
            ?>
						<a href="<?=$fvalue1->Facebooklink;?>" target="blank"><i
					class="fa fa-facebook fb"></i></a>
						<?php
        }
        ?>
					</div>
		</div>
				<?php
    }
    ?>
			</div>
</div>





<?php
if (! isset(Yii::$app->session['Employeeid']) && ! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Campusid']) && ! isset(Yii::$app->session['Teamid'])) {
    ?>
<div class="advertise_your_post">
	<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
		<img src="<?=$imageurl;?>images/job_seekers.png" alt="MCB Job Seekers">
		<a href="<?= Url::toRoute(['site/jobseekersregister'])?>"> Register </a>
	</div>

	<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
		<img src="<?=$imageurl;?>images/team_orange.png"
			alt="MCB Team Register"> <a href="<?= Url::toRoute(['team/index'])?>">Register
		</a>
	</div>


	<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
		<img src="<?=$imageurl;?>images/company.png"
			alt="MCB Employers Register"> <a
			href="<?= Url::toRoute(['site/employersregister'])?>">Register </a>
	</div>


	<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
		<img src="<?=$imageurl;?>images/campus_register.png"
			alt="MCB Campus Register"> <a
			href="<?= Url::toRoute(['campus/campusregister'])?>">Register </a>
	</div>
</div>
<?php
}
?>