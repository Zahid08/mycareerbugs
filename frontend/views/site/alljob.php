<?php
$this->title = 'MY Career Bugs';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
?>
<!-- start main wrapper --> 
	<div id="wrapper">
		<div class="recent-job"><!-- Start Recent Job -->
			<div class="container">
				<div class="row">
				  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
			 
			 <div class="col-lg-12 col-md-5 col-sm-5 col-xs-6">
						<h4><i class="glyphicon glyphicon-briefcase"></i> All Job</h4>
					 </div>	 
					  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6" id="landing_select">
					 
 <div class="clearfix"></div>
		
		<?php
		
		if($model)
		{
		foreach($model as $jkey=>$jvalue)
		{
		?>
		 <div class="item-list">
                <a href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>">
                <div class="col-sm-12 add-desc-box">
                  <div class="add-details">
                    <h5 class="add-title"><?=$jvalue->JobTitle;?></h5>
                    <div class="info"> 
                      <span class="category"><?=$jvalue->position->Position;?></span> -
                      <span class="item-location"><i class="fa fa-map-marker"></i> <?=$jvalue->Location;?></span> <br>
                    <span> <strong><?=$jvalue->CompanyName;?></strong> </span>
					</div>
                    <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Experience : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?php if($jvalue->Experience!='Fresher'){ echo $jvalue->Experience.' Year';}else{echo $jvalue->Experience;}?></span>  
                      </div>
					 </div> 
					  <div class="info bottom">
					     <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Keyskills : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category">
								<?php
								$jskill='';
								foreach($jvalue->jobRelatedSkills as $k=>$v)
								{
								$jskill.=$v->skill->Skill.' , ';
								}
								echo $jskill;
								?>
							</span>  
                      </div>
					 </div>
					 
					   <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Job Description : </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?=substr(htmlspecialchars_decode($jvalue->JobDescription),0,150).'...';?></span>  
                      </div>
					 </div>
					 
					  <div class="info bottom">
					    <div class="col-sm-3 col-xs-3">
					    	<span class="styl">Salary Range </span>  
					    </div>
						<div class="col-sm-9 col-xs-9 left-text">
							<span class="category"><?=$jvalue->Salary;?></span>  
                      </div>
					 </div> 
					 
					<div class="info bottom"> 
						<span class="category" style="text-align:right">    Posted By <?=$jvalue->employer->Name.' ('.date('d M Y, h:i A',strtotime($jvalue->OnDate)).')';?></span> 
                    </div> 
                  </div>
                </div>
                </a>
              </div>
		<?php
		}
		}
        else
        {
		?>
        <h5 class="add-title">No job found in this category</h5>
        <?php
        }
        ?>
							<?php
							echo LinkPager::widget([
								'pagination' => $pages,
							]);
							?>
					 
						<div class="spacer-2"></div>
					</div>
					 
                  </div>
					
					
					<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side">
							<div class="clearfix"></div>
						<div class="spacer-5"></div><div class="spacer-5"></div><div class="spacer-5"></div>
						
							<!--adban_block main -->
								<div class="adban_block">
							      <img src="<?=$imageurl;?>images/adban_block/ban.jpg"> 
							    </div>
						   <!-- adban_block  main -->
								
								
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div><!-- end Recent Job -->

</div><!-- end main wrapper -->

