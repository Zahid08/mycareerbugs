<?php
$this->title = 'Blog - My Career Bugs';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<style>
.company{margin: 0px !important;}
.post-header { 
    height: 73px;
}

.slider-post .heading {  
    height: 5px;
}
.slider-post { 
    background-size: cover !important;
}
</style>
<div style="height:10px; background:#e9eaed"></div>
   
<div id="blog" class="owl-carousel company-post">
					
						<?php
						foreach($allblog as $k=>$value)
						{
							$categorySlug = $value->blogCategory->Slug;
							$postSlug = $value->BlogSlug;
							
						?>
						<div class="company">
				           <div class="col-md-12 col-sm-12 col-xs-12"> 
								   
							   <div class="slider-post slick-slide" style="background:url(<?=$url.'/'.$value->blogImage->Doc;?>) no-repeat">
							 
								 <div class="slider-post-info">
								  <div class="category-wrapper">
								   <a class="category" href="<?= Url::base().'/blog/'.$categorySlug?>" tabindex="-1"><?=$value->blogCategory->BlogCategoryName;?></a></div>
								   <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" class="post-title" tabindex="-1"><h2 class="heading"><?=$value->BlogName;?></h2></a>
								  <span class="publish-date"><?=date('M d,Y',strtotime($value->OnDate));?></span> </div>
							   </div>
								  </div>
					     </div>
						<?php
						}
						?>
</div>



 <div class="main-content">
  <div class="container clearfix">
   <div id="blog-posts-wrapper">
  
               <?php
						foreach($allblog as $k=>$value)
						{
							$categorySlug = $value->blogCategory->Slug;
							$postSlug = $value->BlogSlug;
						?>
                    <div class="post-outer">
						    <div class="post hentry uncustomized-post-template"> 
								<div class="post-header">
										<h3 class="blogpost-title entry-title heading"> <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>"><?=$value->BlogName;?></a> </h3>
										<span class="post-timestamp">  <a class="timestamp-link" href="#" rel="bookmark">
										    <a class="published"><?=date('M d,Y',strtotime($value->OnDate));?></a> </a>
										</span>
								</div>
						    <div class="blogp">
						      <a class="post-image" href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>"> 
						      <img src="<?=$url.'/'.$value->blogImage->Doc;?>" style="width: 360px;height: 190px;"> </a>
						   </div>
						       <?=substr(htmlspecialchars_decode($value->BlogDesc),0,150);?>...
						       <p class="more-link"><a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>">Continue Reading</a></p>
						       </strong>
					    </div>
						 <div class="post-bottom clearfix">
						  <div class="post-share">
						          <?php
								  if($value->IsFacebook==1)
								  {
						           ?>
								   <!--<a class="post-share_link facebook" href="<?=$value->FacebookLink;?>"> <i class="fa fa-facebook"></i></a>-->
								   <a class="post-share_link facebook" 
								   href="https://www.facebook.com/sharer/sharer.php?u=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" target="_blank">
								       <i class="fa fa-facebook"></i></a>
								   <?php
								  }
								  if($value->IsTwitter==1)
								  {
						           ?>
								   <!--<a class="post-share_link twitter" href="<?=$value->TwitterLink;?>"> <i class="fa fa-twitter"></i></a>-->
								   <a class="post-share_link twitter" 
								   href="http://twitter.com/share?url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>&text=<?=$value->BlogName;?>" target="_blank">
								   <i class="fa fa-twitter"></i></a>
								   <?php
								  }
								  if($value->IsLinkedIn==1)
								  {
						           ?>
								   <!--<a class="post-share_link linkedin" href="<?=$value->LinkedinLink;?>"> <i class="fa fa-linkedin"></i></a>-->
								   <a class="post-share_link linkedin" 
href="https://www.linkedin.com/shareArticle?mini=true&url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>&title=<?=$value->BlogName;?>" target="_blank">
							       <i class="fa fa-linkedin"></i></a>
								   <?php
								  }
								  /*if($value->IsGoogleplus==1)
								  {
						           ?>
								   <!--<a class="post-share_link googleplus" href="<?=$value->GooglePlusLink;?>"> <i class="fa fa-google-plus"></i></a>-->
								   <a class="post-share_link googleplus" 
								   href="https://plusone.google.com/_/+1/confirm?hl=en&url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" target="_blank">
								       <i class="fa fa-google-plus"></i></a>
								   <?php
								  }*/
								  ?>
									<?php 
										$wapblogdesc =  $value->BlogName.", for more details visit: " . Url::base().'/blog/'.$categorySlug.'/'.$postSlug;
									?>
									<a class="post-share_link whatsapp" href="https://api.whatsapp.com/send?text=<?=strip_tags($wapblogdesc)?>" target="_blank"><i class="fa fa-whatsapp"></i></a>
									
								   <a class="post-share_link pinterest" href="https://www.pinterest.com/pin/create/link/?url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>&media=<?=Url::base().$url.'/'.$value->blogImage->Doc;?>" target="_blank"><i class="fa fa-pinterest"></i></a>
								
						  </div>
					     </div> 
 	                </div>
					<?php
						}
						?>
  
  
			 
 				      
	<div class="clear"></div>
						 
	 
  </div> 
 
 







<div class="sidebar-wrapper" id="wp-sidebar" >
<div class="section" id="sidebar" name="Sidebar">
	 
	 <div class="widget FollowByEmail">
     <h2 class="title"> Search the blog</h2>
       <div class="widget-content">
                    <div class="follow-by-email-inner">
							
							  <table width="100%">
							    <tbody><tr>
							    <td>
							      <input class="follow-by-email-address" id="blogname" placeholder="Search blog" type="text">
							  </td>
							   <td width="64px">
							    <input class="follow-by-email-submit" onclick="searchblog();" type="button" value="Submit">
							     </td>
							     </tr>
							     </tbody></table>
							    
							
                       </div>
           </div>  
     </div> 




<div class="widget Label" data-version="1" id="Label1">
<h2>categories</h2>
     <div class="widget-content list-label-widget-content">
          <ul>
             <?php
			 foreach($allblogcategory as $ck=>$cvalue)
			 {
				 $categorySlug = $cvalue->blogCategory->Slug;
						?>
				<li>
				<a href="<?= Url::base().'/blog/'.$categorySlug;?>">  <?=$cvalue->BlogCategoryName;?></a>
				<span dir="ltr"><?=$cvalue->cnt;?></span>
				</li>
             <?php
			 }
			 ?>
    </ul>
      <div class="clear"></div>
  
</div>
</div>



       <div class="widget">
        <h2 class="title">recent posts</h2>
					<div class="widget-content recentposts">
					<ul class="recent-posts-wrapper">
                    <?php
					foreach($recent as $rk=>$rvalue)
					{
						$categorySlug = $rvalue->blogCategory->Slug;
						$postSlug = $rvalue->BlogSlug;
                     ?>
					<li class="recent-post-item">
					   <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" class="post-image" ><img src="<?=$url.'/'.$rvalue->blogImage->Doc;?>"></a>
						<a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" class="post-title"><h2 class="heading"><?=$rvalue->BlogName;?></h2></a>
					</li>
                        <?php
					}
					?>
					 <div class="clear"></div>
					  
					</div>

         </div>
		 
		 	 
	 
	 
<div class="widget FollowByEmail"  >
     <h2 class="title">Follow by Email</h2>
       <div class="widget-content">
                    <div class="follow-by-email-inner">
							<form action="#" method="post" >
							  <table width="100%">
							    <tbody><tr>
							    <td>
							      <input class="follow-by-email-address" name="email" placeholder="Email address..." type="text">
							  </td>
							   <td width="64px">
							    <input class="follow-by-email-submit" type="submit" value="Submit">
							     </td>
							     </tr>
							     </tbody></table> 
							</form>
                       </div>
           </div>  
     </div> 
    </div>
   </div>
</div>
   
    </div>
  
  
  
<div style="height:10px; background:#e9eaed"></div>
   
   
                   <div id="blog-post-list" class="owl-carousel company-post">
                     <?php
					 foreach($allblog as $bk=>$bval)
					 {
						 $categorySlug = $bval->blogCategory->Slug;
						 $postSlug = $bval->BlogSlug;
						?>
						<div class="company">
				        <div class="post-outer">
						    <div class="post hentry uncustomized-post-template"> 
								<div class="post-header">
										<h3 class="post-title entry-title heading"> <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>"><?=$bval->BlogName;?></a> </h3>
										<span class="post-timestamp">  <a class="timestamp-link" href="#" rel="bookmark">
										    <a class="published"><?=date('M d, Y',strtotime($bval->OnDate));?></a> </a>
										</span>
								</div>
						    <div class="blogp">
						      <a class="post-image" href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>"> <img src="<?=$url.'/'.$bval->blogImage->Doc;?>"> </a>
						   </div>
						      <?=substr(htmlspecialchars_decode($bval->BlogDesc),0,100)?>...
						       <p class="more-link"><a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>">Continue Reading</a></p>
					    </div>
					  </div> 
					</div>
					 <?php
					 }
					 ?>
					
				</div>
   