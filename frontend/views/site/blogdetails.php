<?php
$this->title = !empty($blogdetail->MetaTitle)?$blogdetail->MetaTitle:$blogdetail->BlogName;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
$categorySlug = $blogdetail->blogCategory->Slug;
$postSlug = $blogdetail->BlogSlug;

$this->params['meta_title'] = !empty($blogdetail->MetaTitle)?$blogdetail->MetaTitle:$blogdetail->BlogName;
$this->params['meta_description'] = !empty($blogdetail->MetaDescription)?$blogdetail->MetaDescription:"";
$this->params['meta_keywords'] = !empty($blogdetail->MetaKeywords)?$blogdetail->MetaKeywords:"";
$this->params['og_title'] = !empty($blogdetail->MetaTitle)?$blogdetail->MetaTitle:$blogdetail->BlogName;
$this->params['og_description'] = !empty($blogdetail->MetaDescription)?$blogdetail->MetaDescription:"";
$this->params['og_image'] = Url::base().'/'.$url.$blogdetail->blogImage->Doc;
$this->params['og_url'] = Url::base().'/blog/'.$categorySlug.'/'.$postSlug;
$wapblogdesc =  $blogdetail->BlogName.", for more details visit: " . Url::base().'/blog/'.$categorySlug.'/'.$postSlug;

?>
<style>
.company{margin: 0px !important;}
</style>
<div style="height:10px; background:#e9eaed"></div>
   
   
   <div class="main-content">
      <div class="container clearfix">
         <div id="blog-posts-wrapper">
              <div class="post-outer">
                  
                        <div class="post-header">
                           <h3 class="post-title entry-title heading full-heading" >
                              <?=$blogdetail->BlogName;?>
                           </h3>
                           <span class="post-timestamp">
                           <a class="timestamp-link" href="#" ><?=date('M d,Y',strtotime($blogdetail->OnDate));?></a>
                           </span>
                        </div>
						
                        <div class="post-body entry-content">
                           <div class="separator" >
                              <img src="<?=$url.'/'.$blogdetail->blogImage->Doc;?>">
                           </div>
                           <br>
						   <?=htmlspecialchars_decode($blogdetail->BlogDesc);?>
                           <div style="clear: both;"></div>
                        </div>
						
                        <div class="post-footer">
							<br/>
                           <div class="post-footer-line post-footer-line-2">
                              <span class="post-labels">
							  <a href="javascript:void(0)" class="like-blog" flag = 1 blog="<?=$blogdetail->BlogId;?>"><i class="fa fa-thumbs-up"></i> Like(<span class="blog-like-count"><?=$blogdetail->BlogLike;?></span>)</a> &nbsp;&nbsp;&nbsp;
							  <a href="javascript:void(0)" class="like-blog" flag = 2 blog="<?=$blogdetail->BlogId;?>"><i class="fa fa-thumbs-down"></i> Dislike(<span class="blog-dislike-count"><?=$blogdetail->BlogDislike;?></span>)</a>
							  <!-- <a class="post-label-anchor" href="#" rel="tag">nature</a>,
                              <a class="post-label-anchor" href="#" rel="tag">people</a> -->
                              </span>
                           </div>
                           <div class="post-footer-line post-footer-line-3">
                              <span class="post-location">
                              </span>
                           </div>
                        </div>
						
						
                        <div class="post-bottom clearfix">
                           <span class="post-author vcard">
                              By:  <span class="fn" > 
                                 <a class="g-profile" href="" >
                                 <span itemprop="name">  Rohit Jaiswal	</span>
                                 </a>
                              </span>
                           </span>
                           <div class="post-share">
                              <!--<a class="post-share_link facebook" href="#"> <i class="fa fa-facebook"></i></a>-->
                              <!--<a class="post-share_link twitter"  href="#"><i class="fa fa-twitter"></i></a>-->
                              <!--<a class="post-share_link pinterest" href="#"> <i class="fa fa-pinterest"></i></a>-->
                              <!--<a class="post-share_link googleplus"  href="#"><i class="fa fa-google-plus"></i></a>-->
                              <!--<a class="post-share_link linkedin"     href="#"> <i class="fa fa-linkedin"></i></a>-->
                              
                            <a class="post-share_link facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" target="_blank"><i class="fa fa-facebook"></i></a>                              
                            <a class="post-share_link twitter" href="http://twitter.com/share?url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>&text=<?=$blogdetail->BlogName;?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a class="post-share_link pinterest" href="https://www.pinterest.com/pin/create/link/?url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <a class="post-share_link linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>&title=<?=$value->BlogName;?>" target="_blank"><i class="fa fa-linkedin"></i></a>
							<a class="post-share_link whatsapp" href="https://api.whatsapp.com/send?text=<?=strip_tags($wapblogdesc)?>" target="_blank"><i class="fa fa-whatsapp"></i></a>
                            <?php /*<a class="post-share_link googleplus" href="https://plusone.google.com/_/+1/confirm?hl=en&url=https://www.mycareerbugs.com/blogdetails?BlogId=<?=$blogdetail->BlogId;?>" target="_blank"><i class="fa fa-google-plus"></i></a> */?>
                            								   
                           </div>
                        </div> 
            
				  <?php
				  if($blogc)
				  {
					 foreach($blogc as $ckey=>$cval)
					 {
				  ?>
				  <div class="author-info">
                     <div class="clearfix">
                        <div class="author-avatar-wrapper">
						  <div style="width: 70px;height: 70px;float: left;border-radius:40px;background: #3d4040;text-align: center;color: #fff;font-size: 38px;font-weight: bold;padding-top: 13px;"><?=substr($cval->Name,0,1);?></div>
                        </div>
						 <div class="author-comment">
                        <h5 class="author-name"><?=$cval->Name;?></h5> <small><?=date('F d, Y',strtotime($cval->UpdatedDate)).' at '.date('h:i A',strtotime($cval->UpdatedDate));?></small>
                        <p class="author-bio"><?=$cval->Comment;?></p>
                      
					      </div>
					</div>
                  </div>
				  <?php
					 }
				  }
				  ?>
				  
				    <div class="post-bottom clearfix">
			 
				  <div class="well">
				      <h4>Leave a Comment:</h4>
				      <?php echo $this->render('_blog_comments',array('model'=>$model));?>
                    
                  
                </div>
			      </div>
				
				
               </div>
			   
               <div id="related-posts">
                  <p>you may also like</p> 
				  
   
                   <div id="second_blog" class="owl-carousel company-post">
					
						<?php
						if($alsolike)
						{
						   foreach($alsolike as $yk=>$yv)
						   {
							   $categorySlug = $yv->blogCategory->Slug;
								$postSlug = $yv->BlogSlug;
						   ?>
						<div class="company">
				           <div class="col-md-12 col-sm-12 col-xs-12"> 
							   <div class="slider-post slick-slide" style="background:url(<?=$url.'/'.$yv->blogImage->Doc;?>) no-repeat">
								 <div class="slider-post-info">
								  <div class="category-wrapper">
								   <a class="category" href="<?= Url::base().'/blog/'.$categorySlug?>" tabindex="-1"><?=$yv->blogCategory->BlogCategoryName;?></a></div>
								   <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" class="post-title" tabindex="-1"><h2 class="heading"><?=$yv->BlogName;?></h2></a>
								  <span class="publish-date"><?=date('M d,Y',strtotime($yv->OnDate));?></span> </div>
							   </div>
								  </div>
					     </div>
					 <?php
						   }
						   }
						   ?>
				</div>
   
   
   
               </div>
        
           
            <div class="clear"></div>
         </div>
		 
		 
		 
		 
		 
		 
         <div class="sidebar-wrapper" id="wp-sidebar" >
            <div class="section" id="sidebar" name="Sidebar">
               <div class="widget FollowByEmail">
                  <h2 class="title"> Search the blog</h2>
                  <div class="widget-content">
                     <div class="follow-by-email-inner">
                        <table width="100%">
							    <tbody><tr>
							    <td>
							      <input class="follow-by-email-address" id="blogname" placeholder="Search blog" type="text">
							  </td>
							   <td width="64px">
							    <input class="follow-by-email-submit" onclick="searchblog();" type="button" value="Submit">
							     </td>
							     </tr>
							     </tbody></table>
                     </div>
                  </div>
               </div>
               <div class="widget Label" data-version="1" id="Label1">
                  <h2>categories</h2>
                  <div class="widget-content list-label-widget-content">
                     <ul>
                         <?php
						   foreach($allblogcategory as $ck=>$cvalue)
						   {
							   $categorySlug = $cvalue->blogCategory->Slug;
							?>
							  <li>
							  <a href="<?= Url::base().'/blog/'.$categorySlug;?>">  <?=$cvalue->BlogCategoryName;?></a>
							  <span dir="ltr"><?=$cvalue->cnt;?></span>
							  </li>
						   <?php
						   }
						   ?>
                     </ul>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="widget">
                  <h2 class="title">recent posts</h2>
                  <div class="widget-content recentposts">
                     <ul class="recent-posts-wrapper">
                     <?php
					foreach($recent as $rk=>$rvalue)
					{
						$categorySlug = $rvalue->blogCategory->Slug;
						$postSlug = $rvalue->BlogSlug;
                     ?>
					<li class="recent-post-item">
					   <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" class="post-image" ><img src="<?=$url.'/'.$rvalue->blogImage->Doc;?>"></a>
						<a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>" class="post-title"><h2 class="heading"><?=$rvalue->BlogName;?></h2></a>
					</li>
                        <?php
					}
					?>
					 </ul>
                     <div class="clear"></div>
                  </div>
               </div>
               <div class="widget FollowByEmail"  >
                  <h2 class="title">Follow by Email</h2>
                  <div class="widget-content">
                     <div class="follow-by-email-inner">
                        <form action="#" method="post" >
                           <table width="100%">
                              <tbody>
                                 <tr>
                                    <td>
                                       <input class="follow-by-email-address" name="email" placeholder="Email address..." type="text">
                                    </td>
                                    <td width="64px">
                                       <input class="follow-by-email-submit" type="submit" value="Submit">
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="blog-post-list" class="owl-carousel company-post">
      <?php
					 foreach($allblog as $bk=>$bval)
					 {
						 $categorySlug = $bval->blogCategory->Slug;
						$postSlug = $bval->BlogSlug;
						 
						?>
						<div class="company">
				        <div class="post-outer">
						    <div class="post hentry uncustomized-post-template"> 
								<div class="post-header">
										<h3 class="post-title entry-title heading"> <a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>"><?=$bval->BlogName;?></a> </h3>
										<span class="post-timestamp">  <a class="timestamp-link" href="#" rel="bookmark">
										    <a class="published"><?=date('M d, Y',strtotime($bval->OnDate));?></a> </a>
										</span>
								</div>
						    <div class="">
						      <a class="post-image" href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>"> <img src="<?=$url.'/'.$bval->blogImage->Doc;?>"> </a>
						   </div>
							<div class="blogp">
						      <?=substr(htmlspecialchars_decode($bval->BlogDesc),0,100)?>...
							</div>
						       <p class="more-link"><a href="<?= Url::base().'/blog/'.$categorySlug.'/'.$postSlug;?>">Continue Reading</a></p>
					    </div>
					  </div> 
					</div>
					 <?php
					 }
					 ?>
   </div>