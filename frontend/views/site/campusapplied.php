<?php
$this->title = 'Applied Campus';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use common\models\Plan;
use common\models\PlanAssign;
$plan=new Plan();
$allplan=$plan->getAllplan();
$planassign=new PlanAssign();
$isassign=$planassign->isAssign(Yii::$app->session['Employerid']);

?>

<style>
  .profile-content .card{min-height:220px; padding-bottom: 10px;}  
  .profile-content{margin-bottom:15px;}
    .profile-content .firstinfo .profileinfo h1{font-size:16px;}
</style>
<div class="wrapper"><!-- start main wrapper -->

		 
		<div class="inner_page">
			<div class="container">
					  
					 
					 <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"  id="mobile_design">
					 <h4> Applied Campus  </h4>
					 <div class="pannel_header margin_top">
					 <!--<div class="width-13">
							 <div class="checkbox">
							    <label> <input type="checkbox" value="">
								<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> 
							    </label>
							  </div>					     
							</div>
							
					      <div class="width-14">
							 <div class="checkbox">
							    <label> 
								  Sms
							    </label>
							  </div>					     
							</div>
					      
						  
						  <div class="width-14">
							 <div class="checkbox">
							    <label>  
								 Mail
							    </label>
							  </div>					     
							</div>
							
							
							 <div class="width-14">
							 <div class="checkbox">
							    <label> 
								  Download  
							    </label>
							  </div>					     
							</div>
					    <div class="width-10" id="select_per_page">	
                            <div class="form-groups">
							<div class="col-sm-8">
						      <label>Result Per page </label>
							 </div>
						<div class="col-sm-4">
							<?php
							if(isset($_GET['perpage']))
							{
							$perpage=$_GET['perpage'];
							}else{$perpage=10;}
							?>
                              <!--<select id="form-filter-location" data-minimum-results-for-search="Infinity" class="form-control23 select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="getresultperpage(this.value,'hirecandidate');">
								<option value="10" <?php if($perpage==10) echo "selected='selected'";?>>10</option>
                                <option value="20" <?php if($perpage==20) echo "selected='selected'";?>>  20  </option>
                                <option value="30" <?php if($perpage==30) echo "selected='selected'";?>>  30 </option>
                                <option value="40" <?php if($perpage==40) echo "selected='selected'";?>>  40  </option>
								<option value="50" <?php if($perpage==50) echo "selected='selected'";?>>  50  </option> 
                              </select> 
                            </div>
							 </div>
                          </div>
					 </div>-->
					 
				<?php
				foreach($appliedjob as $key=>$value)
				{
										if($value->user->photo)
										{
											$doc=$url.$value->user->photo->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
										$availableemail=$isassign->TotalEmail-$isassign->UseEmail;
										$availableviewcontact=$isassign->ViewContact-$isassign->UseViewContact;
										?>
					 <div class="profile-content payment_list">
                                <div class="card">
                                     <p class="last_update_main" style="font-size:16px; margin-bottom:15px"><strong style="color:#f16b22"> Job Title :</strong><b> <?=$value->job->JobTitle;?></b></p>
                                      <div class="clear"> </div>
                                    <div class="firstinfo">
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive">
                                        <div class="profileinfo">
                                            
                                             
                                            
                                             
                                            <h1>  <strong style="color: #f16b22;font-weight:normal">Co-ordinator Name:</strong> <?=$value->user->Name;?></h1>
											
											<small> <strong style="color: #f16b22;font-weight:normal">Location :</strong> <?=$value->user->City;?>, <?=$value->user->State;?></small>
											
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														
														<li>College: <?=$value->user->CollegeName;?> Years   </li>
														<li>Website:  <?=$value->user->CollegeWebsite;?> </li>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc" style="margin-bottom:0px;">
														<?php
														if($value->user->experiences)
														{
														?>
													    <li> Current Salary <?=$value->user->experiences[0]->Salary;?> Lakhs   </li>
														<?php
														}
														if($value->user->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->user->educations[0]->HighestQualification;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills" style="display:none">
													<?php
													if($value->user->empRelatedSkills)
													{
													foreach($value->user->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
										 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->user->UpdatedDate));?></p>
														<!--		 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p><br/> -->
													
										</div>
                                    </div>
		 
									 <div class="contact_me">
									    <!--  <a <?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['campus/collegeprofile','UserId'=>$value->UserId])?>" target="_blank" onclick="viewcontact();"<?php }else{ ?>href='#/'<?php } ?>>
					 View Profile   </a> -->
									  <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->user->Email;?>');" <?php }else{?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
									  
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['campus/collegeprofile','UserId'=>$value->user->UserId])?>" target="_blank" onclick="viewcontact();" <?php }else{ ?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div></a>
							
							
					<!--profile-content-->
					<?php
				}
				?>
				
			 <!-- Modal -->
  <div class="modal fade" id="myModal_buy_a_plan" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Buy a Plan</h4>
        </div>
									     <?php
		 if($allplan)
		 {
			foreach($allplan as $key=>$pland)
			{
				?>
									   <div class="widget_inner">
											   <div class="panel price panel-red">
												<div class="panel-body text-center">
													<p class="lead" ><strong><?=$pland->PlanName;?></strong></p>
												</div> 
												<ul class="list-group list-group-flush text-center width-half"> 
											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
											   <li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li> 
												</ul>
												<div class="panel-footer">
													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
												</div>
											</div>
									   <!-- /PRICE ITEM -->
								       </div>
									   <?php
			}
		 }
		 ?>
			   </div>  
			   </div>
		 	</div>
			<!-- Modal -->		 				

							<?php
							echo LinkPager::widget([
								'pagination' => $pages,
							]);
							?>
  
  
  
                        </div>
      </div>
		    </div>
		<div class="border"></div>
		</div><!-- end main wrapper -->
		
<!-----myModal_email------>
<div class="modal fade" id="myModal_email" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mail To Candidate</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<div class="form-group" style="display: none;">
		<?= $form->field($mailmodel, 'EmployeeId')->textInput(['id'=>'EmployeeId','maxlength' => true])->label(false) ?>
		</div>
		<div class="form-group">
		<?= $form->field($mailmodel, 'Subject')->textInput(['maxlength' => true,'Placeholder'=>'Subject'])->label(false) ?>
		</div>
								  <div class="col-md-12 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Description*  </label>
										
								<?= $form->field($mailmodel, 'MailText')->widget(TinyMce::className(), [
									'options' => ['rows' => 6],'class'=>'form-control textarea-small',
									'language' => 'en_CA',
									'clientOptions' => [
										'plugins' => [
											"advlist autolink lists link charmap print preview anchor",
											"searchreplace visualblocks code fullscreen",
											"insertdatetime media table  paste spellchecker"
										],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								])->label(false);?>
                              </div> 
							   
							  </div>
							  
							  <div class="col-md-12 col-sm-12 col-xs-12">
							
									<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green']) ?>
								</div>
								</div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
			   </div>  
			   </div>
		 	</div>
<!-----myModal_email------>
<script type="text/javascript">
		function mailtosemp(id,email) {
			$('#EmployeeId').val(email);
			$('#'+id).modal('show');
        }
		function viewcontact() {
            $.ajax({url:"<?= Url::toRoute(['site/contactview'])?>",
				   success:function(results)
				   {
					
				   }
			});
        }
</script>