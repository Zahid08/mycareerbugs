<?php
$this->title = 'Campus Profile';
$csrfToken = Yii::$app->request->getCsrfToken();
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';

if($campus->PhotoId!=0)
                {
                    $url=str_replace('frontend','backend',(str_replace('web','',Yii::$app->getUrlManager()->getBaseUrl())));
                    $pimage=$url.$campus->photo->Doc;
                }
                else
                {
                    $pimage=$imageurl.'images/user.png';
                }
?>
<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">
	<div id="wrapper"><!-- start main wrapper -->
		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
			   <div class="col-md-2 col-sm-2 col-xs-12">
			                 <div class="user-profile">
                                    <img src="<?=$pimage;?>" alt="" class="img-responsive center-block ">
                                    <h3><?=$campus->Name;?></h3>
                                </div> 
			      	</div>
		         <div class="col-md-10 col-sm-10 col-xs-12"> 
                            <div class="job-short-detail">
                                <div class="heading-inner">
                                    <p class="title"> Collage Details</p>
                                </div>
								
								
								
                                <dl>
                                     
                                    <dt>College Name</dt>
                                    <dd><?=$campus->CollegeName;?></dd>

                                    <dt> Company Email </dt>
                                    <dd><?=$campus->Email;?></dd>
 
                                    <dt>Address:</dt>
                                    <dd><?=$campus->Address;?></dd>
									 
									 <dt>Website:</dt>
                                    <dd><?=$campus->CollegeWebsite;?></dd>
									
									 <dt> Mobile Number  </dt>
                                    <dd> <?=$campus->MobileNo;?>   </dd>
									
									
									 <dt>  Contact Number</dt>
                                    <dd> <?=$campus->ContactNo;?> </dd>

                                    <dt>City:</dt>
                                    <dd><?=$campus->City;?></dd>

                                    <dt>State:</dt>
                                    <dd><?=$campus->State;?></dd>

                                    <dt>Country:</dt>
                                    <dd><?=$campus->Country;?></dd>
									
									<dt>Pincode</dt>
                                    <dd><?=$campus->PinCode;?></dd>
                                </dl>
                            </div>
 
                        </div>
						 
	 <div class="clearfix"> </div>
	
						
				<div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Collage Description </p>
                                </div>
                                <div class="row education-box">
                                    
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="degree-info">
                                            <p><?=$campus->CompanyDesc;?></p>
                                             </div>
                                    </div>
                                </div>
                                
                            </div>

							<div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Dean / Director / Principal </p>
                                </div>
                                <div class="row education-box">
									<?php
									if($campus->PhotoId!=0)
									{
										$campusphoto=$url.$campus->photo->Doc;
									?>
                                    <div class="col-xs-12 col-md-2 col-sm-2">
									<img style="" src="<?=$url.$campus->photo->Doc;?>"  alt="" class="img-responsive center-block ">
									 </div>
									<?php
									}
									?>
                                       <div class="col-xs-12 col-md-10 col-sm-10">
                                        <div class="degree-info">
                                            <p><?=$campus->AboutYou;?></p>
											 
                                             </div>
											 
                                    </div>
                                </div>
                                
                            </div>
							
							
							
							
								<div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Collage pictures  </p>
                                </div>
                                <div class="row education-box">
                                     <div class="col-xs-12 col-md-12 col-sm-12">
									 
									 
									 
		<?php
				if($campus->collegepics)
				{
					?>
            <div class="col-sm-7 margin_0auto">
			   <div class="col-xs-12" id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div class="col-sm-12" id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel1">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
									<?php
									foreach($campus->collegepics as $key=>$value)
									{
									?>
                                    <div class="item <?=($key==0?'active':'');?>" data-slide-number="<?=$key;?>">
                                        <img src="<?=$url.$value->pic->Doc;?>" >
									</div>
									<?php
									}
									?>

                                <!-- Carousel nav -->
                                <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Slider-->
        </div>
		 <div class="col-sm-12" id="slider-thumbs">
                <!-- Bottom switcher of slider -->
			  <ul class="hide-bulletss">
				<?php
					foreach($campus->collegepics as $key=>$value)
					{
				?>
                    <li class="col-sm-2"> 
					    <a class="thumbnail" id="carousel-selector-<?=$key;?>"> <img src="<?=$url.$value->pic->Doc;?>"> </a>
                    </li>
				<?php
					}
					?>
                </ul>
            </div>
		 <?php
				}
				?>
									 </div>
                                </div>
                                
                            </div>
                        </div>
						 
						 <div class="clearfix"> </div>
  </div>
            </div>
       </div>
		<div class="border"></div>