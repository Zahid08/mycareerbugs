<?php
$this->title = 'Notification';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<!-- start main wrapper --> 
	<div id="wrapper">
	
		<div class="recent-job"><!-- Start Recent Job -->
			<div class="container">
				<div class="row">
				  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
			 
			 <div class="col-lg-12 col-md-5 col-sm-5 col-xs-6">
						<h4><i class="glyphicon glyphicon-briefcase"></i> All Notification</h4>
					 </div>	 
					  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6" id="landing_select">
					 
 <div class="clearfix"></div>
		
		<?php
		if($allnotification)
		{
		foreach($allnotification as $nk1=>$nval1)
		{
		?>
		 <div class="item-list">
                <a href="<?=$nval1['link'];?>">
                <div class="col-sm-12 add-desc-box">
                  <div class="add-details">
                    <h5 class="add-title"><?=$nval1['title'];?></h5>
                    <div class="info"> 
                      <span class="category"><?=$nval1['name'];?></span> -
                      <span class="item-location"><i class="fa fa-map-marker"></i> <?=$nval1['address'];?></span> <br><br/>
                    <span> <strong><?=$nval1['title'];?></strong> </span>
					</div>
                  </div>
                </div>
                </a>
              </div>
		<?php
		}
		}
        else
        {
		?>
        <h5 class="add-title">There is no notification</h5>
        <?php
        }
        ?>
					 
						<div class="spacer-2"></div>
					</div>
					 
                  </div>
					
					
					<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side">
							<div class="clearfix"></div>
						<div class="spacer-5"></div><div class="spacer-5"></div><div class="spacer-5"></div>
						
							<!--adban_block main -->
								<div class="adban_block">
							      <img src="<?=$imageurl;?>images/adban_block/ban.jpg"> 
							    </div>
						   <!-- adban_block  main -->
								
								
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div><!-- end Recent Job -->

</div><!-- end main wrapper -->

