<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\helpers\ArrayHelper;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\EmployeeSkill;
use common\models\Skill;
use common\models\PlanRecord;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
$daily_CV = Yii::$app->myfunctions->GetTodayPlan('CVDownload', $isassign->plan->PlanId, date("Y-m-d"));
$daily_whatsApp = Yii::$app->myfunctions->GetTodayPlan('whatsappview', $isassign->plan->PlanId, date("Y-m-d"));
$daily_Email = Yii::$app->myfunctions->GetTodayPlan('Email', $isassign->plan->PlanId, date("Y-m-d"));
$daily_Download = Yii::$app->myfunctions->GetTodayPlan('Download', $isassign->plan->PlanId, date("Y-m-d"));
$daily_Download = $isassign->plan->daily_download - $daily_Download;
$daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView', $isassign->plan->PlanId, date("Y-m-d"));
$email_limit_reached = 'myModal_buy_a_plan';
$cv_limit_reached = 'myModal_buy_a_plan';
$wapp_limit_reached = 'myModal_buy_a_plan';
$contact_limit_reached = 'myModal_buy_a_plan';
if (isset(Yii::$app->session['Employerid']) && count((array)$isassign) > 0 && $isassign->plan->total_daily_email <= $daily_Email) {
    $email_limit_reached = "daily_limit_reach";
}
if (isset(Yii::$app->session['Employerid']) && count((array)$isassign) > 0 && $isassign->plan->total_daily_cv_download <= $daily_CV) {
    $cv_limit_reached = "daily_limit_reach";
    ;
}
if (isset(Yii::$app->session['Employerid']) && count((array)$isassign) > 0 && $isassign->plan->daily_whatsapptotal <= $daily_whatsApp) {
    $wapp_limit_reached = "daily_limit_reach";
}
if (isset(Yii::$app->session['Employerid']) && count((array)$isassign) > 0 && $isassign->plan->total_daily_view_contact <= $daily_Contact) {
    $contact_limit_reached = "daily_limit_reach";
}
?>
<?php
$cn = 0;
if ($candidate) {
    foreach ($candidate as $key => $value) {

        if (isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid'])) {
            // echo Yii::getAlias('@backend').'/web/'.$value->photo->Doc;
            if ($value->photo && file_exists(Yii::getAlias('@backend') . '/web/' . $value->photo->Doc)) {
                $doc = $url . $value->photo->Doc;
            } else {
                $doc = $imageurl . 'images/user.png';
            }
        } else {
            $doc = $imageurl . 'images/user.png';
        }
        if (isset($_GET['stype']) && $_GET['stype'] == 'experience' && ! empty($salaryrange)) {
            if ($experience != '' && $role == '') {
                $ex = explode('-', $experience);
                $exp = $ex[1];
                $cnt = count($value->experiences) - 1;
                if (in_array($value->experiences[$cnt]->Salary, $salaryrange) && $exp == $value->getTotalexp($value->UserId)) {
                    ?>
<div class="profile-content payment_list">
	<div class="card">
		<input type="checkbox" style="position: absolute; z-index: 999;"
			class="empch" value="">

		<div class="firstinfo">
			<input type="hidden" class="empid" value="<?=$value->Email;?>" /> <input
				type="hidden" class="employeeid" value="<?=$value->UserId;?>" /> <img
				src="<?=$doc;?>" alt="" class="img-circle img-responsive"
				alt="MCB Cadndiate Photo">
			<div class="profileinfo">
				<h1> <?=$value->Name;?></h1>
											<?php
                    if ($value->experiences) {
                        $cnt = reset($value->experiences);
                        $positionv = $cnt->position->Position;
                        ?>
											<small><?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                    } else {
                        ?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                    }
                    ?>
											<div class="spcae1"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
														<?php
                    if ($value->experiences) {
                        $cnt = reset($value->experiences);
                        ?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
								<li>Company:  <?=$cnt->CompanyName;?> </li>
														<?php
                    }
                    ?>
														
													  </ul>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
														<?php
                    if ($value->experiences) {
                        $cnt = reset($value->experiences);
                        ?>
													    <li> Current Salary <?=$cnt->Salary;?> Lakhs   </li>
														<?php
                    }

                    if ($value->educations[0] && ! empty($value->educations[0]->course->CourseName)) {
                        ?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
                    }
                    ?>

													   </ul>
						</div>
					</div>
				</div>


				<div class="profile-skills">
													<?php
                    if ($value->empRelatedSkills) {
                        foreach ($value->empRelatedSkills as $ask => $asv)
                            if (! empty($asv->skill->Skill)) {
                                {
                                    ?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
                                }
                            }
                    }
                    ?>
														
													</div>

				<p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
				<p class="last_update_main" style="display: none;">Shortlisted by 2
					Recruiters recently</p>
			</div>
		</div>

		<div class="contact_me">
									<?php
                    $availableviewwapp = $isassign->plan->daily_whatsapptotal - $daily_whatsApp;
                    if (! empty($value->whatsappno)) {
                        $whatsappno = "https://api.whatsapp.com/send?phone=" . $value->whatsappno;
                        ?>
										<a
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>
				href="<?=$whatsappno?>" target="_blank"
				onclick="viewwhatsapp('<?=$value->UserId?>',event,this);" <?php }else{ ?>
				data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"
				<?php } ?> class="btn-default" type="button"> <i
				class="fa fa-envelope-o"></i> WhatsApp
			</a>
									<?php } ?>
																		 
									<?php
                    $availablecv = $isassign->plan->total_daily_cv_download - $daily_CV;
                    if ($value->CVId != 0 && isset(Yii::$app->session['Employerid']) && count($isassign) > 0 && $availablecv > 0) {
                        $na = explode(" ", $mval->Name);
                        $name = $na[0];
                        $extar = explode(".", $mval->cV->Doc);
                        $ext = $extar[1];
                        $filepath = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/' . $value->cV->Doc;
                        if (file_exists($filepath)) {
                            ?>
									  	<a href="<?=$url.$value->cV->Doc;?>"
				onclick="cvdowndddload('<?=$value->UserId?>');"
				download="<?=$name.'.'.$ext;?>" class="btn-default"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                        }
                    } else {
                        ?>
										<a href="" class="btn-default" type="button"
				data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                    }
                    $availableemail = $isassign->plan->total_daily_email - $daily_Email;
                    $availableviewcontact = $isassign->plan->total_daily_view_contact - $daily_Contact;
                    ?>
                                        <a class="btn-default"
				type="button"
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>
				onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');"
				<?php }else{?> data-toggle="modal"
				data-target="#<?=$email_limit_reached?>" <?php } ?>> <i
				class="fa fa-phone"></i> Email
			</a> <a
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>
				href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>"
				target="_blank" onclick="viewcontact('<?=$value->UserId?>');"
				<?php }else{ ?> data-toggle="modal"
				data-target="#<?=$contact_limit_reached?>" <?php } ?>
				class="btn-default" type="button"> <i class="fa fa-eye"></i> View
				Profile
			</a>
		</div>
	</div>
</div>


<!--profile-content-->
<?php
                    $cn ++;
                }
            } elseif (in_array($value->experiences[$cnt]->Salary, $salaryrange)) {
                ?>
<div class="profile-content payment_list">
	<div class="card">
		<input type="checkbox" style="position: absolute; z-index: 999;"
			class="empch" value="">

		<div class="firstinfo">
			<input type="hidden" class="empid" value="<?=$value->Email;?>" /> <input
				type="hidden" class="employeeid" value="<?=$value->UserId;?>" /> <img
				src="<?=$doc;?>" alt="" class="img-circle img-responsive"
				alt="MCB candidate Photo">
			<div class="profileinfo">
				<h1> <?=$value->Name;?></h1>
											<?php
                if ($value->experiences) {
                    $cnt = reset($value->experiences);
                    $positionv = $cnt->position->Position;
                    ?>
											<small><?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                } else {
                    ?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                }
                ?>
											<div class="spcae1"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
														<?php
                if ($value->experiences) {
                    $cnt = reset($value->experiences);
                    ?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
								<li>Company:  <?=$cnt->CompanyName;?> </li>
														<?php
                }
                ?>
													
																<?php
                if ($value->experiences) {
                    $cnt = reset($value->experiences);
                    ?>
													    <li> Current Salary <?=$cnt->Salary;?> Lakhs   </li>
														<?php
                }
                if ($value->educations) {
                    ?>
                    
													  </ul>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
											
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
                }
                ?>
													   </ul>
						</div>
					</div>
				</div>


				<div class="profile-skills">
													<?php
                if ($value->empRelatedSkills) {
                    foreach ($value->empRelatedSkills as $ask => $asv) {
                        if (! empty($asv->skill->Skill)) {
                            ?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
                        }
                    }
                }
                ?>
														
													</div>

				<p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
				<p class="last_update_main" style="display: none;">Shortlisted by 2
					Recruiters recently</p>
			</div>
		</div>

		<div class="contact_me">
									 <?php
                $availableviewwapp = $isassign->plan->daily_whatsapptotal - $daily_whatsApp;
                if (! empty($value->whatsappno)) {
                    $whatsappno = "https://api.whatsapp.com/send?phone=91" . $value->whatsappno;
                    ?>
										<a
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>
				href="<?=$whatsappno?>" target="_blank"
				onclick="viewwhatsapp('<?=$value->UserId?>',event,this);" <?php }else{ ?>
				data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"
				<?php } ?> class="btn-default" type="button"> <i
				class="fa fa-envelope-o"></i> WhatsApp
			</a>
									<?php

}
                ?>
									 
									  	<?php
                $availablecv = $isassign->plan->total_daily_cv_download - $daily_CV;
                if ($value->CVId != 0 && isset(Yii::$app->session['Employerid']) && count($isassign) > 0 && $availablecv > 0) {
                    $na = explode(" ", $mval->Name);
                    $name = $na[0];
                    $extar = explode(".", $mval->cV->Doc);
                    $ext = $extar[1];
                    $filepath = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/' . $value->cV->Doc;
                    if (file_exists($filepath)) {
                        ?>
									  	<a href="<?=$url.$value->cV->Doc;?>"
				onclick="cvdodddddddddwnload('<?=$value->UserId?>');"
				download="<?=$name.'.'.$ext;?>" class="btn-default"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                    }
                } else {
                    ?>
										<a href="" class="btn-default" type="button"
				data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                }
                $availableemail = $isassign->plan->total_daily_email - $daily_Email;
                $availableviewcontact = $isassign->plan->total_daily_view_contact - $daily_Contact;
                ?>
                                        <a class="btn-default"
				type="button"
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>
				onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');"
				<?php }else{?> data-toggle="modal"
				data-target="#<?=$email_limit_reached?>" <?php } ?>> <i
				class="fa fa-phone"></i> Email
			</a> <a
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>
				href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>"
				target="_blank" onclick="viewcontact('<?=$value->UserId?>');"
				<?php }else{ ?> data-toggle="modal"
				data-target="#<?=$contact_limit_reached?>" <?php } ?>
				class="btn-default" type="button"> <i class="fa fa-eye"></i> View
				Profile
			</a>
		</div>
	</div>
</div>


<!--profile-content-->
<?php
                $cn ++;
            }
        } else {
            if ($experience != '' && $role == '') {
                $ex = explode('-', $experience);
                $exp = $ex[1];
                if ($exp == $value->getTotalexp($value->UserId)) {
                    ?>
<div class="profile-content payment_list">
	<div class="card">
		<input type="checkbox" style="position: absolute; z-index: 999;"
			class="empch" value="">

		<div class="firstinfo">
			<input type="hidden" class="empid" value="<?=$value->Email;?>" /> <input
				type="hidden" class="employeeid" value="<?=$value->UserId;?>" /> <img
				src="<?=$doc;?>" alt="" class="img-circle img-responsive"
				alt="MCB candidate Photo">
			<div class="profileinfo">
				<h1> <?=$value->Name;?></h1>
											<?php
                    if ($value->experiences) {

                        $cnt = reset($value->experiences);
                        $positionv = $cnt->position->Position;
                        ?>
											<small> Current Position :  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                    } else {
                        ?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                    }
                    ?>
											<div class="spcae1"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
														<?php
                    if ($value->experiences) {
                        $cnt = reset($value->experiences);
                        ?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
								<li>Company:  <?=$cnt->CompanyName;?> </li>
														<?php
                    }
                    ?>
														
															<?php
                    if ($value->experiences) {
                        $cnt = reset($value->experiences);
                        ?>
													    <li> Current Salary <?=$cnt->Salary;?> Lakh   </li>
														<?php
                    }
                    if ($value->educations) {
                        ?>
													  </ul>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
													
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
                    }
                    ?>
													   </ul>
						</div>
					</div>
				</div>


				<div class="profile-skills">
													<?php
                    if ($value->empRelatedSkills) {
                        foreach ($value->empRelatedSkills as $ask => $asv) {
                            if (! empty($asv->skill->Skill)) {
                                ?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
                            }
                        }
                    }
                    ?>
														
													</div>

				<p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
				<p class="last_update_main" style="display: none;">Shortlisted by 2
					Recruiters recently</p>
			</div>
		</div>

		<div class="contact_me">
									 <?php
                    $availableviewwapp = $isassign->plan->daily_whatsapptotal - $daily_whatsApp;
                    ;
                    if (! empty($value->whatsappno)) {
                        $whatsappno = "https://api.whatsapp.com/send?phone=91" . $value->whatsappno;
                        ?>
										<a
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>
				href="<?=$whatsappno?>" target="_blank"
				onclick="viewwhatsapp('<?=$value->UserId?>',event,this);" <?php }else{ ?>
				data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"
				<?php } ?> class="btn-default" type="button"> <i
				class="fa fa-envelope-o"></i> WhatsApp
			</a>
									<?php

}
                    ?>
									 
									  	<?php
                    $availablecv = $isassign->plan->total_daily_cv_download - $daily_CV;
                    if ($value->CVId != 0 && isset(Yii::$app->session['Employerid']) && count($isassign) > 0 && $availablecv > 0) {
                        $na = explode(" ", $mval->Name);
                        $name = $na[0];
                        $extar = explode(".", $mval->cV->Doc);
                        $ext = $extar[1];
                        $filepath = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/' . $value->cV->Doc;
                        if (file_exists($filepath)) {
                            ?>
									  	<a href="<?=$url.$value->cV->Doc;?>"
				onclick="cvdaaaaownload('<?=$value->UserId?>');"
				download="<?=$name.'.'.$ext;?>" class="btn-default"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                        }
                    } else {
                        ?>
										<a href="" class="btn-default" type="button"
				data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                    }
                    $availableemail = $isassign->plan->total_daily_email - $daily_Email;
                    $availableviewcontact = $isassign->plan->total_daily_view_contact - $daily_Contact;
                    ?>
                                        <a class="btn-default"
				type="button"
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>
				onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');"
				<?php }else{?> data-toggle="modal"
				data-target="#<?=$email_limit_reached?>" <?php } ?>> <i
				class="fa fa-phone"></i> Email
			</a> <a
				<?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>
				href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>"
				target="_blank" onclick="viewcontact('<?=$value->UserId?>');"
				<?php }else{ ?> data-toggle="modal"
				data-target="#<?=$contact_limit_reached?>" <?php } ?>
				class="btn-default" type="button"> <i class="fa fa-eye"></i> View
				Profile
			</a>
		</div>
	</div>
</div>


<!--profile-content-->
<?php
                    $cn ++;
                }
            } else {
                $cn ++;
                ?>

<div class="profile-content payment_list">
	<!--onclick="if($(this).find('.empch').prop('checked')==true){$(this).find('.empch').prop('checked',false);}else{$(this).find('.empch').prop('checked',true);}"-->
	<div class="card">
		<input type="checkbox" style="position: absolute; z-index: 999;"
			class="empch" value="">
		<div class="firstinfo">
			<input type="hidden" class="empid" value="<?=$value->Email;?>" /> <input
				type="hidden" class="employeeid" value="<?=$value->UserId;?>" /> <img
				src="<?=$doc;?>" alt="" class="img-circle img-responsive"
				alt="MCB candidate Photo">

			<div class="profileinfo">

				<h1> <?=$value->Name;?></h1>
											<?php
                if ($value->experiences) {

                    $cnt = reset($value->experiences);
                    $positionv = $cnt->position->Position;
                    ?>
											<small> Current Position : <?=$positionv;?>    from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                } else {
                    ?>
											<small>From    <?=$value->City;?>, <?=$value->State;?></small>
											<?php
                }
                ?>
											<div class="spcae1"></div>

				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
														<?php
                if ($value->experiences) {
                    $cnt = reset($value->experiences);
                    ?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
								<li>Company:  <?=$cnt->CompanyName;?> </li>
														<?php
                }
                ?>
														
															<?php
                if ($value->experiences) {
                    $cnt = reset($value->experiences);
                    ?>
													    <li> Current Salary <?=$cnt->Salary;?> Lakh   </li>
														<?php
                }
                if ($value->educations && $value->educations[0]->nqual) {
                    // echo "<pre>";print_r($value->educations[0]->nqual->name);
                    ?>
													  </ul>
						</div>

						<div class="col-md-6 col-sm-12 col-xs-12">
							<ul class="commpany_desc">
													
														 <li> Last Qualification: <?=$value->educations[0]->nqual->name;?>   </li>
														 <?php
                }
                if ($value->language) {
                    ?>
															<li> Language: <?=$value->language;?> </li>
														<?php } ?>
														
														<?php if($value->CollarType == "white"){?>
															<?php $CategoryIds1 = ArrayHelper::map(UserJobCategory::find()->where(['user_id' => $value->UserId])->all(), 'category', 'category');
															
															$CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $value->UserId
																])->all(), 'category_id', 'category_id');
														  $expCategory = ArrayHelper::map(WhiteCategory::find()->where([
																	'id' => $CategoryIds
																])->all(), 'name', 'name');
															$categoryData = array_merge($CategoryIds1, $expCategory);
															?>
															<li> Job Category: <?php echo (!empty($categoryData)?implode(', ',$categoryData):"");?> </li>
															<?php $userRole1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $value->UserId
																	])->all(), 'role_id', 'role');
																$expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $value->UserId,
																	])->all(), 'role', 'role');
																$userRole = array_merge($userRole1, $expRoles);	
																	?>
															<li> Role: <?php echo (!empty($userRole)?implode(', ',$userRole):"");?> </li>	
															
															<?php 
																$skill_ary = array();
																$skill_ary1 = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $value->UserId
																	])->all(), 'skill', 'skill');
																	
																 $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $value->UserId,
																	])->all(), 'skill', 'skill');	
																$skill_ary = array_merge($skill_ary1, $expSkills);

																	?>
															
															<?php }else{?>
															<?php
																$role = array();
																$skill_ary = array();
																if ($value->empRelatedSkills) {
																	foreach ($value->empRelatedSkills as $ask => $asv) {
																		if ($asv->skill->position->Position && ! in_array($asv->skill->position->Position, $role)) {
																			$role[] = $asv->skill->position->Position;
																		}
																		if (! empty($asv->skill->Skill)) {

																			$skill_ary[] = $asv->skill->Skill;
																		}
																	}
																}
																
																$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
																	'IsDelete' => 0,
																	'UserId' => $value->UserId,
																])->all(), 'Employeeskillid', 'SkillId');
																
																$skills = ArrayHelper::map(Skill::find()->where([
																	'IsDelete' => 0,
																	'SkillId' => $employeeSkill
																])->all(), 'SkillId', 'Skill');
																
																$skill_ary = array_merge($skill_ary, $skills);
																$skill_ary = array_unique($skill_ary);
																
																
																
																?>
														<li> Role: <?=implode(',', $role);?> </li>
														
														<?php }?>														
														
														
							</ul>
						</div>
					</div>
				</div>


				<div class="profile-skills">
													<?php
				if(!empty($skill_ary)){
                foreach ($skill_ary as $ask => $asv) {
                    ?>
														<span> <?=$asv;?> </span>
													<?php
                } }
                ?>
														
													</div>

				<p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
				<p class="last_update_main" style="display: none;">Shortlisted by 2
					Recruiters recently</p>
			</div>
		</div>

		<div class="contact_me">
									<?php
                $availableviewwapp = $isassign->plan->daily_whatsapptotal - $daily_whatsApp;
                if (! empty($value->whatsappno)) {
                    $whatsappno = "https://api.whatsapp.com/send?phone=91" . $value->whatsappno;
                    ?>
										<a
				<?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableviewwapp>0) { ?>
				href="<?=$whatsappno?>" target="_blank"
				onclick="viewwhatsapp('<?=$value->UserId?>',event,this);" <?php }else{ ?>
				data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"
				<?php } ?> class="btn-default" type="button"> <i
				class="fa fa-envelope-o"></i> WhatsApp
			</a>
									<?php

}
                ?>
									 
									  	<?php
                // if(isset(Yii::$app->session['Employerid'])){

                $availablecv = $isassign->plan->total_daily_cv_download - $daily_CV;
                
                
                if ($availablecv > 0) {
                    $na = explode(" ", $mval->Name);
                    $name = $na[0];
                    $extar = explode(".", $mval->cV->Doc);
                    $ext = $extar[1];
                    $filepath = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/' . $value->cV->Doc;
                    if (file_exists($filepath) && $value->cV->Doc != '') {
                        ?>
									  	<a href="javascript:void(0);"
				onclick="cvdownload('<?=$value->UserId?>','<?=$url.$value->cV->Doc;?>','s');"
				download="<?=$name.'.'.$ext;?>" class="btn-default"> <i
				class="fa fa-download"></i> CV
			</a>
										<?php
                    } else {
                        ?>
									  	<a
				href="javascript:void(0);"
				onclick="cvdownload('<?=$value->UserId?>','<?= Url::toRoute(['content/mycareerbugcv','UserId'=>$value->UserId])?>','n');" 
				class="btn-default"> <i class="fa fa-download"></i> CV
			</a>
										<?php
                    }
                } else {
                    ?>
										<a href="javascript:void(0);" data-toggle="modal"
				data-target="#<?=$cv_limit_reached?>" class="btn-default"
				type="button"> <i class="fa fa-download"></i> CV
			</a>
										<?php
                }

                $availableemail = $isassign->plan->total_daily_email - $daily_Email;
                $availableviewcontact = $isassign->plan->total_daily_view_contact - $daily_Contact;
                ?>
                                        <a class="btn-default"
				type="button"
				<?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableemail>0) { ?>
				onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');"
				<?php }else{?> data-toggle="modal"
				data-target="#<?=$email_limit_reached?>" <?php } ?>> <i
				class="fa fa-phone"></i> Email
			</a> <a
				<?php if(isset(Yii::$app->session['Employerid']) && count((array)$isassign)>0 && $availableviewcontact>0) { ?>
				href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>"
				target="_blank" onclick="viewcontact('<?=$value->UserId?>');"
				<?php }else
			       { ?> data-toggle="modal"
				data-target="#<?=$contact_limit_reached?>" <?php } ?>
				class="btn-default" type="button"> <i class="fa fa-eye"></i> View
				Profile
			</a>
										<?php //} ?>
									 </div>

	</div>
</div>


<!--profile-content-->
<?php
            }
        }
    }
} else {
    $cn = 1;
    ?>
				No Candidate Found In this Search Category
				<?php
}
if ($cn == 0) {
    echo "No Candidate Found In this Search Category";
}
?>

<!-- Modal -->
<div class="modal fade" id="myModal_buy_a_plan" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Buy a Plan</h4>
			</div>
		 
								 <?php
        if ($allplan) {
            foreach ($allplan as $key => $pland) {
                ?>
									   <div class="widget_inner">
				<div class="panel price panel-red">
					<div class="panel-body text-center">
						<p class="lead">
							<strong><?=$pland->PlanName;?></strong>
						</p>
					</div>
					<ul class="list-group list-group-flush text-center width-half">
						<li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
						<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
						<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
						<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
						<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
						<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li>
					</ul>
					<div class="panel-footer">
						<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
					</div>
				</div>
				<!-- /PRICE ITEM -->
			</div>
									   <?php
            }
        }
        ?>  
			   </div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="daily_limit_reach" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">You have reached your daily limit.</h4>
			</div>
		</div>
	</div>
</div>

<?php
echo LinkPager::widget([
    'pagination' => $pages
]);
?>

<script type="text/javascript">
  	function mailtoemp(id) {
			var email='';
			var UserId = '';
			var totalmail = <?php echo (isset(Yii::$app->session['Employerid']))?$availableemail:0;?>;
			var total_checked = $('.profile-content .empch:checkbox:checked').length;
// 			if(total_checked>totalmail){
// 				alert("You have remaining "+totalmail+" mail limit.");
// 				return false;
// 			}
			$('.profile-content .empch:checkbox:checked').each(function(){
				email+=$(this).next().find('.empid').val()+'|';
			});
			$('.profile-content .empch:checkbox:checked').each(function(){
				UserId+=$(this).next().find('.employeeid').val()+'|';
			});
			$('#EmployeeId').val(email);
			$('#MailUserId').val(UserId);
			$('#'+id).modal('show');
        }

        function download() {
            var eid='';
			var totaldownload=<?php echo (isset(Yii::$app->session['Employerid']))?$daily_Download:0;?>;
			var total_checked = $('.profile-content .empch:checkbox:checked').length;
// 			if(total_checked>totaldownload){
// 				alert("You have remaining "+totaldownload+" download limit.");
// 				return false;
// 			}
			if (total_checked>0) {
		  
		    var i=0;
			$('.profile-content .empch:checkbox:checked').each(function(){
				
                        eid+=$(this).next().find('.employeeid').val()+',';
                  
			});
			window.location.href="<?= Url::toRoute(['site/exportcandidate'])?>?Eid="+eid;
			}
			else
			{
					alert("Please check the checkbox to download ");
			}
        }
  </script>