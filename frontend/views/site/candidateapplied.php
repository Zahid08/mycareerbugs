<?php
$this->title = 'Applied Candidate';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use common\models\Plan;
$plan=new Plan();
$allplan=$plan->getAllplan();
use dosamigos\tinymce\TinyMce;
use common\components\MyHelpers;

use common\models\PlanAssign;
$planassign=new PlanAssign();
$isassign=$planassign->isAssign(Yii::$app->session['Employerid']);
?>


<div class="wrapper"><!-- start main wrapper -->

		 
		<div class="inner_page">
			<div class="container">
					  
					 
					 <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"  id="mobile_design">
					 	 <h4> Applied Candidate</h4>
					 	 <a data-toggle="modal" data-target="#new_post_plan" type="button" class="create_p" style="display:none">Purchase Post plan</a>
					 	 <div class="clear"></div>
					 	 <br>
		<style>
.create_p{background: #f16b22;color: #fff;padding: 10px;width: 190px;float: right;z-index: 99;margin: -40px 6px 0 0;font-size: 14px;text-align: center;border-radius: 100px;}
		
#top_fr{color:#fff}
#top_fr .app_select {border-right:1px solid #fff} 
#top_fr .applied_jobs {border-right:1px solid #fff}
#top_fr .applied_location {border-right:1px solid #fff}
#top_fr .applied_created {border-right:1px solid #fff}
#top_fr .applied_candidate {border-right:1px solid #fff}
#top_fr .applied_view {border-right:1px solid #fff}
#top_fr .applied_sponsored{border-right:1px solid #fff;line-height:40px; height:auto; padding: 0 0 0 10px;}


.applied_jobs_main{clear:both; overflow:hidden; color:#000; font-size:12px;}
.app_select{width:6%;float:left; line-height:40px; border-right:1px solid #000;line-height:40px;padding-left:10px;}
.applied_jobs{text-decoration:underline;font-weight:bold; width:23%;float:left; line-height:40px; border-right:1px solid #000; padding-left:10px;}
.applied_location{width:18%;float:left;line-height:40px; border-right:1px solid #000; padding-left:10px;}
.applied_created{width:10%;float:left;line-height:40px; border-right:1px solid #000; padding-left:10px;}
.applied_candidate{width:10%;float:left;line-height:40px; border-right:1px solid #000; padding-left:10px;}
.applied_view{width:8%;float:left;line-height:40px; border-right:1px solid #000; padding-left:10px;}
.applied_status{width:9%;float:right;line-height:40px;   padding-left:10px;}

.applied_sponsored{width: 16%;float: left;height: 40px;border-right:1px solid #000;padding-left: 10px;line-height: 14px; text-decoration:underline;padding: 6px 0 0 10px;}

.job_action{background:#f16b22; color:#fff; padding:5px 10px; text-align:center; float:right; display:none;}
.inner_page{padding:40px 0 60px 0;}
</style>

<div class="applied_jobs_main" style="background:#333333;" id="top_fr">
	<div class="app_select"> Select</div>
	<div class="applied_jobs">Job Title</div>
	<div class="applied_location">Location</div>
	<div class="applied_created">Created</div>
	<div class="applied_candidate">Candidate</div>
	<div class="applied_view">Views</div>
	<div class="applied_sponsored">Budget</div>
	<div class="applied_status">Status</div>
</div>
<?php $count = 1; foreach($allpost as $jobpost){ ?>
 
 
<?php 
if($jobpost['JobStatus']  ==0)
						{
								$status='Open';
								$background='green';
						}
						else
						{
								$status='Closed';
								$background='red';
						}

if($jobpost['PostFor']  =='Campus'){$pf=1;}else{$pf=0;}?>

 

<div class="applied_jobs_main" style="border:1px solid #bbbbbb">
	<div class="app_select"><?php echo $count; ?></div>
	<div class="applied_jobs"> <a target="_blank" href="<?= Url::toRoute(['site/candidateapplied','JobId'=>$jobpost['JobId'] ])?>"> <?php echo  $jobpost['JobTitle'] ;?></a> </div>
	<div class="applied_location"> <?php echo  $jobpost['Location']   ;?></div>
	<div class="applied_created"> <?php echo date('Y-m-d', strtotime( $jobpost['UpdatedDate']  ));?></div>
	<div class="applied_candidate"> <a target="_blank" href="<?= Url::toRoute(['site/candidateapplied','JobId'=>$jobpost['JobId']  ])?>"> <?php echo MyHelpers::applpiedcondidate($jobpost['JobId'] ); ?> Candidates</a> </div>
	<div class="applied_view"> <?php echo $jobpost['TotalView']   ;?> Views </div>
	<div class="applied_sponsored"><a data-toggle="modal" data-target="#myModal_buy_a_plan" style="cursor: pointer;"  type="button">Purchase plan to view more candidate </a></div>
	<div class="applied_status"> 
	<!--	<div class="" style="background: <?=$background;?>">-->
	<div class="" style="background:#fff">
		<p><?=$status;?></p>
	</div>
	<a style="display:none" href="<?= Url::toRoute(['site/jobedit','JobId'=>$jobpost['JobId'] ,'pf'=>$pf])?>">Edit</a>
	</div>
	
	<a class="job_action" href="<?= Url::toRoute(['site/candidateapplied','JobId'=>$jobpost['JobId']  ])?>">View Applied</a>
	
</div>
<?php $count++; } ?>
 

 
 			<p style="margin:20px 0 0 0; txet-align:center">By default you can only view 10 candidate on per individual post after that you have to purchase our plan</p> 
				
				
				
								
			 <!-- Modal -->
  <div class="modal fade" id="myModal_buy_a_plan" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Purchase plan for this Post </h4>
        </div> 				  
									   									   <div class="widget_inner" style="width:100%; float:none">
											   <div class="panel price panel-red">
												<div class="panel-body text-center">
													<p class="lead"><strong>   Rs/- 50</strong></p>
												</div> 
												<ul class="list-group list-group-flush text-center width-half"> 
											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Unlimited SMS</li> 
												<li class="list-group-item"><i class="icon-ok text-danger"></i>Unlimited Email </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i>Unlimited CV </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i>Unlimited Download </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i>Unlimited Candidate View </li> 
												</ul>
												<div class="panel-footer">
													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
													<p style="text-align:center; font-size:14px;  margin: 13px 0 0 0;">Contact us: 8240369924</p></p>
												</div>
											</div> 
									   <!-- /PRICE ITEM -->
								       </div>
									     
			   </div>  
			   </div>
		 	</div>
			<!-- Modal -->			
				
				
				
				
				 <!-- Modal -->
  <div class="modal fade" id="new_post_plan" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Purchase Job Post Plan</h4>
        </div> 				  
									   									   <div class="widget_inner" style="width:100%; float:none; border: 0; margin: 0;">
											   <div class="panel price panel-red">
												<div class="panel-body text-center">
													<p class="lead"><strong>   Rs/- 300</strong></p>
												</div> 
												<ul class="list-group list-group-flush text-center width-half">  
												<li class="list-group-item"  style="width:100%"><i class="icon-ok text-danger"></i> 4 Job Post / month </li> 
												</ul>
												<div class="panel-footer">
													<a class="request-plan btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
													<p style="text-align:center; font-size:14px;    margin: 13px 0 0 0;">Contact us: 8240369924</p></p>
												</div>
											</div> 
									   <!-- /PRICE ITEM -->
								       </div>
									     
			   </div>  
			   </div>
		 	</div>
			<!-- Modal -->			
				
				
					<!--  <div class="pannel_header margin_top">
					 
				
					    <div class="width-10" id="select_per_page">	
                            <div class="form-groups">
							<div class="col-sm-8">
						      <label>Result Per page </label>
							 </div>
						<div class="col-sm-4">
							<?php
							if(isset($_GET['perpage']))
							{
							$perpage=$_GET['perpage'];
							}else{$perpage=10;}
							?>
                              <select id="form-filter-location" data-minimum-results-for-search="Infinity" class="form-control23 select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="getresultperpage(this.value,'hirecandidate');">
								<option value="10" <?php if($perpage==10) echo "selected='selected'";?>>10</option>
                                <option value="20" <?php if($perpage==20) echo "selected='selected'";?>>  20  </option>
                                <option value="30" <?php if($perpage==30) echo "selected='selected'";?>>  30 </option>
                                <option value="40" <?php if($perpage==40) echo "selected='selected'";?>>  40  </option>
								<option value="50" <?php if($perpage==50) echo "selected='selected'";?>>  50  </option> 
                              </select> 
                            </div>
							 </div>
                          </div>
					 </div> --> 
					 
				<?php
				if(isset($appliedjob) && !empty($appliedjob)){
				foreach($appliedjob as $key=>$value)
				{
										if($value->user->photo)
										{
											$doc=$url.$value->user->photo->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
				$availableviewcontact=$isassign->ViewContact-$isassign->UseViewContact;
				?>
					 <?php if(count($isassign)>0 && $availableviewcontact>0) { ?><a href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>" onclick="viewcontact();" target="_blank" >Veiw Contact</a><?php } ?>
					 <div class="profile-content payment_list">
                                <div class="card">
                                    <div class="firstinfo">
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive">
                                        <div class="profileinfo">
										
                                            <h1> <?=$value->user->Name;?></h1>
											<?php
											if($value->user->experiences)
											{
												$positionv=$value->user->experiences[0]->position->Position;
											?>
											<small><?=$value->user->experiences[0]->position->Position;?>  from <?=$value->user->City;?>, <?=$value->user->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->user->City;?>, <?=$value->user->State;?></small>
											<?php
											}
											?>
											
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->user->experiences)
														{
														?>
														<li>Experience: <?=$value->user->experiences[0]->Experience;?> Years   </li>
														<li>Company:  <?=$value->user->experiences[0]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->user->experiences)
														{
														?>
													    <li> Current Salary <?=$value->user->experiences[0]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->user->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->user->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->user->empRelatedSkills)
													{
													foreach($value->user->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <div style="color:#f16b22; font-size:13px;">Job Applied: <?=date('d M, Y H:i', strtotime($value->UpdatedDate));?></div>
													 <p>Last Updated: <?=date('d M, Y H:i', strtotime($value->job->UpdatedDate));?></p>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p><br/>
													 <p class="last_update_main">Job Title :<b> <?=$value->job->JobTitle;?></b></p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
										<?php
										$availablecv=$isassign->CVDownload-$isassign->UseCVDownload;
										if($value->user->CVId!=0 && count($isassign)>0 && $availablecv>0)
										{
												$na=explode(" ",$value->user->Name);
												$name=$na[0];
												$extar=explode(".",$value->user->cV->Doc);
												$ext=$extar[1];
										?>
									  	<a href="<?=$url.$value->user->cV->Doc;?>" onclick="cvdownload();"  download="<?=$name.'.'.$ext;?>" class="btn-default"> <i class="fa fa-download"></i>  CV  </a>
										<?php
										}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#myModal_buy_a_plan"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->TotalEmail-$isassign->UseEmail;
										
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->user->Email;?>');" <?php }else{?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										  <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->user->UserId])?>" target="_blank" onclick="viewcontact();" <?php }else{ ?>data-toggle="modal" data-target="#myModal_buy_a_plan"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div></a>
							
							
					<!--profile-content-->
					<?php
				}
			}
				?>
				
			 <!-- Modal -->
  <div class="modal fade" id="myModal_buy_a_plan" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Buy a Plan</h4>
        </div>
									    <?php
		 if($allplan)
		 {
			foreach($allplan as $key=>$pland)
			{
				?>
									   <div class="widget_inner">
											   <div class="panel price panel-red">
												<div class="panel-body text-center">
													<p class="lead" ><strong><?=$pland->PlanName;?></strong></p>
												</div> 
												<ul class="list-group list-group-flush text-center width-half"> 
											   <li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
											   <li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
												<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li> 
												</ul>
												<div class="panel-footer">
													<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
												</div>
											</div>
									   <!-- /PRICE ITEM -->
								       </div>
									   <?php
			}
		 }
		 ?>  
			   </div>  
			   </div>
		 	</div>
			<!-- Modal -->		 				

							<?php
							if($pages>0){
								echo LinkPager::widget([
									'pagination' => $pages,
								]);
							}
							?>
                        </div>
      </div>

		    </div>
		<div class="border"></div>
		</div><!-- end main wrapper -->
		
		
		
		
<!-----myModal_email------>
<div class="modal fade" id="myModal_email" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mail To Candidate</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<div class="form-group" style="display: none;">
		<?= $form->field($mailmodel, 'EmployeeId')->textInput(['id'=>'EmployeeId','maxlength' => true])->label(false) ?>
		</div>
		<div class="form-group">
		<?= $form->field($mailmodel, 'Subject')->textInput(['maxlength' => true,'Placeholder'=>'Subject'])->label(false) ?>
		</div>
								  <div class="col-md-12 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Description*  </label>
										
								<?= $form->field($mailmodel, 'MailText')->widget(TinyMce::className(), [
									'options' => ['rows' => 6],'class'=>'form-control textarea-small',
									'language' => 'en_CA',
									'clientOptions' => [
										'plugins' => [
											"advlist autolink lists link charmap print preview anchor",
											"searchreplace visualblocks code fullscreen",
											"insertdatetime media table  paste spellchecker"
										],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								])->label(false);?>
                              </div> 
							   
							  </div>
							  
							  <div class="col-md-12 col-sm-12 col-xs-12">
							
									<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green']) ?>
								</div>
								</div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
			   </div>  
			   </div>
		 	</div>
<!-----myModal_email------>
<script type="text/javascript">
		function mailtosemp(id,email) {
			$('#EmployeeId').val(email);
			$('#'+id).modal('show');
        }
		function cvdownload() {
            $.ajax({url:"<?= Url::toRoute(['site/cvdownload'])?>",
				   success:function(results)
				   {
					
				   }
			});
        }
		
		function viewcontact() {
            $.ajax({url:"<?= Url::toRoute(['site/contactview'])?>",
				   success:function(results)
				   {
					
				   }
			});
        }
        $(document).on('click', '.request-plan', function() {
        $.ajax({
    		url: "<?= Url::toRoute(['site/jobplanrequst'])?>",
    		type : 'GET',
    		success: function(result){
    				$('#new_post_plan').modal('hide');
  			}
  		});
    	});
</script>