<?php
$this->title = 'Appliedcandidate - mycareerbugs.com';


use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use common\models\AllUser;
use common\models\Plan;
use common\models\PlanAssign; 
use common\models\PostJob;


$daily_CV = Yii::$app->myfunctions->GetTodayPlan('CVDownload',$isassign->plan->PlanId,date("Y-m-d"));
$daily_whatsApp = Yii::$app->myfunctions->GetTodayPlan('whatsappview',$isassign->plan->PlanId,date("Y-m-d"));
$daily_Email = Yii::$app->myfunctions->GetTodayPlan('Email',$isassign->plan->PlanId,date("Y-m-d"));

$daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView',$isassign->plan->PlanId,date("Y-m-d"));
$email_limit_reached   = 'myModal_buy_a_plan';
$cv_limit_reached      = 'myModal_buy_a_plan';
$wapp_limit_reached    = 'myModal_buy_a_plan';
$contact_limit_reached = 'myModal_buy_a_plan';
if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $isassign->plan->total_daily_email<=$daily_Email) {
	$email_limit_reached="daily_limit_reach";
}
if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $isassign->plan->total_daily_cv_download<=$daily_CV) {
	$cv_limit_reached="daily_limit_reach";;
}
if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $isassign->plan->daily_whatsapptotal<=$daily_whatsApp) {
	$wapp_limit_reached="daily_limit_reach";
}
if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $isassign->plan->total_daily_view_contact<=$daily_Contact) {
	$contact_limit_reached="daily_limit_reach";
}


$education='';$latest='';$gender=array();$salaryrange=array();
$daily_Download = Yii::$app->myfunctions->GetTodayPlan('Download',$isassign->plan->PlanId,date("Y-m-d"));
$daily_Download = $isassign->plan->daily_download-$daily_Download;
//$daily_CV = Yii::$app->myfunctions->GetTodayPlan('CVDownload',$isassign->plan->PlanId,date("Y-m-d"));
//$daily_whatsApp = Yii::$app->myfunctions->GetTodayPlan('whatsappview',$isassign->plan->PlanId,date("Y-m-d"));
$daily_Email = Yii::$app->myfunctions->GetTodayPlan('Email',$isassign->plan->PlanId,date("Y-m-d"));
$daily_Email = $isassign->plan->total_daily_email-$daily_Email;
//$daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView',$isassign->plan->PlanId,date("Y-m-d"));
	$email_limit_reached="myModal_buy_a_plan";
	$download_limit_reached="myModal_buy_a_plan";
if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $isassign->plan->total_daily_email<=$daily_Email) {
	$email_limit_reached="daily_limit_reach";
}
if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $isassign->plan->daily_download<=$daily_Download) {
	$download_limit_reached="daily_limit_reach";
}

$JobId=isset($_REQUEST['JobId'])?$_REQUEST['JobId']:'';
$viewLimit=0;
if($JobId){
     $postJob = PostJob::findOne($JobId);
    $viewLimit=$postJob->viewLimit;
}
?>


<meta name="keywords" content="My Career bugs, MCB, Hire Candidates, Hire Employee, Candidate search" /> 

<meta name="description" content="Find the best Candidate from the exhaustive talent pool at MCB. Reach out to potential hires in one click!" /> 

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<div class="wrapper"><!-- start main wrapper -->
	<style>
	    a.btn-default.viewdProfile {
    background: #ebebeb;
    color: black;
}
	</style>
	
		 
		<div class="inner_page" id="pad_sml">
			<div class="container">
		
                   	<div class="col-lg-9  col-md-9 col-sm-9 col-xs-12">
                   		<div class="pannel_header margin_top">
					 		<div class="width-13">
								<div class="checkbox">
							    <label> <input type="checkbox" id="empchk">
								<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> 
							    </label>
							  	</div>					     
							</div>
							
							<div class="width-14" data-toggle="modal" data-target="#myModal_buy_a_plan" style="display:none">
							 	<div class="checkbox">
							    	<label> 
								  	Sms
							    	</label>
							  	</div>					     
							</div>
						  	<div class="width-14">
							 	<div class="checkbox" <?php if(isset(Yii::$app->session['Employerid']) && $daily_Email>0) { ?>onclick="if($('.empch').prop('checked')==true){mailtoemp('myModal_email');}else{alert('Please check the checkbox to send mail');}" <?php }else{ ?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php }?>>
							    <label>  
								Mail
							    </label>
							  </div>	
							</div>
							<div class="width-14">
							 	<div class="checkbox" <?php if(isset(Yii::$app->session['Employerid']) && $daily_Download>0) { ?>onclick="if($('.empch').prop('checked')==true){download();}else{alert('Please check the checkbox to download');}" <?php }else{ ?>data-toggle="modal" data-target="#<?=$download_limit_reached?>"<?php }?>>
							    <label> 
								  Download  
							    </label>
							  </div>
							</div>
						<div class="width-10" id="select_per_page">	
                            <div class="form-groups">
							<div class="col-sm-8">
						      <label>Result Per page </label>
							 </div>
							<div class="col-sm-4">
							<select id="form-filter-perpage" data-minimum-results-for-search="Infinity" class="form-control23" tabindex="-1" aria-hidden="true">
								<option value="10">10</option>
                                <option value="20">  20  </option>
                                <option value="30">  30 </option>
                                <option value="40">  40  </option>
								<option value="50">  50  </option> 
                              </select> 
                            </div>
							</div>
                        </div>
						
						<div class="pannel_header" style="margin: 0px;">
							<span id="searchdivc" style="margin-left: 10px;font-size: 14px;color: #f16b22;"></span>
						</div>
						
					</div>
					
						<?php 
                
                    	if($viewLimit>0){ ?>
                    	  <div id="w0-error-0" class="alert alert-info fade in">Basic Plan - Your search profile limit is:  10 / <?php echo $viewLimit; ?></div>  
                     <?php	}else{ ?>
                          <div id="w0-error-0" class="alert-danger alert fade in">Your basic plan is expired, Please contact <Span style="background:#fff; color:red;margin: 0 7px 0 3px;">+91 8240369924 </Span> to renew your plan</div>  
                    <?php }?> 

                   		<div id="mobile_design">
                   			<?php
							
								if(!empty($appliedjob)){
                   				if(isset($appliedjob[0]['job']) && isset($appliedjob[0]['job']['JobTitle'])){
                   					echo "<h4>".$appliedjob[0]['job']['JobTitle']."</h4>";
                   				}
								$i = 0; 
								foreach($appliedjob as $key => $value) {
								
								    if($oldJsonViewdData){
                                       $viewProfileArray = explode(',', $oldJsonViewdData);
                                       if (in_array($value->UserId, $viewProfileArray)){
                                        $previousSeenProfile=1;
                                       }
                                    }
                                    
                                    $className='';
                                    if($previousSeenProfile){$className="viewdProfile";}
                                    
                            
								$i++;
								 $value = AllUser::Find()->where(['UserId' => $value->UserId])->one();
								 $value1 = AllUser::Find()->where(['UserId' => Yii::$app->session['Employerid']])->one();
								 
								  $value1->viewlimit=$viewLimit;
								  
							
								if(isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']))
					{
										if($value->photo)
										{
											$doc=$url.$value->photo->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
					}
					else
					{
						$doc=$imageurl.'images/user.png';
					}
                    if(isset($_GET['stype']) && $_GET['stype']=='experience' && !empty($salaryrange))
                    {
						if($experience!='' && $role=='')
						{
						$ex=explode('-',$experience);
						$exp=$ex[1];
                        $cnt=count($value->experiences)-1;
                        if(in_array($value->experiences[$cnt]->Salary,$salaryrange) && $exp==$value->getTotalexp($value->UserId))
                        {
                        ?>
                        <div class="profile-content payment_list">
                                <div class="card" >
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
									
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB Cadndiate Photo">
                                        <div class="profileinfo">
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{
												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[$cnt]->position->Position;
											?>
											<small><?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[$cnt]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <div style="color:#f16b22; font-size:13px;">Job Applied: <?=date('d M, Y H:i', strtotime($value->UpdatedDate));?></div>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									<div class="contact_me">
									<?php 
									$availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno; ?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php } ?>
																		 
									<?php
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($value->CVId!=0 && isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath)){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
						}}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId,'JobId'=>$JobId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default <?=$className?>" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							
							
					<!--profile-content-->
                        <?php
						$cn++;
                        }
						}
						elseif(in_array($value->experiences[$cnt]->Salary,$salaryrange))
                        {
                        ?>
                        <div class="profile-content payment_list">
                                <div class="card" >
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
									
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB candidate Photo">
                                        <div class="profileinfo">
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{
												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[$cnt]->position->Position;
											?>
											<small><?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[$cnt]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <div style="color:#f16b22; font-size:13px;">Job Applied: <?=date('d M, Y H:i', strtotime($value->UpdatedDate));?></div>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
									 <?php 
									 $availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno;?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php }
									?>
									 
									  	<?php
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($value->CVId!=0 && isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath)){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId,'JobId'=>$JobId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default <?=$className?>" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							
							
					<!--profile-content-->
                        <?php
						$cn++;
                        }
					}
                    else
                    {
						if($experience!='' && $role=='')
						{
						$ex=explode('-',$experience);
						$exp=$ex[1];
                        if($exp==$value->getTotalexp($value->UserId))
                        {
				?>
					 <div class="profile-content payment_list">
                                <div class="card">
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
									
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB candidate Photo">
                                        <div class="profileinfo">
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{

												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[$cnt]->position->Position;
											?>
											<small> Current Position : <?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[$cnt]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <div style="color:#f16b22; font-size:13px;">Job Applied: <?=date('d M, Y H:i', strtotime($value->UpdatedDate));?></div>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
									 <?php 
									 $availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno;?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php }
									?>
									 
									  	<?php
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($value->CVId!=0 && isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath)){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId,'JobId'=>$JobId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default <?=$className?>" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							
							
					<!--profile-content-->
					<?php
					$cn++;
						}
						}
						else
						{
							$cn++;
							?>
							
							<div class="profile-content payment_list">
								<!--onclick="if($(this).find('.empch').prop('checked')==true){$(this).find('.empch').prop('checked',false);}else{$(this).find('.empch').prop('checked',true);}"-->
                                <div class="card" >
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB candidate Photo">
										
                                        <div class="profileinfo">
											
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{
												
												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[0]->position->Position;
											?>
											<small> Current Position : <?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[0]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?= $value->educations[0]->course->name;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <div style="color:#f16b22; font-size:13px;">Job Applied: <?=date('d M, Y H:i', strtotime($value->UpdatedDate));?></div>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
									<?php 
									$availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno;?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php }
									?>
									 
									  	<?php
										//if(isset(Yii::$app->session['Employerid'])){
										
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath) && $value->cV->Doc!=''){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}else{
											?>
									  	<a href="<?= Url::toRoute(['content/mycareerbugcv','UserId'=>$value->UserId,'JobId'=>$JobId])?>" onclick="cvdownload('<?=$value->UserId?>');" target="_blank" class="btn-default <?=$className?>" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}}
										else
										{
										?>
										<a href="javascript:void(0);" data-toggle="modal" data-target="#<?=$cv_limit_reached?>" class="btn-default" type="button" > <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && !empty($isassign) && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId,'JobId'=>$JobId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default <?=$className?>" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
										<?php //} ?>
									 </div>
									 
                                </div>
                            </div>
							
							
					<!--profile-content-->
						<?php
						}
                    }
					
					
				}
								
								
						foreach($appliedjob as $key => $value) { 
						 $value = AllUser::Find()->where(['UserId' => $value->UserId])->one();
						 
						}	 
								if($value1->viewlimit  <  $i && $value1->viewlimit != $i )
								 { ?>
							 
							 <!--<a href="#" class="btn btn-primary">Request Views</a>-->
							 
							 <?php
							 
								 }
							 
                   			} else{?>
                   				No Candidate Found.
                   			<?php } ?>
						</div>
					</div>
      	</div>
	</div>
		


		<div class="border"></div>
		</div><!-- end main wrapper -->
		
		
<!-----myModal_email------>
<div class="modal fade" id="myModal_email" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Mail To Candidate</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<div class="form-group" style="display: none;">
		<?= $form->field($mailmodel, 'EmployeeId')->textInput(['id'=>'EmployeeId','maxlength' => true])->label(false) ?>
		</div>
		<div class="form-group">
		<?= $form->field($mailmodel, 'Subject')->textInput(['maxlength' => true,'Placeholder'=>'Subject'])->label(false) ?>
		</div>
								  <div class="col-md-12 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Description*  </label>
										
								<?= $form->field($mailmodel, 'MailText')->widget(TinyMce::className(), [
									'options' => ['rows' => 6],'class'=>'form-control textarea-small',
									'language' => 'en_CA',
									'clientOptions' => [
										'plugins' => [
											"advlist autolink lists link charmap print preview anchor",
											"searchreplace visualblocks code fullscreen",
											"insertdatetime media table  paste spellchecker"
										],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								])->label(false);?>
                              </div> 
							   
							  </div>
							  
							  <div class="col-md-12 col-sm-12 col-xs-12">
							
									<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green']) ?>
								</div>
								</div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
			   </div>  
			   </div>
		 	</div>
<!-----myModal_email------>


		
<script  >

	$(document).on('change', '#empchk', function() {
		if($(this).prop("checked")){
			$('.empch').prop('checked',true);
		}else{
			$('.empch').prop('checked',false);
		}
	});
	
	 	
		function mailtoemp(id) {
			var email='';
			var UserId = '';
			 
			$('.profile-content .empch:checkbox:checked').each(function(){
				email+=$(this).next().find('.empid').val()+'|';
			});
			$('.profile-content .empch:checkbox:checked').each(function(){
				UserId+=$(this).next().find('.employeeid').val()+'|';
			});
			$('#EmployeeId').val(email);
			$('#MailUserId').val(UserId);
			$('#'+id).modal('show');
        }
	function download() {
            var eid='';
			var totaldownload=<?php echo (isset(Yii::$app->session['Employerid']))?$daily_Download:0;?>;
			var total_checked = $('.profile-content .empch:checkbox:checked').length;
			if(total_checked>totaldownload){
				alert("You have remaining "+totaldownload+" download limit.");
				return false;
			}
			if (totaldownload>0) {
		  
		    var i=0;
			$('.profile-content .empch:checkbox:checked').each(function(){
					i++;
					if (i<=totaldownload) {
                        eid+=$(this).next().find('.employeeid').val()+',';
                    }
			});
			window.location.href="<?= Url::toRoute(['site/exportcandidate'])?>?Eid="+eid;
			}
			else
			{
					alert("Your data plan is already used");
			}
        }
 
 
</script>
<style type="text/css">
.black_overlay{
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
</style>
