<?php
$this->title = 'Appliedcandidate - mycareerbugs.com';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
use common\models\AllUser;
use common\models\Plan;


use common\models\PlanAssign;
//echo "<pre>";print_r($appliedjob);die();


$daily_CV = Yii::$app->myfunctions->GetTodayPlan('CVDownload',$isassign->plan->PlanId,date("Y-m-d"));
$daily_whatsApp = Yii::$app->myfunctions->GetTodayPlan('whatsappview',$isassign->plan->PlanId,date("Y-m-d"));
$daily_Email = Yii::$app->myfunctions->GetTodayPlan('Email',$isassign->plan->PlanId,date("Y-m-d"));

$daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView',$isassign->plan->PlanId,date("Y-m-d"));
$email_limit_reached   = 'myModal_buy_a_plan';
$cv_limit_reached      = 'myModal_buy_a_plan';
$wapp_limit_reached    = 'myModal_buy_a_plan';
$contact_limit_reached = 'myModal_buy_a_plan';
if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $isassign->plan->total_daily_email<=$daily_Email) {
	$email_limit_reached="daily_limit_reach";
}
if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $isassign->plan->total_daily_cv_download<=$daily_CV) {
	$cv_limit_reached="daily_limit_reach";;
}
if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $isassign->plan->daily_whatsapptotal<=$daily_whatsApp) {
	$wapp_limit_reached="daily_limit_reach";
}
if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $isassign->plan->total_daily_view_contact<=$daily_Contact) {
	$contact_limit_reached="daily_limit_reach";
}


?>


<meta name="keywords" content="My Career bugs, MCB, Hire Candidates, Hire Employee, Candidate search" /> 

<meta name="description" content="Find the best Candidate from the exhaustive talent pool at MCB. Reach out to potential hires in one click!" /> 

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<div class="wrapper"><!-- start main wrapper -->
	
	
		 
		<div class="inner_page" id="pad_sml">
			<div class="container">
		
                   	<div class="col-lg-9  col-md-9 col-sm-9 col-xs-12">
                   		<div class="pannel_header margin_top">
					 		<div class="width-13">
								<div class="checkbox">
							    <label> <input type="checkbox" id="empchk">
								<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> 
							    </label>
							  	</div>					     
							</div>
							
							<div class="width-14" data-toggle="modal" data-target="#myModal_buy_a_plan" style="display:none">
							 	<div class="checkbox">
							    	<label> 
								  	Sms
							    	</label>
							  	</div>					     
							</div>
						  	<div class="width-14">
							 	<div class="checkbox" <?php if(isset(Yii::$app->session['Employerid']) && $daily_Email>0) { ?>onclick="if($('.empch').prop('checked')==true){mailtoemp('myModal_email');}else{alert('Please check the checkbox to send mail');}" <?php }else{ ?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php }?>>
							    <label>  
								Mail
							    </label>
							  </div>	
							</div>
							<div class="width-14">
							 	<div class="checkbox" <?php if(isset(Yii::$app->session['Employerid']) && $daily_Download>0) { ?>onclick="if($('.empch').prop('checked')==true){download();}else{alert('Please check the checkbox to download');}" <?php }else{ ?>data-toggle="modal" data-target="#<?=$download_limit_reached?>"<?php }?>>
							    <label> 
								  Download  
							    </label>
							  </div>
							</div>
						<div class="width-10" id="select_per_page">	
                            <div class="form-groups">
							<div class="col-sm-8">
						      <label>Result Per page </label>
							 </div>
							<div class="col-sm-4">
							<select id="form-filter-perpage" data-minimum-results-for-search="Infinity" class="form-control23" tabindex="-1" aria-hidden="true">
								<option value="10">10</option>
                                <option value="20">  20  </option>
                                <option value="30">  30 </option>
                                <option value="40">  40  </option>
								<option value="50">  50  </option> 
                              </select> 
                            </div>
							</div>
                        </div>
						
						<div class="pannel_header" style="margin: 0px;">
							<span id="searchdivc" style="margin-left: 10px;font-size: 14px;color: #f16b22;"></span>
						</div>
						
					</div>
                   		<div id="mobile_design">
                   			<?php if(!empty($appliedjob)){
                   				if(isset($appliedjob[0]['job']) && isset($appliedjob[0]['job']['JobTitle'])){
                   					echo "<h4>".$appliedjob[0]['job']['JobTitle']."</h4>";
                   				}
                   				foreach ($appliedjob as $key => $value) { 
								
								$value = AllUser::Find()->where(['UserId' => $value->UserId])->one();
								if(isset(Yii::$app->session['Employerid']) || isset(Yii::$app->session['Employeeid']))
					{
										if($value->photo)
										{
											$doc=$url.$value->photo->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
					}
					else
					{
						$doc=$imageurl.'images/user.png';
					}
                    if(isset($_GET['stype']) && $_GET['stype']=='experience' && !empty($salaryrange))
                    {
						if($experience!='' && $role=='')
						{
						$ex=explode('-',$experience);
						$exp=$ex[1];
                        $cnt=count($value->experiences)-1;
                        if(in_array($value->experiences[$cnt]->Salary,$salaryrange) && $exp==$value->getTotalexp($value->UserId))
                        {
                        ?>
                        <div class="profile-content payment_list">
                                <div class="card" >
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
									
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB Cadndiate Photo">
                                        <div class="profileinfo">
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{
												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[$cnt]->position->Position;
											?>
											<small><?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[$cnt]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									<div class="contact_me">
									<?php 
									$availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno; ?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php } ?>
																		 
									<?php
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($value->CVId!=0 && isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath)){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
						}}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							
							
					<!--profile-content-->
                        <?php
						$cn++;
                        }
						}
						elseif(in_array($value->experiences[$cnt]->Salary,$salaryrange))
                        {
                        ?>
                        <div class="profile-content payment_list">
                                <div class="card" >
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
									
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB candidate Photo">
                                        <div class="profileinfo">
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{
												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[$cnt]->position->Position;
											?>
											<small><?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[$cnt]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
									 <?php 
									 $availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno;?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php }
									?>
									 
									  	<?php
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($value->CVId!=0 && isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath)){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							
							
					<!--profile-content-->
                        <?php
						$cn++;
                        }
					}
                    else
                    {
						if($experience!='' && $role=='')
						{
						$ex=explode('-',$experience);
						$exp=$ex[1];
                        if($exp==$value->getTotalexp($value->UserId))
                        {
				?>
					 <div class="profile-content payment_list">
                                <div class="card">
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
									
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB candidate Photo">
                                        <div class="profileinfo">
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{

												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[$cnt]->position->Position;
											?>
											<small> Current Position : <?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[$cnt]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
									 <?php 
									 $availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno;?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php }
									?>
									 
									  	<?php
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($value->CVId!=0 && isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath)){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}}
										else
										{
										?>
										<a href="" class="btn-default" type="button" data-toggle="modal" data-target="#<?=$cv_limit_reached?>"> <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
									 </div>
                                </div>
                            </div>
							
							
					<!--profile-content-->
					<?php
					$cn++;
						}
						}
						else
						{
							$cn++;
							?>
							
							<div class="profile-content payment_list">
								<!--onclick="if($(this).find('.empch').prop('checked')==true){$(this).find('.empch').prop('checked',false);}else{$(this).find('.empch').prop('checked',true);}"-->
                                <div class="card" >
									<input type="checkbox" style="position:absolute;z-index:999;" class="empch" value="">
                                    <div class="firstinfo">
									<input type="hidden" class="empid" value="<?=$value->Email;?>" />
									<input type="hidden" class="employeeid" value="<?=$value->UserId;?>" />
                                    	<img src="<?=$doc;?>" alt="" class="img-circle img-responsive" alt="MCB candidate Photo">
										
                                        <div class="profileinfo">
											
                                            <h1> <?=$value->Name;?></h1>
											<?php
											if($value->experiences)
											{
												
												$cnt=count($value->experiences)-1;
												$positionv=$value->experiences[0]->position->Position;
											?>
											<small> Current Position : <?=$positionv;?>  from <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											else{
											?>
											<small>From <?=$value->City;?>, <?=$value->State;?></small>
											<?php
											}
											?>
											<div class="spcae1"></div>
											
											<div class="row">
											 <div class="col-md-12 col-sm-12 col-xs-12">
											     <div class="col-md-6 col-sm-12 col-xs-12"> 
             										<ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
														<li>Total Experience: <?=$value->getTotalexp($value->UserId);?> Years   </li>
														<li>Company:  <?=$value->experiences[0]->CompanyName;?> </li>
														<?php
														}
														?>
														
													  </ul>
                                                </div>
												 
												   <div class="col-md-6 col-sm-12 col-xs-12"> 
													  <ul class="commpany_desc">
														<?php
														if($value->experiences)
														{
															$cnt=count($value->experiences)-1;
														?>
													    <li> Current Salary <?=$value->experiences[$cnt]->Salary;?> Lakh   </li>
														<?php
														}
														if($value->educations)
														{
															?>
														 <li> Last Qualification: <?=$value->educations[0]->course->CourseName;?>   </li>
														 <?php
														}
														?>
													   </ul>
                                                   </div> 
												 </div>
											</div>
												
												 
												   <div class="profile-skills">
													<?php
													if($value->empRelatedSkills)
													{
													foreach($value->empRelatedSkills as $ask=>$asv)
												{
													?>
													<span> <?=$asv->skill->Skill;?> </span>
													<?php
												}
													}
												?>
														
													</div> 
													
													 <p class="last_update">Last updated <?=date('d-m-Y',strtotime($value->UpdatedDate));?></p>
													 <p class="last_update_main" style="display: none;">Shortlisted by 2 Recruiters recently</p>
										</div>
                                    </div>
		 
									 <div class="contact_me">
									<?php 
									$availableviewwapp=$isassign->plan->daily_whatsapptotal-$daily_whatsApp;
									if(!empty($value->whatsappno)){
										$whatsappno = "https://api.whatsapp.com/send?phone=".$value->whatsappno;?>
										<a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewwapp>0) { ?>href="<?=$whatsappno?>" target="_blank" onclick="viewwhatsapp('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$wapp_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  WhatsApp   </a>
									<?php }
									?>
									 
									  	<?php
										//if(isset(Yii::$app->session['Employerid'])){
										
										$availablecv=$isassign->plan->total_daily_cv_download-$daily_CV;
										if($availablecv>0)
										{
											$na=explode(" ",$mval->Name);
												  $name=$na[0];
												  $extar=explode(".",$mval->cV->Doc);
												  $ext=$extar[1];
											$filepath = $_SERVER['DOCUMENT_ROOT'].'/backend/web/'.$value->cV->Doc;
											if(file_exists($filepath) && $value->cV->Doc!=''){
										?>
									  	<a href="<?=$url.$value->cV->Doc;?>" onclick="cvdownload('<?=$value->UserId?>');" download="<?=$name.'.'.$ext;?>"  class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}else{
											?>
									  	<a href="<?= Url::toRoute(['content/mycareerbugcv','UserId'=>$value->UserId])?>" onclick="cvdownload('<?=$value->UserId?>');" target="_blank" class="btn-default" > <i class="fa fa-download"></i>  CV  </a>
										<?php
										}}
										else
										{
										?>
										<a href="javascript:void(0);" data-toggle="modal" data-target="#<?=$cv_limit_reached?>" class="btn-default" type="button" > <i class="fa fa-download"></i> CV   </a>
										<?php
										}
										
										$availableemail=$isassign->plan->total_daily_email-$daily_Email;
										$availableviewcontact=$isassign->plan->total_daily_view_contact-$daily_Contact;
										?>
                                        <a class="btn-default" type="button" <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableemail>0) { ?>onclick="mailtosemp('myModal_email','<?=$value->Email;?>','<?=$value->UserId?>');" <?php }else{?>data-toggle="modal" data-target="#<?=$email_limit_reached?>"<?php } ?> > <i class="fa fa-phone"></i>   Email </a>
										
										 <a <?php if(isset(Yii::$app->session['Employerid']) && count($isassign)>0 && $availableviewcontact>0) { ?>href="<?= Url::toRoute(['site/customerdetail','UserId'=>$value->UserId])?>" target="_blank" onclick="viewcontact('<?=$value->UserId?>');" <?php }else{ ?>data-toggle="modal" data-target="#<?=$contact_limit_reached?>"<?php } ?> class="btn-default" type="button" > <i class="fa fa-envelope-o"></i>  View Contact   </a>
										<?php //} ?>
									 </div>
									 
                                </div>
                            </div>
							
							
					<!--profile-content-->
						<?php
						}
                    }
				}
								
								
								
							 
                   			} else{?>
                   				No Candidate Found.
                   			<?php } ?>
						</div>
					</div>
      	</div>
	</div>
		


		<div class="border"></div>
		</div><!-- end main wrapper -->
		
<script  >

	$(document).on('change', '#empchk', function() {
		if($(this).prop("checked")){
			$('.empch').prop('checked',true);
		}else{
			$('.empch').prop('checked',false);
		}
	});
	
	
	var xhr;
		setTimeout(function(){
			$("#state").prepend("<option value='' selected='selected'>Select State</option>");
				<?php
				if(isset($_GET['state']) && $_GET['stype']=='experience')
								{
										?>
				var state='<?=$_GET['state'];?>';
				$('#state').val(state);
				<?php
								}
								?>
		},2000);
		
		function mailtoemp(id) {
			var email='';
			var UserId = '';
			var totalmail = <?php echo (isset(Yii::$app->session['Employerid']))?$daily_Email:0;?>;
			var total_checked = $('.profile-content .empch:checkbox:checked').length;
			if(total_checked>totalmail){
				alert("You have remaining "+totalmail+" mail limit.");
				return false;
			}
			$('.profile-content .empch:checkbox:checked').each(function(){
				email+=$(this).next().find('.empid').val()+'|';
			});
			$('.profile-content .empch:checkbox:checked').each(function(){
				UserId+=$(this).next().find('.employeeid').val()+'|';
			});
			$('#EmployeeId').val(email);
			$('#MailUserId').val(UserId);
			$('#'+id).modal('show');
        }

		function mailtosemp(id,email,UserId) {
			$('#EmployeeId').val(email);
			$('#MailUserId').val(UserId);
			$('#'+id).modal('show');
			var page = $('ul.pagination li.active a').data('page');
    		candidatefilterNew((page+1));
        }
		
		function download() {
            var eid='';
			var totaldownload=<?php echo (isset(Yii::$app->session['Employerid']))?$daily_Download:0;?>;
			var total_checked = $('.profile-content .empch:checkbox:checked').length;
			if(total_checked>totaldownload){
				alert("You have remaining "+totaldownload+" download limit.");
				return false;
			}
			if (totaldownload>0) {
		  
		    var i=0;
			$('.profile-content .empch:checkbox:checked').each(function(){
					i++;
					if (i<=totaldownload) {
                        eid+=$(this).next().find('.employeeid').val()+',';
                    }
			});
			window.location.href="<?= Url::toRoute(['site/exportcandidate'])?>?Eid="+eid;
			}
			else
			{
					alert("Your data plan is already used");
			}
        }
		
		function cvdownload(UserId) {
            $.ajax({url:"<?= Url::toRoute(['site/cvdownload'])?>?UserId="+UserId,
				   success:function(results)
				   {
					var page = $('ul.pagination li.active a').data('page');
    				candidatefilterNew((page+1));
				   }
			});
        }
		
		function viewcontact(UserId) {
            $.ajax({url:"<?= Url::toRoute(['site/contactview'])?>?UserId="+UserId,
				   success:function(results)
				   {
					var page = $('ul.pagination li.active a').data('page');
    				candidatefilterNew((page+1));
				   }
			});
        }
        function viewwhatsapp(UserId) {
            $.ajax({url:"<?= Url::toRoute(['site/whatsappview'])?>?UserId="+UserId,
				   success:function(results)
				   {
					var page = $('ul.pagination li.active a').data('page');
    				candidatefilterNew((page+1));
				   }
			});
        }
		
		/*$(document).ready(function(){
			$('.multiselect-ui').multiselect({
				includeSelectAllOption: true
			});
		});*/
	function candidatefilterNew(page) {
		var dis_searchresult = "";
   		var education=$('#fceducation').val();
   		if(education!=''){
   			dis_searchresult+=' > '+$('#fceducation option:selected').text();
   		}
    	var latest=$('#fcfreshness').val();
    	if(latest!=''){
   			dis_searchresult+=' > '+$('#fcfreshness option:selected').text();
   		}
    	var gender=$('.cfgender:checked').map(function () {
						return this.value;
					}).get();
    	if(gender!=''){
    		dis_searchresult+=' > Gender:'+gender;
   		}
    	var cresume=$('.cresume:checked').map(function () {
						return this.value;
					}).get();
    	if(cresume!=''){
   			dis_searchresult+=' > with resume';
   		}
     	
     	var condidateage=$('#condidateage').val();
     	if(condidateage!=''){
   			dis_searchresult+=' > Age:'+$('#condidateage option:selected').text();
   		}
     	
	 	var worktype=$('#worktype').val();
	 	if(worktype!=''){
   			dis_searchresult+=' > '+$('#worktype option:selected').text();
   		}
	  	var PreferredLocation=$('#PreferredLocation').val();
	  	if(PreferredLocation!=''){
   			dis_searchresult+=' > '+$('#PreferredLocation option:selected').text();
   		}
	 	var crole  =  $('#crole').val();
	 	if(crole!=''){
   			dis_searchresult+=' > '+$('#crole option:selected').text();
   		}
   		var skill = $('.cskillby:checked').map(function () {
						return this.value;
					}).get();
     	if(skill!=''){
     		var skillss = $('.cskillby:checked').map(function() {
			  	return $(this).parents('.checkbox').find('a').html();
		  	}).get();
   			dis_searchresult+=' > '+skillss;
   		}
	 	var course = $('#education-course').val();
	 	if(course!=''){
   			dis_searchresult+=' > '+$('#education-course option:selected').text();
   		}
	 	var specialization = $('#education-specialization').val();
	 	if(specialization!=''){
   			dis_searchresult+=' > '+$('#education-specialization option:selected').text();
   		}
	 	var whatsapp_contact = $('#whatsapp_contact').val();
	 	if(whatsapp_contact!=''){
   			dis_searchresult+=' > WhatsApp';
   		}
   		var language='';
     	var languages=$('#LanguageId').val();
     	if(languages!=null){
     		language = languages.join(",");
     		dis_searchresult+=' > '+language;
     	}
	 	var perpage = $('#form-filter-perpage').val();
	 	var url="<?=Url::toRoute(['site/hirecandidate'])?>?education="+education+"&latest="+latest+"&gender="+gender+"&cresume="+cresume+"&skill="+skill+"&role="+crole+"&condidateage="+condidateage+"&worktype="+worktype+"&PreferredLocation="+PreferredLocation+"&course="+course+"&specialization"+specialization+"&language="+language+"&whatsapp_contact="+whatsapp_contact+"&perpage="+perpage;
	 	
	 	if($('input[name=serchtype]:checked').val()=='Experience'){
	 		var cexperience=$('#cexperience').val();
			if(cexperience!=''){
	 			dis_searchresult+=' > Experience:'+$('#cexperience option:selected').text();
   			}
   			var industry = $('.cindustryby:checked').map(function () {
						return this.value;
					}).get();
			if(industry!=''){
	 			var sss = $('.cindustryby:checked').map(function() {
			  		return $(this).parents('.checkbox').find('a').html();
		  		}).get();
   				dis_searchresult+=' > '+sss;
   			}
	 		var salary = $('.csalary:checked').map(function () {
						return this.value;
					}).get();

	 		if(salary!=''){
	 			var ss = $('.csalary:checked').map(function() {
			  		return this.value+" lakh";
		  		}).get();
   				dis_searchresult+=' > '+ss;
   			}
	 		url+="&salaryrange="+salary+"&industry="+industry+"&experience="+cexperience+"&stype=experience";
	 	}else{
	 		url+="&stype=fresher";
	 	}
    	if(page!=0){
    		url+="&page="+page;
    	}
    	if(xhr){
            xhr.abort();
        }
    	xhr = $.ajax({
    		url: url,
    		type : 'GET',
    		success: function(result){
    			$("#mobile_design").html(result);
    			if(dis_searchresult!=''){
    				dis_searchresult = 'Results > '+$.trim(dis_searchresult).substr(1);
    			}
    				$("#searchdivc").html(dis_searchresult);
    				
  			}
  		});
	}

	$(document).on('change', '#fceducation', function() {
		if($(this).val()!=''){
			$.ajax({
            type: "POST",
            url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
            data: {'id': $(this).val()},
            success : function(response) {
            	var data = JSON.parse(response);
            	$("#naukari-course option").remove();
            	$("#naukari-board option").remove();
            	$.each(data, function( index, value ) {
  					$("#education-course").append('<option value="'+index+'">'+value+'</option>');
				});
        	}, 
        	})
		}else{
			var selectList = $("#education-course option");
			selectList.find("option:gt(0)").remove();
		}
		
		candidatefilterNew(0);
	});
	$(document).on('change', '#form-filter-perpage', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#education-course', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#LanguageId', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#education-specialization', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#fcfreshness', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#condidateage', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#worktype', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#PreferredLocation', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#crole', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '.cfgender', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '.cresume', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '.cskillby', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#whatsapp_contact', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#cexperience', function() {
		candidatefilterNew(0);
	});
	$('input[class=csalary]').change(function(){
		candidatefilterNew(0);
	});
	$('input[class=cindustryby]').change(function(){
		candidatefilterNew(0);
	});
	$('input[name=serchtype]').change(function(){
		candidatefilterNew(0);
			
	});
	var data = [{id: '',text: 'Specialization'}];
	var Ldata = [{id: '',text: 'Select Language'}];
	var Locdata = [{id: '',text: 'Select Location'}];
	<?php foreach ($naukari_specialization as $key => $value) { ?>
		var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
		data.push(d);
	<?php } ?>
	<?php foreach ($language_opts as $key1 => $value1) { ?>
		var ld = {id: '<?=$value1->name?>',text: '<?=$value1->name?>'};
		Ldata.push(ld);
	<?php }  ?>
	$("#education-specialization").select2({
  		data: data
	});
	$("#LanguageId").select2({
  		data: Ldata,
  		maximumSelectionLength: 5
	});
	
	<?php foreach($cities as $city){ ?>
		var lld = {id: '<?=$city->CityId?>',text: '<?=$city->CityName?>'};
		Locdata.push(lld);
	<?php } ?>
	$("#PreferredLocation").select2({
  		data: Locdata
	});
	$(document).on('click', 'ul.pagination li a', function(event) {
		event.preventDefault();
    	var page = $(this).data('page');
    	candidatefilterNew((page+1))
	});


	function selectpositionnew(pid){
		if($('input[name=serchtype]:checked').val()=='Experience'){
			var type = 'ecandidate';
		}else{
			var type = 'candidate';
		}
		if(pid!=''){
			$.ajax({url:mainurl+"site/skills?roleid="+pid+"&type="+type,
               success:function(results)
            {
                $('#jobbyskill').html(results);
            }
        	});
		}else{
			$('#jobbyskill').html('');
		}
	    
	}
</script>
<style type="text/css">
.black_overlay{
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index:1001;
	-moz-opacity: 0.8;
	opacity:.80;
	filter: alpha(opacity=80);
}
</style>
