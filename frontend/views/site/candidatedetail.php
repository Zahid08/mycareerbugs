<?php
$this->title = 'Profile';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Documents;
use common\models\Skill;
use common\models\EmployeeSkill;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

$url = '/backend/web/';

$pimage = Documents::getImageByAttr($candidate, 'PhotoId', 'photo')?>

<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">

<div id="wrapper">
	<!-- start main wrapper -->
	<?php 
		//echo '<pre>';
		//print_r(;
	?>
	<div class="inner_page second">

		<div class="container">
			<div id="profile-desc">
				<div id="margin-top-profile-minus">
					<div class="profile_block_main">
						<div class="profile_block_left">
							<img src="<?=$pimage;?>" alt=""
								class="img-responsive center-block ">
						</div>
						<div class="profile_block_right">
							<h3 class="title_heading"><?=$candidate->Name;?></h3>
							<div class="clear"></div>
							<div class="profilehalf">
								<p> <strong>Phone: </strong> +91 <?=$candidate->MobileNo;?> </p>
								<p> <strong>Email:</strong> <?=$candidate->Email;?>  
								<p><strong>Languages:</strong>  <?=$candidate->language;?> </p>
							</div>

							<div class="profilehalf">
								<p><strong>WhatsApp:</strong> +91  <?=$candidate->whatsappno;?></p>
								<p><strong>Gender:</strong>  <?=$candidate->Gender;?> </p>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="profile_block_main">
						<h3 class="title_heading">Prefered Job Location</h3>
						<p><?=$candidate->getPreferedJobLocation();?> </p>
					</div>





					<div class="profile_block_main">
						<h3 class="title_heading">About Us</h3>
						<?php if($candidate->EntryType == 'Team'){?>
						<p> <?=nl2br($candidate->TeamDesc);?></p>
						<?php }else{?>
						<p> <?=nl2br($candidate->AboutYou);?></p>
						<?php }?>
					</div>

					<div class="profile_block_main">
						<h3 class="title_heading">Address</h3>
						<p><strong> Address:</strong> <?=$candidate->Address;?>  </p>
						<div class="profilehalf">
							<p><strong> City:</strong> <?=$candidate->City;?>  </p>
							<p><strong>State:</strong> <?=$candidate->State;?>  </p>
						</div>

						<div class="profilehalf">
							<p><strong>Country:</strong> <?=$candidate->Country;?>  </p>
							<p><strong>Pincode:</strong> <?=$candidate->PinCode;?> </p>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<div class="clearfix"></div>
				<div class="profile_block_main">
					<h3 class="title_heading">
						Educational Information <span style="display: none;"><?=($candidate->educations[0]->DurationTo);?></span>
					</h3>



					<div class="resume-box" style="margin-top: 15px;"
						id="new_profile_design">

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">
									<h4>Languages</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4><?=$candidate->language;?></h4>
								</div>
							</div>
						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">
									<h4>Pass out Year</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4><?=$candidate->educations[0]->DurationTo;?></h4>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Higest Qualification</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4> <?=$candidate->educations[0]->nqual;?> </h4>
								</div>
							</div>
						</div>
						
						<?php $board = array(4,5,6);?>
						
						<?php if(in_array($candidate->educations[0]->highest_qual_id, $board)){?>
						
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Board</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4> <?=$candidate->educations[0]->nboard;?>  </h4>
								</div>
							</div>
						</div>
						
						<?php }else{?>
						
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Course</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4> <?=$candidate->educations[0]->ncourse;?> </h4>
								</div>
							</div>
						</div>
						
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>University / College</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4><?=$candidate->educations[0]->University;?></h4>
								</div>
							</div>
						</div>
						
						<?php }?>
						
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Specialization</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4> <?=$candidate->educations[0]->specialization_id;?> </h4>
								</div>
							</div>
						</div>

						<div class="row education-box" style="display: none;">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Course</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4><?=$candidate->educations[0]->course->name;?></h4>
								</div>
							</div>
						</div>
						
						
						<?php if($candidate->CollarType == "white"){?>
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Job Category</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<?php $CategoryIds1 = ArrayHelper::map(UserJobCategory::find()->where(['user_id' => $candidate->UserId])->all(), 'category', 'category');
									
									 $CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $candidate->UserId
																])->all(), 'category_id', 'category_id');
									  $expCategory = ArrayHelper::map(WhiteCategory::find()->where([
												'id' => $CategoryIds
											])->all(), 'name', 'name');
									$jobCategory = array_merge($CategoryIds1, $expCategory);
									
									?>
									<h4><?php echo (!empty($jobCategory)?implode(', ',$jobCategory):"");?></h4>
								</div>
							</div>
						</div>
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Role</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<?php $userRole1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $candidate->UserId
																	])->all(), 'role', 'role');
										$expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $candidate->UserId,
																	])->all(), 'role', 'role');
										 $userRole = array_merge($userRole1, $expRoles);
																	?>
									<h4><?php echo (!empty($userRole)?implode(', ',$userRole):"");?></h4>
								</div>
							</div>
						</div>


						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Skills</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">
												<?php $jobSkills1 = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $candidate->UserId
																	])->all(), 'skill_id', 'skill');
													 $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $candidate->UserId,
																	])->all(), 'skill', 'skill');
														$jobSkills = array_merge($jobSkills1, $expSkills);
																	?>
                                            <h4><?php echo $jobSkills ? implode(", ",$jobSkills) : "";?></h4>
											

								</div>

							</div>

						</div>
						
						<?php }else{?>
							
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Role</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<h4><?php echo $candidate->getEducationRoles();?></h4>
								</div>
							</div>
						</div>


						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Skills</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">
												<?php

									$skill = '';
									$skillArray = array();
									foreach ($candidate->empRelatedSkills as $ask => $asv) {
										$skillArray[$asv->skill->SkillId] = $asv->skill->Skill;
										//$skill .= $asv->skill->Skill . ', ';
									}

									//$skill = trim(trim($skill), ",");

									?>
                                            <h4><?php //echo $skillArray ? implode(", ",$skillArray) : "";?>
												 <?php 
														$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
															'IsDelete' => 0,
															'UserId' => $candidate->UserId,
														])->all(), 'Employeeskillid', 'SkillId');
														
														$skills = ArrayHelper::map(Skill::find()->where([
															'IsDelete' => 0,
															'SkillId' => $employeeSkill
														])->all(), 'SkillId', 'Skill');
														
														$blueSkill = array_merge($skillArray, $skills);
														$blueSkill = array_unique($blueSkill);
														if(!empty($blueSkill )){
															echo implode(', ',$blueSkill);
														}
													  ?>
											
											</h4>
											

								</div>

							</div>

						</div>

					
						<?php }?>
					</div>

				</div>



				<div class="clearfix"></div>

			</div>





			<div class="profile_block_main"> 	 
						 
				<?php

    if ($candidate->experiences) {

        foreach ($candidate->experiences as $k => $val) {

            ?>
				
				
				
				  <h3 class="title_heading">
					Work Experience <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span>
				</h3>











				<div class="resume-box" style="margin-top: 15px;"
					id="new_profile_design1">

					<div class="row education-boxs">



						<div class="col-md-12 col-sm-12 col-xs-12">



							<div class="row education-box">



								<div class="col-md-4 col-xs-12 col-sm-4">

									<div class="resume-icon">

										<i class="fa fa-file-text" aria-hidden="true"></i>

									</div>

									<div class="insti-name">

										<h4>Company name</h4>



									</div>

								</div>

								<div class="col-xs-12 col-md-8 col-sm-8">

									<div class="degree-info">

										<h4><?=$val->CompanyName;?></h4>

									</div>

								</div>

							</div>

							<div class="row education-box">

								<div class="col-md-4 col-xs-12 col-sm-4">

									<div class="resume-icon">

										<i class="fa fa-files-o" aria-hidden="true"></i>

									</div>

									<div class="insti-name">

										<h4>Position</h4>

									</div>

								</div>

								<div class="col-xs-12 col-md-8 col-sm-8">

									<div class="degree-info">

										<h4><?=$val->PositionName;?></h4>

									</div>

								</div>

							</div>

							<div class="row education-box">

								<div class="col-md-4 col-xs-12 col-sm-4">

									<div class="resume-icon">

										<i class="fa fa-files-o" aria-hidden="true"></i>

									</div>

									<div class="insti-name">

										<h4>Industry</h4>

									</div>

								</div>

								<div class="col-xs-12 col-md-8 col-sm-8">

									<div class="degree-info">
                                           <?php

            if ($val->industry) {

                ?>
                                            <h4><?=$val->industry->IndustryName;?></h4>
											<?php
            }

            ?>
                                             </div>

								</div>

							</div>

							<div class="row education-box">

								<div class="col-md-4 col-xs-12 col-sm-4">

									<div class="resume-icon">

										<i class="fa fa-briefcase" aria-hidden="true"></i>

									</div>

									<div class="insti-name">
										<h4>Experience</h4>
									</div>
								</div>

								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
                                            <h4> <?=$val->Experience. ' Years';?></h4>
									</div>
								</div>
							</div>
							
							<?php if($candidate->CollarType == "white"){?>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Job Category</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<h4><?=$val->whitecategory->name;?></h4>

									</div>
								</div>
							</div>
							
							<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Role</h4>
								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<?php $expRole = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $candidate->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'role_id', 'role'); ?>
									<h4><?php echo (!empty($expRole)?implode(', ',$expRole):"");?></h4>
								</div>
							</div>
						</div>


						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Skills</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">
									<?php $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
															'user_id' => $candidate->UserId,
															'experience_id' => $val->ExperienceId
														])->all(), 'skill_id', 'skill');?>
								<h4><?php echo $expSkills ? implode(", ",$expSkills) : "N/A";?></h4>
											

								</div>

							</div>

						</div>
							
							<?php }else{?>
							
							
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Role</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<h4><?=$val->position->Position;?></h4>

									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Skills</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
									<?php 
										/*Get Experience Skill*/
										$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
											'IsDelete' => 0,
											'SkillRoleId' => $val->PositionId,
											'UserId' => $candidate->UserId,
											'ExperienceId' => $val->ExperienceId
										])->all(), 'Employeeskillid', 'SkillId');
										
										$skills = ArrayHelper::map(Skill::find()->where([
											'IsDelete' => 0,
											'SkillId' => $employeeSkill
										])->all(), 'SkillId', 'Skill');
										
									?>
										<h4><?php echo $skills ? implode(", ",$skills) : "";?></h4>

									</div>
								</div>
							</div>
							<?php } ?>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Salary</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<h4> <?=$val->Salary.' Lakh';?></h4>

									</div>
								</div>
							</div>
						</div>
					</div>
			  <?php
        }
    }
    ?>
			  </div>
			</div>
		</div>
		<div class="border"></div>
	</div>
</div>