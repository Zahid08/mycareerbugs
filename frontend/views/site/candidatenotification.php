<?php

$this->title = 'Notification';

use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

?>

<!-- start main wrapper --> 

	<div id="wrapper">

	

		<div class="recent-job"><!-- Start Recent Job -->

			<div class="container">

				<div class="row">

				  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

			 

			 <div class="col-lg-12 col-md-5 col-sm-5 col-xs-6">

						<h4><i class="glyphicon glyphicon-briefcase"></i> All Notification</h4>

					 </div>	 

					  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6" id="landing_select">

					 

 <div class="clearfix"></div>

		

		<?php

		if($allnotification)

		{

		foreach($allnotification as $nk1=>$jvalue)
		{
		?>

		<div class="item-list">

                <?php if(empty($jvalue->job->Slug)){ ?>
                 <a target="_blank"	href="<?= Url::base().'/wall-post/'.$jvalue->wallPost->Slug.'?Nid='.$jvalue->NotificationId;?>">
            <?php }else{?>
                  <a target="_blank" href="<?= Url::base().'/job/'.$jvalue->job->Slug.'?Nid='.$jvalue->UserId;?>">
                <?php }?>
              
                <div class="col-sm-12 add-desc-box">

                  <div class="add-details">

                    <h5 class="add-title"><?=!empty($jvalue->job->JobTitle)?$jvalue->job->JobTitle:$jvalue->wallPost->PostTitle?></h5>

					

                    <div class="info"> 

                      <span class="category"><?=!empty($jvalue->job->position->Position)?$jvalue->job->position->Position:$jvalue->wallPost->position->Position;?></span> -

                      <span class="item-location"><i class="fa fa-map-marker"></i> <?=!empty($jvalue->job->Location)?$jvalue->job->Location:$jvalue->wallPost->Locationat;?></span> <br>

                    <span> <strong><?=!empty($jvalue->job->CompanyName)?$jvalue->job->CompanyName:$jvalue->wallPost->industry->IndustryName;?></strong> </span>

					</div>

                    

					  <div class="info bottom">

					     <div class="col-sm-3 col-xs-3">

					    	<span class="styl">Keyskills : </span>  

					    </div>

						<div class="col-sm-9 col-xs-9 left-text">

							<span class="category">

								<?php
								
								$jskill=array();
								if(isset($jvalue->job->jobRelatedSkills) && !empty($jvalue->job->jobRelatedSkills)){
									foreach($jvalue->job->jobRelatedSkills as $k=>$v)
									{
										$jskill[]=$v->skill->Skill;

									}	
								}
								echo implode(',', $jskill);

								?>

							</span>  

                      </div>

					 </div>

					 

					   <div class="info bottom">

					    <div class="col-sm-3 col-xs-3">

					    	<span class="styl">Job Description : </span>  

					    </div>

						<div class="col-sm-9 col-xs-9 left-text">

							<span class="category"><?=!empty($jvalue->job->JobDescription)?substr(htmlspecialchars_decode($jvalue->job->JobDescription),0,150).'...': substr(htmlspecialchars_decode($jvalue->wallPost->Post),0,150).'...';?></span>  

                      </div>

					 </div>

					 

					  <div class="info bottom">

					    <div class="col-sm-3 col-xs-3">

					    	<span class="styl">Salary Range </span>  

					    </div>

						<div class="col-sm-9 col-xs-9 left-text">

							<span class="category"><?=$jvalue->job->Salary;?> Lakh</span>  

                      </div>

					 </div> 

					 

					<div class="info bottom"> 

						<span class="category" style="text-align:right"> <?=' ('.date('d M Y, h:i A',strtotime($jvalue->job->OnDate)).')';?></span> 

                    </div> 

                  </div>

                </div>

                </a>

              </div>

		<?php

		}

		}

        else

        {

		?>

        <h5 class="add-title">There is no notification</h5>

        <?php

        }

        ?>

					 

						<div class="spacer-2"></div>

					</div>

					 

                  </div>

					

					

					<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side">

							<div class="clearfix"></div>

						<div class="spacer-5"></div><div class="spacer-5"></div><div class="spacer-5"></div>

						

							<!--adban_block main -->

								<div class="adban_block">

							      <img src="<?=$imageurl;?>images/adban_block/ban.jpg"> 

							    </div>

						   <!-- adban_block  main -->

								

								

					</div>

					<div class="clearfix"></div>

				</div>

			</div>

		</div><!-- end Recent Job -->



</div><!-- end main wrapper -->



