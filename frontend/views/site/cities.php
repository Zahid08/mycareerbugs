<?php
$this->title = 'Cities';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<!-- start main wrapper --> 
	<div id="wrapper">
	 
	  	  <div class="container" id="company_list_pagination">
			
			 <div class="spacer-5"></div> 
			 
						<div class="mobile_pad">
                            
						    <div class="widget-heading"><span class="title"> Top jobs by City     </span></div>
						<div class="spacer-5"></div> 
				 
				 
						  <div class="top_employores" id="browse_city">
							    <ul> 
									 <li><a href="https://www.mycareerbugs.com/jobsearch?city=Bangalore"> <img class="normal" src="images/banglore_color.png"> <img class="hvr" src="images/banglore_white.png"> Jobs in Bangalore/Bengaluru</a> </li>
									 <li><a href="https://www.mycareerbugs.com/jobsearch?city=Delhi"> <img class="normal" src="images/delhi_orange.png"> <img class="hvr" src="images/delhi_white.png"> Jobs in Delhi </a></li>
									 <li><a href="https://www.mycareerbugs.com/jobsearch?city=Chennai"> <img class="normal" src="images/chennai_orange.png"> <img class="hvr" src="images/chennai_white.png"> Jobs in Chennai </a></li>
									 <li class="float-rt"><a href="https://www.mycareerbugs.com/jobsearch?city=Kolkata">  <img class="normal" src="images/kolkata_color.png"> <img class="hvr" src="images/kolkata_white.png">Jobs in Kolkata </a></li>
									 <li><a href="https://www.mycareerbugs.com/jobsearch?city=Hyderabad">  <img class="normal" src="images/hyderabad_color.png"> <img class="hvr" src="images/hyderabad_white.png">Jobs in Hyderabad</a></li> 
									 <li><a href="https://www.mycareerbugs.com/jobsearch?city=Pune">  <img class="normal" src="images/pune_color.png"> <img class="hvr" src="images/pune_white.png">Jobs in Pune</a></li>
									 <li><a href="https://www.mycareerbugs.com/jobsearch?city=Mumbai">  <img class="normal" src="images/mumbai_orange.png"> <img class="hvr" src="images/mumbai_white.png">Jobs in Mumbai</a></li>
							
								 </ul>
						  </div>
						 
						 <style>
 #browse_city ul li { margin: 0 0.5% 0.5% 0;}
#browse_city ul li {width: 13.8%;}
@media only screen and (max-width: 767px){
#browse_city ul li, #browse_city ul li.float-rt { width: 49% !important;}
}
</style>
						 
						 
		  </div>
		   
		    
						<div class="mobile_pad">
						    <div class="widget-heading"><span class="title"> All Cities     </span></div>
						<div class="spacer-5"></div> 
				 
						</div>
						
							  <div class="clear"></div>
							  
		      <?php
              if($allcity)
              {
				//echo '<pre>'; print_r($allcity);
                foreach($allcity as $k =>$name)
                {?>
				  <div class="col-md-2 col-sm-12 col-xs-12"> 
					<div class="company_show_logo">  <a href="<?= Url::toRoute(['site/jobsearch','city'=>$name])?>"><?=$name;?></a> </div> 	 
				 </div>
				 <?php
				  }
              }
              ?>
			</div>
		
		 <div class="spacer-5"></div> 

		<?php
						if(!isset(Yii::$app->session['Employeeid']) && !isset(Yii::$app->session['Employerid']) && !isset(Yii::$app->session['Campusid']) && !isset(Yii::$app->session['Teamid']))
						{
						?>
		<div class="advertise_your_post">
		    <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
		   	        <img src="<?=$imageurl;?>images/job_seekers.png">
					 
                    <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">  Register  </a>
				  </div>	

                            <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
				  <img src="<?=$imageurl;?>images/team_orange.png">
		   	          
                    <a href="<?= Url::toRoute(['team/index'])?>">Register  </a>
				  </div>	


		           <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
				  <img src="<?=$imageurl;?>images/company.png">
		   	          
                    <a href="<?= Url::toRoute(['site/employersregister'])?>">Register  </a>
				  </div>	
 

				       <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
				  	  <img src="<?=$imageurl;?>images/campus_register.png">
		   	         
                    <a href="<?= Url::toRoute(['campus/campusregister'])?>">Register  </a>
				  </div>	
		</div>
		<?php
						}
						?>