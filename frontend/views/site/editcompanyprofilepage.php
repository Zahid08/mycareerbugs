<?php
$this->title = 'Edit Company Profile Pages';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use bupy7\cropbox\CropboxWidget;
use common\models\States;
use common\models\Cities;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<style>
  .editcontainer{
    width: 100%;
    height: 30px;
    padding: 4px;
    float: left;
    cursor: pointer;
} 
.select-wrapper {
    background-size: cover;
    display: block;
    position: relative;
    width: 100%;
    height: 30px;
}
#image_src {
    width: 100%;
    height: 30px;
    
    opacity: 0;
    filter: alpha(opacity=0); /* IE 5-7 */
}
</style>
<?php
$filepath = $_SERVER['DOCUMENT_ROOT'].'/frontend/web/images/coverpic/'.$employer->coverpic;
if($employer->coverpic <> '' && file_exists($filepath)){
	?>
	<img class="full_width" style="width:100%; height:430px;" src="<?=$imageurl.'images/coverpic/'.$employer->coverpic;?>">
	<?php
}else{
?>
<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">
<?php } ?>
	<div id="wrapper"><!-- start main wrapper -->
	 
		 
		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
			<?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>		        
			   <div class="col-md-2 col-sm-2 col-xs-12">
			                 <div class="user-profile">
                                    <img src="<?php echo $url.$employer->logo->Doc ;?>" alt="<?php echo $employer->Name ;?>" class="img-responsive center-block ">
				    
                      <h3><input type="text" class="form-control" required name="AllUser[Name]" id="Name" value="<?php echo $employer->Name ;?>" placeholder="Name"></h3>
					  
					  <br/>
				   
				   <div class="input-group">
										Edit Photo <?php echo  $form->field($docmodel, 'Doc')->widget(CropboxWidget::className(), [
										    'croppedDataAttribute' => 'crop_info'
										])->label(false); ?>
									</div>
					  <div class="input-group">
								Cover Photo <input type="file" name="coverpic" class="form-control" accept="image/png,image/jpg,image/jpeg">
						</div>
					  
                                </div> 
								
				
			      	</div>
			 <div class="col-md-10 col-sm-10 col-xs-12"> 
                            <div class="job-short-detail" id="edit_profile_page">
                                <div class="heading-inner">
                                    <p class="title"> Company / Consultant details</p>
									<?= Html::submitButton('<i class="fa fa-floppy-o orange"></i> Save Changes') ?>
									
                                </div>
								
                                <dl>
                                     <?php if($employer->EntryType != 'HR'){ ?>
                                    <dt>Company Type</dt>
                                    <dd><select name="AllUser[IndustryId]" required class="form-control">
                    
										<option selected="selected" value="">- Select an Industry -</option>
										<?php foreach($industry as $key=>$value){?>
										<option value="<?php echo $key;?>" <?php if($key==$employer->IndustryId){ echo "selected"; } ?> ><?=$value;?></option>
										<?php } ?>
								</select>
				    </dd>
									 <?php } ?>


                                   <dt> <?php echo ($employer->EntryType == 'HR')?'Email':'Company Email';?> </dt>
                                   <dd><input type="text" class="form-control" readonly required name="AllUser[Email]" id="Email" value="<?php echo $employer->Email ;?>" placeholder="Enter your Email"></dd>
				   <dt>Address:</dt>
                                   <dd><input type="text" class="form-control" required name="AllUser[Address]" id="Address" value="<?php echo $employer->Address ;?>" placeholder="Address"></dd>
				   <dt> Mobile Number  </dt> 
				   <dd><input type="text" class="form-control" required name="AllUser[MobileNo]" id="MobileNo" onkeypress="return numbersonly(event)" onblur="return IsMobileno(this.value);" value="<?php echo $employer->MobileNo ;?>" placeholder="MobileNo"></dd>
				   <?php if($employer->EntryType != 'HR'){ ?>
				   <dt>  Contact Number </dt>
				   <dd><input type="text" class="form-control" name="AllUser[ContactNo]" id="ContactNo" value="<?php echo $employer->ContactNo ;?>" placeholder="ContactNo"></dd>
				   
				   <dt>  Contact Person </dt>
				   <dd><input type="text" class="form-control" name="AllUser[ContactPerson]" id="ContactPerson" value="<?php echo $employer->ContactPerson ;?>" required placeholder="ContactPerson"></dd>
				   <?php } ?>
                   <dt>  Country </dt>
				   <dd><input type="text" class="form-control" required name="AllUser[Country]" id="Country" value="<?php echo $employer->Country; ?>" placeholder="Country"></dd>
				   
                   <dt>  State </dt>
				   <dd>
				   <?php $stateList = States::getStateList();?>
				   <select style="width:100%" class="form-control" name="AllUser[State]" id="alluser-state" >
						<option value="">Select State</option>
						<?php foreach ($stateList as $stateId => $stateName) { ?>
						<option value="<?=$stateId;?>" <?php echo (($employer->State == $stateName)?"selected":"");?>><?=$stateName;?></option>
						<?php  } ?>
					</select>
				   <?php 
				   $state = $employer->State;
				   $city = $employer->City;
				   $stateId = States::findOne(['StateName' => $state])->StateId;?>
				   </dd>
				   
                   <dt>City </dt>
				   <dd>
					<?php 
					$cityList = array();
					if(!empty($stateId)){
						$cityList = Cities::getCityList($stateId);
					}?>
				    <select style="width:100%" class="form-control" name="AllUser[City]" id="alluser-city" >
						<option value="">Select City</option>
						
						<?php 
						if(!empty($cityList)){
							foreach ($cityList as $cityId => $cityName) { ?>
							<option value="<?=$cityId;?>" <?php echo (($employer->City == $cityName)?"selected":"");?>><?=$cityName;?></option>
							<?php  } 
						}?>
					</select>
				   </dd>
				   
                   <dt>Pincode:</dt>
                   
				   <dd><input type="text" class="form-control"  name="AllUser[PinCode]" value="<?php echo  $employer->PinCode; ?>"  id="email" placeholder="Pincode"></dd>
				 
				 <dt>Description:</dt>
                 
				 <dd><textarea type="text" class="form-control" rows="10" name="AllUser[CompanyDesc]" id="CompanyDesc" placeholder="Company Description"><?php echo  html_entity_decode($employer->CompanyDesc); ?></textarea></dd>
                 </dl>
                            </div>
 
                        </div>			 
	 <div class="clearfix"> </div>
	
						
				<div class="col-md-12 col-sm-12 col-xs-12">
 
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Company Description </p>
                                </div>
                                <div class="row education-box">
                                    
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="degree-info">
                                            <p><?php echo  html_entity_decode($employer->CompanyDesc); ?></p>
                                             </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
			<?php ActiveForm::end(); ?>
		<div class="clearfix"> </div>
  </div>
</div>
       </div>
		<div class="border"></div>
</div>
	
<script type="text/javascript">
	 $(document).ready(function(){
		 $(document).on('change','#alluser-state',function(e){
			var stateid = $(this).val();
			getCity(stateid);
		});
		 
	 });
	
	var baseUrl = "<?= Url::toRoute(['/'])?>";    
	function getCity(stateid){
    	$.get(baseUrl+'site/getcity?stateId='+stateid, function(res){
    		$('#alluser-city').empty();
    		$('#alluser-city').append(res.result);
    	});
    }
</script>

