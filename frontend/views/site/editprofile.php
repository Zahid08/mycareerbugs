<?php
$this->title = 'Edit Profile';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\LanguagesOpts;
use bupy7\cropbox\CropboxWidget;
use common\models\Skill;
use common\models\EmployeeSkill;
use common\models\WhiteRole;
use common\models\UserWhiteRole;
use common\models\UserWhiteSkill;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\NaukariQualData;

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
$language_opts = LanguagesOpts::find()->all();
//$language_ary = explode(',', $profile->language);
$language_ary = $profile->language;

?>
<style>
.editcontainer {
	width: 100%;
	height: 30px;
	padding: 4px;
	float: left;
	cursor: pointer;
}

.select-wrapper {
	background-size: cover;
	display: block;
	position: relative;
	width: 100%;
	height: 30px;
}

#image_src {
	width: 100%;
	height: 30px;
	opacity: 0;
	filter: alpha(opacity = 0); /* IE 5-7 */
}
.select2-container{width:100%!important;}
</style>
<link href="https://mycareerbugs.com/css/jquery.dropdown.css"
	rel="stylesheet">




<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">

<div id="wrapper">
	<!-- start main wrapper -->



	<div class="inner_page second">

		<div class="container">

			<div id="profile-desc">

				 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>

		

			   <div class="col-md-2 col-sm-2 col-xs-12">

					<div class="user-profile">

						<img src="<?=Yii::$app->session['EmployeeDP'];?>" alt=""
							class="img-responsive center-block " style="width:150px; height:150px">



						<h3>
							<input type="text" class="form-control" required
								name="AllUser[Name]" id="Name" value="<?=$profile->Name;?>"
								placeholder="Name">
						</h3>

						<br />



						<div class="input-group">

										Edit Photo

										<?php echo  $form->field($docmodel, 'Doc')->widget(CropboxWidget::className(), [
										    'croppedDataAttribute' => 'crop_info'									    
										])->label(false); ?>									
						</div>
					</div>
				</div>



				<div class="col-md-10 col-sm-10 col-xs-12">

					<div class="job-short-detail" id="edit_profile_page">

						<div class="heading-inner">

							<p class="title">Profile detail</p>

									<?= Html::submitButton('<i class="fa fa-floppy-o orange"></i> Save Changes') ?>

									

                                </div>

						<dl>



							<dt>Phone:</dt>

							<dd>

								<input type="text" class="form-control" name="AllUser[MobileNo]"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10"
									id="MobileNo" value="<?=$profile->MobileNo;?>" required>
							</dd>



							<dt>Email:</dt>

							<dd>
								<input type="email" class="form-control" name="AllUser[Email]"
									value="<?=$profile->Email;?>" id="email"
									placeholder="martine-aug234@domain.com" readonly>
							</dd>



							<dt>Gender:</dt>

							<dd>
								<input type="radio" name="AllUser[Gender]" value="Male"
									<?php if($profile->Gender=='Male') echo "checked";?> required />
								&nbsp; Male <input type="radio" name="AllUser[Gender]"
									value="Female"
									<?php if($profile->Gender=='Female') echo "checked";?> />
								&nbsp; Female
							</dd>



							<dt>Address:</dt>

							<dd>
								<input type="text" class="form-control" name="AllUser[Address]"
									id="address" value="<?=$profile->Address;?>"
									placeholder="234 Uptown new City Tower ">
							</dd>



							<dt>Pincode:</dt>

							<dd>
								<input type="text" class="form-control" name="AllUser[PinCode]"
									id="address" value="<?=$profile->PinCode;?>"
									onkeypress="return numbersonly(event)" maxlength="6">
							</dd>



							<dt>City</dt>

							<dd>
								<select class="questions-category cities form-control"
									tabindex="-1" aria-hidden="true" id="cityId"
									name="AllUser[City]" required>
									<option value="">Select City</option>
								</select>

							</dd>



							<dt>State</dt>

							<dd>

				   <?php

    $state = $profile->State;
    $city = $profile->City;
	
    ?>

				   <select class="questions-category states form-control"
									tabindex="-1" aria-hidden="true" id="stateId"
									name="AllUser[State]" required>

									<option value="">Select State</option>

								</select>

					<?php if($profile->CampusId!=0){ ?>

					<input type="hidden" name="PostJob[State]" value="<?=$state;?>" />

					<?php
    }

    ?>

				   </dd>



							<dt>Country:</dt>



							<dd>
								<input type="text" class="form-control" name="AllUser[Country]"
									id="country" placeholder="Somewere at Antarctica "
									value="<?=$profile->Country;?>">
							</dd>

							<dt>Prefered 5 Job Location:</dt>
							<dd>								
								<?php 
								$prefered_loc = array_map('trim',$prefered_loc);
								//echo '<pre>'; print_r($prefered_loc); die;?>
								<select id="naukari-location" placeholder="Select" name="AllUser[PreferredLocation][]" multiple >
									<?php 
										foreach($prefered_loc as $cityName){ ?>
										<option selected value="<?php echo $cityName; ?>"><?php echo $cityName; ?></option>
									<?php } ?>
								</select>
								
							</dd>

							<dt>About Me:</dt>
							<dd>
								<textarea name="AllUser[AboutYou]"
									value="<?=$profile->AboutYou;?>" class="form-control textarea"
									maxlength="250"
									onkeyup="$(this).next().html($(this).val().length +'- 250 character'); if($(this).val().length>250){$(this).next().css('color','red');}else{$(this).next().css('color','#333');}"><?=$profile->AboutYou;?></textarea>
								<p class="help-block">250 Characters</p>
							</dd>

						</dl>

					</div>



				</div>



				<div class="clearfix"></div>




				<div class="col-md-12 col-sm-12 col-xs-12 resume-box">

<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Language</h4>



								</div>

							</div>



							<div class="col-xs-12 col-md-8 col-sm-8">
								<select multiple="multiple"
									class="questions-category form-control" tabindex="0"
									aria-hidden="true" id="LanguageId" name="AllUser[Language][]">
								</select>

							</div>

						</div>
						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>WhatsApp No</h4>



								</div>

							</div>



							<div class="col-xs-12 col-md-8 col-sm-8">
								<input type="text" class="form-control"
									name="AllUser[WhatsAppNo]" id="WhatsAppNo"
									placeholder="Enter your WhatsApp No"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10"
									value="<?=$profile->whatsappno?>" />

							</div>

						</div>
						
						
				</div>



				<div class="clearfix"></div>

			
						
						
						



				<div class="col-md-12 col-sm-12 col-xs-12">



					<div class="resume-box">

						<div class="heading-inner">

							<p class="title">
								Educational Information <span style="display: none;"><?=($profile->educations[0]->PassingYear);?></span>
							</p>

						</div>

						<div class="row education-box" style="display: none;">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Higest Qualification</h4>



								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<input type="text" class="form-control"
										name="AllUser[HighestQualification]" id="hq"
										placeholder="Master of Business Administration"
										value="<?=$profile->educations[0]->HighestQualification;?>">

								</div>

							</div>

						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Pass out Year</h4>



								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">



									<select class="questions-category form-control"
										name="AllUser[PassingYear]" required>

										<option value="">Select Year</option>

										<?php

        for ($y = 1980; $y <= date('Y'); $y ++) 
        {

            ?>

										<option value="<?=$y;?>"
											<?php if($profile->educations[0]->PassingYear==$y) echo "selected";?>><?=$y;?></option>

										<?php
        }

        ?>

										 </select>

								</div>

							</div>

						</div>

						<?php $boardArr = array(4,5,6);?>
						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Higest Qualification</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select id="empcourse11"
										class="questions-category form-control"
										name="AllUser[HighestQualId]"
										<?php if($profile->CampusId!=0){ ?> disabled <?php } ?>
										required>

										<option value="">Select Higest Qualification</option>

										<?php
        foreach ($NaukariQualData as $key => $value) {
            ?>
										<option
											<?php if($profile->educations[0]->highest_qual_id==$value->id) echo "selected";?>
											value="<?=$value->id;?>"><?=$value->name;?></option>
										<?php
        }
        ?>

										</select>

										<?php if($profile->CampusId!=0){ ?>

										<!-- <input type="hidden" name="AllUser[CourseId]" value="<?=$profile->educations[0]->CourseId;?>" /> -->

										<?php
        }

        ?>

                                        </div>

							</div>

						</div>
						
					
						
						<div class="row education-box high-qual" style="<?=(in_array($profile->educations[0]->highest_qual_id, $boardArr)?'display:none':"");?>">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Course</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">
									<select class="questions-category form-control"
										id="naukari-course" name="AllUser[CourseId]">
									</select>
								</div>

							</div>

						</div>
						
						
						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Specialization</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select class="questions-category form-control"
										name="AllUser[Specialization]"
										<?php if($profile->CampusId!=0){ ?> disabled <?php } ?>
										required>

										<option value="">Select specialization</option>

										<?php
        foreach ($naukari_specialization as $key => $value) {
            ?>
										<option
											<?php if($profile->educations[0]->specialization_id==$value->name) echo "selected";?>
											value="<?=$value->name;?>"><?=$value->name;?></option>
										<?php
        }
        ?>

										</select>



								</div>

							</div>

						</div>
						

						<div class="row education-box high-qual" style="<?=(in_array($profile->educations[0]->highest_qual_id, $boardArr)?'display:none':"");?>">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>University / College</h4>



								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<input type="text" class="form-control"
										name="AllUser[University]" id="University"
										placeholder="University/College"
										value="<?=$profile->educations[0]->University;?>"
										<?php if($profile->CampusId!=0){ ?> disabled <?php } ?>
										/>

								</div>

							</div>

						</div>
						
						<div class="row education-box low-qual" style="<?=(in_array($profile->educations[0]->highest_qual_id, $boardArr)?'display:block':"display:none;");?>">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Board</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select class="questions-category form-control"
										name="AllUser[board_id]" id="board-list">

										<option value="">Select Board</option>

										<?php
										$boardList = array();
										if(in_array($profile->educations[0]->highest_qual_id, $boardArr)){
											$boardList = ArrayHelper::map(NaukariQualData::find()->where([
														'parent_id' => $profile->educations[0]->highest_qual_id
													])->all(), 'id', 'name');
										}										
										foreach ($boardList as $key => $name) {?>
										<option
											<?php if($profile->educations[0]->board_id==$key) echo "selected";?>
											value="<?=$key;?>"><?=$name;?></option>
										<?php
        }
        ?>

										</select>



								</div>

							</div>

						</div>
                    </div>
				</div>


 

 <style>
     #first_sec{padding:0; margin:0 0 15px 0; background:#f2f2f2;}
      #first_sec .page-head  { color:#fff; background:#6d136a; text-align:left;padding:10px 20px;}
    #blue-collar-options{padding:10px 20px;}
    #white-collar-options{padding:10px 20px;}
</style>

<div class="clear"></div>
<div class="clearfix"></div>


     <div id="first_sec">
					 	
						<h5 class="page-head">Interested Role and Skills</h5>   
                        
				<div class="col-md-6">  
				 
				
					<div class="form-group field-alluser-worktype" style=" margin-bottom:0px">
                       <label class="control-label" for="education-roleid">Job Category</label>
  
  
				 	<div class="panel-group" style="margin-bottom: 9px; background:none">
					<div class="panel panel-default" style="background:none ; box-shadow:none; border:0px">
						<div class="panel-heading sty" style="background:none !important; box-shadow:none; padding-top: 0;">
							<div class="col-md-6 no-pad-123">
								<a><h4 class="panel-title" style="font-size:12px;  font-weight: normal; color: #565656;">	<input type="radio" name="AllUser[CollarType]" class="collar-type" value="blue" <?=(($profile->CollarType !="white")? 'checked':""); ?>> Blue Collar  
								 <span class="checkmark"></span>
								 </h4></a>
							</div>

							<div class="col-md-6 no-pad-123">
								<a><h4 class="panel-title"  style="font-size:12px; font-weight: normal; color: #565656;"><input type="radio" name="AllUser[CollarType]" class="collar-type" value="white"  <?=(($profile->CollarType =="white")? 'checked':""); ?>>  White Collar  
								 <span class="checkmark"></span>
								 </h4> </a>
							</div>

						</div>
					</div>
				</div>
				</div>
					<div class="clear"></div>
						</div>
							<div class="clear"></div>
							
							<div id="blue-collar-options" style="display:<?=(($profile->CollarType !="white")? 'block':"none");?>;">
								<?php
                                $role_skill_ary = array();
                                $role_id = '';
                                foreach ($profile->empRelatedSkills as $ask => $asv) {
                                    
                                    if($asv->TypeId == 0){
                                        if ($asv->skill->position->PositionId && ! empty($asv->skill->position->PositionId)) {
                                            $role_id = $asv->skill->position->PositionId;
                                        }
                                        if (! empty($role_id)) {
                                            // break;
                                        }
    
                                        $role_skill_ary[$asv->SkillRoleId][] = array(
                                            $asv->skill->SkillId => $asv->skill->Skill
                                        );
                                    }
                                }
								if(!empty($role_skill_ary)){
                                ?>
                                    <?php foreach ($role_skill_ary as $key => $value) { ?>
									<div class="education-box role-container">
									<div class="row">
									<div class="col-md-2">  
										<label class="control-label"> Role</label> 
									</div>	
									<div class="col-md-4">  
										<select class="emp-role form-control"
										name="AllUser[SkillRoleId][]">
										<option value="">Select Role</option>
                                        <?php
                                        foreach ($position as $key1 => $value1) 
                                        { ?>
                                        <option
											value="<?=$value1->PositionId;?>"
											<?php if($value1->PositionId==$key) echo "selected";?>><?=$value1->Position;?></option>

                                        <?php
                                        }?>

                                        </select>
										</div>
									</div>
									<div class="clear"></div>
									<div class="row">
									<div class="col-md-2">  
										<label class="control-label"> Skills</label> 
									</div>
									<div class="col-md-10">	
										<div class="clearfix"></div>
											<div class="skill-profiles">
												<?php
											$skill = '';
											$skillid = '';
											$askill = '';
											foreach ($value as $ask2 => $asv2) {
												foreach ($asv2 as $k1 => $v2) {
													?>
													<?php if(!empty($v2)){ ?>
														<input
											type="checkbox" value="<?=$k1?>" name="AllUser[SkillId][<?=$key;?>][]"
											checked> <label><?=$v2?></label> <span>|</span>
													<?php } ?>
												<?php } ?>
											<?php } ?>
											</div>
										</div>
									</div>
									</div>
							<?php } }else{?>
								<div class="education-box role-container">
								<div class="row">
									<div class="col-md-2">
										<label class="control-label"> Role</label> 
									</div>
									<div class="col-md-4">
										<select class="emp-role questions-category form-control"
													name="AllUser[SkillRoleId][]">

													<option value="">Select Role</option>

													<?php
													foreach ($position as $key1 => $value1) 
													{

														?>

													<option
														value="<?=$value1->PositionId;?>"
														<?php if($value1->PositionId==$key) echo "selected";?>><?=$value1->Position;?></option>

													<?php
													}

													?>

										</select>
									</div>
								</div>
								
								<div class="clear"></div>
								<div class="row">
									<div class="col-md-2">  
										<label class="control-label"> Skills</label> 
									</div>
									<div class="col-md-10"> 
										<br/>
										<div class="skill-profiles">
													
										</div>
									</div>
								</div>	
								</div>
								<?php }?>		
                                <div class="form-group addrole-continer">
									<a href="javascript:;" onclick="admorerole();">+ Add More Role</a>
								</div>
						
				    		
							</div>
					  
				<div id="white-collar-options" style="display:<?=(($profile->CollarType =="white")? 'block':"none"); ?>;">
					<?php //echo '<pre>'; print_r($profile->whitejobcategory);?>
				
				<?php if(!empty($profile->whitejobcategory)){
						$k = 0;
						foreach($profile->whitejobcategory as $categoryData){?>
							<br class="clearfix"/>
								<div class="row">
									<div class="col-md-2">  
										<label class="control-label" style="width:100%"> Job Category</label> 
									</div>	
									<div class="col-md-4">  
										<select class="questions-category form-control" name="AllUser[Category][<?=$k;?>][WhiteCategory]" tabindex="<?=$k;?>" aria-hidden="true" id="white_callar_category">
										 <option value = "">Select Category</option>
										 <?php if(!empty($whiteCategories)){
												foreach($whiteCategories as $categoryId => $categoryName){?>
													<option value="<?php echo $categoryId?>" <?=(($categoryId == $categoryData->category_id)?"selected":"")?> ><?=$categoryName;?></option>
												<?php  }
												}?>
										 
										</select> 
									</div>
									<div class="col-md-2">  
										<label class="control-label"> Role</label> 
									</div>	
									<div class="col-md-4"> 
									<!--Get Category Role-->
									<?php $categoryRoleList = WhiteRole::getRoleList($categoryData->category_id);
										  $selectRoleList = UserWhiteRole::getCategoryRoleList($categoryData->category_id, $profile->UserId);
									?>
									<select class="questions-category form-control white_callar_role" name="AllUser[Category][<?=$k;?>][WhiteRole][]" id="white_callar_role<?=$k;?>" multiple="multiple">
										<?php if(!empty($categoryRoleList)){
											foreach($categoryRoleList as $roleId => $roleName){?>
												<option value="<?php echo $roleId?>" <?=(in_array($roleId, $selectRoleList)?"selected":"")?> ><?=$roleName;?></option>
											<?php  }
											}?>
									
									</select> 
									</div>
								</div>
						
					<?php 
					$countRole = $k;
					$k++; 
					
					}?>	
				<?php }else{
					$countRole = 0;
					?>
				
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-2">  
							<label class="control-label" style="width:100%"> Job Category</label> 
						</div>	
						<div class="col-md-4">  
							<select class="questions-category form-control" name="AllUser[Category][0][WhiteCategory]" tabindex="0" aria-hidden="true" id="white_callar_category">
							 <option value = "">Select Category</option>
							 <?php if(!empty($whiteCategories)){
									foreach($whiteCategories as $categoryId => $categoryName){?>
										<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
									<?php  }
									}?>
							 
							</select> 
						</div>
						<div class="col-md-2">  
							<label class="control-label"> Role</label> 
						</div>	
							<div class="col-md-4"> 
							<select class="questions-category form-control white_callar_role" name="AllUser[Category][0][WhiteRole][]" id="white_callar_role0" multiple="multiple"></select> 
							</div>
						</div>
						
						
						
			<?php }?>
					<div id="white-role-container">
								
					</div>
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-4">
							<a class="btn btn-info more_add_blck" count="<?=$countRole;?>" href="javascript:void(0);" id="add-white-role" style="float:left; padding:0px 0px 0px 0px; width: 109px !important; color: #f15b22; background: none;border: 0px;text-decoration: underline; margin:10px 0 10px 0"> + Add More Role </a>
						</div>
					</div>
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-2">  
							<label class="control-label"> Skills</label> 
						</div>	
						<div class="col-md-4">
							<?php $userWhiteskillData = UserWhiteSkill::getUserSkillList($profile->UserId);?>
							<select class="questions-category form-control" id="white_callar_skills" name="AllUser[WhiteSkills][]" multiple="multiple">
								<?php if(!empty($userWhiteskillData)){
									foreach($userWhiteskillData as $skillId => $skillName){?>
										<option value="<?php echo $skillId?>" selected ><?=$skillName;?></option>
									<?php  }
									}?>
							</select>
						</div>
					</div>
					
				</div>
				
	   	</div>
         <div class="clearfix"></div>
			
		 
			<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="resume-box">

						<div class="heading-inner">

							<p class="title">Resume</p>

									<?php

        if ($profile->cV) 
        {

            $na = explode(" ", $profile->Name);

            $name = $na[0];

            $extar = explode(".", $profile->cV->Doc);

            $ext = $extar[1];
        } else {
            $name = '';
            $ext = '';
        }

        ?>
		</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Change Resume</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-5 col-sm-5">

								<div class="degree-info">

									<input type="file" name="AllUser[CVId]"
										accept=".doc, .docx,.rtf,.pdf" id='cvid'>

									<p>doc,docx,pdf,rtf - 2MB max</p>

								</div>

							</div>
							<?php if ($profile->cV){?>
							<div class="col-xs-12 col-md-3 col-sm-3" id="delete-resume">
								<div class="degree-info">
								<i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;
								<?=($profile->CVId!=0)?'<a href="'.$url.$profile->cV->Doc.'" download="'.$name.'.'.$ext.'">'.$profile->Name.' Resume </a>':'';?>
								<a class="remove-resume" href="javascript:void(0);">
									&nbsp;&nbsp;<i style="color:red" class="fa fa-remove" aria-hidden="true"></i>
								</a>
								
								</div>
							</div>
							<?php }?>
						</div>

					</div>

				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">

                <?php
				$count = 0;
                if ($profile->experiences) 
                {
					
                    foreach ($profile->experiences as $k => $val) 
                    {
						$count++;
						

                        ?>

                            <div class="resume-box">

						<div class="heading-inner">

							<p class="title">
								Work Experience <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span>
							</p>

						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Company name</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<input type="hidden" name="AllUser[Experience][<?=$k;?>][ExperienceId]" value="<?=$val->ExperienceId;?>" /> 
										<input type="text"
										class="form-control" name="AllUser[Experience][<?=$k;?>][CompanyName]"
										id="CompanyName" placeholder="Company name"
										value="<?=$val->CompanyName;?>" />

								</div>

							</div>

						</div>
						
						<!--White Collar & Blue Collar Role-->
						<div class="exp-blue-collar" style="display:<?=(($profile->CollarType != "white")? 'block':"none"); ?>">
							
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>

								<div class="insti-name">

									<h4>Role</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select class="experience-role form-control"
										name="AllUser[Experience][<?=$k;?>][PositionId]" count="<?=$k;?>">

										<option value="">Select Role</option>

										<?php
						
                        foreach ($position as $key => $value) 
                        {
							
                            ?>

										<option value="<?=$value->PositionId;?>"
											<?php if($val->PositionId==$value->PositionId) echo "selected";?>><?=$value->Position;?></option>

										<?php
                        }

                        ?>

										</select>

								</div>
							</div>
							<?php 
								$roleId = $val->PositionId; 
								$experiencesId = $val->ExperienceId;
								$userId = $val->UserId;
								$allSkills = ArrayHelper::map(Skill::find()->where([
										'IsDelete' => 0,
										'PositionId' => $roleId
									])->all(), 'SkillId', 'Skill');
									
								$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
										'IsDelete' => 0,
										'SkillRoleId' => $roleId,
										'UserId' => $userId,
										'ExperienceId' => $experiencesId
									])->all(), 'Employeeskillid', 'SkillId');
							?>
							
							</div>
							
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">

    								<div class="resume-icon">
    
    									<i class="fa fa-file-text" aria-hidden="true"></i>
    
    								</div>
    
    								<div class="insti-name">
    									<h4>Skills</h4>
    								</div>
    
    							</div>
							
								<div class="col-xs-12 col-md-8 col-sm-8">
									
    								<div class="degree-info skill-profiles">
                                        <?php
                                            $skill = '';
                                            $skillid = '';
                                            $askill = '';
                                            foreach ($allSkills as $skillId => $skillVal) {?>
                                               <input type="checkbox" value="<?=$skillId?>" name="AllUser[Experience][<?=$k;?>][ExperienceSkillId][]" <?=(in_array($skillId, $employeeSkill)?'checked':"");?>> <label><?=$skillVal?></label> <span>|</span> 
                                            <?php } ?>
                                        </div>
    
    							</div>
							</div>
						
						</div>
						
						<div class="exp-white-collar" style="display:<?=(($profile->CollarType == "white")? 'block':"none"); ?>">
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Job Category</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<select class="questions-category form-control white_callar_exp_category" name="AllUser[Experience][<?=$k;?>][Category]" id="white_callar_exp<?=$k;?>" data-id-count="<?=$k;?>">
										 <option value="">Select Category</option>
										 <?php if(!empty($whiteCategories)){
												foreach($whiteCategories as $categoryId => $categoryName){?>
													<option value="<?php echo $categoryId;?>" <?=($val->CategoryId == $categoryId)?"selected":"";?>><?=$categoryName;?></option>
												<?php  }
												}?>
											 
										</select> 
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Role</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info exp-white-collar-role">
										<?php $categoryRoleList = WhiteRole::getRoleList($val->CategoryId);
										  $selectExpRoleList = ExperienceWhiteRole::getCategoryRoleList($val->ExperienceId, $profile->UserId); ?>
										<select class="questions-category form-control white_callar_role" name="AllUser[Experience][<?=$k;?>][WhiteRole][]" id="white_callar_role_exp<?=$k;?>" multiple="multiple">
										<?php if(!empty($categoryRoleList)){
											foreach($categoryRoleList as $roleId => $roleName){?>
												<option value="<?php echo $roleId?>" <?=(in_array($roleId, $selectExpRoleList)?"selected":"")?> ><?=$roleName;?></option>
											<?php  }
											}?>
										</select> 
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Skills</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info exp-white-collar-role">
										<?php 
										$expSkillList = ExperienceWhiteSkill::getSkillList($val->ExperienceId, $profile->UserId);
										?>
										<select class="questions-category form-control white_callar_skills" id="white_callar_skills_exp<?=$k;?>" name="AllUser[Experience][<?=$k;?>][WhiteSkills][]" multiple="multiple">
											<?php if(!empty($expSkillList)){
													foreach($expSkillList as $skillId => $skillName){?>
														<option value="<?php echo $skillId?>" selected ><?=$skillName;?></option>
													<?php  }
													}?>
										</select> 
									</div>
								</div>
							</div>
						
						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Industry</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select name="AllUser[Experience][<?=$k;?>][CIndustryId]" required
										class="questions-category form-control">



										<option selected="selected" value="">- Select an Industry -</option>

										<?php foreach($industry as $key=>$va){?>

										<option value="<?php echo $key;?>"
											<?php if($key==$val->IndustryId) echo "selected";?>><?=$va;?></option>

										<?php } ?>

								</select>

								</div>

							</div>

						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Experience</h4>



								</div>

							</div>

							<?php 
							//echo '<pre>';
							//print_r($val);die;
							?>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<?php //echo $val->Experience;?>
									<select id="form-filter-location" name="AllUser[Experience][<?=$k;?>][Experience]"
										data-minimum-results-for-search="Infinity" required
										class="questions-category form-control" tabindex="-1"
										aria-hidden="true">

										<option value=""
											<?php if($val->Experience=='') echo "selected";?>>Select Experience</option>
										<option value="Experience"
											<?php if($val->Experience=='Experience') echo "selected";?>>
											Experience</option>
										<option value="Fresher"
											<?php if($val->Experience=='Fresher') echo "selected";?>>
											Fresher</option>

										<option value="0-1"
											<?php if($val->Experience=='0-1') echo "selected";?>>Below 1
											Year</option>

					    <?php

                        for ($fr = 1; $fr <= 30; $fr = $fr + 1) 
                        {

                            $frn = $fr + 1;

                            ?>

                                            <option
											value="<?=$fr.'-'.$frn;?>"
											<?php if($val->Experience==($fr.'-'.$frn)) echo "selected";?>><?=$fr.'-'.$frn;?> Years</option>

                                           <?php
                        }

                        ?>

                              </select>

								</div>

							</div>

						</div>









						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-calendar" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Year</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<div class="cols-sm-5">

										<div class="input-group" style="width: 33%; float: left;">

											<span class="input-group-addon"><i class="fa fa-calendar"
												aria-hidden="true"></i></span> <input type="text"
												class="form-control date" name="AllUser[Experience][<?=$k;?>][YearFrom]"
												readonly style="cursor: pointer; background: #fff;"
												autocomplete="off" value="<?=$val->YearFrom;?>"
												placeholder="From" onkeypress="return numbersonly(event)" />

										</div>
						
										<div class="input-group exp-to-date"
											style="width: 33%; float: left; margin-left: 10%; <?=(!empty($val->CurrentWorking)?"display:none;":"");?>">

											<span class="input-group-addon"><i class="fa fa-calendar"
												aria-hidden="true"></i></span> <input type="text"
												class="form-control date" name="AllUser[Experience][<?=$k;?>][YearTo]" readonly
												style="cursor: pointer; background: #fff;"
												autocomplete="off" value="<?=$val->YearTo;?>"
												placeholder="To" onkeypress="return numbersonly(event);"
												onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />

										</div>

									</div>

								</div>

							</div>

						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">
									<h4>Position Name</h4>
								</div>
							</div>
							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<input type="text" value="<?= $val->PositionName?>"
										class="questions-category form-control"
										name="AllUser[Experience][<?=$k;?>][PositionName]">

								</div>
							</div>
						</div>
					

					<div class="row education-box">

						<div class="col-md-4 col-xs-12 col-sm-4">

							<div class="resume-icon">

								<i class="fa fa-briefcase" aria-hidden="true"></i>

							</div>

							<div class="insti-name">

								<h4>Salary</h4>

							</div>

						</div>

						<div class="col-xs-12 col-md-8 col-sm-8">
                            
							<div class="degree-info">
                                
								<select class="questions-category form-control" tabindex="-1"
									aria-hidden="true" name="AllUser[Experience][<?=$k;?>][Salary]">

									<option value="0-1.5"
										<?php if($val->Salary=='0-1.5') echo "selected";?>>0 - 1.5
										Lakh</option>

									<option value="1.5-3"
										<?php if($val->Salary=='1.5-3') echo "selected";?>>1.5 - 3
										Lakh</option>

									<option value="3-6"
										<?php if($val->Salary=='3-6') echo "selected";?>>3 - 6 Lakh</option>

									<option value="6-10"
										<?php if($val->Salary=='6-10') echo "selected";?>>6 - 10 Lakh</option>

									<option value="10-15"
										<?php if($val->Salary=='10-15') echo "selected";?>>10 - 15
										Lakh</option>

									<option value="15-25"
										<?php if($val->Salary=='15-25') echo "selected";?>>15-25 Lakh</option>

									<option value="Above 25"
										<?php if($val->Salary=='Above 25') echo "selected";?>>Above 25
										Lakh</option>

									<option value="Negotiable"
										<?php if($val->Salary=='Negotiable') echo "selected";?>>
										Negotiable</option>

								</select>
								
								<div class="current-work-exp" style="margin-top:10px;"><input id="currentWorking" type="checkbox" value="<?=$val->CurrentWorking;?>" <?=(!empty($val->CurrentWorking)?"checked":"");?> name="AllUser[Experience][<?=$k;?>][CurrentWorking]">&nbsp;<span style="font-size:12px; color:##565656">I currently work here</span></div>

							</div>

						</div>

					</div>





				</div>

				

					<?php
                    }

                    $style = 'none';
                } 
                else {

                    $style = 'block';
                }

                ?>

				<div id="yearbox" style="display: <?=$style;?>;">

					<div class="heading-inner">

						<p class="title">Work Experience</p>

					</div>
					<div class="resume-box">
						<div class="row education-box">

						<div class="col-md-4 col-xs-12 col-sm-4">

							<div class="resume-icon">

								<i class="fa fa-calendar" aria-hidden="true"></i>

							</div>

							<div class="insti-name">

								<h4>Year</h4>

							</div>

						</div>

						<div class="col-xs-12 col-md-8 col-sm-8">

							<div class="degree-info">

								<div class="cols-sm-5">
									<div class="input-group" style="width: 33%; float: left;">
										<span class="input-group-addon"><i class="fa fa-calendar"
											aria-hidden="true"></i></span> <input type="text"
											class="form-control date" name="AllUser[Experience][<?=$count;?>][YearFrom]" readonly
											style="cursor: pointer; background: #fff;" autocomplete="off"
											placeholder="From" onkeypress="return numbersonly(event)" />

									</div>

									<div class="input-group exp-to-date"
										style="width: 33%; float: left; margin-left: 10%;">

										<span class="input-group-addon"><i class="fa fa-calendar"
											aria-hidden="true"></i></span> <input type="text"
											class="form-control date" name="AllUser[Experience][<?=$count;?>][YearTo]" readonly
											style="cursor: pointer; background: #fff;" autocomplete="off"
											placeholder="To" onkeypress="return numbersonly(event);"
											onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />

									</div>

								</div>

							</div>

						</div>

					</div>
					</div>
				</div>

				<div id="workexpdiv" style="display: <?=$style;?>">

					<div class="resume-box">

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Company name</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<input type="text" class="form-control"
										name="AllUser[Experience][<?=$count;?>][CompanyName]" id="experience-company-name"
										placeholder="Company name" />

								</div>

							</div>

						</div>
						
						<div class="exp-blue-collar" style="display:<?=(($profile->CollarType != "white")? 'block':"none"); ?>">
						
						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Role</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select id="experience-role" class="experience-role form-control"
										name="AllUser[Experience][<?=$count;?>][PositionId]" count="<?=$count;?>">

										<option value="">Select Role</option>

										<?php

        foreach ($position as $key => $value) 
        {

            ?>

										<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>

										<?php
        }

        ?>

										</select>

								</div>

							</div>

						</div>
						
						<div id="skill-container-<?=$count;?>" style="none">
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Skills</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">

    								<div class="degree-info skill-profiles">
                                    </div>
    
    							</div>
								
							</div>
						</div>

						</div>
																		
						<div class="exp-white-collar" style="display:<?=(($profile->CollarType == "white")? 'block':"none"); ?>">
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Job Category</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<select class="questions-category form-control white_callar_exp_category" name="AllUser[Experience][<?=$count;?>][Category]" id="white_callar_exp<?=$count;?>" data-id-count="<?=$count;?>">
										 <option value="">Select Category</option>
										 <?php if(!empty($whiteCategories)){
												foreach($whiteCategories as $categoryId => $categoryName){?>
													<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
												<?php  }
												}?>
											 
										</select> 
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Role</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info exp-white-collar-role">
										<select class="questions-category form-control white_callar_role" name="AllUser[Experience][<?=$count;?>][WhiteRole][]" id="white_callar_role_exp<?=$count;?>" multiple="multiple"></select> 
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Skills</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info exp-white-collar-skill">
										<select class="questions-category form-control white_callar_skills" id="white_callar_skills_exp<?=$count;?>" name="AllUser[Experience][<?=$count;?>][WhiteSkills][]" multiple="multiple"></select> 
									</div>
								</div>
							</div>
						
						</div>

						
						
						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-files-o" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Industry</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select name="AllUser[Experience][<?=$count;?>][CIndustryId]"
										class="questions-category form-control" id="experience-cindustry-id">

										<option selected="selected" value="">- Select an Industry -</option>

										<?php foreach($industry as $key=>$va){?>

										<option value="<?php echo $key;?>"><?=$va;?></option>

										<?php } ?>

								</select>

								</div>

							</div>

						</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Experience</h4>



								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select id="experience-user" name="AllUser[Experience][<?=$count;?>][Experience]"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control" tabindex="-1"
										aria-hidden="true">

										<option value="">Experience</option>

										<option value="Fresher">Fresher</option>

										<option value="0-1">Below 1 Year</option>

					    <?php

        for ($fr = 1; $fr <= 30; $fr = $fr + 1) 
        {

            $frn = $fr + 1;

            ?>

                                            <option
											value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>

                                           <?php
        }

        ?>

                              </select>

								</div>

							</div>

						</div>


						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Position Name</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<input type="text" class="questions-category form-control"
										name="AllUser[Experience][<?=$count;?>][PositionName]" id="experience-position-name">

								</div>

							</div>

						</div>




						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-briefcase" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Salary</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">

								<div class="degree-info">

									<select class="questions-category form-control" tabindex="-1"
										aria-hidden="true" id="experience-salary" name="AllUser[Experience][<?=$count;?>][Salary]">

										<option value="0-1.5">0 - 1.5 Lakh</option>

										<option value="1.5-3">1.5 - 3 Lakh</option>

										<option value="3-6">3 - 6 Lakh</option>

										<option value="6-10">6 - 10 Lakh</option>

										<option value="10-15">10 - 15 Lakh</option>

										<option value="15-25">15 - 25 Lakh</option>

										<option value="Above 25">Above 25 Lakh</option>

										<option value="Negotiable">Negotiable</option>

									</select>
									
									<div class="current-work-exp" style="margin-top:10px;"><input id="currentWorking" type="checkbox" value="1" name="AllUser[Experience][<?=$count;?>][CurrentWorking]">&nbsp;<span style="font-size:12px; color:##565656">I currently work here</span></div>

								</div>
								
								

							</div>

						</div>

					</div>

				</div>



				<div id="moreskill"></div>

				<div class="form-group">

					<label class="cols-sm-2 control-label" onclick="admoreexp();"
						style="color: #f15b22; cursor: pointer;">+ Add More</label>

				</div>



			</div>

		</div>

	

    <?php ActiveForm::end(); ?>

  </div>

</div>

</div>



<div class="border"></div>


<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script>







	
<script type="text/javascript"> 
    $(document).on('click', '.collar-type', function(){
		var value = $(this).val();
		if(value == "blue"){
			$('#blue-collar-options').show(0);
			$('#white-collar-options').hide(0);
			
			$('.exp-white-collar').hide(0);
			$('.exp-blue-collar').show(0);
		}else if(value == 'white'){
			$('#blue-collar-options').hide(0);
			$('#white-collar-options').show(0);
			
			$('.exp-white-collar').show(0);
			$('.exp-blue-collar').hide(0);
		}
	});
	
	
</script>
	
<script type="text/javascript">

    $(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true, yearRange: "-50:+0"});

		var p=<?=$count;?>;
		
		
		function getSkillExperienceNew(roleId,dataidcount){
			$.get(baseUrl+'site/getexperienceskills?id='+roleId+'&count='+dataidcount, function(res){
				
				
			});
		}
		

		function admoreexp() {

		  if ($('#workexpdiv').css('display') == 'none') {

            $('#workexpdiv').show();$('#yearbox').show();

          }

		  else

		  {

				p++;

				var result=$('#workexpdiv').html();

				var date='<div class="resume-box"><div class="row education-box"><div class="col-md-4 col-xs-12 col-sm-4"><div class="resume-icon"><i class="fa fa-calendar" aria-hidden="true"></i></div><div class="insti-name"> <h4>Year</h4></div></div><div class="col-xs-12 col-md-8 col-sm-8"><div class="degree-info"><div class="cols-sm-5"><div class="input-group" style="width: 33%;float: left;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="AllUser[Experience]['+p+'][YearFrom]" readonly  style="cursor: pointer;background: #fff;" autocomplete="off" placeholder="From"  onkeypress="return numbersonly(event)"/></div><div class="input-group" style="width: 33%;float: left;margin-left: 10%;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="AllUser[Experience]['+p+'][YearTo]" readonly  style="cursor: pointer;background: #fff;" autocomplete="off" placeholder="To" onkeypress="return numbersonly(event);" onchange="if(new Date($(this).val()).getTime()<=new Date($(&#39;#YearFrom&#39;).val()).getTime()){alert(&#39;Wrong Year Duration&#39;);$(this).val(&#39; &#39;);}"/></div></div></div></div></div></div>';
				

            $('#moreskill').append('<div id="workexpindiv'+p+'" ><span style="color:red;cursor:pointer;" onclick=del('+p+');>X</span>'+date+' '+result+'</div>');
			
			$('#workexpindiv'+p).find('.current-work-exp').remove();
			
			$('#workexpindiv'+p).find('[id^=experience-company-name]').attr('name', 'AllUser[Experience]['+p+'][CompanyName]');
			$('#workexpindiv'+p).find('[id^=experience-company-name]').attr('id', 'experience-'+p+'-CompanyName');
			
			$('#workexpindiv'+p).find('[id^=experience-role]').attr('name', 'AllUser[Experience]['+p+'][PositionId]');
			$('#workexpindiv'+p).find('[id^=experience-role]').attr('id', 'experience-'+p+'-PositionId');
			$('#workexpindiv'+p).find('[id^=experience-role]').attr('count', p);
			
			$('#workexpindiv'+p).find('[id^=experience-cindustry-id]').attr('name', 'AllUser[Experience]['+p+'][CIndustryId]');
			$('#workexpindiv'+p).find('[id^=experience-cindustry-id]').attr('id', 'experience-'+p+'-CIndustryId');
			
			$('#workexpindiv'+p).find('[id^=experience-cindustry-id]').attr('name', 'AllUser[Experience]['+p+'][CIndustryId]');
			$('#workexpindiv'+p).find('[id^=experience-cindustry-id]').attr('id', 'experience-'+p+'-CIndustryId');
			
			$('#workexpindiv'+p).find('.white_callar_exp_category').attr('name', 'AllUser[Experience]['+p+'][Category]');
			$('#workexpindiv'+p).find('.white_callar_exp_category').attr('id', 'white_callar_exp'+p);
			$('#workexpindiv'+p).find('.white_callar_exp_category').attr('data-id-count', p);
			
			var expWhiteRoleHtml = '<select class="questions-category form-control white_callar_role" name="AllUser[Experience]['+p+'][WhiteRole][]" id="white_callar_role_exp'+p+'" multiple="multiple"></select>';
			var expWhiteSkillHtml = '<select class="questions-category form-control" id="white_callar_skills_exp'+p+'" name="AllUser[Experience]['+p+'][WhiteSkills][]" multiple="multiple"></select>';
			
			$('#workexpindiv'+p).find('.exp-white-collar-role').html(expWhiteRoleHtml);
			$('#workexpindiv'+p).find('.exp-white-collar-skill').html(expWhiteSkillHtml);
			$("#white_callar_role_exp"+p).select2({
				placeholder: "Select Role"
			});
			
			$("#white_callar_skills_exp"+p).select2({
				maximumSelectionLength: 5,
				placeholder: "Select Skills",
				ajax: {
					url: '<?=Url::toRoute(['getwhiteskills']);?>',
					dataType: 'json',
					type: "GET",
					data: function (params) {
						return {
							q: params.term,
							page: 1
						  }
					},
					processResults: function(data){
						return {
							results: $.map(data.results, function (item) {
								
								return {
									text: item.name,
									id: item.id
								}
							})
						};
					}
				 }
			});


              $(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true, yearRange: "-50:+0"});

		  }

        }

		function del(id) {

            $('#workexpindiv'+id).remove();

        }



  setTimeout(function(){

		var loc = new locationInfo();

		$('#stateId').val('<?=$state;?>');

		<?php if($profile->CampusId!=0){ ?>

		$('#stateId').attr('disabled','disabled');

		<?php
}

?>

		var loc1='<?=$state;?>';

		// var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');

        
	//$('option:selected', this).attr('mytag');
	var cityname = '<?=$city;?>';
	
		setTimeout(function(){$('#cityId option[value="'+cityname+'"]').attr('selected');},1000);

		
		getNcourse($('#empcourse11').val());
	},2000);
	
  	$( document ).ready(function() {
		
		$('#currentWorking').click(function(){
			if($(this).is(':checked')){
				$('.exp-to-date').hide(0);
			}else{
				$('.exp-to-date').show(0);
			}
			
		});
		
		/*Load Experience Role Skills*/
		$(document).on('change', '.experience-role', function() {    
			var roleid = $(this).val();
			var newCount = $(this).attr('count');
			var loderimg = '<img src="'+mainurl+'img/ajax-loader.gif">';
			var e = $(this);
			e.parents('.resume-box:first').find('.skill-profiles').html(loderimg);
			//$('.skill-profiles').html(loderimg);
			 $.getJSON( "/site/skilldata", {roleid:roleid}, function( data ) {
				
				 var html = '';
				  $.each( data, function( key, val ) {
					 html += '<span><input type="checkbox" value="'+val.id+'" name="AllUser[Experience]['+newCount+'][ExperienceSkillId][]" />'+val.value+'&nbsp;</span><span>|</span>&nbsp;'; 
				  });
				  html +='</div></div>';
				  
				e.parents('.resume-box:first').find('.skill-profiles').html(html);
			});
			
		}); 
  		
        $(".states option:gt(0)").remove(); 
        $(".cities option:gt(0)").remove(); 
        var url2 = '/site/locationinfo'+'?type=getStates&countryId=101';
        var method = "post";
        var data = {};
        $('.states').find("option:eq(0)").html("Please wait..");
    
        $.ajax({
            url:url2,
           success:function(data)
           {
            //data=JSON.parse(data);
            $('.states').find("option:eq(0)").html("Select State");
            if(data.tp == 1){
            $.each(data.result, function(key, val) {
                var option = $('<option />');
                option.attr('value', val).text(val);
                option.attr('stateid', key);
                $('.states').append(option);
            });
                $(".states").prop("disabled",false);
            }

            $('#stateId').val('<?=$state;?>');
            getcitites();
           }
        });
  	});
    function getcitites(){
        var id = $("#stateId option[value='<?=$state;?>']").attr('stateid');
        $(".cities option:gt(0)").remove();
        var url1 = '/site/locationinfo'+'?type=getCities&stateId=' + id;
        var method = "get";
        var data = {};
        $('.cities').find("option:eq(0)").html("Please wait..");
        $.ajax({url:url1,
           success:function(data)
           {
        //data=JSON.parse(data);
        $('.cities').find("option:eq(0)").html("Select City");
        if(data.tp == 1){
            $.each(data.result, function(key, val) {
                var option = $('<option />');
                option.attr('value', val).text(val);
                 option.attr('cityid', key);
                $('.cities').append(option);
            });
            $(".cities").prop("disabled",false);
            }
            $('#cityId').val('<?=$city;?>');
           }
    });
    }
  	$(document).on('change', '#empcourse11', function() {
  		getNcourse($(this).val());
	});
	
	

  	function getNcourse(val){
  		
        if(val==""){
        	$("#naukari-course option").remove();
            //$("#naukari-specialization option").remove();
            $("#naukari-board option").remove();
        	return true;
        }
        
        if(val=="4" || val=="5" || val=="6"){
        	$("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#university_college").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').removeClass('hide');
			$('.high-qual').hide(0);
			$('.low-qual').show(0);
            var elm = 'naukari-board';
			var course = <?=(isset($profile->educations[0]->board_id)?$profile->educations[0]->board_id:"0");?>;
        }else{
        	$("#naukari-course").parents('.form-group').removeClass('hide');
            $("#naukari-specialization").parents('.form-group').removeClass('hide');
            $("#university_college").parents('.form-group').removeClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
			$('.high-qual').show(0);
			$('.low-qual').hide(0);
            var elm = 'naukari-course';
			var course = <?=(isset($profile->educations[0]->course_id)?$profile->educations[0]->course_id:"0");?>;
        }
        
        var selected = '';	
        //var id = $(this).val();
  			$.ajax({
                type: "POST",
                url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
                data: {'id': val},
                 success : function(response) {
            		var data = JSON.parse(response);
					if(val=="4" || val=="5" || val=="6"){
						$("#board-list option").remove();
						//$("#naukari-specialization option").remove();
						$("#naukari-board option").remove();
						$("#board-list").append('<option value="">Select Board</option>');
						$.each(data, function( index, value ) {
							selected = '';
							if(index==course){
								selected = "selected=selected";
							}
							$("#board-list").append('<option '+selected+' value="'+index+'">'+value+'</option>');
						});
					}else{
						$("#naukari-course option").remove();
						//$("#naukari-specialization option").remove();
						$("#naukari-board option").remove();
						$("#"+elm).append('<option value="">select course</option>');
						$.each(data, function( index, value ) {
							selected = '';
							if(index==course){
								selected = "selected=selected";
							}
							$("#"+elm).append('<option '+selected+' value="'+index+'">'+value+'</option>');
						});
					}
            		
        		}, 

            })
  	}
  	 var selectedValues = new Array();

  	var Ldata = [{id: '',text: 'Select Language'}];
  	<?php

foreach ($language_opts as $key1 => $value1) {
    if (in_array($value1->name, $language_ary)) {
        ?>
  			selectedValues.push('<?=$value1->name?>');
  		<?php

}
    ?>
			var ld = {id: '<?=$value1->name?>',text: '<?=$value1->name?>'};
			Ldata.push(ld);
	<?php }  ?>
	$("#LanguageId").select2({
  		data: Ldata,
  		selectedValues,
  		maximumSelectionLength: 5,
	});
	$("#LanguageId").val(selectedValues).trigger("change");
    $('.dropdown-sin-1').dropdown({
      readOnly: true,
      limitCount:5,
      input: '<input type="text" maxLength="5" placeholder="Search">'
    });
    
    $(document).on('change', '.role-container .emp-role', function() {    
        var roleid = $(this).val();
        var loderimg = '<img src="'+mainurl+'img/ajax-loader.gif">';
        var e = $(this);
        e.parents('.role-container:first').find('.skill-profiles').html(loderimg);
        //$('.skill-profiles').html(loderimg);
         $.getJSON( "/site/skilldata", {roleid:roleid}, function( data ) {
            
             var html = '';
              $.each( data, function( key, val ) {
				  if(typeof val.value != "undefined"){
					html += '<span><input type="checkbox" value="'+val.id+'" name="AllUser[SkillId]['+roleid+'][]" />'+val.value+'&nbsp;</span><span>|</span>&nbsp;'; 
				  }
					
              });
              html +='</div></div>';
            e.parents('.role-container:first').find('.skill-profiles').html(html);
        });
        
    }); 
	
	
	

    function admorerole(){
        var div = $('.role-container').first().html();
        //var div_skill= '<div class="form-group skilldatalist"></div>';
        var oDiv = document.createElement('div');
        oDiv.setAttribute("class", "role_skills");
        oDiv.innerHTML=('<div class="append_role education-box">'+div+'</div><div class="education-skills"></div>');
        $('.addrole-continer').before(oDiv);
		//$('.append_role:last').find('.emp-role').find('option:selected').remove();
		$('.append_role:last').find('.emp-role').val($('.append_role:last').find('.emp-role option:first').val());
		
		$('.append_role:last').find('.skill-profiles').html("");
        //$('.addrole-continer').prepend(div_skill);
    }

    $(document).on('change', '.append_role select', function() {
        var roleid = $(this).val();
        var loderimg = '<img src="'+mainurl+'img/ajax-loader.gif">';
        var e = $(this);
        $(this).parents('.role_skills:first').find('.skill-profiles').html(loderimg);
         $.getJSON( "/site/skilldata", {roleid:roleid}, function( data ) {
             var html = "";
             /*var html = '<div class="row education-box"><div class="col-md-4 col-xs-12 col-sm-4"><div class="resume-icon"><i class="fa fa-file-text" aria-hidden="true"></i></div> <div class="insti-name"><h4>Skills</h4></div></div><div class="col-xs-8 col-md-8 col-sm-8"> <div class="degree-info skill-profiles">';*/
              $.each( data, function( key, val ) {
				  if(typeof val.value != "undefined"){
					   html += '<span><input type="checkbox" value="'+val.id+'" name="AllUser[SkillId]['+roleid+'][]" />'+val.value+'&nbsp;</span>'; 
				  }
                
              });
              //html +='</div></div></div>';
             e.parents('.role_skills:first').find('.skill-profiles').html(html);
        });
        
    });
	
function getWhiteCategoryList(count) {
	var options = $('#white_callar_category').html();
	var html = '<br class="clearfix"/><div class="row extra-white-role"><div class="col-md-2">'+
					'<label class="control-label" style="width:100%" for="white_callar_category'+count+'">Job Category</label></div><div class="col-md-4">'+
						'<select id="white_callar_category'+count+'" class="form-control white_callar_category" name="AllUser[Category]['+count+'][WhiteCategory]" data-id-count = "'+count+'">'+
							options+
						'</select>'+
					'<p class="help-block help-block-error"></p>'+
				'</div><div class="col-md-2"><label class="control-label"> Roles</label></div><div class="col-md-4"><select class="questions-category form-control" name="AllUser[Category]['+count+'][WhiteRole][]" id="white_callar_role'+count+'" multiple="multiple"></select></div></div><div class="clearfix"></div>';
	return html;
}	
$(document).ready(function(){	
	/*Add Collar Type*/
	$(document).on('click', '.collar-type', function(){
		var value = $(this).val();
		if(value == "blue"){
			$('#blue-collar-options').show(0);
			$('#white-collar-options').hide(0);
			
			$('.exp-white-collar').hide(0);
			$('.exp-blue-collar').show(0);
		}else if(value == 'white'){
			$('#blue-collar-options').hide(0);
			$('#white-collar-options').show(0);
			
			$('.exp-white-collar').show(0);
			$('.exp-blue-collar').hide(0);
		}
	});
	
	countT = 0;
	$(document).on('click' , '#add-white-role' , function(e){
		var countVal = $(this).attr('count');
		if(countVal != ""){
			countT = countVal;
		}
		countT++;
		var html = getWhiteCategoryList(countT);
		$('#white-role-container').append(html);
		$('#white_callar_category'+countT).val($('#white_callar_category'+countT+' option:first').val());
		$("#white_callar_role"+countT).select2({
			placeholder: "Select Role"
		});
		$(this).attr('count', countT);
		
	});
	
	
	$(".white_callar_role").select2({
  		placeholder: "Select Role"
	});
	
	$(document).on('change', '#white_callar_category', function(){
		var value = $(this).val();
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role0').html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role0').append(option);
						});
					}
				}
			});
		}
		
	});
	
	$(document).on('change', '.white_callar_category', function(){
		var value = $(this).val();
		var countC = $(this).attr('data-id-count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role'+countC).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role'+countC).append(option);
						});
					}
				}
			});
		}
		
	});
	
	$("#white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});
	
	$("#white_callar_role_exp0").select2({
  		placeholder: "Select Role"
	});
	$("#white_callar_role_exp1").select2({
  		placeholder: "Select Role"
	});
	
	$(document).on('change', '.white_callar_exp_category', function(){
		var value = $(this).val();
		var countC = $(this).attr('data-id-count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role_exp'+countC).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role_exp'+countC).append(option);
						});
					}
				}
			});
		}
		
	});
	
	$(".white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});	
	
	$("#naukari-location").select2({
		maximumSelectionLength: 5,
		placeholder: "Select Max 5 Preferred Locations ...",
		ajax: {
			url: '<?=Url::toRoute(['getpreferredlocation']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.CityName,
							id: item.id
						}
					})
				};
			}
		 }
	});
	/*Remove Resume*/
	$(document).on('click', '.remove-resume', function(){
		$.ajax({
			dataType : "json",
			type : 'GET',
			url : '<?=Url::toRoute(['deleteuserresume']);?>',
			data : false,
			success : function(data) {
				if(data == true){
					$('#delete-resume').html("");
				}				
			}
		});
		
	});
	
});	
	
	
</script>
