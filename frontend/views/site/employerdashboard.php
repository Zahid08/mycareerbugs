<?php
$this->title = 'Dashboard - My Career Bugs';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
use common\models\Plan;
use common\models\PlanAssign;
use common\models\PlanRecord;
$plan = new Plan();
$allplan = $plan->getAllplan();

?>

<style>
    .user-profile{margin:0px;}
    .inner_page {padding: 30px 0;}
     </style>

<div>
	<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">

</div>

<div id="wrapper">
	<!-- start main wrapper -->

	<div class="inner_page">
		<div class="container">
			<div id="profile-desc">
				<div class="col-md-2 col-sm-2 col-xs-12">
					<div class="user-profile">

						<img src="<?php echo Yii::$app->session['EmployerDP'] ;?>"
							alt="<?php echo Yii::$app->session['EmployerName'] ;?>"
							class="img-responsive center-block ">
						<h3><?php echo Yii::$app->session['EmployerName'] ;?></h3>
					</div>
				</div>
				<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="col-md-12" style="margin-bottom: 10px;">
						<!--  <div class="col-md-3"><a class="btn btn-lg btn-block btn-danger" href="<?= Url::toRoute(['site/companyprofile'])?>">Company Profile</a></div>
				        <div class="col-md-3"><a class="btn btn-lg btn-block btn-danger" href="<?= Url::toRoute(['wall/careerbugwall'])?>">My MCB Wall</a></div> -->
					</div>
					<div class="col-md-12" style="margin-bottom: 10px;">
						<div class="widget-heading">
							<span class="title"> Our Data Plans </span>
						</div>
						<br> <br>
					
						<table class="table table-striped"
							style="border: 1px solid #cccccc">
							<thead>
								<tr class="text-r" style="background: #f16b22">
									<th style="text-align: left">Limits</th>
									<th>Resume</th>
									<th>Excel</th>
									<!--<th>SMS Send</th> -->
									<th>Email Send</th>
									<th>View Profile</th>
									<th>WhatsApp</th>
								</tr>
							</thead>
								<?php
					
                        if ($planassign->isExpired()) {
                          echo  '<div id="w0-error-0" class="alert-danger alert fade in">Your Plan is Expired, Please Contact Us on: +91 8240366924</div>';
                           
                        }else{
						?>
							<tbody>
								<?php $employerId = Yii::$app->session['Employerid'];?>
								<tr class="text-r">
									<td style="text-align: left"><strong> <?=$planassign->plan->PlanName?> </strong></td>
									<td><strong><?=$planassign->plan->CVDownload?> </strong></td>
									<td><strong><?=$planassign->plan->TotalDownload?></strong></td>
									<td><strong><?=$planassign->plan->TotalEmail?> </strong></td>
									<td><strong><?=$planassign->plan->ViewContact?>	 </strong></td>
									<td><strong><?=$planassign->plan->whatsapptotal?>	 </strong></td>
								</tr>
								<tr class="text-r">
									<td style="text-align: left">
										<strong> Today (Date) </strong> 
										<br>Consumed</td>
									<td>
										<strong><?=$planassign->plan->total_daily_cv_download;?> </strong>
										<br> <?php echo (PlanRecord::getDailyUsedData($employerId,'CVDownload')); ?>  
									</td>
									<td>
										<strong><?=$planassign->plan->daily_download;?></strong> 
										<br> <?php echo PlanRecord::getDailyUsedData($employerId,'Download'); ?>  
									</td>
									<td>
										<strong><?=$planassign->plan->total_daily_email;?> </strong>
										<br> <?php echo PlanRecord::getDailyUsedData($employerId,'Email'); ?>  
									</td>
									<td>
										<strong><?=$planassign->plan->total_daily_view_contact;?></strong>
										<br><?php echo PlanRecord::getDailyUsedData($employerId,'ContactView'); ?></td>
									<td>
										<strong><?=$planassign->plan->daily_whatsapptotal;?></strong>
										<br> <?php echo PlanRecord::getDailyUsedData($employerId,'whatsappview'); ?>  
									</td>
								</tr>
								<tr class="text-r">
									<td style="text-align: left">
										<strong> <?php echo date("jS M", strtotime($planassign->UpdatedDate)); ?> - <?php echo date("jS M", time()); ?> </strong>
										<br> Consumed
									</td>
									<td>
										<strong> <?=$planassign->CVDownload;?> </strong> 
										<br> <?php echo PlanRecord::getUsedDataFrom($employerId,'CVDownload', $planassign->UpdatedDate); ?>  
									</td>
									<td>
										<strong><?=$planassign->TotalSMS;?> </strong> 
										<br> <?php echo PlanRecord::getUsedDataFrom($employerId,'Download', $planassign->UpdatedDate); ?>  </strong>
									</td>
									<td>
										<strong><?=$planassign->TotalEmail;?></strong> 
										<br> <?php echo PlanRecord::getUsedDataFrom($employerId,'Email', $planassign->UpdatedDate); ?>  </strong>
									</td>
									<td>
										<strong><?=$planassign->ViewContact;?></strong> 
										<br> <?php echo PlanRecord::getUsedDataFrom($employerId,'ContactView', $planassign->UpdatedDate); ?> </strong>
									</td>
									<td>
										<strong><?=$planassign->plan->whatsapptotal;?>	</strong> 
										<br> <?php echo PlanRecord::getUsedDataFrom($employerId,'whatsappview', $planassign->UpdatedDate); ?> </strong>
									</td>
								</tr>

								<tr class="text-r">
									<td style="text-align: left"><strong> Total </strong> <br>
										Consumed</td>
									<td>
										<strong><?=$planassign->CVDownload;?></strong> 
										<br> <?=$planassign->UseCVDownload;?>  
									</td>
									<td>
										<strong><?=$planassign->TotalDownload;?></strong> 
										<br> <?=$planassign->UseDownload;?>  
									</td>
									<td>
										<strong><?=$planassign->TotalEmail;?></strong> 
										<br> <?=$planassign->UseEmail?>  
									</td>
									<td>
										<strong><?=$planassign->ViewContact;?>	 </strong> 
										<br> <?=$planassign->UseViewContact;?>  
									</td>
									<td>
										<strong>	<?=$planassign->plan->whatsapptotal;?>	 </strong>
										<br> <?=$planassign->UseWhatsApp;?>  
									</td>
								</tr>
							</tbody>
							<?php }?>
						</table>
					</div>
				</div>
				<div class="col-md-3">
					<div
						style="border-radius: 3px; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); margin-bottom: 20px; background-color: #26a69a; border-color: #26a69a; color: #fff; height: 120px;">
						<a href="<?= Url::toRoute(['site/yourpost'])?>"><div
								style="padding: 20px;">
								<h3 style="font-size: 21px; color: #fff; margin: 0px;"><?=$totaljob;?></h3>
								<div style="font-size: 12px; color: #fff;">Total Job Posted</div>
								<br />
								<div style="font-size: 12px; color: rgba(255, 255, 255, 0.8);">Total Active Job <?=$totalactivejob;?></div>
							</div></a>
					</div>
				</div>
				<div class="col-md-3">
					<a href="<?= Url::toRoute(['site/candidateapplied'])?>"><div
							style="border-radius: 3px; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); margin-bottom: 20px; background-color: #ec407a; border-color: #ec407a; color: #fff; height: 120px;">
							<div style="padding: 20px;">
								<h3 style="font-size: 21px; color: #fff; margin: 0px;"><?=$appliedjob;?></h3>
								<div style="font-size: 12px;">Total Applied</div>
							</div>
						</div></a>
				</div>
				<div class="col-md-3">
					<a href="<?= Url::toRoute(['site/candidatebookmarked'])?>"><div
							style="border-radius: 3px; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); margin-bottom: 20px; background-color: #29b6f6; border-color: #29b6f6; color: #fff; height: 120px;">
							<div style="padding: 20px;">
								<h3 style="font-size: 21px; color: #fff; margin: 0px;"><?=$bookmarked;?></h3>
								<div style="font-size: 12px;">Total Bookmarked</div>
							</div>
						</div></a>
				</div>
				<div class="col-md-3">
					<a href="<?= Url::toRoute(['site/empnotification'])?>"><div
							style="border-radius: 3px; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); margin-bottom: 20px; background-color: #26a69a; border-color: #26a69a; color: #fff; height: 120px;">
							<div style="padding: 20px;">
								<h3 style="font-size: 21px; color: #fff; margin: 0px;"
									id="totalenot"></h3>
								<div style="font-size: 12px;">Notification</div>
							</div>
						</div></a>
				</div>
			</div>
		</div>

		<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"
			id="mobile_design"></div>
	</div>




	<div class="container">
		<div class="widget-heading">
			<span class="title"> Our Data Plans </span>
		</div>
		<div id="orange_payment_block">     
						  <?php
        if ($allplan) {
            foreach ($allplan as $key => $pland) {
                ?>
						  	   <div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<div class="panel price panel-red">
							<div class="panel-body text-center">
								<p class="lead">
									<strong><?=$pland->PlanName;?></strong>
								</p>
							</div>
							<div class="clear"></div>
							<ul class="list-group list-group-flush text-center width-half">
								<li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#"  style="width:200px">Call Us: +91 8240369924</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>    
						<?php
            }
        }
        ?>  				
        </div>
	</div>
	<div style="height: 50px"></div>

	<div class="container">
		<div class="widget-heading"  style="display:none">
			<span class="title"> Career Bugs Wall Plans </span>
		</div>
		<div id="orange_payment_block"  style="display:none">

			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<div class="panel price panel-red">
							<div class="panel-body text-center">
								<p class="lead">
									<strong>Basic Plan</strong>
								</p>
							</div>
							<div class="clear"></div>
							<ul class="list-group list-group-flush text-center width-half">
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									Rs/- 100</li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									2 post (per day)</li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									30 Days</li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>

			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<!-- PRICE ITEM -->
						<div class="panel price panel-red ">
							<div class="panel-body text-center">
								<p class="lead">
									<strong>REGULAR PLAN</strong>
								</p>
							</div>
							<ul class="list-group list-group-flush text-center width-half">
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									Rs/- 100</li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									2 post (per day)</li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									30 Days</li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>

			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<!-- PRICE ITEM -->
						<div class="panel price panel-red">
							<div class="panel-body text-center">
								<p class="lead">
									<strong>STAR PLAN</strong>
								</p>
							</div>
							<ul class="list-group list-group-flush text-center width-half">
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									Rs/- 100</li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									2 post (per day)</li>
								<li class="list-group-item"><i class="icon-ok text-danger"></i>
									30 Days</li>
							</ul>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>

		</div>
	</div>
	<div style="height: 50px"></div>
	<div class="container">
		<div class="widget-heading"  style="display:none">
			<span class="title"> Portal Plans </span>
		</div>
		<div id="orange_payment_block" style="display:none">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<!-- PRICE ITEM -->
						<div class="panel price panel-red ">
							<div class="panel-body text-center">
								<p class="lead">
									<strong>6 Months </strong>
								</p>
							</div>
							<div class="table_list">
								<ul>
									<li>1 Job post per day</li>
									<li>2 post on Career Bugs wall per day</li>
									<li>100 CV Download per day</li>
									<li>50 sms per day</li>
									<li>50 sms per day</li>
								</ul>
							</div>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#">Rs 600/-</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>


			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<!-- PRICE ITEM -->
						<div class="panel price panel-red ">
							<div class="panel-body text-center">
								<p class="lead">
									<strong>6 Months </strong>
								</p>
							</div>
							<div class="table_list">
								<ul>
									<li>1 Job post per day</li>
									<li>2 post on Career Bugs wall per day</li>
									<li>100 CV Download per day</li>
									<li>50 sms per day</li>
									<li>50 sms per day</li>
								</ul>
							</div>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#">Rs 600/-</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>

			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<div class="widget_inner-main">
					<div class="widget_inner">
						<!-- PRICE ITEM -->
						<div class="panel price panel-red ">
							<div class="panel-body text-center">
								<p class="lead">
									<strong>6 Months </strong>
								</p>
							</div>
							<div class="table_list">
								<ul>
									<li>1 Job post per day</li>
									<li>2 post on Career Bugs wall per day</li>
									<li>100 CV Download per day</li>
									<li>50 sms per day</li>
									<li>50 sms per day</li>
								</ul>
							</div>
							<div class="panel-footer">
								<a class="btn btn-lg btn-block btn-danger" href="#">Rs 600/-</a>
							</div>
						</div>
						<!-- /PRICE ITEM -->
					</div>
				</div>
			</div>


		</div>
	</div>
</div>
<div class="border"></div>