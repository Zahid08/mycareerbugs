<?php
$this->title = 'All Notification';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>

<style>
    h5 {font-size: 15px;}
</style>


<!-- start main wrapper --> 
	<div id="wrapper">
	
		<div class="recent-job"><!-- Start Recent Job -->
			<div class="container">
				<div class="row">
				  <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
			 
			 <div class="col-lg-12 col-md-5 col-sm-5 col-xs-12">
						<h4> All Notification</h4>
					 </div>	 
					  <div class="col-lg-12 col-md-7 col-sm-7 col-xs-12" id="landing_select">
					 
 <div class="clearfix"></div>
		
		<?php
		if($allnotification)
		{
		foreach($allnotification as $nk1=>$nval1)
		{
            $exp='';$link='searchcandidate';
										if($nval1->user->experiences)
										{
											$exp='Having'.$nval1->user->experiences[0]->Experience.' Year Experience'; 
										}
										elseif($nval1->user->UserTypeId==4)
										{
											$exp='Company';$link='campusdetail';
										}
										else
										{
											 $exp=$nval1->user->educations[0]->HighestQualification;
										}
		?>
		 <div class="item-list">
                <a href="<?= Url::toRoute(['wall/'.$link,'userid'=>$nval1->UserId])?>">
                <div class="col-sm-12 add-desc-box">
                  <div class="add-details">
                    <h5 class="add-title"> <strong style="color: #f16b22;">Job Title : </strong><?=$nval1->job->JobTitle;?></h5>
                    <div class="info" style="padding:0px;"> 
                        <span class="category"> <strong style="color: #f16b22;"> <?=$nval1->Type;?>  by :</strong>  <?=$nval1->user->Name;?></span>  <br>
                        <span class="category"> <strong style="color: #f16b22;"> Location :</strong>  <?=$nval1->user->City;?></span><br>
                        <span class="category"> <strong style="color: #f16b22;">Phone : </strong> +91 <?=$nval1->user->MobileNo;?> </span> 
					</div>
                    
                  </div>
                </div>
                </a>
              </div>
		<?php
		}
		}
        else
        {
		?>
        <h5 class="add-title">There is no notification</h5>
        <?php
        }
        ?>
					 
						<div class="spacer-2"></div>
					</div>
					 
                  </div>
					
					
					<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side">
							<div class="clearfix"></div>
						<div class="spacer-5"></div><div class="spacer-5"></div><div class="spacer-5"></div>
						
							<!--adban_block main -->
								<div class="adban_block">
							      <img src="<?=$imageurl;?>images/adban_block/ban.jpg"> 
							    </div>
						   <!-- adban_block  main -->
								
								
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div><!-- end Recent Job -->

</div><!-- end main wrapper -->

