<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
use yii\helpers\Html;

$this->title = $name;

?>
<div id="wrapper">
	<!-- start main wrapper -->

	<div class="job_search new12">
		<!-- Start Recent Job -->
		<div class="container">
			<div class="row">
				<div class="site-error">

					<h1><?= Html::encode($this->title) ?></h1>

					<div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>
                <h1 style="    text-align: center;
    padding: 119px;
    font-size: 224px;">404!</h1>
				</div>
			</div>
		</div>
	</div>
</div>
