<?php
$this->title = 'My Career Bugs';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
use yii\widgets\LinkPager;
use common\components\MyHelpers;
use yii\base\View;
use common\models\Industry;
use common\models\Position;
use common\models\Documents;


?>
<meta name="keywords" content="jobsearch, career,  hiring,  recruitment,resume,   recruiting, jobopening, training, cv, staffing, humanresources, business,#jobinterview">

<link rel="stylesheet" href="css/jquery.auto-complete.css">
<script src="js/jquery3.js"></script>
<script src="js/jquery.auto-complete.js"></script>

<style>
#keynameSearchContainer {
	z-index: 99999;
	position: relative;
	background-color: white;
	border: 2px;
}

#IndexCitySearchContainer {
	left: 42%;
	z-index: 99999;
	position: relative;
	background-color: white;
	border: 2px;
}
.ui-autocomplete { position: absolute; top: -15px; }
#IndexCitySearchContainer .ui-autocomplete { height:auto }
.widget .related-post li a{font-size:14px;}
@media only screen and (max-width: 767px) {
#keynameSearchContainer .ui-autocomplete {position: absolute;top: -163px;left: 0;cursor: default;}
#IndexCitySearchContainer .ui-autocomplete {position: absolute;top: -109px;left: -120px;cursor: default;}
 .no_mb1-1{display:none;}
.widget .categories-module li {padding-bottom: 8px;padding-top: 9px;}
}
</style>

<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- start main wrapper -->

<div id="wrapper">
	<div class="headline">
		<div class=" container">
			<!-- start headline section -->
			<div class="row">
				<div class="col-md-12 bg-full">

					<h2 class="banner_heading">
						Find a <span>Job </span> You Will <span> Love </span>
					</h2>
					<div class="sticky">
				<?php $form = ActiveForm::begin(['action'=>Url::toRoute(['index']),'options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form_1']]); ?>
                        <div class="group-sm group-top">

							<div class="group-item col-md-5 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="keyname"
										id="keyname" placeholder="Job title, skills, or company">
								</div>

							</div>

							<div class="group-item col-md-2 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" name="indexlocation"
										id="indexlocation" placeholder="Location">
								</div>
							</div>
							<div class="group-item col-md-2 col-xs-6">
								<div class="form-group">
									<select id="form-filter-experience-1" name="experience"
										 data-minimum-results-for-search="Infinity"
										class="form-control select2-hidden-accessible" tabindex="-1"
										aria-hidden="true">
										<option value="">Experience</option>
										<option value="Fresher">Fresher</option>
										<option value="Both">Both</option>
										<option value="0-1">Below 1 Year</option>
					    <?php
        for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
            $frn = $fr + 1;
            ?>
                                            <option
											value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                           <?php
        }
        ?>
                              </select>

								</div>
							</div>

							<div class="group-item col-md-2 col-xs-6">
								<div class="form-group">

									<select id="form-filter-salary-1" name="salary"
										data-minimum-results-for-search="Infinity"
										class="form-control select2-hidden-accessible" tabindex="-1"
										aria-hidden="true">
										<option value="">Salary</option>
										<option value="0 - 1.5 Lakhs">0 - 1.5 Lakhs</option>
										<option value="1.5 - 3 Lakhs">1.5 - 3 Lakhs</option>
										<option value="3 - 6 Lakhs">3 - 6 Lakhs</option>
										<option value="6 - 10 Lakhs">6 - 10 Lakhs</option>
										<option value="10 - 15 Lakhs">10 - 15 Lakhs</option>
										<option value="15 - 25 Lakhs">15 - 25 Lakhs</option>
										<option value="Above 25 Lakhs">Above 25 Lakhs</option>
										<option value="Negotiable">Negotiable</option>
									</select>
								</div>
							</div>


							<div
								class=" group-item reveal-block reveal-lg-inline-block col-md-1 col-xs-12">
							<?= Html::submitButton('Search', ['name'=>'indexsearch','class' => 'btn btn-primary element-fullwidth']) ?>
                          </div>

						</div>
                      <?php ActiveForm::end(); ?>
                      <div id="keynameSearchContainer"></div>
						<div id="IndexCitySearchContainer"></div>

					</div>

				</div>
				<div class="col-md-12 align-left">
					<h4>Need a Job ?</h4>
					<p>It is a long established fact that a reader will be distracted
						by the readable content of a page when looking at its layout. The
						point of using</p>
					<p>
						<a href="#" class="btn btn-default btn-light">Post a Job</a>
					</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- end headline section -->
	</div>

	<div class="recent-job">
		<!-- Start Recent Job -->
		<div class="container">

			<div class="row">

				<!-- Mobile   Job -->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="mobile_menu">
						<ul>
									<?php
        if (isset(Yii::$app->session['Employeeid'])) {
            ?>
									<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
							<li><a href="<?= Url::toRoute(['campus/jobbycompany'])?>">Job By
									Company</a></li>
							<li><a href="#job-opening">Top Job</a></li>
									<?php
        } else {
            ?>
									<li><a href="<?= Url::toRoute(['site/jobsearch'])?>">Job Search</a></li>
							<li><a href="<?= Url::toRoute(['site/hirecandidate'])?>">Hire
									Candidate</a></li>
							<li><a href="<?= Url::toRoute(['site/employersregister'])?>">Post
									a Job</a></li>
									<?php
        }
        ?>
								</ul>
					</div>
				</div>
				<!--  Mobile  Tab -->



				<div class="mobile_pad">
					<div class="col-xs-12">
						<div class="spacer-4"></div>
						<div class="widget-heading big-head">
							<span class="title">Most Popular Jobs </span>
						</div>
						<div class="spacer-5"></div>
								<?php
        if ($mostpopular) {
            $m = 0;
            foreach ($mostpopular as $key => $mval) {
                $m ++;
                ?>
							   <div
							class="three_main_block <?php if($m==5) echo 'last_one';?>">
							<a
								href="<?= Url::toRoute(['jobsearch/'.$mval->slug])?>">
								<img class="normal" src="<?=$url.$mval->whitephoto->Doc;?>"
								alt="jobsearch"> <img class="hvr" src="<?=$url.$mval->photo->Doc;?>"
								alt="career"><?=$mval->caption;?> </a>
						</div>
							   <?php
            }
        }
        ?>

						 </div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
					<div class="spacer-5"></div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"
						id="companies_left_logo">
						<div class="widget-heading">
							<span class="title">Hiring Companies </span>
						</div>
						<div class="spacer-5"></div>
						<div class="top_employores">
						    <?php
        foreach ($allemployers as $ek => $evalue) {
            if ($evalue->hiringcompanieslogo) {
                $lc = $url . $evalue->hiringcompanieslogo->Doc;
            } else {
                $lc = $imageurl . 'images/user.png';
            }

            ?>

								<a
								href="<?= Url::toRoute(['jobsearch/'.str_replace(' ','-',$evalue->Name)]);?>">
								<img src="<?=$lc;?>" style="" alt="Recruitment - MCB Hiring companies">
							</a>
							<?php
        }
        ?>
						   </div>
					</div>


					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"
						id="main-block_middile">
						<div class="mobile_pad">
							<div class="widget-heading">
								<span class="title">Browse jobs by Industries </span>
							</div>
							<div class="spacer-5"></div>
							<div class="top_employores">
								<ul>
								   <?php
        foreach ($allindustry as $ikey => $ivalue) {
            ?>
								   <li><a
										href="<?= Url::toRoute(['jobsearch/'.$ivalue->slug])?>"><?=$ivalue->IndustryName;?></a>
									</li>
								   <?php
        }
        ?>
                            	<div class="clear"></div>
								</ul>

							</div>
						</div>
						<div class="mobile_pad">
							<div class="mobile_show_only">
								<div class="widget-heading">
									<span class="title">Hiring Companies </span>
								</div>
								<div class="spacer-5"></div>
								<div class="top_employores" id="hiring_main">
							    <?php
        foreach ($allemployers as $ek => $evalue) {
            if ($evalue->hiringcompanieslogo) {
                $lc = $url . $evalue->hiringcompanieslogo->Doc;
            } else {
                $lc = $imageurl . 'images/user.png';
            }
            ?>
								<a
										href="<?= Url::toRoute(['jobsearch/'.str_replace(' ','-',$evalue->Name)]);?>">
										<img src="<?=$lc;?>" style="width: 81px; height: 35px;"
										alt="job Opportunity - MCB hiring Companies">
									</a>
							<?php
        }
        ?>
						   </div>
							</div>
						</div>

						<div class="mobile_pad">

							<div class="widget-heading">
								<span class="title">Top Consultants </span>
							</div>
							<div class="spacer-5"></div>

							<div class="top_employores" id="consultancy_logo_middle">
							<?php
    foreach ($topconsultancy as $ck => $cnval) {
        if ($cnval->logo) {
            $lc = $url . $cnval->logo->Doc;
        } else {
            $lc = $imageurl . 'images/user.png';
        }
        ?>
							  	<a
									href="<?= Url::toRoute(['site/alljob','keyword'=>'employer','key'=>$cnval->UserId])?>">
									<img src="<?=$lc;?>" style="width: 105px; height: 45px;"
									alt="job Seekers - My career bugs Top Consultants">
								</a>
							<?php
    }
    ?>
									<div class="clear"></div>
							</div>

						</div>

					</div>

					<div class="clear"></div>

					<div class="mobile_pad">
						<?php
    if ($allcity) {
        ?>
						    <div class="widget-heading">
							<span class="title">Browse jobs by City </span>
						</div>
						<div class="spacer-5"></div>


						<div class="top_employores" id="browse_city">
							<ul>
								<li><a href="https://www.mycareerbugs.com/jobsearch?city=Bangalore"> <img class="normal"
										src="images/banglore_color.png" alt="Jobs in Bangalore"> <img class="hvr"
										src="images/banglore_white.png" alt="Jobs in Bangalore"> Jobs in Bangalore
								</a></li>
								<li><a href="https://www.mycareerbugs.com/jobsearch?city=Delhi"> <img class="normal"
										src="images/delhi_orange.png" alt="Jobs in Delhi"> <img class="hvr"
										src="images/delhi_white.png" alt="Jobs in Delhi"> Jobs in Delhi
								</a></li>
								<li><a href="https://www.mycareerbugs.com/jobsearch?city=Chennai"> <img class="normal"
										src="images/chennai_orange.png" alt="Jobs in Chennai"> <img class="hvr"
										src="images/chennai_white.png" alt="Jobs in Chennai"> Jobs in Chennai
								</a></li>
								<li class="float-rt"><a href="https://www.mycareerbugs.com/jobsearch?city=Kolkata"> <img class="normal"
										src="images/kolkata_color.png" alt="Jobs in Kolkata"> <img class="hvr"
										src="images/kolkata_white.png" alt="Jobs in Kolkata">Jobs in Kolkata
								</a></li>
								<li><a href="https://www.mycareerbugs.com/jobsearch?city=Hyderabad"> <img class="normal"
										src="images/hyderabad_color.png" alt="Jobs in Hyderabad"> <img class="hvr"
										src="images/hyderabad_white.png" alt="Jobs in Hyderabad">Jobs in Hyderabad
								</a></li>
								<li><a href="https://www.mycareerbugs.com/jobsearch?city=Pune"> <img class="normal" src="images/pune_color.png" alt="Jobs in Pune">
										<img class="hvr" src="images/pune_white.png"alt="Jobs in Pune" >Jobs in Pune
								</a></li>
								<li><a href="https://www.mycareerbugs.com/jobsearch?city=Mumbai"> <img class="normal"
										src="images/mumbai_orange.png" alt="Jobs in Mumbai"> <img class="hvr"
										src="images/mumbai_white.png" alt="Jobs in Mumbai">Jobs in Mumbai
								</a></li>
								<li class="float-rt"><a href="<?= Url::toRoute(['cities'])?>"> <img
										class="normal" src="<?=$imageurl;?>images/view_color.png"
										alt="jobs - My Career Bugs" > <img class="hvr"
										src="<?=$imageurl;?>images/view_white.png"
										alt="jobs My Career Bugs">View All
								</a></li>
							</ul>
						</div>


						<div class="top_employores" id="browse_city" style="display: none">
							<ul>
								<?php
        $c = 0;

        foreach ($allcity as $ckey => $cvalue) {
            $c ++;
            if ($cvalue->cityimage) {
                $color = $url . $cvalue->cityimage->Doc;
                $white = $url . $cvalue->citywhiteimage->Doc;
            }
            ?>
									 <li <?php if($c==4) echo 'class="float-rt"';?>><a
									href="<?= Url::toRoute(['site/alljob','keyword'=>'city','key'=>$cvalue->CityName])?>">
										<img class="normal" src="<?=$color;?>" alt="Hiring Now My Career Bugs"> <img
										class="hvr" src="<?=$white;?>" alt="Hiring Now My Career Bugs"> Jobs in <?=$cvalue->CityName;?></a>
								</li>
								<?php
        }
        if ($c == 7) {
            ?>

									 <li class="float-rt"><a href="<?= Url::toRoute(['cities'])?>">
										<img class="normal" src="<?=$imageurl;?>images/view_color.png"
										alt="Internship My Career Bugs"> <img class="hvr"
										src="<?=$imageurl;?>images/view_white.png"
										alt="Internship My Career Bugs">View All
								</a></li>
									 <?php
        }
        ?>
								 </ul>
						</div>
						 <?php
    }
    ?>


						 <?php
    if (! empty($topcompany)) {
        ?>
						  <div class="widget-heading">
							<span class="title">Top Company Jobs by Industries</span>
						</div>
						<div class="spacer-5"></div>
						<div class="top_employores" id="company_job_main">
								<?php
        foreach ($topcompany as $tk => $tval) {
            ?>
									<ul style="height: auto; height: 200px; overflow:hidden; margin-bottom:20px;">
								<li><a href="" class="head_main no-mar"><?=$tval['category'];?></a></li>
									<?php
            if ($tval['companyname']) {
                foreach ($tval['companyname'] as $ck => $cval) {
                    ?>
									<li><a
									href="<?= Url::toRoute(['jobsearch/'.str_replace(' ','-',$cval)]);?>"><?=$cval;?></a></li>
									<?php
                }
            }
            ?>
									</ul>
								<?php
        }
        ?>
							     </div>
						   <?php
    }
    ?>
					</div>
					<div class="clear"></div>
					  <?php
    if ($walkin) {
        ?>
						 <div class="mobile_pad">
						<div class="widget-heading">
							<span class="title"> Companies Walk-In Interviews </span>
						</div>
						<div class="spacer-5"></div>
						<div id="walk_in_interviews">
							<ul class="related-post">
					<?php
        $w = 0;
        foreach ($walkin as $wkey => $wvalue) {
            $w ++;
            ?>
                                        <li
									<?php if($w==3) echo 'class="no_padding"';?>><a
									href="<?= Url::base().'/job/'.$wvalue->Slug;?>">
										<span><i class="fa fa-suitcase"></i>Designation:  <?=$wvalue->Designation;?> </span>
										<span><i class="fa fa-map-marker"></i>Place: <?=$wvalue->Location;?> </span>
										<span><i class="fa fa-clock-o"></i>Walking Date: <?=date('d M Y',strtotime($wvalue->WalkinFrom)).' - '.date('d M Y',strtotime($wvalue->WalkinTo));?></span>
								</a></li>
                                        <?php
        }
        if ($w == 5) {
            ?>
                                        <li class="no_padding"><a
									href="<?= Url::toRoute(['site/walkin'])?>"> <span
										class="view_all">View All</span>
								</a></li>
					<?php
        }
        ?>
										<div class="clear"></div>
							</ul>

							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
					<?php
    }
    ?>
						</div>




				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="right-side">

					<div class="mobile_pad">
						<div class="spacer-5"></div>

						<div class="widget-heading">
							<span class="title">Recent Jobs </span>
						</div>
						<div class="spacer-5"></div>
						<div class="rsw">
							<aside>
								<div class="widget">

									<ul class="related-post">
					<?php
    if ($topjob) {
        foreach ($topjob as $jkey => $jvalue) {
            ?>
                                        <li><a
											href="<?= Url::base().'/job/'.$jvalue->Slug;?>"><?=$jvalue->JobTitle;?> </a>
											<span><i class="fa fa-suitcase"></i>Designation:   <?=$jvalue->Designation;?></span>
											<span><i class="fa fa-map-marker"></i>Place: <?=$jvalue->Location;?> </span>
											<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?></span>
										</li>
					<?php
        }
    }
    ?>
										<a href="<?= Url::toRoute(['site/recentjob'])?>"
											class="sidebar_view_all"> View all </a>


									</ul>
								</div>

							</aside>
						</div>
						<?php
    if ($topjobopening) {
        ?>
					  	<div id="job-opening">
							<div class="job-opening-top">
								<div class="widget-heading fl-left">
									<span class="title">Top Job Opening</span>
								</div>

								<div class="job-opening-nav">
									<a class="btn prev"></a> <a class="btn next"></a>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
							<br />
							<div id="job-opening-carousel" class="owl-carousel">
							<?php
        foreach ($topjobopening as $tk => $tval) {
            if ($tval->docDetail) {
                $doc = $url . $tval->docDetail->Doc;
            } else {
                $doc = $imageurl . 'images/user.png';
            }
            ?>
								<div class="item-home">
									<div class="job-opening">
										<div style="width: 100%; height: 150px; float: left;">
											<img src="<?=$doc;?>" class="img-responsive"
												alt="MCB Top job opening" />
										</div>
										<div class="job-opening-content"
											style="float: left; min-height: 170px;">
											<a
												href="<?= Url::base().'/job/'.$tval->Slug;?>"><?=$tval->JobTitle;?></a><br />
											<p><?=substr(htmlspecialchars_decode($tval->JobDescription),0,100);?></p>
										</div>

										<div class="job-opening-meta clearfix">
											<div class="meta-job-location meta-block">
												<i class="fa fa-map-marker"></i><?=$tval->Location;?></div>
											<div class="meta-job-type meta-block">
												<i class="fa fa-user"></i><?=$tval->JobType;?></div>
										</div>
									</div>
								</div>
								<?php
        }
        ?>
							</div>
						</div>

						 <?php
    }
    ?>

		 <div class="modal add-resume-modal" id="myModal_buy_a_plan"
							tabindex="-1" role="dialog" aria-labelledby="">
							<div class="modal-dialog modal-md" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title" id="myModalLabel">Add New Resume</h4>
									</div>
									<div class="modal-body">
										<div class="input-group image-preview form-group ">
											<input type="text"
												class="form-control image-preview-filename"
												disabled="disabled"> <span class="input-group-btn">
												<button type="button"
													class="btn btn-default image-preview-clear"
													style="display: none;">
													<span class="glyphicon glyphicon-remove"></span> Clear
												</button>
												<div class="btn btn-default image-preview-input upload_btn">
													<i class="fa fa-file" aria-hidden="true"></i> <span
														class="image-preview-input-title">Browse</span> <input
														type="file" accept="file_extension"
														name="input-file-preview" />
												</div>
											</span>
										</div>
										<p>Only pdf and doc files are accepted</p>
									</div>
									<div class="modal-footer">
										<a href="" type="button" class="btn btn-default">Save Resume</a>
									</div>
								</div>
							</div>
						</div>



						<div class="clearfix"></div>
<div class="no_mb1-1">
	<div class="widget-heading"><span class="title">Hot Categories </span></div>

			<div class="spacer-5"></div>
        <div class="widget">
                                    <ul class="categories-module" style="height: 608px;overflow: hidden;">
                                       <?php
								if($hotcategory)
								{
								foreach($hotcategory as $hkey=>$hvalue)
								{
								    $string=Yii::$app->myfunctions->clean(trim($hvalue->CategoryName));
								?>
								<li> <a href="<?= Url::toRoute(['site/jobsearch/'.$string])?>"> <?=$hvalue->CategoryName;?> <span>(<?=$hvalue->cnt;?>)</span> </a> </li>
								<?php
								}
								}
								?>
                                    </ul>
                                </div>
						</div>
						<!--
						<div class="adban_block">
							<a href="tel:918240369924" target="_blank"> <img
								src="<?=$imageurl;?>images/adban_block/ban.gif"
								alt="Jobhunt">
							</a>
						</div>
			  -->


						<!-- adban_block  main -->
						<!--adban_block main -->
						<div class="adban_block" style="display: none">
							<a href="tel:918240369924" target="_blank"> <img
								src="<?=$imageurl;?>images/add.jpg" alt="Jobhunt">
							</a>
						</div>
						<!-- adban_block  main -->

						<!--adban_block main -->
						<div class="adban_block">
							<a href="tel:918240369924" target="_blank"> <img
								src="<?=$imageurl;?>images/adban_block/tata.jpg"
								alt="Jobhunt">
							</a>
						</div>
						<!-- adban_block  main -->




					</div>
					<div class="clearfix"></div>
				</div>





			</div>
		</div>
	</div>
	<!-- end Recent Job -->

	<div class="spacer-5"></div>

		 <?php
if (! isset(Yii::$app->session['Employerid'])) {
    ?>





							<div id="index_wall">

		<div class="container">
			<div class="widget-heading">
				<span class="title"> MCB Wall Post</span>
			</div>
			<div class="spacer-5"></div>


			<div id="wall_top_first">

					    <?php

    if ($allpost) {
        foreach ($allpost as $postkey => $postvalue) {
            if ($postvalue->employer->LogoId != '') {
                $logo = $url . $postvalue->employer->logo->Doc;
            } else {
                $logo = $url . 'images/user.png';
            }
            ?>
						 <div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							<img class="img-circle" src="<?=$logo?>"
								alt="MCB Wall Company Profile"> <span class="username"><a
								href="http://mycareerbugs.com/wall/searchcompany?userid=<?php echo $postvalue->EmployerId;?>"><?=$postvalue->employer->Name;?></a></span>
							<span class="description">Shared publicly - <?=date('h:i A D M',strtotime($postvalue->OnDate));?>   - <?=$postvalue->Locationat;?></span>
						</div>
					</div>
	<style>#index_wall .box.box-widget{height:212px}
	.user-block .description{height: 14px; overflow: hidden;}
	.box-body p {  height: 53px;  overflow: hidden;margin-bottom: 12px;    margin-top: 11px;}
	.heading_m{font-size: 11px;color: #807e7e;height: 37px;overflow: hidden;margin: 0 0 5px 0;line-height: 14px;background: #f4f4f4;padding: 5px 10px; }
	</style>

					<div class="box-body" style="display: block;">
						<h2 class="heading_m" ><?php echo $postvalue->PostTitle;?></h2>
						  	<p><?php echo $postvalue->Post;?></p>

					<?php /*
						<p>
                                 <?php
            // $srt= str_replace("\n", '<br>', $postvalue->Post);
            $arr_Post = explode("\n", $postvalue->Post);
            for ($i = 0; $i < 3; $i ++) {
                echo $arr_Post[$i] . '<br>';
            }

            ?>
								 </p> */?>
								  <?php

			if(!empty($postvalue->Slug)){
				$urlLink = Url::base().'/wall-post/'.$postvalue->Slug;
			}else{
				$urlLink = Url::toRoute(['/wall/post-view','id' =>  $postvalue->WallPostId]);
			}

            if (! isset(Yii::$app->session['Employeeid']) && ! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Campusid']) && ! isset(Yii::$app->session['Teamid'])) {
                ?>
							       <a href="<?= $urlLink; ?>"
							class="btn btn-default btn-xs"><i class="fa fa-eye"></i> View</a>
								   <!-- <?= Url::toRoute(['wall/searchcompany','userid'=>$postvalue->EmployerId])?> -->
								  <?php }else{?>
									 <a
							href="<?= $urlLink;?>"
							class="btn btn-default btn-xs"><i class="fa fa-eye"></i> View</a>

								<?php  } ?>

								  <span class="pull-right text-muted"><?=count($postvalue->likes);?> likes - <?=count($postvalue->comments);?> comments</span>
					</div>
				</div>


						<?php
        }
    }
    ?>
					    <div class="box box-widget">
					<a href="<?= Url::toRoute(['wall/index'])?>"> <img src="/img/wall_l.jpg"
						style="width: 100%" alt="MCB wall"></a>

				</div>
			</div>


		</div>
	</div>





	<div class="container new_citiew">

		<div class="widget-heading">
			<span class="title"> Top Jobs
			<a href="<?= Url::toRoute(['site/recentjob'])?>"
				style=" position: absolute; right: 11px; top:25px; color: #fff; background: #f16b22; padding: 5px 10px 3px 10px; font-size: 12px; line-height: 15px;">View
					all</a>

		</div>
		<div class="spacer-5"></div>

		<div class="rsw" id="index-top_jobs">
			<aside>
				<div class="widget">

					<ul class="related-post">
										<?php
    if ($topjob) {
        foreach ($topjob as $tkey => $tvalue) {
            ?>
                                        <li><a
							href="<?= Url::base().'/job/'.$tvalue->Slug;?>"> <?=$tvalue->CompanyName;?>  </a>
							<span><i class="fa fa-suitcase"></i>Position: <?=$tvalue->Designation;?>  <!-- <?=$tvalue->position->Position;?> -->  </span>
							<span><i class="fa fa-map-marker"></i>Place:  <?=$tvalue->Location;?> </span>
							<span><i class="fa fa-clock-o"></i>Post Time: <?=date('h:i A',strtotime($tvalue->OnDate));?> </span>
						</li>
                                        <?php
        }
    }
    ?>
                                    </ul>
				</div>

			</aside>
		</div>
	</div>







	<div id="index_wall_hr">

		<div class="container">
			<div class="widget-heading" style="position:relative;">
				<span class="title"> Top 10  HR</span>


				<a href="<?= Url::toRoute(['site/all-hr'])?>" style="position: absolute; right: 11px; top: 25px; color: #fff; background: #f16b22; padding: 5px 10px 3px 10px; font-size: 12px;
				line-height: 15px;">View
					all HR</a>
							</div>
			<div class="spacer-5"></div>


			<div id="wall_top_first">

					    <?php

    if ($allhrs) {
        foreach ($allhrs as $postkey => $postvalue) {

            $logo = Documents::getImageByAttr($postvalue, 'LogoId', 'logo');

            ?>
						 <div class="box box-widget" style="height: 109px">
					<div class="box-header with-border">
						<div class="user-block" style="min-height: 55px;">
							<img class="img-circle" src="<?=$logo?>" alt="HR Profile">

									<?php $emp_name = preg_replace('/\s+/', '', $postvalue->Name); ?>
									<span class="username"><a
								href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".$postvalue->UserId?>"><?=$postvalue->Name;?></a>
<div>
                        <span style="font-size:11px;font-weight:normal"> <?php echo MyHelpers::totalhrpostjob($postvalue->UserId) ?> Job post</span>
                        <span style="font-size:11px; margin:0 5px;font-weight:normal">  - </span>
                        <span style="font-size:11px;font-weight:normal"> <?php echo MyHelpers::totalwallpostjob($postvalue->UserId) ?> Wall post</span> </span>
	</div>
						</div>
						<span style="margin:3px 0 0 0; display:block; clear:both; overflow:hidden"> <a
							href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".$postvalue->UserId?>"
							class="btn btn-default btn-xs" style="color: #000;width: 48.5%; float: left;line-height: 23px !Important;margin-right: 3%;padding: 2px 0 0 0;">
						    <i class="fa fa-eye"></i>  View</a>

							<a
							href="<?=Yii::$app->urlManager->createAbsoluteUrl('').$emp_name."-".$postvalue->UserId?>"
							class="btn btn-default btn-xs" style="color: #000;width: 48.5%; float: left;line-height: 23px !Important; padding: 2px 0 0 0;">
							   <i class="fa fa-plus"></i>  Follow</a>

						</span>
						<div class="clear"></div>
					</div>

				</div>


						<?php
        }
    }
    ?>

					   </div>


		</div>
	</div>


	<div class="upload_resume_main">
		<div class="container">
			<div class="col-sm-7 col-xs-12">
				<p>Upload your resume and let your next job find you.</p>
			</div>
			<div class="col-sm-3 col-xs-12">
						<?php
    if (isset(Yii::$app->session['Employeeid'])) {
        ?>
						<button type="button" class="post-resume-button"
					data-toggle="modal" data-target="#myModal_cvupload">
					Upload Your Resume<i class="icon-upload grey"></i>
				</button>
						<?php
    } else {
        ?>
						<a href="<?= Url::toRoute(['site/jobseekersregister'])?>">
					<button type="button" class="post-resume-button">
						Upload Your Resume<i class="icon-upload grey"></i>
					</button>
				</a>
							<?php
    }
    ?>
					 	</div>
			<div class="col-sm-2 col-xs-12"></div>
		</div>
	</div>



		<?php

    if (! isset(Yii::$app->session['Employeeid'])) {
        ?>
		<div class="step-to">
		<div class="container">
			<h1>Easiest Way To Use for Candidate</h1>
			<p>
				Here, you can check out easiest way effective for My Career Bugs
				candidates. You can easily <br> create own account, profile, send
				resume and get valid job. Now, you don’t need to worry about
				searching <br> for the valid and qualification relevant job for
				anymore. Get ready to follow these simple steps to get job
				immediately.
			</p>

			<div class="step-spacer"></div>
			<div id="step-image">
				<div class="step-by-container">
					<div class="step-by">
						<a href="<?= Url::toRoute(['site/jobseekersregister'])?>">
							<div class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-1.png"
										alt="My Career Bugs Candidate steps 1" />
								</div>
							</div>
						</a>
						<h5>Create an Account</h5>
					</div>

					<div class="step-by">

						<a href="<?= Url::toRoute(['site/jobseekersregister'])?>"><div
								class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-2.png"
										alt="My Career Bugs Candidate steps 2" />
								</div>
							</div> </a>
						<h5>Create your profile</h5>
					</div>

					<div class="step-by">

						<a href="<?= Url::toRoute(['site/jobseekersregister'])?>"><div
								class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-3.png"
										alt="My Career Bugs Candidate steps 3" />
								</div>
							</div> </a>
						<h5>Send your Resume</h5>
					</div>

					<div class="step-by">

						<a href="<?= Url::toRoute(['site/jobsearch'])?>"><div
								class="step-by-inner">
								<div class="step-by-inner-img">
									<img src="<?=$imageurl;?>images/step-icon-4.png"
										alt="My Career Bugs Candidate steps 4" />
								</div>
							</div> </a>
						<h5>Get your Job</h5>
					</div>

				</div>
			</div>
			<div class="step-spacer"></div>
		</div>
	</div>
		<?php
    }
    ?>





		 	<div class="recommendations_banner upload_resume_main"
		id="jobalertbox">
		<div class="container">
			<div class="col-sm-9 col-xs-12">
				<p>No job recommendations yet, but opportunity is out there!</p>
			</div>
			<div class="col-sm-3 col-xs-12">

				<a href="" class="btn-default  type=" data-toggle="modal"
					data-target="#myModal_jobalert"><button type="button"
						class="post-resume-button">Create Job alert</button></a>
			</div>
			<div class="col-sm-2 col-xs-12"></div>
		</div>
	</div>
		<?php
}
?>


		<div class="developed_by_e-developed_technology" id="company-post"
		style="display:none">
		<div class="row">
			<h1>Companies Who Have Posted Jobs</h1>

			<div id="company-post-list" class="owl-carousel company-post">
					<?php
    if ($allcompany) {
        foreach ($allcompany as $ckey => $cvalue) {
            if ($cvalue->employer->logo) {
                $doc = $url . $cvalue->employer->logo->Doc;
            } else {
                $doc = $imageurl . 'images/user.png';
            }
            if(!file_exists($doc)){
				$doc = $imageurl . 'images/user.png';
			}
            ?>
					<a href="<?=$cvalue->Website;?>" target="_blank"><div
						class="company" style="background: #fff;">
						<img src="<?=$doc;?>" class="img-responsive" alt=""
							style="height: 100px;" />
					</div></a>
				<?php
        }
    }
    ?>

				</div>
		</div>
	</div>





	<div class="testimony">
		<div class="container">
			<h1><?=$peoplesayblock->Heading;?></h1>
				<?=htmlspecialchars_decode($peoplesayblock->Content);?>

			</div>
		<div id="sync2" class="owl-carousel">
				<?php
    foreach ($allfeedback as $fk => $fvalue) {
        if ($fvalue->docDetail) {
            $doc = $url . $fvalue->docDetail->Doc;
        } else {
            $doc = $imageurl . 'images/user.png';
        }
        ?>
				<div class="testimony-image">
				<img src="<?=$doc;?>" class="img-responsive"
					alt="MCB Testimonial Block" style="height: 150px; width: 150px;" />
			</div>
				<?php
    }
    ?>
			</div>

		<div id="sync1" class="owl-carousel">
				<?php
    foreach ($allfeedback as $fk => $fvalue1) {
        ?>
				<div class="testimony-content container">
					<?=htmlspecialchars_decode($fvalue1->Message);?>
					<p>
						<?php echo $fvalue1->Name.','.$fvalue1->Designation.','.$fvalue1->Companyname;?>
					</p>
				<div class="media-testimony">
						<?php
        if ($fvalue1->Twitterlink != '') {
            ?>
						<a href="<?=$fvalue1->Twitterlink;?>" target="blank"><i
						class="fa fa-twitter twit"></i></a>
						<?php
        }
        if ($fvalue1->LinkedinLink != '') {
            ?>
						<a href="<?=$fvalue1->LinkedinLink;?>" target="blank"><i
						class="fa fa-linkedin linkedin"></i></a>
						<?php
        }
        if ($fvalue1->Facebooklink != '') {
            ?>
						<a href="<?=$fvalue1->Facebooklink;?>" target="blank"><i
						class="fa fa-facebook fb"></i></a>
						<?php
        }
        ?>
					</div>
			</div>
				<?php
    }
    ?>
			</div>
	</div>




		<?php
if (! isset(Yii::$app->session['Employeeid']) && ! isset(Yii::$app->session['Employerid']) && ! isset(Yii::$app->session['Campusid']) && ! isset(Yii::$app->session['Teamid'])) {
    ?>
		<div class="advertise_your_post">
		<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
			<img src="<?=$imageurl;?>images/job_seekers.png"
				alt="MCB Candidate Register "> <a
				href="<?= Url::toRoute(['jobseekersregister'])?>"> Register </a>
		</div>

		<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
			<img src="<?=$imageurl;?>images/team_orange.png"
				alt="MCB Team Register"> <a
				href="<?= Url::toRoute(['team/index'])?>">Register </a>
		</div>


		<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
			<img src="<?=$imageurl;?>images/company.png"> <a
				href="<?= Url::toRoute(['site/employersregister'])?>"
				alt="MCB Employers Register">Register </a>
		</div>


		<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
			<img src="<?=$imageurl;?>images/campus_register.png"> <a
				href="<?= Url::toRoute(['campus/campusregister'])?>"
				alt="MCB Campus Register">Register </a>
		</div>
	</div>
		<?php
}
?>


	<div class="modal add-resume-modal" id="myModal_cvupload" tabindex="-1"
		role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-md" role="document">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30']]); ?>
                <div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Quick Review and update
						your profile</h4>
				</div>
				<div class="modal-body">
					<div class="input-group image-preview form-group ">
							<?php
    if ($user->CVId != 0) {
        $na = explode(" ", $user->Name);
        $name = $na[0];
        $extar = explode(".", $user->cV->Doc);
        $ext = $extar[1];
        ?>
							<a href='<?=$url.$user->cV->Doc;?>' class="cv"
							download="<?=$name.'.'.$ext;?>"><?=$user->Name;?> Resume</a><br />
						<br />
						<?php
    }
    ?>
							 <?= $form->field($user, 'CVId')->fileInput(['required'=>true,'accept'=>'.doc, .docx,.rtf,.pdf','id'=>'cvid'])->hint('doc,docx,pdf,rtf - 2MB max') ?>
                        </div>
				</div>
				<div class="modal-footer">
						<?= Html::submitButton('Upload and Apply', ['name'=>'jobalertsearch','class' => 'btn btn-lg btn-primary btn-block']) ?>

                    </div>
			</div>
				<?php ActiveForm::end(); ?>
            </div>
	</div>

	<div class="modal add-resume-modal" id="myModal_jobalert" tabindex="-1"
		role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-md" role="document"
			style="height: 400px; overflow: auto;">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30']]); ?>
                <div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Tell us what kind of jobs
						you want</h4>
				</div>
				<div class="modal-body">
					<div class="input-group image-preview form-group ">
                        <?= $form->field($jobalert, 'Industry')->dropDownList(Industry::getIndustryLists(),['id'=>'alertindustry'])->label('Industry')  ?>
						<?php $form->field($jobalert, 'Keyword')->textInput(['maxlength' => true,'placeholder'=>'Skills, Designations, Roles, Companies','id'=>'alertkeywords'])?>
                        </div>
					<div class="input-group image-preview form-group ">
						<!-- <input id="citylist" type="text" class="form-control"name="CompanyWallPost[Locationat]" placeholder="City"> -->

							<?=$form->field($jobalert, 'Location')->textInput(['maxlength' => true,'placeholder' => 'Enter the cities you want to work in','id' => 'alertlocation'])?>
							<div id="locationContainer"
							style="background-color: white !important; margin-top: 28px;"></div>
					</div>
					<div class="input-group image-preview form-group ">
							<?= $form->field($jobalert, 'Keyword')->dropDownList(Position::getPositionLists(),['id'=>'alertkeywords'])->label('Role')  ?>
							<?php $form->field($jobalert, 'Industry')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired industry where you want to work','id'=>'alertindustry']) ?>
                        </div>
					<div class="input-group image-preview form-group "
						style="display: none;">
							<?= $form->field($jobalert, 'JobCategory')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired category where you want to work','id'=>'alertjobcategory']) ?>
                        </div>
					<div class="input-group image-preview form-group "
						style="display: none;">
							<?= $form->field($jobalert, 'NameJobAlert')->textInput(['maxlength' => true,'value'=>'Careerbug','placeholder'=>'Enter a name that will help you recognize this Job Alert']) ?>
                        </div>
					<div class="input-group image-preview form-group ">
							<?= $form->field($jobalert, 'EmailId')->textInput(['maxlength' => true,'placeholder'=>'Enter your Email ID where you would like to receive this Job Alert']) ?>
                        </div>
				</div>
				<div class="modal-footer">
						<?= Html::submitButton('Create Job alert', ['name'=>'jobalertsearch','class' => 'btn btn-lg btn-primary btn-block']) ?>

                    </div>
			</div>
				<?php ActiveForm::end(); ?>
            </div>
	</div>

    <link href="css/jquery-ui.css" rel="Stylesheet">

    <script src="js/jquery-ui.js" ></script>

    <?php
    $script = <<< JS

     $(document).ready(function () {  
            $("#alertlocation").autocomplete({ 
            	source: 'wall/getcities', 
            	minLength:1,
            	appendTo: "#locationContainer" 
            });  
        }); 
        
        
        $(document).ready(function(){
  $('.need_help span' ).click(function(){
			$('.need_help').animate({right:0});  
				});});
			$('.closed a').click(function(){
				$('.need_help').animate({right:-270}); 
				return true
	});
	
JS;
    $this->registerJs($script, \yii\web\View::POS_END);
    /*
     * $url = '/backend/web/';
     * $this->registerJsFile(Yii::$app->basePath.'js/jquery.auto-complete.js', \yii\web\View::POS_END);
     * $this->registerCssFile(Yii::$app->basePath.'css/jquery.auto-complete.css', \yii\web\View::POS_END);
     */
    ?>