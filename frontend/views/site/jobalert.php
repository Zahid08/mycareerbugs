<?php
$this->title = 'Job Alert - My Career Bugs';
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use common\models\Position;
$blueWhiteRoles = Position::getPositionLists();

?> <meta name="keywords" content="My Career Bugs, MCB Jobs, Walk-ins, walk in interview jobs" />  
<meta name="description" content="Top Employers hiring through Walk-ins. Fresher and Experienced Walk-Ins. Create a Resume & apply for walk in interview jobs." /> 


<style>
.only_show{top:45px;}
#keynameSearchContainer .ui-autocomplete { position: absolute; top: 55px; left: 16px;}
#IndexCitySearchContainer .ui-autocomplete { position: absolute; top: 55px; left: 365px;}
#IndexCitySearchContainer .ui-autocomplete { height:auto }
.find_a_job{display:none}   
@media only screen and (max-width: 767px) {
.bg-full {width: 100%;}
.search_result{padding: 23px 0 1px 0 !Important;}
#keynameSearchContainer .ui-autocomplete { position: absolute; top: 57px; left:0px;}
#IndexCitySearchContainer .ui-autocomplete { position: absolute; top: 110px; left:15px}
#clear {margin-top: 0 !important;text-align: right;float: right;width: 100%;}
.number_drop_down {margin: 0px 6px 7px 18px;float: right;width: 188px;}
.form-control23.select2-hidden-accessible{background:#fff;}
#select_per_page {margin: 0px 0 0 0;}
#right-side{background:#f2f2f2; margin-bottom: 0px !important;}
.spacer-5 {width: 100%;height: 16px;}

}
.need_help{display:none;}
.info.bottom .left-text.min-h21{height: 73px;
    overflow: hidden;}
.select2-results__option{font-size:12px; }
.select2-container--krajee .select2-selection--multiple .select2-selection__rendered{font-size:12px;}

</style>

<div id="wrapper">
    <!-- start main wrapper -->
    <div class="headline_inner">
        <div class=" container">
            <h2>Tell us what kind of jobs you want</h2>
            <div class="clearfix"></div>
        </div>
        <!-- end headline section -->
    </div>
    <div class="inner_page">
        <div class="container">
           <div class="row ">
                    <div class="col-xs-12 col-sm-12">
						<div id="myModal_jobalert" >
						<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30']]); ?>
                        <div class="panel panel-default" style="margin-bottom:0px;">

							<div id="collapseOne19" class="panel-collapse collapse in">
								<div class="panel-group" style="padding:0; margin-bottom:0px">
									<div class="panel panel-default" style="border:0px;box-shadow: none;">
										<div class="panel-heading sty" style="background: #fff !important;padding: 9px 28px 5px 28px;color: #fff !important;">
										    
										    <ul style="margin: 20px 0px 22px; visibility: visible;" class="nav nav-tabs">
                                              <li data-toggle="tab" class="active collar_type"><a href="#">Blue Collar</a></li>
                                              <li data-toggle="tab" class="collar_type"><a href="#">White Collar</a></li>
                                            </ul>
                                            <input type="hidden" name="CollarType" id="whitefield"/>
                                            <input type="hidden" name="CollarType" id="bluefield"/>
                                            <div class="col-md-2">
												<a style="padding: 6px 0">
													<h4 class="panel-title" style="font-size:12px;color:#000">	
													<input type="radio" checked="checked" value="blue" disabled name="CollarTypeRadio" id="blue_caller" class="collar-type-alert"> Blue Collar <span class="checkmark"></span></h4>
												</a>
											</div>
											<div class="col-md-2">
												<a style="padding: 6px 0">
													<h4 class="panel-title" style="font-size:12px; color:#000">
													<input type="radio" value="white" name="CollarTypeRadio" disabled id="white_caller" class="collar-type-alert"> White Collar <span class="checkmark"></span></h4> 
												</a>
											</div>
										</div>
										<div class="panel-body" >	
											<div id="blue-collar-fields" style="">
												<div class="input-group image-preview form-group">
												    <?php echo $form->field($education, 'RoleId[]')->dropDownList(Position::getPositionLists(),['data-id-count' => 0,'prompt' => 'Select Role'])->label('Role')?>
												</div>
											</div>

											<div id="white-collar-fields" style="display:none;">										

												<div class="input-group image-preview form-group">
													<label class="control-label" for="JobCategory">Category</label>
													<select id="JobCategory" style="padding:10px; font-size:12px;width: 100%;">
														<option value="">Select Catogory</option>
														 <?php if(!empty($whiteCategories)){
                        									foreach($whiteCategories as $category){?>
                        										<option value="<?php echo $category->id?>"><?=$category->name;?></option>
                        									<?php  }
                        									}?>
													</select>
												
												</div>
												<div class="input-group image-preview form-group">
													<label class="control-label" for="jobalert-role">Role</label>
													<select style="padding:0px; font-size:12px;width: 100%;" name="white_callar_role" id="white_callar_roles" multiple="multiple">
														<option value="">Role</option>
													</select>
												</div>
												
											</div>
										
											<div class="input-group image-preview form-group ">
												<label class="control-label" for="jobalert-experience">Experience (Years)</label>
												<select id="jobalert-experience" name="experience" class="form-control">
														<option value="">Experience</option>
                										<option value="Fresher">Fresher</option>
                										<option value="0-1">Below 1 Year</option>
                                					    <?php
                                                        for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
                                                            $frn = $fr + 1;
                                                            ?>
                                                        <option
            											value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                                       <?php
                                                        }
                                                        ?>
												</select>
											</div>

											<div class="input-group image-preview form-group ">
												<label class="control-label" for="jobalert-salary">Salary</label>
												<select id="jobalert-salary" name="salary" class="form-control" >
													<option value="">Salary</option>
													<option value="0 - 1.5 Lakhs">0 - 1.5 Lakhs</option>
													<option value="1.5 - 3 Lakhs">1.5 - 3 Lakhs</option>
													<option value="3 - 6 Lakhs">3 - 6 Lakhs</option>
													<option value="6 - 10 Lakhs">6 - 10 Lakhs</option>
													<option value="10 - 15 Lakhs">10 - 15 Lakhs</option>
													<option value="15 - 25 Lakhs">15 - 25 Lakhs</option>
													<option value="Above 25 Lakhs">Above 25 Lakhs</option>
													<option value="Negotiable">Negotiable</option>
												</select>
											</div>
											
											<div class="input-group image-preview form-group ">
												<?= $form->field($jobalert, 'Keyword')->textInput(['maxlength' => true,'placeholder'=>'Skills, Designations, Companies','id'=>'alertkeywords']) ?>
											</div>

											<div class="input-group image-preview form-group ">
											    <label class="control-label" for="AllUser[PreferredLocation][]">Location</label>
												<select class="questions-category form-control" id="naukari-location" name="AllUser[PreferredLocation][]" multiple="multiple"></select>
											</div>
											<div class="input-group image-preview form-group ">
												<!--<?= $form->field($jobalert, 'Industry')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired industry where you want to work','id'=>'alertindustry']) ?>-->
												<select name="AllUser[IndustryId]" required="" class="form-control bfh-states" style="border: 1px solid rgb(204, 204, 204);">
                    
										<option selected="selected" value="0">- Select an Industry -</option>
																				<option value="76">Accounting / Finance</option>
																				<option value="71">Advertising / MR / PR / Events Jobs</option>
																				<option value="77">Agriculture / Dairy</option>
																				<option value="80">Animation / Gaming</option>
																				<option value="78">Architecture / Interior Design</option>
																				<option value="79">Auto Anciliary</option>
																				<option value="39">Auto Components</option>
																				<option value="38">Automobiles</option>
																				<option value="40">Aviation / Airline Jobs</option>
																				<option value="41">Banking / Financial Services Jobs </option>
																				<option value="42">Biotechnology</option>
																				<option value="83">Bottling</option>
																				<option value="73">BPO / Call Center Jobs</option>
																				<option value="81">Brewery/Distillery</option>
																				<option value="82">Broadcasting</option>
																				<option value="43">Cement</option>
																				<option value="84">Ceramics/Sanitaryware</option>
																				<option value="85">Chemicals / PetroChemical / Plastics / Rubber</option>
																				<option value="89">Computer</option>
																				<option value="86">Construction</option>
																				<option value="87">Consumer Electronics / Durables/Appliances</option>
																				<option value="44">Consumer Markets</option>
																				<option value="88">Courier / Transportation / Freight/ Warehousing</option>
																				<option value="91">Defence</option>
																				<option value="90">Development Sector/NGO/Social Services/Regulators/ Industry Associations</option>
																				<option value="45">Education and Training</option>
																				<option value="93">Educational Institute Management</option>
																				<option value="95">Electricals/Switchgears</option>
																				<option value="46">Engineering</option>
																				<option value="92">Environmental Services</option>
																				<option value="94">Event Management</option>
																				<option value="96">Export / Import / General Trading</option>
																				<option value="97">Facility Management</option>
																				<option value="98">Fertilizers/Pesticides</option>
																				<option value="47">Financial Services</option>
																				<option value="99">FMCG / Foods / Beverage</option>
																				<option value="48">Food Processing</option>
																				<option value="100">Forestry</option>
																				<option value="72">Fresher (No Industry) Jobs</option>
																				<option value="49">Gems and Jewellery</option>
																				<option value="101">Glass/Glassware</option>
																				<option value="69">Government Department Jobs</option>
																				<option value="50">Healthcare</option>
																				<option value="102">Heat Ventilation/Air Conditioning</option>
																				<option value="103">Hotels/Restaurants/Hospitality</option>
																				<option value="75">Human Resource</option>
																				<option value="106">Industrial Products/ Heavy Machinery</option>
																				<option value="51">Infrastructure</option>
																				<option value="52">Insurance Jobs</option>
																				<option value="70">Internet / E-Commerce Jobs</option>
																				<option value="53">IT &amp; ITeS</option>
																				<option value="104">IT Hardware / Networking</option>
																				<option value="105">IT Software / Software Services</option>
																				<option value="107">Leather</option>
																				<option value="108">Legal</option>
																				<option value="54">Manufacturing</option>
																				<option value="111">Marine Vessels &amp; Components</option>
																				<option value="114">Matrimony Jobs</option>
																				<option value="55">Media and Entertainment</option>
																				<option value="109">Medical devices / Equipments</option>
																				<option value="113">Medical Transcription</option>
																				<option value="112">Metalworking Machinery &amp; Equipment</option>
																				<option value="110">Mining / Quarrying</option>
																				<option value="115">Non-Ferous Metals(Aluminium/Zinc etc.)</option>
																				<option value="116">Office Equipment / Automation</option>
																				<option value="56">Oil &amp; Gas / Petroleum Jobs </option>
																				<option value="118">Packaging / Printing</option>
																				<option value="121">Pet Care</option>
																				<option value="57">Pharmaceuticals</option>
																				<option value="117">Power /Projects</option>
																				<option value="120">Publishing</option>
																				<option value="119">Pulp and Paper</option>
																				<option value="122">Railways</option>
																				<option value="58">Real Estate Jobs</option>
																				<option value="123">Religion / Spirituality</option>
																				<option value="59">Research and Development</option>
																				<option value="60">Retail Jobs</option>
																				<option value="74">Sales &amp; Marketing</option>
																				<option value="61">Science and Technology</option>
																				<option value="126">Sculpture / Craft Jobs</option>
																				<option value="124">Security / Law Enforcement</option>
																				<option value="62">Semiconductor</option>
																				<option value="63">Services</option>
																				<option value="125">Shipping/Marine</option>
																				<option value="64">Steel</option>
																				<option value="128">Strategy/Management Consulting Firms</option>
																				<option value="127">Sugar</option>
																				<option value="65">Telecommunications</option>
																				<option value="136">test industry</option>
																				<option value="66">Textile / Garments / Fashion Jobs </option>
																				<option value="129">Tobacco</option>
																				<option value="67">Tourism and Hospitality</option>
																				<option value="132">Toy</option>
																				<option value="130">Travel/Tourism</option>
																				<option value="131">Tyres</option>
																				<option value="68">Urban Market</option>
																				<option value="133">Water Treatment/Waste Management</option>
																				<option value="134">Wellness/Fitness/Sports</option>
																				<option value="135">Wood</option>
																		</select>
											</div>
											<div class="input-group image-preview form-group ">
												<?= $form->field($jobalert, 'EmailId')->textInput(['maxlength' => true,'placeholder'=>'Enter your Email ID where you would like to receive this Job Alert']) ?>
											</div>

										</div>

									</div>
								</div>
							</div>
						</div>
						<?= Html::submitButton('Create Job alert', ['name'=>'jobalertsearch','class' => 'btn btn-lg btn-primary btn-block']) ?>
						<?php ActiveForm::end(); ?>
						</div>
                    </div>
                </div>
		   </div>
        </div>
    </div>

    <div class="border"></div>

</div>
<link href="/assets/cc11e89d/themes/smoothness/jquery-ui.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>

	$(document).ready(function(){
	    $("#bluefield").val('blue');
		$('.collar_type').click(function(){
		    var val = $(this).text();
			if(val == "White Collar"){
			    $("#white_caller").prop("checked", true);
			    $("#blue_caller").prop("checked", false);
			    $("#whitefield").val('white');
			    $("#bluefield").val('');
				$('#blue-collar-fields').hide(0);
				$('#white-collar-fields').show(0);
			}else{
			    $("#blue_caller").prop("checked", true);
			    $("#white_caller").prop("checked", false);
			    $("#bluefield").val('blue');
			    $("#whitefield").val('');
				$('#blue-collar-fields').show(0);
				$('#white-collar-fields').hide(0);
			}
		});
		
		$(document).on('change', '#JobCategory', function(){
    		var value = $(this).val();
    		if(value != ""){
    			$.ajax({
    				dataType : "json",
    				type : 'GET',
    				url : '<?=Url::toRoute(['getwhiteroles']);?>',
    				data : {
    					category_id : value
    				},
    				success : function(data) {
    					$('#white_callar_roles').html("");
    					if(data != ""){
    						$.each(data, function(key, val) {
    							var option = $('<option />');
    							option.attr('value', key).text(val);
    							$('#white_callar_roles').append(option);
    						});
    					}
    				}
    			});
    		}
    		
    	});
    	
    	$("#naukari-location").select2({
    		maximumSelectionLength: 5,
    		placeholder: "Select Max 5 Preferred Locations ...",
    		ajax: {
    			url: '<?=Url::toRoute(['getpreferredlocation']);?>',
    			dataType: 'json',
    			type: "GET",
    			data: function (params) {
    				return {
    					q: params.term,
    					page: 1
    				  }
    			},
    			processResults: function(data){
    				return {
    					results: $.map(data.results, function (item) {
    						
    						return {
    							text: item.CityName,
    							id: item.id
    						}
    					})
    				};
    			}
    		 }
    	});
	
		
	});
	
	</script>
