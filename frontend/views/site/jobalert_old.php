<?php
$this->title = 'Job Alert - My Career Bugs';
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
use common\models\Position;
$blueWhiteRoles = Position::getPositionLists();

?> <meta name="keywords" content="My Career Bugs, MCB Jobs, Walk-ins, walk in interview jobs" />  
<meta name="description" content="Top Employers hiring through Walk-ins. Fresher and Experienced Walk-Ins. Create a Resume & apply for walk in interview jobs." /> 


<style>
.only_show{top:45px;}
#keynameSearchContainer .ui-autocomplete { position: absolute; top: 55px; left: 16px;}
#IndexCitySearchContainer .ui-autocomplete { position: absolute; top: 55px; left: 365px;}
#IndexCitySearchContainer .ui-autocomplete { height:auto }
.find_a_job{display:none}   
@media only screen and (max-width: 767px) {
.bg-full {width: 100%;}
.search_result{padding: 23px 0 1px 0 !Important;}
#keynameSearchContainer .ui-autocomplete { position: absolute; top: 57px; left:0px;}
#IndexCitySearchContainer .ui-autocomplete { position: absolute; top: 110px; left:15px}
#clear {margin-top: 0 !important;text-align: right;float: right;width: 100%;}
.number_drop_down {margin: 0px 6px 7px 18px;float: right;width: 188px;}
.form-control23.select2-hidden-accessible{background:#fff;}
#select_per_page {margin: 0px 0 0 0;}
#right-side{background:#f2f2f2; margin-bottom: 0px !important;}
.spacer-5 {width: 100%;height: 16px;}

}
.need_help{display:none;}
.info.bottom .left-text.min-h21{height: 73px;
    overflow: hidden;}
</style>

<div id="wrapper">
    <!-- start main wrapper -->
    <div class="headline_inner">
        <div class=" container">
            <h2>Tell us what kind of jobs you want</h2>
            <div class="clearfix"></div>
        </div>
        <!-- end headline section -->
    </div>
    <div class="inner_page">
        <div class="container">
           <div class="row ">
                    <div class="col-xs-12 col-sm-12">
						<div id="myModal_jobalert" >
						<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30']]); ?>
                        <div class="panel panel-default" style="margin-bottom:0px;">

							<div id="collapseOne19" class="panel-collapse collapse in">
								<div class="panel-group" style="padding:0; margin-bottom:0px">
									<div class="panel panel-default" style="border:0px;box-shadow: none;">
										<div class="panel-heading sty" style="background: #fff !important;padding: 9px 28px 5px 28px;color: #fff !important;">
											<div class="col-md-2">
												<a style="padding: 6px 0">
													<h4 class="panel-title" style="font-size:12px;color:#000">	
													<input type="radio" checked="checked" value="blue" name="CollarType" class="collar-type-alert"> Blue Collar <span class="checkmark"></span></h4>
												</a>
											</div>
											<div class="col-md-2">
												<a style="padding: 6px 0">
													<h4 class="panel-title" style="font-size:12px; color:#000"><input type="radio" value="white" name="CollarType" class="collar-type-alert"> White Collar <span class="checkmark"></span></h4> 
												</a>
											</div>
										</div>
										<div class="panel-body" >	
											<div id="blue-collar-fields" style="">
												<div class="input-group image-preview form-group">
													<label class="control-label" for="jobalert-role">Role</label>
													<select id="jobalert-role" style="padding:10px; font-size:12px;width: 100%;">
														<option value="">Select Role</option>
													</select>
												</div>
											</div>

											<div id="white-collar-fields" style="display:none;">										

												<div class="input-group image-preview form-group">
													<label class="control-label" for="jobalert-category">Category</label>
													<select id="jobalert-category" style="padding:10px; font-size:12px;width: 100%;">
														<option value="">Select Catogory</option>
													</select>
												</div>
												<div class="input-group image-preview form-group">
													<label class="control-label" for="jobalert-role">Role</label>
													<select style="padding:10px; font-size:12px;width: 100%;">
														<option value="">Role</option>
													</select>
												</div>
												
											</div>
										
											<div class="input-group image-preview form-group ">
												<label class="control-label" for="jobalert-experience">Experience</label>
												<select id="jobalert-experience" name="experience" class="form-control">
													<option value="">Experience</option>
													<option value="Fresher">Fresher</option>
													<option value="0-1">Below 1 Year</option>
													<option value="1-2">1-2 Years</option>
													<option value="2-3">2-3 Years</option>
													<option value="3-4">3-4 Years</option>
													<option value="4-5">4-5 Years</option>
													<option value="5-6">5-6 Years</option>
													<option value="6-7">6-7 Years</option>
													<option value="7-8">7-8 Years</option>
													<option value="8-9">8-9 Years</option>
													<option value="9-10">9-10 Years</option>
													<option value="10-11">10-11 Years</option>
													<option value="11-12">11-12 Years</option>
													<option value="12-13">12-13 Years</option>
													<option value="13-14">13-14 Years</option>
													<option value="14-15">14-15 Years</option>
													<option value="15-16">15-16 Years</option>
													<option value="16-17">16-17 Years</option>
													<option value="17-18">17-18 Years</option>
													<option value="18-19">18-19 Years</option>
													<option value="19-20">19-20 Years</option>
													<option value="20-21">20-21 Years</option>
													<option value="21-22">21-22 Years</option>
													<option value="22-23">22-23 Years</option>
													<option value="23-24">23-24 Years</option>
													<option value="24-25">24-25 Years</option>
													<option value="25-26">25-26 Years</option>
													<option value="26-27">26-27 Years</option>
													<option value="27-28">27-28 Years</option>
													<option value="28-29">28-29 Years</option>
													<option value="29-30">29-30 Years</option>
													<option value="30-31">30-31 Years</option>
												</select>
											</div>

											<div class="input-group image-preview form-group ">
												<label class="control-label" for="jobalert-salary">Experience</label>
												<select id="jobalert-salary" name="salary" class="form-control" >
													<option value="">Salary</option>
													<option value="0 - 1.5 Lakhs">0 - 1.5 Lakhs</option>
													<option value="1.5 - 3 Lakhs">1.5 - 3 Lakhs</option>
													<option value="3 - 6 Lakhs">3 - 6 Lakhs</option>
													<option value="6 - 10 Lakhs">6 - 10 Lakhs</option>
													<option value="10 - 15 Lakhs">10 - 15 Lakhs</option>
													<option value="15 - 25 Lakhs">15 - 25 Lakhs</option>
													<option value="Above 25 Lakhs">Above 25 Lakhs</option>
													<option value="Negotiable">Negotiable</option>
												</select>
											</div>
											
											<div class="input-group image-preview form-group ">
												<?= $form->field($jobalert, 'Keyword')->textInput(['maxlength' => true,'placeholder'=>'Skills, Designations, Companies','id'=>'alertkeywords']) ?>
											</div>

											<div class="input-group image-preview form-group ">
												<?= $form->field($jobalert, 'Location')->textInput(['maxlength' => true,'placeholder'=>'Enter the cities you want to work in','id'=>'alertlocation']) ?>
											</div>
											<div class="input-group image-preview form-group ">
												<?= $form->field($jobalert, 'Industry')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired industry where you want to work','id'=>'alertindustry']) ?>
											</div>
											<div class="input-group image-preview form-group ">
												<?= $form->field($jobalert, 'EmailId')->textInput(['maxlength' => true,'placeholder'=>'Enter your Email ID where you would like to receive this Job Alert']) ?>
											</div>

										</div>

									</div>
								</div>
							</div>
						</div>
						<?= Html::submitButton('Create Job alert', ['name'=>'jobalertsearch','class' => 'btn btn-lg btn-primary btn-block']) ?>
						<?php ActiveForm::end(); ?>
						</div>
                    </div>
                </div>
		   </div>
        </div>
    </div>

    <div class="border"></div>

</div>

<script>

	$(document).ready(function(){
		$('.collar-type-alert').click(function(){
			var val = $(this).val();
			if(val == "white"){
				$('#blue-collar-fields').hide(0);
				$('#white-collar-fields').show(0);
			}else{
				$('#blue-collar-fields').show(0);
				$('#white-collar-fields').hide(0);
			}
		});
	});
	
	</script>
