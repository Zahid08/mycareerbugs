<?php
$this->title = $allpost->JobTitle;

use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\HrContactLimit;
use common\models\AllUser;
use common\models\Education;
use common\models\PostJobWRole;
use common\models\PostJobWSkill;


$admin_hr_limit = HrContactLimit::find()->one();
$hr_limit = 10;
if (isset($admin_hr_limit->allow_limit)) {
    $hr_limit = $admin_hr_limit->allow_limit;
}

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

use dosamigos\tinymce\TinyMce;

$url = '/backend/web/';

if (isset(Yii::$app->session['Employeeid'])) {

    $hr_cnt = Yii::$app->myfunctions->GetHrContact(Yii::$app->session['Employeeid'], date("Y-m-d"));
}

// echo "<pre>";print_r($allpost->employer);die();

$checkEdu = Education::find()
            ->where([
				'UserId' => Yii::$app->session['Employeeid']
			])
			->count();

$employerData = AllUser::find()->where(['UserId' => $allpost->EmployerId])->one();	


?>

<?php

$jobdesc = 'Name:' . $allpost->CompanyName . "<br>Website:" . $allpost->Website . "<br>City:" . $allpost->City . "<br>Job Description:" . htmlspecialchars_decode($allpost->JobDescription) . "<br>Skill:" . trim(trim($jskill), ",");

$mailmodel->MailText = $jobdesc;


$slug = (isset(Yii::$app->request->get()['alias'])?Yii::$app->request->get()['alias']:"");

$jobUrl = Url::base().'/job/'.$slug;

$wapjobdesc =  'Job posted by ' . $allpost->CompanyName .  "<br>, Job location: " . $allpost->Location .  ", for more details visit: " . $jobUrl;

?>
 

<?php

        if ($allpost->docDetail) {

            $doc = $url . $allpost->docDetail->Doc;
        } else {

            $doc = $imageurl . 'images/user.png';
        }
		?>
	
 
<!--

<script src="https://mycareerbugs.com/js/jquery.min.js"></script>
-->


<style>
    .simple-translate-panel {height:0 !important;}
	.bookmark-active a{ background-color:#666!important;}
	.job-desc ul { 
    list-style-type: circle;
    padding-left:20px;
}
.marker{background:yellow;}
</style>





<div id="wrapper">
	<!-- start main wrapper -->







	<div class="headline_inner">



		<div class="row">



			<div class=" container">
				<!-- start headline section -->



				<h2 style="opacity: 0"> <?=$allpost->JobTitle;?>   </h2>



				<div class="clearfix"></div>



			</div>



		</div>
		<!-- end headline section -->



	</div>





	<style>
.short-decs-sidebars li {padding: 10px 0;position: relative;overflow: hidden;border-bottom: 1px solid #cccccc;}
.short-decs-sidebars li div {  font-weight: normal;font-size: 12px;}
.short-decs-sidebars li h4 {margin: 0;font-weight: 600;font-size: 12px;}
aside {
    margin-bottom: 15px;
}

aside .company-detail .company-img {
	width: 20%;
	padding: 10px;
	float: left;
}

.company-contact-detail {
	width: 78%;
	float: right;
	padding: 10px;
}

h2.title_heading {
	line-height: 23px;
	font-size: 18px;
	font-weight: bold;
	color: #f16b22;
	margin: 12px 0 15px 0;
	border-bottom: 2px solid #f16b22;
	display: inline-block;
	padding: 0 0 5px 0;
}

.heading_main_tilte12 {
	width: 80%;
}

.company-contact-detail p {
	font-size: 13px;
	margin: 0 0 7px 0;
}

.company-contact-detail p strong {
	font-weight: bold;
}

#margin-minus-top13 {
	margin-top: -130px
}

.company-contact-detail p i {
	margin-right: 5px;
}

strong.color_org1 {
	color: #f16b22;
}

#job-details {
	position: relative
}

#job-details .apply-job {
	width: 123px;
	position: absolute;
	right: 0px;
	top: 0px;
}

#job-details .click_box a {
	float: left;
	background: #6d136a;
	color: #fff;
	padding: 0px;
	width: 23px;
	height: 23px;
	display: block;
	border-radius: 100px;
	font-size: 11px;
	line-height: 27px;
	text-align: center;
	margin: 10px 2px !important;
}

#job-details a.share_btn {
	width: 36px;
	float: left;
	margin: 0;
	color: #f16b22;
	background-color: #fff;
	font-size: 13px;
	line-height: 25px;
}

#job-details .click_box i {
	margin: 0px;
}

.company-contact-detail p.post_date {
	float: right;
	margin: -5px 0 0 0;
	font-size: 11px
}


@media only screen and (max-width: 767px) { 

    .need_help, .find_a_job, .wall{display:none;}
    #last_applied{position:fixed;z-index:9999; bottom:0px;left:0px; background:#fff;padding:10px;}
    
    #new_job_seaarch_btn .apply-job a {
    margin-bottom:  0 !important;
}

#new_mobile_only_bg{background:#f0efef; margin:40px 0 0 0;}
.heading-inner .title{background:none;}
#walk_in_interviews .related-post li{background:#fff;}

}
</style>



	<div class="inner_page">

		<div class="container">

			<div class="row">

				<div class="col-lg-9 col-xs-12">



					<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"
						id="margin-minus-top13">



						<aside>

							<div class="company-detail">



								<div class="company-img">



										<?php

        if ($allpost->docDetail) {

            $doc = $url . $allpost->docDetail->Doc;
        } else {

            $doc = $imageurl . 'images/user.png';
        }

        ?>



                                        <img src="<?=$doc;?>"
										class="img-responsive" alt="" style="border-radius: 200px;">



								</div>



								<div class="company-contact-detail" id="job-details">





									<div class="apply-job">

										<a href="javascript:void(0)" class="click_share share_btn">
											Share </a>

										<div class="click_box">

											<a
												href="https://www.facebook.com/sharer/sharer.php?u=<?= $jobUrl;?>"
												target="_blank"><i class="fa fa-facebook"></i> </a> <a
												href="https://api.whatsapp.com/send?text=<?=strip_tags($wapjobdesc)?>"
												style="line-height: 27px;" target="_blank"><i class="fa fa-whatsapp"></i> </a>

 											<a href="javascript:;" onclick="mailtoemp('myModal_email');" 
 											style="line-height: 26px;"><i class="fa fa-envelope-o"></i> 
  											</a>  

										</div>

									</div>



									<div class="heading_main_tilte12">



										<h2 class="title_heading"> <?=$allpost->JobTitle;?>   </h2>
									</div>

									<p>
										<strong><?=(isset($employerData->EntryType)?$employerData->EntryType:"");?> Name: </strong><?=$allpost->CompanyName;?></p>



									<p>
										<strong> <?=(isset($employerData->EntryType)?$employerData->EntryType:"");?>  Details: </strong>



             <?=substr($allpost->Description,0,55).'...';?>



			<?php

if (strlen($allpost->Description) > 20) {

    ?>



				<a href="<?= Url::toRoute(['wall/searchcompany','userid'=>$allpost->EmployerId])?>"
											target="_blank" style="text-decoration: underline;">See More</a>



			<?php
}

?> </p>



									<div style="clear: both; overflow: hidden">

										<p style="width: 40%; float: left">
											<strong>Email:</strong> <?=$allpost->Email;?></p>

										<p style="width: 60%; float: left">
											<strong>Website:</strong> <?=$allpost->Website;?> </p>

									</div>



									<div style="clear: both; overflow: hidden">

										<p style="width: 40%; float: left">
											<strong>State:</strong> <?=$allpost->State;?>  </p>

										<p style="width: 60%; float: left">
											<strong> City:</strong> <?=$allpost->City;?>  </p>

									</div>



									<div
										style="height: 1px; background: #cccccc; margin: 5px 0 10px 0;"></div>

									<p>
										<strong class="color_org1"><i class="fa fa-location-arrow"></i>
											Job Location: </strong>  <?=$allpost->Location;?>  </p>


									<?php if($allpost->CollarType == "white"){?>
									<p><strong class="color_org1">Job Category: </strong>  <?=$allpost->category->name;?>  </p>
									<p><strong class="color_org1">Roles: </strong>  
										<?php $whiteRoleList = ArrayHelper::map(PostJobWRole::find()->where([
																	'postjob_id' => $allpost->JobId,
																	'user_id' => $allpost->EmployerId
																])->all(), 'role_id', 'role'); ?>
										<?php echo (!empty($whiteRoleList)?implode(', ',$whiteRoleList):"");?>
									</p>
									<p><strong class="color_org1">Skills: </strong>  
										<?php $whiteSkillList = ArrayHelper::map(PostJobWSkill::find()->where([
															'postjob_id' => $allpost->JobId,
															'user_id' => $allpost->EmployerId
														])->all(), 'skill_id', 'skill'); ?>
										<?php echo (!empty($whiteSkillList)?implode(', ',$whiteSkillList):"");?>
									</p>
									
									
									<?php }else{?>
									<div style="clear: both; overflow: hidden">

										<p style="width: 40%; float: left">
											<strong class="color_org1">
												Role: </strong> <?=$allpost->position->Position;?></p>

										<p style="width: 60%; float: left">
											<strong class="color_org1"> Skills: </strong> 	<?php

												$jskill = '';

												foreach ($allpost->jobRelatedSkills as $k => $v) {

													$jskill .= $v->skill->Skill . ' , ';
												}

												echo trim(trim($jskill), ",");

												?>

											</p>

									</div>


									<?php }?>


									<p class="post_date">Posted On:  <?=date('d M, Y',strtotime($allpost->OnDate));?> </p>





									<table style="display: none">



										<tbody>



											<tr>



												<th>Referrence id:</th>



												<td><?=$allpost->RefId;?></td>



											</tr>



											<tr>



												<th>Name:</th>



												<td><?=$allpost->CompanyName;?></td>



											</tr>



											<tr>



												<th>Company Details:</th>



												<td> <?=substr($allpost->Description,0,30).'...';?>



												<?php

            if (strlen($allpost->Description) > 20) {

                ?>



													<a
													href="<?= Url::toRoute(['wall/searchcompany','userid'=>$allpost->EmployerId])?>"
													target="_blank">See More</a>



												<?php
            }

            ?>



												</td>



											</tr>



											<tr style="display: none">



												<th>Email:</th>



												<td><?=$allpost->Email;?></td>



											</tr>



											<tr style="display: none">



												<th>Phone:</th>



												<td> +<?=$allpost->Phone;?></td>



											</tr>



											<tr>



												<th>Website:</th>



												<td><?=$allpost->Website;?></td>



											</tr>



											<tr>



												<th>City:</th>



												<td> <?=$allpost->City;?></td>



											</tr>



											<tr>



												<th>State:</th>



												<td><?=$allpost->State;?></td>



											</tr>



										</tbody>
									</table>



								</div>



							</div>



						</aside>







					</div>



























					<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">

		<div class="company-detail" style="padding:15px 20px 12px 20px;margin-bottom:20px">  
		    <div class="heading-inner" style="margin-bottom: 7px;"><p class="title" style="padding-bottom:8px">Education  </p> </div>




         <div class="widget" style="border:0px;margin:0px;padding:0px"> 
                     <ul class="short-decs-sidebars"> 
	                      		<li>
                            	<div class="col-3  col-md-3 col-sm-4 col-xs-12" style="padding:0px"> <h4><stong style="color:#f16b22">Highest Qualifcation:</stong></h4> </div>
                                <div class="col-9  col-md-9 col-sm-8 col-xs-12"> <?=($allpost->qualifications=='')?'Not mentioned':$allpost->qualifications;?></div>
							</li>  
							<li>
						    	<div class="col-3  col-md-3 col-sm-4 col-xs-12" style="padding:0px"> <h4><stong style="color:#f16b22">Course:</stong></h4> </div>
								<div class="col-9  col-md-9 col-sm-8 col-xs-12"><?=($allpost->course=='')?'Not mentioned':$allpost->course;?> 	</div>
							</li> 
						   
							<li>
								<div class="col-3  col-md-3 col-sm-4 col-xs-12" style="padding:0px"> <h4><stong style="color:#f16b22">Specific Qualification:</stong></h4> </div>
								<div class="col-9  col-md-9 col-sm-8 col-xs-12"> <?=($allpost->SpecialQualification=='')?'Not mentioned':$allpost->SpecialQualification;?></div>
							</li> 
					  </ul>  				
		      </div> 				
	    	</div>

						<div class="single-job-page-2 job-short-detail">



							<div class="heading-inner">



								<p class="title">Job Description</p>



							</div>



							<div class="job-desc job-detail-boxes">



                                    



                                        <?=htmlspecialchars_decode($allpost->JobDescription);?>



                                   



                                    <div class="heading-inner">



									<p class="title">Job Specification:</p>



								</div>



                                   



                                        <?=!empty($allpost->JobSpecification)?htmlspecialchars_decode($allpost->JobSpecification):'N/M';?>



                                    







                                    <div class="heading-inner">



									<p class="title">Technical Guidance:</p>



								</div>

								<?=!empty($allpost->TechnicalGuidance)?htmlspecialchars_decode($allpost->TechnicalGuidance):'N/M';?>
	


									



									<?php

        if ($allpost->IsWalkin == 1) {

            ?>



									<div class="heading-inner">



									<p class="title">Walk-In Details:</p>



								</div>



								<p>



										Venue :  <?=$allpost->Venue;?><br />



										Interview Date & time : <?=date('dS M Y',strtotime($allpost->WalkinFrom)).'-'.date('dS M Y',strtotime($allpost->WalkinTo));?> between <?=$allpost->	WalkinTimeFrom;?> to <?=$allpost->WalkinTimeTo;?>



										<br />



								</p>



									<?php
        }

        ?>



                                </div>







						</div>



					</div>

				</div>













				<div class="col-lg-3  col-md-12 col-sm-4 col-xs-12">



						  		<?php

        if (Yii::$app->session['Employeeid']) {

            // var_dump($allpost->appliedJobs);

            if ($allpost->appliedJobs) {

                foreach ($allpost->appliedJobs as $ke => $value1) {

                    if ($value1->UserId == Yii::$app->session['Employeeid']) {

                        $rest[] = $value1->Status;
                    }
                }

                if (! empty($rest)) {

                    if (in_array('Bookmark', $rest)) {

                        $ap = 1;
                    } else {

                        $ap = 0;
                    }
                } else {

                    $ap = 1;
                }
            } else {

                $ap = 1;
            }
        } elseif (Yii::$app->session['Teamid']) {

            // var_dump($allpost->appliedJobs);

            if ($allpost->appliedJobs) {

                foreach ($allpost->appliedJobs as $ke => $value1) {

                    if ($value1->UserId == Yii::$app->session['Teamid']) {

                        $rest[] = $value1->Status;
                    }
                }

                if (! empty($rest)) {

                    if (in_array('Bookmark', $rest)) {

                        $ap = 2;
                    } else {

                        $ap = 0;
                    }
                } else {

                    $ap = 2;
                }
            } else {

                $ap = 2;
            }
        } else {

            $ap = 2;
        }

        ?>



						  



                            <div class="widget">



						<div class="widget-heading">
							<span class="title">Short Description</span>
						</div>



						<ul class="short-decs-sidebar">



							<li>



								<div>



									<h4>Designation:</h4>



								</div>



								<div>



									<i class="fa fa-briefcase"></i>



											<?=$allpost->Designation;?>



										</div>



							</li>





							<li>



								<div>



									<h4>Job Type:</h4>
								</div>



								<div>
									<i class="fa fa-bars"></i><?=$allpost->JobType;?></div>



							</li>



							<li>



								<div>



									<h4>Job Experience:</h4>
								</div>



							  <div><i class="fa fa-calendar"></i>
							  <?php if($allpost->Experience!='Fresher'){ echo $allpost->Experience.' Year';}
							  else{echo $allpost->Experience;}?>
							  </div>



							</li>



							<li>



								<div>



									<h4>Eligibility:</h4>



								</div>



								<div>



									<i class="fa fa-bars"></i>



										<?php

        if ($allpost->IsExp == 0) {
            echo "Fresher";
        } elseif ($allpost->IsExp == 1) {
            echo "Experience";
        } elseif ($allpost->IsExp == 2) {
            echo "Both";
        }

        ?>



										</div>



							</li>





							<li>



								<div>



									<h4>Industry:</h4>
								</div>



								<div>
									<i class="fa fa-industry"></i><?=$allpost->jobCategory->IndustryName;?> </div>



							</li>



							<li>



								<div>



									<h4>Job Shift:</h4>
								</div>



								<div>
									<i class="fa fa-clock-o"></i><?=$allpost->JobShift;?> </div>



							</li>







							<li>



								<div>



									<h4>Expected Salary:</h4>
								</div>



								<div>
									<i class="fa fa-rupee"></i><?=$allpost->Salary;?> / Annum</div>



							</li>



							<li>



								<div>



									<h4>Other Salary:</h4>
								</div>



								<div>
									<i class="fa fa-rupee"></i>



										<?=($allpost->OtherSalary=='')?'Not mentioned':$allpost->OtherSalary;?>



										</div>



							</li>



							<li>
								<div>
									<h4>No Of Vacancy:</h4>
								</div>
								<div>
									<i class="fa fa-user"></i><?=$allpost->NoofVacancy;?></div>
							</li>
							
					  
						</ul>
					</div>
				</div>
			</div>

		</div>
		<div style="clear: both; overflow: hidden"></div>
		
		
		<div id="new_job_seaarch_btn">
			<?php if($checkEdu == 0 && isset(Yii::$app->session['Employeeid'])){?>
		<div class="alert alert-danger">
		  <strong>Alert!</strong> Your profile is incomplete. Please complete your profile details. <a href="<?= Url::toRoute(['site/editprofile']);?>" >Edit Profile</a>
		</div>
		<?php }?>
		<?php if(!empty($rest) && in_array('Bookmark', $rest)){?>
			<div class="apply-job bookmark-active">
				<a
					href="javascript:void(0);"><i
					class="fa fa-star"></i>Bookmarked</a>
			</div>
			
		<?php }else{?>
			<div class="apply-job">
				<a
					href="<?= Url::toRoute(['site/bookmark','JobId'=>$allpost->JobId])?>"><i
					class="fa fa-star"></i>Bookmark</a>
			</div>
			
		<?php }?>
			<?php if(!empty($allpost->Phone)){?>
			<div class="apply-job" style="">
				<a data-toggle="modal" data-target="#hr_contact"
					<?php if((isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Teamid'])) && $hr_cnt<$hr_limit){ ?>
					onclick="contact_hr()" <?php } ?>><i class="fa fa-phone"></i>HR
					Contact</a>
			</div>
			<?php }?>	


						<?php
						
	//if($checkEdu > 0){				

    if ($ap == 1) {
        if ($allpost->AltEmail != '') {
			if($checkEdu == 0 && Yii::$app->session['Employeeid']){?>
			<div class="apply-job">
				<a href="javascript:void(0);" onclick="jobWorning();" > <i class="fa fa-upload"></i>Apply
					For Position
				</a>
			</div>
			<?php }else{ ?>
			<div class="apply-job">
				<a href="#" class="" data-toggle="modal"
					data-target="#myModal_applyposition"> <i class="fa fa-upload"></i>Apply
					For Position
				</a>
			</div>
			<?php }?>
			
		<?php
        } else {
			if($checkEdu == 0 && Yii::$app->session['Employeeid']){?>
				<div class="apply-job">
					<a href="javascript:void(0);" onclick="jobWorning();"> <i class="fa fa-upload"></i>Apply For Position</a>
				</div>
			<?php }else{?>
				<div class="apply-job">
					<a
						href="<?= Url::toRoute(['site/applyjob','JobId'=>$allpost->JobId])?>"
						class=""> <i class="fa fa-upload"></i>Apply For Position
					</a>
				</div>
			<?php }?>
		<?php
        }
    } elseif ($ap == 2) {
        ?>
		<div class="apply-job" id="last_applied">
		<?php 	if($checkEdu == 0 && Yii::$app->session['Employeeid']){?>
				
					<a href="javascript:void(0);" onclick="jobWorning();"> <i class="fa fa-upload"></i>Apply For Position</a>
				
			<?php }else{?>
				<a
					href="<?= Url::toRoute(['site/applyjob','JobId'=>$allpost->JobId])?>"
					class=""> <i class="fa fa-upload"></i>Apply For Position
				</a>
			<?php }?>	
			</div>
							<?php
    } else {

        ?>



							<div class="apply-job" id="last_applied">



				<a href="#" class=""> <i class="fa fa-upload"></i>Applied
				</a>



			</div>



							<?php
    }
	//}else{?>
		<!--<br style="clear:both"/> -->


						 
<?php ///}?>


						  	<div class="apply-job" id="share_block">



				<a href="javascript:void(0)" class="click_share"> <i
					class="fa fa-share"></i>Share this Job
				</a>







				<div class="click_box">



					<a
						href="https://www.facebook.com/sharer/sharer.php?u=<?=$jobUrl;?>"
						target="_blank"><i class="fa fa-facebook"></i> </a> <a
						href="https://api.whatsapp.com/send?text=<?=strip_tags($wapjobdesc)?>"
						style="line-height: 33px;"><i class="fa fa-whatsapp"></i> </a> <a
						href="javascript:;" onclick="mailtoemp('myModal_email');"
						style="line-height: 32px;"><i class="fa fa-envelope-o"></i> </a>



				</div>







			</div>







		</div>







		<style>
#new_job_seaarch_btn .apply-job a {
	font-size: 14px
}

#new_job_seaarch_btn .click_box {
	position: absolute;
	width: 103px;
	top: -3px;
	left: 112px;
}

#new_job_seaarch_btn .click_box a {
	float: left;
	background: #6d136a;
	color: #fff;
	padding: 0px;
	width: 30px;
	height: 30px;
	display: block;
	border-radius: 100px;
	font-size: 14px;
	line-height: 35px;
	text-align: center;
	margin: 10px 2px !important
}

#new_job_seaarch_btn .click_box a i {
	margin: 0px
}

#new_job_seaarch_btn {
	width: 675px;
	margin: 0 auto;
	display: block;
	clear: both;
	overflow: hidden
}

#new_job_seaarch_btn .apply-job {
	width: 30.3%;
	float: left;
	margin: 20px 1.5% 0 1.5%
}

#share_block {
	display: block !important;
	float: none !important;
	width: 218px !important;
	margin: 0 auto !important;
	text-align: center;
}

#share_block a {
	text-align: left;
	background: #fff;
	color: #f16b22;
	line-height: 28px;
}

#new_job_seaarch_btn .apply-job a {
	margin-bottom: 20px
}
</style>










<div id="new_mobile_only_bg"> 




		<div class="container">



			<div class="border" style="margin-bottom: 30px"></div>



			<div class="row">







				<div class="col-lg-9  col-md-9 col-sm-9 col-xs-12">



					<div class="heading-inner">



						<p class="title">Silimar Jobs</p>



					</div>



					<div id="walk_in_interviews">



						<ul class="related-post">



										<?php

        if ($simmilar) {

            $w = 0;

            foreach ($simmilar as $sk => $sval) {

                $w ++;

                ?>



                                        <li
								<?php if($w%3==0) echo 'class="no_padding"';?>><a
								href="<?= Url::base().'/job/'.$sval->Slug;?>">



									<span> <strong style="color: #f16b22;">Job Title: </strong> <?=$sval->JobTitle;?> </span>
<span><i class="fa fa-suitcase"></i>Designation:  	<?=$sval->Designation;?> </span>
 

									<span><i class="fa fa-map-marker"></i>Place: <?=$sval->Location;?> </span>



									<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($sval->OnDate)+37800);?> </span>
							</a></li>



										<?php
            }
        }

        ?>



                                       



										<div class="clear"></div>



						</ul>







						<div class="clear"></div>



					</div>
				</div>
				<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
					<div class="adban_block">
						<a href="" target="_blank"> <img
							src="<?=$imageurl;?>images/adban_block/ban.jpg">
						</a>
					</div>
					<!-- adban_block  main -->
					<!--adban_block main -->
					<div class="adban_block">
						<a href="" target="_blank"> <img
							src="<?=$imageurl;?>images/add.jpg">
						</a>
					</div>
					<!-- adban_block  main -->
				</div>
			</div>
			<div class="clear"></div>

		</div>
	</div>
	</div>

	<div class="border"></div>





	<div class="modal add-resume-modal" id="myModal_applyposition"
		tabindex="-1" role="dialog" aria-labelledby="">
		<div class="modal-dialog modal-md" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
				<?php

$form = ActiveForm::begin([
        'options' => [
            'class' => 'offset-top-10 offset-sm-top-30'
        ],
        'action' => Url::toRoute(['/applyjob','JobId' => $allpost->JobId])
    ]);
    ?>
	<div class="input-group image-preview form-group ">
	<?php
if ($alluser->CVId != 0) {
    ?>
		<label>Uploaded Resume : </label> <label> 
    		<?php
    if ($alluser && $alluser->CVId != 0) {
        $na = explode(" ", $alluser->Name);
        $name = $na[0];
        $extar = explode(".", $alluser->cV->Doc);
        $ext = $extar[1];
        ?>
                <a href='<?=$url.$alluser->cV->Doc;?>' class="cv"
							download="<?=$name.'.'.$ext;?>"><?=$alluser->Name;?> Resume</a> <br />
							<br />
    						<?php
    }
    ?>
		</label>
		<?php
    if ($alluser->CVId != 0) {
        ?>
			 <a
							onclick="$(this).prev().remove();$(this).remove();deletecv(<?=$alluser->UserId;?>);"
							style="color: red;">X</a> 
		 <?php
    }
} else {
    echo "<label style='color:#f16b22;'>Use My Career Bugs CV</label>";
}

?>  <span style="    background: #f16b22;color: #fff;padding: 5px 7px 2px 7px;font-size: 11px;margin: 7px;"> OR  </span> 
	<label> apply With a different Resume ?</label>
		<?= $form->field($alluser, 'CVId')->fileInput(['accept'=>'.doc, .docx,.rtf,.pdf','id'=>'cvid'])->hint('doc,docx,pdf,rtf - 2MB max') ?>
    </div>
					<div class="modal-footer">
						<?= Html::submitButton('Apply', ['name'=>'applyjobwith','class' => 'btn btn-lg btn-primary btn-block']) ?>
                    </div>
                   <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="hr_contact" role="dialog">
		<div class="modal-dialogs">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="panel-body text-center">
			   <h4 class="modal-title" style="line-height: 9px;font-size: 14px;">
			       	<?php if((isset(Yii::$app->session['Employeeid']) || isset(Yii::$app->session['Teamid'])) && $hr_cnt<$hr_limit){ ?>
             Contact HR </h4>
		   <h4 class="modal-title" style="line-height: 43px;text-transform:uppercase">		<?php if(!empty($allpost->ContactPerson)){?>
			<?=$allpost->ContactPerson;?> </h4>
	 <strong style="margin: -10px 0 0 0;font-size: 18px;line-height: 31px;">
	     	 <?=$allpost->LandlineNo;?> </strong> 
		
				<?php }?>
				<a href="tel:<?=$allpost->Phone;?>" class="lead" style="margin-top:0px"><strong style=" margin: -3px;font-size: 18px;"><?=$allpost->Phone;?></strong></a>



													<?php }else if(Yii::$app->session['Employeeid'] || isset(Yii::$app->session['Teamid'])){ ?>

														You have reached your daily limit

													<?php }else{ ?>



												 Contact HR 
<a href="https://mycareerbugs.com/login?Job=<?=$allpost->Slug;?>">Login</a>


													<?php } ?>



												</div>

		</div>



	</div>



	<script type="text/javascript">



		 		function contact_hr(){



		 			$.ajax({



		 				url:mainurl+"site/contacthr?tomail=<?=$allpost->employer->Email?>"+"&username=<?=Yii::$app->session['EmployeeName']?>"+"&emp_email=<?=Yii::$app->session['EmployeeEmail']?>&job_id=<?=$allpost->JobId;?>&can_id=<?=Yii::$app->session['Employeeid']?>",



              			success:function(results)



            			{



                			



            			}



		 			});



		 		}

		 		function mailtoemp(id) {

					$('#'+id).modal('show');

        		}

		 	</script>

	<div class="modal fade" id="myModal_email" role="dialog">

		<div class="modal-dialog">

			<div class="modal-content">

				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<h4 class="modal-title">Share Job</h4>

				</div>



				<div class="row main">

					<div class="xs-12 col-sm-12 main-center">

		<?php $form = ActiveForm::begin(['options' => ['id'=>'sendshareemail','class' => 'row','enctype'=>'multipart/form-data']]); ?>

		<div class="form-group">

							<input type="text" id="MailId" class="form-control"
								name="Mail[EmailId]" Placeholder="Enter E-Mail">

						</div>

						<div class="form-group">

		<?= $form->field($mailmodel, 'Subject')->textInput(['value'=>'Job For You','maxlength' => true,'Placeholder'=>'Subject'])->label('Subject') ?>

		</div>



						<!-- <div class="col-md-12 col-sm-12 col-xs-12">

							<div class="form-group">
								<label>Job Description* </label>

										

								< ?=$form->field($mailmodel, 'MailText')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small','language' => 'en_CA','clientOptions' => ['plugins' => ["advlist autolink lists link charmap print preview anchor","searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);?>

                              </div>



						</div> -->



						<div class="col-md-12 col-sm-12 col-xs-12">



							<div class="form-group">

									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green','id' => 'sendshareemail-share-submit']) ?>

								</div>

						</div>

		<?php ActiveForm::end(); ?>

		</div>

				</div>

			</div>

		</div>

	</div>

	<script>

		 		$( document ).ready(function() {

		 			$(document).on('submit', '#sendshareemail', function(event) {

					event.preventDefault();

					var email = $('#MailId').val();
					var jobid = "<?php echo $allpost->JobId;?>";
					if(email.length==0){

					return false;

					}

					var Subject = $('#mail-subject').val();

					var text = $('#mail-mailtext').text();

					$.ajax({

            			type: "POST",

            			url: '<?=Yii::$app->urlManager->baseUrl."/jobpostsharemail"?>',

            			data: {'email': email,'Subject':Subject,'text':text,'jobid': jobid},

            			success : function(response) {

            				$('#myModal_email').modal('hide');

        				}, 

        			})

				});

		 		});
				
				function jobWorning(){
					alert('Alert! Your profile is incomplete. Please complete your profile details');
				}

		 	</script>