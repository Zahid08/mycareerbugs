<link href="https://mycareerbugs.com/css/jquery.dropdown.css" rel="stylesheet"> 
<script src="js/ckeditor/ckeditor.js"></script>

<?php
$this->title = 'Job Edit';
use common\models\City;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;use dosamigos\tinymce\TinyMce;
use bupy7\cropbox\CropboxWidget;
use common\models\AllUser;
use common\models\Skill;
use common\models\EmployeeSkill;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
$ctyloc = City::find()->all();
$emp = Yii::$app->session['Employerid'];
 $alusr = AllUser::find()->where([
    'UserId' => $emp
])->one();

 foreach($ctyloc as $cty){	
			$che[]  =    $cty->CityName ;	
			$keys[] =  $cty->CityName ;		
		}			 			 
		$cj =array_combine($che,$keys);	
?>
  
  <link href="/assets/cc11e89d/themes/smoothness/jquery-ui.css" rel="stylesheet">

	<div id="wrapper"><!-- start main wrapper -->

		<div class="headline_inner">
				<div class="row"> 
			       <div class=" container"><!-- start headline section --> 
						 <h2>Job Edit</h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	</div>
	
		 
		<div class="inner_page">
			<div class="container">
						<div class="container">
			<div class="row main">
				 
				<div class="xs-12 col-sm-12 main-center">
					<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
								<?php
							if($pf==0)
							{
								?>
								<div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Job For</label><div class="clear"></div>
                                        <input style="float: left; width: 15px;height: 15px;" type="radio" value="Candidate" name="PostJob[PostFor]" <?php if($allpost->PostFor=='Candidate'){ echo 'checked';} ?> onclick="$('#teamvacancy').hide();$('#candidatevacancy').show();" class="form-control"> &nbsp; Candidate
										&nbsp; &nbsp;<div class="clear"></div>
										<input style="float: left; width: 15px;height: 15px;" type="radio" value="Team" name="PostJob[PostFor]" class="form-control" onclick="$('#teamvacancy').show();$('#candidatevacancy').hide();" <?php if($allpost->PostFor=='Team'){ echo 'checked';} ?> > &nbsp; Team
										&nbsp; &nbsp;<div class="clear"></div>
										<input style="float: left; width: 15px;height: 15px;" type="radio" value="Both" name="PostJob[PostFor]" class="form-control" onclick="$('#teamvacancy').hide();$('#candidatevacancy').show();" <?php if($allpost->PostFor=='Both'){ echo 'checked';} ?> > &nbsp; Both
                                    </div>
                                </div>
							<?php
							}
							?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Title</label>
                                        <input type="text" value="<?=$allpost->JobTitle;?>" name="PostJob[JobTitle]" required class="form-control">
										<p class="help-block help-block-error"></p>
                                    </div>
                                </div>
								<div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Designation</label>
                                        <input type="text" placeholder=" " value="<?=$allpost->Designation;?>" name="PostJob[Designation]" required class="form-control">
										<p class="help-block help-block-error"></p>
                                    </div>
                                </div>
								
								
								
								<?php 
								$loc_ary = array();
								if(!empty($allpost->Location)){
									$loc_ary = explode(',', $allpost->Location);
								}
								?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Location</label>
									<select name="PostJob[Location][]"  class="form-control" multiple="multiple" id="naukari-location">
										<?php 
										if(!empty($loc_ary)){
										foreach($loc_ary as $city){ ?>
									
											<option selected value="<?php echo $city ?>"><?php echo $city?></option>
										<?php } } ?>
									
										</select>
									<p class="help-block help-block-error"></p>
                                    </div>
                                </div>
								<?php
								if($pf==0)
								{
								?>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Experiencce  </label>
                                        <select class="questions-category form-control " tabindex="0" aria-hidden="true" name="PostJob[Experience]">
                                            <option value="Fresher" <?php if($allpost->Experience=='Fresher') echo 'selected="selected"';?>>  Fresher</option>
					    <?php
					    for($fr=1;$fr<=30;$fr=$fr+1)
					    {
							$frn=$fr+1;
					?>
                                            <option value="<?=$fr.'-'.$frn;?>" <?php if($allpost->Experience==$fr.'-'.$frn) echo 'selected="selected"';?>><?=$fr.'-'.$frn;?> Years</option>
                                           <?php
					    }
					    ?>
                                        </select>
										<!--<input type="checkbox" name="PostJob[IsExp]" value="1" <?php if($allpost->IsExp==1){ echo "checked";}?> /> &nbsp; Experience &nbsp; &nbsp;-->
										<input type="checkbox" name="PostJob[IsExp]" <?php if
										($allpost->IsExp==2){ echo "checked";}?> value="2" /> &nbsp; Both
                                    </div>
                                </div>
								<?php
								}
								?>
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Salary  </label>
                                        <select class="questions-category form-control" tabindex="0" aria-hidden="true" name="PostJob[Salary]">
											<option value="0 - 1.5" <?php if($allpost->Salary=='0 - 1.5') echo 'selected="selected"';?>> 0 - 1.5 Lakhs</option>
                                            <option value="1.5 - 3" <?php if($allpost->Salary=='1.5 - 3') echo 'selected="selected"';?>>1.5 - 3 Lakhs</option>
                                            <option value="3 - 6" <?php if($allpost->Salary=='3 - 6') echo 'selected="selected"';?>>3 - 6 Lakhs</option>
											<option value="6 - 10" <?php if($allpost->Salary=='6 - 10') echo 'selected="selected"';?>>6 - 10 Lakhs</option>
											<option value="10 - 15" <?php if($allpost->Salary=='10 - 15') echo 'selected="selected"';?>>10 - 15 Lakhs</option>
											<option value="15 - 25" <?php if($allpost->Salary=='15 - 25') echo 'selected="selected"';?>>15 - 25 Lakhs</option>
											<option value="Above 25" <?php if($allpost->Salary=='Above 25') echo 'selected="selected"';?>>Above 25 Lakhs</option>
											<option value="Negotiable" <?php if($allpost->Salary=='Negotiable') echo 'selected="selected"';?>>Negotiable</option>
                                        </select> 
                                    </div>
                                </div>
								 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Other Salary</label>
                                        <input type="text" placeholder="spot, bonus etc" id="othersalary" name="PostJob[OtherSalary]" class="form-control" value="<?=$allpost->OtherSalary;?>">
					<?php
										$othersalary=explode(", ",$allpost->OtherSalary);
										$other='';
										foreach($othersalary as $key=>$value)
										{
											$value=trim($value);
											if($value!='')
											{
											$other.='<div id="otherloc'.$key.'" style="width:auto;float:left;margin-left:10px;background:red;"><div style="width:auto;float:left;padding:5px;">'.$value.'</div><span onclick="removeskillother(&#39;'.$value.'&#39;,'.$key.');" style="width:auto; float:left;margin-top:-5px;cursor:pointer;color:#fff; margin-left:5px;font-size:16px;">x</span></div>';
											}
										}
										?>
					<div id="allothersalary" style="width: 100%; margin-top: 5px; height: 25px; padding: 3px;font-size:12px; color: #fff;">	<?=$other;?>
					</div>
                                    </div>
                                </div>
								
								
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Type</label>
                                        <select class="questions-category form-control " tabindex="0" aria-hidden="true" name="PostJob[JobType]">
                                            <option value="Full Time" <?php if($allpost->JobType=='Full Time') echo 'selected="selected"';?>>Full Time</option>
                                            <option value="Part Time" <?php if($allpost->JobType=='Part Time') echo 'selected="selected"';?>>Part Time</option>
                                            <option value="Fresher" <?php if($allpost->JobType=='Fresher') echo 'selected="selected"';?>>Fresher</option>
                                             
                                            <option value="Work From Home" <?php if($allpost->JobType=='Work From Home') echo 'selected="selected"';?>>Work From Home</option>
											<option value="Walkin" <?php if($allpost->JobType=='Walkin') echo 'selected="selected"';?>>Walkin</option>
											<option value="Internship" <?php if($allpost->JobType=='Internship') echo 'selected="selected"';?>>Internship</option>
											<option value="Contract" <?php if($allpost->JobType=='Contract') echo 'selected="selected"';?>>Contract</option>
											<option value="Commission" <?php if($allpost->JobType=='Commission') echo 'selected="selected"';?>>Commission</option>
											<option value="Temporary" <?php if($allpost->JobType=='Temporary') echo 'selected="selected"';?>>Temporary</option>
											<option value="Volunter" <?php if($allpost->JobType=='Volunter') echo 'selected="selected"';?>>Volunter</option>
                                        </select> 
                                    </div>
                                </div>
								 
								
																  
								   <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Industry</label>
                                        <select class="questions-category form-control " tabindex="0" aria-hidden="true" name="PostJob[JobCategoryId]" required>
										<option value="">Select Industry</option>
                                            <?php
										foreach($jobcategory as $key=>$value)
										{
										?>
										<option value="<?=$value->IndustryId;?>" <?php if($allpost->JobCategoryId==$value->IndustryId) echo 'selected="selected"';?>><?=$value->IndustryName;?></option>
										<?php
										}
										?>
                                        </select> 
										<p class="help-block help-block-error"></p>
                                    </div>
                                </div>
								
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label>Qualification</label>
										<?php $qualificationData = (!empty($allpost->qualifications)?explode(',', $allpost->qualifications):array());?>
										<select class="questions-category form-control"
											id="naukari-qualification" name="PostJob[Qualifications][]"
											multiple="multiple">
											 <?php foreach ($topQualifications as $key => $qual) {?>
												<option <?=(in_array($qual, $qualificationData)?"selected":"");?> value="<?=$qual;?>"><?=$qual;?></option>
												<?php }?>
											
										</select>
										<p class="help-block help-block-error"></p>
									</div>	
								
								</div>
								
								<div class="clearfix height-20"></div>
								
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label>Special Qualification (Optional)</label>
													<?php
													if (isset($_GET['education']) && $_GET['education'] != '') {
														$education = $_GET['education'];
													} else {
														$education = '';
													}
													?>
							
							<select class="questions-category form-control"
												id="naukari-specialization" name="PostJob[course][]"
												multiple="multiple">
											</select> 
										</div>
									</div>
									
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<label>Specialization</label>
											<select
												class="form-control" name="PostJob[SpecialQualification][]"
												id="specialskill" multiple="multiple">
											</select>

											<div id="allspecskill"
												style="width: 100%; margin-top: 5px; height: 25px; padding: 3px; font-size: 12px; color: #fff;">
											</div>
										</div>
									</div>		
								   
								   
								   
					 			   
							<?php if($allpost->CollarType == 'white'){?>	   
							<!-- Blue Caller  Start----------------------------------------------------------->
							<div id="white_caller" >
									<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
												<label> Job Category</label> 
												<select class="questions-category form-control" name="PostJob[WhiteCategory]" tabindex="0" aria-hidden="true" id="white_callar_category">
											 <option value = "">Select Category</option>
											 <?php if(!empty($whiteCategories)){
													foreach($whiteCategories as $categoryId => $categoryName){?>
														<option value="<?php echo $categoryId?>" <?=($categoryId == $allpost->WhiteCategoryId?"selected":"");?>><?=$categoryName;?></option>
													<?php  }
													}?>
											 
											</select>
											<p class="help-block help-block-error"></p>
									</div>   
										</div>  
								 
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
											<label> Role</label> 
											<select class="questions-category form-control" name="PostJob[WhiteRole][]" id="white_callar_role" multiple="multiple">
												   <option value = "">Select Role</option>
												  <?php if(!empty($whiteCategoryRole)){
													foreach($whiteCategoryRole as $roleId => $roleName){?>
														<option value="<?php echo $roleId?>"><?=$roleName;?></option>
													<?php  }
													}?> 
												 
											</select> 
											<p class="help-block help-block-error"></p>
											</div>   
										</div> 
										<div class="clear"></div>
										
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="form-group">
											<label> Skill</label> 
											<select class="questions-category form-control" id="white_callar_skills" name="PostJob[WhiteSkills][]" multiple="multiple">
												<option value = "">Select Skill</option>
												<?php if(!empty($whiteSkills)){
													foreach($whiteSkills as $skillId => $skillName){?>
														<option value="<?php echo $skillId?>"><?=$skillName;?></option>
													<?php  }
													}?> 
											</select>		
											
									</div>   
										</div> 
										
										
										
								</div>	
						<?php }else{?>
						
								
								
							<div id="blue_caller">		    
								 
								  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select class="questions-category form-control " tabindex="0" aria-hidden="true" name="PostJob[PositionId]" id="postroleid"  required>
                                            <option value="">Select Role  </option>
										<?php
										foreach($position as $key=>$value)
										{
										?>
										<option value="<?=$value->PositionId;?>"  <?php if($allpost->PositionId==$value->PositionId) echo 'selected="selected"';?>><?=$value->Position;?></option>
										<?php
										}
										?>
                                        </select> 
										<p class="help-block help-block-error"></p>
                                    </div>
									<div class="form-group" id="postskilldatalist">
										<?php
											$roleId = $allpost->PositionId; 
											$allSkills = ArrayHelper::map(Skill::find()->where([
												'IsDelete' => 0,
												'PositionId' => $roleId
											])->all(), 'SkillId', 'Skill');
											
											$jobSkills = array();
											if(!empty($allpost->jobRelatedSkills)){
												foreach($allpost->jobRelatedSkills as $jobSkill){
													$jobSkills[$jobSkill->JobRelatedSkillId] = $jobSkill->SkillId;
													
												}
											}
											
										?>
										<?php if(!empty($allSkills)){?>
											<label for="confirm" class="cols-sm-2 control-label">Key Skill</label>
											<div class="cols-sm-10">
												<div class="input-group">
													<?php foreach($allSkills as $key => $value){?>
														<span><input type="checkbox" data-name="3D" value="<?=$key;?>" name="PostJob[KeySkill][]" <?=(in_array($key,$jobSkills)?"checked":"");?>><?=$value;?>&nbsp;</span>
													<?php }?>
												</div>
											</div>
										<?php }?>
									
									</div>
                                </div>
								
					  			
				  </div>
							
							
							<?php }?>
					<!-- Blue Caller  End----------------------------------------------------------->					
					<div class="clear"></div>				
					   
                                <div class="clearfix height-20"></div>
								
                                 <h5 class="page-head"><?php if($alusr->EntryType == 'HR') { echo 'HR';} else { echo 'Company'; } ?> Details</h5>
								   <div class="clearfix height-20"></div>
								 
								 <?php /*<div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Company Description</label>
                                        <input type="text" name="PostJob[Description]" value="<?=$allpost->Description;?>" readonly placeholder="" class="form-control">
                                    </div>
                                </div>
								 
								 
								 
								  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" name="PostJob[CompanyName]" required value="<?=$allpost->CompanyName;?>" class="form-control">
                                    </div>
                                </div>
								 
								 */?>
								  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Email Address*</label>
                                        <input type="email"  name="PostJob[Email]" value="<?=$allpost->Email;?>" required class="form-control">
										<p class="help-block help-block-error"></p>
                                    </div>
                                </div>
								  
								  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Contact Person:</label>
                                        <input type="text" maxlength="100" value="<?=$allpost->ContactPerson;?>" name="PostJob[ContactPerson]" class="form-control" >
                                    </div>
                                </div>
								<div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>HR Number:</label>
                                        <input type="text" maxlength="10" onkeypress="return numbersonly(event)" onblur="return IsMobileno(this.value);" id="MobileNo" value="<?=$allpost->Phone;?>" name="PostJob[Phone]" class="form-control" >
                                    </div>
                                </div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label>Landline Number: </label> <input type="text" maxlength="15"
											onkeypress="return numbersonly(event)"
											id="LandlineNo"
											placeholder="" value="<?=$allpost->LandlineNo;?>"
											name="PostJob[LandlineNo]" class="form-control">
										<p class="help-block help-block-error"></p>
									</div>
								</div>
								
								  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Website:	</label>
                                        <input type="text" value="<?=$allpost->Website;?>" id="post-job-url" class="form-control" name="PostJob[Website]">
                                        <p  style="display:none;color:red" id="website_error">Please check website url format.</p>
                                    </div>
                                </div>
								 
								
								<?php /*
								 
								  <div class="col-md-6 col-sm-6 col-xs-12">
								  <div class="form-group">
                                        <label>Country</label> 
										<input type="text" value="<?=$allpost->Country;?>" name="PostJob[Country]" required class="form-control">
								        </div>
								   </div>
								 
								  <div class="col-md-6 col-sm-6 col-xs-12">
								     <div class="form-group">
                                        <label>State</label>
										 <select class="questions-category states form-control select2-hidden-accessible" tabindex="0" aria-hidden="true"  id="stateId" name="PostJob[State]" required>
										<option value="">Select State</option>
										</select> 
								     </div>
							     </div>
								  <?php
								  $state=$allpost->State;
								  $city=$allpost->City;
								  ?>
								  
										 <div class="col-md-6 col-sm-6 col-xs-12">
											   <div class="form-group">
												<label>City</label>
												<select class="questions-category cities form-control select2-hidden-accessible" tabindex="0" aria-hidden="true"  id="cityId" name="PostJob[City]" required>
												<option value="">Select City</option>
											</select>
												</div>
										   </div>
								  
									*/?>		
										
										  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>No. of Vacancies:</label>
                                         <select class="questions-category form-control " tabindex="0" aria-hidden="true" name="PostJob[NoofVacancy]" id="candidatevacancy" <?php if($allpost->PostFor=='Team'){?>style="display: none;" <?php }?>>
											<?php
											if($pf==0)
											{
											?>
											<option value="1" <?php if($allpost->NoofVacancy==1) echo 'selected="selected"';?>>  1</option>
                                            <option value="2" <?php if($allpost->NoofVacancy==2) echo 'selected="selected"';?>> 2</option>
											<option value="3" <?php if($allpost->NoofVacancy==3) echo 'selected="selected"';?>> 3</option>
											<option value="4" <?php if($allpost->NoofVacancy==4) echo 'selected="selected"';?>> 4</option>
											<option value="5" <?php if($allpost->NoofVacancy==5) echo 'selected="selected"';?>> 5</option>
											<?php
											}
											for($v=6;$v<=50;$v++)
											{
											?>
                                            <option value="<?=$v;?>" <?php if($allpost->NoofVacancy==$v) echo 'selected="selected"';?>><?=$v;?></option>
											<?php
											}
											?>
                                        </select>
										 <select class="questions-category form-control " tabindex="0" aria-hidden="true" name="PostJob[NoofVacancyt]" id="teamvacancy" <?php if($allpost->PostFor!='Team'){?>style="display: none;" <?php }?>>
											<?php
											for($v=0;$v<=50;$v=$v+5)
											{
												$vnext=$v+5;
											?>
                                            <option value="<?=$v.'-'.$vnext;?>" <?php if($allpost->NoofVacancy==$v.'-'.$vnext) echo 'selected="selected"';?>><?=$v.'-'.$vnext;?></option>
											<?php
											}
											?>
										 </select>
										 
                                    </div>
                                </div>
								
								
								  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Job Shift:</label>
										
										<select
											class="questions-category form-control " tabindex="0"
											id="jbsft" aria-hidden="true" name="PostJob[JobShift]" required>
											<option value="Day" <?=(($allpost->JobShift == "Day")?"selected":"");?>>Day</option>
											<option value="Night" <?=(($allpost->JobShift == "Night")?"selected":"");?>>Night</option>
											<option value="Evening" <?=(($allpost->JobShift == "Evening")?"selected":"");?>>Evening</option>
											<option value="Day/Evening/Night" <?=(($allpost->JobShift == "Day/Dvening/Night")?"selected":"");?>>Day/Evening/Night</option>
											<option value="Rotational" <?=(($allpost->JobShift == "Rotational")?"selected":"");?>>Rotational</option>
										</select>
										<?php /*?>
                                        <input type="text" value="<?=$allpost->JobShift;?>" name="PostJob[JobShift]" class="form-control" pattern="[a-zA-Z_,-/\s]+" id="jobshift"> */?>
										<?php /*
										$jobshiftar = explode(", ",$allpost->JobShift);
										$jobshift='';
										foreach($jobshiftar as $key=>$value)
										{
											$value=trim($value);
											if($value!='')
											{
											$jobshift.='<div id="otherjobshift'.$key.'" style="width:auto;float:left;margin-left:10px;background:red;"><div style="width:auto;float:left;padding:5px;">'.$value.'</div><span onclick="removejobshift(&#39;'.$value.'&#39;,'.$key.');" style="width:auto; float:left;margin-top:-5px;cursor:pointer;color:#fff; margin-left:5px;font-size:16px;">x</span></div>';
											}
										}*/
										?>
										
										<div id="alljobshift" style="width: 100%; margin-top: 5px; height: 25px; padding: 3px;font-size:12px; color: #fff;"><?=$jobshift;?>
										</div>
                                    </div>
                                </div> 
										 
								 
								  <div class="col-md-12 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Description*  </label>
										
								<?= $form->field($allpost, 'JobDescription')->textarea([
									'options' => ['rows' => 6],'class'=>'form-control ckeditor'
								])->label(false);?>
								<p class="help-block help-block-error"></p>
                              </div> 
							   
							  </div>
							  
							   <div class="col-md-6 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Job Specification: </label>
								<?= $form->field($allpost, 'JobSpecification')->textarea([
									'options' => ['rows' => 6],'class'=>'form-control ckeditor'])->label(false);?>
                              </div> 
							   
							  </div>
							  
							  
							   <div class="col-md-6 col-sm-12 col-xs-12">
								    <div class="form-group"><label>Technical Guidance:  </label>
								<?= $form->field($allpost, 'TechnicalGuidance')->textarea([
									'options' => ['rows' => 6],'class'=>'form-control ckeditor'
								])->label(false);?>
                              </div> 
							   
							  </div>
							   <div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
									<div class="company-img">
										<?php
										if($allpost->docDetail)
										{
											$doc=$url.$allpost->docDetail->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
										?>
                                        <img src="<?=$doc;?>" class="img-responsive" alt="">
                                    </div>
								<label for="logo">Logo <span>(Optional)</span> <small>Max. file size: 8 MB.</small></label>
								<div class="upload">
									<?php echo  $form->field($docmodel, 'Doc')->widget(CropboxWidget::className(), ['croppedDataAttribute' => 'crop_info',])->label(false); ?>
								</div>
						
							  </div>
							</div> 
							<div class="clear"></div>
						
							<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="form-group">
								<label for="logo">Is Walking </label>
								<div class="upload">
									<input type="checkbox" id="iswalkin" value="1" name="PostJob[IsWalkin]" onclick="if($(this).prop('checked')==true){$('.walkinbox').show();$('.walkindate').prop('required',true);}else{$('.walkinbox').hide();$('.walkindate').removeAttr('required')}" <?php if($allpost->IsWalkin==1){ echo "checked";}?>>
								</div>
						
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 walkinbox" style="display: <?=($allpost->IsWalkin==1)?'block':'none';?>;">
								<div class="form-group">
								<label for="logo">From </label>
								<div class="upload">
									<input type="text" class="form-control walkindate" id="WalkinFrom" name="PostJob[WalkinFrom]"  readonly style="cursor: pointer;background: #fff;" autocomplete="off" value="<?=$allpost->WalkinFrom;?>"  placeholder="Date From"/>
								</div>
						
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 walkinbox" style="display: <?=($allpost->IsWalkin==1)?'block':'none';?>;">
								<div class="form-group">
								<label for="logo">To </label>
								<div class="upload">
									<input type="text" class="form-control walkindate" id="WalkinTo" name="PostJob[WalkinTo]"  readonly style="cursor: pointer;background: #fff;" autocomplete="off" value="<?=$allpost->WalkinTo;?>"  placeholder="Date To"/>
								</div>
						
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 walkinbox" style="display: <?=($allpost->IsWalkin==1)?'block':'none';?>;">
								<div class="form-group">
								<label for="logo">Walkin Time From </label>
								<div class="upload">
									<input type="text" class="form-control timepickerFrom" value="<?=$allpost->WalkinTimeFrom;?>"  name="PostJob[WalkinTimeFrom]" style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="Time From"/>
								</div>
						
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 walkinbox" style="display: <?=($allpost->IsWalkin==1)?'block':'none';?>;">
								<div class="form-group">
								<label for="logo">Walkin Time To </label>
								<div class="upload">
									<input type="text" class="form-control timepickerTo" name="PostJob[WalkinTimeTo]"  style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="Time To" value="<?=$allpost->WalkinTimeTo;?>"/>
								</div>
						
								</div>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12 walkinbox" style="display: <?=($allpost->IsWalkin==1)?'block':'none';?>;">
								<div class="form-group">
								<label for="logo">Venue </label>
								<div class="upload">
									<input type="text" class="form-control " id="WalkinTo" name="PostJob[Venue]" value="<?=$allpost->Venue;?>"  style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="Venue"/>
								</div>
						
								</div>
							</div>
							<div class="clear"></div>
							
							<div class="col-md-6 col-sm-12 col-xs-12">
								<div class="form-group">
								<label for="logo"><input type="checkbox" value="1" <?=($allpost->AltEmail!='')?'checked':'';?> id="forresume" onclick="if($(this).prop('checked')==true){$('#altemailbox').show();}else{$('#altemailbox').hide();}"/><span>Ask job seekers for resume</span></label>
								<div class="upload" id="altemailbox" style="display:<?=($allpost->AltEmail!='')?'block':'none';?>;">
									<p class="help-block help-block-error">Applications for this job will sent to the following email address</p>
									<div class="form-group" id="additionalemail">
									<?php
									$altemailar=explode("|",$allpost->AltEmail);
									$ec=0;
									foreach($altemailar as $e=>$em)
									{
										$ec++;
									?>
									<div id="additional<?=$ec;?>">
									<input type="text" class="form-control altemail" name="PostJob[AltEmail][]"  style="cursor: pointer;background: #fff;" autocomplete="off" value="<?=$em;?>"/>
									<span  onclick="$('#additional<?=$ec;?>').remove();">X</span>
									</div>
									<?php
									}
									?>
									<div id="additional<?=$ec;?>">
									<input type="text" class="form-control altemail" name="PostJob[AltEmail][]"  style="cursor: pointer;background: #fff;" autocomplete="off" />
									<span  onclick="$('#additional<?=$ec;?>').remove();">X</span>
									</div>
									</div>
									
									<span onclick="addadditionalemail();">  + Add Additional Email</span>
									
									
								</div>
						
								</div>
							</div>
							
							<div class="col-md-12 col-sm-12 col-xs-12">
							
							<div class="form-group">
							<?= Html::submitButton('UPDATE JOB', ['class' => 'btn btn-default btn-green']) ?>
						</div>
						</div>
						
						
						
                           <?php ActiveForm::end(); ?>
					
				</div>
				
				 
				
			</div>
		</div>

      </div>
		    </div>
		<div class="border"></div>
        </div>

<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<?php 
	$courseData = (!empty($allpost->course)?explode(',', $allpost->course):array());
	$specializationData = (!empty($allpost->SpecialQualification)?explode(',', $allpost->SpecialQualification):array());
	
	
?>
<!--$("#naukari-specialization").val(selectedQual);
		$('#naukari-specialization').select2(selectedQual, null, false); -->
	

<script type="text/javascript">
	setTimeout(function(){
		var loc = new locationInfo();
		$('#stateId').val('<?=$state;?>');
		var loc1='<?=$state;?>';
		 var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');
		 console.log(stateId);
        if(stateId != '' || !stateId){
        loc.getCities(stateId);
        }
		
		setTimeout(function(){$('#cityId').val('<?=$city;?>');},2000);
		
	},8000);
	
	var data = [{id: '',text: 'Select course'}];
	var Ldata = [{id: '',text: 'Select Specialization'}];
	var selectedQual = [];
	var selectedCourse = [];
	var selectedWhiteRole = [];
	var selectedWhiteSkills = [];
	
	<?php
	
	if(!empty($courseData)){
		foreach($courseData as $courseVal){?>
			selectedQual.push("<?=$courseVal;?>");
	<?php }
	}
	if(!empty($specializationData)){
		foreach($specializationData as $special){?>
			selectedCourse.push("<?=$special;?>");
	<?php }
	}
	if(!empty($selectedWhiteRole)){
		foreach($selectedWhiteRole as $roleId => $roleName){?>
			selectedWhiteRole.push("<?=$roleId;?>");
	<?php }
	}
	
	if(!empty($selectedWhiteSkills)){
		foreach($selectedWhiteSkills as $skillId){?>
			selectedWhiteSkills.push("<?=$skillId;?>");
	<?php }
	}
$is_otrher = 0;
foreach ($naukari_specialization as $key => $value) {
    if ($value == 'other' && $is_otrher == 1) {
        continue;
    }
    ?>
				var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
				data.push(d);
		<?php
    if ($value == 'other') {
        $is_otrher = 1;
    }
}
?>
	<?php foreach ($NaukariQualData as $key => $value) { ?>
				var ds = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
				Ldata.push(ds);
		<?php }  ?>
	$("#naukari-specialization").select2({
  		data: data,
  		maximumSelectionLength: 5
	});	
	
	$("#naukari-specialization").val(selectedQual);
	$('#naukari-specialization').select2(selectedQual, null, false);
		
	$("#specialskill").select2({
  		data: Ldata,
  		maximumSelectionLength: 5
	});
	
	$("#specialskill").val(selectedCourse);
	$('#specialskill').select2(selectedCourse, null, false);
	
	$("#naukari-qualification").select2({
  		maximumSelectionLength: 5
	});
	
	$("#white_callar_role").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Role"
	});
	
	$("#white_callar_role").val(selectedWhiteRole);
	$('#white_callar_role').select2(selectedWhiteRole, null, false);
	
	$(document).on('change', '#white_callar_category', function(){
		var value = $(this).val();
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role').html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role').append(option);
						});
					}
				}
			});
		}
		
	});
	
	$("#white_callar_skills").val(selectedWhiteSkills);
	$('#white_callar_skills').select2(selectedWhiteSkills, null, false);
	$("#white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});

	$("#naukari-location").select2({
		maximumSelectionLength: 5,
		placeholder: "Select Max 5 Preferred Locations ...",
		ajax: {
			url: '<?=Url::toRoute(['getpreferredlocation']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.CityName,
							id: item.id
						}
					})
				};
			}
		 }
	});
	
	$('.timepickerFrom').timepicker({
		timeFormat: 'h:mm p',
		interval: 60,
		minTime: '10',
		maxTime: '11:00pm',
		startTime: '10:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});
	$('.timepickerTo').timepicker({
		timeFormat: 'h:mm p',
		interval: 60,
		minTime: '10',
		maxTime: '11:00pm',
		startTime: '10:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true
	});


	
	
	
	$('.dropdown-sin-1').dropdown({
      readOnly: true,
	  limitCount:5,
      input: '<input type="text" maxLength="5" placeholder="Search">'
    });
</script>