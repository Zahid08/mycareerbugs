<style type="text/css">
.black_overlay{display: none;position: absolute;top: 0%;left: 0%;width: 100%;height: 100%;background-color: rgba(255,255,255,0.9);z-index:1001;
-moz-opacity: 0.8;
opacity:.80;filter: alpha(opacity=80);}
#loading {display: none;position: absolute;top: 20%;left: 40%;padding: 16px;width: 201px;height:201px;z-index:1002;overflow: auto;background-image: url('/img/ajax-loader-bar.gif');background-repeat: no-repeat}
h4 {font-size: 18px;}
.radio, .checkbox { margin-top: 5px;margin-bottom: 5px;}


.find_a_job{display:none;}
@media only screen and (max-width: 767px){
#right-side{background:#fff}  
.jobsearch-continer {background:#e9eaed; margin-top: 4px; padding: 3px 0;}
.item-list{background:#fff;padding-top:0px;}
.job_search.new12 {background: #e9eaed;clear: both;overflow: hidden;padding: 20px 0 0 0;margin: 0;}
.need_help{display:none}
.filter_content{width:100%;}
.height_set{height:84px !important;}
}


</style>

<?php
$this->title = 'Job Search, My Career Bugs - mycareerbugs.com';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\AppliedJob;
use common\models\City;
use kartik\select2\Select2;
use yii\web\JsExpression;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use yii\widgets\LinkPager;
$appliedjob=new AppliedJob();
$ctyloc = City::find()->all();

 foreach($ctyloc as $cty){	
			$che[]  =    $cty->CityName ;	
			$keys[] =  $cty->CityName ;		
		}			 			 
		$cj =array_combine($che,$keys);	
		
?>

<meta name="keywords" content="Online Job Search, My Career Bugs Job Search, Latest jobs in kolkata, Jobs in Delhi, Jobs in Pune, Jobs in Banglore, Jobs in Chennai, latest jobs in delhi" /> 

<meta name="description" content="Job Search made easier through filters. MCB caters the most relevant Jobs to you. Get relevant notifications in your inbox." /> 
 
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>




<div id="wrapper"><!-- start main wrapper -->

		<div class="job_search new12"><!-- Start Recent Job -->
			<div class="container">
				<div class="rows">
					<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side"> 
			 
	 <h4 class="job_head"> Job Filter</h4>
	 
	  <div class="col-md-122">
	 
		<?php if(isset($type) && $type == "team"){?>
		 <input type="hidden" id="search-type" value="team"/>
		
						<a href="<?= Url::toRoute(['site/teamjobsearch']);?>">


                          <div class="pannel_header margin_top">
			      <label>Clear </label>
			     </div>
                           </a>
		<?php }else{?>	
		
			 <input type="hidden" id="search-type" value=""/>
				<a href="<?= Url::toRoute(['site/jobsearch']);?>">


                          <div class="pannel_header margin_top">
			      <label>Clear </label>
			     </div>
                           </a>
		<?php }?>	
			  </div>







	 <a href="javascript:void(0)" class="filter"> <i class="fa fa-filter"></i> Filter Option</a> 
		
	 <div class="clear"></div>
	 <div class="clearfix"></div>
	 
	 
	  <div class="filter_content">
	      
	      
				
	      
	      
	 
            <div class="panel-group" id="accordion">
			<input type="hidden" id="#is_campus_search" value="1"/>
			 <div class="panel panel-default" >
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne12"><h4 class="panel-title">
							 Latest
                        </h4></a>
                    </div>
                    <div id="collapseOne12" class="panel-collapse collapse">
                        <div class="panel-body">
								<?php
								if(isset($_GET['latest']))
								{
										$latest=$_GET['latest'];}else{$latest='';}
								?>
								<select class="form-control" id="latest" onchange="localStorage.jobsearch='collapseOne12';getsearch();">
										<option value="">Select</option>
										<option value="1" <?php if($latest==1) echo "selected='selected'";?>>1 Days</option>
										<option value="3" <?php if($latest==3) echo "selected='selected'";?>>3 Days</option>
										<option value="7" <?php if($latest==7) echo "selected='selected'";?>>7 Days</option>
										<option value="15" <?php if($latest==15) echo "selected='selected'";?>>15 Days</option>
										<option value="30" <?php if($latest==30) echo "selected='selected'";?>>30 Days</option>
						        </select>
                        </div>
                    </div>
                </div>
				
				<div class="panel panel-default">
                    <div class="panel-heading">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">   <h4 class="panel-title">
                        
							 Location
                        </h4></a>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
						   <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
						<!-- <select class="questions-category form-control" id="state_new" onchange='localStorage.jobsearch="collapseOne";getsearch();'></select> -->

						   	<?php 
							$url = Url::toRoute(['getpreferredlocation']);
							echo $form->field($jbpst, 'Location')->widget(Select2::classname(), [
								
								// 'data' => ArrayHelper::map(Cities::find()->all(), 'CityId', 'CityName'),
								
								'language' => 'en',
								'options' => ['placeholder' => 'Select Cities ...', 'onchange' => 'localStorage.jobsearch="collapseOne";getsearch();', 'id'=>'state'],
								'pluginOptions' => [
								    //'allowClear' => true,
									'multiple' => false,
									'maximumSelectionLength' => 5,
									'ajax' => [
										'url' => $url ,
										'dataType' => 'json' ,
										'data' => new JsExpression( 'function(params) { return {q:params.term, page:params.page || 1}; }' )
									] ,
									'escapeMarkup' => new JsExpression ( 'function (markup) { return markup; }' ) ,
									'templateResult' => new JsExpression ( 'function(product) { console.log(product);return product.CityName; }' ) ,
									'templateSelection' => new JsExpression ( 'function (subject) { return subject.CityName; }' ) ,
								],
							])->label(false);
							
	
							
							
						   	/*echo $form->field($jbpst, 'Location')->widget(kartik\select2\Select2::classname(), [
												'data' => $cj,
												'language' => 'en',
												'options' => [ 'placeholder' => 'Select Cities ...' ,'id'=>'state','onchange' => 'localStorage.jobsearch="collapseOne";getsearch();'],
												 
											])->label(false);*/
											?>
                      
			 <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
				
				
				
				     
            <div class="panel panel-default" >
                
                 <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne19"><h4 class="panel-title">
							 Category, Role and Skill
                        </h4></a>
                    </div>
                    
                <div id="collapseOne19" class="panel-collapse collapse">      
                	<div class="panel-group" style="padding:0 10px 10px 10px; margin-bottom:0px">
					<div class="panel panel-default" style="border:0px;box-shadow: none;">
						<div class="panel-heading sty" style="background:#fff !important; padding:0px 10px">
							<div class="col-md-6 no-pad-123">
								<a style="padding: 6px 0"><h4 class="panel-title" style="font-size:12px;color:#000">	<input type="radio" value="blue" name="CollarType" class="collar-type"> Blue Collar
								 <span class="checkmark"></span>
								 </h4></a>
							</div>

							<div class="col-md-6 no-pad-123">
								<a style="padding: 6px 0"><h4 class="panel-title" style="font-size:12px; color:#000"><input type="radio" value="white" name="CollarType" class="collar-type"> White Collar
								 <span class="checkmark"></span>
								 </h4> </a>
							</div>

						</div>
						
						
						<div class="panel-body" id="blue-collar-fields" style="display:none; border: 0px; padding:0 10px 1px 10px">
						     <h4 class="panel-title" style="padding: 10px 0;">Role</h4> 
						<?php
								if(isset($_GET['role']))
								{$role1=$_GET['role'];}else{$role1='';}?>
                           <select class="form-control" onchange="selectposition(this.value)" id="position"><option value="">Select</option>
						   <?php
						   foreach($role as $rk=>$rvalue)
						   {
						   ?>
						   <option value="<?=$rvalue->PositionId;?>" <?php if($role1==$rvalue->PositionId) echo "selected='selected'";?>><?=$rvalue->Position;?></option>
						   <?php
						   }
						   ?>
						   </select> 
						   <h4 class="panel-title" style="padding: 10px 0;">Job By Skill</h4> 
						   <div class="panel-body" id="jobbyskill" style="max-height: 200px;overflow: auto;">
							<?php
							if(isset($_GET['skillby']) && $_GET['skillby']!='')
											{
											$skillby=explode(",",$_GET['skillby']);
											
											

									foreach($allskill as $sk=>$svalue)
									{
									?>
									<span> 
											<div class="checkbox">
											<label> <input type="checkbox" class="skillby"  onclick="getsearch()" value="<?=$svalue->SkillId;?>" <?php if(in_array($svalue->SkillId,$skillby)) echo "checked";?>>
											<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
											<a><?=$svalue->Skill;?> </a>
											</label>
											</div>	 
									</span>
									<?php
											}
											} ?>
									</div>
                        </div>
						
						<div class="panel-body" id="white-collar-fields" style="display:none;border: 0px">
							<select class="questions-category form-control" name="WhiteCategory" tabindex="0" aria-hidden="true" id="white_callar_category" onchange="selectposition(this.value)">
							 <option value="" >Select Category</option>
							 <?php if(!empty($whiteCategories)){
									foreach($whiteCategories as $categoryId => $categoryName){?>
										<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
									<?php  }
									}?>
							 
							</select> <br/>
							<select class="questions-category form-control" name="WhiteRole[]" id="white_callar_role" multiple="multiple"></select> <br/>
							
							<select class="questions-category form-control" id="white_callar_skills" name="WhiteSkills[]" multiple="multiple"></select>
						</div>
						
						
						
						
					</div>
				</div>
                  </div>
                
                 </div>   
                 
                 
                 
			  <div class="panel panel-default" style="display:none">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne13"><h4 class="panel-title">
							 Role
                        </h4></a>
                    </div>
                    <div id="collapseOne13" class="panel-collapse collapse">
                       
                    </div>

					
                </div>
				
              
				
				<div class="panel panel-default" style="display:none;">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><h4 class="panel-title">
                             Job By Company/HR/Consultancy
                        </h4></a>
                    </div>
                    <div id="collapseSeven" class="panel-collapse collapse">
                        <div class="panel-body" style="max-height: 200px;overflow: auto;">
				<?php
				if(isset($_GET['companyby']) && $_GET['companyby']!='')
								{
								$jobbycompany=explode(",",$_GET['companyby']);
								}
								else
								{
								$jobbycompany=array();
								}
				foreach($allcompany as $ck=>$cvalue)
				{
				?>
				<span> 
						<div class="checkbox">
						<label> <input type="checkbox" class="companyby" value="<?=$cvalue->CompanyName;?>" <?php if(in_array($cvalue->CompanyName,$jobbycompany)) echo "checked";?>>
						<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
						<a><?=$cvalue->CompanyName;?> </a>
						</label>
						</div>	 
				</span>
				<?php
				}
				?>
			</div>
		    </div>
		</div>
				
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse30"><h4 class="panel-title">
                             Job Search By
                        </h4></a>
                    </div>
                    <div id="collapse30" class="panel-collapse collapse">
                        <div class="panel-body" style="max-height: 200px;overflow: auto;">
									<?php
										if(isset($_GET['postby']) && $_GET['postby']!='')
										{
										$postby=explode(",",$_GET['postby']);
										}
										else
										{
											$postby=array();
										}
										?>
						            <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="postby" value="Company"  <?php if(in_array('Company',$postby)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Company Jobs   </label>
								      </div>	 
							       </span>
								   <div class="Company-list" style="border:1px solid #ccc; padding:5px 10px; display:none;">
								   <!--Company List-->
								   <?php
									if(isset($_GET['companyby']) && $_GET['companyby']!='')
									{
									$jobbycompany=explode(",",$_GET['companyby']);
									}
									else
									{
									$jobbycompany=array();
									}
									foreach($allcompany as $ck=>$cvalue)
									{
									?>
									<span> 
											<div class="checkbox">
											<label> <input type="checkbox" class="companyby" value="<?=$cvalue->CompanyName;?>" <?php if(in_array($cvalue->CompanyName,$jobbycompany)) echo "checked";?>>
											<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
											<a><?=$cvalue->CompanyName;?> </a>
											</label>
											</div>	 
									</span>
									<?php
									}
									?>
								   </div>
								 
								     <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="postby" value="HR" <?php if(in_array('HR',$postby)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>HR Jobs </label>
								      </div>	 
							       </span>
								   
								   <div class="HR-list" style="border:1px solid #ccc; padding:5px 10px; display:none;">
								   <!--Company List-->
								   <?php
									if(isset($_GET['companyby']) && $_GET['companyby']!='')
									{
									$jobbycompany=explode(",",$_GET['companyby']);
									}
									else
									{
									$jobbycompany=array();
									}
									if(!empty($allHR)){
									foreach($allHR as $ck=>$cvalue)
									{
									?>
									<span> 
											<div class="checkbox">
											<label> <input type="checkbox" class="companyby" value="<?=$cvalue->CompanyName;?>" <?php if(in_array($cvalue->CompanyName,$jobbycompany)) echo "checked";?>>
											<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
											<a><?=$cvalue->CompanyName;?> </a>
											</label>
											</div>	 
									</span>
									<?php
									}}
									?>
								   </div>
							       
							       
							         <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="postby" value="Consultancy" <?php if(in_array('Consultancy',$postby)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Consultancy   </label>
								      </div>	 
							       </span>
								   
								   <div class="Consultancy-list" style="border:1px solid #ccc; padding:5px 10px; display:none;">
								   <!--Company List-->
								   <?php
									if(isset($_GET['companyby']) && $_GET['companyby']!='')
									{
									$jobbycompany=explode(",",$_GET['companyby']);
									}
									else
									{
									$jobbycompany=array();
									}
									if(!empty($allConsultancy)){
									foreach($allConsultancy as $ck=>$cvalue)
									{
									?>
									<span> 
											<div class="checkbox">
											<label> <input type="checkbox" class="companyby" value="<?=$cvalue->CompanyName;?>" <?php if(in_array($cvalue->CompanyName,$jobbycompany)) echo "checked";?>>
											<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
											<a><?=$cvalue->CompanyName;?> </a>
											</label>
											</div>	 
									</span>
									<?php
									}}
									?>
								   </div>
						</div>
                    </div>
                </div>
				
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><h4 class="panel-title">
                             Salary
                        </h4></a>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body" style="max-height: 200px;overflow: auto;">
								<?php
								if(isset($_GET['salaryrange']) && $_GET['salaryrange']!='')
								{
								$salaryrange=explode(",",$_GET['salaryrange']);
								}
								else
								{
										$salaryrange=array();
								}
								//echo "<pre>";
								//print_r($salaryrange);
								//die;
								?>
						            <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="0 - 1.5 Lakhs" <?php if(in_array("0 - 1.5 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">0 - 1.5 Lakhs</a>  </label>
								      </div>	 
							       </span>
								 
								     <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="1.5 - 3 Lakhs" <?php if(in_array("1.5 - 3 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">1.5 - 3 Lakhs</a>  </label>
								      </div>	 
							       </span>
								    
								       <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="3 - 6 Lakhs" <?php if(in_array("3 - 6 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">3 - 6 Lakhs</a>   </label>
								      </div>	 
							       </span>
								   
								   
								       <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="6 - 10 Lakhs" <?php if(in_array("6 - 10 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">6 - 10 Lakhs</a>  </label>
								      </div>	 
							       </span>
								  
								       <span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="10 - 15 Lakhs" <?php if(in_array("10 - 15 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">10 - 15 Lakhs</a>  </label>
								      </div>	 
							       </span>
								   
								    
								<span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="15 - 25 Lakhs" <?php if(in_array("15 - 25 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">15 - 25 Lakh</a>   </label>
								      </div>	 
							       </span>
								<span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="Above 25 Lakhs" <?php if(in_array("Above 25 Lakhs",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">Above 25 Lakh</a>   </label>
								      </div>	 
							       </span> 
							       
							       	<span> 
									   <div class="checkbox"> <label> <input type="checkbox" class="salary" value="Negotiable" <?php if(in_array("Negotiable",$salaryrange)) echo "checked";?>>
									   <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a href="javascript:void(0);">Negotiable</a>   </label>
								      </div>	 
							       </span>   
							
                        </div>
                    </div>
                </div>
				
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#expcollapseSix"><h4 class="panel-title">
                             Industry
                        </h4></a>
                    </div>
                    <div id="expcollapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
				<?php
				if(isset($jobcategoryid) && $jobcategoryid!='')
								{
								$industryval=$jobcategoryid;
								}
								else
								{
								$industryval='';
								}
				?>
				 <select class="form-control bfh-states" id="industry" onchange="localStorage.jobsearch='expcollapseSix';getsearch();">
								<option value="">Select</option>
								<?php
								if($industry)
								{
								foreach($industry as $lk=>$lv)
								{
								?>
								<option value="<?=$lv->IndustryId;?>" <?php if($industryval==$lv->IndustryId) echo 'selected';?>><?=$lv->IndustryName;?></option>
								<?php
								}
								}
								?>
							</select>
				
			</div>
		    </div>
		</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><h4 class="panel-title">
                             Job Type
                        </h4></a>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body" style="max-height: 200px;overflow: auto;">
                           <?php
								if(isset($_GET['jobtype']) && $_GET['jobtype']!='')
								{
								$jobtype=explode(",",$_GET['jobtype']);
								}
								else
								{
										$jobtype=array();
								}
								?>
								<span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Full Time" <?php if(in_array('Full Time',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Full Time
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Part Time" <?php if(in_array('Part Time',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Part Time
									</label>
								  </div>	 
							     </span>
							      <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Work From Home" <?php if(in_array('Work From Home',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Work From Home
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Fresher" <?php if(in_array('Fresher',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Fresher
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Walkin" <?php if(in_array('Walkin',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Walkin
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Internship" <?php if(in_array('Internship',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Internship
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Contract" <?php if(in_array('Contract',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Contract
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Commission" <?php if(in_array('Commission',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Commission
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Temporary" <?php if(in_array('Temporary',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Temporary
									</label>
								  </div>	 
							     </span>
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="jobtype" value="Volunter" <?php if(in_array('Volunter',$jobtype)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Volunter
									</label>
								  </div>	 
							     </span>
								
                        </div>
                    </div>
                </div>
				
				
				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsenine"><h4 class="panel-title">
                             Eligibility
                        </h4></a>
                    </div>
                    <div id="collapsenine" class="panel-collapse collapse">
                        <div class="panel-body">
                           <?php
								if(isset($_GET['experience']) && $_GET['experience']!='')
								{
								$experience=explode(",",$_GET['experience']);
								}
								else
								{
										$experience=array();
								}
								if(isset($_GET['expno']) && $_GET['expno']!='')
								{
										$expno=$_GET['expno'];
								}else{$expno='';}
								?>
						         <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="experience" value="1" <?php if(in_array(1,$experience)) echo "checked";?>>
									<span class="cr">
										<i class="cr-icon glyphicon glyphicon-ok"></i></span><a> Experience</a>
										<span >
										<select class="questions-category form-control  " tabindex="-1" aria-hidden="true" style="z-index: 9999;position: relative;display:<?=((in_array(1,$experience)))?'block':'none';?>" id="jobexp" onchange="localStorage.jobsearch='collapsenine'; getsearch(); ">
												<option value="">Choose Years</option>
										<option value="0-1" <?php if($expno=='0-1') echo 'selected';?>>  Below 1 Year</option>
					    <?php
					    for($fr=1;$fr<=30;$fr=$fr+1)
					    {
							$frn=$fr+1;
					?>
                                            <option value="<?=$fr.'-'.$frn;?>" <?php if($expno==$fr.'-'.$frn) echo 'selected';?>><?=$fr.'-'.$frn;?> Years</option>
                                           <?php
					    }
					    ?>
                                        </select>
										</span>
									</label>
								  </div>	 
							     </span>
								 
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="experience" value="0" <?php if(in_array(0,$experience)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a>Fresher </a>
									</label>
								  </div>	 
							     </span>
								 
								  <span> 
									<div class="checkbox">
									<label> <input type="checkbox" class="experience" value="2" <?php if(in_array(2,$experience)) echo "checked";?>>
									<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span><a>Both - Fresher & Experienced</a>
									</label>
								  </div>	 
							     </span>
								
                        </div>
                    </div>
                </div>
		
		

				<div class="panel panel-default">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#accordion" href="#educollapseSix"><h4 class="panel-title">
                             Education
                        </h4></a>
                    </div>
                    <div id="educollapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
				<?php
				if(isset($_GET['education']) && $_GET['education']!='')
								{
								$education=$_GET['education'];
								}
								else
								{
								$education='';
								}
								if(isset($_GET['nak_course']) && $_GET['nak_course']!='')
								{
								$nak_course=$_GET['nak_course'];
								}
								else
								{
								$nak_course='';
								}
								if(isset($_GET['qualification']) && $_GET['qualification']!='')
								{
								$qualification_rec =$_GET['qualification'];
								}
								else
								{
								$qualification_rec='';
								}
				
				?>
					<select class="form-control" style="margin-bottom:10px" id="qualification"  onchange="localStorage.jobsearch='educollapseSix';getsearch();">
					<option value="">Select Qualification</option> 
					<?php
					// if($course)
					foreach($qualifications as $key1=>$value1){?>
						<option value="<?=$value1->name;?>" <?php if($qualification_rec==$value1->name) echo "selected='selected'";?>><?=$value1->name;?></option>
					<?php }?>	
					</select>
					<select class="form-control" style="margin-bottom:10px" id="nak_course"  onchange="localStorage.jobsearch='educollapseSix';getsearch();">
				<option value="">Select Course</option> 
				<?php
				// if($course)
				$is_otrher = 0;
				foreach($naukari_specialization as $key1=>$value1)
										{
											if($value1->name=='Other' && $is_otrher == 1){
												continue;
											}
										?>
										<option value="<?=$value1->name;?>" <?php if($nak_course==$value1->name) echo "selected='selected'";?>><?=$value1->name;?></option>
										<?php
										if($value1->name=='Other'){$is_otrher = 1;}
										}
										?>
				</select>
				
				
				<select class="form-control" id="education"  onchange="localStorage.jobsearch='educollapseSix';getsearch();">
				<option value="">Select Specializations </option> 
				<?php
				// if($course)
				// {
				foreach($NaukariQualData as $key=>$value)
										{
										?>
										<option value="<?=$value->name;?>" <?php if($education==$value->name) echo "selected='selected'";?>><?=$value->name;?></option>
										<?php
										}
										?>
				</select>
			
				
			</div>
		    </div>
		</div>


                <div class="panel panel-default" style="display:none">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">  Reports</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                             
                        </div>
                    </div>
                </div>
            </div>
		       </div>	 
			
			<div class="spacer-5"></div>
			<div class="widget-heading"><span class="title">Hot Categories </span></div>
			
			<div class="spacer-5"></div>
        <div class="widget"> 
                                    <ul class="categories-module">
                                       <?php
								if($hotcategory)
								{
								foreach($hotcategory as $hkey=>$hvalue)
								{
								    $string=Yii::$app->myfunctions->clean(trim($hvalue->CategoryName));
								?>
								<li> <a href="<?= Url::toRoute(['site/jobsearch/'.$string])?>"> <?=$hvalue->CategoryName;?> <span>(<?=$hvalue->cnt;?>)</span> </a> </li>
								<?php
								}
								}
								?>
                                    </ul>
                                </div>
					</div>
					
					 
					
					<div class="col-lg-9  col-md-9 col-sm-9 col-xs-12 jobsearch-continer" id="jobsearch-continer">
						<?= $this->render('jobsearch_ajax', array('alljob'=>$alljob,'pages'=>$pages)); ?>
					</div>
					
					
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		</div>	
		<div class="border"></div>
</div>
<script type="text/javascript">
		setTimeout(function(){
				<?php
				if(isset($_GET['state']))
								{
										?>
				var state='<?=$_GET['state'];?>';
				$('#state').val(state);
				<?php
								}
								?>
		},2000);
		$(document).on('change', '.skillby', function() {
			localStorage.jobsearch='collapseSix';
            getsearch(); 	
		});
		$( document ).ready(function() {
    		$.ajax({url:mainurl+"site/skills?roleid="+$('#position').val()+"&type="+'',
               success:function(results)
            	{
                	$('#jobbyskill').html(results);
            	}
        	});
		});
		$(document).on('click', 'ul.pagination li a', function(event) {
			event.preventDefault();
    		var url = $(this).attr('href');
    		jobsearchAjax(url);
		});
		$("#white_callar_role").select2({
			placeholder: "Please select role",
			allowClear: true
		});
	
	$(document).on('change', '#white_callar_category', function(){
		var value = $(this).val();
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role').html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role').append(option);
						});
					}
				}
			});
		}
		
	});
	
	$("#white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {
						
						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});
	
	
	    $(document).ready(function(){
  $('.need_help span' ).click(function(){
			$('.need_help').animate({right:0});  
				});});
			$('.closed a').click(function(){
				$('.need_help').animate({right:-270}); 
				return true
	});
	
	$(document).ready(function(){
		$('.collar-type').click(function(){
			var val = $(this).val();
			if(val == "white"){
				$('#blue-collar-fields').hide(0);
				$('#white-collar-fields').show(0);
			}else{
				$('#blue-collar-fields').show(0);
				$('#white-collar-fields').hide(0);
			}
		});
		
		$(document).on('click', '.postby', function(){
			var value = $(this).val();
			if($(this).is(':checked')){
				$('.'+value+'-list').show(0);
			}else{
				$('.'+value+'-list').hide(0);
			}
		});
	});
</script>