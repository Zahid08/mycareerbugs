<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use common\models\AppliedJob;
use common\models\AllUser;
$appliedjob=new AppliedJob();
$companyData = array();
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
if(!empty($_GET['company'])){
	$companyData = AllUser::find()->where([
                'UserId' => $_GET['company'],
           ])->one();
}


?>
						<div class="col-md-8">
						<div class="pannel_header margin_top" id="mbl-no-nd">
								<h4> All Job</h4>
						</div>
						</div>
						
						<div class="pannel_header margin_top" style="margin: 0px;">
						<span id="searchdiv" style="font-size: 14px;color: #f16b22;">
								<?=(isset($companyData->Name))?'Search > Company: '.$companyData->Name:'';?>
								<?=(isset($_GET['city']))?'Search > Location: '.$_GET['city']:'';?>
						</span>
						</div>
		 <?php
		if($alljob)
		{
		foreach($alljob as $jkey=>$jvalue)
		{
				if(isset(Yii::$app->session['Employeeid']))
				{
				$iscount=$appliedjob->getIsapplied($jvalue->JobId,Yii::$app->session['Employeeid']);
				}elseif(Yii::$app->session['Teamid'])
                {
						$iscount=$appliedjob->getIsapplied($jvalue->JobId,Yii::$app->session['Teamid']);
				}else{$iscount=0;}
		?>
		 <div class="item-list">
                <a href="<?= Url::base().'/job/'.$jvalue->Slug;?>" target="_blank">
                <div class="col-sm-12 add-desc-box">
                  <div class="add-details">
                      
                      
                      		<div class="company-img">



										<?php
			
		
        if ($jvalue->docDetail) {
			$url = $imageurl."backend/web/";
            $doc = $url . $jvalue->docDetail->Doc;
        } else {

            $doc = $imageurl . 'images/user.png';
        }

        ?>



                                        <img src="<?=$doc;?>"
										class="img-responsive" alt="" style="border-radius: 200px;">



								</div>
								
<style>
    
.company-img{width:6%; float:left; margin-right:20px;}
.cont_rught{float:right; width:91%;}
.cont_rught h5{    margin: 0px;padding: 7px 0 0 0;line-height: 17px;font-size: 14px;font-weight: bold;}
.cont_rught .info{padding:0px;}
.height_set{height:36px; overflow:hidden; margin-bottom:5px;}
.category.height_set h1, .category.height_set h2, .category.height_set h3, .category.height_set h4, .category.height_set h5, .category.height_set h6{  margin:0px;padding:0 0 5px 0;  font-size: 14px;}
</style>								
								
								
								
			<div class="cont_rught">		
                      
                    <h5 class="add-title"><?=$jvalue->JobTitle;?></h5>
					<?php
					if($iscount==1)
					{
						?>
					<img src="<?=$imageurl;?>images/applied.png" class="applied"/>
					<?php
					}
					?>
                    <div class="info"> 
                      <span class="category"> <?=$jvalue->jobCategory->IndustryName;?>  </span> -
                      <span class="item-location"><i class="fa fa-map-marker"></i> <?=$jvalue->Location;?></span> <br>
                    <span> <strong><?=$jvalue->CompanyName;?></strong> </span>
					</div>
					
						</div>
						
					<div class="clear"></div>
					
					<?php
					if($jvalue->IsWalkin==1)
					{
				?>
				
	
 			
		
				
				
						<div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Walkin : </span>  
					    </div>
						<div class="col-sm-10 col-xs-8 left-text">
							<span class="category"><?=date('d M Y',strtotime($jvalue->WalkinFrom)).' - '.date('d M Y',strtotime($jvalue->WalkinTo));?></span>  
                      </div>
					 </div>
				<?php
					}
					?>
                    <div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Eligibility : </span>  
					    </div>
                    <div class="col-sm-10 col-xs-8 left-text">
							<?php
						if($jvalue->IsExp==0){
								$exp='Fresher';
						}
						elseif($jvalue->IsExp==1)
						{
								$exp=($jvalue->Experience!='')?$jvalue->Experience.' Years':'Experience';
						}
						elseif($jvalue->IsExp==2)
						{
								$exp='Both - Freshers & Experienced';
						}
						?>
							<span class="category"><?=$exp?></span> 
                      </div>
					 </div> 
					 
					 
					 </strong> 
					 	
					 	 	
					 	
					  <div class="info bottom">
					     <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Designation : </span>  
					    </div>
					<div class="col-sm-10 col-xs-8 left-text">
							<span class="category"> 	
							  <?=$jvalue->Designation;?>
							</span>  
                      </div>
					 </div>
					 
					 	
					  <div class="info bottom" style="display:none">
					     <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Keyskills : </span>  
					    </div>
					<div class="col-sm-10 col-xs-8 left-text">
							<span class="category">
								<?php
								$jskill='';
								foreach($jvalue->jobRelatedSkills as $k=>$v)
								{
								$jskill.=$v->skill->Skill.' , ';
								}
								echo $jskill;
								?>
							</span>  
                      </div>
					 </div>
					
					   <div class="info bottom">
					    <div class="col-sm-2 col-xs-4 mmbl_fl1">
					    	<span class="styl">Job Description : </span>  
					    </div>
                      <div class="col-sm-10 col-xs-8 left-text mmbl_fl1">
							<span class="category height_set"><?=strip_tags($jvalue->JobDescription);?></span>  
                      </div>
					 </div>
					 
					  <div class="info bottom">
					    <div class="col-sm-2 col-xs-4">
					    	<span class="styl">Salary Range </span>  
					    </div>
							<div class="col-sm-10 col-xs-8 left-text">
							    <?php
							    //if (strpos($jvalue->Salary, 'Lakhs') !== false) { ?>
							        <span class="category"><?=$jvalue->Salary;?>  </span>  
                                <?php //} else {   
                                //$jvalue->Salary = $jvalue->Salary." Lakhs"; ?>
                                <!--<span class="category"><?=$jvalue->Salary;?>  </span>  -->
                                <?php // }?>
							
                      </div>
					 </div> 
					 
					<div class="info bottom"> 
						<span class="category" style="text-align:right"> <?=' ('.date('d M Y, h:i A',strtotime($jvalue->OnDate)).')';?></span> 
                    </div> 
                  </div>
                </div>
                </a>
              </div>
		<?php
		}
		}
		else
		{
		?>
		No Job Found In this Search Category
		<?php
		}
		?>
					 
					 <?php
							echo LinkPager::widget([
								'pagination' => $pages,
							]);
							?>
						<div class="spacer-2"></div>