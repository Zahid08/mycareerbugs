<?php
$this->title = 'Candidate Register / Job seekers / Register - mycareerbugs.com';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\AllUser;
use common\models\States;
use kartik\select2\Select2;
use common\models\Industry;
use common\models\Education;
use common\models\Position;
use common\models\NaukariQualData;
use common\models\NaukariSpecialization;
use common\models\LanguagesOpts;
use yii\helpers\ArrayHelper;
use common\models\Cities;
use bupy7\cropbox\CropboxWidget;
use yii\web\JsExpression;
?>
<meta name="keywords" content="My Career Bugs, MCB, Employee Registration, Candidate Register, Register for Job" />
<meta name="description" content="Get Registered with MCB and be instatntly visible to the Top Employers. Keep a tab on HOT JOBS and apply Instantly!" />

<link href="https://mycareerbugs.com/css/jquery.dropdown.css"
      rel="stylesheet">


<style>
    .show_blue{ width: 333px;padding: 24px 24px 10px 20px;position: absolute;z-index: 999; top: 33px;display: none;left: 115px;background: #000;color: #fff;font-size: 11px;font-family: arial;}
    .close_tool{position: absolute;cursor: pointer; right: 15px;top: 10px;color: #fff;font-size: 16px;}

    .show_blue1{ width: 333px;padding: 24px 24px 10px 20px;position: absolute;z-index: 999; top: 33px;display: none;left: 332px;background: #000;color: #fff;font-size: 11px;font-family: arial;}
    .close_tool1{position: absolute;cursor: pointer; right: 15px;top: 10px;color: #fff;font-size: 16px;}



    #qloader{display:none !important;}
    ul.auth-clients .linkedin.auth-link {
        display: none
    }

    .find_a_job, .need_help {
        display: none
    }

    .rgster_media {
        padding: 40px 0 0 0
    }

    #first_sec .page-head{font-size:13px;}

    #role-container .control-label {    width: 8% !important;}
    #role-container .form-control{width:38.5% !important; float:left !important; }
    .select2-results__option{font-size:12px; }

    input[type=checkbox]{margin:4px 0px 0 5px}

    #experience-skills {width:89.4% !important; float:right; font-size:13px;}
    #education-skill-container-0 .control-label{width:8%;}


    #rgstr_inner_page {
        padding: 5px 0 40px 0
    }
    .form-horizontal .form-group {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    .form-horizontal .control-label{text-align:left;}
    #alluser-gender label{    font-size: 12px;}
    #rgstr_inner_page .form-control{width:78%; float:right;}
    #rgstr_inner_page .help-block.help-block-error{font-size: 10px;float: right;width: 78%;margin: 8px 0 0 0;line-height: 3px;}
    #rgstr_inner_page .select2{width:78% !important; float:right;}

    .form-group.field-experience-skills{margin-bottom:0px;}
    #rgstr_inner_page .btn.btn-info.more_add_blck{border:1px solid :#f15b22;cursor: pointer; background: #f15b22;padding: 9px 0px 5px 0px;margin: -3px 0 16px 10px;width: 205px; text-align: center;}
    .hasDatepicker{    padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;}

    .auth-clients li {
        display: block;
        margin: 0 10px !important;}

    .auth-clients  {  margin: 0   !important;}

    #new_year{width:7%;margin: 0; padding: 10px 0 0 0;}

    #YearFrom{width:100% !important}
    #YearTo{width: 100% !important}
    #workexpindiv1{padding:0 20px;}
    #workexpindiv3{padding:0 20px;}
    #workexpindiv2{padding:0 20px;}

</style>

<style>
    #first_sec{padding:0; margin:0 0 15px 0; background:#f2f2f2;}
    #first_sec .page-head  { color:#fff; background:#6d136a; text-align:left;padding:10px 20px;}
    #about .form-control{width: 91%;height: 113px;}
    #about label{ width: 63px;}
    .select2-container--krajee .select2-selection--multiple .select2-selection__rendered{font-size:12px;}
    #rgstr_inner_page .form-control{font-size:12px;}
    .form-group.field-experience-skills{margin-bottom:0px !important;}
    .show_main{display:none;}
    #click1{background:#6d136a; border-radius:5px; color:#fff; font-size:12px; width:180px; text-align:center; display:block; margin:10px 0 10px 0; color:#fff; padding:10px;}
</style>


<div id="wrapper">
    <!-- start main wrapper -->

    <div class="headline_inner" style="display:none">
        <div class="row">
            <div class=" container">
                <!-- start headline section -->
                <h2>Job seekers</h2>

                <div class="clearfix"></div>

            </div>
            <!-- end headline section -->
        </div>
    </div>
    <div class="rgster_media">
        <div class="container">
            <div class="row">
                <div class="omb_login">
                    <div class="row omb_row-sm-offset-3 omb_socialButtons">
                        <div class="co">
                            <h2 style="font-size:18px;text-align:center;     margin: 0 0 22px 0;">Quick Register</h2>
                            <div style="width:108px; margin:0 auto">

                                <?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>

                            </div>
                        </div>

                    </div>
                    <div class="row omb_row-sm-offset-3 omb_loginOr">
                        <div class="col-xs-12 col-sm-6">
                            <hr class="omb_hrOr">
                            <span class="omb_spanOr">or</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="height: 5px"></div>
    <div id="rgstr_inner_page" class="inner_page">
        <div class="container">
            <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal submitedjobsekker','enctype'=>'multipart/form-data']]); ?>
            <div class="row">

                <div class="col-md-12">
                    <div id="first_sec">
                        <h5 class="page-head">Personal Information</h5>
                        <div class="col-md-4">
                            <?php echo $form->field($model, 'Name')->textInput(['maxlength' => 100])?></div>
                        <div class="col-md-4">
                            <?php echo $form->field($model, 'Email')->textInput(['maxlength' => 100])?></div>
                        <div class="col-md-4">
                            <?php echo $form->field($model, 'MobileNo')->textInput(['maxlength' => 10])?> </div>
                        <div class="clear"></div>
                        <div class="col-md-4"><?php echo $form->field($model, 'whatsappno')->textInput(['maxlength' => 10])?> </div>
                        <div class="col-md-4">     <?php echo $form->field($model, 'Age')->dropDownList(array_combine(range(18, 45),range(18, 45)),['prompt' => 'Select Age'])?>
                        </div>
                        <div class="col-md-4">   <?php echo $form->field($model, 'Address')->textInput(['maxlength' => 255])?> </div>
                        <div class="col-md-4">   <?php echo $form->field($model, 'State')->dropDownList(States::getStateList(),['prompt' => 'Select State'])?>    </div>
                        <div class="col-md-4">    <?php echo $form->field($model, 'City')->dropDownList([])?>      </div>

                        <div class="col-md-4">      <?php echo $form->field($model, 'PinCode')->textInput(['maxlength' => 15])?>  </div>

                        <div class="clear"></div>
                    </div>


                    <div id="first_sec">
                        <h5 class="page-head">Education Information</h5>


                        <div class="col-md-4">

                            <?php echo $form->field($education, 'highest_qual_id')->dropDownList(NaukariQualData::getHighestQualificationList(),['prompt' => 'Select Highest Qualification'])?>
                        </div>

                        <div class="col-md-4" id="not-board">
                            <?php echo $form->field($education, 'CourseId')->dropDownList(['prompt' => 'Select Course'])?>
                            <?php //echo $form->field($education, 'specialization_id')->dropDownList(NaukariSpecialization::getSpecializationList(),['prompt' => 'Select Specialization'])?>
                        </div>

                        <div class="col-md-4">
                            <?php echo $form->field($education, 'specialization_id')->dropDownList(NaukariSpecialization::getSpecializationListName(),['prompt' => 'Select Specialization'])?>

                        </div>

                        <div class="clear"></div>

                        <div class="col-md-4" id="not-board-university">
                            <?php echo $form->field($education, 'University')->textInput(['maxlength' => 255])?> 					</div>

                        <div class="col-md-4">
                            <?php echo $form->field($education, 'PassingYear')->dropDownList(array_combine(range(1980, date('Y')), range(1980, date('Y'))),['prompt' => 'Select Passing Year'])?>
                        </div>

                        <div class="col-md-4">
                            <div id="board"> 	<?php echo $form->field($education, 'board_id')->dropDownList([])?> </div>
                        </div>

                        <div class="clear"></div>


                    </div>
                    <div id="first_sec">

                        <h5 class="page-head">Job Preference</h5>

                        <div class="col-md-6" id="work_type_block">
                            <?php echo $form->field($model, 'WorkType')->radioList(AllUser::$worktypelist,[
                                'item' => function($index, $label, $name, $checked, $value) {
                                    $return = '<label class="modal-radio">';
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                    $return .= '<i></i>';
                                    $return .= '<span> ' . ucwords($label) . ' </span>';
                                    $return .= '</label>';
                                    return $return;
                                }
                            ])?>

                        </div>
                        <div class="col-md-6">

                            <?php echo $form->field($model, 'Gender')->radioList(AllUser::$genderlist,[
                                'item' => function($index, $label, $name, $checked, $value) {
                                    $return = '<label class="modal-radio">';
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                    $return .= '<i></i>';
                                    $return .= '<span> ' . ucwords($label) . ' </span>';
                                    $return .= '</label>';
                                    return $return;
                                }
                            ])?>

                        </div>




                        <div class="col-md-6">
                            <label class="control-label">Preferred Location    </label>
                            <?php
                            /*$url = Url::toRoute(['getpreferredlocation']);
                            echo $form->field($model, 'PreferredLocation[]')->widget(Select2::classname(), [
                                   //'data' => ArrayHelper::map(Cities::find()->all(), 'CityId', 'CityName'),
                                   'language' => 'en',
                                   'options' => ['placeholder' => 'Select Max 5 Preferred Locations ...'],
                                   'pluginOptions' => [
                                      // 'allowClear' => true,
                                       'multiple' => true,
                                       'maximumSelectionLength' => 5,
                                       'ajax' => [
                                           'url' => $url ,
                                           'dataType' => 'json' ,
                                           'data' => new JsExpression( 'function(params) { return {q:params.term, page:params.page || 1}; }' )
                                       ] ,
                                       'escapeMarkup' => new JsExpression ( 'function (markup) { return markup; }' ) ,
                                       'templateResult' => new JsExpression ( 'function(product) { console.log(product);return product.CityName; }' ) ,
                                       'templateSelection' => new JsExpression ( 'function (subject) { return subject.CityName; }' ) ,
                                   ],
                               ]); */?>

                            <select class="questions-category form-control"
                                    id="naukari-location" name="AllUser[PreferredLocation][]"
                                    multiple="multiple">

                            </select>

                        </div>



                        <div class="col-md-6">
                            <label class="control-label">Select Language  </label>
                            <?php $languageList = ArrayHelper::map(LanguagesOpts::find()->all(), 'name', 'name');?>
                            <select class="questions-category form-control" name="AllUser[language][]" tabindex="0" aria-hidden="true" id="candidate-language" multiple="multiple">
                                <?php if(!empty($languageList)){
                                    foreach($languageList as $name){?>
                                        <option value="<?php echo $name?>"><?=$name;?></option>
                                    <?php  }
                                }?>

                            </select>

                            <!--<?php echo $form->field($education, 'AllUser[language][]')->dropDownList(LanguagesOpts::getLanguageLists(),['data-id-count' => 0,'prompt' => 'Select Role', 'class' => 'questions-category form-control', 'multiple'=>"multiple", "id" => "candidate-language"])->label('Language')?>-->


                        </div>


                        <br class="clearfix" />

                        <div class="clear"></div>
                    </div>

                    <div id="first_sec">

                        <h5 class="page-head">Interested Role and Skills</h5>

                        <div class="col-md-6">


                            <div class="form-group field-alluser-worktype" style=" margin-bottom:0px">
                                <label class="control-label" id="no_mbl09" for="education-roleid">Category</label>


                                <div class="panel-group" style="margin-bottom: 9px; background:none; position:relative">
                                    <div class="panel panel-default" style="background:none ; box-shadow:none; border:0px">
                                        <div class="panel-heading sty" style="background:#none !important; box-shadow:none; padding-top: 4px;">
                                            <div class="col-md-6 no-pad-123">
                                                <a><h4 class="panel-title" style="font-size:12px;  font-weight: normal; color: #fff;">	<input type="radio" name="AllUser[CollarType]" class="collar-type" value="blue" checked> Blue Collar

                                                        <span  id="blue_clr1" style="cursor:pointer"> <i class="fa fa-question-circle-o" style="font-size:14px"></i></span>

                                                        <span class="checkmark"></span>
                                                    </h4></a>
                                            </div>

                                            <div class="col-md-6 no-pad-123">
                                                <a><h4 class="panel-title"  style="font-size:12px; font-weight: normal; color: #fff;"><input type="radio" name="AllUser[CollarType]" class="collar-type" value="white"  >  White Collar
                                                        <span  id="white_clr1" style="cursor:pointer"> <i class="fa fa-question-circle-o" style="font-size:14px"></i></span>

                                                        <span class="checkmark"></span>
                                                    </h4> </a>
                                            </div>

                                        </div>


                                    </div>


                                    <div class="show_blue">
                                        <span class="close_tool"> <i class="fa fa-times-circle"></i> </span>
                                        <p>A blue-collar worker is a working class person who performs manual labor. Blue-collar work may involve skilled or unskilled manufacturing, mining, sanitation, custodial work, textile manufacturing, power plant operations, farming, commercial fishing, landscaping, pest control, food processing, oil field work, waste disposal, recycling, electrical, plumbing, construction, mechanic, maintenance, warehousing, shipping, technical installation, and many other types of physical work. Blue-collar work often involves something being physically built or maintained.</p>

                                    </div>

                                    <div class="show_blue1">
                                        <span class="close_tool1"> <i class="fa fa-times-circle"></i> </span>
                                        <p>    A white-collar worker is a person who performs professional, managerial, or administrative work. White-collar work may be performed in an office or other administrative setting. White-collar workers includes works related to academia, business management, customer support, market research, finance, human resources, engineering, operations research, marketing, information technology, networking, attorneys, medical professionals, architects, research and development and contracting. </p>

                                    </div>


                                </div>

                            </div>

                        </div>
                        <div id="blue-collar-options">

                            <div class="clear"></div>

                            <div class="col-md-6">

                                <?php echo $form->field($education, 'RoleId[]')->dropDownList(Position::getPositionLists(),['data-id-count' => 0,'prompt' => 'Select Role'])->label('Role')?>

                            </div>

                            <div class="col-md-12">

                                <div id="education-skill-container-0">
                                    <?php echo $form->field($education, 'SkillId[]')->checkboxList([0 => 'Select Role First'],[
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            $return = '<label class="modal-check">';
                                            $return .= '<input type="checkbox" name="' . $name . '" value="' . $value . '" tabindex="3">';
                                            $return .= '<i></i>';
                                            $return .= '<span> ' . ucwords($label) . ' </span>';
                                            $return .= '</label>';
                                            return $return;
                                        }

                                    ])->label('Skills')?>
                                </div>
                            </div>

                            <div class="clear"></div>

                            <div class="col-md-12">
                                <div id="role-container"></div>
                            </div>
                            <div class="clear"></div>
                            <a class="btn btn-info more_add_blck" href="javascript:;" id="add-role" style="float:left; padding:0px 0px 0px 0px; width: 109px !important;
						color: #f15b22;background: none;border: 0px;text-decoration: underline; margin:10px 0 10px 0"> + Add More Role </a>
                            <div class="clear"></div>
                        </div>
                        <div id="white-collar-options" style="display:none;">
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"> Job Category</label>
                                    <select class="questions-category form-control" name="AllUser[Category][0][WhiteCategory]" tabindex="0" aria-hidden="true" id="white_callar_category">
                                        <option value = "">Select Category</option>
                                        <?php if(!empty($whiteCategories)){
                                            foreach($whiteCategories as $categoryId => $categoryName){?>
                                                <option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
                                            <?php  }
                                        }?>

                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"> Roles</label>
                                    <select class="questions-category form-control" name="AllUser[Category][0][WhiteRole][]" id="white_callar_role0" multiple="multiple"></select>
                                </div>
                            </div>

                            <div class="clear"></div>
                            <div id="white-role-container">

                            </div>
                            <div class="clear"></div>

                            <a class="btn btn-info more_add_blck" href="javascript:void(0);" id="add-white-role" style="float:left; padding:0px 0px 0px 0px; width: 109px !important; color: #f15b22; background: none;border: 0px;text-decoration: underline; margin:10px 0 10px 0"> + Add More Role </a>
                            <div class="clear"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label"> Skills</label>
                                    <select class="questions-category form-control" id="white_callar_skills" name="AllUser[WhiteSkills][]" multiple="multiple"></select>

                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>


                    <div class="clear"></div>


                    <hr style="margin:0 0 20px 0">


                    <div class="col-md-6">   <?php echo $form->field($model, 'Password')->passwordInput(['maxlength' => 20])?></div>
                    <div class="col-md-6">    <?php echo $form->field($model, 'ConfirmPassword')->passwordInput(['maxlength' => 20])?>        </div>

                    <div class="clear"></div>

                    <div class="col-md-6">
                        <?php echo $form->field($model, 'photoFile')->widget(CropboxWidget::className(), [
                            'croppedDataAttribute' => 'crop_info',
                        ]);?>
                        <?php //echo $form->field($model, 'photo')->fileInput()?>
                    </div>
                    <div class="col-md-6">      <?php echo $form->field($model, 'cvFile')->fileInput()?> </div>

                    <div class="clear"></div>

                    <div class="col-md-12" id="about">
                        <?php echo $form->field($model, 'AboutYou')->textarea(['rows' => 8,'cols' => 8, 'maxlength' => 500])?>
                    </div>
                </div>






                <div class="col-md-12">
                    <hr style="margin:0 0 20px 0">
                    <div class="clear"></div>
                    <a id="click1" href="javascript:void(0)">+ Add work experience</a>
                    <div class="show_main">
                        <div id="first_sec">
                            <h5 class="page-head">Current Work  Experience</h5>
                            <?php echo Yii::$app->controller->renderPartial('_workexperience-form' , compact('form' , 'experience','whiteCategories'))?>


                        </div>
                        <div class="clear"></div>
                        <div class="form-group" id="main_add_block">
                            <a class="cols-sm-12 btn btn-success more_add_blck" onClick="admoreexp()" id="add-more-experience"> +
                                Add More ( Previous Experience)</a>
                        </div>
                    </div>
                    <hr style="margin:30px 0 20px 0">


                </div>
                <!--Education-->
            </div>
            <div class="clear"></div>
            <div class="col-xs-12">
                <div class="form-group">
                    <label for="confirm" class="col-sm-2 control-label" style="position:relative; z-index:999">
                        <input 	type="checkbox" id="check_box" class="" required> </label>
                    <div class="col-sm-8 info">
                        I agreed to the <a style="color:#f16b22" href="https://mycareerbugs.com/content/terms" title="My Career Bugs Terms and Contditions" target="_blank">Terms and Conditions</a>
                        governing the use of My Career Bugs. I have reviewed the default Mailer &
                        Communications settings. Register Now</div>
                </div>
                <br>
                <div class="form-group ">
                    <?= Html::submitButton('Register', ['class' => 'btn btn-primary btn-lg btn-block login-button']) ?>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="border"></div>
</div>
<link href="/assets/cc11e89d/themes/smoothness/jquery-ui.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://mycareerbugs.com/js/mock.js"></script>
<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script> -->
<style>
    .omb_socialButtons #w1 {
        width: 97px;
    }

    .more_add_blck {
        color: #fff;
        cursor: pointer;
        background: #f15b22;
        padding: 10px 10px;
        margin: 10px 0 0 10px;
        width: 232px;
        text-align: center;
    }
</style>


<script>
    $(document).ready(function(){
        $("#blue_clr1").click(function(){
            $(".show_blue").toggle();
        });

        $(".close_tool").click(function(){
            $(".show_blue").hide();
        });

        $("#white_clr1").click(function(){
            $(".show_blue1").toggle();
        });

        $(".close_tool1").click(function(){
            $(".show_blue1").hide();
        });


    });
</script>


<script>
    var board = new Array("4", "5", "6");
    var baseUrl = "<?php echo Url::toRoute(['/'])?>";

    function getRoleListHtml(count) {
        var options = $('#education-roleid').html();
        var html = '<div class="form-group field-education-roleid">'+
            '<label class="control-label" for="education-roleid">Role</label>'+
            '<select id="education-roleid-'+count+'" class="form-control" name="Education[RoleId][]" data-id-count = "'+count+'">'+
            options+
            '</select>'+
            '<p class="help-block help-block-error"></p>'+
            '</div>'+
            '<div id="education-skill-container-'+count+'">';
        return html;
    }
    var count = 0;

    $('#click1').click(function(){
        $(".show_main").toggle(200);
    });



    $(document).on('click' , '#add-role' , function(e){
        console.log('add role');
        count++;
        console.log('count'+count)
        var html = getRoleListHtml(count);
        $('#role-container').append(html);
    });




    $(document).ready( function(e){
        var eduqualid = $('#education-highest_qual_id').val();
        getCourseList(eduqualid);
        var stateid = $('#alluser-state').val();
        getCity(stateid);
        var roleId = $('#education-roleid').val();
        var dataidcount = $('#education-roleid').attr('data-id-count');
        console.log('onload dataidcount = '+dataidcount);
        getSkill(roleId,dataidcount);

        getRequiredEducationField();

    });
    $(document).on('change', '#education-highest_qual_id', function(e){
        e.preventDefault();
        var eduqualid = $(this).val();
        getCourseList(eduqualid);
    });
    $(document).on('change', '#alluser-state', function(e){
        e.preventDefault();
        var stateid = $(this).val();
        getCity(stateid);
    });
    $(document).on('change' , '#education-highest_qual_id' , function(e){
        getRequiredEducationField();
    });
    $(document).on('change' , '[id^=education-roleid]', function(e){
        e.preventDefault();
        var roleId = $(this).val();
        var dataidcount = $(this).attr('data-id-count');
        getSkill(roleId,dataidcount);
    });

    function getWhiteCategoryList(count) {
        var options = $('#white_callar_category').html();
        var html = '<div class="extra-white-role"><div class="col-md-6"><div class="form-group">'+
            '<label class="control-label" for="white_callar_category'+count+'">Job Category</label>'+
            '<select id="white_callar_category'+count+'" class="form-control white_callar_category" name="AllUser[Category]['+count+'][WhiteCategory]" data-id-count = "'+count+'">'+
            options+
            '</select>'+
            '<p class="help-block help-block-error"></p>'+
            '</div></div><div class="col-md-6"><div class="form-group"><label class="control-label"> Roles</label> <select class="questions-category form-control" name="AllUser[Category]['+count+'][WhiteRole][]" id="white_callar_role'+count+'" multiple="multiple"></select></div> </div></div><div class="clearfix"></div>';
        return html;
    }

    $(document).ready(function(){
        /*Add Collar Type*/
        $('#education-roleid').prop('required',true);
        $('#alluser-confirmpassword').prop('required',true);
        $('#alluser-password').prop('required',true);
        $(document).on('click', '.collar-type', function(){
            var value = $(this).val();
            if(value == "blue"){
                $('#blue-collar-options').show(0);
                $('#white-collar-options').hide(0);

                $('.exp-white-collar').hide(0);
                $('.exp-blue-collar').show(0);
                $('#education-roleid').prop('required',true);
                $('#white_callar_category').prop('required',false);
            }else if(value == 'white'){
                $('#blue-collar-options').hide(0);
                $('#white-collar-options').show(0);

                $('.exp-white-collar').show(0);
                $('.exp-blue-collar').hide(0);

                $('#white_callar_category').prop('required',true);
                $('#education-roleid').prop('required',false);
            }
        });

        countT = 0;
        $(document).on('click' , '#add-white-role' , function(e){
            countT++;
            var html = getWhiteCategoryList(countT);
            $('#white-role-container').append(html);
            $("#white_callar_role"+countT).select2({
                placeholder: "Select Role"
            });

        });


        $("#white_callar_role0").select2({
            placeholder: "Select Role"
        });

        $(document).on('change', '#white_callar_category', function(){
            var value = $(this).val();
            if(value != ""){
                $.ajax({
                    dataType : "json",
                    type : 'GET',
                    url : '<?=Url::toRoute(['getwhiteroles']);?>',
                    data : {
                        category_id : value
                    },
                    success : function(data) {
                        $('#white_callar_role0').html("");
                        if(data != ""){
                            $.each(data, function(key, val) {
                                var option = $('<option />');
                                option.attr('value', key).text(val);
                                $('#white_callar_role0').append(option);
                            });
                        }
                    }
                });
            }

        });

        $(document).on('change', '.white_callar_category', function(){
            var value = $(this).val();
            var countC = $(this).attr('data-id-count');
            if(value != ""){
                $.ajax({
                    dataType : "json",
                    type : 'GET',
                    url : '<?=Url::toRoute(['getwhiteroles']);?>',
                    data : {
                        category_id : value
                    },
                    success : function(data) {
                        $('#white_callar_role'+countC).html("");
                        if(data != ""){
                            $.each(data, function(key, val) {
                                var option = $('<option />');
                                option.attr('value', key).text(val);
                                $('#white_callar_role'+countC).append(option);
                            });
                        }
                    }
                });
            }

        });

        $("#white_callar_skills").select2({
            maximumSelectionLength: 5,
            placeholder: "Select Skills",
            ajax: {
                url: '<?=Url::toRoute(['getwhiteskills']);?>',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                        q: params.term,
                        page: 1
                    }
                },
                processResults: function(data){
                    return {
                        results: $.map(data.results, function (item) {

                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $("#naukari-location").select2({
            maximumSelectionLength: 5,
            placeholder: "Select Max 5 Preferred Locations ...",
            ajax: {
                url: '<?=Url::toRoute(['getpreferredlocation']);?>',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                        q: params.term,
                        page: 1
                    }
                },
                processResults: function(data){
                    return {
                        results: $.map(data.results, function (item) {

                            return {
                                text: item.CityName,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $("#candidate-language").select2({
            placeholder: "Select Languages",
            maximumSelectionLength: 5,
            placeholder: "Select Max 5 Languages..."
        });


        $("body").on("submit", "#w1", function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });


    });

    function getRequiredEducationField(){
        var value = $('#education-highest_qual_id').val();
        if($.inArray(value, board)  !== -1){
            $('#not-board').hide();
            $('#not-board-university').hide();
            $('#board').show();
        }else{
            $('#not-board').show();
            $('#not-board-university').show();
            $('#board').hide();
        }
    }
    function getCourseList(eduqualid){
        if(eduqualid !=""){
            $.get(baseUrl+'site/getcourselist?id='+eduqualid, function(res){
                $('#education-courseid').empty();
                $('#education-courseid').append(res.result);
                $('#education-board_id').empty();
                $('#education-board_id').append(res.result);
            });
        }

    }
    function getCity(stateid){
        $.get(baseUrl+'site/getcity?stateId='+stateid, function(res){
            $('#alluser-city').empty();
            $('#alluser-city').append(res.result);
        });
    }
    function getSkill(roleId,dataidcount){
        $.get(baseUrl+'site/geteducationskills?id='+roleId, function(res){
            console.log('count id = '+dataidcount);
            $('#education-skill-container-'+dataidcount).empty();
            $('#education-skill-container-'+dataidcount).append(res.result);
        });
    }
    $(function(){
        $('#s2-togall-alluser-preferredlocation').hide();
    });


    $(document).on('click','button.btn.btn-primary.btn-lg.btn-block.login-button',function(){
        var confirmedPassword=$('input#alluser-confirmpassword').val();
        var password=$('input#alluser-password').val();

        if (confirmedPassword=='') {
            $('.form-group.field-alluser-confirmpassword').addClass('has-error');
            $('.form-group.field-alluser-confirmpassword p.help-block.help-block-error').html('Confirmed password can not blank');
            return false;
        }else if (password!=confirmedPassword) {
            $('.form-group.field-alluser-confirmpassword').addClass('has-error');
            $('.form-group.field-alluser-confirmpassword p.help-block.help-block-error').html('Password not match');
            return false;
        }else{
            $('.form-group.field-alluser-confirmpassword p.help-block.help-block-error').html('');
            return true;
        }
    });


</script>