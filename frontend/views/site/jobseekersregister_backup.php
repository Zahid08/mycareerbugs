<?php
$this->title = 'Candidate Register / Job seekers / Register - mycareerbugs.com';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use bupy7\cropbox\CropboxWidget;

$salary_range = array(
    '0-1.5',
    '1.5-3',
    '3-6',
    '6-10',
    '10-15',
    '15-25',
    'Above 25'
);
?>
<meta name="keywords"
	content="Mycareer bugs, MCB, Employee Registration, Candidate Register, Register for Job" />
<meta name="description"
	content="Get Registered with MCB and be instatntly visible to the Top Employers. Keep a tab on HOT JOBS and apply Instantly!" />
<link href="https://mycareerbugs.com/css/jquery.dropdown.css"
	rel="stylesheet">

<style>
.find_a_job, .need_help {
	display: none
}

.rgster_media {
	padding: 40px 0 0 0
}

#rgstr_inner_page {
	padding: 5px 0 40px 0
}
</style>
<div id="wrapper">
	<!-- start main wrapper -->

	<div class="headline_inner">
		<div class="row">
			<div class=" container">
				<!-- start headline section -->
				<h2>Job seekers</h2>
				<div class="clearfix"></div>

			</div>
			<!-- end headline section -->
		</div>
	</div>






	<div class="rgster_media" style="display: none">
		<div class="container">
			<div class="row">
				<div class="omb_login">
					<div class="row omb_row-sm-offset-3 omb_socialButtons">
						<div class="col-xs-4 col-sm-3">
								<?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>
							</div>
						<div class="col-xs-4 col-sm-3">
							<a href="#" class="btn btn-lg btn-block omb_btn-google"> <i
								class="fa fa-google-plus visible-xs"></i> <span
								class="hidden-xs">Google+</span>
							</a>
						</div>
					</div>
					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<hr class="omb_hrOr">
							<span class="omb_spanOr">or</span>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>





	<div style="height: 25px"></div>

	<div id="rgstr_inner_page" class="inner_page">
		<div class="container">
		 

					<?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>
					<div class="rowsw">
				<div class="col-xs-12 col-sm-6">
					<h5 class="page-head">Personal</h5>

					<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">Your Name</label>
							<?php
    if (isset(Yii::$app->session['SocialName']) && Yii::$app->session['SocialName'] != '') {
        $name = 'value="' . Yii::$app->session['SocialName'] . '" readonly';
    } else {
        $name = '';
    }
    if (isset(Yii::$app->session['SocialEmail']) && Yii::$app->session['SocialEmail'] != '') {
        $email = 'value="' . Yii::$app->session['SocialEmail'] . '" readonly';
    } else {
        $email = '';
    }
    if (isset(Yii::$app->session['SocialGender']) && Yii::$app->session['SocialGender'] != '') {
        $gender = Yii::$app->session['SocialGender'];
    } else {
        $gender = '';
    }
    ?>
							<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" <?=$name;?> name="AllUser[Name]" id="name"
									placeholder="Enter your Name" required />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Your Email</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope fa"
									aria-hidden="true"></i></span> <input type="email"
									class="form-control" <?=$email;?> name="AllUser[Email]"
									id="email" placeholder="Enter your Email" required />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Mobile No</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone-square fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[MobileNo]" id="MobileNo"
									placeholder="Enter your MobileNo"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10" required />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> WhatsApp No</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone-square fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[WhatsAppNo]" id="WhatsAppNo"
									placeholder="Enter your WhatsApp No"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10" />
							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Gender</label>
						<div class="cols-sm-10">
							<div class="input-group">

								<input type="radio" name="AllUser[Gender]"
									<?php if(strtolower($gender)=='male'){ echo "checked"; } ?>
									value="Male" required /> &nbsp; Male <input type="radio"
									name="AllUser[Gender]" value="Female"
									<?php if(strtolower($gender)=='female'){ echo "checked"; } ?> />
								&nbsp; Female
							</div>
						</div>
					</div>



					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Age</label>
						<div class="cols-sm-10">
							<div class="input-group full">
								<select style="width: 100%"
									; class="questions-category form-control" name="AllUser[Age]"
									required>
									<?php for($age=14; $age <= 45; $age++){ ?>
									
										<option value="<?php echo $age ?>"><?php echo $age?></option>
									<?php } ?>
									</select>
							</div>
						</div>
					</div>



					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Address</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-location-arrow"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[Address]" id="address"
									placeholder="Enter your Address" required />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Upload Photo
						</label>
						<div class="cols-sm-10">
							<div class="input-grousp">
								<div class="input-grohup" style="font-size: 14px;">
									<?php 
									$whitelist = array(
									    '127.0.0.1',
									    '::1'
									);
									
									if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
									    //echo  $form->field($docmodel, 'Doc')->widget(CropboxWidget::className(), ['attributeCropInfo' => 'crop_info',])->label(false); 
									}?>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group" style="display: none;">
						<label for="confirm" class="cols-sm-2 control-label">Country </label>
						<div class="cols-sm-12">
							<div class="input-group full">
								<select class="questions-category countries form-control"
									tabindex="0" aria-hidden="true" id="countryId"
									name="AllUser[Country]" required>
									<option value="">Select Country</option>
									<option value="India" countryid="101" selected="selected">India</option>
								</select>
							</div>
						</div>
					</div>



					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Select State</label>
						<div class="cols-sm-12">
							<div class="input-group full">
								<select class="questions-category states form-control"
									tabindex="0" aria-hidden="true" id="stateId"
									name="AllUser[State]" required>
									<option value="">Select State</option>
								</select>

							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="cityId" class="cols-sm-2 control-label"> City </label>

						<div class="input-group full">
							<select class="questions-category cities form-control"
								tabindex="0" aria-hidden="true" id="cityId" name="AllUser[City]"
								required>
								<option value="">Select City</option>
							</select>

						</div>
					</div>
					<div class="form-group">
						<label for="LanguageId" class="cols-sm-2 control-label"> Language </label>
						<div class="input-group full">
							<select multiple="multiple"
								class="questions-category form-control" tabindex="0"
								aria-hidden="true" id="LanguageId" name="AllUser[Language][]"
								required>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Pincode </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-thumb-tack"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[PinCode]" id="confirm"
									placeholder="Pincode" maxlength="6" required
									onkeypress="return numbersonly(event)" autocomplete="off">
							</div>
						</div>
					</div>



					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg"
									aria-hidden="true"></i></span> <input type="password"
									class="form-control" name="AllUser[Password]" id="password"
									placeholder="Enter your Password" required autocomplete="off" />
							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Confirm
							Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg"
									aria-hidden="true"></i></span> <input type="password"
									class="form-control" name="confirm" id="confirm"
									placeholder="Confirm your Password" required
									onblur="if($(this).val()!=$('#password').val()){alert('Password  and Confirm password Must Be Same');$(this).val('');setTimeout(function() {  $('#password').focus();return false; }, 10);}" />
							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Upload CV </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<div class="input-group" style="font-size: 14px;">
									<input type="file" name="AllUser[CVId]"
										accept=".doc, .docx,.rtf,.pdf" id="cvid">
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">About You /
							Objective</label>
						<div class="cols-sm-10">
							<div class="input-group full">
								<textarea name="AllUser[AboutYou]" class="form-control textarea"
									maxlength="250"
									onkeyup="$(this).next().html($(this).val().length +'- 250 character'); if($(this).val().length>250){$(this).next().css('color','red');}else{$(this).next().css('color','#333');}"></textarea>
								<p class="help-block">250 Characters</p>
							</div>
						</div>
					</div>

				</div>



				<!--Education-->
				<div class="col-xs-12 col-sm-6">
					<h5 class="page-head">Education</h5>

					<div class="form-group" style="display: none;">
						<label for="name" class="cols-sm-2 control-label">Highest
							Qualification</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[HighestQualification]"
									id="hq" placeholder="Highest Qualification" />
							</div>
						</div>
					</div>
						<?php

    $university = (isset(Yii::$app->session['University'])) ? Yii::$app->session['University'] : '';
    $universityid = (isset(Yii::$app->session['Universityid'])) ? Yii::$app->session['Universityid'] : '';
    $coursecampus = (isset(Yii::$app->session['Course'])) ? Yii::$app->session['Course'] : '';
    ?>
						<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Highest
							Qualification </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-files-o"
									aria-hidden="true"></i></span> <select
									class="questions-category form-control" id="empcourse1"
									name="AllUser[HighestQualId]" required>
									<option value="">Select Higest Qualification</option>
										<?php
        foreach ($NaukariQualData as $key => $value) {
            ?>
										<option value="<?=$value->id;?>"><?=$value->name;?></option>
										<?php
        }
        ?>
								       </select> <span id="otc"> </span>

							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Course </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <select
									class="questions-category form-control" id="naukari-course"
									name="AllUser[CourseId]" required>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Specialization
						</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span>
								<!-- <input type="text" class="form-control" name="AllUser[Specialization]" id="naukari-specialization"  placeholder=" Specialization" /> -->

								<select class="questions-category form-control"
									id="naukari-specialization" name="AllUser[Specialization]">
								</select>
							</div>
						</div>
					</div>
					<div class="form-group hide">
						<label for="email" class="cols-sm-2 control-label"> Other </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[otherSpecialization]"
									id="naukari-other-specialization"
									placeholder=" Other Specialization" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Board </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <select
									class="questions-category form-control" id="naukari-board"
									name="AllUser[BoardId]">
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">
							University/College </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[University]"
									id="university_college" placeholder="University/College" /> </select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Passing Year</label>
						<div class="cols-sm-8">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-calendar"
									aria-hidden="true"></i></span> <select class="form-control"
									name="AllUser[DurationTo]" required>
									<option value="">Select Year</option>
										<?php
        for ($y = 1980; $y <= date('Y'); $y ++) {
            ?>
										<option value="<?=$y;?>"><?=$y;?></option>
										<?php
        }
        ?>
									</select>

							</div>
							<!--<div class="input-group" style="width: 33%;float: left;">
									<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
									<input type="text" class="form-control educationdatepicker" name="AllUser[DurationFrom]" readonly style="cursor: pointer;background: #fff;" id="dfrom" autocomplete="off" placeholder="From" required maxlength="4" onkeypress="return numbersonly(event)" />
								</div>
								
								<div class="input-group" style="width: 33%;float: left;margin-left: 10%;">
									<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
									<input type="text" class="form-control educationdatepicker" name="AllUser[DurationTo]" readonly style="cursor: pointer;background: #fff;" id="dto" autocomplete="off"  placeholder="To" required maxlength="4" onkeypress="return numbersonly(event)" onchange="if(new Date($(this).val()).getTime()<=new Date($('#dfrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}"/>
								</div>-->
						</div>

					</div>
					
					<!-- Skill Role Id -->
					
					<div class="form-group role-container">
						<label for="confirm" class="cols-sm-2 control-label">Role </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg"
									aria-hidden="true"></i></span> <select class="form-control"
									required="required" id="roleid" name="AllUser[SkillRoleId][]">
									<option value="">Select Role</option>
										<?php
        foreach ($position as $key => $value) {
            ?>
										<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
										<?php
        }
        ?> 
								   </select>
							</div>

						</div>
					</div>
					<div class="form-group" id="skilldatalist"></div>
					<div class="form-group addrole-continer">
						<a href="javascript:;" onclick="admorerole();">+ Add More Role</a>
					</div>
					<h5 class="page-head">Additional Information</h5>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Working Time </label>
						<div class="cols-sm-10">
							<div class="input-group">

								<input type="radio" name="AllUser[WorkType]"
									<?php if(strtolower($worktype)=='part time'){ echo "checked"; } ?>
									value="Part Time" required /> &nbsp; Part Time <input
									type="radio" name="AllUser[WorkType]" value="Full Time"
									<?php if(strtolower($worktype)=='Full Time'){ echo "checked"; } ?> />
								&nbsp; Full Time <input type="radio" name="AllUser[WorkType]"
									<?php if(strtolower($worktype)=='Work From Home'){ echo "checked"; } ?>
									value="Work From Home" /> &nbsp; Work From Home <input
									type="radio" name="AllUser[WorkType]" value="Internships"
									<?php if(strtolower($worktype)=='Internships'){ echo "checked"; } ?> />
								&nbsp; Internships
							</div>
						</div>
					</div>





					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label"> Prefered 5
							Job Location </label>
						<div class="cols-sm-10">

							<div class="input-group" style="width: 79%">
								<div class="dropdown-sin-1">
								      	<?php //echo '<pre>'; print_r($cities); die;?>
									<select style="display: none" multiple placeholder="Select"
										name="AllUser[PreferredLocation][]" multiple="multiple"
										maxlength="5" maxlength="5">
										<?php foreach($cities as $city){ ?>
									
										<option value="<?php echo $city->CityId ?>"><?php echo $city->CityName?></option>
									<?php } ?>
									
									</select>
								</div>
							</div>
						</div>
					</div>


















					<h5 class="page-head">Work Experience (Optional)</h5>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Year </label>
						<div class="cols-sm-5">
							<div class="input-group" id="responsive_full"
								style="width: 33%; float: left;">
								<span class="input-group-addon"><i class="fa fa-calendar"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control date" name="AllUser[YearFrom][]"
									id="YearFrom" readonly
									style="cursor: pointer; background: #fff;" autocomplete="off"
									placeholder="From" onkeypress="return numbersonly(event)" />
							</div>
							<div class="input-group" id="responsive_full1"
								style="width: 33%; float: left; margin-left: 10%;">
								<span class="input-group-addon"><i class="fa fa-calendar"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control date" name="AllUser[YearTo][]" id="YearTo"
									readonly style="cursor: pointer; background: #fff;"
									autocomplete="off" placeholder="To"
									onkeypress="return numbersonly(event);"
									onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />
							</div>
						</div>

					</div>
					<div id="workexpdiv">
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"> Company name
							</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-briefcase"
										aria-hidden="true"></i></span> <input type="text"
										class="form-control" name="AllUser[CompanyName][]"
										id="CompanyName" placeholder="Company name" />
								</div>
							</div>
						</div>


						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"> Position </label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-briefcase"
										aria-hidden="true"></i></span> <input type="text"
										class="form-control" name="Experience[PositionName][]"
										id="position" placeholder="Position" />
								</div>
							</div>
						</div>



						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label"> Industry </label>
							<div class="cols-sm-10">
								<div class="input-group full">
									<select name="AllUser[CIndustryId][]"
										class="form-control bfh-states">

										<option selected="selected" value="">- Select an Industry -</option>
										<?php foreach($industry as $key=>$value){?>
										<option value="<?php echo $key;?>"><?=$value;?></option>
										<?php } ?>
								</select>

								</div>
							</div>
						</div>
						<!--  Position Id -->
						<div class="form-group">
							<label for="emppos" class="cols-sm-2 control-label"> Role </label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-briefcase"
										aria-hidden="true"></i></span> <select
										class="questions-category form-control exp-role"
										name="AllUser[PositionId][]" id="emppos">
										<option value="">Select Role</option>
										<?php
        foreach ($position as $key => $value) {
            ?>
										<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
										<?php
        }
        ?>
										</select> <span id="pos"></span>
								</div>
							</div>
							<div class="form-group skilldatalist" id="roleskilllist"></div>
						</div>
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"> Experience </label>
							<div class="cols-sm-10">
								<div class="input-group full">
									<select id="form-filter-location" name="AllUser[Experience][]"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control" tabindex="0"
										aria-hidden="true">
										<option value="">Experience</option>
										<option value="Fresher">Fresher</option>
										<option value="0-1">Below 1 Year</option>
					    <?php
        for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
            $frn = $fr + 1;
            ?>
                                            <option
											value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                           <?php
        }
        ?>
                              </select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"> Salary </label>
							<div class="cols-sm-10">
								<div class="input-group full">
									<select id="form-filter-location" name="AllUser[Salary][]"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control" tabindex="0"
										aria-hidden="true">
										<option value="">Salary</option>
									<?php foreach ($salary_range as $key => $value) { ?>
                                		<option value="<?=$value?>"><?=$value?> lakhs</option>
									<?php } ?>
                              		</select>
								</div>
							</div>
						</div>


					</div>
					<div id="moreskill"></div>
					<style>
#main_add_blck label {
	color: #fff;
	cursor: pointer;
	background: #f15b22;
	padding: 10px 10px;
	margin: 10px 0 0 10px;
	width: 232px;
	text-align: center;
}
</style>

					<div class="form-group" id="main_add_blck">
						<label class="cols-sm-12 control-label" onclick="admoreexp();">+
							Add More ( Previous Experience)</label>
					</div>




				</div>

				<!--Education-->


			</div>


			<div class="clear"></div>

			<div class="col-xs-12">
				<div class="form-group">
					<label for="confirm" class="cols-sm-2 control-label"><input
						type="checkbox" id="check_box" class="" required> </label>
					<div class="cols-sm-10 info">I agreed to the Terms and Conditions
						governing the use of MCB. I have reviewed the default Mailer &
						Communications settings. Register Now</div>
				</div>





				<div class="form-group ">
							<?= Html::submitButton('Register', ['class' => 'btn btn-primary btn-lg btn-block login-button']) ?>
						</div>
						 
					<?php ActiveForm::end(); ?>
					
					
					
					</div>



		</div>




	</div>




	<div class="border"></div>


</div>



<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://mycareerbugs.com/js/mock.js"></script>

<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script>





<script>
    
    var Random = Mock.Random;
     var json1 = Mock.mock({
      "data|10-50": [{
        name: function () {
          return Random.name(true)
        },
        "id|+1": 1,
        "disabled|1-2": true,
        groupName: 'Group Name',
        "groupId|1-4": 1,
        "selected": false
      }]
    }); 
    
    $('.dropdown-sin-1').dropdown({
      readOnly: true,
	  limitCount:5,
      input: '<input type="text" maxLength="5" placeholder="Search">'
    }); 
  </script>




<script type="text/javascript">
		var p=0;
		function admoreexp() {
				p++;
				
				var result = $('#workexpdiv').html();
				//var result = $('#workexpdiv').find('#roleskilllist').empty().html();
				
				var date='<div class="form-group"><label for="email" class="cols-sm-2 control-label">  Year    </label><div class="cols-sm-5"><div class="input-group" id="responsive_full" style="width: 33%;float: left;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="AllUser[YearFrom][]" readonly style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="From" maxlength="4" onkeypress="return numbersonly(event)"/></div><div class="input-group" id="responsive_full1" style="width: 33%;float: left;margin-left: 10%;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="AllUser[YearTo][]" readonly style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="To"  maxlength="4" onkeypress="return numbersonly(event);" onchange="if(new Date($(this).val()).getTime()<=new Date($(&#39;YearFrom&#39;).val()).getTime()){alert(&#39;Wrong Year Duration&#39;);$(this).val(&#39; &#39;);}"/></div></div></div>';
            	$('#moreskill').append('<div id="workexpindiv'+p+'" ><span style="color:red;cursor:pointer;" onclick=del('+p+');>X</span>'+date +' '+result+'</div>');
				$(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true});
		 		$('#workexpindiv'+p).find('#roleskilllist').empty();
		
				console.log(result);
        }
		function del(id) {
            $('#workexpindiv'+id).remove();
        }

        $(document).on('change', '#empcourse1', function() {
        if($(this).val()==""){
        	$("#naukari-course option").remove();
            //$("#naukari-specialization option").remove();
            $("#naukari-board option").remove();
        	return true;
        }

        if($(this).val()=="6"){
        	$("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
            return true;
        }
        if($(this).val()=="4" || $(this).val()=="5"){
        	$("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#university_college").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').removeClass('hide');
            var elm = 'naukari-board';
        }else{
        	$("#naukari-course").parents('.form-group').removeClass('hide');
            $("#naukari-specialization").parents('.form-group').removeClass('hide');
            $("#university_college").parents('.form-group').removeClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
            var elm = 'naukari-course';
        }	
        var id = $(this).val();
  			$.ajax({
                type: "POST",
                url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
                data: {'id': $(this).val()},
                 success : function(response) {
            		var data = JSON.parse(response);
            		$("#naukari-course option").remove();
            		//$("#naukari-specialization option").remove();
            		$("#naukari-board option").remove();
            		$.each(data, function( index, value ) {
  						$("#"+elm).append('<option value="'+index+'">'+value+'</option>');
					});
        		}, 

            })
		});

		/*$(document).on('change', '#naukari-course', function() {
  			$.ajax({
                type: "POST",
                url: 'Yii::$app->urlManager->baseUrl."/getcourse"?>',
                data: {'id': $(this).val()},
                 success : function(response) {
            		var data = JSON.parse(response);
            		$("#naukari-specialization option").remove();
            		$.each(data, function( index, value ) {
  						$("#naukari-specialization").append('<option value="'+index+'">'+value+'</option>');
					});
        		}, 

            })
		});*/
		$(document).on('change', '#naukari-specialization', function() {
			if($(this).val()=="Other"){
				$("#naukari-other-specialization").parents('.form-group').removeClass('hide');
			}else{
				$("#naukari-other-specialization").parents('.form-group').addClass('hide');
			}
		});
		var data = [{id: '',text: 'Select Specialization'}];
		var Ldata = [{id: '',text: 'Select Language'}];

		<?php foreach ($naukari_specialization as $key => $value) { ?>
				var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
				data.push(d);
		<?php }  ?>
		<?php foreach ($language_opts as $key1 => $value1) { ?>
				var ld = {id: '<?=$value1->name?>',text: '<?=$value1->name?>'};
				Ldata.push(ld);
		<?php }  ?>
	$("#naukari-specialization").select2({
  		data: data
	});
	$("#LanguageId").select2({
  		data: Ldata,
  		maximumSelectionLength: 5
	});
	$(document).on('change' , '.exp-role' , function(e){
		var roleid = $(this).val();
		var loderimg = '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group"><img src="'+mainurl+'img/ajax-loader.gif"></div></div>';
		var e = $(this);
		$(this).parents('.form-group').find('.skilldatalist').html(loderimg);
		 $.getJSON( "/site/skilldata", {roleid:roleid}, function( data ) {
			 var html = '';
			 if(data.status == 'OK') { 
			  html += '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group">';
			  $.each( data, function( key, val ) {
				 html += '<span><input type="checkbox" value="'+val.value+'" name="AllUser[SkillsId][]" />'+val.value+'&nbsp;</span>'; 
			  });
			  html +='</div></div>';
    		 }else{
    			 html += 'No Record Found';
        	 }
			 e.parents('.form-group').find('.skilldatalist').html(html);
		});
	});
// 	$(".exp-role").change(function(){
		
		
// 	});  
	$(document).on('change', '.append_role select', function() {
		var roleid = $(this).val();
		var loderimg = '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group"><img src="'+mainurl+'img/ajax-loader.gif"></div></div>';
		var e = $(this);
		$(this).parents('.append_role').find('.skilldatalist').html(loderimg);
		 $.getJSON( "/site/skilldata", {roleid:roleid}, function( data ) {
			
			 var html = '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group">';
			  $.each( data, function( key, val ) {
				 html += '<span><input type="checkbox" value="'+val.id+'" name="AllUser[SkillId][]" />'+val.value+'&nbsp;</span>'; 
			  });
			  html +='</div></div>';
			 e.parents('.append_role').find('.skilldatalist').html(html);
		});
		
	});

	function admorerole(){
		var div = $('.role-container').html();
		var div_skill= '<div class="form-group skilldatalist"></div>';
		var oDiv = document.createElement('div');
      	oDiv.setAttribute("class", "append_role");
      	oDiv.innerHTML=(div+div_skill);
		$('.addrole-continer').before(oDiv);
		//$('.addrole-continer').prepend(div_skill);
	}
</script>
<style>
.omb_socialButtons #w1 {
	width: 97px;
}

ul.auth-clients .linkedin.auth-link {
	display: none
}
</style>