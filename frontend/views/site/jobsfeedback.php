<?php
$this->title = 'Jobs Feedback';
//var_dump($model);
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl();
$url='/backend/web/';
if(isset(Yii::$app->session['Employeeid']))
{
		$link='editprofile';
}
else
{
		$link='login';
}
?>
<div id="wrapper"><!-- start main wrapper -->
	 
	 
		 
		<div class="inner_page_extra_pages">
			<div class="container">
		
		<style>
		.inner_page_extra_pages{padding:20px 0 40px 0}
		  .tab_block h1{color:#f16b22; font-size:22px}
		  .tab_block h3{  font-size:18px;}
		  .tab-content{padding:10px}
		  .tab_block p{font-size:14px; line-height:25px; margin-left:20px }
		</style>
		
<div class="tab_block">
   
<?php
if($type=='no')
{
    ?>
    <p>
        <b>Dear JobSeeker, Thank you for your valuable feedback.</b><br/>
        <b>In order to get relevant job alerts, here are a few things you can do:</b><br/>
        
        
Keep you Current Designation, Years of Experience, Functional Area, Industry, Location Preferences, Salary and Key Skills up to date. <br/>
<a href="<?= Url::toRoute(['site/'.$link])?>" target="_blank"> 
<input type="button" value="Update Profile" class="btn btn-primary" />
</a>
<br/>
OR
<br/>
You can create your own Job Alerts. You can specify the details of the kind of jobs you are looking for and we will send you Job Alerts matching your preferences. <br/>
<a href="<?= Url::toRoute(['site/index#jobalertbox'])?>" target="_blank">
<input type="button" value="Create Job Alert" class="btn btn-primary" />
</a>
    </p>
    <?php
}
else
{
?>
<p>
        <b>Dear JobSeeker, Thank you for your valuable feedback.</b><br/>
        
        <b>You may now:</b> <br/>
        <ul>
            <a href="<?= Url::toRoute(['site/jobsearch'])?>" target="_blank"><li>Search & Apply to jobs</li></a>
           <a href="<?= Url::toRoute(['site/'.$link])?>" target="_blank"> <li>Update your profile</li></a>
        </ul>
</p>
<?php
}
?>

</div>
  
           </div>  
      </div>
 <div class="border"></div>