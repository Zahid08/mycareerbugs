<?php
$this->title = 'Profile';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='https://www.mycareerbugs.com/backend/web/';
use common\models\AllUser;
$alluser=new AllUser();
?>
<div id="wrapper"><!-- start main wrapper -->

		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
		         <div class="col-md-10 col-sm-10 col-xs-12"> 
                            <div class="job-short-detail">
                                <div class="heading-inner">
										 <h3><?=$profile->Name;?></h3>
                                    <p class="title">Profile detail</p>
									
									<p>
									</div>
                                </div>
								
								
								
                                <dl>
                                     <dt>About Me:</dt>
									<dd><?=$profile->AboutYou;?></dd>
                                    <dt>Phone:</dt>
                                    <dd>+<?=$profile->MobileNo;?> </dd>

                                    <dt>Email:</dt>
                                    <dd><?=$profile->Email;?> </dd>
				    
								    <dt>Gender:</dt>
                                    <dd><?=$profile->Gender;?> </dd>
 
                                    <dt>Address:</dt>
                                    <dd><?=$profile->Address;?></dd>

                                    <dt>City:</dt>
                                    <dd><?=$profile->City;?></dd>

                                    <dt>State:</dt>
                                    <dd><?=$profile->State;?></dd>

                                    <dt>Country:</dt>
                                    <dd><?=$profile->Country;?></dd>
									
									<dt>Pincode:</dt>
                                    <dd><?=$profile->PinCode;?></dd>
                                </dl>
                            </div>
 
                        </div>
						 
	 <div class="clearfix"> </div>
	
						
				<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">Educational Information  <span style="display: none;"><?=($profile->educations[0]->DurationTo);?></span></p>
                                </div>
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Pass out Year</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->DurationTo;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Higest Qualification</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->course->name;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box" style="display: none;">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Course</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->course->name;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>University / College</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->University;?></h4>
                                             </div>
                                    </div>
                                </div>
								
								 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4> 	Skills</h4>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												<?php
												$skill='';
												foreach($profile->empRelatedSkills as $ask=>$asv)
												{
														$skill.=$asv->skill->Skill.', ';
												}
												$skill=trim(trim($skill),",");
												?>
                                            <h4><?=$skill;?></h4>
                                             </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						 
						 <div class="clearfix"> </div>
						 
						 
						 
				<?php
				if($profile->experiences)
				{
						foreach($profile->experiences as $k=>$val)
						{
				?>
				<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Work Experience  <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span></p>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Company name</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->CompanyName;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Role</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->position->Position;?></h4>
                                             </div>
                                    </div>
                                </div>
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Industry</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                           <?php
											if($val->industry)
											{
											?>
                                            <h4><?=$val->industry->IndustryName;?></h4>
											<?php
											}
											?>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Experience</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												<?php
												$expy=$val->Experience.' Years';
												?>
                                            <h4> <?=$expy;?></h4>
                                             </div>
                                    </div>
                                </div>
								
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Salary</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												
                                            <h4> <?=$val->Salary.' Lakh';?></h4>
                                             </div>
                                    </div>
                                </div>
								
								  
                            </div>
                        </div>
	
  
			  <?php
						}
				}
				?>
			  </div>
            </div>
       </div>
		 
		
		
		
		<div class="border"></div>
</div>
