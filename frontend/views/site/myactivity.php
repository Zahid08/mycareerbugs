<?php
use yii\widgets\ListView;
use frontend\components\searchpeople\SearchPeople;
use yii\helpers\Url;
use frontend\components\sidebar\SidebarWidget;
?>
<style>
.new_alert_design1 {
	padding: 10px 10px 10px;
	margin: 0px;
}

.user-block a {
	color: #6c146b;
	font-size: 14px;
}

@media only screen and (max-width: 767px) { 

.cover.profile .cover-info {
    background: #6c146b !important;
    clear: both;
    height: auto;}
    
    .cover.profile .cover-info .cover-nav li a{color:#fff !important;}

.cover.profile .cover-info .cover-nav {
    margin: 11px 0 0 0 !important;}
}




@media only screen and (max-width: 767px) { 
.cover.profile .cover-info .cover-nav {
    margin: 10px 0 0 0 !important;
    position: relative;
} 


}
</style>
<div class="cover profile" id="cover_photo_b">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
					<div class="profile_main" style="display: none !important">
						<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
					</div>
					<div class="name" style="display: none">
						<a href="#">My Career Bugs Wall</a>
					</div>
					<ul class="cover-nav" style="left: 0px; top: 0px;">

						<!--	<li class="active" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i></span> <span id="fstatus"><?=$fs;?></span></a></li> -->


						<li style="display: none"><a
							href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>


						<li><a href="<?= Url::toRoute(['wall/candidateprofile']);?>">  My Profile</a></li>
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>"> My Activity</a></li>
				 
						<li><a href="<?= Url::toRoute(['wall/mcbwallpost']);?>"> Wall Post </a></li>
					</ul>
				
				</div>
			</div>
		</div>
	</div>
</div>

<div class="form-group right_main" id="mobile_view_only"> 
 	<?php echo SearchPeople::widget()?>
</div>

<!--  Start  -->

<div class="container">
	<div class="row">
		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12">
			<div class="widget" id="wall_contact">
				<div class="widget-header">
					<h3 class="widget-caption">Contact Us</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">
					<div class="row">
						<div class="col-xs-3">Email:</div>
						<div class="col-xs-9">info@mycareerbugs.com</div>
						<div class="col-xs-3">Phone:</div>
						<div class="col-xs-9">+ 91-8240369924</div>
						<div class="col-xs-3">Address:</div>
						<div class="col-xs-9">CF 318 Saltlake Sector 1</div>
						<div class="col-xs-3">URL:</div>
						<div class="col-xs-9">www.mycareerbugs.com</div>
					</div>

					<div class="row">
						<div class="directory-info-row col-xs-12">
							<ul class="social-links">
								<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>


			<div class="widget" id="wall_description">
				<div class="widget-header">
					<h3 class="widget-caption">Career quote</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">Choose a job you
					love, and you will never have to work a day in your life. —
					Confucius</div>
			</div>



			<div class="widget widget-friends" id="wall_followers"
				style="display: none">
				<div class="widget-header">
					<h3 class="widget-caption">Followers</h3>
				</div>
				<div class="widget-like company">
					<div class="row">
						<div class="col-md-12">
							<ul class="img-grid">
					<?php
    if ($totalfollowlist) {
        foreach ($totalfollowlist as $fk => $fvalue) {
            if ($fvalue->followBy->UserTypeId == 2 || $fvalue->followBy->UserTypeId == 5) {
                if ($fvalue->followBy->PhotoId != 0) {
                    $ll = $url . $fvalue->followBy->photo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            } else {
                if ($fvalue->followBy->LogoId != 0) {
                    $ll = $url . $fvalue->followBy->logo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            }
            if ($fvalue->followBy->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->followBy->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->followBy->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }
            ?>
						<li><a
									href="<?= Url::toRoute([$link,'userid'=>$fvalue->followBy->UserId])?>"><img
										src="<?=$ll;?>" alt="image" style="width: 43px; height: 43px;"><?=$fvalue->followBy->Name;?></a>
								</li>
					<?php
        }
    }
    ?>              
                  </ul>
				  <?php
    if ($totalfollow > 8) {
        ?>
					<a href="<?= Url::toRoute(['wallfollower']);?>"><span class="badge">See
									More</span></a>
					<?php
    }
    ?>
                </div>
					</div>
				</div>
			</div>


			<div class="widget" id="wall_advertisement" style="display: none">
				<div class="widget-header">
					<h3 class="widget-caption">Advertise</h3>
				</div>
				<div class="advertisement bordered-sky">
					<img src="https://mycareerbugs.com/images/adban_block/ban.gif"
						style="margin: 0; width: 100%">
				</div>
			</div>
		</div>
		<div class="col-lg-6  col-md-6 col-sm-12 col-xs-12 padding-left">
			<div class="rows">
				<!-- left posts-->
				<div class="activities_main-block">
					<div class="widget-header" style="background: #6c146b">
						<h3 class="widget-caption">My Activites</h3>
					</div>
	<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}\n{pager}\n",
    'itemView' => function ($model, $key, $index, $widget) {
        return $this->render('_myactivity', [
            'model' => $model,
            'date' => $widget->dataProvider->models[$index - 1]->created_on != null ? date('Y-m-d', strtotime($widget->dataProvider->models[$index - 1]->created_on)) : null
        ]);
    }
])?>
</div>
			</div>
		</div>

		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 padding-left">
			<?php echo SidebarWidget::widget()?>
		</div>

	</div>
</div>


















































