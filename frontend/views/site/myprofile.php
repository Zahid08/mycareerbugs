<?php
$this->title = $EmployerName;
use common\models\Likes;
use frontend\components\follow\FollowWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\AllUser;
use yii\helpers\ArrayHelper;
use common\models\UserWhiteRole;
use common\models\ExperienceWhiteRole;
use common\models\UserWhiteSkill;
use common\models\ExperienceWhiteSkill;
use common\models\EmployeeSkill;
use common\models\Skill;
use common\models\PostJob;
use common\models\PostJobWRole;
use common\models\JobRelatedSkill;
use common\models\PostJobWSkill;

// $imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
// $url = '/backend/web/';

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';


use frontend\components\sidebar\SidebarWidget;
use frontend\components\searchpeople\SearchPeople;
use common\components\loadmore\LoadMore;
use common\models\LeaveComment;
$leavecomment = new LeaveComment();
$lik = new Likes();
$filepath = $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/images/coverpic/' . $employer->coverpic;
if (file_exists($filepath) && ! empty($employer->coverpic)) {
    $cover_pic = $imageurl . 'images/coverpic/' . $employer->coverpic;
} else {
    $cover_pic = $imageurl . 'images/background-main.jpg';
}

if ($model->LogoId != 0) {
    $ph = $url . $model->logo->Doc;
} else {
    $ph = $url . 'images/user.png';
}
$cUserId = 0;
if (isset(Yii::$app->session['Employerid'])) {
    $lg = Yii::$app->session['EmployerDP'];
	$cUserId  = Yii::$app->session['Employerid'];
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $lg = Yii::$app->session['EmployeeDP'];
	$cUserId  = Yii::$app->session['Employeeid'];
} elseif (isset(Yii::$app->session['Campusid'])) {
    $lg = Yii::$app->session['CampusDP'];
	$cUserId  = Yii::$app->session['Campusid'];
} elseif (isset(Yii::$app->session['Teamid'])) {
    $lg = Yii::$app->session['TeamDP'];
	$cUserId  = Yii::$app->session['Teamid'];
}

?>



<!-- Begin page content -->
<style>

    @media only screen and (max-width:767px){
.cover.profile .cover-info .cover-nav {
    margin: 0px 0 0 0!important;
    position: relative;
}
.picture-container{    height: 55px !important;}

}

#wall_edit_profile {
	background: #f16b22;
	color: #fff;
	position: absolute;
	right: 15px;
	bottom: 55px;
	width: 134px;
}

.widget-like.company li {
	width: 7%;
	margin: 5px 2px 2px 8px;
}

.uploadFile {
	background:
		url('https://mycareerbugs.com/frontend/web/img/whitecam.png')
		no-repeat;
	height: 32px;
	width: 32px;
	overflow: hidden;
	cursor: pointer;
}

.pos {
	color: #fff;
	position: absolute;
	right: -25px;
	bottom: 55px;
}

body {
	margin: 0;
	padding: 0;
}

.picture-container {
	margin: 0 auto;
	overflow: hidden;
	height: 150px;
	position: relative;
}

.picture-container img {
	min-height: 100%;
	min-width: 100%;
}

.picture-container img {
	position: absolute;
}

.controls a {
	text-decoration: none;
	color: white;
	padding: 15px;
	display: inline-block;
}

.save {
	color: #fff;
	position: absolute;
	right: 150px;
	bottom: 65px;
	width: 134px;
	text-align: center;
}
</style>

<style>
#login_system {
	position: fixed;
	bottom: 0px;
	width: 100%;
	z-index: 99999;
	background: rgba(255, 255, 255, 0.92);
	height: 100vh;
}

.white {
	width: 100%;
	min-height: 270px;
	top: 95%;
	position: absolute;
	left: 50%;
	margin: -225px 0 0 -50%;
	background: #fff;
	padding: 20px 20px 20px 20px;
	border-top: 2px solid #f16b22;
}

#login_system .step.form {
	padding: 20px 0
}

#login_system .step.form img {
	width: 250px;
	margin: 0 auto;
	display: block
}

.wall_logo_1 {
	margin-bottom: 10px !Important
}

#head {
	padding: 0px;
	border-bottom: 1px solid #f16b22
}

#landing_page_banner_button .head_right {
	width: 35%
}
</style>



<style>
.navbar {
	min-height: 41px !important;
}

.select2-selection.select2-selection--single {
	height: 34px !important;
}

#wall_description1 .widget {
	margin: 0;
	box-shadow: 0 0 0 #000;
}

#wall_description1 {
	position: fixed;
	z-index: 999;
	bottom: 0px;
	left: 0px;
	width: 100%;
	margin-bottom: 0px;
}



#wall_description1 .widget-body {
	padding: 12px 12px 0px 12px;
}

.ui-autocomplete {
	width: 179.6px;
	display: block;
	background-color: white;
	margin-top: -19px;
}

#pf_shw_mb{display:none}

@media (max-width: 767px) {
#pf_shw_mb .cover.profile .cover-info {
    background: #6c146b !important;
    clear: both;
    height: auto;}
    
   #pf_shw_mb  .cover.profile .cover-info .cover-nav li a{color:#fff !important;}

#pf_shw_mb .cover.profile .cover-info .cover-nav {
    margin: 11px 0 0 0 !important;}

#pf_shw_mb{display:block}
}
</style>


<?php if(!isset($lg)){ ?>
<div class="header-top" id="head">
	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<a class="logo_top" href=" "> <img class="wall_logo_1"
					src="<?=$imageurl;?>careerimg/logo.png" alt="MCB Logo"
					style="width: 100%">
				</a>
			</div>
			<div class="col-lg-8  col-md-8 col-sm-6 col-xs- 2 main-nav">
				<!-- Main Navigation -->

				<div class="head_right">
					<ul style="margin: 20px 0 0 0;">
						<li><a class="btn-123"
							href="https://mycareerbugs.com/site/employersregister">Employer
								Zone</a></li>
						<li class=""><strong> Are you recruiting?</strong> <a
							href="https://mycareerbugs.com/content/help"> <span>How we can
									help</span>
						</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
</div>



<div class="page-content">
	<div id="login_system">
		<div class="white">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123"
								href="<?= Url::toRoute(['site/login'])?>"> Login </a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="step form">
			    
				<div class="omb_login">
					<h6>Login</h6>

					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="email" class="form-control" name="
								[Email]"
									required placeholder="Email address">
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control"
									name="AllUser[Password]" required placeholder="Password">
							</div>

							<!-- <span class="help-block">Password error</span>  -->

							<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
					</div>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-3">
							<p class="center-left">
								Not Yet Register ? <a
									href="<?= Url::toRoute(['site/register'])?>" class="color">
									Register Now </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-3">
							<p class="omb_forgotPwd">
								<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
									password?</a>
							</p>
						</div>
					</div>
					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<span class="omb_spanOr">or</span>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
 <?php } ?>


<div id="pf_shw_mb" style="display:none"> 
<div class="cover profile" id="cover_photo_b">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
					<div class="profile_main" style="display: none !important">
						<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
					</div>
					<div class="name" style="display: none">
						<a href="#">My Career Bugs Wall</a>
					</div>
					<ul class="cover-nav" style="left: 0px; top: 0px; "> 
						<li><a href="<?= Url::toRoute([$page]);?>"> <i 	class="  fa fa-user"></i> My Profile</a></li>
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>">  My Activity</a></li>
						<li><a href="<?= Url::toRoute(['site/hirecandidate']);?>">  Hire candidate  </a></li> 
						<li><a href="<?= Url::toRoute(['wall/mcbwallpost']);?>"> MCB Wall Post </a></li>
				  	<?php if ($profile->UserTypeId == 3) {?>  
									<li><a href="<?= Url::toRoute(['wall/companyprofile']);?>"> Wall Post  </a></li>
								  <?php } ?>	
								
					</ul>
					<div class="form-group right_main" id="no_dis_mbl1">
						<?php echo SearchPeople::widget()?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

 <div class="form-group right_main" id="mobile_view_only">
 
 	<?php echo SearchPeople::widget()?>
			 
</div>  
</div>











<div class="page-content">
	<div class="cover profile">
		<div class=" ">
			<div class="image">
				<div class="picture-container">
					<img data-top="0" data-left="0" src="<?=$cover_pic?>"
						style="width: 100%; height: 430px;" class="show-in-modal"
						alt="people">
				</div>

			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="cover-info">
					 

						<!--  <?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?>
                  <ul class="cover-nav">
                     <li  class="active"><a href="<?= Url::toRoute(['wall/companyprofile'])?>"><i class="fa fa-user"></i> Profile </a></li>
                     <li  class=""><a href="#"><i class="fa fa-user-plus"></i> Follow </a></li>  
                        <li  class=""><a href="#"><i class="fa fa-envelope"></i> Leave a comment </a></li>                              
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-fa fa-commenting-o"></i> View Comments</a></li>
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-clipboard"></i> Job Post</a></li>
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-sticky-note-o"></i> Wall Post</a></li>
                  </ul>
                 <?php } ?> -->


						<ul class="cover-nav">
							<li class="active">
							<?php if($employer->UserId == $cUserId){ 
								$url = Url::base().'/'. str_replace(' ','',$employer->Name).'-'.$employer->UserId; ?> 
							<a
								href="<?php echo $url;?>"><i
									class="fa fa-user"></i> Profile </a>
									
							<?php }else{?>		
							<a
								href="#"><i
									class="fa fa-user"></i> Profile </a>
							<?php }?>
							</li>
							<!--<li  class=""><a href="#"><i class="fa fa-user-plus"></i> Follow </a></li>  
                        <li  class=""><a href="#"><i class="fa fa-envelope"></i> Leave a comment </a></li>-->
							<li class="">
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
							<a href="<?= Url::toRoute(['site/yourpost']);?>"><i
									class="fa fa-clipboard"></i> Job Post</a>
							<?php }else{ ?>	
							<a href="<?= Url::toRoute(['site/jobsearch','company' => $employer->UserId]);?>"><i
									class="fa fa-clipboard"></i> Job Post</a>
							<?php }?>
							
									</li>
							
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
							<li class="">
							<a href="<?= Url::toRoute(['wall/companyprofile']);?>"><i
									class="fa fa-sticky-note-o"></i> Wall Post</a>
							</li>
							<li class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i
									class="fa fa-fa fa-commenting-o"></i> View Comments</a></li>
									
							<?php }else{?>
							<li class="">
							<a href="<?= Url::toRoute(['wall/searchcompany','userid' => $employer->UserId])?>"><i
									class="fa fa-sticky-note-o"></i> Wall Post</a>
							</li>
							<li class="">
							<a href="#"
								onclick="if(isMobile==true){$('#mobileleavecomment').slideToggle();}else{$('#leavecomment').slideToggle();}$(this).parent().toggleClass('active');$('#quickupdate').toggle();$('.profile').toggleClass('active');">
									<i class="fa fa-envelope"> </i> Write a Review
							</a>		
							</li>	
							<?php }?>
							
							
									
						</ul>

					</div>
				</div>
				<div class="col-md-3">
					
					<div class="widget no_shadow" style="margin: 10px 0 0 0">
						<div class="action-buttons">
							<div class="row">
								<div class="col-md-12">
									<?php //echo '<pre>'; print_r($cUserId); print_r($id);die;?>
									<?php if($user_loggenin && $cUserId != $id){ ?>
									<?php
									echo FollowWidget::widget([
										'profile' => $employer,
										'empid' => $cUserId
									]);
									?>
									
<!-- 									<div class="half-block-left"> -->
									<!-- 										<a href="#" class="btn btn-azure btn-block"><i -->
									<!-- 											class="fa fa-user-plus"></i> Follow</a> -->
									<!-- 									</div> -->
									<div class="half-block-right">
										<a href="javascript:;" data-toggle="modal"
											data-target="#myModalmessage" class="btn btn-azure btn-block">
											<i class="fa fa-envelope"> </i> Message
										</a>
									</div>
				  <?php }?>	
				  <?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
                   <a href="" class="save">Save Changes</a> <a
										href="<?= Url::toRoute(['site/companyprofileeditpage'])?>"
										class="btn btn-azure btn-block" id="wall_edit_profile"> <i
										class="fa fa-pencil-square-o"></i> Edit Profile
									</a>
									<div class="controls">
										<img
											src="https://mycareerbugs.com/frontend/web/img/whitecam.png"
											class="pos" alt="edcvr">
									</div>
                  <?php } ?>
			
                </div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- Begin page content -->
	<div class="page-content like_pae">
		<div class="container">
			<div class="row">
			 
				<div class="col-md-9 padding-left">
					<div class="row">
						<!-- left posts-->
						<div class="col-md-12">
							
							<div id="leavecomment" style="display: none;">
								<div class="widget-header">
									<h3 class="widget-caption"> Write Review  </h3>
								</div>
								<div class="widget">
									<div class="resume-box">                              
									<?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'row',
                'enctype' => 'multipart/form-data'
            ],
            'action' => Url::toRoute([
                'wall/searchcandidate',
                'userid' => $employer->UserId
            ])
        ]);
    ?>                              <div class="form-group"> 
                                    	<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>                              
                                    </div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">                                    
											<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                                 
										</div>
									</div>                             
									 <?php ActiveForm::end(); ?>                           
									 </div>
								</div>
							</div>

						
						 
						</div>
					</div>
				 
				 
					 
					 
				</div>
			 
			</div>
		</div>
	</div>
</div>





 <style>
 
.col-md-3  .widget-know li img {
    width: 20%;
    margin: 0 10px 0 0;
    float: left;
    border-radius: 100px;
}

.col-md-3 .widget-know li {
    border-radius: 25px 0 0 28px;
    float: left;
    margin: 10px 10px 0 10px;
    width: 94%;
}

.col-md-3 .widget-know li a span {
    text-align: left;
    display: block;
    font-size: 12px;
    color: #565656;
}
                        .cover.profile .cover-info .cover-nav{left:0px;}
                        .btn.btn-default.btn-xs.bg_prpl{margin-bottom:0px;}
                        .widget-like.company li {width: 7%;margin: 5px 2px 2px 8px;}
                        .half-block-three{float:left; width:48%; margin:0 1%}
                        .action-buttons a{background:#f16b22}
                        .widget-know li img{width:100%}
                        #profile_followers ul li {
                        width: 7%;
                        }
                        .comment.more p a {
                        background: #e9e9e9;
                        color: #000;
                        padding: 9px;
                        font-size: 12px;
                        width: 100%;
                        display: block;
                        }
                        #profile_followers  img{margin: 0 0 10px 0;}
                        .box-body p {
                        font-size: 12px;
                        line-height: 18px;
                        color: #000;
                        overflow: hidden;
                        }
                        .comment {
                        font-size: 12px;
                        margin: 0 10px 10px 10px;
                        }
                        #profile_followers a{font-size:10px; text-align:Center;}
                        .widget-know li{margin:10px 1%}


  #wall_edit_profile{    background: #f16b22;
            color: #fff;position:absolute; right:15px;bottom:55px;width:134px}
            #login_system{position:fixed; bottom:0px;width:100%;height:100vh; z-index:99999; background:rgba(0,0,0,0.5);}
            #login_system .step.form {padding: 20px 0 }
            #login_system .step.form img{width:200px; margin:0 auto; display:block}
            .wall_logo_1{margin-bottom:10px !Important}
            #head {  padding: 0px;  border-bottom: 1px solid #f16b22}
            #landing_page_banner_button .head_right{width:35%}
            #landing_page_banner_button ul li a.full_2{width:100%}
            #landing_page_banner_button ul li a{  margin: 10px 0 0 0;}
            #login_system .wrapper_m{  position:absolute;bottom:0px; background:#fff; width:100%;  border-top:4px solid #f16b22}

                     </style>
 

<!-- New Design Start-------------------------------------------------------------------------------------------------------->

  <div class="page-content like_pae">
               <div class="container">
  <div class="row"> 
                     <div class="col-md-9 padding-left">
                        <div class="profile_main" style="width:100%; border-top:2px solid #f16b22; padding:20px 0 10px 0; margin:0px 0 15px 0; background:#fff; clear:both; overflow:hidden">
                           <img src="<?=$pimage;?>" alt="people" class="" style="width:80px; float:left;display:block;  margin:0 20px 4px 18px; border-radius:200px"> 
                           <div class="profile_cont">
                              <div style="float:left;">
                                 <a href="#" style="padding:0 ">   <a href="#"><?=$EmployerName;?></a>  <span style="font-size:13px; color:#565656">- Company / HR </span>
                                 </a>
                                 <p style="line-height:20px;">    
                                 <strong> Address: </strong> <?php echo  $employer->Address; ?> <br> Pincode: <?php echo  $employer->PinCode; ?>  <br>
                                <?php echo  $employer->City; ?> , <?php echo  $employer->State; ?>,  - <?php echo  $employer->Country; ?> </p>
                              					
                              </div>
                              <div class="widget no_shadow" style="float:right; width:300px;">
                                 <div class="action-buttons">
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="half-block-three">
                                              
							<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
							<a class="btn btn-azure btn-block" href="<?= Url::toRoute(['site/yourpost']);?>" target="_blank">Job Post</a>
							<?php }else{ ?>	
							<a class="btn btn-azure btn-block"  href="<?= Url::toRoute(['site/jobsearch','company' => $employer->UserId]);?>" target="_blank"> Job Post</a>
							<?php }?>
						 
                                          </div>
                                          <div class="half-block-three">
                                             <a href="<?= Url::toRoute(['wall/searchcompany','userid' => $employer->UserId])?>" class="btn btn-azure btn-block" target="_blank"> Wall post</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div style="clear:both; overflow:hidden"></div>
                              <div style="padding:0px 20px 10px 20px;">
                                 <div style="width:100%; height:2px; background:#eeeeee; margin:10px   0"></div>
                                 <p style="line-height:23px;     margin: 0 0 0px;float:left; width:50%">  
                                    <i class="  fa fa-thumbs-up"></i> Followers : <strong> <?=count($lik->getTotalfollow($id));?>  </strong><br>
                                    	<?php if($user_loggenin && ($id==0 || $sess_id==$id)){ ?> 
                                    	<i class=" fa fa-comments"></i> Number of wall Post :  <strong>  <?=count($allpost);?>- </strong><a href="<?= Url::toRoute(['wall/companyprofile'])?>">View all</a><br>
            							<?php }else{?>		
            							<i class=" fa fa-comments"></i> Number of wall Post : <strong><?=count($allpost);?>- </strong><a href="<?= Url::toRoute(['wall/searchcompany','userid' => $employer->UserId])?>">View all</a><br>							
            							<?php }?>
							
                                  
                                    <i class=" fa fa-briefcase"></i> Total Active Jobs :  		<strong> <?=$totalactive;?> - </strong>View all 
                                 </p>
                                 <p style="line-height:23px;    margin: 0 0 0px; float:left; width:50%">  
                                    Industry : <strong> <?php echo  $employer->industry->IndustryName; ?>   </strong><br>
                                    Contact Person :  <strong> <?php echo  $employer->ContactPerson; ?>  </strong><br>
                                    Contact Number :  <strong>  <?php echo  $employer->MobileNo; ?>   </strong><br>
                                    
                                    
                                    Company Email :  		<strong>  <?php echo  $employer->Email; ?>  </strong> 
								
                                 </p>
                                 <div style="clear:both; overflow:hidden"></div>
                                 <div style="width:100%; height:2px; background:#eeeeee; margin:10px   0"></div>
                                 <p>  <strong style="margin:2px 10px 0px 0; float:left"> Skills:   </strong>   
                                 
                                    
              <?php

            if ($skill) {

                foreach ($skill as $sk => $sval) {

                    ?>

             <button class="btn btn-default btn-xs bg_prpl"><?=$sval;?> </button>

              <?php
                }
            }

            ?>
            
                                 </p>
                              </div>
                           </div>
                        </div>
                        <div class="widget">
                           <div class="widget-header">
                              <h3 class="widget-caption">  Decsription</h3>
                           </div>
                           <div class="widget-body bordered-top bordered-sky">
                                   	<span class="description"><?php echo  html_entity_decode($employer->CompanyDesc); ?></span>
                           </div>
                        </div>
                        <div class="widget">
                           <div class="widget-header">
                              <h3 class="widget-caption"> Job Post</h3>
                           </div>
                          
                           <?php
                           
                                $postjob = new PostJob();

            $allpostJobs = $postjob->find()
                ->where([
                    'EmployerId' => $employer->UserId
                ])
                ->andFilterWhere([
                    'PostFor' => $postType
                ])
                ->orderBy([
                    'OnDate' => SORT_DESC
                ])
                ->all();
                
                $count=1;
				foreach($allpostJobs as $key=>$value)
				{
						if($value->JobStatus==0)
						{
								$status='Open';
								$background='green';
						}
						else
						{
								$status='Closed';
								$background='red';
						}
						
						$slug=$value->Slug;
						$urlCandidate= Url::base().'/job/'.$slug;
						
						if($value->PostFor=='Campus'){$pf=1;}else{$pf=0;}
					    if($count<3){
					        
						?>
						
							<?php $whiteRoleList = ArrayHelper::map(PostJobWRole::find()->where([
															'postjob_id' => $value->JobId,
															'user_id' => $value->EmployerId
														])->all(), 'role_id', 'role'); ?>
														

					  
                           <div class="resume-box" style="padding:10px">
                            
                              <div class="box-body profile_co" style="padding:0px 0 0px 0">
                                 <h2 style="background: #f16b22;color:#fff;margin-top:0px;font-weight:normal;padding:10px;font-size:14px;">
                                     <?php
                                       if($user_loggenin && ($id==0 || $sess_id==$id)){
                                     ?>  
                                    <a href="<?= Url::toRoute(['site/postdetail','JobId'=>$value->JobId,'pf'=>$pf])?>" style="color:#fff; display:block;"><?=$value->JobTitle;?> </a> 
                                    <?php }else{ ?>
                                        <a href="<?=$urlCandidate?>" style="color:#fff; display:block;"><?=$value->JobTitle;?> </a> 
                                    <?php }?>
                                 </h2>
                              </div>
                              <div class="box-header with-border">
                                 <div class="user-block">
                                    <img class="img-circle" src="<?=$pimage;?>" alt="User Image">
                                    <span class="username">
                                           <?php
                                                if($user_loggenin && ($id==0 || $sess_id==$id)){
                                            ?>  
                                            <a href="<?= Url::toRoute(['site/postdetail','JobId'=>$value->JobId,'pf'=>$pf])?>"><?=$value->CompanyName;?></a>
                                         <?php }else{ ?>
                                          <a href="<?= $urlCandidate?>"><?=$value->CompanyName;?></a>
                                           <?php }?>
                                           
                                        </span>
                                    <!-- INDUS -->
                                    <span class="description extra_top_mrg">  <?php echo  $employer->industry->IndustryName; ?>      -   <?=$value->Location;?>     </span>
                                   <!-- <span class="description top_mr_abs" style="font-size: 10px; position: absolute; right: 9px; top: 41px;"> 
                                    04:55 pm 05 Sep, 2020    </span> -->
                                 </div>
                              </div>
                              <style>
                                 #post-e-c li{line-height:22px;font-size: 13px;font-weight:normal;}
                                 #post-e-c span{ color:#6c146b; font-weight:bold;}
                                 #post-e-c small{ font-size: 13px; text-align:right;margin-right:20px; display:inline-block;width:100px;font-weight:normal;}
                              </style>
                              <div style="padding:10px 10px 0 10px" id="post-e-c">
                                 <ul id="details" style="margin:0;padding:0px;">
                                    <li> <small>Designation :  </small><span> <?=$value->Designation;?></span> </li>
                                    <?php
                						if($value->IsExp==0){
                								$exp='Fresher';
                						}
                						elseif($value->IsExp==1)
                						{
                								$exp=($value->Experience!='')?$value->Experience.' Years':'Experience';
                						}
                						elseif($value->IsExp==2)
                						{
                								$exp='Both - Freshers & Experienced';
                						}
                						?>
						
                                    <li> <small>Experience  :  </small><span> <?=$exp;?>  </span> </li>
                                    <li> <small>Salary Range :  </small><span>1.5 - 3 Lakhs    </span> </li>
                                    <li> <small>Role  :  </small><span>
                                        <?php 
                                         if($value->CollarType == 'white'){
                                        ?>
                                        	<?php echo (!empty($whiteRoleList)?implode(', ',$whiteRoleList):"");?>
                                        	
                                        	<?php }else{?>
                                        	<?=$value->position->Position;?>
                                        	<?php }?>
                                           </span> </li>
                                           
                                    <li style="magin:10px 0 0 0"> <small>Skills  :  </small><span>   
                                     <?php if($value->CollarType == 'white'){?>
                                     
                                     	<?php $whiteSkillList = ArrayHelper::map(PostJobWSkill::find()->where([
															'postjob_id' => $value->JobId,
															'user_id' => $value->EmployerId
														])->all(), 'skill_id', 'skill'); ?>
									<?php 
									foreach($whiteSkillList as $items){
									?>					
                                     <button class="btn btn-default btn-xs bg_prpl"> <?=$items?>  </button>
                                     
                                     <?php } }else{
                                     $kyt = JobRelatedSkill::find()->where(['JobId' => $value->JobId])->all(); 
                                     	foreach($kyt as $ky){
                                     	    $skll = Skill::find()->where(['SkillId'=> $ky->SkillId])->one();
                                     ?>
                                     <button class="btn btn-default btn-xs bg_prpl"> <?=$skll->Skill?>  </button>
                                     
                                     <?php } }?>
                                    </li>
                                 </ul>
                                 <div class="degree-info">
                                    <h4 style="    font-size: 12px;
                                       line-height: 18px;
                                       color: #999; margin-top:10px; margin-bottom:5px"> <strong style="color:#6c146b !important">Description:</strong><?=substr(htmlspecialchars_decode($value->JobDescription),0,150);?> 
                                       
                                         <?php
                                                if($user_loggenin && ($id==0 || $sess_id==$id)){
                                            ?>  
                                            <a href="<?= Url::toRoute(['site/postdetail','JobId'=>$value->JobId,'pf'=>$pf])?>">View Now</a>
                                         <?php }else{ ?>
                                            <a href="<?= $urlCandidate?>">View Now</a>
                                           <?php }?>
                                       </h4>
                                 </div>
                              </div>
                           </div>
                           
                            <?php } $count++;} ?>
                        </div>
                        <div class="widget">
                           <div class="widget-header">
                              <h3 class="widget-caption"> Wall Post - Latest</h3>
                           </div>
                           <?php

if ($allpost) 
{
	/*echo '<pre>';
	print_r($allpost);die;*/

$count=1;
    foreach ($allpost as $postkey => $postvalue) 
    {
		if(!empty($postvalue->Slug)){
			$urlLink = Url::base().'/wall-post/'.$postvalue->Slug;
		}else{
			$urlLink = Url::toRoute(['/wall/post-view','id' =>  $postvalue->WallPostId]);	
		}

        if ($postvalue->employer->LogoId != '') 
        {

            $logo = $imageurl.'backend/web/' . $postvalue->employer->logo->Doc;
        } 
        else 
        {

            $logo = $imageurl .'backend/web/'. 'images/user.png';
        }

    

 //echo $urlLink;
//echo $logo;

        if($count<3){
        ?>
    
        
        	<div class="box box-widget">
		<div class="box-header with-border">
			<div class="user-block"> 
			<img class="img-circle" src="<?=$logo;?>" alt="User Image"> <span class="username">
			    <a	href="<?=$urlLink?>"><?=$postvalue->employer->Name;?></a></span> <span class="description">Shared publicly - <?=date('h:i A D M',strtotime($postvalue->OnDate));?>   - <?=$postvalue->Locationat;?></span> <span class="postright" onclick="$(this).next().toggle();"><i													class="fa fa-angle-down"></i> </span>
				<div class="postbox">
					<ul>
						<li style="width:100%" onclick="$('#editbox<?=$postvalue->WallPostId;?>').show();">Edit</li>
						<li style="width:100%" onclick="deletepost1(<?=$postvalue->WallPostId;?>);">Delete</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="box-body" style="display: block;">
		<?php
			if ($postvalue->wallimage){ ?>
              <img class="img-responsive pad show-in-modal" src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
        <?php }

        if ($postvalue->wallvideo){?>
                                                            
			<video width="320" height="240" controls>
				<source src="<?=$url.$postvalue->wallvideo->Doc;?>" type="video/mp4"> </video>
			<?php
        }?>
		<div class="comment more">
		<p><a href="<?=$urlLink;?>"><?=$postvalue->PostTitle;?></a></p>
        <?=str_replace("\n", '<br />',  $postvalue->Post);?>
		</div>
		<?php

        if ($postvalue->likes) 
        {

            $lk = $postvalue->getlike($postvalue->WallPostId, Yii::$app->session['Campusid']);

            if ($lk) 
            {

                if ($lk->IsLike == 1) 
                {

                    ?>
                                                                    <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-down"></i> UnLike </button>
                                                                    <?php
                } 
                else 
                {

                    ?>
                                                                        <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-up"></i> Like </button>
                                                                        <?php
                }
            } else {

                ?>
                                                                            <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-up"></i> Like </button>
                                                                            <?php
            }

            ?>
                                                                                <span class="pull-right text-muted"><?=count($postvalue->likes);?> likes  </span>
                                                                                <?php
        } 
        else 
        {

            ?>
                                                                                    <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-up"></i> Like </button>
                                                                                    <?php
        }

        ?>
		
		<a href="<?=$urlLink;?>">View </a>
                                               
												
												<br class="clear"/>
												<div class="share" style="float: right; padding-top: 0px;display:none">
												<?php 
														$whatsapp_desc = 'Job opportunity at ' . $postvalue->employer->Name . " for Location " . $postvalue->Locationat . " for more detils visit " . $urlLink;
													?>													
												Share On <a
													href="https://www.facebook.com/sharer/sharer.php?u=<?=$urlLink;?>"
													target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i>
												</a>
												<!-- <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://developer.linkedin.com&title=LinkedIn%20Developer%20Network&summary=My%20favorite%20developer%20program&source=LinkedIn" target="_blank">
															 <i class="fa fa-linkedin"></i>
														  </a> -->
												<a href="javascript:;"
													onclick="mailtoemp('myModal_email','<?=$postvalue->WallPostId;?>');"
													style="line-height: 32px;"><i class="fa fa-envelope-o"></i> </a> <a
													href="https://api.whatsapp.com/send?text=<?=strip_tags($whatsapp_desc);?>"
													target="_blank"><i class="fa fa-whatsapp"></i></a>

											</div>
											
											   </div>
											   
											   
											   
									
        
                                                <div class="box-footer" style="display: none;"> <img class="img-responsive img-circle img-sm" src="<?=Yii::$app->session['EmployerDP'];?>" alt="Alt Text">
                                                    <div class="img-push">
                                                        <div class="form-group"> <span class="input-icon inverted"> <input type="hidden"														name="CommentToPost[PostId]"														id="commentpostid<?=$postvalue->WallPostId;?>"														value="<?=$postvalue->WallPostId;?>" /> <input														type="hidden" name="CommentToPost[EmpId]"														id="commentempid<?=$postvalue->WallPostId;?>"														value="<?=Yii::$app->session['Employerid'];?>" /> <input														class="form-control input-lg" type="text"														id="commentmessage<?=$postvalue->WallPostId;?>"														name="CommentToPost[Message]" required>														<p															class="help-block help-block-error error<?=$postvalue->WallPostId;?>"															style="color: #d73d32;"></p>														<button type="button"															onclick="Postcomment(<?=$postvalue->WallPostId;?>);"															class="fa fa-envelope bg-palegreen"															style="border: none; width: 46px;"></button>													</span> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
  } $count++; }
}

?>
    
                        </div>
                        <div class="widget">
                           <div class="widget-header">
                              <h3 class="widget-caption"> Followers</span></h3>
                           </div>
                           <div class="widget-know" id="profile_followers">
                              <ul class=" ">
                                  	<?php
											$followers = $lik->getTotalfollow($employer->UserId);
												
											if ($followers) {

												foreach ($followers as $fk => $fvalue) {

													if ($fvalue->likeFrom->UserTypeId == 2 || $fvalue->likeFrom->UserTypeId == 5) {

														if ($fvalue->likeFrom->PhotoId != 0) {

															$ll = $url . $fvalue->likeFrom->photo->Doc;
														} else {

															$ll = $imageurl . 'images/user.png';
														}
													} else {

														if ($fvalue->likeFrom->LogoId != 0) {

															$ll = $url . $fvalue->likeFrom->logo->Doc;
														} else {

															$ll = $imageurl . 'images/user.png';
														}
													}

													if ($fvalue->likeFrom->UserTypeId == 2) {
														$link = 'searchcandidate';
													} elseif ($fvalue->likeFrom->UserTypeId == 3) {
														$link = 'searchcompany';
													} elseif ($fvalue->likeFrom->UserTypeId == 4) {
														$link = 'searchcampus';
													} else {
														$link = 'searchteam';
													}

													?>

                                                   <li> <a href="<?= Url::toRoute(['/wall/'.$link,'userid'=>$fvalue->likeFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"> <?=$fvalue->likeFrom->Name;?>
                                                                                    </a> 
                                                                                 </li>
                                                                                 
										

											   <?php
												}
											}

											?>  
									
									<?php
										if ($followers) {
									?>		
                                 <li> <a href=""> View All</a> </li>
                                 
                                 <?php }?>
                              </ul>
                           </div>
                        </div>
                        <div class="widget">
                           <div class="widget-header">
                              <h3 class="widget-caption">  Latest Reviews </h3>
                           </div>
                           <div class="widget-body bordered-top bordered-sky">
                             <?php
                                                $lv = new LeaveComment();
                                                $allcomment = $lv->getComment($employer->UserId);
                                                if ($allcomment) {
                                                    foreach ($allcomment as $ck => $cval) {
                                                        if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) {
                                                            $ll = ($cval->commentFrom->PhotoId != 0) ? $imageurl.'backend/web/'. $cval->commentFrom->photo->Doc : '/backend/web/imageupload/user.png';
                                                        } elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) {
                                                            $ll = ($cval->commentFrom->LogoId != 0) ? $imageurl.'backend/web/'. $cval->commentFrom->logo->Doc : '/backend/web/imageupload/user.png';
                                                        }
                                                        if ($cval->commentFrom->UserTypeId == 2) {
                                                            $link = 'searchcandidate';
                                                        } elseif ($cval->commentFrom->UserTypeId == 3) {
                                                            $link = 'searchcompany';
                                                        } elseif ($cval->commentFrom->UserTypeId == 4) {
                                                            $link = 'searchcampus';
                                                        } else {
                                                            $link = 'searchteam';
                                                        }
                                                        
                                                       // echo $ll;
                                                        ?>
                                                    <div class="box-header with-border">
                                                     <div class="user-block">
                                                        <img class="img-circle" src="<?=$ll;?>" alt="User Image">
                                                        <span class="username"><a href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>"><?=$cval->commentFrom->Name;?></a>   </span> 
                                                        <span class="description">Accounting / Finance - Kolkata -  7:30 PM Today </span>
                                                     </div>
                                                     <div class="box-body profile_co">
                                                        <ul>
                                                           <li class="comments-block">
                                                              <p> <strong>Feedback  </strong>
                                                                 <br>
                                                                <?=$cval->Comment;?>
                                                              </p>
                                                           </li>
                                                        </ul>
                                                     </div>
                                                  </div>
                              
                                        											<?php
                                                    }
                                                }
                                                ?> 
                           </div>
                        </div>
                        <div class="widget">
                           <div class="widget-header">
                              <h3 class="widget-caption"> Following </h3>
                           </div>
                           <style>
                              #following_main .profile_cont{width:85%;  text-align:left; float:right; margin:0 0 10px 0; font-size:18px;}
                           </style>
                           
                           <?php 
                           	$followers = $lik->getMyfollow($employer->UserId);
                           	if ($followers) {
							foreach ($followers as $fk => $fvalue) {

								if ($fvalue->likeTo->UserTypeId == 2 || $fvalue->likeTo->UserTypeId == 5) {

									if ($fvalue->likeTo->PhotoId != 0) {
										$ll = $imageurl.'backend/web/'. $fvalue->likeTo->photo->Doc;
									} else {

										$ll = $imageurl.'backend/web/'. 'images/user.png';
									}
								} else {

									if ($fvalue->likeTo->LogoId != 0) {

										$ll =$imageurl.'backend/web/'. $fvalue->likeTo->logo->Doc;
									} else {

										$ll =$imageurl.'backend/web/'. 'images/user.png';
									}
								}
								
								if ($fvalue->likeTo->UserTypeId == 2) {
									$link = 'searchcandidate';
								} elseif ($fvalue->likeTo->UserTypeId == 3) {
									$link = 'searchcompany';
								} elseif ($fvalue->likeTo->UserTypeId == 4) {
									$link = 'searchcampus';
								} else {
									$link = 'searchteam';
								}
						
						        $allRoles=$allSkill='';
    						if($fvalue->likeTo->CollarType == 'white'){
    						      $jobRoles1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $fvalue->likeTo
																	])->all(), 'role', 'role');
														  $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $fvalue->likeTo
																	])->all(), 'role', 'role');
														  $jobRoles = array_merge($jobRoles1, $expRoles);	
														  
														 $allRoles=(!empty($jobRoles)?implode(', ',$jobRoles):"");
														 
														 
														 $jobSkills1 = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $fvalue->likeTo->UserId
																	])->all(), 'skill', 'skill'); 
														  $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $fvalue->likeTo->UserId,
																	])->all(), 'skill', 'skill');
														$jobSkills = array_merge($jobSkills1, $expSkills);		
														
														$allSkill=(!empty($jobSkills)?implode(', ',$jobSkills):"");
    						}else{
    						    $allRoles=$fvalue->likeTo->getEducationRoles();
    						    $skillArray = array();
    						    
								foreach ($fvalue->likeTo->empRelatedSkills as $ask => $asv) {
									$skillArray[$asv->skill->SkillId] = $asv->skill->Skill;
								    
								}
								$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
											'IsDelete' => 0,
											'UserId' => $fvalue->likeTo->UserId,
										])->all(), 'Employeeskillid', 'SkillId');
										
										$skills = ArrayHelper::map(Skill::find()->where([
											'IsDelete' => 0,
											'SkillId' => $employeeSkill
										])->all(), 'SkillId', 'Skill');
										
										
										$blueSkill = array_merge($skillArray, $skills);
										$blueSkill = array_unique($blueSkill);
														
    						}
    						$locations=$fvalue->likeTo->getPreferedJobLocation();
    						
                           ?>
                           <div class="profile_main" id="following_main" style="width:100%; border-top:2px solid #f16b22; padding:20px 20px 0 20px; margin-bottom:10px; background:#fff; clear:both; overflow:hidden">
                              <div>
                                 <img src="<?=$ll?>" alt="people" class="" style="width:100px; float:left; border-radius:200px"> 
                                 <div class="profile_cont">
                                    <a href="#"><?=$fvalue->likeTo->Name;?><span style="font-size:13px; color:#565656"> - Candidate </span> </a>
                                    <p style="line-height:22px">  <strong><?=$locations?></strong>
                                       <br>    
                                       <strong> Role   </strong>:  <?=$allRoles?>   
                                    </p>
                                    <p style="line-height:25px"> 
                                       <strong style="float:left; magin:0px 15px 0 0"> Skills   </strong>:
                                       <?php if($blueSkill){foreach($blueSkill as $item){ ?>
                                       <button class="btn btn-default btn-xs bg_prpl"><?=$item?></button> 
                                       <?php } }?>
                                    </p>
                                 </div>
                              </div>
                           </div>
                             <?php	}	}	?>           
                             
                        </div>
                     </div>
                     
                     
                     
                     
                     
                     <div class="col-md-3 padding-left">
                     
                        <div class="widget">
                            <div class="advertisement bordered-sky"> <img src="https://www.mycareerbugs.com/careerimg/adban_block/ban.jpg"> <img class="no_mar" src="https://www.mycareerbugs.com/careerimg/adban_block/ban1.jpg"> <img src="https://www.mycareerbugs.com/careerimg/adban_block/ban1.jpg"> <img class="no_mar" src="https://www.mycareerbugs.com/careerimg/adban_block/ban.jpg"> </div>
                        </div>
                    </div>
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                     
                  </div>
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  

  </div>
                  </div>

<!-- New Design End-------------------------------------------------------------------------------------------------------->














<!-- Modal -->
<div class="modal fade" id="myModalmessage" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">  
					 <?php
    $url_info = explode('/', Yii::$app->request->url);
    $form = ActiveForm::begin([
        'options' => [
            'class' => 'row',
            'enctype' => 'multipart/form-data'
        ],
        'action' => Url::toRoute([
            '/' . $url_info[1]
        ])
    ]);
    ?>               
					 <div class="form-group">                  
						<?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>               
						</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">                     
							<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                  
						</div>
					</div>              
				 	<?php ActiveForm::end(); ?>            
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>

<script>
$(document).ready(function () {
		$('.save').hide();
		var img = $('.picture-container img'); 
		img.attr('style', 'top:<?= $employer->data_top?>px;left:<?= $employer->data_lft?>px');
		var y1 = $('.picture-container').height();
    	var y2 = img.height();
    	var x1 = $('.picture-container').width();
    	var x2 = img.width();
    	var desktop_start_x=0;
    	var desktop_start_y=0;
    	var mobile_start_x= -200;
    	var mobile_start_y= -200;
		$('.save').click(function(event){
	            event.preventDefault();
	            var t = img.position().top,
				l = img.position().left;
				img.attr('top', t);
	            img.attr('left', l);
	            img.draggable({ disabled: true });
				$('.save').hide(); 
				var cmid =  "<?= $employer->UserId ?>";
				$.ajax({url:"<?= Url::toRoute(['site/covloc'])?>?imgtp="+t+'&imglf='+l+'&cmid='+cmid,
				success:function(result)
				{
					  $('#allcompanypost').html(result);
					  $('#myModalpostsearch').modal('hide');
				}
			});
		})
		$('.pos').click(function(event){
			event.preventDefault();
			$('.save').show();
			  img.draggable({ 
			  	disabled: false,
			  	scroll: false,
			  	axis: 'y, x',
			  	cursor : 'move',
    		  	drag: function(event, ui) {
                     if(ui.position.top >= 0)
                      {
                          ui.position.top = 0;
                      }
                      if(ui.position.top <= y1 - y2)
                      {
                          ui.position.top = y1 - y2;
                      }
                      if (ui.position.left >= 0) {
                      	ui.position.left = 0;
                      };
                      if(ui.position.left <= x1 - x2)
                      {
                          ui.position.left = x1 - x2;
                      }
    		 	}
		});
	});
});

		</script>