<?php
   if(!empty($EmployerName)){
      $this->title = $EmployerName;

   }else{
      $this->title = 'Companyprofile';
      
   }
   
   use yii\helpers\Url;
   use yii\helpers\Html;
   use yii\bootstrap\ActiveForm;
   $imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
   $url='/backend/web/';
   use common\models\Likes;
   $lik=new Likes();
   $filepath = $_SERVER['DOCUMENT_ROOT'].'/frontend/web/images/coverpic/'.$employer->coverpic;
   if(file_exists($filepath)){
      $cover_pic = $imageurl.'images/coverpic/'.$employer->coverpic;
   }else{
      $cover_pic = $imageurl.'images/background-main.jpg';
   }
   ?>
<!-- Begin page content -->
<div class="page-content">
   <div class="cover profile">
      <div class=" ">
         <div class="image">
            <img src="<?=$cover_pic?>" style="width:100%; height:430px;" class="show-in-modal" alt="people">
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="cover-info">
                  <div class="profile_main">
                     <img src="<?=$pimage;?>" alt="people" class="">
                  </div>
                  <div class="name"><a href="#"><?=$EmployerName;?></a></div>
                  <ul class="cover-nav">
                     <li  class="active"><a href="<?= Url::toRoute(['companyprofile'])?>"><i class="fa fa-user"></i> Profile </a></li>
                     <!--<li  class=""><a href="#"><i class="fa fa-user-plus"></i> Follow </a></li>  
                        <li  class=""><a href="#"><i class="fa fa-envelope"></i> Leave a comment </a></li>-->                               
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-eye"></i> View Comments</a></li>
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-eye"></i> Job Post</a></li>
                     <li  class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i class="fa fa-eye"></i> MCB Wall Post</a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   

 

<div id="wrapper"><!-- start main wrapper -->
   <div class="inner_page">
      <div class="container">
         <div class="omb_login">                 
                  <div class="row omb_row-sm-offset-3">

                     <div class="col-xs-12 col-sm-6"> 

                        <?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>

                           <div class="input-group">

                              <span class="input-group-addon"><i class="fa fa-user"></i></span>

                              <input type="email" class="form-control" name="AllUser[Email]" required placeholder="Email address">

                           </div>

                           <span class="help-block"></span>

                                          

                           <div class="input-group">

                              <span class="input-group-addon"><i class="fa fa-lock"></i></span>

                              <input  type="password" class="form-control" name="AllUser[Password]" required placeholder="Password">

                           </div>

                             <span class="help-block"> &nbsp  </span> 

                            <!-- <span class="help-block">Password error</span>  -->



                           <input type="submit" class="btn btn-primary btn-lg btn-block login-button" value="Log In" >

                  

                        <?php ActiveForm::end(); ?>

                     </div>

                  </div>

                  <div class="row omb_row-sm-offset-3">

                     <div class="col-xs-12 col-sm-3">

                          <p class="center-left"> Not Yet Register ?  <a  href="<?= Url::toRoute(['site/register'])?>"  class="color"> Register Now </a></p>

                     </div>

                     <div class="col-xs-12 col-sm-3">

                        <p class="omb_forgotPwd">

                           <a  href="<?= Url::toRoute(['site/forgetpassword'])?>" >Forgot password?</a>

                        </p>

                     </div>

                  </div>

                

                  

                  <div class="row omb_row-sm-offset-3 omb_loginOr">

                     <div class="col-xs-12 col-sm-6">

                        <hr class="omb_hrOr">

                        <span class="omb_spanOr">or</span>

                     </div>

                  </div>



                  

         <div class="row omb_row-sm-offset-3 omb_socialButtons">


                     <?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>

                  </div>

               </div>

                             </div>

          </div>

       

      <style>

      .omb_socialButtons #w1 {   width: 97px;}

       ul.auth-clients .linkedin.auth-link{display:none}

      </style>

      

      <div class="border"></div>

        

    

</div>

    

    

   
</div>