<?php
$this->title = 'Post a Job';

use yii\helpers\Url;
use yii\helpers\Html;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<div id="wrapper"><!-- start main wrapper -->
		<div class="inner_page">
			 
						<div class="container" id="small_img">
							<div class="row main" id="register">
							 
							<div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
								  <h2>
								     <a href="<?= Url::toRoute(['site/postajoboffline','pf'=>0])?>"> 
								      <img class="normal" src="<?=$imageurl;?>images/post_candidate.png">
								   	  <img class="img_white" src="<?=$imageurl;?>images/post_candidate_white.png"> 
								    </a> 
								  </h2>
								 
							</div>
							<div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
								  <h2> <a href="<?= Url::toRoute(['site/postajoboffline','pf'=>1])?>">
										 <img class="normal"  src="<?=$imageurl;?>images/post_campus.png">
										 <img class="img_white" src="<?=$imageurl;?>images/post_campus_white.png">
										 </a> 	
								  </h2>
								 
							</div>
						</div>
             </div>
    </div>
		 <div class="clearfix"></div>
		<div class="border"></div>

