<?php
$this->title = 'Post Job on My Career Bugs';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\select2\Select2;
use common\models\AllUser;
use common\models\City;
use common\models\Cities;
use bupy7\cropbox\CropboxWidget;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use common\models\Industry;

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';

$state = $empd['State'];
$city = $empd['City'];

$emp = Yii::$app->session['Employerid'];
$alusr = AllUser::find()->where([
    'UserId' => $emp
])->one();
$ctyloc = City::find()->all();

foreach ($ctyloc as $cty) {
    $che[] = $cty->CityName;
    $keys[] = $cty->CityName;
}
$cj = array_combine($che, $keys);
// /echo "<pre>";print_r($cj);die();

 $industry = ArrayHelper::map(Industry::find()->where([
                'IsDelete' => 0
            ])
                ->orderBy([
                'IndustryName' => 'SORT_ASC'
            ])
                ->all(), 'IndustryId', 'IndustryName');
?>


<link href="css/jquery.dropdown.css" rel="stylesheet">

<style>
      div.employeRegistrationsSections {
        display: none;
    }
    .tox-statusbar__branding{display:none !important}
    #preview .form-group span{font-weight:normal}
    #preview .form-group label{font-weight:bold; font-size: 13px;}
    .inner_page{padding-top:40px;}

    .select2-container--default .select2-selection--multiple .select2-selection__choice{font-size: 12px;}
    .select2-results__option{font-size: 12px;}
    .form-group{font-size: 14px;}
    .mandatory {color: red;}
    #blue_caller{margin: 8px 0}
    #white_caller{margin: 0px 0;}

    .tox .tox-mbtn{height: 25px; font-size:13px}

    #tinymce pre{min-height:400px;}

    .find_a_job, .need_help{display:none !important;}
    .select2-container{width:100% !important;}
    @media only screen and (max-width: 767px) {
        .select2-container{width:100% !important;}
        #first_sec{margin:0 15px 10px 15px !important;}


        .tinymce-mobile-content-container{border:1px solid #dcdcdc;}

        .mob_mar{margin:0 15px !important; padding:20px 0px 0 0px !important;}
        .tinymce-mobile-content-container{width: 100% !important;
            height: 100% !important;
            background: none !important;
            border-radius: 0 !important;}

        .tinymce-mobile-content-tap-section{width: 100% !important;
            height: 100% !important;}

    }

</style>

<div id="wrapper">
    <!-- start main wrapper -->

    <div class="headline_inner" style="display:none">
        <div class="row">
            <div class=" container">
                <!-- start headline section -->
                <h2>
                    <?php
                    if ($pf == 0) {
                        echo "Post a Job";
                    } else {
                        echo "Post a Job For Campus";
                    }
                    ?>
                </h2>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- end headline section -->
    </div>


    <div class="inner_page" style="background:#f2f2f2">
        <div class="container">

            <div class="row main">
                <!--------------------job form------------------------>
                <div class="xs-12 col-sm-12 main-center" id="jobform">
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
                    <?php
                    if ($pf == 0) {?>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Job For</label>


                                <div class="panel panel-default">
                                    <div class="panel-heading sty" style="background:#6d136a !important">
                                        <div class="col-md-3">
                                            <a><h4 class="panel-title" style="font-size:13px">	 	<input style="float: left; width: 13px; height: 13px"
                                                                                                             type="radio" value="Candidate" name="PostJob[PostFor]" checked
                                                                                                             onclick="$('#teamvacancy').hide();$('#candidatevacancy').show();"
                                                                                                             class="form-control"> &nbsp; Candidate &nbsp; &nbsp;
                                                    <span class="checkmark"></span>
                                                </h4></a>
                                        </div>

                                        <div class="col-md-3">
                                            <a><h4 class="panel-title" style="font-size:13px"> 	<input style="float: left; width: 13px; height: 13px"
                                                                                                         type="radio" value="Team" name="PostJob[PostFor]"
                                                                                                         class="form-control"
                                                                                                         onclick="$('#teamvacancy').show();$('#candidatevacancy').hide();">
                                                    &nbsp; Team &nbsp; &nbsp;
                                                    <span class="checkmark"></span>
                                                </h4> </a>
                                        </div>

                                        <div class="col-md-6">
                                            <a><h4 class="panel-title" style="font-size:13px"> 	<input style="float: left; width: 13px; height: 13px"
                                                                                                         type="radio" value="Both" name="PostJob[PostFor]"
                                                                                                         class="form-control"
                                                                                                         onclick="$('#teamvacancy').hide();$('#candidatevacancy').show();">
                                                    &nbsp; Both ( Candidates & Team )
                                                    <span class="checkmark"></span>
                                                </h4> </a>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>


                        <div style="height: 0px" class="clear"></div>
                        <?php
                    } elseif ($pf == 1) {
                        ?>
                        <input
                                style="float: left; width: 20px; height: 10px; display: none;"
                                type="radio" value="Campus" name="PostJob[PostFor]"
                                class="form-control" checked="checked">
                        <?php
                    }
                    ?>


                    <div class="mob_mar" style="padding:20px 10px 0 10px; background:#fff; border:1px solid #dcdcdc">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Job Title <span class="mandatory">*</span></label> <input type="text" placeholder=" "
                                                                                                 name="PostJob[JobTitle]" required class="form-control">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Designation <span class="mandatory">*</span></label> <input type="text" placeholder=" "
                                                                                                   name="PostJob[Designation]" required class="form-control">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Job Location (City) <span class="mandatory">*</span></label>
                                <?php /*?>
								<div class="dropdown-sin-1">
									<select style="display: none" multiple placeholder="Select" id="lcslt"
										name="PostJob[Location][]" multiple="multiple" maxlength="5"
										maxlength="5">
										<?php foreach($cj as $city){ ?>

										<option value="<?php echo $city ?>"><?php echo $city?></option>
									<?php } ?>

									</select>
								</div>

								<?php */?>
                                <?php
                                /*echo $form->field($model, 'Location')->widget(kartik\select2\Select2::classname(), [
                                 'data' => $cj,
                                 'language' => 'en',
                                 'options' => ['multiple' => true,'placeholder' => 'Select from list ...','id'=>'lcslt','name' => 'PostJob[Location][]'],
                                 'pluginOptions' => ['maximumSelectionLength'=> 5],
                                 ])->label(false);*/
                                $cityData =  ArrayHelper::map(Cities::find()->all(), 'CityId', 'CityName');
                                ?>
                                <select class="questions-category form-control"
                                        id="naukari-location" name="PostJob[Location][]"
                                        multiple="multiple">
                                </select>
                                <p class="help-block help-block-error"></p>

                                <?php
                                /* $url = Url::toRoute(['getpreferredlocation']);
                                 echo $form->field($model, 'Location[]')->widget(Select2::classname(), [
                                        //'data' => ArrayHelper::map(Cities::find()->all(), 'CityId', 'CityName'),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Select any 5 cities','id'=>'lcslt','name' => 'PostJob[Location][]'],
                                        'pluginOptions' => [
                                           // 'allowClear' => true,
                                            'multiple' => true,
                                            'maximumSelectionLength' => 5,
                                            'ajax' => [
                                                'url' => $url ,
                                                'dataType' => 'json' ,
                                                'data' => new JsExpression( 'function(params) { return {q:params.term, page:params.page || 1}; }' )
                                            ] ,
                                            'escapeMarkup' => new JsExpression ( 'function (markup) { return markup; }' ) ,
                                            'templateResult' => new JsExpression ( 'function(product) { console.log(product);return product.CityName; }' ) ,
                                            'templateSelection' => new JsExpression ( 'function (subject) { return subject.CityName; }' ) ,
                                        ],
                                    ])->label(false); */?>


                            </div>

                        </div>
                        <?php
                        if ($pf == 0) {
                            ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Experience <span class="mandatory">*</span></label> <select
                                            class="questions-category form-control " tabindex="0"
                                            aria-hidden="true" name="PostJob[Experience]">
                                        <option  >Select</option>
                                        <option value="Fresher">Fresher</option>
                                        <option value="0-1">Below 1 Year</option>
                                        <?php
                                        for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
                                            $frn = $fr + 1;
                                            ?>
                                            <option
                                                    value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <!--<input type="checkbox" name="PostJob[IsExp]" value="1" /> &nbsp; Experience &nbsp; &nbsp;-->
                                    <input type="checkbox" name="PostJob[IsExp]" id="IsExp"
                                           value="2"
                                           onclick="if($(this).prop('checked')==true){$(this).prev().attr('disabled', 'disabled');}else{$(this).prev().removeAttr('disabled');}" />
                                    &nbsp; Both
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                        <div class="clear"></div>





                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Qualification <span class="mandatory">*</span></label>
                                <select class="questions-category form-control"
                                        id="naukari-qualification" name="PostJob[Qualifications][]"
                                        multiple="multiple" >
                                    <?php foreach ($topQualifications as $key => $qual) {?>
                                        <option value="<?=$qual;?>"><?=$qual;?></option>
                                    <?php }?>

                                </select>
                                <p class="help-block help-block-error"></p>
                            </div>

                        </div>



                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Course  </label>
                                <?php
                                if (isset($_GET['education']) && $_GET['education'] != '') {
                                    $education = $_GET['education'];
                                } else {
                                    $education = '';
                                }
                                ?>

                                <select class="questions-category form-control"
                                        id="naukari-specialization" name="PostJob[course][]"
                                        multiple="multiple">
                                </select>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group" style="margin-bottom:0">
                                <label>Specialization </label>
                                <select
                                        class="form-control" name="PostJob[SpecialQualification][]"
                                        id="specialskill" multiple="multiple">
                                </select>

                                <div id="allspecskill"
                                     style="width: 100%; margin-top: 5px; height: 5px; padding: 3px; font-size: 12px; color: #fff;">
                                </div>
                            </div>
                        </div>



                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Industry <span class="mandatory">*</span></label> <select
                                        class="questions-category form-control " tabindex="0"
                                        aria-hidden="true" name="PostJob[JobCategoryId]"
                                        id="jobcategory" required>
                                    <option value="">Select Industry</option>
                                    <?php
                                    foreach ($jobcategory as $key => $value) {
                                        ?>
                                        <option value="<?=$value->IndustryId;?>"><?=$value->IndustryName;?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

                        <div class="clear"></div>




                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Job Type <span class="mandatory">*</span></label> <select
                                        class="questions-category form-control " tabindex="0"
                                        aria-hidden="true" name="PostJob[JobType]">
                                    <option>Select  </option>
                                    <option value="Full Time">Full Time</option>
                                    <option value="Part Time">Part Time</option>
                                    <option value="Work From Home">Work From Home</option>
                                    <option value="Fresher">Fresher</option>
                                    <option value="Walkin">Walkin</option>
                                    <option value="Internship">Internship</option>
                                    <option value="Contract">Contract</option>
                                    <option value="Commission">Commission</option>
                                    <option value="Temporary">Temporary</option>
                                    <option value="Volunter">Volunter</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Salary  </label> <select
                                        class="questions-category form-control " tabindex="0"
                                        aria-hidden="true" name="PostJob[Salary]">
                                    <option>Select  </option>
                                    <option value="0 - 1.5 Lakhs">0 - 1.5 Lakhs</option>
                                    <option value="1.5 - 3 Lakhs">1.5 - 3 Lakhs</option>
                                    <option value="3 - 6 Lakhs">3 - 6 Lakhs</option>
                                    <option value="6 - 10 Lakhs">6 - 10 Lakhs</option>
                                    <option value="10 - 15 Lakhs">10 - 15 Lakhs</option>
                                    <option value="15 - 25 Lakhs">15 - 25 Lakhs</option>
                                    <option value="Above 25 Lakhs">Above 25 Lakhs</option>
                                    <option value="Negotiable">Negotiable</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <label>Other Salary Detail (Optional) </label> <input
                                        type="text" placeholder="Ex: Fooding, Accommodation, Travelling Expenses  etc" id="othersalary-disabled"
                                        name="PostJob[OtherSalary]" class="form-control">

                                <div id="allothersalary"
                                     style="width: 100%; margin-top: 5px; height:  5px; padding: 3px; font-size: 12px; color: #fff;">
                                </div>
                            </div>
                        </div>


                        <div class="clear"></div>

                    </div>


                    <div style="height:20px"></div>

                    <?php
                    if ($pf == 0) {
                        ?>

                        <div id="first_sec">
                            <h5 class="page-head">Select Role and Skills   </h5>

                            <div class="col-md-4 col-xs-12">


                                <div class="panel panel-default" style="background:none ; box-shadow:none; border:0px; margin:0">
                                    <div class="panel-heading sty" style="background:none !important; box-shadow:none; padding-top: 0; border: 0px;">
                                        <div class="col-md-6 no-pad-123">
                                            <a><h4 class="panel-title" style="font-size:12px;  font-weight: normal; color: #565656;">
                                                    <input type="radio" name="PostJob[CollarType]" class="role_type" checked="" value="blue"> Blue Collar
                                                    <span class="checkmark"></span>
                                                </h4></a>
                                        </div>

                                        <div class="col-md-6 no-pad-123">
                                            <a><h4 class="panel-title" style="font-size:12px; font-weight: normal; color: #565656;"><input type="radio" class="role_type" name="PostJob[CollarType]" value="white">  White Collar
                                                    <span class="checkmark"></span>
                                                </h4> </a>
                                        </div>

                                    </div>
                                </div>


                            </div>


                            <div class="clearfix"></div>




                            <!-- Blue Caller  Start----------------------------------------------------------->
                            <div id="blue_caller">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Role <span class="mandatory">*</span></label>

                                        <select
                                                class="questions-category form-control"
                                                name="PostJob[PositionId]" id="postroleid">
                                            <option value="">Select Role</option>
                                            <?php
                                            foreach ($position as $key => $value) {
                                                ?>
                                                <option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                    <div class="clear"></div>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="postskilldatalist"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- Blue Caller  End----------------------------------------------------------->


                            <div class="clear"></div>


                            <div id="white_caller" style="display:none;">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label> Job Category <span class="mandatory">*</span></label>
                                        <select class="questions-category form-control" name="PostJob[WhiteCategory]" tabindex="0" aria-hidden="true" id="white_callar_category">
                                            <option value = "">Select Category</option>
                                            <?php if(!empty($whiteCategories)){
                                                foreach($whiteCategories as $categoryId => $categoryName){?>
                                                    <option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
                                                <?php  }
                                            }?>

                                        </select>
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                </div>



                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label> Roles </label>
                                        <select class="questions-category form-control" name="PostJob[WhiteRole][]" id="white_callar_role" multiple="multiple"></select>
                                    </div>
                                </div>
                                <div class="clear"></div>

                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label> Skills</label>
                                        <select class="questions-category form-control" id="white_callar_skills" name="PostJob[WhiteSkills][]" multiple="multiple"></select>

                                    </div>
                                </div>



                            </div>

                            <div class="clear"></div>

                        </div>


                        <div class="clear"></div>


                        <?php
                    }
                    ?>


                    <?php if ($pf == 1) {/*?>

	<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="form-group">
							    	    	<label>Role</label>

							 <select
									class="questions-category form-control"
									name="PostJob[PositionId]" id="postroleid" required>
									<option value="">Select Role</option>
										<?php
											foreach ($position as $key => $value) {?>
												<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
										<?php } ?>
                                        </select>
								<p class="help-block help-block-error"></p>
							</div>
							<div class="clear"></div>

							<div class="col-md-12 col-xs-12">
						    	<div class="form-group" id="postskilldatalist"></div>
					     	</div>
							</div>


	<?php */}  ?>




                    <!-- 						<div class="col-md-12 col-sm-12 col-xs-12"> -->
                    <!-- 							<div class="form-group"> -->
                    <!-- 								<label>< ?php if($alusr->EntryType == 'HR') { echo 'HR';} else { echo 'Company'; } ?> Description</label> -->
                    <!-- 								<input type="text" name="PostJob[Description]" -->
                    <!-- 									value="< ?=$empd['CompanyDesc'];?>" readonly placeholder="" -->
                    <!-- 									class="form-control"> -->
                    <!-- 							</div> -->
                    <!-- 						</div> -->

                    <!-- 						<div class="col-md-6 col-sm-6 col-xs-12"> -->
                    <!-- 							<div class="form-group"> -->
                    <!-- 								<label>< ?php if($alusr->EntryType == 'HR') { echo 'HR';} else { echo 'Company'; } ?> Name</label> -->
                    <!-- 								<input type="text" name="PostJob[CompanyName]" -->
                    <!-- 									value="< ?=$empd['Name'];?>" placeholder="" class="form-control"> -->
                    <!-- 								<p class="help-block help-block-error"></p> -->
                    <!-- 							</div> -->
                    <!-- 						</div> -->



                    <style>
                        #first_sec{padding:0; margin:0 0 15px 0; background:#fff; border-bottom:1px solid #dcdcdc}
                        #first_sec .page-head  { color:#fff; background:#6d136a; text-align:left;padding:10px 20px;}

                    </style>

                    <div id="first_sec">
                        <h5 class="page-head">HR / Company Details  </h5>


                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Email Address <span class="mandatory">*</span></label> <input type="email" placeholder=""
                                                                                                     name="PostJob[Email]" value="<?=$empd['Email'];?>" required
                                                                                                     class="form-control">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Contact Person:</label> <input type="text" maxlength="100"
                                                                      id="ContactPerson" placeholder="" value="<?=$empd['ContactPerson'];?>"
                                                                      name="PostJob[ContactPerson]" class="form-control">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>HR Number:</label> <input type="text" maxlength="10"
                                                                 onkeypress="return numbersonly(event)"
                                                                 onblur="return IsMobileno(this.value);" id="MobileNo"
                                                                 placeholder="" value="<?=$empd['MobileNo'];?>"
                                                                 name="PostJob[Phone]" class="form-control">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Landline Number: </label> <input type="text" maxlength="15"
                                                                        onkeypress="return numbersonly(event)"
                                                                        id="LandlineNo"
                                                                        placeholder="" value=""
                                                                        name="PostJob[LandlineNo]" class="form-control">
                                <p class="help-block help-block-error"></p>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Website: </label> <input type="url" placeholder="Enter Url" id="post-job-url"
                                                                class="form-control" name="PostJob[Website]">
                                <p  style="display:none;color:red" id="website_error">Please check website url format.</p>
                                <p class="help-block help-block-error"></p>
                                <span class="help-block">e.g - https://www.google.com</span>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>No. of Vacancies: <span class="mandatory">*</span></label> <select
                                        class="questions-category form-control " tabindex="0"
                                        aria-hidden="true" name="PostJob[NoofVacancy]"
                                        id="candidatevacancy">
                                    <?php
                                    if ($pf == 0) {
                                        ?>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <?php
                                    }
                                    for ($v = 6; $v <= 50; $v ++) {
                                        ?>
                                        <option value="<?=$v;?>"><?=$v;?></option>
                                        <?php
                                    }
                                    ?>
                                </select> <select
                                        class="questions-category form-control " tabindex="0"
                                        aria-hidden="true" name="PostJob[NoofVacancyt]"
                                        id="teamvacancy" style="display: none;">
                                    <?php
                                    for ($v = 0; $v <= 50; $v = $v + 5) {
                                        $vnext = $v + 5;
                                        ?>
                                        <option
                                                value="<?=$v.'-'.$vnext;?>"><?=$v.'-'.$vnext;?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label>Job Shift: <span class="mandatory">*</span></label> <select
                                        class="questions-category form-control " tabindex="0"
                                        id="jbsft" aria-hidden="true" name="PostJob[JobShift]" required>
                                    <option>Select</option>
                                    <option value="Morning">Morning</option>
                                    <option value="Day">Day</option>
                                    <option value="Night">Night</option>
                                    <option value="Evening">Evening</option>
                                    <option value="Day/Evening/Night">Day/Evening/Night</option>
                                    <option value="Rotational">Rotational</option>
                                </select>
                                <p class="help-block help-block-error"></p>

                            </div>
                        </div>



                        <div class="clear"></div>
                    </div>



                    <div id="first_sec">
                        <h5 class="page-head">Description  </h5>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Job Description11 <span class="mandatory">*</span>
                                </label>
                                <?php
                                /*$form->field($model, 'JobDescription')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small ckeditor','language' => 'en_CA','clientOptions' => ['plugins' => ["searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);*/
                                echo $form->field($model, 'JobDescription')->textarea(['options' => ['rows' => 6],'class' => 'form-control ckeditor', 'id' => 'postjob-jobdescription'])->label(false);?>
                                <p class="help-block help-block-error"></p>

                            </div>

                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Job Specification: </label>
                                <?php /*$form->field($model, 'JobSpecification')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small','language' => 'en_CA','clientOptions' => ['plugins' => ["searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);*/


                                echo $form->field($model, 'JobSpecification')->textarea(['options' => ['rows' => 6],'class' => 'form-control ckeditor'])->label(false);?>
                            </div>

                        </div>


                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Technical Guidance: </label>
                                <?php /*$form->field($model, 'TechnicalGuidance')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small','language' => 'en_CA','clientOptions' => ['plugins' => ["searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);*/

                                echo $form->field($model, 'TechnicalGuidance')->textarea(['options' => ['rows' => 6],'class' => 'form-control ckeditor'])->label(false);?>

                            </div>


                        </div>
                        <div class="clear"></div>

                    </div>
                    
                    
                        <div id="first_sec" class="employeRegistrationsSections">
            					 <h5 class="page-head">Employers  Registrations</h5>
            					 
            					 	<div class="col-md-12 col-sm-12 col-xs-12"> 
                             				<div class="form-group"> 
                             					<label for="name" class="cols-sm-2 control-label"> Company Type  </label>
                             						<div class="cols-sm-10">
        								<div class="input-group">  
        									  <div class="radio float-left-widtth3" style="margin-top: -3px;">
        										  <input type="radio" value="Company" onclick="companyType('Company')" name="PostJob[EntryType]" checked>
        										  Company
        									  </div>
        									  <div class="radio float-left-widtth3">
        										  <input type="radio" value="Consultancy"  onclick="companyType('Consultancy')"  name="PostJob[EntryType]">
        										   Consultancy
        									  </div> 
        									  <div class="radio float-left-widtth3">
        										  <input type="radio" value="HR"  onclick="companyType('HR')" name="PostJob[EntryType]">
        										Individual HR
        									  </div> 
        								</div>
        							</div>
                             				</div>
        						    </div>
                				
        						    
            						 <div class="col-md-6 col-sm-6 col-xs-12"> 
                     					<div class="form-group"> 
                     					<label for="name" class="cols-sm-2 control-label"> Industry  </label>
                     			        <select name="PostJob[IndustryId]"  class="form-control bfh-states" required>
                            
        										<option selected="selected" value="0">- Select an Industry -</option>
        										<?php foreach($industry as $key=>$value){?>
        										<option value="<?php echo $key;?>"><?=$value;?></option>
        										<?php } ?>
        								</select>
                     		            </div>
                             		</div>
                             	
        						<?php
        							if(isset(Yii::$app->session['SocialName']) && Yii::$app->session['SocialName']!='')
        							   {
        								$name='value="'.Yii::$app->session['SocialName'].'" readonly';
        							   }
        							   else
        							   {
        								$name='';
        							   }
        							   if(isset(Yii::$app->session['SocialEmail'])  && Yii::$app->session['SocialEmail']!='')
        							   {
        								$email='value="'.Yii::$app->session['SocialEmail'].'" readonly';
        							   }
        							   else
        							   {
        								$email='';
        							   }
        							?>
        							
        							<div class="col-md-6 col-sm-6 col-xs-12"> 
                     					<div class="form-group"> 
                     						<label for="name" class="cols-sm-2 control-label clabelname"> Company Name  </label>
                     						<input type="text" class="form-control" <?=$name;?> name="PostJob[Name]" id="Name" required placeholder="Company Name"/>
                     					 </div>
                             		</div>
                             		
                             		
                             		    
                             		    	<div class="col-md-6 col-sm-6 col-xs-12"> 
                             				<div class="form-group"> 
                             					<label for="email" class="cols-sm-2 control-label"> Address</label>
                             						<div class="input-group">
                									<span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
                									<input type="text" class="form-control" required name="PostJob[Address]" id="Address"  placeholder="Address"/>
                									
                								</div>
                             				  </div>
                             		    </div>
                             		    
      
        							<div class="col-md-6 col-sm-6 col-xs-12"> 
                     					<div class="form-group"> 
                     						<label for="email" class="cols-sm-2 control-label">  Mobile No</label>
                     						<input type="text" onkeypress="return numbersonly(event)" onblur="return IsMobileno(this.value);" class="form-control" required name="PostJob[MobileNo]" id="MobileNo"  placeholder="Mobile No" maxlength="10"/>
                     					</div>
                					</div> 
                			
                			        <div class="col-md-6 col-sm-6 col-xs-12"> 
                     					<div class="form-group"> 
                     						<label for="email" class="cols-sm-2 control-label">  Contact No</label>
                     						<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
            									<input type="text" class="form-control" name="PostJob[ContactNo]" onkeypress="return numbersonly(event)" id="ContactNo"  placeholder="Contact No" autocomplete="off"/>
            								</div>
                     					</div>
                					</div> 
                     					
            					
            				
            					
            						<div class="col-md-6 col-sm-6 col-xs-12"> 
                     					<div class="form-group"> 
                     						<label for="password" class="cols-sm-2 control-label">Password</label>
                     							<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
            									<input type="password" required  class="form-control" name="PostJob[Password]" id="password"  placeholder="Enter your Password" autocomplete="off" />
            								</div>
                     					</div>
                					</div> 
            						
        						
        						    <div class="col-md-6 col-sm-6 col-xs-12"> 
                     					<div class="form-group"> 
                     						<label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                     							<div class="input-group">
            									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
            									<input type="password"  class="form-control" onblur="return ConfirmPassword(this.value);" name="confirm" id="confirmpassword"  placeholder="Confirm your Password"/>
            								</div>
                     					</div>
                					</div> 
                					
            						
    						
    								            <div class="col-md-6 col-sm-6 col-xs-12" style="display:none"> 
                         							<div class="form-group"> 
                         								<label>Country</label> <select 
                         									class="questions-category countries form-control " tabindex="0" 
                         									aria-hidden="true" id="countryId" name="PostJob[Country]"> 
                         									<option value="">Select Country</option> 
                         									<option value="India" countryid="101" selected="selected">India</option> 
                         								</select> 
                         							</div> 
                         						</div> 


                         						<div class="col-md-6 col-sm-6 col-xs-12"> 
                         							<div class="form-group"> 
                         								<label>State</label> <select 
                         									class="questions-category states form-control " tabindex="0" 
                         									aria-hidden="true" id="stateId" name="PostJob[State]" required> 
                         									<option value="">Select State</option> 
                         								</select> 
                         								<p class="help-block help-block-error"></p> 
                         							</div> 
                         						</div> 
                         						<div class="col-md-6 col-sm-6 col-xs-12"> 
                         							<div class="form-group"> 
                         								<label>City</label> <select 
                         									class="questions-category cities form-control " tabindex="0" 
                         									aria-hidden="true" id="cityId" name="PostJob[City]" required> 
                         									<option value="">Select City</option> 
                         								</select> 
                         								<p class="help-block help-block-error"></p> 
                         							</div> 
                         						</div> 
                         						
        					
        						                <div class="col-md-6 col-sm-6 col-xs-12"> 
                         							<div class="form-group"> 
                         					            <label for="confirm" class="cols-sm-2 control-label">Pincode  </label>
                         					            <div class="input-group">
                    									<span class="input-group-addon"><i class="fa fa-thumb-tack" aria-hidden="true"></i></span>
                    									<input type="text" class="form-control" maxlength="6" onkeypress="return numbersonly(event)" name="PostJob[PinCode]" id="Pincode" required  placeholder="Pincode"/>
                    								</div>
                         						    </div> 
                         						</div> 
                         						
                         						
                         						<div class="col-md-12 col-sm-12 col-xs-12"> 
                         							<div class="form-group">
                    							<label for="confirm" class="cols-sm-12 control-label">Description </label>
                    							<div class="cols-sm-12">
                    								<div class="input-group full">
                    									<textarea name="PostJob[CompanyDesc]" class="form-control textarea"></textarea>
                    								</div>
                    							</div>
                        						</div>
                         					</div>
                         						
            
                    						 
                    						   <div class="col-md-12 col-sm-12 col-xs-12"> 
                         							<div class="form-group"> 
                         							<label for="confirm" class="cols-sm-12 control-label"><input type="checkbox"  id="agreed"  >  </label>
                         								<div class="" style="position: absolute;top: 0px;left: 35px">
                    									I agreed to the Terms and Conditions  
                    							        </div>
                         							
                         							</div>
                    						    </div>
                    						
                    					
                    						<div class="col-md-6 col-sm-6 col-xs-12"> 
                         							<div class="form-group"> 
                         							 <?= $form->field($model, 'captcha')->widget(\frontend\components\recaptcha\ReCaptcha::className())->label(false); ?>
                         							</div>
                    						</div>
                    						
                    					
        				                    <div class="clear"></div>
						        </div>



                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="logo">Logo <span>(Optional)</span> <small>Max. file
                                    size: 8 MB.</small></label>
                            <div class="upload">
                                <?php

                                echo $form->field($docmodel, 'Doc')
                                    ->widget(CropboxWidget::className(), [
                                        'croppedDataAttribute' => 'crop_info'
                                    ])
                                    ->label(false);
                                ?>
                            </div>

                        </div>
                    </div>


                    <div class="clear"></div>
                    <div class="se">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="logo"> Is Walking </label>
                                <div class="upload">
                                    <input type="checkbox" id="iswalkin" value="1"
                                           name="PostJob[IsWalkin]"
                                           onclick="if($(this).prop('checked')==true){$('.walkinbox').show();$('.walkindate').prop('required',true);}else{$('.walkinbox').hide();$('.walkindate').removeAttr('required')}">
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 walkinbox"
                             style="display: none;">
                            <div class="form-group">
                                <label for="logo">From </label>
                                <div class="upload">
                                    <input type="text" class="form-control walkindate"
                                           id="WalkinFrom" name="PostJob[WalkinFrom]" readonly
                                           style="cursor: pointer; background: #fff;" autocomplete="off"
                                           placeholder="Date From" />
                                    <p class="help-block help-block-error"></p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 walkinbox"
                             style="display: none;">
                            <div class="form-group">
                                <label for="logo">To </label>
                                <div class="upload">
                                    <input type="text" class="form-control walkindate"
                                           id="WalkinTo" name="PostJob[WalkinTo]" readonly
                                           style="cursor: pointer; background: #fff;" autocomplete="off"
                                           placeholder="Date To" />
                                    <p class="help-block help-block-error"></p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 walkinbox"
                             style="display: none;">
                            <div class="form-group">
                                <label for="logo">Walkin Time From </label>
                                <div class="upload">
                                    <input type="text" class="form-control timepicker"
                                           name="PostJob[WalkinTimeFrom]" id="WalkinTimeFrom"
                                           style="cursor: pointer; background: #fff;" autocomplete="off"
                                           placeholder="Time From" />
                                    <p class="help-block help-block-error"></p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 walkinbox"
                             style="display: none;">
                            <div class="form-group">
                                <label for="logo">Walkin Time To </label>
                                <div class="upload">
                                    <input type="text" class="form-control timepicker"
                                           name="PostJob[WalkinTimeTo]" id="WalkinTimeTo"
                                           style="cursor: pointer; background: #fff;" autocomplete="off"
                                           placeholder="Time To" />
                                    <p class="help-block help-block-error"></p>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 walkinbox"
                             style="display: none;">
                            <div class="form-group">
                                <label for="logo">Venue </label>
                                <div class="upload">
                                    <input type="text" class="form-control " id="Venue"
                                           name="PostJob[Venue]"
                                           style="cursor: pointer; background: #fff;" autocomplete="off"
                                           placeholder="Venue" />
                                    <p class="help-block help-block-error"></p>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="logo"><input type="checkbox" value="1"
                                                     id="forresume"
                                                     onclick="if($(this).prop('checked')==true){$('#altemailbox').show();}else{$('#altemailbox').hide();}" /><span>
										Ask job seekers for resume</span></label>
                            <div class="upload" id="altemailbox" style="display: none;">
                                <p class="help-block help-block-error">Applications for this
                                    job will sent to the following email address</p>
                                <div class="form-group" id="additionalemail">
                                    <div id="additional1">
                                        <input type="text" class="form-control altemail"
                                               name="PostJob[AltEmail][]"
                                               style="cursor: pointer; background: #fff;"
                                               autocomplete="off" /> <span
                                                onclick="$('#additional1').remove();">X</span>
                                    </div>
                                </div>

                                <span onclick="addadditionalemail();"> + Add Additional Email</span>


                            </div>

                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-default btn-green"
                                    onclick="preview();">PREVIEW</button>
                        </div>
                    </div>


                </div>
                <!--------------------job form------------------------>
                <style>

                    #title_heading{  line-height: 23px;
                        font-size: 18px;
                        font-weight: bold;
                        color: #f16b22;
                        margin: 12px 0 15px 0;
                        border-bottom: 2px solid #f16b22;
                        display: inline-block;
                        padding: 0 0 5px 0;
                    }
                    .company-detail{border: 1px solid #cccccc;
                        position: relative;
                        overflow: hidden;
                        background-color: #FFF;}
                    .company-contact-detail {
                        width: 78%;
                        float: right;
                        padding: 10px;
                    }
                    .company-detail .company-img {
                        width: 20%;
                        padding: 10px;
                        float: left;
                    }


                    aside .company-detail .company-img{width:20%;padding:10px; float:left;}
                    .company-contact-detail  {width:78%; float:right;     padding: 10px;}
                    h2.title_heading{ line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;}
                    .heading_main_tilte12{   width: 95%;}
                    .company-contact-detail p{ font-size: 13px; margin: 0 0 7px 0;}
                    .company-contact-detail p strong{font-weight: bold; }
                    #margin-minus-top13{margin-top:-130px}
                    .company-contact-detail p i{margin-right:5px;}
                    strong.color_org1{color:#f16b22;}
                    #job-details{position:relative}
                    #job-details .apply-job{width: 123px; position:absolute; right:0px; top:0px;}
                    #job-details .click_box a {float: left;background: #6d136a;color: #fff;padding: 0px;width: 23px;height: 23px;display: block;border-radius: 100px;
                        font-size: 11px;line-height: 27px;text-align: center;margin: 10px 2px !important;}
                    #job-details a.share_btn{    width: 36px;float: left;margin: 0;color: #f16b22;background-color:#fff;font-size: 13px;line-height: 25px;}
                    #job-details .click_box i{margin:0px;}
                    .company-contact-detail p.post_date{float:right;  margin: -5px 0 0 0;font-size: 11px}
                    .heading-inner {
                        margin-bottom: 20px;
                        display: block;
                        width: 100%;
                        position: relative;
                        overflow: hidden;
                        margin-top: 0px;
                    }

                    .heading-inner .title {
                        background-color: #fff;
                        border-bottom: 2px solid #f16b22;
                        font-size: 20px;
                        font-weight: 600;
                        padding: 0 0 20px 0;
                        position: relative;
                        text-transform: capitalize;
                        float: left;
                        color: #333;
                        margin-bottom: 0;
                    }

                    #preview_logo img{border-radius: 200px; width: 90%;}
                    .help-block-error{ color:red; font-size:13px;}

                </style>


                <!------------preview----------------------------->
                <div class="xs-12 col-sm-12 main-center" id="preview" style="display: none;">

                    <div class="Full">
                        <div class="form-group">
                            <label>Job For: </label> <span id="preview_jobfor"></span>
                        </div>
                    </div>

                    <h5 class="page-head"><?php if($alusr->EntryType == 'HR') { echo 'HR';} else { echo 'Company'; } ?>  Details</h5>





                    <div class="company-detail">

                        <div class="company-img">

                            <span id="preview_logo"></span>

                        </div>

                        <div  class="company-contact-detail">




                            <div class="form-group" id="title_heading">
                                <label>  </label> <span id="preview_jobtitle"></span>
                            </div>
                            <div class="clear"></div>


                            <!--new-->


                            <div class="company-contact-detail" id="job-details" style="width:100%">


                                <p><strong><?php if($alusr->EntryType == 'HR') { echo 'HR';} else { echo 'Company'; } ?>  Name : </strong>  <span id="preview_companyname"><?php echo $empd['Name']; ?></span> </p>



                                <p> <strong> Company Details: </strong><span id="comanyDescriptions"><?php echo $empd['CompanyDesc']; ?></span></p>



                                <div style="clear:both; overflow:hidden">

                                    <p style="width:40%;float:left"><strong>Email:</strong> <span id="preview_email"></span> </p>

                                    <p style="width:60%;float:left"> <strong>Website:</strong> <span id="preview_website"></span> </p>

                                </div>



                                <div style="clear:both; overflow:hidden">

                                    <p style="width:40%;float:left"><strong>State:</strong> <span id="preview_state"><?=$state?></span>  </p>

                                    <p style="width:60%;float:left"> <strong> City:</strong><span id="preview_city"><?=$city?></span> </p>

                                </div>

                                <div style="clear:both; overflow:hidden">

                                    <p style="width:40%;float:left"><strong>Contact person / HR Number :</strong> <span id="preview_phone"></span> </p>
                                </div>


                                <div style="height:1px ; background:#cccccc; margin:5px 0 10px 0;"></div>

                                <p>    <strong class="color_org1"><i class="fa fa-location-arrow"></i> Job Location: </strong>  <span id="preview_location"></span>  </p>



                                <div style="clear:both; overflow:hidden">

                                    <div class="blue-collar-preview">
                                        <p style="width:40%;float:left">    <strong class="color_org1"><i class="fa fa-calendar"></i> Role: </strong></label> <span id="preview_role"></span></p>
                                        <p style="width:60%;float:left">  <strong class="color_org1"> Skill: </strong> 	<span id="preview_keyskill"></span>
                                        </p>
                                    </div>
                                    <div class="white-collar-preview" style="display:none;">
                                        <p style="width:40%;float:left"><strong class="color_org1"><i class="fa fa-calendar"></i> Category: </strong></label> <span id="preview_white_category"></span></p>
                                        <p style="width:60%;float:left">  <strong class="color_org1"> Role: </strong> 	<span id="preview_white_role"></span>
                                        </p>
                                        <p style="width:60%;float:left">  <strong class="color_org1"> Skills: </strong> 	<span id="preview_white_skills"></span>
                                        </p>
                                    </div>
                                    <p>    <strong class="color_org1"><i class="fa fa-bars"></i> Qualification  : </strong>  <span id="preview_qualification"></span> </p>
                                    <p>    <strong class="color_org1"><i class="fa fa-bars"></i> Course  : </strong>  <span id="preview_course"></span> </p>
                                    <p>    <strong class="color_org1"><i class="fa fa-bars"></i> Specialization  : </strong>  <span id="preview_specialization"></span> </p>
                                    <p>    <strong class="color_org1"><i class="fa fa-bars"></i> Designation  : </strong>  <span id="preview_designation"></span> </p>


                                </div>
                            </div>
                            <!--new end-->


                        </div>

                    </div>


                    <div class="clearfix height-20"></div>



                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Job Category :</label> <span id="preview_jobcategory"></span>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12" id="expprev">
                        <div class="form-group">
                            <label>Experiencce : </label> <span id="preview_experience"></span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Salary : </label> <span id="preview_Salary"></span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Other Salary Detail (Optional) :</label>
                            <span id="preview_othersalary"></span>
                        </div>
                    </div>



                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Job Type :</label> <span id="preview_jobtype"></span>
                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Special Qualification (Optional) :</label> <span
                                    id="preview_specialqualification"></span>
                        </div>
                    </div>





                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>No. of Vacancies :</label> <span
                                    id="preview_noofvacancies"></span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label>Job Shift :</label> <span id="preview_jobshift"></span>
                        </div>
                    </div>





                    <div class="col-md-12 col-sm-12 col-xs-12" id="preview_walkin">
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Walk In :</label>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>From :</label> <span id="preview_walkinfrom"></span>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>To :</label> <span id="preview_walkinto"></span>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Time From :</label> <span id="preview_walkintimefrom"></span>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Time To :</label> <span id="preview_walkintimeto"></span>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Venue :</label> <span id="preview_venue"></span>
                            </div>
                        </div>

                    </div>



                    <div class="clear"></div>

                    <div class="clearfix height-20"></div>

                    <div class="job-desc job-detail-boxes">
                        <div class="heading-inner">   <p class="title">Job Description:</p>   </div>
                        <span id="preview_jobdescription"></span>
                    </div>


                    <div class="job-desc job-detail-boxes">
                        <div class="heading-inner">   <p class="title">Job Specification:</p>   </div>
                        <span id="preview_jobspecification"></span>
                    </div>

                    <div class="job-desc job-detail-boxes">
                        <div class="heading-inner">   <p class="title">Technical Guidance :</p>
                        </div>
                        <span
                                id="preview_technicalguidance"></span>
                    </div>
                    <div class="clear"></div>





                    <div class="col-md-6 col-sm-6 col-xs-12" id="preview_altemail">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Additional Email :</label> <span
                                        id="preview_additionalemail"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <button class="btn btn-default btn-green" type="button"
                                    onclick="$('#jobform').show();$('#preview').hide();">EDIT</button>
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="form-group">
                            <?= Html::submitButton('POST A JOB', ['class' => 'btn btn-default btn-green', 'id' => 'submit-post']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <!------------preview----------------------------->




            </div>
        </div>

    </div>
</div>
<div class="border"></div>
</div>

<script src="js/ckeditor/ckeditor.js"></script>

<link href="/assets/cc11e89d/themes/smoothness/jquery-ui.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>





<script type="text/javascript">

    $(document).ready(function(){
        /*setTimeout(function(){
            var loc = new locationInfo();
            $('#stateId').val('<?=$state;?>');
		var loc1='<?=$state;?>';
		 var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');
        if(stateId != '' || !stateId){
        loc.getCities(stateId);
        }

		setTimeout(function(){$('#cityId').val('<?=$city;?>');},2000);

	},8000);*/
        var data = [{id: '',text: 'Select course'}];
        var Ldata = [{id: '',text: 'Select Specialization'}];
        <?php
        $is_otrher = 0;
        foreach ($naukari_specialization as $key => $value) {
        if ($value == 'other' && $is_otrher == 1) {
            continue;
        }
        ?>
        var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
        data.push(d);
        <?php
        if ($value == 'other') {
            $is_otrher = 1;
        }
        }
        ?>
        <?php foreach ($NaukariQualData as $key => $value) { ?>
        var ds = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
        Ldata.push(ds);
        <?php }  ?>
        $("#naukari-specialization").select2({
            data: data,
            maximumSelectionLength: 5,
            placeholder: "Select a Course",
        });
        $("#specialskill").select2({
            data: Ldata,
            maximumSelectionLength: 5,
            placeholder: "Select a Specialization"
        });

        $("#naukari-qualification").select2({
            maximumSelectionLength: 5,
            placeholder: "Select Qualification"
        });

        $("#white_callar_role").select2({
            maximumSelectionLength: 5,
            placeholder: "Select Role"
        });

        $(document).on('change', '#white_callar_category', function(){
            var value = $(this).val();
            if(value != ""){
                $.ajax({
                    dataType : "json",
                    type : 'GET',
                    url : '<?=Url::toRoute(['getwhiteroles']);?>',
                    data : {
                        category_id : value
                    },
                    success : function(data) {
                        $('#white_callar_role').html("");
                        if(data != ""){
                            $.each(data, function(key, val) {
                                var option = $('<option />');
                                option.attr('value', key).text(val);
                                $('#white_callar_role').append(option);
                            });
                        }
                    }
                });
            }

        });

        $("#white_callar_skills").select2({
            maximumSelectionLength: 5,
            placeholder: "Select Skills",
            ajax: {
                url: '<?=Url::toRoute(['getwhiteskills']);?>',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                        q: params.term,
                        page: 1
                    }
                },
                processResults: function(data){
                    return {
                        results: $.map(data.results, function (item) {

                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });

        $("#naukari-location").select2({
            maximumSelectionLength: 5,
            placeholder: "Select Location",
            ajax: {
                url: '<?=Url::toRoute(['getpreferredlocation']);?>',
                dataType: 'json',
                type: "GET",
                data: function (params) {
                    return {
                        q: params.term,
                        page: 1
                    }
                },
                processResults: function(data){
                    return {
                        results: $.map(data.results, function (item) {

                            return {
                                text: item.CityName,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });



        $('.dropdown-sin-1').dropdown({
            readOnly: true,
            limitCount:5,
            input: '<input type="text" maxLength="5" placeholder="Search">'
        });

        $(document).on('click', '.role_type', function(){
            var value = $(this).val();
            if(value == "blue"){
                $('#blue_caller').show(0);
                $('#white_caller').hide(0);
            }else if(value == 'white'){
                $('#blue_caller').hide(0);
                $('#white_caller').show(0);
            }
        });

        $("body").on("submit", "#w0", function() {
            $(this).submit(function() {
                return false;
            });
            $('#submit-post').attr('disabled', true);
            return true;
        });

    });
</script>