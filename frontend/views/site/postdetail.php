<?php
$this->title = 'My Post Job';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use common\models\Education;
use common\models\PostJobWRole;
use common\models\PostJobWSkill;
use common\models\AllUser;
$employerData = AllUser::find()->where(['UserId' => $allpost->EmployerId])->one();
$loginUserId = (isset(Yii::$app->session['Employerid'])?Yii::$app->session['Employerid']:"");


$slug = (isset($allpost->Slug)?$allpost->Slug:"");

$jobUrl = Url::base().'/job/'.$slug;

$wapjobdesc =  'Job posted by ' . $allpost->CompanyName .  "<br>, Job location: " . $allpost->Location .  ", for more details visit: " . $jobUrl;
?>

<style>

.job-desc ul {     list-style-type: circle;padding-left: 20px;}
.marker{background:yellow;}

aside .company-detail .company-img{width:20%;padding:10px; float:left;}
.company-contact-detail  {width:78%; float:right;     padding: 10px;}
h2.title_heading{ line-height: 23px;font-size: 18px;font-weight: bold;color: #f16b22;margin: 12px 0 15px 0;border-bottom: 2px solid #f16b22;display: inline-block;padding: 0 0 5px 0;}
.heading_main_tilte12{   width: 95%;}
.company-contact-detail p{ font-size: 13px; margin: 0 0 7px 0;}
.company-contact-detail p strong{font-weight: bold; }
#margin-minus-top13{margin-top:-130px} 
.company-contact-detail p i{margin-right:5px;}
strong.color_org1{color:#f16b22;}
#job-details{position:relative}
#job-details .apply-job{width: 123px; position:absolute; right:0px; top:0px;}
#job-details .click_box a {float: left;background: #6d136a;color: #fff;padding: 0px;width: 23px;height: 23px;display: block;border-radius: 100px;
font-size: 11px;line-height: 27px;text-align: center;margin: 10px 2px !important;}
#job-details a.share_btn{    width: 36px;float: left;margin: 0;color: #f16b22;background-color:#fff;font-size: 13px;line-height: 25px;}
#job-details .click_box i{margin:0px;}
.company-contact-detail p.post_date{float:right;  margin: -5px 0 0 0;font-size: 11px}

.short-decs-sidebars li {padding: 10px 0;position: relative;overflow: hidden;border-bottom: 1px solid #cccccc;}
.short-decs-sidebars li div {  font-weight: normal;font-size: 12px;}
.short-decs-sidebars li h4 {margin: 0;font-weight: 600;font-size: 12px;}
aside {margin-bottom: 15px;}



</style>


	<div id="wrapper"><!-- start main wrapper -->
 
		<div class="headline_inner">
				<div class="row"> 
			       <div class=" container"><!-- start headline section --> 
						 <h2 style="opacity:0"> <?=$allpost->JobTitle;?>   </h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	</div>
	
		 
		<div class="inner_page">
			<div class="container">
	 
					 <div class="row">
                 <div class="col-lg-9 col-xs-12"> 
                   <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12" id="margin-minus-top13"> 
                   
				    <aside>
                                
                                <div class="company-detail">
                                    <div class="company-img">
										<?php
										if($allpost->docDetail)
										{
											$doc=$url.$allpost->docDetail->Doc;
										}
										else
										{
											$doc=$imageurl.'images/user.png';
										}
										?>
                                        <img src="<?=$doc;?>" class="img-responsive" alt="" style="border-radius: 200px;">
                                    </div>
                                    
                                    
           <div class="company-contact-detail" id="job-details"> 
		   
		   <div class="apply-job">
				<?php if(!empty($loginUserId) && $allpost->employer->UserId == $loginUserId){?>
				<a style="float:right; font-size:14px" href="<?= Url::toRoute(['site/jobedit','JobId'=>$allpost->JobId,'pf' => 0])?>"><i class="fa fa-edit"></i> Edit Job</a>
				<a href="javascript:void(0)" class="click_share share_btn">Share </a>

				<div class="click_box">
					<a
						href="https://www.facebook.com/sharer/sharer.php?u=<?= $jobUrl;?>"
						target="_blank"><i class="fa fa-facebook"></i> </a> <a
						href="https://api.whatsapp.com/send?text=<?=strip_tags($wapjobdesc)?>"
						style="line-height: 27px;" target="_blank"><i class="fa fa-whatsapp"></i> </a>

					<a href="javascript:;" onclick="mailtoemp('myModal_email');" 
					style="line-height: 26px;"><i class="fa fa-envelope-o"></i> 
					</a>  

				</div>

			</div>
 
                    	  
<div class="heading_main_tilte12">
	
	<h2 class="title_heading"> <?=$allpost->JobTitle;?></h2> 
	
	<?php }?>
</div>

	 
	     <p><strong><?=(isset($employerData->EntryType)?$employerData->EntryType:"");?>: </strong><?=$allpost->CompanyName;?></p>
	     
	     

        <p> <strong> <?=(isset($employerData->EntryType)?$employerData->EntryType:"");?> Details: </strong>



             <?=substr($allpost->Description,0,55).'...';?>



			<?php



			if(strlen($allpost->Description)>20){



				?>



				<a href="<?= Url::toRoute(['wall/searchcompany','userid'=>$allpost->EmployerId])?>" target="_blank" style="text-decoration: underline;">See More</a>



			<?php



			}



			?> </p>                                          

    

     <div style="clear:both; overflow:hidden">

    <p style="width:40%;float:left"><strong>Email:</strong> <?=$allpost->Email;?></p>  
    
    
 
  
    <p style="width:60%;float:left"> <strong>Website:</strong> <?=$allpost->Website;?> </p>

    </div>

 

    <div style="clear:both; overflow:hidden">

    <p style="width:40%;float:left"><strong>State:</strong> <?=$allpost->State;?>  </p>    

    <p style="width:60%;float:left"> <strong> City:</strong> <?=$allpost->City;?>  </p>

    </div>

    

    <div style="height:1px ; background:#cccccc; margin:5px 0 10px 0;"></div>

    <p>    <strong class="color_org1"><i class="fa fa-location-arrow"></i> Job Location: </strong>  <?=$allpost->Location;?>  </p>

    <div style="clear:both; overflow:hidden">
		<?php if($allpost->CollarType == "white"){?>
		
    	<p style="width:80%;float:left">  <strong class="color_org1">Job Category: </strong>  <?=$allpost->category->name;?>  </p>
		<div class="clear"></div>
		<p style="width:80%;float:left">  <strong class="color_org1">Roles: </strong>  
		<?php $whiteRoleList = ArrayHelper::map(PostJobWRole::find()->where([
										'postjob_id' => $allpost->JobId,
										'user_id' => $allpost->EmployerId
									])->all(), 'role_id', 'role'); ?>
					<?php echo (!empty($whiteRoleList)?implode(', ',$whiteRoleList):"");?>
		</p>
		<div class="clear"></div>
		<p style="width:80%;float:left">  <strong class="color_org1">Skills: </strong>  
		
		<?php $whiteSkillList = ArrayHelper::map(PostJobWSkill::find()->where([
															'postjob_id' => $allpost->JobId,
															'user_id' => $allpost->EmployerId
														])->all(), 'skill_id', 'skill'); ?>
										<?php echo (!empty($whiteSkillList)?implode(', ',$whiteSkillList):"");?>
		</p>
		<div class="clear"></div>

    

<div class="clear"></div>

		
		<?php }else{ ?>
		
			 <p  style="width:40%;float:left">    <strong class="color_org1"> Role: </strong> <?=$allpost->position->Position;?></p> 
<div class="clear"></div>



     <p style="width:60%;float:left">  <strong class="color_org1"> Skill: </strong> 	<?php



											$jskill='';



											foreach($allpost->jobRelatedSkills as $k=>$v)



											{



											$jskill.=$v->skill->Skill.' , ';



											}



											echo trim(trim($jskill),",");



											?>

											</p>
		
		<?php }?>


    
											<div class="clear"></div>
								
							 			

    </div>


  <p class="post_date">Posted On:  <?=date('d M, Y',strtotime($allpost->OnDate));?> </p>
  
  
  
  
  
  
  
                                    </div>



                                </div>



                            </aside>



			  					
							
         </div>                           
                                  
                               
                               
                               
                               
                               
                               
                               
                               
                               
                               
                 
                        <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">




		<div class="company-detail" style="padding:15px 20px 12px 20px;margin-bottom:20px">  
		    <div class="heading-inner" style="margin-bottom: 7px;"><p class="title" style="padding-bottom:8px">Education  </p> </div>




         <div class="widget" style="border:0px;margin:0px;padding:0px"> 
                     <ul class="short-decs-sidebars"> 
	                      		<li>
                            	<div class="col-3  col-md-3 col-sm-4 col-xs-12" style="padding:0px"> <h4><stong style="color:#f16b22">Highest Qualifcation:</stong></h4> </div>
                                <div class="col-9  col-md-9 col-sm-8 col-xs-12"> <?=($allpost->qualifications=='')?'Not Mentioned':$allpost->qualifications;?></div>
                                
							</li>  
						
							<li>
						    	<div class="col-3  col-md-3 col-sm-4 col-xs-12" style="padding:0px"> <h4><stong style="color:#f16b22">Course:</stong></h4> </div>
								<div class="col-9  col-md-9 col-sm-8 col-xs-12"> <?=($allpost->course=='')?'Not Mentioned':$allpost->course;?>	</div>
						 	</li> 
						 
				
							<li>
								<div class="col-3  col-md-3 col-sm-4 col-xs-12" style="padding:0px"> <h4><stong style="color:#f16b22">Specific Qualification:</stong></h4> </div>
								<div class="col-9  col-md-9 col-sm-8 col-xs-12"> <?=($allpost->SpecialQualification=='')?'Not Mentioned':$allpost->SpecialQualification;?></div>
							</li> 
					  </ul>  				
		      </div> 				
	    	</div>

    


                            <div class="single-job-page-2 job-short-detail">



                                <div class="heading-inner">



                                    <p class="title">Job Description</p>



                                </div>



                                <div class="job-desc job-detail-boxes">



                                    



                                        <?=htmlspecialchars_decode($allpost->JobDescription);?>



                                   



                                    <div class="heading-inner">



                                        <p class="title">Job Specification:</p>



                                    </div>


                                        <?=!empty($allpost->JobSpecification)?htmlspecialchars_decode($allpost->JobSpecification):"N/M";?>



                                    







                                    <div class="heading-inner">



                                        <p class="title">Technical Guidance:</p>



                                    </div>



                                    



									<?=!empty($allpost->TechnicalGuidance)?htmlspecialchars_decode($allpost->TechnicalGuidance):"N/M";?>



									



									<?php



									if($allpost->IsWalkin==1)



									{



									?>



									<div class="heading-inner">



                                        <p class="title">Walk-In Details:</p>



                                    </div>



									<p >



										Venue :  <?=$allpost->Venue;?><br/>



										Interview Date & time : <?=date('dS M Y',strtotime($allpost->WalkinFrom)).' - '.date('dS M Y',strtotime($allpost->WalkinTo));?> between <?=$allpost->	WalkinTimeFrom;?> to <?=$allpost->WalkinTimeTo;?>



										<br/>



									</p>



									<?php



									}



									?>



                                </div>







                            </div>



                        </div>

   </div>

						
              
                               
                               
                               
                               
							
                            <div class="widget">
                                <div class="widget-heading"><span class="title">Short Description</span></div>
                                <ul class="short-decs-sidebar">
									
								  <li>
                                        <div>
                                            <h4>HR Name:  </h4></div>
                                        <div><i class="fa fa-user"></i> <?=$allpost->ContactPerson;?></div>
                                    </li>
								
						 
								  <li>
                                        <div>
                                            <h4>Designation  : </h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->Designation;?> </div>
                                    </li>
									   
							
								
									<?php
									if($pf==0)
									{
										?>
                                    <li>
                                        <div>
                                            <h4>Job Type:</h4></div>
                                        <div><i class="fa fa-bars"></i><?=$allpost->JobType;?></div>
                                    </li>
									
                                    <li>
                                        <div>
                                            <h4>Job Experience:</h4></div>
                                        <div><i class="fa fa-clock-o"></i><?php if($allpost->Experience!='Fresher'){ echo $allpost->Experience.' Years';}else{echo $allpost->Experience;}?></div>
                                    </li>
									<?php
									}
									?>
									<li>
										<div>
											<h4>Eligibility:</h4>
										</div>
										<div>
											<i class="fa fa-bars"></i>
										<?php
										if($allpost->IsExp==0){echo "Fresher";}
										elseif($allpost->IsExp==1){echo "Experience";}
										elseif($allpost->IsExp==2){echo "Both - Fresher & Experienced";}
										?>
										</div>
									</li>
								 
									  <li>
                                        <div>
                                            <h4>Industry:</h4></div>
                                        <div><i class="fa fa-industry"></i><?=$allpost->jobCategory->IndustryName;?> </div>
                                    </li>
                                    <li>
                                        <div>
                                            <h4>Job Shift:</h4></div>
                                        <div><i class="fa fa-calendar"></i><?=$allpost->JobShift;?> </div>
                                    </li> 
                                    
                                 
                                    
                                    
                                    <li>
                                        <div>
                                            <h4>Expected Salary:</h4></div>
                                        <div><i class="fa fa-rupee"></i><?=$allpost->Salary;?> / Annum</div>
                                    </li>
									<li>
                                        <div>
                                            <h4>Other Salary:</h4></div>
                                        <div><i class="fa fa-rupee"></i>
										<?=($allpost->OtherSalary=='')?'Not mentioned':$allpost->OtherSalary;?>
										</div>
                                    </li>
									<li>
                                        <div>
                                            <h4>No Of Vacancy:</h4></div>
                                        <div><i class="fa fa-user"></i><?=$allpost->NoofVacancy;?></div>
                                    </li>
                                    
                                    
                                     
                                </ul>
                            </div>
                        <?php
						if($allpost->JobStatus==0)
						{
						?>
                            <div class="apply-job" onclick="closestatus(<?=$allpost->JobId;?>);">
                                  <a>  <i class="fa fa-trash-o"></i>Close Status</a>
                            </div> 
                        <?php
						}
						?>
                    </div>
                        <div class="col-lg-9  col-md-8 col-sm-8 col-xs-12" style="display:none">
                            <div class="single-job-page-2 job-short-detail">
                                <div class="heading-inner">
                                    <p class="title">Job Description</p>
                                </div>
                                <div class="job-desc job-detail-boxes">
                                    
                                        <?=htmlspecialchars_decode($allpost->JobDescription);?>
                                    
                                    <div class="heading-inner">
                                        <p class="title">Job Specification:</p>
                                    </div>
                                    
                                        <?=htmlspecialchars_decode($allpost->JobSpecification);?>
                                    
                                    <div class="heading-inner">
                                        <p class="title">Technical Guidance:</p>
                                    </div>
                                    <?=htmlspecialchars_decode($allpost->TechnicalGuidance);?>
									
									<?php
									if($allpost->IsWalkin==1)
									{
									?>
									<div class="heading-inner">
                                        <p class="title">Walk-In Details:</p>
                                    </div>
									<p >
										Venue :  <?=$allpost->Venue;?><br/>
										Interview Date & time : <?=date('dS M Y',strtotime($allpost->WalkinFrom)).'-'.date('dS M Y',strtotime($allpost->WalkinTo));?> between <?=$allpost->	WalkinTimeFrom;?> to <?=$allpost->WalkinTimeTo;?>
										<br/>
									</p>
									<?php
									}
									?>
                                </div>

                            </div>
                        </div>
                      
                </div>
				  

      </div>


 
		    </div>
		 
		
		
		
		<div class="border"></div>