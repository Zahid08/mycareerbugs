<?php

$this->title = 'Profile';



use yii\helpers\Url;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use common\models\Skill;
use common\models\EmployeeSkill;

use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;

$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

use common\models\AllUser;
use common\models\City;
$city_name = City::find()->where(['CityId'=>explode(',', $profile->PreferredLocation)])->all();
$alluser=new AllUser();

?>
<style>
    .cv { 
    right: 22% !important;
    top: -4px;}
</style>
<div class="profile_main_cover_photo">
<img class="full_width no_mbl" src="<?=$imageurl;?>images/background-main.jpg">
<img class="full_width only_mbl" src="<?=$imageurl;?>images/background-main1.jpg">
 </div>

<div id="wrapper"><!-- start main wrapper -->



		<div class="inner_page second">

			<div class="container">

			  <div  id="profile-desc" >
			      
			      	   <div class="col-md-12 col-sm-12 col-xs-12"  id="margin-top-profile-minus">
			      	
			      	   <div class="profile_block_main">
		                  <div class="profile_block_left">
		                      <img src="<?=Yii::$app->session['EmployeeDP'];?>" alt="" class="img-responsive center-block" style="width:100px;height:100px"> 
		                  </div>
		              <div class="profile_block_right">
		                  
                                <div class="main_button"> 
									<a href="<?= Url::toRoute(['site/editprofile'])?>">  <i class="fa fa-pencil-square-o"></i> Edit Profile</a></p>
 </div>
<div class="main_button1"><?php
	$linkname=($profile->CVId!=0)?'Attached Resume':'Upload CV';?>
	<a data-toggle="modal" data-target="#myModal_cvupload" href="">  <i class="icon-upload"></i> <?=$linkname;?></a></div> 
                        <h3  class="title_heading"><?=$profile->Name;?></h3>  
                        <div class="clear"> </div>
                        <div class="profilehalf">  <p> <strong>Phone: </strong> +91 <?=$profile->MobileNo;?> </p> 
                        <p><strong>Email:</strong> <?=$profile->Email;?>  
                        <p> <strong>Languages:</strong>  <?=$profile->language;?> </p>
                        </div>
                        <div class="profilehalf">    <p><strong>WhatsApp:</strong>+91   <?=$profile->whatsappno;?></p> </p>
                        <p><strong>Gender:</strong>  <?=$profile->Gender;?> </p> </div>
                            
  <div class="clear"> </div>
  
		                  </div>
		                      <div class="clear"> </div>
		             </div>
		                 
		                 
		                       <div class="profile_block_main"> 
		                    <h3 class="title_heading">Prefered Job Location </h3>
		                    <p>   <?=$profile->getPreferedJobLocation();?> 
                        </p>
		                    </div>
		                    
		                    
		              <div class="profile_block_main"> 
		                    <h3 class="title_heading"> About Us  </h3>
		                       <p> <?=$profile->AboutYou;?></p>
		                    </div>
		                    
		                    
		                 
		         
		               
		              <div class="profile_block_main"> 
		                    <h3 class="title_heading"> Address  </h3>
		                    
                                <p> <strong> Address:</strong> <?=$profile->Address;?>  </p>
                                <div class="profilehalf">  <p> <strong> City:</strong> <?=$profile->City;?>  </p>
                                 <p> <strong>State:</strong> <?=$profile->State;?>  </p> </div>
                                 <div class="profilehalf">  <p> <strong>Country:</strong> <?=$profile->Country;?>  </p>
                                <p> <strong>Pincode:</strong> <?=$profile->PinCode;?> </p> </div>
		                       <div class="clear"> </div>
		                    </div>
		                    
		                
		                
		                    
		              <div class="profile_block_main"> 
		                    <h3 class="title_heading"> Educational Information  <span style="display: none;"><?=($profile->educations[0]->DurationTo);?></span>  </h3>
		                  
		                     <div class="resume-box" style="margin-top: 15px;" id="new_profile_design"> 
								<div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">  <i class="fa fa-calendar" aria-hidden="true"></i>  </div>

                                        <div class="insti-name"> <h4>Pass out Year</h4>  </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$profile->educations[0]->DurationTo;?></h4>

                                             </div>

                                    </div>

                                </div>

                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                           <i class="fa fa-graduation-cap" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Higest Qualification</h4>

                                           

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$profile->educations[0]->nqual->name;?></h4>

                                             </div>

                                    </div>

                                </div>
                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                           <i class="fa fa-plus-square-o" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">
                                            <h4>Course</h4>
                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$profile->educations[0]->ncourse;?></h4>

                                             </div>

                                    </div>

                                </div>
                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                           <i class="fa fa-file-text" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Specialization</h4>

                                           

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">
                                            <h4><?// =$profile->educations[0]->specialization->name;?></h4>
                                            <h4><?=$profile->educations[0]->specialization_id;?></h4>

                                             </div>

                                    </div>

                                </div>

                                <div class="row education-box" style="display: none;">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                            <i class="fa fa-files-o" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Course</h4> 

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$profile->educations[0]->course->name;?></h4>

                                             </div>

                                    </div>

                                </div>

                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                              <i class="fa fa-briefcase" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>University / College</h4>

                                           

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$profile->educations[0]->University;?></h4>

                                             </div>

                                    </div>

                                </div>
	
								<?php 
									if($profile->CollarType == 'white'){ ?>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Job Category</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $jobCategory = ArrayHelper::map(UserJobCategory::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'category_id', 'category'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($jobCategory)?implode(', ',$jobCategory):"");?></h4>
												</div> 
											</div>
										</div>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Role</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $jobRoles = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'role_id', 'role'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($jobRoles)?implode(', ',$jobRoles):"");?></h4>
												</div> 
											</div>
										</div>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Skills</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $jobSkills = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'skill_id', 'skill'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($jobSkills)?implode(', ',$jobSkills):"");?></h4>
												</div> 
											</div>
										</div>
										
										
										
								<?php }else{
								
								
									$skill = array();
									$roles = array();
									foreach($profile->empRelatedSkills as $ask=>$asv)

									{
										if($asv->skill->position->Position && !in_array($asv->skill->position->Position, $roles)){
											$roles[] = $asv->skill->position->Position;
										}
										$skill[$asv->skill->SkillId] = $asv->skill->Skill;
										

									}
								?>
								<div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                             <i class="fa fa-file-text" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name"> 
                                            <h4>    Role</h4> 
                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                                
                                            <h4 style="line-height:18px;"><?=implode(',', $roles);?></h4>

                                             </div> 
                                    </div>
                                    
                                </div>
								
								
								

								 <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                             <i class="fa fa-file-text" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name"> 
                                            <h4> 	Skills</h4> 
                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

												<?php


												?>

                                            <h4 style="line-height:18px;"><?=implode(',',$skill);?></h4>

                                             </div> 
                                    </div>

                                </div>
                                
                                
							<?php }?> 
                                
                                
                                
                            </div> 
		                
		                 
		               
		               <div class="clear"> </div>
		                 </div>

				    <div class="profile_block_main"> 
				    
				    
				 	<?php

				if($profile->experiences)

				{

						foreach($profile->experiences as $k=>$val)

						{

				?>
				
		                    <h3 class="title_heading"> Work Experience  <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span> </h3>
		                  
		                     <div class="resume-box" style="margin-top: 15px;" id="new_profile_design1"> 
								<div class="row education-boxs">
  
				<div class="col-md-12 col-sm-12 col-xs-12">
 

                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                           <i class="fa fa-file-text" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Company name</h4>

                                           

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$val->CompanyName;?></h4>

                                             </div>

                                    </div>

                                </div>
								
								<?php if($profile->CollarType == "white"){?>
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												 <i class="fa fa-file-text" aria-hidden="true"></i>
											</div>
											<div class="insti-name"> 
												<h4>Job Category</h4> 
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<?php $CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $profile->UserId,
																	'experience_id' => $val->ExperienceId
																])->all(), 'category_id', 'category_id');
													  $expCategory = WhiteCategory::find()->where([
																	'id' => $CategoryIds
																])->one();
																?>
												<h4 style="line-height:18px;"><?php echo (!empty($expCategory->name)?$expCategory->name:"");?></h4>
											</div> 
										</div>
									</div>
									
									<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Role</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $profile->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'role_id', 'role'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($expRoles)?implode(', ',$expRoles):"");?></h4>
												</div> 
											</div>
										</div>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Skills</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $profile->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'skill_id', 'skill'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($expSkills)?implode(', ',$expSkills):"");?></h4>
												</div> 
											</div>
										</div>
									
								
								
								
								<?php }else{?>

                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                            <i class="fa fa-files-o" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Role</h4> 

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$val->position->Position;?></h4>
                                            
                                            
                                            

                                             </div>

                                    </div>

                                </div>
                                
                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                             <i class="fa fa-file-text" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name"> 
                                            <h4> 	Skills</h4> 
                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

												<?php 
													/*Get Experience Skill*/
													$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
														'IsDelete' => 0,
														'SkillRoleId' => $val->PositionId,
														'UserId' => $profile->UserId,
														'ExperienceId' => $val->ExperienceId
													])->all(), 'Employeeskillid', 'SkillId');
													
													$skills = ArrayHelper::map(Skill::find()->where([
														'IsDelete' => 0,
														'SkillId' => $employeeSkill
													])->all(), 'SkillId', 'Skill');
													
												?>
											
											
												<h4><?php echo $skills ? implode(", ",$skills) : "";?></h4>

                                             </div> 
                                    </div>

                                </div>
                                
                                <?php }?>
                                <div class="row education-box">
        							<div class="col-md-4 col-xs-12 col-sm-4">
        								<div class="resume-icon">
        									<i class="fa fa-files-o" aria-hidden="true"></i>
        								</div>
        								<div class="insti-name">
        									<h4>Position</h4>
        								</div>
        							</div>
        							<div class="col-xs-12 col-md-8 col-sm-8">
        								<div class="degree-info">
        									<h4><?=$val->PositionName;?></h4>
        								</div>
        							</div>
        						</div>

								<div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                            <i class="fa fa-files-o" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Industry</h4> 

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">
											<h4><?=isset($val->industry->IndustryName) ? $val->industry->IndustryName : 'Not Set';?></h4>

                                             </div>

                                    </div>

                                </div>

                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                              <i class="fa fa-briefcase" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Experience</h4>

                                           

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

												<?php

												$expy=$val->Experience;

												?>

                                            <h4> <?=!empty($expy)?$expy.' Years':"";?></h4>

                                             </div>

                                    </div>

                                </div>

								

								<div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                              <i class="fa fa-briefcase" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Salary</h4>

                                           

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">
                                            <h4> <?=(!empty($val->Salary)?$val->Salary.' Lakh':"");?></h4>

                                             </div>

                                    </div>


								

								  

                            </div>

                        </div>

                            </div> 
                        </div>  
  

			  <?php

						}

				}

				?>

                               
                            
                            
		     
		                
		                </div> 
		                
		                
		                
			<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side" style="display:none">
				 <div class="widget-heading"><span class="title">Recommended Jobs     </span></div>	
				 <div class="spacer-5"></div> 
						  <div class="rsw"> 
                            <aside> 
                                <div class="widget">
                                    
                                    <ul class="related-post">
                                        <?php
                    if($alljob)
                    {
                    foreach($alljob as $jkey=>$jvalue)
                    {
                    ?>
                                        <li>
                                            <a href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>"><?=$jvalue->JobTitle;?> </a>
                                            <span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?></span>
                                            <span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
                                            <span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?></span>
                                        </li>
                    <?php
                        }
                        }
                        ?>
				 	<a href="<?= Url::toRoute(['site/recentjob'])?>" class="sidebar_view_all"> View all  </a>
										 
										
                                    </ul>
                                </div>

                            </aside>
                        </div>
			</div>
			
			
			
			
				 
				  </div>    </div> 
		            
				       
		

			  </div>

            </div>

       </div>

		 
               </div> 
		               
						 
    </div> 
		

		

		

		<div class="border"></div>

</div>







<div class="modal add-resume-modal" id="myModal_cvupload" tabindex="-1" role="dialog" aria-labelledby="">

            <div class="modal-dialog modal-md" role="document">

				<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30']]); ?>

                <div class="modal-content">

                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                        <h4 class="modal-title" id="myModalLabel">Quick Review and update your resume</h4>

                    </div>

					 <div class="modal-body">

                        <div class="input-group image-preview form-group ">

						<label style="width: 100%;float: left;">Uploaded Resume</label>

							<?php

									if($profile->CVId!=0)

									{

										$na=explode(" ",$profile->Name);

									$name=$na[0];

									$extar=explode(".",$profile->cV->Doc);

									$ext=$extar[1];

										?>

							<a href='<?=$url.$profile->cV->Doc;?>' class="cv" download="<?=$name.'.'.$ext;?>"><?=$profile->Name;?> Resume</a><br/><br/>

						<?php

									}

									?>

						<label>Change Resume ?</label>

					<br>	Upload Updated CV <?= $form->field($profile, 'CVId')->fileInput(['required'=>true,'accept'=>'.doc, .docx,.rtf,.pdf','id'=>'cvid'])->hint('doc,docx,pdf,rtf - 2MB max')->label(false) ?>

                        </div>

					 </div>

				 <div class="modal-footer">

						<?= Html::submitButton('Upload and Update', ['name'=>'jobalertsearch','class' => 'btn btn-lg btn-primary btn-block']) ?>



                    </div>

                </div>

				<?php ActiveForm::end(); ?>

            </div>

	</div>