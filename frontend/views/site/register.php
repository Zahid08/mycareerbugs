<?php
$this->title = 'Register - My Career Bugs';

use yii\helpers\Url;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>


 <meta name="keywords" content="My Career Bugs, MCB, Employers Registration, Company Registration, Team Registration, Candidate Registration, Job Seekers Registration" /> 

 <meta name="description" content="Register and head to the new way of Hiring. Post Jobs. Get Hired. Post Team Requiremets. Hiring made easy!" />  

 <div id="wrapper"><!-- start main wrapper -->
 
	  
		<div class="headline_inner">
				 
			       <div class=" container"><!-- start headline section --> 
						 <h2>Register   </h2>
						<div class="clearfix"></div>
					</div>
			</div><!-- end headline section -->
    	 
	
		 
		<div class="inner_page">
			 
						<div class="container" >
							<div class="row main" id="register">
							 
							<div class="col-lg-3  col-md-3 col-sm-3 col-xs-6">
								  <h2>
								     <a  href="<?= Url::toRoute(['site/jobseekersregister'])?>" > 
								      <img class="normal" src="<?=$imageurl;?>images/job_seekers.png">
								   	  <img class="img_white" src="<?=$imageurl;?>images/job_seekers_white.png"> 
								    </a> 
								  </h2>
								 
							</div>

                                                      <div class="col-lg-3  col-md-3 col-sm-3 col-xs-6">
								  <h2>
								     <a  href="<?= Url::toRoute(['team/index'])?>" > 
								      <img class="normal" src="<?=$imageurl;?>images/team_orange.png">
								   	  <img class="img_white" src="<?=$imageurl;?>images/team_white.png"> 
								    </a> 
								  </h2>
								 
							</div>

                                              <div class="col-lg-3  col-md-3 col-sm-3 col-xs-6">
					               <h2> <a  href="<?= Url::toRoute(['site/employersregister'])?>" >
							   <img class="normal"  src="<?=$imageurl;?>images/company.png">
							   <img class="img_white" src="<?=$imageurl;?>images/company_white.png">
							 </a> 	
							</h2>  
					       </div>

                                               <div class="col-lg-3  col-md-3 col-sm-3 col-xs-6">
								  <h2>
								     <a  href="<?= Url::toRoute(['campus/campusregister'])?>" > 
								      <img class="normal" src="<?=$imageurl;?>images/campus_register.png">
								   	  <img class="img_white" src="<?=$imageurl;?>images/campus_register_white.png"> 
								    </a> 
								  </h2>
								 
							</div>

							
						</div>

             </div>
    </div>
		 <div class="clearfix"></div>
		<div class="border"></div>
</div>