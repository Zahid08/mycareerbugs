<?php
$this->title = 'Thankyou';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<div id="wrapper" style="min-height: 400px;"><!-- start main wrapper -->
<div class="modal add-resume-modal" id="thankyou" style="display: block;" tabindex="-1" role="dialog" aria-labelledby="">
            <div class="modal-dialog modal-md" role="document" style="width: 50%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#thankyou').hide();"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Thank You</h4>
                    </div>
                    <div class="modal-body">
                        <div class="input-group image-preview form-group " style="width: 100%;">
                            <div class="container" style="width: 100%;border: 5px solid #f2f2f2;text-align: center;padding: 10px;">
                                <div  id="profile-desc">
                                 <img src="<?=$imageurl;?>images/thankyou.png" style="width: 70px;"/>
                                 <br/><br/>
                                 <h4>Thank you for registering with us.</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" value="OK" aria-hidden="true" data-dismiss="alert" class="btn btn-lg btn-primary btn-block" style="width: 100px;margin: auto;" onclick="$('#thankyou').hide(); thankyouredirect();"/>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<div class="modal-backdrop  in">
</div>
<script type="text/javascript">
function thankyouredirect() {
    <?php
    if(isset(Yii::$app->session['Employeeid']))
    {
    ?>
    setTimeout(function(){window.location.href="<?= Url::toRoute(['wall/mcbwallpost'])?>"},50);
    <?php
    }elseif(isset(Yii::$app->session['Employerid']))
    {
    ?>
    setTimeout(function(){window.location.href="<?= Url::toRoute(['wall/mcbwallpost'])?>"},50);
    <?php
    }
    elseif(isset(Yii::$app->session['Campusid']))
    {
    ?>
    setTimeout(function(){window.location.href="<?= Url::toRoute(['wall/mcbwallpost'])?>"},50);
    <?php
    }
    elseif(isset(Yii::$app->session['Teamid']))
    {
    ?>
    setTimeout(function(){window.location.href="<?= Url::toRoute(['wall/mcbwallpost'])?>"},50);
    <?php
    }else{
    ?>    setTimeout(function(){window.location.href="<?= Url::toRoute(['/'])?>"},50);    <?php }?>
}
    
</script>