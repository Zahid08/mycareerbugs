<?php
   $this->title = 'Walk-In Jobs - My Career Bugs';
   use yii\helpers\Url;
   use yii\helpers\Html;
   use yii\bootstrap\ActiveForm;
   $imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
   $url='/backend/web/';
   use yii\widgets\LinkPager;   
   use common\models\AppliedJob;
   $appliedjob=new AppliedJob();
   ?> 
<meta name="keywords" content="My Career Bugs, MCB Jobs, Walk-ins, walk in interview jobs" />
<meta name="description" content="Top Employers hiring through Walk-ins. Fresher and Experienced Walk-Ins. Create a Resume & apply for walk in interview jobs." />
<style>
   .only_show{top:45px;}
   #keynameSearchContainer .ui-autocomplete { position: absolute; top: 55px; left: 16px;}
   #IndexCitySearchContainer .ui-autocomplete { position: absolute; top: 55px; left: 365px;}
   #IndexCitySearchContainer .ui-autocomplete { height:auto }
   .find_a_job{display:none}   
   @media only screen and (max-width: 767px) {
   .bg-full {width: 100%;}
   .search_result{padding: 23px 0 1px 0 !Important;}
   #keynameSearchContainer .ui-autocomplete { position: absolute; top: 57px; left:0px;}
   #IndexCitySearchContainer .ui-autocomplete { position: absolute; top: 110px; left:15px}
   #clear {margin-top: 0 !important;text-align: right;float: right;width: 100%;}
   .number_drop_down {margin: 0px 6px 7px 18px;float: right;width: 188px;}
   .form-control23.select2-hidden-accessible{background:#fff;}
   #select_per_page {margin: 0px 0 0 0;}
   #right-side{background:#f2f2f2; margin-bottom: 0px !important;}
   .spacer-5 {width: 100%;height: 16px;}
   }
   .need_help{display:none;}
   .info.bottom .left-text.min-h21{height: 73px;
   overflow: hidden;}
</style>
<!-- start main wrapper --> 
<div id="wrapper">
   <div class="headline job_head" style="display:none">
   </div>
   <div class="recent-job">
      <!-- Start Recent Job -->
      <div class="container">
         <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
               <div class="heading_top">
                  <h4> Find Walkin Jobs near your Location </h4>
               </div>
               <div class="rows" >
                  <div class="col-md-12 bg-full">
                     <div class="sticky">
                        <?php $form = ActiveForm::begin([
                           'options' => [
                               'class' => 'offset-top-10 offset-sm-top-30',
                               'id'=>'home_page_form'
                           ]]); ?>
                        <?php 
                           $keyname = null;
                           $indexlocation = null;
                           $experience = null;
                           $salary = null; 
                           if ($_POST){
                               $keyname = isset($_POST['keyname']) ? $_POST['keyname'] : null;
                               $indexlocation = isset($_POST['indexlocation']) ? $_POST['indexlocation'] : null;
                               $experience = isset($_POST['experience']) ? $_POST['experience'] : null;
                               
                               
                               
                               $salary = isset($_POST['salary']) ? $_POST['salary'] : null;
                           }
                           ?>
                        <div class="group-sm group-top">
                           <div  class="group-item col-md-5 col-xs-12">
                              <div class="form-group"> 
                                 <input type="text" class="form-control" value = "<?php echo $keyname?>" name="keyname" id="keyname" placeholder="Job title, skills, or company">
                              </div>
                           </div>
                           <div  class="group-item col-md-2 col-xs-12">
                              <div class="form-group"> 
                                 <input type="text" class="form-control" value="<?php echo $indexlocation?>" name="indexlocation" id="indexlocation" placeholder="Location"> 
                              </div>
                           </div>
                           <div   class="group-item col-md-2 col-xs-6">
                              <div class="form-group">
                                 <select id="form-filter-location" name="experience" id="experience" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">Experience</option>
                                    <option value="Fresher">  Fresher</option>
                                    <option value="0-1">  Below 1 Year</option>
                                     <option value="Both">Both</option>
                                    <?php
                                       for($fr=1;$fr<=30;$fr=$fr+1)
                                       {
                                       $frn=$fr+1;
                                       
                                       ?>
                                    <option value="<?=$fr.'-'.$frn;?>" <?=  ($experience ==  "$fr-$frn") ? 'selected' : 'no'?>><?=$fr.'-'.$frn;?> Years</option>
                                    <?php
                                       }
                                       ?>
                                 </select>
                              </div>
                           </div>
                           <!-- id="no_nd_mbl12" id="no_nd_mbl13" -->		  
                           <div  class="group-item col-md-2 col-xs-6">
                              <div class="form-group">
                                 <select id="form-filter-location" name="salary" id="salary" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                    <option value="">Salary</option> 
                                    <option value="0 - 1.5 Lakhs" <?php echo $salary == "0 - 1.5 Lakhs" ? 'selected' : null?>> 0 - 1.5 Lakhs</option>
                                    <option value="1.5 - 3 Lakhs" <?php echo $salary == "1.5 - 3 Lakhs" ? 'selected' : null?>>1.5 - 3 Lakhs</option>
                                    <option value="3 - 6 Lakhs" <?php echo $salary == "3 - 6 Lakhs" ? 'selected' : null?>>3 - 6 Lakhs</option>
                                    <option value="6 - 10 Lakhs" <?php echo $salary == "6 - 10 Lakhs" ? 'selected' : null?>>6 - 10 Lakhs</option>
                                    <option value="10 - 15 Lakhs" <?php echo $salary == "10 - 15 Lakhs" ? 'selected' : null?>>10 - 15 Lakhs</option>
                                    <option value="15 - 25 Lakhs" <?php echo $salary == "15 - 25 Lakhs" ? 'selected' : null?>>15 - 25 Lakhs</option>
                                    <option value="Above 25 Lakhs" <?php echo $salary == "Above 25 Lakhs" ? 'selected' : null?>>Above 25 Lakhs</option>
                                    <option value="Negotiable" <?php echo $salary == "Negotiable" ? 'selected' : null?>>Negotiable</option>
                                 </select>
                              </div>
                           </div>
                           <div class=" group-item reveal-block reveal-lg-inline-block col-md-1 col-xs-12">
                              <?= Html::submitButton('Search', ['name'=>'indexsearch','class' => 'btn btn-primary element-fullwidth']) ?>
                           </div>
                           <?php ActiveForm::end(); ?>
                           <div id="keynameSearchContainer"></div>
                           <div id="IndexCitySearchContainer"></div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <span class="search_result" style="color: #f16b22; display:block; padding:6px 0 0px 0;">
                     <?php
                        if($keyname!='')
                        {
                        	echo "'$keyname'";
                        	}
                        if($indexlocation!='')
                        {
                        	echo ' '."'$indexlocation'";
                        	}
                        if($experience!='')
                        {
                        	echo ' '."'$experience Years'";
                        	}
                        if($salary!='')
                        {
                        	echo ' '."'$salary'";
                        	}
                        
                        ?>
                     </span>
                     <style>
                        .select-date{clear:both; overflow:hidden; padding: 30px 0 0 0;}
                        .select-date .checkbox{float:left;      margin-right: 14px;   margin-top: 0;}
                        .select-date label	{padding: 0 0px 0 3px;}
                     </style>
                     <div class="select-date">
                        <div class="checkbox"> <label> <input type="checkbox" class="search-job" value="today" <?php if(isset($_GET['filtertoday'])) echo 'checked'; ?>>
                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Today  </label>
                        </div>
                        <div class="checkbox"> <label> <input type="checkbox" class="search-job" value="tomorrow" <?php if(isset($_GET['filtertomorrow'])) echo 'checked'; ?>>
                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Tomorrow  </label>
                        </div>
                        <div class="checkbox"> <label> <input type="checkbox" class="search-job" value="week" <?php if(isset($_GET['filterweek'])) echo 'checked'; ?>>
                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> This Week  </label>
                        </div>
                        <div class="checkbox"> <label> <input type="checkbox" class="search-job" value="month" <?php if(isset($_GET['filtermonth'])) echo 'checked'; ?>>
                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> This Month  </label>
                        </div>
                        <div class="checkbox"> <label> <input type="checkbox" class="search-job" value="nextmonth" <?php if(isset($_GET['filternextmonth'])) echo 'checked'; ?>>
                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span> Next Month </label>
                        </div>
                        <div class="checkbox" style="width:31%;">
                           <label style="float: left; margin: 5px 10px 0 0;"> <input type="checkbox" value="">   Select Date   </label>
                           <div class="input-group" style="width:59%; float: left;margin-top:-8px">
                              <span class="input-group-addon"><i class="fa fa-calendar"
                                 aria-hidden="true"></i></span> 
                              <input type="text" 	class="form-control date" name=""
                                  readonly 	style="cursor: pointer; background: #fff;" autocomplete="off"
                                 placeholder="Date" onkeypress="return numbersonly(event)" id="filterdate" value="<?php if(isset($_GET['filterdate'])) echo $_GET['filterdate']; ?>">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-8">
                        <div  id="clear">
                           <div class="col-sm-12">
                              <a href="<?= Url::toRoute(['/site/walkin']);?>"><label>Clear </label></a>
                           </div>
                        </div>
                     </div>
                     <div  class="number_drop_down">
                        <div class="width-10" id="select_per_page">
                           <div class="form-groups">
                              <div class="col-sm-8">
                                 <label>Result Per page </label>
                              </div>
                              <div class="col-sm-4">
                                 <?php
                                    if(isset($_GET['perpage']))
                                    {
                                    $perpage=$_GET['perpage'];
                                    }else{$perpage=10;}
                                    ?>
                                 <select name="resultperpage" data-minimum-results-for-search="Infinity" class="form-control23 select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="getresultperpage(this.value,'recentjob');">
                                    <option value="10" <?php if($perpage==10) echo "selected='selected'";?>>10</option>
                                    <option value="20" <?php if($perpage==20) echo "selected='selected'";?>>  20  </option>
                                    <option value="30" <?php if($perpage==30) echo "selected='selected'";?>>  30 </option>
                                    <option value="40" <?php if($perpage==40) echo "selected='selected'";?>>  40  </option>
                                    <option value="50" <?php if($perpage==50) echo "selected='selected'";?>>  50  </option>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix" style="margin-bottom: 20px;"></div>
               <?php
                  if($alljob)
                  {
                  foreach($alljob as $jkey=>$jvalue)
                  {
                  	if(isset(Yii::$app->session['Employeeid']))
                  		{
                  		$iscount=$appliedjob->getIsapplied($jvalue->JobId,Yii::$app->session['Employeeid']);
                  		}elseif(Yii::$app->session['Teamid'])
                                {
                  				$iscount=$appliedjob->getIsapplied($jvalue->JobId,Yii::$app->session['Teamid']);
                  		}else{$iscount=0;}
                  ?>
               <div class="item-list">
                  <div class="col-sm-12 add-desc-box">
                     <div class="only_show">Walkin Jobs</div>
                     <div class="add-details">
                        <a href="<?= Url::base().'/job/'.$jvalue->Slug;?>" target="target_blank">
                           <h5 class="add-title"><?=$jvalue->JobTitle;?></h5>
                           <?php
                              if($iscount==1)
                              {
                              	?>
                           <img src="<?=$imageurl;?>images/applied.png" class="applied"/>
                           <?php
                              }
                              ?>
                           <div class="info"> 
                              <span class="category"><?=$jvalue->Designation;?></span> -
                              <span class="item-location"><i class="fa fa-map-marker"></i> <?=$jvalue->Location;?></span> <br>
                              <span> <strong><?=$jvalue->CompanyName;?></strong> </span>
                           </div>
                           <div class="info bottom">
                              <div class="col-sm-3 col-xs-3">
                                 <span class="styl">Walkin : </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text">
                                 <span class="category"><?=date('d M Y',strtotime($jvalue->WalkinFrom)).' - '.date('d M Y',strtotime($jvalue->WalkinTo));?></span>  
                              </div>
                           </div>
                           <div class="info bottom">
                              <div class="col-sm-3 col-xs-3">
                                 <span class="styl">Experience : </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text">
                                 <?php
                                    if($jvalue->IsExp==0){
                                    		$exp='Fresher';
                                    }
                                    elseif($jvalue->IsExp==1)
                                    {
                                    		$exp=($jvalue->Experience!='')?$jvalue->Experience.' Years':'Experience';
                                    }
                                    elseif($jvalue->IsExp==2)
                                    {
                                    		$exp='Both - Freshers & Experienced';
                                    }
                                    ?>
                                 <span class="category"><?=$exp?></span>  
                              </div>
                           </div>
                           <div class="info bottom">
                              <div class="col-sm-3 col-xs-3">
                                 <span class="styl">Designation : </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text">
                                 <span class="category"><?=$jvalue->Designation;?></span>  
                              </div>
                           </div>
                           <div class="info bottom" style="display:none">
                              <div class="col-sm-3 col-xs-3">
                                 <span class="styl">Keyskills : </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text">
                                 <span class="category">
                                 <?php
                                    $jskill='';
                                    foreach($jvalue->jobRelatedSkills as $k=>$v)
                                    {
                                    $jskill.=$v->skill->Skill.' , ';
                                    }
                                    echo $jskill;
                                    ?>
                                 </span>  
                              </div>
                           </div>
                           <div class="info bottom">
                              <div class="col-sm-3 col-xs-3">
                                 <span class="styl">Job Description : </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text min-h21">
                                 <span class="category "><?=htmlspecialchars_decode($jvalue->JobDescription);?></span>  
                              </div>
                           </div>
                           <!--   <div class="info bottom">
                              <div class="col-sm-3 col-xs-3">
                              	<span class="styl">Job Description : </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text">
                              <span class="category"><?=substr(htmlspecialchars_decode($jvalue->JobDescription),0,150).'...';?></span>  
                                           </div>
                              </div> -->
                           <div class="info bottom">
                              <div class="col-sm-3 col-xs-3">
                                 <span class="styl">Salary Range </span>  
                              </div>
                              <div class="col-sm-9 col-xs-9 left-text">
                                 <span class="category"><?=$jvalue->Salary;?></span>  
                              </div>
                           </div>
                           <div class="info bottom"> 
                              <span class="category" style="text-align:right">    Posted By <?=$jvalue->employer->Name.' ('.date('d M Y, h:i A',strtotime($jvalue->OnDate)).')';?></span> 
                           </div>
                     </div>
                  </div>
                  </a>
               </div>
               <?php
                  }
                  }
                  else{
                  ?>
               No Job Found In this Search Category
               <?php
                  }
                  ?>
               <?php
                  echo LinkPager::widget([
                  	'pagination' => $pages,
                  ]);
                  ?>
               <div class="spacer-2"></div>
            </div>
            <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side">
               <div class="job-oppening-title-right">
                  <h4> Top Jobs </h4>
               </div>
               <div class="rsw">
                  <aside>
                     <div class="widget">
                        <ul class="related-post">
                           <?php
                              if($topjob)
                              {
                              foreach($topjob as $tkey=>$tvalue)
                              {
                              	?>
                           <li>
                              <!--    <a href="<?= Url::base().'/job/'.$tvalue->Slug;?>"> <?=$tvalue->CompanyName;?>  </a> -->
                              <a href="<?= Url::base().'/job/'.$jvalue->Slug;?>"><?=$jvalue->JobTitle;?> </a>
                              <span><i class="fa fa-suitcase"></i>Designation:   <?=$tvalue->Designation;?></span> 
                              <span><i class="fa fa-calendar"></i>Place:  <?=$tvalue->Location;?> </span>
                              <span><i class="fa fa-clock-o"></i>Post Time: <?=date('h:i A',strtotime($tvalue->OnDate));?> </span>
                           </li>
                           <?php
                              }
                              }
                              ?>
                        </ul>
                     </div>
                  </aside>
               </div>
               <?php
                  if($topjobopening)
                  {
                  ?>
               <div id="job-opening">
                  <div class="job-opening-top">
                     <div class="widget-heading fl-left"><span class="title">Top Job Opening</span></div>
                     <div class="job-opening-nav">
                        <a class="btn prev"></a>
                        <a class="btn next"></a>
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <br/>
                  <div id="job-opening-carousel" class="owl-carousel">
                     <?php
                        foreach($topjobopening as $tk=>$tval)
                        {
                        	if($tval->docDetail)
                        	{
                        		$doc=$url.$tval->docDetail->Doc;
                        	}
                        	else
                        	{
                        		$doc=$imageurl.'images/user.png';
                        	}
                        ?>
                     <div class="item-home">
                        <a href="<?= Url::base().'/job/'.$tval->Slug;?>">
                           <div class="job-opening">
                              <img src="<?=$doc;?>" class="img-responsive" alt="dummy-job-opening" />
                              <div class="job-opening-content">
                                 <?=$tval->JobTitle;?><br/>
                                 <p><?=substr(htmlspecialchars_decode($tval->JobDescription),0,100);?></p>
                              </div>
                              <div class="job-opening-meta clearfix">
                                 <div class="meta-job-location meta-block"><i class="fa fa-map-marker"></i><?=$tval->Location;?></div>
                                 <div class="meta-job-type meta-block"><i class="fa fa-user"></i><?=$tval->JobType;?></div>
                              </div>
                           </div>
                        </a>
                     </div>
                     <?php
                        }
                        ?>
                  </div>
               </div>
               <?php
                  }
                  ?>
               <div id="top-employers" style="">
                  <div class="widget-heading fl-left"><span class="title">Top Employers</span></div>
                  <div class="clearfix"></div>
                  <br/> 
                  <div class="spacer-5"></div>
               </div>
               <div class="widget" style="padding: 0px;display:none">
                  <div style="width: 100%;height: auto;float: left;">
                     <?php
                        foreach($allemployers as $ek=>$evalue)
                        {
                        	if($evalue->logo)
                        	{
                        		$lc=$url.$evalue->logo->Doc;
                        	}
                        	else
                        	{
                        		$lc=$imageurl.'images/user.png';
                        	}
                        ?>
                     <div style="width: 80px;height: 60px;float: left;border: 1px solid #ccc;margin: 4px;">
                        <img src="<?=$lc;?>" style="width: 80px;height: 60px;" />
                     </div>
                     <?php
                        }
                        ?>
                  </div>
               </div>
               <div class="spacer-4"></div>
               <div class="widget-heading"><span class="title">Hot Categories </span></div>
               <div class="spacer-5"></div>
               <div class="widget">
                  <ul class="categories-module">
                     <?php
                        if($hotcategory)
                        {
                        foreach($hotcategory as $hkey=>$hvalue)
                        {
                        ?>
                     <li> <a href="<?= Url::toRoute(['site/jobsearch','JobCategoryId'=>$hvalue->JobCategoryId])?>"> <?=$hvalue->CategoryName;?> <span>(<?=$hvalue->cnt;?>)</span> </a> </li>
                     <?php
                        }
                        }
                        ?>
                  </ul>
               </div>
               <div class="spacer-4"></div>
               <div class="widget-heading"><span class="title">Job Recommendations </span></div>
               <div class="spacer-5"></div>
               <div class="post-resume-container" style="background: #f16b22;color: #fff;font-size: 14px;">
                  <p>No job recommendations yet, but opportunity is out there!</p>
                  <p>  To start getting recommendations, 
                     <a href="" class="btn-default btn_alert"  type="" button="" data-toggle="modal" data-target="#myModal_jobalert"> Create Job alert </a>
                  </p>
               </div>
               <div class="modal add-resume-modal" id="myModal_buy_a_plan" tabindex="-1" role="dialog" aria-labelledby="">
                  <div class="modal-dialog modal-md" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 class="modal-title" id="myModalLabel">Add New Resume</h4>
                        </div>
                        <div class="modal-body">
                           <div class="input-group image-preview form-group ">
                              <input type="text" class="form-control image-preview-filename" disabled="disabled">
                              <span class="input-group-btn">
                                 <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                 <span class="glyphicon glyphicon-remove"></span> Clear
                                 </button>
                                 <div class="btn btn-default image-preview-input upload_btn">
                                    <i class="fa fa-file" aria-hidden="true"></i>
                                    <span class="image-preview-input-title">Browse</span>
                                    <input type="file" accept="file_extension" name="input-file-preview" />
                                 </div>
                              </span>
                           </div>
                           <p>Only pdf and doc files are accepted</p>
                        </div>
                        <div class="modal-footer">
                           <a href="" type="button" class="btn btn-default">Save Resume</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <!-- 
                  <div class="adban_block">
                       <img src="<?=$imageurl;?>images/adban_block/ban.jpg"> 
                     </div>
                  -->
            </div>
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
   <!-- end Recent Job -->
   <div class="upload_resume_main" style="display: none;">
      <div class="container">
         <div class="col-sm-7 col-xs-12">
            <p>Upload  your resume and let your next job find you.</p>
         </div>
         <div class="col-sm-3 col-xs-12">
            <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">
            <button type="button" class="post-resume-button">Upload Your Resume<i class="icon-upload grey"></i></button></a>
         </div>
         <div class="col-sm-2 col-xs-12">
         </div>
      </div>
   </div>
   <?php
      if(!isset(Yii::$app->session['Employeeid']))
      {
      ?>
   <div class="step-to">
      <div class="container">
         <h1>Easiest Way To Use for Candidate</h1>
         <p>
            Here, you can check out easiest way effective for My Career Bugs
            candidates. You can easily <br> create own account, profile, send
            resume and get valid job. Now, you don’t need to worry about
            searching <br> for the valid and qualification relevant job for
            anymore. Get ready to follow these simple steps to get job
            immediately.
         </p>
         <div class="step-spacer"></div>
         <div id="step-image">
            <div class="step-by-container">
               <div class="step-by">
                  <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">
                     <div class="step-by-inner">
                        <div class="step-by-inner-img">
                           <img src="<?=$imageurl;?>images/step-icon-1.png"
                              alt="My Career Bugs Candidate steps 1" />
                        </div>
                     </div>
                  </a>
                  <h5>Create an Account</h5>
               </div>
               <div class="step-by">
                  <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">
                     <div
                        class="step-by-inner">
                        <div class="step-by-inner-img">
                           <img src="<?=$imageurl;?>images/step-icon-2.png"
                              alt="My Career Bugs Candidate steps 2" />
                        </div>
                     </div>
                  </a>
                  <h5>Create your profile</h5>
               </div>
               <div class="step-by">
                  <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">
                     <div
                        class="step-by-inner">
                        <div class="step-by-inner-img">
                           <img src="<?=$imageurl;?>images/step-icon-3.png"
                              alt="My Career Bugs Candidate steps 3" />
                        </div>
                     </div>
                  </a>
                  <h5>Send your Resume</h5>
               </div>
               <div class="step-by">
                  <a href="<?= Url::toRoute(['site/jobsearch'])?>">
                     <div class="step-by-inner">
                        <div class="step-by-inner-img">
                           <img src="<?=$imageurl;?>images/step-icon-4.png"
                              alt="My Career Bugs Candidate steps 4" />
                        </div>
                     </div>
                  </a>
                  <h5>Get your Job</h5>
               </div>
            </div>
         </div>
         <div class="step-spacer"></div>
      </div>
   </div>
   <?php
      }
      ?>
   <div class="developed_by_e-developed_technology"  id="company-post" style="display:none">
      <div class="row">
         <h1>Companies Who Have Posted Jobs  </h1>
         <div id="company-post-list" class="owl-carousel company-post">
            <?php
               if($allcompany)
               {
               foreach($allcompany as $ckey=>$cvalue)
               {
               	if($cvalue->docDetail)
               	{
               		$doc=$url.$cvalue->docDetail->Doc;
               	}
               	else
               	{
               		$doc=$imageurl.'images/user.png';
               	}
               	?>
            <a href="<?=$cvalue->Website;?>" target="_blank">
               <div class="company" style="background: #fff;">
                  <img src="<?=$doc;?>" class="img-responsive" alt="company-post" style="height: 100px;"/>
               </div>
            </a>
            <?php
               }
               }
               ?>
         </div>
      </div>
   </div>
   <div class="testimony">
      <div class="container">
         <h1><?=$peoplesayblock->Heading;?></h1>
         <?=htmlspecialchars_decode($peoplesayblock->Content);?>
      </div>
      <div id="sync2" class="owl-carousel">
         <?php
            foreach($allfeedback as $fk=>$fvalue)
            {
            	if($fvalue->docDetail)
            	{
            		$doc=$url.$fvalue->docDetail->Doc;
            	}
            	else
            	{
            		$doc=$imageurl.'images/user.png';
            	}
            ?>
         <div class="testimony-image">
            <img src="<?=$doc;?>" class="img-responsive" alt="testimony" style="height: 150px;width: 150px;"/>
         </div>
         <?php
            }
            ?>
      </div>
      <div id="sync1" class="owl-carousel">
         <?php
            foreach($allfeedback as $fk=>$fvalue1)
            {
            	?>
         <div class="testimony-content container">
            <?=htmlspecialchars_decode($fvalue1->Message);?>
            <p>
               <?php echo $fvalue1->Name.','.$fvalue1->Designation.','.$fvalue1->Companyname;?>
            </p>
            <div class="media-testimony">
               <?php
                  if($fvalue1->Twitterlink!='')
                  {
                  ?>
               <a href="<?=$fvalue1->Twitterlink;?>" target="blank"><i class="fa fa-twitter twit"></i></a>
               <?php
                  }
                  if($fvalue1->LinkedinLink!='')
                  {
                  ?>
               <a href="<?=$fvalue1->LinkedinLink;?>" target="blank"><i class="fa fa-linkedin linkedin"></i></a>
               <?php
                  }
                  if($fvalue1->Facebooklink!='')
                  {
                  ?>
               <a href="<?=$fvalue1->Facebooklink;?>" target="blank"><i class="fa fa-facebook fb"></i></a>
               <?php
                  }
                  ?>
            </div>
         </div>
         <?php
            }
            ?>
      </div>
   </div>
   <?php
      if(!isset(Yii::$app->session['Employeeid']))
      {
      ?>
   <div class="advertise_your_post">
      <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
         <img src="<?=$imageurl;?>images/job_seekers.png">
         <a href="<?= Url::toRoute(['site/jobseekersregister'])?>">  Register  </a>
      </div>
      <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
         <img src="<?=$imageurl;?>images/team_orange.png">
         <a href="<?= Url::toRoute(['team/index'])?>">Register  </a>
      </div>
      <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 new">
         <img src="<?=$imageurl;?>images/company.png">
         <a href="<?= Url::toRoute(['site/employersregister'])?>">Register  </a>
      </div>
      <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12">
         <img src="<?=$imageurl;?>images/campus_register.png">
         <a href="<?= Url::toRoute(['campus/campusregister'])?>">Register  </a>
      </div>
   </div>
   <?php
      }
      ?>
</div>
<!-- end main wrapper -->
<div class="modal add-resume-modal" id="myModal_jobalert" tabindex="-1" role="dialog" aria-labelledby="">
   <div class="modal-dialog modal-md" role="document" style="width: 700px;height: 422px;overflow: auto;">
      <?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30']]); ?>
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Tell us what kind of jobs you want</h4>
         </div>
         <div class="modal-body">
            <!--
               <div class="input-group image-preview form-group ">
               <?= $form->field($jobalert, 'Keyword')->textInput(['maxlength' => true,'placeholder'=>'Skills, Designations, Roles, Companies','id'=>'alertkeywords']) ?>
               </div>
               <div class="input-group image-preview form-group ">
               <?= $form->field($jobalert, 'Location')->textInput(['maxlength' => true,'placeholder'=>'Enter the cities you want to work in','id'=>'alertlocation']) ?>
               </div>
               <div class="input-group image-preview form-group ">
               <?= $form->field($jobalert, 'Industry')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired industry where you want to work','id'=>'alertindustry']) ?>
               </div>
               <div class="input-group image-preview form-group ">
               <?= $form->field($jobalert, 'JobCategory')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired category where you want to work','id'=>'alertjobcategory']) ?>
               </div>
               <div class="input-group image-preview form-group ">
               <?= $form->field($jobalert, 'NameJobAlert')->textInput(['maxlength' => true,'placeholder'=>'Enter a name that will help you recognize this Job Alert']) ?>
               </div>
               <div class="input-group image-preview form-group ">
               <?= $form->field($jobalert, 'EmailId')->textInput(['maxlength' => true,'placeholder'=>'Enter your Email ID where you would like to receive this Job Alert']) ?>
               </div> 
               -->
            <div class="panel panel-default" style="margin-bottom:0px;">
               <div id="collapseOne19" class="panel-collapse collapse in">
                  <div class="panel-group" style="padding:0; margin-bottom:0px">
                     <div class="panel panel-default" style="border:0px;box-shadow: none;">
                        <div class="panel-heading sty" style="background: #fff !important;padding: 9px 28px 5px 28px;color: #fff !important;margin-bottom: 10px;">
                           <div class="col-md-6 no-pad-123">
                              <a style="padding: 6px 0">
                                 <h4 class="panel-title" style="font-size:12px;color:#000">	<input type="radio" value="blue" name="CollarType" class="collar-type"> Blue Collar
                                    <span class="checkmark"></span>
                                 </h4>
                              </a>
                           </div>
                           <div class="col-md-6 no-pad-123">
                              <a style="padding: 6px 0">
                                 <h4 class="panel-title" style="font-size:12px; color:#000"><input type="radio" value="white" name="CollarType" class="collar-type"> White Collar
                                    <span class="checkmark"></span>
                                 </h4>
                              </a>
                           </div>
                        </div>
                        <div class="panel-body" id="blue-collar-fields" style="border: 0px; padding:0 10px 1px 10px">
                           <div class="input-group image-preview form-group ">
                              <select id="form-filter-location" name="experience" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                 <option value="">Experience</option>
                                 <option value="Fresher">Fresher</option>
                                 <option value="0-1">Below 1 Year</option>
                                 <option value="1-2">1-2 Years</option>
                                 <option value="2-3">2-3 Years</option>
                                 <option value="3-4">3-4 Years</option>
                                 <option value="4-5">4-5 Years</option>
                                 <option value="5-6">5-6 Years</option>
                                 <option value="6-7">6-7 Years</option>
                                 <option value="7-8">7-8 Years</option>
                                 <option value="8-9">8-9 Years</option>
                                 <option value="9-10">9-10 Years</option>
                                 <option value="10-11">10-11 Years</option>
                                 <option value="11-12">11-12 Years</option>
                                 <option value="12-13">12-13 Years</option>
                                 <option value="13-14">13-14 Years</option>
                                 <option value="14-15">14-15 Years</option>
                                 <option value="15-16">15-16 Years</option>
                                 <option value="16-17">16-17 Years</option>
                                 <option value="17-18">17-18 Years</option>
                                 <option value="18-19">18-19 Years</option>
                                 <option value="19-20">19-20 Years</option>
                                 <option value="20-21">20-21 Years</option>
                                 <option value="21-22">21-22 Years</option>
                                 <option value="22-23">22-23 Years</option>
                                 <option value="23-24">23-24 Years</option>
                                 <option value="24-25">24-25 Years</option>
                                 <option value="25-26">25-26 Years</option>
                                 <option value="26-27">26-27 Years</option>
                                 <option value="27-28">27-28 Years</option>
                                 <option value="28-29">28-29 Years</option>
                                 <option value="29-30">29-30 Years</option>
                                 <option value="30-31">30-31 Years</option>
                              </select>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <select id="form-filter-location" name="salary" data-minimum-results-for-search="Infinity" class="form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                 <option value="">Salary</option>
                                 <option value="0 - 1.5 Lakhs">0 - 1.5 Lakhs</option>
                                 <option value="1.5 - 3 Lakhs">1.5 - 3 Lakhs</option>
                                 <option value="3 - 6 Lakhs">3 - 6 Lakhs</option>
                                 <option value="6 - 10 Lakhs">6 - 10 Lakhs</option>
                                 <option value="10 - 15 Lakhs">10 - 15 Lakhs</option>
                                 <option value="15 - 25 Lakhs">15 - 25 Lakhs</option>
                                 <option value="Above 25 Lakhs">Above 25 Lakhs</option>
                                 <option value="Negotiable">Negotiable</option>
                              </select>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'Keyword')->textInput(['maxlength' => true,'placeholder'=>'Skills, Designations, Roles, Companies','id'=>'alertkeywords']) ?>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'Location')->textInput(['maxlength' => true,'placeholder'=>'Enter the cities you want to work in','id'=>'alertlocation']) ?>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'Industry')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired industry where you want to work','id'=>'alertindustry']) ?>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'EmailId')->textInput(['maxlength' => true,'placeholder'=>'Enter your Email ID where you would like to receive this Job Alert']) ?>
                           </div>
                        </div>
                        <div class="panel-body" id="white-collar-fields" style="display:none;border: 0px; padding-top:0px; padding-bottom:5px">
                           <div class="input-group image-preview form-group" style="margin-bottom:0px;height:50px">
                              <select style="padding:10px; font-size:12px;width: 100%;">
                                 <option>Experience</option>
                              </select>
                           </div>
                           <div class="input-group image-preview form-group" style="margin-bottom:0px; height:50px">
                              <select style="padding:10px; font-size:12px;width: 100%;">
                                 <option>Salary</option>
                              </select>
                           </div>
                           <div class="input-group image-preview form-group" style="margin-bottom:0px;height:50px">
                              <select style="padding:10px; font-size:12px;width: 100%;">
                                 <option>Catogory</option>
                              </select>
                           </div>
                           <div class="input-group image-preview form-group" style="margin-bottom:0px; height:50px">
                              <select style="padding:10px; font-size:12px;width: 100%;">
                                 <option>Role</option>
                              </select>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'Keyword')->textInput(['maxlength' => true,'placeholder'=>'Skills, Designations, Companies','id'=>'alertkeywords']) ?>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'Location')->textInput(['maxlength' => true,'placeholder'=>'Enter the cities you want to work in','id'=>'alertlocation']) ?>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'Industry')->textInput(['maxlength' => true,'placeholder'=>'Type or Select the desired industry where you want to work','id'=>'alertindustry']) ?>
                           </div>
                           <div class="input-group image-preview form-group ">
                              <?= $form->field($jobalert, 'EmailId')->textInput(['maxlength' => true,'placeholder'=>'Enter your Email ID where you would like to receive this Job Alert']) ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <?= Html::submitButton('Create Job alert', ['name'=>'jobalertsearch','class' => 'btn btn-lg btn-primary btn-block']) ?>
         </div>
      </div>
      <?php ActiveForm::end(); ?>
   </div>
</div>
<script>
   $(document).ready(function(){
   	$('.collar-type').click(function(){
   		var val = $(this).val();
   		if(val == "white"){
   			$('#blue-collar-fields').hide(0);
   			$('#white-collar-fields').show(0);
   		}else{
   			$('#blue-collar-fields').show(0);
   			$('#white-collar-fields').hide(0);
   		}
   	});
	
	$(document).on('click','.search-job', function(){
		var value = $(this).attr('value');
		window.location.href = window.location.href.replace( /[\?#].*|$/, "?filter"+value+"="+value );
	});
	
	$(document).on('change','#filterdate', function(){
		var value = $(this).val();
		window.location.href = window.location.href.replace( /[\?#].*|$/, "?filterdate="+value );
	});
	
	
   });
   
</script>