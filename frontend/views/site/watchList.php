<?php 
use common\models\CompanyWallPost;
use yii\helpers\Url;
?>
<div id="wrapper">
    <div class="recent-job"><!-- Start Recent Job -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-9 col-sm-9 col-xs-12">

                    <div class="col-lg-12 col-md-5 col-sm-5 col-xs-6">
                        <h4><i class="glyphicon glyphicon-briefcase"></i> All Watch List</h4>
                    </div>
                    <div class="col-lg-12 col-md-7 col-sm-7 col-xs-6" id="landing_select">

                        <div class="clearfix"></div>
                        	<?php
		if($model)
		{
		foreach($model as $jkey=>$jvalue)
		{
		   $postId=$jvalue->PostId;
		     $postJob = CompanyWallPost::find()->where([
                    'WallPostId' => $postId
                ])->one();
    
                if(!empty($postJob->Slug)){
                	$urlLink = Url::base().'/wall-post/'.$postJob->Slug;
                }else{
                	$urlLink = Url::toRoute(['/wall/post-view','id' =>  $postJob->WallPostId]);	
                }

		?>
		<div class="item-list">
        <a href="<?= $urlLink ?>">
            <div class="col-sm-12 add-desc-box">
                  <div class="add-details">
                    <h5 class="add-title"><?=$postJob->PostTitle;?></h5>
                    <div class="info">
                        <p><?=$postJob->Post;?></p>
					</div>
                 </div>
            </div>
         </a>
              </div>
		<?php
		}
		}
        else
        {
		?>
        <h5 class="add-title">No Watchlist found</h5>
        <?php
        }
        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 
 