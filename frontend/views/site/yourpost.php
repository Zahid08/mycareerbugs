<?php
$this->title = 'My Post Jobs';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\JobRelatedSkill;
use common\models\Skill;
use common\models\PostJobWRole;
use common\models\PostJobWSkill;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
//$postjob=new PostaJob();
?>

<style>
#simple-translate{display:none;}
    .find_a_job, .need_help{display:none;}
    
</style>
<div id="wrapper"><!-- start main wrapper -->

		<div class="inner_page" style="padding:30px 0 40px 0">
			<div class="container">

					 <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"  id="mobile_design">


	 <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12" style="padding:0px">
	     
					   <h4>  My Post Jobs</h4>
</div>
 <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12"  style="padding:0px">

<a id="mob_bt"  href="<?= Url::toRoute(['site/yourpost','type'=>'candidate'])?>"  style="float:right; display:block; color:#fff; padding: 10px 15px; text-align:center; margin:0px 10px  0; background: #f16b22;">Candidate </a>
<a id="mob_bt" href="<?= Url::toRoute(['site/yourpost','type'=>'campus'])?>" style="color:#fff;float:right; display:block; padding:10px 15px; text-align:center; margin:0px 10px 10px  0; background: #f16b22;">Campus</a>
</div>

<div class="clear"></div>
		
	
		 <?php
				foreach($allpost as $key=>$value)
				{
						if($value->JobStatus==0)
						{
								$status='Open';
								$background='green';
						}
						else
						{
								$status='Closed';
								$background='red';
						}
						if($value->PostFor=='Campus'){$pf=1;}else{$pf=0;}
						?>
				</div>				
					</div>	
				
				<?php $whiteRoleList = ArrayHelper::map(PostJobWRole::find()->where([
															'postjob_id' => $value->JobId,
															'user_id' => $value->EmployerId
														])->all(), 'role_id', 'role'); ?>		
						
			<div class="container">					
				 <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 no_pad_mmb">			
		<div class="item-list">
                
                <div class="col-sm-12 add-desc-box">
						<div class="edit_job post_fr" style="width: 70px; left: 0;top:-10px;height: 25px;">
								<p><?=$value->PostFor;?></p>
						</div>
						<div class="applied_job" style="background: <?=$background;?>">
								<p><?=$status;?></p>
						</div>
						<div class="edit_job">
								<a href="<?= Url::toRoute(['site/jobedit','JobId'=>$value->JobId,'pf'=>$pf])?>"><p><i class="fa fa-edit"></i> Edit Job</p></a>
						</div>
						<br/><br/> 	
						
					
						
                  <div class="add-details">
                    <a href="<?= Url::toRoute(['site/postdetail','JobId'=>$value->JobId,'pf'=>$pf])?>"><h8><?=$value->JobTitle;?></h8></a>
					
                    <div class="info" style="padding-top:0px">					  
                      <span class="category" style="color:#f16b22">
					  <?php 
					  if(!empty($value->CollarType) && $value->CollarType == "white"){
						  $arrRole = $whiteRoleList;
						  echo (!empty($arrRole)?array_shift($arrRole):"");
					  }else{
						  echo $value->position->Position;
					  }
					  ?>
					  </span> -
                      <span class="item-location" style="font-size:11px; margin-right:0.5%"><i class="fa fa-map-marker"></i> <?=$value->Location;?></span>
                    <span style="font-size:11px">by <strong ><?=$value->CompanyName;?></strong> </span>
					</div>
					
					
					 
					 	
					 	  <div class="info bottom">
					     <div class="col-sm-3 col-xs-4">
					    	<span class="styl">Designation :  </span>  
					    </div>
						<div class="col-sm-9 col-xs-8 left-text">
							<span class="category">
							  <?=$value->Designation;?>
							</span>  
                      </div>
					 </div>
					 
					 
			
				 
				 
				 
                    <div class="info bottom">
					    <div class="col-sm-3 col-xs-4">
					    	<span class="styl">Experience : </span>  
					    </div>
						<div class="col-sm-9 col-xs-8 left-text">
						<?php
						if($value->IsExp==0){
								$exp='Fresher';
						}
						elseif($value->IsExp==1)
						{
								$exp=($value->Experience!='')?$value->Experience.' Years':'Experience';
						}
						elseif($value->IsExp==2)
						{
								$exp='Both - Freshers & Experienced';
						}
						?>
							<span class="category"><?=$exp?></span>  
						
						
						
                      </div>
					 </div> 
					 
					 <?php if($value->CollarType == 'white'){?>
						<div class="info bottom">
								 <div class="col-sm-3 col-xs-4">
									<span class="styl">Job Category :  </span>  
								</div>
								<div class="col-sm-9 col-xs-8 left-text">
									<span class="category">
									<?=$value->category->name;?>
									</span>  
							  </div>
						 </div>
						 <div class="info bottom">
								 <div class="col-sm-3 col-xs-4">
									<span class="styl">Roles :  </span>  
								</div>
								<div class="col-sm-9 col-xs-8 left-text">
									<span class="category">
									<?php echo (!empty($whiteRoleList)?implode(', ',$whiteRoleList):"");?>
									</span>  
							  </div>
						 </div>
						 <div class="info bottom" style="display:none">
								 <div class="col-sm-3 col-xs-4">
									<span class="styl">Skills :  </span>  
								</div>
								<div class="col-sm-9 col-xs-8 left-text">
									<span class="category">
									<?php $whiteSkillList = ArrayHelper::map(PostJobWSkill::find()->where([
															'postjob_id' => $value->JobId,
															'user_id' => $value->EmployerId
														])->all(), 'skill_id', 'skill'); ?>
									<?php echo (!empty($whiteSkillList)?implode(', ',$whiteSkillList):"");?>
									</span>  
							  </div>
						 </div>
					 		 	
					 	 
					 <?php }else{?>
						<div class="info bottom">
								 <div class="col-sm-3 col-xs-4">
									<span class="styl">Role :  </span>  
								</div>
								<div class="col-sm-9 col-xs-8 left-text">
									<span class="category">
									<?=$value->position->Position;?>
									</span>  
							  </div>
						 </div>
						 
						 <div class="info bottom" style="display:none">
					     <div class="col-sm-3 col-xs-4">
					    	<span class="styl">Keyskills : <?php 
							$kyt = JobRelatedSkill::find()->where(['JobId' => $value->JobId])->all(); 
						 
							
							 ?> </span>  
					    </div>
						<div class="col-sm-9 col-xs-8 left-text">
							<span class="category">
								<?php
								foreach($kyt as $ky){
									$skll = Skill::find()->where(['SkillId'=> $ky->SkillId])->one();
									echo $skll->Skill .',';
								 
								}
								
								?>
							</span>  
                      </div>
					 </div>
					 
					 <?php }?>
				 
					  
					 
					   <div class="info bottom">
					    <div class="col-sm-3 col-xs-4">
					    	<span class="styl">Job Description : </span>  
					    </div>
						<div class="col-sm-9 col-xs-8 left-text">
						    
							<span class="category">  <?=htmlspecialchars_decode($value->JobDescription);?></span>  
							<!--	<span class="category"><?=substr(htmlspecialchars_decode($value->JobDescription),0,150);?></span>  -->
                      </div>
					 </div>
					 
					  <div class="info bottom">
					    <div class="col-sm-3 col-xs-4">
					    	<span class="styl">Salary Range </span>  
					    </div>
						<div class="col-sm-9 col-xs-8 left-text">
							<span class="category"><?=$value->Salary;?> Lakhs</span>  
                      </div>
					 </div> 
					 
					<div class="info bottom"> 
						<span class="category" style="text-align:right">
						    	<p style="float: right; display:block;font-size: 12px;margin-bottom:5px">No of views : <?=$value->TotalView;?> & No Of Responses : <?=$value->gettotalresponse($value->JobId);?></p>
						    	 <div class="clear"></div>
								<p style=" font-size: 11px;">	Posted By <?=Yii::$app->session['EmployerName'].' ('.date('d M Y, h:i A',strtotime($value->OnDate)).')';?></span> </p>
                    </div> 
                  </div>
                </div>
             
       </div>   
       	</div>	
		
		</div>
 <?php
		}
				 	     
				?>
 
  </div>
                        </div>
      </div>
		    </div>
		   </div>	
			
			<div class="clear"></div>
		 
		  </div>	  </div>	
		
		<div class="border"></div>
		
		
			  </div>	  </div>	
			  
			  
			  	  </div>	  </div>	