<?php
$this->title = 'Dashboard';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>
<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">
<div id="wrapper"><!-- start main wrapper -->

		<div class="inner_page">
			<div class="container">
                  <div  id="profile-desc">
                    <div class="col-md-2 col-sm-2 col-xs-12">
			                 <div class="user-profile">
					
                                    <img src="<?php echo Yii::$app->session['TeamDP'] ;?>" alt="<?php echo Yii::$app->session['TeamName'] ;?>" class="img-responsive center-block ">
                                    <h3><?php echo Yii::$app->session['TeamName'] ;?></h3>
                                </div> 
			      	</div>
		         <div class="col-md-7 col-sm-7 col-xs-12">
						<div class="col-md-5">
								<div style="border-radius: 3px; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); margin-bottom: 20px;background-color: #26a69a;border-color: #26a69a;color: #fff;height: 120px;">
										<a href="<?= Url::toRoute(['team/appliedjob'])?>"><div style="padding: 20px;">
												<h3 style="font-size: 21px;color: #fff;margin: 0px;"><?=$appliedjob;?></h3>
												<div style="font-size:12px;color: #fff;">Total Applied</div>
												
										</div></a>
								</div>
						</div>
						<div class="col-md-5">
								<a href="<?= Url::toRoute(['team/bookmarkjob'])?>"><div style="border-radius: 3px; box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); margin-bottom: 20px;background-color: #ec407a;border-color: #ec407a;color: #fff;height: 120px;">
										<div style="padding: 20px;">
												<h3 style="font-size: 21px;color: #fff;margin: 0px;"><?=$bookmarked;?></h3>
												<div style="font-size:12px;">Total Bookmark</div>
										</div>
								</div></a>
						</div>
						
                 </div>
				 
				 
				 
				 
			<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12"  id="right-side">
				 <div class="widget-heading"><span class="title">Recent Jobs     </span></div>	
				 <div class="spacer-5"></div> 
						  <div class="rsw"> 
                            <aside> 
                                <div class="widget">
                                    
                                    <ul class="related-post">
					<?php
					if($alljob)
					{
					foreach($alljob as $jkey=>$jvalue)
					{
					?>
                                        <li>
                                            <a href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>"><?=$jvalue->JobTitle;?> </a>
											<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?></span>
											<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
											<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?></span>
                                        </li>
					<?php
						}
						}
						?>
										<a href="<?= Url::toRoute(['site/recentjob'])?>" class="sidebar_view_all"> View all  </a>
										 
										
                                    </ul>
                                </div>

                            </aside>
                        </div>
			</div>
				 
				 
				 
				 
				 
				 
                  </div>
				  
					 <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12"  id="mobile_design">
					   
                     </div>
      </div>
		    </div>
		 
		
		
		<div class="border"></div>