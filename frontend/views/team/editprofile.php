<?php
$this->title = 'Edit Profile';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use bupy7\cropbox\CropboxWidget;
use common\models\States;
use common\models\Cities;
use common\models\City;
use common\models\Industry;
use common\models\Experience;
use common\models\NaukariQualData;
use common\models\Skill;
use common\models\EmployeeSkill;
use common\models\WhiteRole;
use common\models\UserWhiteRole;
use common\models\UserWhiteSkill;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';

?>

<style>.editcontainer {	width: 20px;	height: 20px;	padding: 4px;	float: right;	cursor: pointer;}.select-wrapper {	background: url(<?=$imageurl;?>images/edit.png) no-repeat;	background-size: cover;	display: block;	position: relative;	width: 15px;	height: 15px;}#image_src {	width: 15px;	height: 15px;	margin-right: 100px;	opacity: 0;	filter: alpha(opacity = 0); /* IE 5-7 */}</style>



<img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">

<div id="wrapper">
	<!-- start main wrapper -->
	<div class="inner_page second">
		<div class="container">
			<div id="profile-desc">
			   <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>
			   <div class="col-md-2 col-sm-2 col-xs-12">
					<div class="user-profile">
						<img src="<?=Yii::$app->session['TeamDP'];?>" alt="team-image"
							class="img-responsive center-block ">
						<h3>
							<input type="text" class="form-control" required
								name="AllUser[Name]" id="Name" value="<?=$profile->Name;?>"
								placeholder="Name">
						</h3>
						<br />
						<div class="input-group">
										Edit Photo
						<?php echo  $form->field($docmodel, 'Doc')->widget(CropboxWidget::className(), ['croppedDataAttribute' => 'crop_info',])->label(false); ?>
				   </div>
					</div>
				</div>

				<div class="col-md-10 col-sm-10 col-xs-12">
					<div class="job-short-detail" id="edit_profile_page">
						<div class="heading-inner">
							<p class="title">Profile detail</p>
									<?= Html::submitButton('<i class="fa fa-floppy-o orange"></i> Save Changes') ?>
                                </div>
						<dl>
							<dt>Phone:</dt>
							<dd>

								<input type="text" class="form-control" name="AllUser[MobileNo]"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10"
									id="MobileNo" value="<?=$profile->MobileNo;?>" required>
							</dd>
							<dt>Whatsapp:</dt>
							<dd>
								<?php

        echo $form->field($profile, 'whatsappno', ['options' => ['tag' => false]])
            ->textInput([
            'maxlength' => 10,
            'onkeypress' => 'return numbersonly(event)',
            'onblur' => 'return IsMobileno(this.value);'
        ])
            ->label(false);?>

							</dd>

							<dt>Email:</dt>
							<dd>
								<input type="email" class="form-control" name="AllUser[Email]"
									value="<?=$profile->Email;?>" id="email"
									placeholder="Enter Email" readonly>
							</dd>

							<dt>Address:</dt>
							<dd>
								<input type="text" class="form-control" name="AllUser[Address]"
									id="address" value="<?=$profile->Address;?>"
									placeholder="234 Uptown new City Tower ">
							</dd>



							<dt>Pincode:</dt>

							<dd>
								<input type="text" class="form-control" name="AllUser[PinCode]"
									id="address" value="<?=$profile->PinCode;?>"
									onkeypress="return numbersonly(event)" maxlength="6">
							</dd>


							<dt>State</dt>
							<dd>
				   			<?php
								$state = $profile->State;
								$city = $profile->City;
							   //echo "state = $state";
							   // echo "city = $city";
								$profile->State = States::findOne(['StateName' => $state])->StateId;
								$profile->City = Cities::findOne(['CityName' => $city])->CityId;
								//echo "city =".$profile->City;
								?>
				   				<?php echo $form->field($profile, 'State', ['options' => ['tag' => false]])->dropDownList(States::getStateList(),['prompt'=>'Select State'])->label(false)?>

							</dd>

							<dt>City</dt>
							<dd>
								<?php echo $form->field($profile, 'City', ['options' => ['tag' => false]])->dropDownList(Cities::getCityList($profile->State),['prompt'=>'Select City'])->label(false)?>
							</dd>

							<dt>Country:</dt>
							<dd>
								<input type="text" class="form-control" name="AllUser[Country]"
									id="country" placeholder="Somewere at Antarctica "
									value="<?=$profile->Country;?>">
							</dd>

							<dt>Industry</dt>
							<dd>
				   			<?php

								//$profile->IndustryId = Industry::findOne(['IndustryId' => $IndustryId])->IndustryName;

								?>
				   				<?php echo $form->field($profile, 'IndustryId', ['options' => ['tag' => false]])->dropDownList(Industry::getIndustryLists(), ['prompt'=>'Select Industry'])->label(false)?>

							</dd>
							<?php //echo '<pre>'; print_r($profile);?>
							<dt>Salary</dt>
							<dd>

								<?php echo $profile->Salary;?>
								<select style="width:100%" class="form-control" name="AllUser[MySalary]">
									<option value="">Select Salary </option>
									<option value="0-1.5" <?=(($profile->Salary == '0-1.5')?"selected":"")?>>0 - 1.5 Lakh</option>
									<option value="1.5-3" <?=(($profile->Salary == '1.5-3')?"selected":"")?>>1.5 - 3 Lakh</option>
									<option value="3-6" <?=(($profile->Salary == '3-6')?"selected":"")?>>3 - 6 Lakh</option>
									<option value="6-10" <?=(($profile->Salary == '6-10')?"selected":"")?>>6 - 10 Lakh</option>
									<option value="10-15" <?=(($profile->Salary == '10-15')?"selected":"")?>>10 - 15 Lakh</option>
									<option value="15-25" <?=(($profile->Salary == '15-25')?"selected":"")?>>15 - 25 Lakh</option>
									<option value="Negotiable" <?=(($profile->Salary == 'Negotiable')?"selected":"")?>>Negotiable</option>
								  </select>

							</dd>



						</dl>

					</div>



				</div>



				<div class="clearfix"></div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="resume-box">
						<div class="heading-inner">
							<p class="title">Team Members Details</p>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Number of team members</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select id="form-filter-location"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control "
										tabindex="0" aria-hidden="true" name="AllUser[NoOfTeamMember]"
										required>
										<option value="">Select numbers</option>
                                            <?php for ($t = 0; $t <= 20; $t ++) { ?>                                            <option value="<?=$t;?>"
											<?php if($profile->NoOfTeamMember==$t){ echo "selected";} ?>><?=$t;?></option>
                                            <?php } ?>
                                    </select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-user" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Member Photo</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<div class="col-sm-12">


                                        <?php
											$count = 0;
											if ($profile->teams) {
                                                    foreach ($profile->teams as $key => $value) {
                                                        if ($value->MemberPhoto != 0) {
                                                            $teammemp = $url . $value->memberphoto->Doc;
                                                        } else {
                                                            $teammemp = '/images/user.png';
                                                        }
                                                        ?>
										<div id="teammember<?=$value->TeamMembersId;?>">
										<div class="row">
											<div class="col-sm-3">
												<div
													style="float: left; border: 1px solid #333; padding: 5px; margin: 5px; position: relative;">
													<img src="<?=$teammemp;?>" class="img-fluid img-thumbnail" /><br /><?=$value->MemberName;?>
													<div style="position: absolute; z-index: 99999; background: red; color: #fff; font-size: 9px; padding: 2px; height: 15px; right: 0px; top: 0px; padding-top: 0px; margin-top: 0px;"
														onclick="delpics(<?=$value->TeamMembersId;?>);">X</div>

												</div>
											</div>

											<div class="col-sm-9">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-user fa"
													aria-hidden="true"></i></span>
													<input type="hidden" name="AllUser[Team][<?=$k;?>][TeamMembersId]" value="<?=$value->TeamMembersId;?>" />
													<input class="form-control"
													name="AllUser[Team][<?=$key;?>][TeamName]" placeholder="Team Name"
													value="<?= $value->MemberName;?>" id="team-name-<?=$key;?>"
													type="text">
												</div>

												<div class="input-group"  style="width:100%; padding-top:10px;">
													<select style="width:100%" class="form-control load-qualification" name="AllUser[Team][<?=$key;?>][MemberQualification]" id="member-qualification-<?=$key;?>" >
														<option value="">Select Qualification</option>
														<?php foreach ($NaukariQualData as $key1 => $value1) { ?>
														<option value="<?=$value1->id;?>" <?php echo (($value1->id == $value->MemberQualification)?"selected":"");?>><?=$value1->name;?></option>
														<?php  } ?>
													</select>
												</div>

												<?php $courseList = NaukariQualData::getHighestQualificationList($value->MemberQualification);

												?>
												<div class="input-group"  style="width:100%;padding-top:10px;">
												 <select style="width:100%" class="form-control load-courses" name="AllUser[Team][<?=$key;?>][MemberCourse]" id="member-course-<?=$key;?>">
													<option value="">Select Course </option>
													<?php foreach ($courseList as $courseId => $courseName) { ?>
														<option value="<?=$courseId;?>" <?php echo (($courseId == $value->MemberCourse)?"selected":"");?> ><?=$courseName;?></option>
														<?php  } ?>
												  </select>
												</div>



												<div class="input-group"  style="width:100%;padding-top:10px;">
												 <select style="width:100%" class="form-control" name="AllUser[Team][<?=$key;?>][MemberSalary]" id="member-salary-<?=$key;?>">
													<option value="">Select Salary </option>
													<option value="0-1.5" <?=(($value->MemberSalary == '0-1.5')?"selected":"")?>>0 - 1.5 Lakh</option>
													<option value="1.5-3" <?=(($value->MemberSalary == '1.5-3')?"selected":"")?>>1.5 - 3 Lakh</option>
													<option value="3-6" <?=(($value->MemberSalary == '3-6')?"selected":"")?>>3 - 6 Lakh</option>
													<option value="6-10" <?=(($value->MemberSalary == '6-10')?"selected":"")?>>6 - 10 Lakh</option>
													<option value="10-15" <?=(($value->MemberSalary == '10-15')?"selected":"")?>>10 - 15 Lakh</option>
													<option value="15-25" <?=(($value->MemberSalary == '15-25')?"selected":"")?>>15 - 25 Lakh</option>
													<option value="Negotiable" <?=(($value->MemberSalary == 'Negotiable')?"selected":"")?>>Negotiable</option>
												  </select>
												</div>



											</div>
										</div>
                                        <hr style="margin:5px 0px;"/>
										</div>
                                         <?php
                                                  $count = $key+1;  }
                                                }

                                                ?>
                                            </div>
									<br/>
									<div class="col-sm-12" id="teammember-<?=$count;?>">
										<div class="row">
											<div class="col-sm-3">
												<div class="input-group">
													<label class="btn btn-default btn-file design-1"> Browse <input
														type="file" name="TeamMemberPhoto[<?=$count;?>]"
														style="display: none;"
														onchange="$(this).parent().next().html($(this).val());"
														accept="image/*" id="team-member-photo-<?=$count;?>">
													</label> <span></span>
												</div>
											</div>
											<div class="col-sm-9">
    											<div class="input-group">
    												<span class="input-group-addon"><i class="fa fa-user fa"
    													aria-hidden="true"></i></span> <input class="form-control"
    													name="AllUser[Team][<?=$count;?>][TeamName]" placeholder="Team Name"
    													type="text" id="team-name-<?=$count;?>">
    											</div>
												<div class="input-group"  style="width:100%; padding-top:10px;">
													<select style="width:100%" class="form-control load-qualification" name="AllUser[Team][<?=$count;?>][MemberQualification]" id="member-qualification-<?=$count;?>" >
														<option value="">Select Qualification</option>
														<?php foreach ($NaukariQualData as $key => $value) { ?>
														<option value="<?=$value->id;?>"><?=$value->name;?></option>
														<?php  } ?>
													</select>
												</div>

												<div class="input-group"  style="width:100%;padding-top:10px;">
												 <select style="width:100%" class="form-control load-courses" name="AllUser[Team][<?=$count;?>][MemberCourse]" id="member-course-<?=$count;?>">
													<option value="">Select Course </option>

												  </select>
												</div>

												<div class="input-group"  style="width:100%;padding-top:10px;">
												 <select style="width:100%" class="form-control" name="AllUser[Team][<?=$count;?>][MemberSalary]" id="member-salary-<?=$count;?>">
													<option value="">Select Salary </option>
													<option value="0-1.5">0 - 1.5 Lakh</option>
													<option value="1.5-3">1.5 - 3 Lakh</option>
													<option value="3-6">3 - 6 Lakh</option>
													<option value="6-10">6 - 10 Lakh</option>
													<option value="10-15">10 - 15 Lakh</option>
													<option value="15-25">15 - 25 Lakh</option>
													<option value="Negotiable">Negotiable</option>
												  </select>
												</div>
    										</div>
										</div>
									</div>

									<div class="col-sm-12">
										<div id="moreteammember"></div>
									</div>

									<div class="form-group">
										<label class="cols-sm-2 control-label"
											onclick="admoremember(<?=$count;?>);"
											style="color: #f15b22; cursor: pointer;">+ Add More</label>
									</div>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Description</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<textarea class="form-control textarea-small"
										name="AllUser[TeamDesc]"><?=$profile->TeamDesc;?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="resume-box">
						<div class="heading-inner">
							<p class="title">
								Educational Information <span style="display: none;"><?=($profile->educations[0]->PassingYear);?></span>
							</p>
						</div>

						<div class="row education-box" style="display: none;">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Higest Qualification</h4>
								</div>
							</div>
							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<input type="text" class="form-control"
										name="AllUser[HighestQualification]" id="hq"
										placeholder="Master of Business Administration"
										value="<?=$profile->educations[0]->HighestQualification;?>">
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Pass out Year</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select
										class="questions-category form-control "
										name="AllUser[PassingYear]" required>
										<option value="">Select Year</option>										<?php for ($y = 1980; $y <= date('Y'); $y ++) { ?>										<option value="<?=$y;?>" <?php if($profile->educations[0]->PassingYear==$y) echo "selected";?>><?=$y;?></option>										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Higest Qualification</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select
										class="questions-category form-control " id="empcourse1"
										name="AllUser[HighestQualId]" required>
										<option value="">Select Higest Qualification</option>
										<?php foreach ($NaukariQualData as $key => $value) {  ?>
										<option value="<?=$value->id;?>"
											<?php if($profile->educations[0]->highest_qual_id==$value->id) echo "selected";?>>
											<?=$value->name;?>
										</option>
										<?php } ?>
										</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Course</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select id="naukari-course"
										class="questions-category form-control "
										name="AllUser[CourseId]">
										<option value="">Select Course</option>
										<?php $courseList = NaukariQualData::getHighestQualificationList($profile->educations[0]->highest_qual_id); ?>
										<?php foreach ($courseList as $courseId => $courseName) { ?>
										<option value="<?=$courseId;?>"
											<?php if($profile->educations[0]->course_id==$courseId) echo "selected";?>><?=$courseName;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Specialization</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select
										class="questions-category form-control "
										name="AllUser[Specialization]" required>
										<option value="">Select Naukri Specialization</option>
										<?php foreach ($naukriSpecialization as $key => $value) { ?>
										<option value="<?=$value;?>"
											<?php if($profile->educations[0]->specialization_id==$value) echo "selected";?>><?=$value;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>University / College</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<input type="text" class="form-control"
										name="AllUser[University]" id="University"
										placeholder="University/College"
										value="<?=$profile->educations[0]->University;?>" required />
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Board</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select
										class="questions-category form-control "
										name="AllUser[board_id]">
										<option value="">Select Board</option>
										<?php foreach ($board as $key => $value) { ?>
										<option value="<?=$key;?>"
											<?php if($profile->educations[0]->board_id==$key) echo "selected";?>><?=$value;?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<?php if(!empty($profile->whitejobcategory)){
						$k = 0;
						foreach($profile->whitejobcategory as $categoryData){?>
							<br class="clearfix"/>
								<div class="row">
									<div class="col-md-2">
										<label class="control-label" style="width:100%"> Job Category</label>
									</div>
									<div class="col-md-4">
										<select class="questions-category form-control" name="AllUser[Category][<?=$k;?>][WhiteCategory]" tabindex="<?=$k;?>" aria-hidden="true" id="white_callar_category">
										 <option value = "">Select Category</option>
										 <?php if(!empty($whiteCategories)){
												foreach($whiteCategories as $categoryId => $categoryName){?>
													<option value="<?php echo $categoryId?>" <?=(($categoryId == $categoryData->category_id)?"selected":"")?> ><?=$categoryName;?></option>
												<?php  }
												}?>

										</select>
									</div>
									<div class="col-md-2">
										<label class="control-label"> Role</label>
									</div>
									<div class="col-md-4">
									<!--Get Category Role-->
									<?php $categoryRoleList = WhiteRole::getRoleList($categoryData->category_id);
										  $selectRoleList = UserWhiteRole::getCategoryRoleList($categoryData->category_id, $profile->UserId);
									?>
									<select class="questions-category form-control white_callar_role" name="AllUser[Category][<?=$k;?>][WhiteRole][]" id="white_callar_role<?=$k;?>" multiple="multiple">
										<?php if(!empty($categoryRoleList)){
											foreach($categoryRoleList as $roleId => $roleName){?>
												<option value="<?php echo $roleId?>" <?=(in_array($roleId, $selectRoleList)?"selected":"")?> ><?=$roleName;?></option>
											<?php  }
											}?>

									</select>
									</div>
								</div>

					<?php
					$countRole = $k;
					$k++;

					}?>
				<?php }else{
					$countRole = 0; ?>

					<div class="clear"></div>
					<div class="row">
						<div class="col-md-2">
							<label class="control-label" style="width:100%"> Job Category</label>
						</div>
						<div class="col-md-4">
							<select class="questions-category form-control" name="AllUser[Category][0][WhiteCategory]" tabindex="0" aria-hidden="true" id="white_callar_category">
							 <option value = "">Select Category</option>
							 <?php if(!empty($whiteCategories)){
									foreach($whiteCategories as $categoryId => $categoryName){?>
										<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
									<?php  }
									}?>

							</select>
						</div>
						<div class="col-md-2">
							<label class="control-label"> Role</label>
						</div>
							<div class="col-md-4">
							<select class="questions-category form-control white_callar_role" name="AllUser[Category][0][WhiteRole][]" id="white_callar_role0" multiple="multiple"></select>
							</div>
						</div>

				<?php }?>
					<div id="white-role-container">

					</div>
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-4">
							<a class="btn btn-info more_add_blck" count="<?=$countRole;?>" href="javascript:void(0);" id="add-white-role" style="float:left; padding:0px 0px 0px 0px; width: 109px !important; color: #f15b22; background: none;border: 0px;text-decoration: underline; margin:10px 0 10px 0"> + Add More Role </a>
						</div>
					</div>
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-2">
							<label class="control-label"> Skills</label>
						</div>
						<div class="col-md-4">
							<?php $userWhiteskillData = UserWhiteSkill::getUserSkillList($profile->UserId);?>
							<select class="questions-category form-control" id="white_callar_skills" name="AllUser[WhiteSkills][]" multiple="multiple">
								<?php if(!empty($userWhiteskillData)){
									foreach($userWhiteskillData as $skillId => $skillName){?>
										<option value="<?php echo $skillId?>" selected ><?=$skillName;?></option>
									<?php  }
									}?>
							</select>
						</div>
					</div>


					</div>
				</div>

				<div class="clearfix"></div>


			<div class="col-md-12 col-sm-12 col-xs-12">

					<div class="resume-box">

						<div class="heading-inner">

							<p class="title">Resume</p>

									<?php

        if ($profile->cV)
        {

            $na = explode(" ", $profile->Name);

            $name = $na[0];

            $extar = explode(".", $profile->cV->Doc);

            $ext = $extar[1];
        } else {
            $name = '';
            $ext = '';
        }

        ?>
		</div>

						<div class="row education-box">

							<div class="col-md-4 col-xs-12 col-sm-4">

								<div class="resume-icon">

									<i class="fa fa-file-text" aria-hidden="true"></i>

								</div>

								<div class="insti-name">

									<h4>Change Resume</h4>

								</div>

							</div>

							<div class="col-xs-12 col-md-5 col-sm-5">

								<div class="degree-info">

									<input type="file" name="AllUser[CVId]"
										accept=".doc, .docx,.rtf,.pdf" id='cvid'>

									<p>doc,docx,pdf,rtf - 2MB max</p>

								</div>

							</div>
							<?php if ($profile->cV){?>
							<div class="col-xs-12 col-md-3 col-sm-3" id="delete-resume">
								<div class="degree-info">
								<i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;&nbsp;
								<?=($profile->CVId!=0)?'<a href="'.$url.$profile->cV->Doc.'" download="'.$name.'.'.$ext.'">'.$profile->Name.' Resume </a>':'';?>
								<a class="remove-resume" href="javascript:void(0);">
									&nbsp;&nbsp;<i style="color:red" class="fa fa-remove" aria-hidden="true"></i>
								</a>

								</div>
							</div>
							<?php }?>
						</div>

					</div>

				</div>



				<div class="col-md-12 col-sm-12 col-xs-12">
                <?php
				$count = 0;
                if ($profile->experiences) {
                    foreach ($profile->experiences as $k => $val) {
					$count++;
                        ?>
                     <div class="resume-box"
						id="selected-experience-box">
						<div class="heading-inner">
							<p class="title">
								Work Experience <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span>
							</p>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-calendar" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Year</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<div class="cols-sm-5">
										<div class="input-group" style="width: 33%; float: left;">
											<span class="input-group-addon"><i class="fa fa-calendar"
												aria-hidden="true"></i></span> <input type="text"
												class="form-control date" name="AllUser[Experience][<?=$k;?>][YearFrom]"
												readonly style="cursor: pointer; background: #fff;"
												autocomplete="off" value="<?=$val->YearFrom;?>"
												placeholder="From" onkeypress="return numbersonly(event)" />
										</div>

										<div class="input-group exp-to-date"
											style="width: 33%; float: left; margin-left: 10%; <?=(!empty($val->CurrentWorking)?"display:none;":"");?>">

											<span class="input-group-addon"><i class="fa fa-calendar"
												aria-hidden="true"></i></span> <input type="text"
												class="form-control date" name="AllUser[Experience][<?=$k;?>][YearTo]" readonly
												style="cursor: pointer; background: #fff;"
												autocomplete="off" value="<?=$val->YearTo;?>"
												placeholder="To" onkeypress="return numbersonly(event);"
												onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />

										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Company name</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
								<input type="hidden" name="AllUser[Experience][<?=$k;?>][ExperienceId]" value="<?=$val->ExperienceId;?>" />
									<input type="text" class="form-control"
										name="AllUser[Experience][<?=$k;?>][CompanyName]" id="company-name-<?=$k?>"
										placeholder="Company name" value="<?=$val->CompanyName;?>" />

								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-calendar" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Position</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<input type="text" class="form-control"
										name="AllUser[Experience][<?=$k;?>][PositionName]" id="position-name-<?=$k?>"
										placeholder="Position name" value="<?=$val->PositionName;?>" />

								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Industry</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select name="AllUser[Experience][<?=$k;?>][CIndustryId]"
										class="questions-category form-control "
										id="industry-id-<?=$k?>">
										<option selected="selected" value="">- Select an Industry -</option>
										<?php foreach($industry as $key=>$va){?>
										<option value="<?php echo $key;?>"
											<?php if($key==$val->IndustryId) echo "selected";?>><?=$va;?></option>
										<?php } ?>
										</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Job Category</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<select class="questions-category form-control white_callar_exp_category" name="AllUser[Experience][<?=$k;?>][Category]" id="white_callar_exp<?=$k;?>" data-id-count="<?=$k;?>">
										 <option value="">Select Category</option>
										 <?php if(!empty($whiteCategories)){
												foreach($whiteCategories as $categoryId => $categoryName){?>
													<option value="<?php echo $categoryId;?>" <?=($val->CategoryId == $categoryId)?"selected":"";?>><?=$categoryName;?></option>
												<?php  }
												}?>

										</select>
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Role</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info exp-white-collar-role">
										<?php $categoryRoleList = WhiteRole::getRoleList($val->CategoryId);
										  $selectExpRoleList = ExperienceWhiteRole::getCategoryRoleList($val->ExperienceId, $profile->UserId); ?>
										<select class="questions-category form-control white_callar_role" name="AllUser[Experience][<?=$k;?>][WhiteRole][]" id="white_callar_role_exp<?=$k;?>" multiple="multiple">
										<?php if(!empty($categoryRoleList)){
											foreach($categoryRoleList as $roleId => $roleName){?>
												<option value="<?php echo $roleId?>" <?=(in_array($roleId, $selectExpRoleList)?"selected":"")?> ><?=$roleName;?></option>
											<?php  }
											}?>
										</select>
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-files-o" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Skills</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info exp-white-collar-role">
										<?php
										$expSkillList = ExperienceWhiteSkill::getSkillList($val->ExperienceId, $profile->UserId);
										?>
										<select class="questions-category form-control white_callar_skills" id="white_callar_skills_exp<?=$k;?>" name="AllUser[Experience][<?=$k;?>][WhiteSkills][]" multiple="multiple">
											<?php if(!empty($expSkillList)){
													foreach($expSkillList as $skillId => $skillName){?>
														<option value="<?php echo $skillId?>" selected ><?=$skillName;?></option>
													<?php  }
													}?>
										</select>
									</div>
								</div>
							</div>


						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Experience</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select id="experience-<?=$k?>"
										name="AllUser[Experience][<?=$k;?>][Experience]"
										data-minimum-results-for-search="Infinity" required
										class="questions-category form-control "
										tabindex="0" aria-hidden="true">
										<option value="" <?php if($val->Experience=='Experience') echo "selected";?>> Experience</option>
										<option value="Fresher" <?php if($val->Experience=='Fresher') echo "selected";?>> Fresher</option>
										<option value="0-1" <?php if($val->Experience=='0-1') echo "selected";?>>Below 1 Year</option>					    				<?php for ($fr = 1; $fr <= 30; $fr = $fr + 1) { $frn = $fr + 1;  ?>
					    				<option value="<?=$fr.'-'.$frn;?>" <?php if($val->Experience==$fr.'-'.$frn) echo "selected";?>><?=$fr.'-'.$frn;?> Years</option>
                        				<?php  }  ?>
                              		</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Salary</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select
										class="questions-category form-control "
										tabindex="0" aria-hidden="true" name="AllUser[Experience][<?=$k;?>][Salary]"
										id="salary-<?=$k?>">
										<option value="">Salary</option>
										<option value="0-1.5"
											<?php if($val->Salary=='0-1.5') echo "selected";?>>0 - 1.5
											Lakh</option>
										<option value="1.5-3"
											<?php if($val->Salary=='1.5-3') echo "selected";?>>1.5 - 3
											Lakh</option>
										<option value="3-6"
											<?php if($val->Salary=='3-6') echo "selected";?>>3 - 6 Lakh</option>
										<option value="6-10"
											<?php if($val->Salary=='6-10') echo "selected";?>>6 - 10 Lakh
										</option>
										<option value="10-15"
											<?php if($val->Salary=='10-15') echo "selected";?>>>10 - 15
											Lakh</option>
										<option value="15-25"
											<?php if($val->Salary=='15-25') echo "selected";?>>15 - 25
											Lakh</option>
										<option value="Negotiable"
											<?php if($val->Salary=='Negotiable') echo "selected";?>>
											Negotiable</option>
									</select>
									<?php if($count == 1){?>
										<div class="current-work-exp" style="margin-top:10px;"><input id="currentWorking" type="checkbox" value="1" <?=(!empty($val->CurrentWorking)?"checked":"");?> name="AllUser[Experience][<?=$k;?>][CurrentWorking]">&nbsp;<span style="font-size:12px; color:##565656">I currently work here</span></div>
									<?php }?>
								</div>
							</div>
						</div>
					</div>
					<?php
                    }
                } else {

                    ?>
				  <div class="heading-inner">
						<p class="title">Work Experience</p>
					</div>
					<div class="row education-box">
						<div class="col-md-4 col-xs-12 col-sm-4">
							<div class="resume-icon">
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>

							<div class="insti-name">
								<h4>Year</h4>
							</div>
						</div>

						<div class="col-xs-12 col-md-8 col-sm-8">
							<div class="degree-info">
								<div class="cols-sm-5">
									<div class="input-group" style="width: 33%; float: left;">
										<span class="input-group-addon"><i class="fa fa-calendar"
											aria-hidden="true"></i></span> <input type="text"
											class="form-control date" name="AllUser[Experience][<?=$count;?>][YearFrom]" readonly
											style="cursor: pointer; background: #fff;" autocomplete="off"
											placeholder="From" onkeypress="return numbersonly(event)" />
									</div>

									<div class="input-group exp-to-date"
										style="width: 33%; float: left; margin-left: 10%;">
										<span class="input-group-addon"><i class="fa fa-calendar"
											aria-hidden="true"></i></span> <input type="text"
											class="form-control date" name="AllUser[Experience][<?=$count;?>][YearTo]" readonly
											style="cursor: pointer; background: #fff;" autocomplete="off"
											placeholder="To" onkeypress="return numbersonly(event);"
											onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="resume-box">
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-file-text" aria-hidden="true"></i>
								</div>
								<div class="insti-name">
									<h4>Company name</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<input type="text" class="form-control"
										name="AllUser[Experience][<?=$count;?>][CompanyName]" id="company-name-<?=$count;?>"
										placeholder="Company name" />
								</div>
							</div>
						</div>
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-calendar" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Position</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<input type="text" class="form-control"
										name="AllUser[Experience][<?=$count;?>][PositionName]" id="position-name-<?=$count;?>"
										placeholder="Position name" />

								</div>
							</div>
						</div>
						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Industry</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select name="AllUser[Experience][<?=$count;?>][CIndustryId]" id="industry-id-<?=$count;?>"
										class="questions-category form-control ">
										<option selected="selected" value="">- Select an Industry -</option>
										<?php foreach($industry as $key=>$va){?>
										<option value="<?php echo $key;?>"><?=$va;?></option>
										<?php } ?>
								</select>
								</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-files-o" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Role</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select
										class="questions-category form-control "
										data-role-id="selected-role-<?=$value->PositionId;?>"
										name="AllUser[Experience][<?=$count;?>][PositionId]" data-key="<?=$count;?>" id="experience-roleid-<?=$count;?>">
										<option value="">Select Role</option>
										<?php foreach ($position as $key => $value) { ?>
										<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
										<?php  } ?>
									</select>
								</div>
							</div>
						</div>

						<div id="experience-skill-list-<?=$count;?>">
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">

    								<div class="resume-icon">

    									<i class="fa fa-file-text" aria-hidden="true"></i>

    								</div>

    								<div class="insti-name">
    									<h4>Skills</h4>
    								</div>

    							</div>

								<div class="col-xs-12 col-md-8 col-sm-8">

    								<div class="degree-info skill-profiles"></div>

    							</div>
							</div>
						</div>

						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Experience</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select id="experience-<?=$count;?>" name="AllUser[Experience][<?=$count;?>][Experience]"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control "
										tabindex="0" aria-hidden="true">
										<option value="Experience">Experience</option>
										<option value="Fresher">Fresher</option>
										<option value="0-1">Below 1 Year</option>
					    				<?php for ($fr = 1; $fr <= 30; $fr = $fr + 1) { $frn = $fr + 1; ?>
                                 		<option value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                        <?php }  ?>
                             		 </select>
								</div>
							</div>
						</div>


						<div class="row education-box">
							<div class="col-md-4 col-xs-12 col-sm-4">
								<div class="resume-icon">
									<i class="fa fa-briefcase" aria-hidden="true"></i>
								</div>

								<div class="insti-name">
									<h4>Salary</h4>
								</div>
							</div>

							<div class="col-xs-12 col-md-8 col-sm-8">
								<div class="degree-info">
									<select id="salary-<?=$count;?>" class="questions-category form-control " tabindex="0" aria-hidden="true" name="AllUser[Experience][<?=$count;?>][Salary]">
										<option value="0-1.5">0 - 1.5 Lakh</option>
										<option value="1.5-3">1.5 - 3 Lakh</option>
										<option value="3-6">3 - 6 Lakh</option>
										<option value="6-10">6 - 10 Lakh</option>
										<option value="10-15">10 - 15 Lakh</option>
										<option value="15-25">15 - 25 Lakh</option>
										<option value="Negotiable">Negotiable</option>
									</select>

									<div class="current-work-exp" style="margin-top:10px;"><input id="currentWorking" type="checkbox" value="1" <?=(!empty($val->CurrentWorking)?"checked":"");?> name="AllUser[Experience][<?=$count;?>][CurrentWorking]">&nbsp;<span style="font-size:12px; color:##565656">I currently work here</span></div>

								</div>
							</div>
						</div>
					</div>
				<?php
                }

                ?>				<div id="experience-box-container"></div>
					<a href="javascript:void(0);" class="btn btn-primary"
						id="add-more-experience" count="<?=$count;?>"> Add More Experience </a>

				</div>


				<div class="clearfix"></div>


    <?php ActiveForm::end(); ?>
  			</div>
		</div>
	</div>
</div>
<div class="border"></div>

<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://mycareerbugs.com/js/jquery.dropdown.js"></script>
<script type="text/javascript">

    $(document).on('change', '#empcourse1', function() {
        if($(this).val()==""){
            $("#naukari-course option").remove();
            //$("#naukari-specialization option").remove();
            $("#naukari-board option").remove();
            return true;
        }

        /*if($(this).val()=="6"){
        	$("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
            return true;
        }*/
        if($(this).val()=="4" || $(this).val()=="5" || $(this).val()=="6"){
            $("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#university_college").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').removeClass('hide');
            var elm = 'naukari-board';
        }else{
            $("#naukari-course").parents('.form-group').removeClass('hide');
            $("#naukari-specialization").parents('.form-group').removeClass('hide');
            $("#university_college").parents('.form-group').removeClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
            var elm = 'naukari-course';
        }
        var id = $(this).val();
        $.ajax({
            type: "POST",
            url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
            data: {'id': $(this).val()},
            success : function(response) {
                var data = JSON.parse(response);
                $("#naukari-course option").remove();
                //$("#naukari-specialization option").remove();
                $("#naukari-board option").remove();
                $.each(data, function( index, value ) {
                    $("#"+elm).append('<option value="'+index+'">'+value+'</option>');
                });
            },

        })
    });

		var p=0;
		function admoremember(count) {
			p = parseInt(count) + 1;
			var result=$('#teammember-'+count).html();
            $('#moreteammember').append('<div id="teamember-'+p+'" ><span style="color:red;cursor:pointer;" onclick=del('+p+');>X</span>'+result+'</div>');



			$('#teamember-'+p).find('#team-member-photo-'+count).attr('name', 'TeamMemberPhoto['+p+']');
			$('#teamember-'+p).find('#team-member-photo-'+count).attr('id', '#team-member-photo-'+p);

			$('#teamember-'+p).find('#team-name-'+count).attr('name', 'AllUser[Team]['+p+'][TeamName]');
			$('#teamember-'+p).find('#team-name-'+count).attr('id', '#team-name-'+p);

			$('#teamember-'+p).find('#member-qualification-'+count).attr('name', 'AllUser[Team]['+p+'][MemberQualification]');
			$('#teamember-'+p).find('#member-qualification-'+count).attr('id', '#member-qualification-'+p);

			$('#teamember-'+p).find('#member-course-'+count).attr('name', 'AllUser[Team]['+p+'][MemberCourse]');
			$('#teamember-'+p).find('#member-course-'+count).attr('id', '#member-course-'+p);

			$('#teamember-'+p).find('#member-salary-'+count).attr('name', 'AllUser[Team]['+p+'][MemberSalary]');
			$('#teamember-'+p).find('#member-salary-'+count).attr('id', '#member-salary-'+p);

        }
		function del(cnt) {
            $('#teamember'+cnt).remove();
        }
		function delpics(picid) {
            var r=confirm("Are you sure you want to delete this member?");
			if (r==true) {
                $.ajax({
                    url:"<?= Url::toRoute(['team/delmember'])?>?picid="+picid,
					success:function(results) {
					var res=JSON.parse(results);
					if (res==1) {
                        	$('#teammember'+picid).remove();
                    	}
					}
				});
            }
        }
		setTimeout(function(){
    		var loc = new locationInfo();
    		$('#stateId').val('<?=$state;?>');
    		var loc1='<?=$state;?>';
    		var stateId =$("#stateId option[value='"+loc1+"']").attr('stateid');
            if(stateId != ''){
            loc.getCities(stateId);
        }
		setTimeout(function(){
			$('#cityId').val('<?=$city;?>');
		},2000);
	},5000);
	$(document).ready(function(e){
		var stateid = $('#alluser-state').val();
		//getCity(stateid);

		$('#currentWorking').click(function(){
			if($(this).is(':checked')){
				$('.exp-to-date:first').hide(0);
			}else{
				$('.exp-to-date:first').show(0);
			}

		});
	})
	$(document).on('change','#alluser-state',function(e){
		var stateid = $(this).val();
		getCity(stateid);
	})
	var baseUrl = "<?= Url::toRoute(['/'])?>";
	function getCity(stateid){
    	$.get(baseUrl+'site/getcity?stateId='+stateid, function(res){
    		$('#alluser-city').empty();
    		$('#alluser-city').append(res.result);
    	});
    }
    $(document).ready( function(e){
        var len =  $('[id^=education-roleid]').length;
        console.log('length = '+len);
		var i = 0;
        for(i = 0; i < len; i++){
           /*  var idvalue = $('#education-roleid-'+i).val();
            console.log('dataroleid = '+idvalue); */
        	var roleId = $('#education-roleid-'+i).val();
            console.log('roleid = '+roleId);
        	getSkill(roleId, i);
        }

        var len =  $('[id^=experience-roleid]').length;
        console.log('length = '+len);
		var i = 0;
        for(i = 0; i < len; i++){
        	var roleId = $('#experience-roleid-'+i).val();
            console.log('experience roleid = '+roleId);
        	//getSkill(roleId, i, 'ExperienceSkillId', true, 'experience-skill-list-');
        }

    });
    $(document).on('change' , '[id^=education-roleid]', function(e){
    	e.preventDefault();
    	var key = $(this).attr('data-key');
    	var idvalue = $(this).attr('data-roleid');
    	console.log('key = '+key);
        console.log('dataroleid = '+idvalue);
    	var roleId = $(this).val();
    	getSkill(roleId,key);
    })
    $(document).on('change' , '[id^=experience-roleid]', function(e){
    	e.preventDefault();
    	var newCount = $(this).attr('data-key');
    	var roleid = $(this).val();
		var loderimg = '<img src="'+mainurl+'img/ajax-loader.gif">';
		var e = $(this);
		$('#experience-skill-list-'+newCount).find('.skill-profiles').html(loderimg);
		//$('.skill-profiles').html(loderimg);
		 $.getJSON( "/site/skilldata", {roleid:roleid}, function( data ) {

			 var html = '';
			  $.each( data, function( key, val ) {
				 html += '<span><input type="checkbox" value="'+val.id+'" name="AllUser[Experience]['+newCount+'][ExperienceSkillId][]" />'+val.value+'&nbsp;</span><span>|</span>&nbsp;';
			  });
			  html +='</div></div>';

			$('#experience-skill-list-'+newCount).find('.skill-profiles').html(html);
			$('#experience-skill-list-'+newCount).show(0);
		});





		/*console.log('key = '+key);
        console.log('dataroleid = '+idvalue);
    	var roleId = $(this).val();
    	getSkill(roleId,key,'ExperienceSkillId',true,'experience-skill-list-');*/
    })
    var userid = "<?php echo $profile->UserId?>";
    function getSkill(roleId,key,name='EducationSkillId',existcheck = true,container='skilllist-'){
    	$.get(baseUrl+'site/getselectedskills?id='+roleId+'&userId='+userid+'&existCheck='+existcheck+'&name='+name, function(res){
    		console.log(res.result);
    		console.log('append to : '+'#'+container+key)
    		$('#'+container+key).empty();
    		$('#'+container+key).append(res.result);
    	});
    }
    function getSkillListField(count){
		var skilllist = $('.skilllist-options').html();
		var html = '<div class="row education-box">'+
						'<div class="col-md-4 col-xs-12 col-sm-4">'+
							'<div class="resume-icon">'+
								'<i class="fa fa-files-o" aria-hidden="true"></i>'+
							'</div>'+
                    		'<div class="insti-name">'+
                    			'<h4>Role</h4>'+
                    		'</div>'+
						'</div>'+
                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                        		'<select class="questions-category form-control  skilllist-options"'+
    							'id="education-roleid-'+count+'"'+
    							'data-key = "'+count+'"'+
    							'data-roleid = "<?= $employeeRole->Employeeskillid?>"'+
    							'name="AllUser[CourseId][]" required>'+
                    				skilllist +
                    			'</select>'+
                    		'</div>'+
                    	'</div>'+
                    '</div>'+
                    '<div class="row education-box">'+
						'<div class="col-md-4 col-xs-12 col-sm-4">'+
							'<div class="resume-icon">'+
								'<i class="fa fa-file-text" aria-hidden="true"></i>'+
							'</div>'+
							'<div class="insti-name">'+
								'<h4>Skills</h4>'+
							'</div>'+
						'</div>'+
						'<div class="col-xs-12 col-md-8 col-sm-8">'+
							'<div class="degree-info" id="skilllist-'+count+'">'+

							'</div>'+
						'</div>'+
					'</div>';
        return html;
    }
    var count = $('#add-more-skill').attr('data-last-key');
    $(document).on('click','#add-more-skill',function(e){
        count++;
        console.log('count = '+count);
		var html = getSkillListField(count);
		$('#role-list-container').append(html);
		var svalue = $('#education-roleid-'+count).val();
		getSkill(svalue,count,'EducationSkillId',false);
    });
    function getExperienceContent(){
		var html = $('#selected-experience-box').html();
		return html;
   	}
   	$(document).on('click','#add-more-experience',function(e){
		//$('.experience-box-container').append(getExperienceContent());
		var count =  $(this).attr('count');
		admoreexp(count);
   	});
   	function getCompanyNameField(count){
   		var html = '<div class="row education-box">'+
            			'<div class="col-md-4 col-xs-12 col-sm-4">'+
                    		'<div class="resume-icon">'+
                    			'<i class="fa fa-file-text" aria-hidden="true"></i>'+
                    		'</div>'+
                    		'<div class="insti-name">'+
                    			'<h4>Company name</h4>'+
                    		'</div>'+
                    	'</div>'+
                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                    			'<input type="text" class="form-control" name="AllUser[Experience]['+count+'][CompanyName]" id="CompanyName-'+count+'" placeholder="Company name" value="">'+
                    		'</div>'+
                    	'</div>'+
                    '</div>';
   		return html;
   	}
   	function getPositionName(count){
   		var html = '<div class="row education-box">'+
						'<div class="col-md-4 col-xs-12 col-sm-4">'+
                    		'<div class="resume-icon">'+
                    			'<i class="fa fa-calendar" aria-hidden="true"></i>'+
                    		'</div>'+

                    		'<div class="insti-name">'+
                    			'<h4>Position</h4>'+
                    		'</div>'+
                    	'</div>'+

                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                    			'<input type="text" class="form-control" name="AllUser[Experience]['+count+'][PositionName]" id="PositionName-'+count+'" placeholder="Position name" />'+

                    		'</div>'+
                    	'</div>'+
                    '</div>';
   		return html;
   	}
   	function getIndustry(count) {
   		var options = $('#industry-id-0').html();
   		var html = '<div class="row education-box">'+
                    	'<div class="col-md-4 col-xs-12 col-sm-4">'+
                    		'<div class="resume-icon">'+
                    			'<i class="fa fa-files-o" aria-hidden="true"></i>'+
                    		'</div>'+
                    		'<div class="insti-name">'+
                    			'<h4>Industry</h4>'+
                    		'</div>'+
                    	'</div>'+

                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                    			'<select name="AllUser[Experience]['+count+'][CIndustryId]"  class="questions-category form-control " id="industry-id-'+count+'">'+
                    				options+
                    			'</select>'+
                    		'</div>'+
                    	'</div>'+
                    '</div>';
   		return html;
   	}
   	function getRole(count){
   		var options = $('#experience-roleid-0').html();
   		var html = '<div class="row education-box">'+
            			'<div class="col-md-4 col-xs-12 col-sm-4">'+
                    		'<div class="resume-icon">'+
                    			'<i class="fa fa-files-o" aria-hidden="true"></i>'+
                    		'</div>'+
                    		'<div class="insti-name">'+
                    			'<h4>Role</h4>'+
                    		'</div>'+
                    	'</div>'+
                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                    			'<select class="questions-category form-control " data-key='+count+' id="experience-roleid-'+count+'" name="AllUser[Experience]['+count+'][PositionId]">'+
									options+
								'</select>'+
                    		'</div>'+
                    	'</div>'+
                    '</div>'+
                    '<div id="experience-skill-list-'+count+'" style="display:none;"><div class="row education-box"><div class="col-md-4 col-xs-12 col-sm-4"><div class="resume-icon"><i class="fa fa-file-text" aria-hidden="true"></i></div><div class="insti-name"><h4>Skills</h4></div></div><div class="col-xs-12 col-md-8 col-sm-8"><div class="degree-info skill-profiles"></div></div></div></div>';


   		return html;
   	}
   	function getExperience(count) {
   		var options = $('#experience-0').html();
   		var html = '<div class="row education-box">'+
						'<div class="col-md-4 col-xs-12 col-sm-4">'+
                    		'<div class="resume-icon">'+
                    			'<i class="fa fa-briefcase" aria-hidden="true"></i>'+
                    		'</div>'+
                    		'<div class="insti-name">'+
                    			'<h4>Experience</h4>'+
                    		'</div>'+
                    	'</div>'+
                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                    			'<select id="experience-'+count+'" name="AllUser[Experience]['+count+'][Experience]" data-minimum-results-for-search="Infinity" required="" class="questions-category form-control " tabindex="0" aria-hidden="true">'+
                    				options+
                                '</select>'+
                    		'</div>'+
                    	'</div>'+
                    '</div>';
   		return html;
   	}
   	function getSalary(count){
   		var options = $('#salary-0').html();
   		var html = '<div class="row education-box">'+
                    	'<div class="col-md-4 col-xs-12 col-sm-4">'+
                    		'<div class="resume-icon">'+
                    			'<i class="fa fa-briefcase" aria-hidden="true"></i>'+
                    		'</div>'+
                    		'<div class="insti-name">'+
                    			'<h4>Salary</h4>'+
                    		'</div>'+
                    	'</div>'+
                    	'<div class="col-xs-12 col-md-8 col-sm-8">'+
                    		'<div class="degree-info">'+
                    			'<select class="questions-category form-control " tabindex="0" aria-hidden="true" name="AllUser[Experience]['+count+'][Salary]" id="salary-'+count+'">'+
                    				options+
                    			'</select>'+
                    		'</div>'+
                    	'</div>'+
                    '</div>';
   		return html;
   	}
   	//var p=0;
   	function admoreexp(p) {
   			p++;
			///alert(p);
   			var result = getCompanyNameField (p) +
   			getPositionName (p) +
   			getIndustry (p) +
   			getRole (p) +
   			getExperience (p) +
   			getSalary (p);
   			var date='<div class="row education-box"><div class="col-md-4 col-xs-12 col-sm-4"><div class="resume-icon"><i class="fa fa-calendar" aria-hidden="true"></i></div><div class="insti-name"><h4>Year</h4></div></div><div class="col-xs-12 col-md-8 col-sm-8"><div class="degree-info"><div class="cols-sm-5"><div class="input-group" id="responsive_full" style="width: 33%;float: left;">\
   				<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>\
   				<input type="text" class="form-control date" name="AllUser[Experience]['+p+'][YearFrom]" readonly style="cursor: pointer;background: #fff;"\
   				autocomplete="off"  placeholder="From" maxlength="4" onkeypress="return numbersonly(event)"/>\
   				</div><div class="input-group" id="responsive_full1" style="width: 33%;float: left;margin-left: 10%;">\
   				<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>\
   				<input type="text" class="form-control date" name="AllUser[Experience]['+p+'][YearTo]" readonly style="cursor: pointer;background: #fff;" autocomplete="off"  \
   				placeholder="To"  maxlength="4" onkeypress="return numbersonly(event);" \
   				onchange="if(new Date($(this).val()).getTime()<=new Date($(&#39;YearFrom&#39;).val()).getTime()){alert(&#39;Wrong Year Duration&#39;);$(this).val(&#39; &#39;);}"/>\
   				</div></div></div></div></div>';

   	    	$('#experience-box-container').append('<br/><div id="workexpindiv'+p+'" class="resume-box" >'+date +' '+result+'</div>');
   			$(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true});
   	 		$('#workexpindiv'+p).find('#roleskilllist').empty();

			$('#salary-'+p+' option:selected').removeAttr('selected');
			$('#experience-'+p+' option:selected').removeAttr('selected');
			$('#experience-roleid-'+p+' option:selected').removeAttr('selected');
			$('#industry-id-'+p+' option:selected').removeAttr('selected');

       	 	//var svalue = $('#experience-roleid-'+p).val();
    		//getSkill(svalue,p,'ExperienceSkillId',false,'experience-skill-list-');
   	}

	$(document).on('change', '.load-qualification',function(){
	 var id = $(this).val();
	 var obj = $(this);
	 if(id != ""){
		$.ajax({
			type: "POST",
			url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
			data: {'id': id},
			 success : function(response) {
				var data = JSON.parse(response);
				$(obj).parents('.row:first').find(".load-courses option:not(:first)").remove();
				$.each(data, function( index, value ) {
					$(obj).parents('.row:first').find('.load-courses').append('<option value="'+index+'">'+value+'</option>');
				});

			},

		});
	 }
  });

  $(document).ready(function(){

	$(".white_callar_role").select2({
  		placeholder: "Select Role"
	});

	$(document).on('change', '#white_callar_category', function(){
		var value = $(this).val();
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role0').html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role0').append(option);
						});
					}
				}
			});
		}

	});

	$(document).on('change', '.white_callar_category', function(){
		var value = $(this).val();
		var countC = $(this).attr('data-id-count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['site/getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role'+countC).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role'+countC).append(option);
						});
					}
				}
			});
		}

	});

	$("#white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['site/getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {

						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});

	$("#white_callar_role_exp0").select2({
  		placeholder: "Select Role"
	});
	$("#white_callar_role_exp1").select2({
  		placeholder: "Select Role"
	});

	$(document).on('change', '.white_callar_exp_category', function(){
		var value = $(this).val();
		var countC = $(this).attr('data-id-count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Url::toRoute(['getwhiteroles']);?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role_exp'+countC).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role_exp'+countC).append(option);
						});
					}
				}
			});
		}

	});

	$(".white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Url::toRoute(['getwhiteskills']);?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {

						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});

	/*Remove Resume*/
	$(document).on('click', '.remove-resume', function(){
		$.ajax({
			dataType : "json",
			type : 'GET',
			url : '<?=Url::toRoute(['deleteuserresume']);?>',
			data : false,
			success : function(data) {
				if(data == true){
					$('#delete-resume').html("");
				}
			}
		});
	});

  });
</script>