<?php
$this->title = 'Hire Team - My Career Bugs';

use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\States;
use common\models\City;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';

$education = '';
$latest = '';
$gender = array();
$salaryrange = array();
$daily_Download = Yii::$app->myfunctions->GetTodayPlan('Download', $isassign->plan->PlanId, date("Y-m-d"));
// $daily_CV = Yii::$app->myfunctions->GetTodayPlan('CVDownload',$isassign->plan->PlanId,date("Y-m-d"));
// $daily_whatsApp = Yii::$app->myfunctions->GetTodayPlan('whatsappview',$isassign->plan->PlanId,date("Y-m-d"));
$daily_Email = Yii::$app->myfunctions->GetTodayPlan('Email', $isassign->plan->PlanId, date("Y-m-d"));
// $daily_Contact = Yii::$app->myfunctions->GetTodayPlan('ContactView',$isassign->plan->PlanId,date("Y-m-d"));
$email_limit_reached = "myModal_buy_a_plan";
$download_limit_reached = "myModal_buy_a_plan";
if (isset(Yii::$app->session['Employerid']) && count((array)$isassign) > 0 && $isassign->plan->total_daily_email <= $daily_Email) {
    $email_limit_reached = "daily_limit_reach";
}
if (isset(Yii::$app->session['Employerid']) && count((array)$isassign) > 0 && $isassign->plan->daily_download <= $daily_Download) {
    $download_limit_reached = "daily_limit_reach";
}
$daily_Email = $isassign->plan->total_daily_email - $daily_Email;
$daily_Download = $isassign->plan->daily_download - $daily_Download;

?>


<meta name="keywords"
	content="My Career bugs, MCB, Hire Candidates, Hire Employee, Candidate search" />

<meta name="description"
	content="Find the best Candidate from the exhaustive talent pool at MCB. Reach out to potential hires in one click!" />

<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
#expcollapseTwo .checkbox {
	height: 18px;
}

#expcollapseSix .checkbox {
	height: 18px;
}
</style>
<style>
#keynameSearchContainer {
	z-index: 99999;
	position: relative;
	background-color: white;
	border: 2px;
}

#IndexCitySearchContainer {
	left: 33%;
	z-index: 99999;
	position: relative;
	background-color: white;
	border: 2px;
}
</style>
<div class="wrapper">
	<!-- start main wrapper -->
	<div class="headline job_head" style="display:none">
		<div class=" container">
			<!-- start headline section -->
			<div class="row">
				<div
					class="col-lg-12  col-md-12 col-sm-12 col-xs-12 top-main bg-full margin_auto">
					<h2 class="banner_heading">
						<span>Search </span> Candidate
					</h2>

					<div class="sticky">
					<?php $form = ActiveForm::begin(['options' => ['class' => 'offset-top-10 offset-sm-top-30','id'=>'home_page_form']]); ?>
                        <div class="group-sm group-top">
							<div class="group-item col-md-4 col-xs-12">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-key"></i></span>
										<input type="text" value="<?php echo $keyname?>" class="form-control" name="keysearch"
											id="keysearch" placeholder="Industry Ex : IT , BPO">
									</div>
								</div>
							</div>

							<div class="group-item col-md-3 col-xs-12">
								<div class="form-group">
									<input type="text" class="form-control" value="<?php echo $indexlocation?>" name="location"
										id="candidatelocation" placeholder="Location">
								</div>
							</div>

							<div class="group-item col-md-2 col-xs-6">
								<div class="form-group">
									<select id="form-filter-location" name="experience"
										data-minimum-results-for-search="Infinity"
										class="form-control" tabindex="-1" aria-hidden="true">
										<option value=" ">Experience</option>
										<option value="1" <?php echo $experience == 1 ? 'selected' : ''?>>1 Year</option>
										<option value="2" <?php echo $experience == 2 ? 'selected' : ''?>>2 Year</option>
										<option value="3" <?php echo $experience == 3 ? 'selected' : ''?>>3 Year</option>
										<option value="4" <?php echo $experience == 4 ? 'selected' : ''?>>4 Year</option>
										<option value="5" <?php echo $experience == 5 ? 'selected' : ''?>>5 Year</option>
										<option value="6" <?php echo $experience == 6 ? 'selected' : ''?>>6 Year</option>
										<option value="7" <?php echo $experience == 7 ? 'selected' : ''?>>7 Year</option>
									</select>
								</div>
							</div>

							<div class="group-item col-md-2 col-xs-6">
								<div class="form-group">
									<select id="form-filter-location" name="category"
										data-minimum-results-for-search="Infinity"
										class="form-control" tabindex="-1" aria-hidden="true">
										<option value="">Select By position</option>
        								<?php  foreach ($allcategory as $hk => $hv) {  ?>
        									<option value="<?=$hv->PositionId;?>" <?php echo $category == $hv->PositionId ? 'selected' : ''?>><?=$hv->Position;?></option>
        								<?php   }  ?>
                              </select>
								</div>
							</div>

							<div
								class=" group-item reveal-block reveal-lg-inline-block col-md-1 col-xs-12">
							<?= Html::submitButton('Search', ['name'=>'candidatesearch','class' => 'btn btn-primary element-fullwidth']) ?>
                          </div>

						</div>
                      <?php ActiveForm::end(); ?>
                          <div id="keynameSearchContainer"></div>
						<div id="IndexCitySearchContainer"></div>
					</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
		<!-- end headline section -->
	</div>


	<div class="inner_page" id="pad_sml">
		<div class="container">

			<div class="col-lg-3  col-md-3 col-sm-3 col-xs-12" id="right-side">
				<h4>Hire Team</h4>


				<div class="col-md-122">

					<a href="<?= Url::toRoute(['team/findteam']);?>">Clear </a>
				</div>



				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading sty">
							<div class="col-md-6 no-pad-123">
								<a><h4 class="panel-title"
										onclick="$('#accordion').show();$('#accordion1').hide();">
										<input type="radio" name="serchtype" value="Fresher"
											<?php if(isset($_GET['stype'])){ if($_GET['stype']=='fresher'){ echo 'checked'; }} else{ echo 'checked';}?> />
										Fresher
									</h4></a>
							</div>

							<div class="col-md-6 no-pad-123">
								<a><h4 class="panel-title" onclick="$('#accordion1').show();">
										<input type="radio" name="serchtype" value="Experience"
											<?php if(isset($_GET['stype'])){ if($_GET['stype']=='experience'){ echo 'checked'; }}?> />
										Experience
									</h4></a>
							</div>

						</div>
					</div>
				</div>

				<!---------------fresher box-------------------------->
				<div class="panel-group" id="accordion1" style="display: none;">
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion1"
								href="#expcollapseThree"><h4 class="panel-title">Experience</h4></a>
						</div>
						<div id="expcollapseThree" class="panel-collapse collapse">
							<div class="panel-body">
                           <?php
                        $experience = '';
                        if (isset($_GET['experience'])) {
                            $experience = $_GET['experience'];
                        }
                        ?>
						    <select class="form-control" id="cexperience">
									<option value="">Experience</option>
									<option value="0-1"
										<?php if($experience=='0-1') echo "selected";?>>Below 1 Year</option>
											<?php
        for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
            $frn = $fr + 1;
            ?>
										<option value="<?=$fr.'-'.$frn;?>"
										<?php if($experience==$fr.'-'.$frn) echo "selected";?>><?=$fr.'-'.$frn;?> Years</option>
															   <?php
        }
        ?>
							</select>
							</div>
						</div>
					</div>

					<div class="panel panel-default" style="display: none">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion1"
									href="#expcollapseFour"> Reports</a>
							</h4>
						</div>
						<div id="expcollapseFour" class="panel-collapse collapse">
							<div class="panel-body"></div>
						</div>
					</div>
				</div>







				<div class="panel-group" id="accordion">
				    
				    
				    
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseSeven"><h4 class="panel-title">Profile Freshness
								</h4></a>
						</div>
						<div id="collapseSeven" class="panel-collapse collapse">
							<div class="panel-body">
								<?php
        if (isset($_GET['latest']) && $_GET['stype'] == 'fresher') {
            $latest = $_GET['latest'];
        }
        ?>
								<select class="form-control" id="fcfreshness">
									<option value="">Select</option>
									<option value="1"
										<?php if($latest==1) echo "selected='selected'";?>>1 Days</option>
									<option value="3"
										<?php if($latest==3) echo "selected='selected'";?>>3 Days</option>
									<option value="7"
										<?php if($latest==7) echo "selected='selected'";?>>7 Days</option>
									<option value="15"
										<?php if($latest==15) echo "selected='selected'";?>>15 Days</option>
									<option value="30"
										<?php if($latest==30) echo "selected='selected'";?>>30 Days</option>
								</select>
							</div>
						</div>
					</div>


					<!-- New Catogory -->
					<div class="panel panel-default" >
						
						 <div class="panel-heading" style="position:relative;">
							 <span style="position: absolute;right: 0px;color: #fff;font-size: 11px;background: #f87d39; padding: 4px 9px 2px 9px; border-radius: 10px 0 0 10px;top: 9px;">New</span>
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne19"><h4 class="panel-title">
									 Category, Role and Skill
								</h4></a>
							</div>

						<div id="collapseOne19" class="panel-collapse collapse" style="margin-bottom:10px">      
							<div class="panel-group" style="padding:0 10px 10px 10px; margin-bottom:0px">
								<div class="panel panel-default" style="border:0px;box-shadow: none;">
									<div class="panel-body">
										<select class="questions-category form-control" name="WhiteCategory" tabindex="0" aria-hidden="true" id="white_callar_category_can">
										 <option value="" >Select Category</option>
										 <?php if(!empty($whiteCategories)){
												foreach($whiteCategories as $categoryId => $categoryName){?>
													<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
												<?php  }
												}?>
										 
										</select> <br/>
										<select class="questions-category form-control" name="WhiteRole[]" id="white_callar_role_can" multiple="multiple"></select> <br/>
										
										<select class="questions-category form-control" id="white_callar_skills_can" name="WhiteSkills[]" multiple="multiple"></select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- New Catogory Role and skill-->

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#education_tab">Education</a>
							</h4>
						</div>
						<div id="education_tab" class="panel-collapse collapse">
							<div class="panel-body">
				<?php
    if (isset($_GET['education']) && $_GET['stype'] == 'fresher') {
        $education = $_GET['education'];
    }
    if ($NaukariQualData) {
        ?>
					<select class="form-control" id="fceducation" style="margin: 0 0 7px 0">
							<option value="">Highest Qualification</option>
						<?php foreach ($NaukariQualData as $key => $value) { ?>
							<option value="<?=$value->id;?>"><?=$value->name;?></option>
						<?php } ?>
					</select>
					<?php } ?>
				<select class="questions-category form-control"
									id="education-course" style="margin: 0 0 7px 0"><option
										value="">Course</option></select> <select
									class="questions-category form-control"
									id="education-specialization"
									style="margin: 0 0 7px 0; width: 100%"></select>
							</div>
						</div>
					</div>
					
					


					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion1"
									href="#expcollapseSix"> Industry </a>
							</h4>
						</div>
						<div id="expcollapseSix" class="panel-collapse collapse">
							<div class="panel-body"
								style="max-height: 200px; overflow: auto;">
				<?php
    if (isset($_GET['industry']) && $_GET['industry'] != '') {
        $industry = explode(",", $_GET['industry']);
    } else {
        $industry = array();
    }
    foreach ($allindustry as $sk => $svalue) {
        ?>
				
									<div class="checkbox">
									<label> <input type="checkbox" class="cindustryby"
										value="<?=$svalue->IndustryId;?>"
										<?php if(in_array($svalue->IndustryId,$industry)) echo "checked";?>>
										<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
										<a><?=$svalue->IndustryName;?> </a>
									</label>
								</div>
								
				<?php
    }
    ?>
			</div>
						</div>
					</div>
					

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion1"
									href="#expcollapseTwo"> Salary </a>
							</h4>
						</div>
						<div id="expcollapseTwo" class="panel-collapse collapse">
							<div class="panel-body"
								style="max-height: 200px; overflow: auto;"> 
						            <?php
                if (isset($_GET['salaryrange']) && $_GET['salaryrange'] != '') {
                    $salaryrange = explode(",", $_GET['salaryrange']);
                }

                ?>
						            <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary" value="0-1.5"
											<?php if(in_array('0-1.5',$salaryrange)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
											0 - 1.5 Lakh
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary" value="1.5-3"
											<?php if(in_array('1.5-3',$salaryrange)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>1.5
											- 3 Lakh
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary" value="3-6"
											<?php if(in_array('3-6',$salaryrange)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>3
											- 6 Lakh
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary" value="6-10"
											<?php if(in_array('6-10',$salaryrange)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>6
											- 10 Lakh
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary" value="10-15"
											<?php if(in_array('10-15',$salaryrange)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>10
											- 15 Lakh
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary" value="15-25"
											<?php if(in_array('15-25',$salaryrange)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>15
											- 25 Lakh
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="csalary"
											value="Above 25"
											<?php if(in_array('Above 25',$salaryrange)) echo "checked";?>>
											<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Above
											25 Lakh
										</label>
									</div>
								</span>
							</div>
						</div>
					</div>





					<div class="panel panel-default">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseOne13"><h4 class="panel-title">Role</h4></a>
						</div>
						<div id="collapseOne13" class="panel-collapse collapse">
							<div class="panel-body">
						<?php
    if (isset($_GET['role'])) {
        $role1 = $_GET['role'];
    } else {
        $role1 = '';
    }
    ?>
                           <select class="form-control"
									onchange="selectpositionnew(this.value)" id="crole"><option
										value="">Select</option>
						   <?php
        foreach ($allcategory as $rk => $rvalue) {
            ?>
						   <option value="<?=$rvalue->PositionId;?>"
										<?php if($role1==$rvalue->PositionId) echo "selected='selected'";?>><?=$rvalue->Position;?>
						   </option>
						   <?php
        }
        ?>
						   </select><br> <br>
								<h4 class="panel-title">Job By Skill</h4>
								<br>
								<div class="panel-body" id="jobbyskill"
									style="max-height: 200px; overflow: auto;">
							<?php

foreach ($allskill as $sk => $svalue) { /*
                                         * ?>
                                         * <span>
                                         * <div class="checkbox">
                                         * <label> <input type="checkbox" class="cskillby" value="<?=$svalue->SkillId;?>" <?php if(!empty($skillby) && in_array($svalue->SkillId,$skillby)) echo "checked";?>>
                                         * <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                         * <a><?=$svalue->Skill;?> </a>
                                         * </label>
                                         * </div>
                                         * </span>
                                         * <?php
                                         */
    }
    ?>
								</div>
							</div>
						</div>
					</div>
					
					
					
					<div class="panel panel-default" style="display:none">
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseTwo"><h4 class="panel-title">Gender</h4></a>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body"> 
						            <?php
                if (isset($_GET['gender']) && $_GET['stype'] == 'fresher') {
                    $gender = explode(",", $_GET['gender']);
                }
                ?>
						            <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="cfgender" value="Male"
											<?php if(in_array('Male',$gender)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
											Male
										</label>
									</div>
								</span> <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="cfgender" value="Female"
											<?php if(in_array('Female',$gender)) echo "checked";?>> <span
											class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>Female
										</label>
									</div>
								</span>

							</div>
						</div>
					</div>

					<div class="panel panel-default" >
						<div class="panel-heading">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapseThree"><h4 class="panel-title">Show Profiles</h4></a>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
                           <?php
                        if (isset($_GET['cresume'])) {
                            $showprofile = $_GET['cresume'];
                        } else {
                            $showprofile = '';
                        }
                        ?>
						         <span>
									<div class="checkbox">
										<label> <input type="checkbox" class="cresume" value="1"
											<?php if($showprofile==1) echo "checked";?>> <span class="cr"><i
												class="cr-icon glyphicon glyphicon-ok"></i></span> With
											resume only
										</label>
									</div>
								</span>

							</div>
						</div>
					</div>


					<!--------------------------fresher box---------------------->





					<div class="panel panel-default"  style="display:none">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#age_tab"> Age</a>
							</h4>
						</div>
						<div id="age_tab" class="panel-collapse collapse">
							<div class="panel-body">
								<select class="questions-category form-control"
									id="condidateage">
									<option value="">Select</option>
									<?php for($age=14; $age <= 45; $age++){ ?>
									
										<option value="<?php echo $age ?>"
										<?php echo ($_GET['condidateage']==$age)?'selected="selected"':''; ?>><?php echo $age?></option>
									<?php } ?>
									</select>
							</div>
						</div>
					</div>




					<div class="panel panel-default"  style="display:none">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#worktype_tab"> Work Type</a>
							</h4>
						</div>
						<div id="worktype_tab" class="panel-collapse collapse">
							<div class="panel-body">
								<select class="questions-category form-control" id="worktype">

									<option value="">Select</option>
									<option value="Part Time"
										<?php echo ($_GET['worktype']=='Part Time')?'selected="selected"':''; ?>>Part
										Time</option>
									<option value="Full Time"
										<?php echo ($_GET['worktype']=='Full Time')?'selected="selected"':''; ?>>Full
										Time</option>
									<option value="Work From Home"
										<?php echo ($_GET['worktype']=='Work From Home')?'selected="selected"':''; ?>>Work
										From Home</option>
									<option value="Internships"
										<?php echo ($_GET['worktype']=='Internships')?'selected="selected"':''; ?>>Internships</option>

								</select>

							</div>
						</div>
					</div>





					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#whatsapp_tab"> WhatsApp No</a>
							</h4>
						</div>
						<div id="whatsapp_tab" class="panel-collapse collapse">
							<div class="panel-body">

								<select class="questions-category form-control"
									id="whatsapp_contact">

									<option value="">All</option>
									<option value="Y">Yes</option>
									<option value="N">No</option>
								</select>
							</div>
						</div>
					</div>


	<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#location_tab">Location </a>
							</h4>
						</div>
						<div id="location_tab" class="panel-collapse collapse">
							<div class="panel-body"> 
								<?php $stateList = States::getStateList();?>
								<select style="width:100%" class="form-control" id="filter-state" >
									<option value="">Select State</option>
									<?php foreach ($stateList as $stateId => $stateName) { ?>
									<option value="<?=$stateId;?>"><?=$stateName;?></option>
									<?php  } ?>
								</select><br/>
								<select style="width:100%" class="form-control" id="filter-city" >
									<option value="">Select City</option>
								</select>
								
							</div>
						</div>
					</div>

			



					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#preferredlocation_tab"> Preferred Location </a>
							</h4>
						</div>
						<div id="preferredlocation_tab" class="panel-collapse collapse">
							<div class="panel-body"> 
								<?php $PreferredLocation = explode(',', $PreferredLocation); ?>
									<select
									class="questions-category form-control mdbselect colorful-select dropdown-primary"
									id="PreferredLocation" style="width: 100%">
								</select>
							</div>
						</div>
					</div>
					</div>	
				
				<!-----------------------------experience box------------------->

				<!----------------------- 
		<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"></label>
							<div class="cols-sm-10">
								<div class="input-group" style="width:100%">
								
								</div>
							</div>
						</div>
						
						
					
					
							<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">WhatsApp</label>
							<div class="cols-sm-10">
					 
									<select class="questions-category form-control" id="whatsapp_contact">
									
										<option value="">All</option>
										<option value="Y">Yes</option>
										<option value="N">No</option>
									</select>
									 
							</div>
						</div>
						---------------------->

				<div style="height: 6px"></div>

				<div class="form-group">
					<label for="email" class="cols-sm-2 control-label">Language</label>
					<div class="cols-sm-10">
						<div class="input-group" style="width: 100%">

							<select multiple="multiple"
								class="questions-category form-control" id="LanguageId"
								name="AllUser[Language][]">
							</select>
						</div>
					</div>
				</div>
				<div class="spacer-5"></div>

				<div class="price_block" style="display:none">
					<div class="widget-heading">
						<span class="title"> Our Plans </span>
					</div>
					<div id="orange_payment_block">     
								 <?php
        if ($allplan) {
            foreach ($allplan as $key => $pland) {
                ?>
									   <div class="widget_inner">
							<div class="panel price panel-red">
								<div class="panel-body text-center">
									<p class="lead">
										<strong><?=$pland->PlanName;?></strong>
									</p>
								</div>
								<ul class="list-group list-group-flush text-center width-half">
									<li class="list-group-item"><i class="icon-ok text-danger"></i> Rs/- <?=$pland->Price;?> </li>
									<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalSMS;?> SMS </li>
									<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalEmail;?> Email </li>
									<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->CVDownload;?> CV </li>
									<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->TotalDownload;?> Download </li>
									<li class="list-group-item"><i class="icon-ok text-danger"></i><?=$pland->ViewContact;?> Candidate View </li>
								</ul>
								<div class="panel-footer">
									<a class="btn btn-lg btn-block btn-danger" href="#">BUY NOW!</a>
								</div>
							</div>
							<!-- /PRICE ITEM -->
						</div>
									   <?php
            }
        }
        ?>  
		
							</div>
				</div>
			</div>
			<div class="col-lg-9  col-md-9 col-sm-9 col-xs-12">
				<div class="pannel_header margin_top">
					<div class="width-13">
						<div class="checkbox">
							<label> <input type="checkbox" id="empchk"> <span class="cr"><i
									class="cr-icon glyphicon glyphicon-ok"></i></span>
							</label>
						</div>
					</div>

					<div class="width-14" data-toggle="modal"
						data-target="#myModal_buy_a_plan" style="display: none">
						<div class="checkbox">
							<label> Sms </label>
						</div>
					</div>
					<div class="width-14">
						<div class="checkbox"
							<?php if(isset(Yii::$app->session['Employerid']) && $daily_Email>0) { ?>
							onclick="if($('.empch').prop('checked')==true){mailtoemp('myModal_email');}else{alert('Please check the checkbox to send mail');}"
							<?php }else{ ?> data-toggle="modal"
							data-target="#<?=$email_limit_reached?>" <?php }?>>
							<label> Mail </label>
						</div>
					</div>
					<div class="width-14">
						<div class="checkbox"
							<?php if(isset(Yii::$app->session['Employerid']) && $daily_Download>0) { ?>
							onclick="if($('.empch').prop('checked')==true){download();}else{alert('Please check the checkbox to download');}"
							<?php }else{ ?> data-toggle="modal"
							data-target="#<?=$download_limit_reached?>" <?php }?>>
							<label> Download </label>
						</div>
					</div>
					<div class="width-10" id="select_per_page">
						<div class="form-groups">
							<div class="col-sm-8">
								<label>Result Per page </label>
							</div>
							<div class="col-sm-4">
								<select id="form-filter-perpage"
									data-minimum-results-for-search="Infinity"
									class="form-control23" tabindex="-1" aria-hidden="true">
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="30">30</option>
									<option value="40">40</option>
									<option value="50">50</option>
								</select>
							</div>
						</div>
					</div>

					<div class="pannel_header" style="margin: 0px;">
						<span id="searchdivc"
							style="margin-left: 10px; font-size: 14px; color: #f16b22;"></span>
					</div>

				</div>
				<div id="mobile_design">
							<?= $this->render('_findteam_ajax', array('candidate'=>$candidate,'pages'=>$pages,'allplan'=>$allplan,'planassign'=>$planassign,'url'=>$url,'imageurl'=>$imageurl,'isassign'=>$isassign,'education'=>$education,'latest'=>$latest,'gender'=>$gender,'salaryrange'=>$salaryrange)); ?>
						</div>
			</div>
		</div>
	</div>

	<!-----myModal_email------>
	<div class="modal fade" id="myModal_email" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Mail To Candidate</h4>
				</div>

				<div class="row main">
					<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<input type="hidden" id="MailUserId" class="form-control"
							name="Mail[UserId]">
						<div class="form-group" style="display: none;">
		<?= $form->field($mailmodel, 'EmployeeId')->textInput(['id'=>'EmployeeId','maxlength' => true])->label(false) ?>
		</div>
						<div class="form-group">
		<?= $form->field($mailmodel, 'Subject')->textInput(['maxlength' => true,'Placeholder'=>'Subject'])->label(false) ?>
		</div>
						<div class="form-group">
		<?= $form->field($mailmodel, 'JobId')->dropDownList($allpost)->label(false) ?>
		</div>
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<label>Job Description* </label>
										
								<?=$form->field($mailmodel, 'MailText')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small','language' => 'en_CA','clientOptions' => ['plugins' => ["advlist autolink lists link charmap print preview anchor","searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);?>
                              </div>

						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green']) ?>
								</div>
						</div>
		<?php ActiveForm::end(); ?>
		</div>
				</div>
			</div>
		</div>
	</div>
	<!-----myModal_email------>

	<div class="border"></div>
</div>
<!-- end main wrapper -->

<script type="text/javascript">
	var xhr;
		setTimeout(function(){
			$("#state").prepend("<option value='' selected='selected'>Select State</option>");
				<?php
    if (isset($_GET['state']) && $_GET['stype'] == 'experience') {
        ?>
				var state='<?=$_GET['state'];?>';
				$('#state').val(state);
				<?php
    }
    ?>
		},2000);
		
		

		function mailtosemp(id,email,UserId) {
			$('#EmployeeId').val(email);
			$('#MailUserId').val(UserId);
			$('#'+id).modal('show');
			var page = $('ul.pagination li.active a').data('page');
    		candidatefilterNew((page+1));
        }
		
		
		
		function cvdownload(UserId) {
            $.ajax({url:"<?= Url::toRoute(['site/cvdownload'])?>?UserId="+UserId,
				   success:function(results)
				   {
					var page = $('ul.pagination li.active a').data('page');
    				candidatefilterNew((page+1));
				   }
			});
        }
		
		function viewcontact(UserId) {
            $.ajax({url:"<?= Url::toRoute(['site/contactview'])?>?UserId="+UserId,
				   success:function(results)
				   {
					var page = $('ul.pagination li.active a').data('page');
    				candidatefilterNew((page+1));
				   }
			});
        }
        function viewwhatsapp(UserId,e,anc) {
        	e.preventDefault();
            $.ajax({
               url:"<?= Url::toRoute(['site/whatsappview'])?>?UserId="+UserId,
			   success:function(results)
			   {
				   if(results.status == 'NOK'){
					   window.location.href = "<?= Url::toRoute(['site/hirecandidate'])?>";
				   }else{
					    var page = $('ul.pagination li.active a').data('page');
	    		    	candidatefilterNew((page+1));
	    		    	window.location.href = $(anc).attr('href');
				   }
			   }
			});
        }
		
		/*$(document).ready(function(){
			$('.multiselect-ui').multiselect({
				includeSelectAllOption: true
			});
		});*/
	function candidatefilterNew(page) {
		var dis_searchresult = "";
   		var education=$('#fceducation').val();
   		if(education!=''){
   			dis_searchresult+=' > '+$('#fceducation option:selected').text();
   		}
    	var latest=$('#fcfreshness').val();
    	if(latest!=''){
   			dis_searchresult+=' > '+$('#fcfreshness option:selected').text();
   		}
    	var gender=$('.cfgender:checked').map(function () {
						return this.value;
					}).get();
    	if(gender!=''){
    		dis_searchresult+=' > Gender:'+gender;
   		}
    	var cresume=$('.cresume:checked').map(function () {
						return this.value;
					}).get();
    	if(cresume!=''){
   			dis_searchresult+=' > with resume';
   		}
     	
     	var condidateage=$('#condidateage').val();
     	if(condidateage!=''){
   			dis_searchresult+=' > Age:'+$('#condidateage option:selected').text();
   		}
     	
	 	var worktype=$('#worktype').val();
	 	if(worktype!=''){
   			dis_searchresult+=' > '+$('#worktype option:selected').text();
   		}
	  	var PreferredLocation=$('#PreferredLocation').val();
	  	if(PreferredLocation!=''){
   			dis_searchresult+=' > '+$('#PreferredLocation option:selected').text();
   		}
	 	var crole  =  $('#crole').val();
	 	if(crole!=''){
   			dis_searchresult+=' > '+$('#crole option:selected').text();
   		}
   		var skill = $('.cskillby:checked').map(function () {
						return this.value;
					}).get();
     	if(skill!=''){
     		var skillss = $('.cskillby:checked').map(function() {
			  	return $(this).parents('.checkbox').find('a').html();
		  	}).get();
   			dis_searchresult+=' > '+skillss;
   		}
	 	var course = $('#education-course').val();
	 	if(course!=''){
   			dis_searchresult+=' > '+$('#education-course option:selected').text();
   		}
	 	var specialization = $('#education-specialization').val();
	 	if(specialization!=''){
   			dis_searchresult+=' > '+$('#education-specialization option:selected').text();
   		}
	 	var whatsapp_contact = $('#whatsapp_contact').val();
	 	if(whatsapp_contact!=''){
   			dis_searchresult+=' > WhatsApp';
   		}
   		var language='';
     	var languages=$('#LanguageId').val();
     	if(languages!=null){
     		language = languages.join(",");
     		dis_searchresult+=' > '+language;
     	}
		
		var state = $('#filter-state').val();
     	if(state != ''){
			state = $('#filter-state option:selected').text();
   			dis_searchresult+=' > State:'+$('#filter-state option:selected').text()
   		}
		
		var city = $('#filter-city').val();
		if(city != ''){
			city = $('#filter-city option:selected').text();
   			dis_searchresult+=' > City:'+$('#filter-city option:selected').text()
   		}
		
		var collarRolleText = $('#white_callar_role_can option:selected').map(function() {
			return $(this).text();
			}).get();
			collarRolle = $('#white_callar_role_can option:selected').map(function() {
				return $(this).val();
			}).get();
			if(collarRolle !=  ""){
				dis_searchresult += " > Role : " + collarRolleText;
			}
			
			var collarSkillText = $('#white_callar_skills_can option:selected').map(function() {
				return $(this).text();
			}).get();
			collarSkills = $('#white_callar_skills_can option:selected').map(function() {
				return $(this).val();
			}).get();
			if(collarSkills !=  ""){
				dis_searchresult += " > Skills : " + collarSkillText;
			}
			
			category = $('#white_callar_category_can').val();
			if(category != ""){
				dis_searchresult += " >Job Category : " + $('#white_callar_category_can option:selected').html();
			}
		
	 	var perpage = $('#form-filter-perpage').val();
	 	var url="<?=Url::toRoute(['team/findteam'])?>?education="+education+"&latest="+latest+"&gender="+gender+"&cresume="+cresume+"&skill="+skill+"&role="+crole+"&condidateage="+condidateage+"&worktype="+worktype+"&PreferredLocation="+PreferredLocation+"&course="+course+"&specialization"+specialization+"&language="+language+"&whatsapp_contact="+whatsapp_contact+"&perpage="+perpage+"&state="+state+"&city="+city+"&category_id="+category+"&collar_role="+collarRolle+"&collar_skills="+collarSkills;
	 	
	 	if($('input[name=serchtype]:checked').val()=='Experience'){
	 		var cexperience=$('#cexperience').val();
			if(cexperience!=''){
	 			dis_searchresult+=' > Experience:'+$('#cexperience option:selected').text();
   			}
   			var industry = $('.cindustryby:checked').map(function () {
						return this.value;
					}).get();
			if(industry!=''){
	 			var sss = $('.cindustryby:checked').map(function() {
			  		return $(this).parents('.checkbox').find('a').html();
		  		}).get();
   				dis_searchresult+=' > '+sss;
   			}
	 		var salary = $('.csalary:checked').map(function () {
						return this.value;
					}).get();

	 		if(salary!=''){
	 			var ss = $('.csalary:checked').map(function() {
			  		return this.value+" lakh";
		  		}).get();
   				dis_searchresult+=' > '+ss;
   			}
	 		url+="&salaryrange="+salary+"&industry="+industry+"&experience="+cexperience+"&stype=experience";
	 	}else{
	 		url+="&stype=fresher";
	 	}
    	if(page!=0){
    		url+="&page="+page;
    	}
    	if(xhr){
            xhr.abort();
        }
    	xhr = $.ajax({
    		url: url,
    		type : 'GET',
    		success: function(result){
    			$("#mobile_design").html(result);
    			if(dis_searchresult!=''){
    				dis_searchresult = 'Results > '+$.trim(dis_searchresult).substr(1);
    			}
    				$("#searchdivc").html(dis_searchresult);
    				
  			}
  		});
	}

	$(document).on('change', '#fceducation', function() {
		if($(this).val()!=''){
			$.ajax({
            type: "POST",
            url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
            data: {'id': $(this).val()},
            success : function(response) {
            	var data = JSON.parse(response);
            	$("#naukari-course option").remove();
            	$("#naukari-board option").remove();
            	$.each(data, function( index, value ) {
  					$("#education-course").append('<option value="'+index+'">'+value+'</option>');
				});
        	}, 
        	})
		}else{
			var selectList = $("#education-course option");
			selectList.find("option:gt(0)").remove();
		}
		
		candidatefilterNew(0);
	});
	$(document).on('change', '#form-filter-perpage', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#education-course', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#LanguageId', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#education-specialization', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#fcfreshness', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#condidateage', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#worktype', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#PreferredLocation', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#crole', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '.cfgender', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '.cresume', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '.cskillby', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#whatsapp_contact', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#cexperience', function() {
		candidatefilterNew(0);
	});
	$('input[class=csalary]').change(function(){
		candidatefilterNew(0);
	});
	$('input[class=cindustryby]').change(function(){
		candidatefilterNew(0);
	});
	$('input[name=serchtype]').change(function(){
		candidatefilterNew(0);
	});
	
	$(document).on('change', '#white_callar_category_can', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#white_callar_role_can', function() {
		candidatefilterNew(0);
	});
	$(document).on('change', '#white_callar_skills_can', function() {
		candidatefilterNew(0);
	});
	var data = [{id: '',text: 'Specialization'}];
	var Ldata = [{id: '',text: 'Select Language'}];
	var Locdata = [{id: '',text: 'Select Location'}];
	<?php foreach ($naukari_specialization as $key => $value) { ?>
		var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
		data.push(d);
	<?php } ?>
	<?php foreach ($language_opts as $key1 => $value1) { ?>
		var ld = {id: '<?=$value1->name?>',text: '<?=$value1->name?>'};
		Ldata.push(ld);
	<?php }  ?>
	$("#education-specialization").select2({
  		data: data
	});
	$("#LanguageId").select2({
  		data: Ldata,
  		maximumSelectionLength: 5
	});
	
	<?php foreach($cities as $city){ ?>
		var lld = {id: '<?=$city->CityId?>',text: '<?=$city->CityName?>'};
		Locdata.push(lld);
	<?php } ?>
	$("#PreferredLocation").select2({
  		data: Locdata
	});
	$(document).on('click', 'ul.pagination li a', function(event) {
		event.preventDefault();
    	var page = $(this).data('page');
    	candidatefilterNew((page+1))
	});


	$(document).on('change', '#empchk', function() {
		if($(this).prop("checked")){
			$('.empch').prop('checked',true);
		}else{
			$('.empch').prop('checked',false);
		}
	});
	function selectpositionnew(pid){
		if($('input[name=serchtype]:checked').val()=='Experience'){
			var type = 'ecandidate';
		}else{
			var type = 'candidate';
		}
		if(pid!=''){
			$.ajax({url:mainurl+"site/skills?roleid="+pid+"&type="+type,
               success:function(results)
            {
                $('#jobbyskill').html(results);
            }
        	});
		}else{
			$('#jobbyskill').html('');
		}
	    
	}
	
	var baseUrl = "<?= Url::toRoute(['/'])?>";
	$(document).ready(function(){
		
		$(document).on('change', '#filter-state', function(){
			var stateId = $(this).val();
			$.get(baseUrl+'site/getcity?stateId='+stateId, function(res){
    		$('#filter-city').empty();
    		$('#filter-city').append(res.result);
    	});
		});
		
		
		
		$("#white_callar_role_can").select2({
			placeholder: "Please select role",
			allowClear: true
		});
	
		$(document).on('change', '#white_callar_category_can', function(){
			var value = $(this).val();
			if(value != ""){
				$.ajax({
					dataType : "json",
					type : 'GET',
					url : '<?=Yii::$app->urlManager->baseUrl."/getwhiteroles"?>',
					data : {
						category_id : value
					},
					success : function(data) {
						$('#white_callar_role_can').html("");
						if(data != ""){
							$.each(data, function(key, val) {
								var option = $('<option />');
								option.attr('value', key).text(val);
								$('#white_callar_role_can').append(option);
							});
						}
					}
				});
			}
			
		});
	
		$("#white_callar_skills_can").select2({
			maximumSelectionLength: 5,
			placeholder: "Select Skills",
			ajax: {
				url: '<?=Yii::$app->urlManager->baseUrl."/getwhiteskills"?>',
				dataType: 'json',
				type: "GET",
				data: function (params) {
					return {
						q: params.term,
						page: 1
					  }
				},
				processResults: function(data){
					return {
						results: $.map(data.results, function (item) {
							
							return {
								text: item.name,
								id: item.id
							}
						})
					};
				}
			 }
		});
	});
</script>
<style type="text/css">
.black_overlay {
	display: none;
	position: absolute;
	top: 0%;
	left: 0%;
	width: 100%;
	height: 100%;
	background-color: black;
	z-index: 1001;
	-moz-opacity: 0.8;
	opacity: .80;
	filter: alpha(opacity = 80);
}
</style>