<?php
$this->title = 'Team';

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use bupy7\cropbox\CropboxWidget;

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

$url = '/backend/web/';

?>

<style>
#team_upload_photo {
	clear: both;
	overflow: hidden
}



.find_a_job, .need_help {display:none;}
#spc .select2.select2-container.select2-container--default{width:100% !important;}

.input-group span{font-size:11px; }
.select2-results__option{font-size:12px; }

#team_upload_photo .col-sm-2 {
	padding: 0px;
	margin: 0 10px 0 0;
	width: 18%
}

#team_upload_photo .col-sm-10 {
	padding: 0px;
	margin: 0;
	width: 79%;
	float: right
}

#team_upload_photo .col-sm-2 label {
	text-align: right;
	width: 100%
}

#team_upload_photo .clear {
	height: 2px
}

#team_upload_photo .form-control {
	height: 33px
}
.expextra .select2-container{width:100%!important}
</style>


<link
	href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"
	rel="stylesheet" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<div id="wrapper">
	<!-- start main wrapper -->



	<div class="headline_inner">

		<div class="row">

			<div class=" container">
				<!-- start headline section -->
				<h2>Team Members</h2>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- end headline section -->
	</div>

	<div class="inner_page">

		<div class="container">



				 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data']]); ?>

					<div class="ros">



				<div class="col-xs-12 col-sm-6">

					<h5 class="page-head">Team Leader Information</h5>

					<?php
    $name = '';
    if (isset(Yii::$app->session['SocialName']) && Yii::$app->session['SocialName'] != '') {
        $name = 'value="' . Yii::$app->session['SocialName'] . '" readonly';
    }
    $email = '';
    if (isset(Yii::$app->session['SocialEmail']) && Yii::$app->session['SocialEmail'] != '') {
        $email = 'value="' . Yii::$app->session['SocialEmail'] . '" readonly';
    }

    ?>

						<div class="form-group">
						<label for="name" class="cols-sm-2 control-label">Your Name</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" <?=$name;?> name="AllUser[Name]" id="name"
									placeholder="Enter your Name" required />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">Your Email</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope fa"
									aria-hidden="true"></i></span> <input type="email"
									class="form-control" <?=$email;?> name="AllUser[Email]"
									id="email" placeholder="Enter your Email" required />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Mobile No</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone-square fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[MobileNo]" id="MobileNo"
									placeholder="Enter your MobileNo"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10" required />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> WhatsApp No</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone-square fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[WhatsAppNo]" id="WhatsAppNo"
									placeholder="Enter your WhatsApp No"
									onkeypress="return numbersonly(event)"
									onblur="return IsMobileno(this.value);" maxlength="10" />
							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Address</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope fa"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[Address]" id="address"
									placeholder="Enter your Address" required />
							</div>
						</div>
					</div>



					<div class="form-group" style="display:none">
						<label for="confirm" class="cols-sm-2 control-label">Country </label>
						<div class="cols-sm-12">
							<div class="input-group full">
								<select class="questions-category countries form-control "
									tabindex="0" aria-hidden="true" id="countryId"
									name="AllUser[Country]" required>
									<option value="India" countryid="101">India</option>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label"> State
						</label>

							<div class="col-sm-4" style="padding:0px; float:right">
								<div class="input-group full" style="width:100%">
							<select style="width:100%" class="questions-category cities form-control "
								tabindex="0" aria-hidden="true" id="cityId" name="AllUser[City]"
								required>
								<option value="">Select City</option>
							</select>
						</div>
						</div>


						<div class="col-sm-5" style="padding:0px;margin:0 4% 0 0; float:right">
							<div class="input-group full" style="width:100%">
								<select style="width:100%" class="questions-category states form-control "
									tabindex="0" aria-hidden="true" id="stateId"
									name="AllUser[State]" required>
									<option value="">Select State</option>
								</select>
							</div>
						</div>


					</div>


					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Pincode </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-thumb-tack"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[PinCode]" id="confirm"
									placeholder="Pincode" maxlength="6" required
									onkeypress="return numbersonly(event)" autocomplete="off">
							</div>
						</div>
					</div>







					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg"
									aria-hidden="true"></i></span> <input type="password"
									class="form-control" name="AllUser[Password]" id="password"
									placeholder="Enter your Password" required autocomplete="off" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Confirm
							Password</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock fa-lg"
									aria-hidden="true"></i></span> <input type="password"
									class="form-control" name="AllUser[ConfirmPassword]" id="confirm"
									placeholder="Confirm your Password" required
									onblur="if($(this).val()!=$('#password').val()){alert('Password  and Confirm password Must Be Same');$(this).val('');setTimeout(function() {  $('#password').focus();return false; }, 10);}" />
							</div>
						</div>
					</div>


















					<div class="form-group" id="team_upload_photo">

						<div class="col-sm-2">

							<label for="confirm" class=" control-label">Your Profile Photo </label>

						</div>

						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-12">

										<!--<label class="btn btn-default btn-file design-1">

										Browse <input type="file"  name="AllUser[PhotoId]" accept=

"image/*" style="display: none;"  onchange="$(this).parent().next().html($(this).val());">

									</label><span></span>-->

                                <?php echo $form->field($docmodel, 'Doc')->widget(CropboxWidget::className(), [
                                      'croppedDataAttribute' => 'crop_info',
                                ])->label(false);?>
								</div>
							</div>
						</div>

						<div class="clear"></div>


						<div class="form-groups field-alluser-cvfile" style="margin-bottom:10px">
<label class="control-label" for="alluser-cvfile">Team Cv File</label>
<input type="hidden" name="AllUser[cvFile]" value=""><input type="file" id="alluser-cvfile" name="AllUser[cvFile]">

<p class="help-block help-block-error"></p>
</div>

	<div class="clear"></div>


							<div class="form-groups" style="margin-bottom:10px;">
						<label for="confirm" class="cols-sm-2 control-label">   Salary
						</label>

							<div class="col-sm-10" >
							    		<div class="input-group full" style="width: 96%; margin-left: 1%;">

							<select style="width:100%" class="form-control" name="AllUser[MySalary]" >
                							<option value="">   Salary	</option>
											<option value="0-1.5">0 - 1.5 Lakhs</option>
											<option value="1.5-3">1.5 - 3 Lakhs</option>
											<option value="3-6">3 - 6 Lakhs</option>
											<option value="6-10">6 - 10 Lakhs</option>
											<option value="10-15">10 - 15 Lakhs</option>
											<option value="15-25">15 - 25 Lakhs</option>
											<option value="Negotiable">Negotiable</option>
                							</select>
						</div>
						</div>

							<div class="clear"></div>

					</div>


















						<div class="form-groups" style="margin-bottom:10px">
							<label for="email" class="cols-sm-2 control-label"> Number of
								team members </label>
							<div class="col-sm-10">
								<div class="input-group full"
									style="width: 96%; margin-left: 1%;">
									<select id="form-filter-location"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control " tabindex="0"
										aria-hidden="true" name="AllUser[NoOfTeamMember]">
										<option value="">Select numbers</option>
                                <?php
                                for ($t = 0; $t <= 20; $t ++) {
                                    ?>
								<option value="<?=$t;?>"><?=$t;?></option>
								<?php
                                }
                                ?>
                              </select>
								</div>
							</div>
						</div>
	<div class="clear"></div>


					</div>

					<div class="form-groups" style="margin-bottom:10px">

						<label for="confirm" class="cols-sm-2 control-label">Team Description </label>
						<div class="cols-sm-10">
							<div class="input-group full">
								<textarea class="form-control textarea-small"
									name="AllUser[TeamDesc]" maxlength="250"></textarea>
							</div>
						</div>
					</div>
				</div>

				<!--Education-->











				<div class="col-xs-12 col-sm-6">

					<h5 class="page-head">Your Education</h5>
					<div class="form-group" style="display: none;">
						<label for="name" class="cols-sm-2 control-label">Highest
							Qualification</label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-file-text"
									aria-hidden="true"></i></span> <input type="text"
									class="form-control" name="AllUser[HighestQualification]"
									placeholder="Highest Qualification" />

							</div>
						</div>
					</div>


					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label">
							Qualification </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-files-o"
									aria-hidden="true"></i></span> <select
									class="questions-category form-control" id="empcourse1"
									name="AllUser[HighestQualId]" required>
									<option value="">Select Higest Qualification</option>

										<?php foreach ($NaukariQualData as $key => $value) { ?>
										<option value="<?=$value->id;?>"><?=$value->name;?></option>
										<?php  } ?>
								       </select> <span id="otc"> </span>

							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Course </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <select
									class="questions-category form-control" id="naukari-course"
									name="AllUser[CourseId]">
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Specialization
						</label>
						<div class="cols-sm-10" id="spc">
							<div class="input-group">
								<span class="input-group-addon" style="padding:10px"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span>
								<!-- <input type="text" class="form-control" name="AllUser[Specialization]" id="naukari-specialization"  placeholder=" Specialization" /> -->
								<select class="questions-category form-control"
									id="naukari-specialization" name="AllUser[Specialization]">
								</select>
							</div>
						</div>
					</div>


					<style>
					    #select2-naukari-specialization-container{font-size: 12px;line-height: 35px;}

					    .input-group .form-control{font-size: 12px;}
					</style>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> Board </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <select
									class="questions-category form-control" id="naukari-board"
									name="AllUser[BoardId]">
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="email" class="cols-sm-2 control-label"> University / College </label>
						<div class="cols-sm-10">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-briefcase"
									aria-hidden="true"></i></span> <input type="text" class="form-control"
										name="AllUser[University]" id="university_college"
										placeholder="University/College" />
								</select>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="password" class="cols-sm-2 control-label">Pass Out
							Year</label>
						<div class="cols-sm-8">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-calendar"
									aria-hidden="true"></i></span> <select class="form-control"
									name="AllUser[DurationTo]" required>
									<option value="">Select Year</option>
										<?php for ($y = 1980; $y <= date('Y'); $y ++) { ?>
                                        	<option value="<?=$y;?>"><?=$y;?></option>
                                        <?php } ?>
								</select>
							</div>
						</div>
					</div>




	<div class="form-group">
		<label for="confirm" class="cols-sm-2 control-label">   Industry
		</label>
			<?php //echo '<pre>'; print_r( $industry );?>
			<div class="cols-sm-10">
						<div class="input-group full">

			<select style="width:100%" class="form-control" name="AllUser[IndustryId]">
			<option value=""> Select Industry	</option>
				<?php foreach ($industry as $key => $value) { ?>
				<option value="<?=$key;?>"><?=$value;?></option>
				<?php  } ?>
			</select>
		</div>
		</div>
	</div>


			<div class="clear"></div>
				<!-- White Catgory, Role and Skills-->

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Job Category </label>
							<div class="cols-sm-10">
								<div class="input-group full">
									<select class="questions-category form-control white_callar_category" required name="AllUser[Category][0][Category]" count="0" id="white_callar_category0">
											<option value = "">Select Category</option>
											<?php if(!empty($whiteCategories)){
											foreach($whiteCategories as $categoryId => $categoryName){?>
												<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
											<?php  }
											}?>
									</select>
								</div>
							</div>
						</div>
						<div class="white-role">
							<div class="form-group">
								<label for="confirm" class="cols-sm-2 control-label">Role </label>
								<div class="cols-sm-10">
									<div class="input-group full">
									<select class="questions-category form-control white_callar_role" name="AllUser[Category][0][WhiteRole][]" id="white_callar_role0" multiple="multiple"></select>
									</div>

								</div>
							</div>
							<div class="clear"></div>
							<div id="white-role-container">

							</div>
						</div>
					<a class="btn btn-info more_add_blck" href="javascript:;" id="add-role"> + Add More Role </a>
					<br class="clearfix" />	<br class="clearfix" />
					<div class="form-group">
						<label for="confirm" class="cols-sm-2 control-label">Skills </label>
						<div class="cols-sm-10">
							<div class="input-group full">
								<select class="questions-category form-control white_callar_skills" id="white_callar_skills" name="AllUser[WhiteSkills][]" multiple="multiple"></select>
							</div>
						</div>
					</div>












	<h5 class="page-head">Team Members Details</h5>


						<div id="teammember">
							<div class="clear"></div>
						 <label for="confirm" class="cols-sm-2 control-label">Team Member Photo </label>


							<div class="col-sm-10" id="member_small">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-4" style="padding-right:0px; ">
											<?php //echo $form->field($teammembers, 'MemberPhoto')->fileInput()?>
											<div class="input-group" style="width:100%">
												<label class="btn btn-default btn-file design-1" style="width:100%"> Browse <input
													type="file" name="AllUser[TeamMemberPhoto][]"
													style="display: none;"
													onchange="$(this).parent().next().html($(this).val());"
													accept="image/*" >
												</label> <span></span>
											</div>
										</div>

										<div class="col-sm-8">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user fa"
													aria-hidden="true"></i></span> <input class="form-control"
													name="AllUser[TeamName][]" required placeholder="Team Member Name"
													type="text">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-4" style="padding-right:0px">
												<div class="input-group"  style="width:100%">
													<select style="width:100%" class="form-control load-qualification" name="AllUser[MemberQualification][]" >
														<option value="">Qualification</option>
															<?php foreach ($NaukariQualData as $key => $value) { ?>
														<option value="<?=$value->id;?>"><?=$value->name;?></option>
														<?php  } ?>
													</select>
												</div>

										</div>

										<div class="col-sm-8">
											<div class="input-group"  style="width:100%">
											 <select style="width:100%" class="form-control load-courses" name="AllUser[MemberCourse][]">
												<option value="">Select Course </option>

											  </select>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
								<div class="row">

									<div class="col-sm-12">
										<div class="input-group"  style="width:100%">
										 <select style="width:100%" class="form-control" name="AllUser[MemberSalary][]" >
                							<option value="">   Salary	</option>
											<option value="0-1.5">0 - 1.5 Lakhs</option>
											<option value="1.5-3">1.5 - 3 Lakhs</option>
											<option value="3-6">3 - 6 Lakhs</option>
											<option value="6-10">6 - 10 Lakhs</option>
											<option value="10-15">10 - 15 Lakhs</option>
											<option value="15-25">15 - 25 Lakhs</option>
											<option value="Negotiable">Negotiable</option>
                							</select>
										</div>
									</div>

								</div>
								</div>
							</div>




						</div>

						<div id="moreteammember">

						</div>
						<div class="clear"></div>
						<div class="form-group">
							<label class="cols-sm-2 control-label" onclick="admoremember();"
								style="color: #f15b22; cursor: pointer;">+ Add  Member</label>

						</div>


					<a id="click" href="javascript:void(0)" >+ Add work experience</a>

					<style>
					.btn.btn-info.more_add_blck{float:right;background:#f15b22; border-radius:5px; color:#fff; font-size:12px; width:120px;border:0px; text-align:center;color:#fff; padding:7px 10px 4px 10px;}

					#click{background:#6d136a; border-radius:5px; color:#fff; font-size:12px; width:180px; text-align:center; display:block; margin:10px auto 10px; color:#fff; padding:10px;}
					    #workexpdiv{display:none;}
					    #main_add_blck{display:none; width:250px; color:#fff;}
					    #main_add_blck label{color:#fff};
					</style>

						<div id="workexpdiv" class="expextra">
							<div class="form-group date-html">
								<label for="YearFrom" class="cols-sm-2 control-label"> Year </label>
								<div class="cols-sm-5">
									<div class="input-group" style="width: 33%; float: left;">
										<span class="input-group-addon"><i class="fa fa-calendar"
											aria-hidden="true"></i></span> <input type="text"
											class="form-control date" name="Experience[0][YearFrom]"
											id="YearFrom" readonly
											style="cursor: pointer; background: #fff;" autocomplete="off"
											placeholder="From" onkeypress="return numbersonly(event)" />
									</div>

									<div class="input-group exp-to-date"
										style="width: 33%; float: left; margin-left: 10%;">
										<span class="input-group-addon"><i class="fa fa-calendar"
											aria-hidden="true"></i></span> <input type="text"
											class="form-control date" name="Experience[0][YearTo]" id="YearTo"
											readonly style="cursor: pointer; background: #fff;"
											autocomplete="off" placeholder="To"
											onkeypress="return numbersonly(event);"
											onchange="if(new Date($(this).val()).getTime()<=new Date($('#YearFrom').val()).getTime()){alert('Wrong Year Duration');$(this).val('');}" />
									</div>
								</div>
							</div>


						<div class="form-group">
							<label for="CompanyName" class="cols-sm-2 control-label"> Company name
							</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-briefcase"
										aria-hidden="true"></i></span> <input type="text"
										class="form-control" name="Experience[0][CompanyName]"
										id="CompanyName" placeholder="Company name" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"> Position </label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-briefcase"
										aria-hidden="true"></i></span> <input type="text"
										class="form-control" name="Experience[0][PositionName]"
										id="position" placeholder="Position" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label"> Industry </label>
							<div class="cols-sm-10">
								<div class="input-group full">
									<select name="Experience[0][CIndustryId]"
										class="form-control bfh-states"  id="CIndustryId">
										<option selected="selected" value="">- Select an Industry -</option>
										<?php foreach($industry as $key=>$value){?>
										<option value="<?php echo $key;?>"><?=$value;?></option>
										<?php } ?>
								</select>
								</div>
							</div>
						</div>

						<div class="exp-white-role">
						<div class="form-group">
							<label for="emppos" class="cols-sm-2 control-label"> Job Category </label>
							<div class="cols-sm-10">
								<div class="input-group full">
									<select class="questions-category form-control white_callar_exp_category" name="Experience[0][Category]" id="white_callar_exp0" data-id-count="0">
									 <option value="">Select Category</option>
									 <?php if(!empty($whiteCategories)){
											foreach($whiteCategories as $categoryId => $categoryName){?>
												<option value="<?php echo $categoryId?>"><?=$categoryName;?></option>
											<?php  }
											}?>

								</select>
								</div>
							</div>
						</div>

							<div class="form-group">
								<label for="emppos" class="cols-sm-2 control-label"> Role </label>
								<div class="cols-sm-10">
									<div class="input-group full">
										<select class="questions-category form-control white_callar_role_exp" name="Experience[0][WhiteRole][]" id="white_callar_role_exp0" multiple="multiple"></select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="emppos" class="cols-sm-2 control-label"> Skills </label>
								<div class="cols-sm-10">
									<div class="input-group full">
										<select class="questions-category form-control white_callar_skills_exp" id="white_callar_skills_exp0" name="Experience[0][WhiteSkills][]" multiple="multiple"></select>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="form-filter-location" class="cols-sm-2 control-label"> Experience </label>
							<div class="cols-sm-10">
								<div class="input-group full">

									<select id="experience-experience" name="Experience[0][Experience]"
										data-minimum-results-for-search="Infinity"
										class="questions-category form-control " tabindex="0"
										aria-hidden="true">
										<option value="Experience">Experience</option>
										<option value="Fresher">Fresher</option>
										<option value="0-1">Below 1 Year</option>
					    <?php
        for ($fr = 1; $fr <= 30; $fr = $fr + 1) {
            $frn = $fr + 1;
            ?>
                                            <option
											value="<?=$fr.'-'.$frn;?>"><?=$fr.'-'.$frn;?> Years</option>
                                           <?php
        }
        ?>
                              </select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label"> Salary</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-briefcase"
										aria-hidden="true"></i></span> <select
										class="questions-category form-control" id="experience-salary" tabindex="0"
										aria-hidden="true" name="Experience[0][Salary]">
										<option value="0-1.5">0 - 1.5 Lakhs</option>
										<option value="1.5-3">1.5 - 3 Lakhs</option>
										<option value="3-6">3 - 6 Lakhs</option>
										<option value="6-10">6 - 10 Lakhs</option>
										<option value="10-15">10 - 15 Lakhs</option>
										<option value="15-25">15 - 25 Lakhs</option>
										<option value="Negotiable">Negotiable</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group current-working">
							<label class="cols-sm-2 control-label">&nbsp;</label>
							<div class="cols-sm-10">
								<span><input type="checkbox" id="currentWorking"  value="1" name="Experience[0][CurrentWorking]">&nbsp;<span style="font-size:12px; color:##565656">I currently work here</span></span>
							</div>
						</div>

					</div>



					<div id="moreskill" class="form-group"></div>
					<div class="form-group btn btn-success" id="main_add_blck" style="margin:0px">
						<label class="cols-sm-12 control-label" onclick="admoreexp();">+
						Add More ( Previous Experience )</label>
					</div>
				</div>

				<!-- 				<div class="col-xs-12 col-sm-6"> -->

				<!-- 					<h5 class="page-head">Team Members Minimum Qualification</h5> -->
				<!-- 					<div class="form-group"> -->
				<!-- 						<label for="name" class="cols-sm-2 control-label">Min <br> -->
				<!-- 							Qualification -->
				<!-- 						</label> -->
				<!-- 						<div class="cols-sm-10"> -->
				<!-- 							<div class="input-group"> -->
				<!-- 								<span class="input-group-addon"><i class="fa fa-file-text" -->
				<!-- 									aria-hidden="true"></i></span> <input type="text" -->
				<!-- 									class="form-control" name="AllUser[MinQualification]" required -->
				<!-- 									placeholder="Highest Qualification" /> -->
				<!-- 							</div> -->
				<!-- 						</div> -->
				<!-- 					</div> -->
				<!-- 					<div class="form-group"> -->
				<!-- 						<label for="email" class="cols-sm-2 control-label">Course </label> -->
				<!-- 						<div class="cols-sm-10"> -->
				<!-- 							<div class="input-group"> -->
				<!-- 								<span class="input-group-addon"><i class="fa fa-files-o" -->
				<!-- 									aria-hidden="true"></i></span> <select -->
				<!-- 									class="questions-category form-control " id="emptcourse" -->
				<!-- 									name="AllUser[TeamCourseId]" -->
				<!-- 									onchange="if($('#emptcourse option:selected').text()=='Others'){$('#totc').append('<input  -->
				<!-- 									type=&#34;text&#34; class=&#34;form-control&#34; -->
				<!-- 									name=&#34;AllUser[TeamOtherCourse]&#34; -->
				<!-- 									placeholder=&#34;Course&#34; required />');} -->
				<!-- 								else{$('#otc').html('');}" required> -->
				<!-- 								<option value="">Select Course</option> -->
				<!-- 		< ?php foreach ($course as $key => $value) { ?> -->
				<!-- 										<option value="< ?=$value->CourseId;?>">< ?=$value->CourseName;?></option> -->
				<!-- 										<   ?php }?> -->
				<!-- 								       </select> <span id="totc"> </span> -->
				<!-- 							</div> -->
				<!-- 						</div> -->
				<!-- 					</div> -->

				<!-- 					<div class="form-group"> -->
				<!-- 						<label for="confirm" class="cols-sm-2 control-label">Skills </label> -->
				<!-- 						<div class="cols-sm-10"> -->
				<!-- 							<div class="input-group"> -->
				<!-- 								<span class="input-group-addon"><i class="fa fa-lock fa-lg" -->
				<!-- 									aria-hidden="true"></i></span> <input type="text" -->
				<!-- 									placeholder=" " name="AllUser[TeamRawSkill]" id="teamskills" -->
				<!-- 									required class="form-control"> <input type="hidden" -->
				<!-- 									id="teamskillid" name="AllUser[TeamSkillId]" /> -->

				<!-- 							</div> -->

				<!--
<div id="allteamskill" style="margin-top: 5px; margin-left: 110px; height: 25px; padding: 3px; font-size: 12px; color: #fff;"></div>-->

				<!-- 						</div> -->

				<!-- 					</div> -->

				<!-- 				</div> -->
			</div>
			<!--Education-->
			<div class="clear"></div>
			<div class="col-xs-12">
				<div class="form-group">
					<label for="confirm" class="cols-sm-2 control-label"><input
						type="checkbox" id="check_box" class="" required> </label>
					<div class="cols-sm-10 info">I agreed to the Terms and Conditions
						governing the use of My Career Bugs. I have reviewed the default
						Mailer & Communications settings. Register Now</div>
				</div>
				<div class="form-group ">
							<?= Html::submitButton('Register', ['class' => 'btn btn-primary btn-lg btn-block login-button']) ?>
						</div>
					<?php ActiveForm::end(); ?>
						</div>
		</div>
	</div>
	<div class="border"></div>
	<script type="text/javascript">
	function getRoleField(count){
		var selectoptionlist = $('.white_callar_category:first').html();
		var html = '<div class="form-group">'+
			'<label for="confirm" class="cols-sm-2 control-label">Job Category </label>'+
			'<div class="cols-sm-10">'+
				'<div class="input-group full">'+
					'<select class="questions-category form-control white_callar_category" required name="AllUser[Category]['+count+'][Category]" count="'+count+'" id="white_callar_category'+count+'">'+selectoptionlist+'</select>'+
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="form-group">'+
			'<label for="confirm" class="cols-sm-2 control-label">Role </label>'+
			'<div class="cols-sm-10">'+
				'<div class="input-group full">'+
				'<select class="questions-category form-control white_callar_role" name="AllUser[Category]['+count+'][WhiteRole][]" id="white_callar_role'+count+'" multiple="multiple"></select>'+
				'</div>'+
			'</div>'+
		'</div>';
		console.log('html' + html);
		return html;
	}
	var count = 0;
	$(document).on('click' , '#add-role' , function(e){
		count++;
		$('#white-role-container').append(getRoleField(count));
		$(".white_callar_role").select2({
			placeholder: "Select Role"
		});
	});


 
    $(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true, yearRange: "-50:+0"});
    
    

	$(document).ready(function(){
		$('#currentWorking').click(function(){
			if($(this).is(':checked')){
				$('.exp-to-date').hide(0);
			}else{
				$('.exp-to-date').show(0);
			}

		});
	});

	var p=0;
	function admoremember() {
		p++;
		/*var result='<div class="clear"></div><div class="col-sm-2"><label for="confirm" class=" control-label">Team Member Photo</label></div><div class="col-sm-10"><div class="row"><div class="col-sm-4"><div class="input-group"><label class="btn btn-default btn-file design-1">Browse <input type="file" name="AllUser[TeamMemberPhoto][]" style="display: none;" onchange="$(this).parent().next().html($(this).val());" accept="image/*"></label><span></span></div></div><div class="col-sm-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span><input class="form-control" name="AllUser[TeamName][]" required placeholder="Team Name" type="text"></div></div></div></div>';*/

		var result = '<div class="clear"></div><div class="col-sm-2" style="padding-right:0px"><label for="confirm" class=" control-label">Team Member Photo </label></div><div class="col-sm-10"><div class="form-group"><div class="row"><div class="col-sm-4" style="padding-right:0px; "><div class="input-group" style="width:100%"><label class="btn btn-default btn-file design-1" style="width:100%"> Browse <input type="file" name="AllUser[TeamMemberPhoto][]" style="display: none;" onchange="$(this).parent().next().html($(this).val());" accept="image/*"></label><span></span></div></div><div class="col-sm-8"><div class="input-group"><span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span><input class="form-control" name="AllUser[TeamName][]" required="" placeholder="Team Name" type="text"></div></div></div></div><div class="form-group"><div class="row"><div class="col-sm-4" style="padding-right:0px"><div class="input-group" style="width:100%"><select style="width:100%" class="form-control load-qualification" name="AllUser[MemberQualification][]"><option value="">Qualification</option></select></div></div><div class="col-sm-8"><div class="input-group" style="width:100%"><select style="width:100%" class="form-control load-courses" name="AllUser[MemberCourse][]"><option value="">Select Course </option></select></div></div></div></div><div class="form-group"><div class="row"><div class="col-sm-12"><div class="input-group" style="width:100%"><select style="width:100%" class="form-control" name="AllUser[MemberSalary][]"><option value="">   Salary	</option><option value="0-1.5">0 - 1.5 Lakh</option><option value="1.5-3">1.5 - 3 Lakh</option><option value="3-6">3 - 6 Lakh</option><option value="6-10">6 - 10 Lakh</option><option value="10-15">10 - 15 Lakh</option><option value="15-25">15 - 25 Lakh</option><option value="Negotiable">Negotiable</option></select></div></div></div></div></div>';

        $('#moreteammember').append('<div id="teamember'+p+'" ><span style="color:red;cursor:pointer;" onclick=delMember('+p+');>X</span>'+result+'</div>');

		loadQulification('teamember'+p);
    }
	function del(cnt) {
        $('#teamember'+cnt).remove();
    }

	function loadQulification(elm){
		$.ajax({
                type: "POST",
                url: '<?=Yii::$app->urlManager->baseUrl."/getqualification"?>',
                data: false,
                 success : function(response) {
            		var data = JSON.parse(response);
					$("#"+elm+" .load-qualification option:not(:first)").remove();
            		$.each(data, function( index, value ) {
  						$("#"+elm).find('.load-qualification').append('<option value="'+index+'">'+value+'</option>');
					});
        		}

            });

	}




    $(document).on('change', '#naukari-specialization', function() {
		if($(this).val()=="Other"){
			$("#naukari-other-specialization").parents('.form-group').removeClass('hide');
		}else{
			$("#naukari-other-specialization").parents('.form-group').addClass('hide');
		}
	});
	var data = [{id: '',text: 'Select Specialization'}];

	<?php foreach ($naukari_specialization as $key => $value) { ?>
			var d = {id: '<?=$value->name?>',text: '<?=$value->name?>'};
			data.push(d);
	<?php }  ?>
	$("#naukari-specialization").select2({
  		data: data
	});
	$(document).on('change', '#empcourse1', function() {
        if($(this).val()==""){
        	$("#naukari-course option").remove();
            //$("#naukari-specialization option").remove();
            $("#naukari-board option").remove();
        	return true;
        }

        /*if($(this).val()=="6"){
        	$("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
            return true;
        }*/
        if($(this).val()=="4" || $(this).val()=="5" || $(this).val()=="6"){
        	$("#naukari-course").parents('.form-group').addClass('hide');
            $("#naukari-specialization").parents('.form-group').addClass('hide');
            $("#university_college").parents('.form-group').addClass('hide');
            $("#naukari-board").parents('.form-group').removeClass('hide');
            var elm = 'naukari-board';
        }else{
        	$("#naukari-course").parents('.form-group').removeClass('hide');
            $("#naukari-specialization").parents('.form-group').removeClass('hide');
            $("#university_college").parents('.form-group').removeClass('hide');
            $("#naukari-board").parents('.form-group').addClass('hide');
            var elm = 'naukari-course';
        }
        var id = $(this).val();
  			$.ajax({
                type: "POST",
                url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
                data: {'id': $(this).val()},
                 success : function(response) {
            		var data = JSON.parse(response);
            		$("#naukari-course option").remove();
            		//$("#naukari-specialization option").remove();
            		$("#naukari-board option").remove();
            		$.each(data, function( index, value ) {
  						$("#"+elm).append('<option value="'+index+'">'+value+'</option>');
					});
        		},

            })
		});
		$(document).on('change' , '.exp-role' , function(e){
			//var mainurl = 'http://localhost/www.mycareerbugs.com/frontend/web/index.php/';
            var mainurl = 'https://mycareerbugs.com';
            console.log('log');
        	var html = '';
    		var roleid = $(this).val();
			var key1 = $(this).attr('key');
    		var loderimg = '<label for="confirm" class="cols-sm-2 control-label">Skills</label><div class="cols-sm-10"><div class="input-group"><img src="'+mainurl+'/img/ajax-loader.gif"></div></div>';
    		var e = $(this);
    		$(this).parents('.form-group').find('.skilldatalist').html(loderimg);
    		 $.getJSON( mainurl+"/site/skilldata", {roleid:roleid}, function( data ) {
        		 if(data.status == 'OK') {
        			  html += '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group">';
        			  $.each( data, function( key, val ) {
        				 html += '<span><input type="checkbox" value="'+val.id+'" name="Experience['+key1+'][skills]['+roleid+']['+key+']" />'+val.value+'&nbsp;</span>';
        			  });

        			  html +='</div></div>';
        		 }else{
        			 html += 'No Record Found';
            	 }
    			 e.parents('.form-group').find('.skilldatalist').html(html);
    		});
    	});
    	$(document).on('change','[id^=roleidlist]',function(e){
			//var mainurl = 'http://localhost/www.mycareerbugs.com/frontend/web/index.php/';
			console.log('This Working')
    		var mainurl = 'https://mycareerbugs.com';
    		console.log('workinh');
    		var roleid = $(this).val();
			var count = $(this).attr('data-count');
    		var loderimg = '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group"><img src="'+mainurl+'/img/ajax-loader.gif"></div></div>';
    		$("#skilldatalist-"+count).html(loderimg);
    		$.getJSON( mainurl+"/site/skilldata", {roleid:roleid}, function( data ) {
    			 var html = '<label for="confirm" class="cols-sm-2 control-label">Skills  </label><div class="cols-sm-10"><div class="input-group">';
    			 $.each( data, function( key, val ) {
    				html += '<span><input type="checkbox" value="'+val.id+'" name="AllUser[EducationSkillId][]" />'+val.value+'&nbsp;</span>';
    			 });
    			 html +='</div></div>';
    			 $("#skilldatalist-"+count).html(html);
    		});
        });

		function addMoreRoleSkill( count ){
			var categoryOption = $('#white_callar_category0').html();
			var html = '<div class="form-group"><label class="cols-sm-2 control-label"> Job Category </label><div class="cols-sm-10"><div class="input-group full"><select class="questions-category form-control white_callar_exp_category" name="Experience['+count+'][Category]" id="white_callar_exp'+count+'" data-id-count="'+count+'">'+categoryOption+'</select></div></div></div>';
			html += '<div class="form-group"><label class="cols-sm-2 control-label"> Role </label><div class="cols-sm-10"><div class="input-group full"><select class="questions-category form-control white_callar_role_exp" name="Experience['+count+'][WhiteRole][]" id="white_callar_role_exp'+count+'" multiple=""></select></div></div></div>';
			html +='<div class="form-group"><label class="cols-sm-2 control-label"> Skills </label><div class="cols-sm-10"><div class="input-group full"><select class="questions-category form-control white_callar_skills_exp" id="white_callar_skills_exp'+count+'" name="Experience['+count+'][WhiteSkills][]" multiple=""></select></div></div></div>';

			return html;

		}


    	var p=0;
		function admoreexp() {
				p++;

				var result = $('#workexpdiv').html();
				//var result = $('#workexpdiv').find('#roleskilllist').empty().html();

				var date='<div class="form-group"><label for="email" class="cols-sm-2 control-label">  Year    </label><div class="cols-sm-5"><div class="input-group" id="responsive_full" style="width: 33%;float: left;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="Experience['+p+'][YearFrom]" readonly style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="From" maxlength="4" onkeypress="return numbersonly(event)"/></div><div class="input-group" id="responsive_full1" style="width: 33%;float: left;margin-left: 10%;"><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span><input type="text" class="form-control date" name="Experience['+p+'][YearTo]" readonly style="cursor: pointer;background: #fff;" autocomplete="off"  placeholder="To"  maxlength="4" onkeypress="return numbersonly(event);" onchange="if(new Date($(this).val()).getTime()<=new Date($(&#39;YearFrom&#39;).val()).getTime()){alert(&#39;Wrong Year Duration&#39;);$(this).val(&#39; &#39;);}"/></div></div></div>';
            	$('#moreskill').append('<div id="workexpindiv'+p+'" class="expextra" ><span style="color:red;cursor:pointer;" onclick=del('+p+');>X</span>'+result+'</div>');
				$('#workexpindiv'+p).find('.date-html').remove();
				$('#workexpindiv'+p).find('span:first').after(date);
				$('.current-working').remove();

				$('#workexpindiv'+p).find('[id^=CompanyName]:last').attr('name', 'Experience['+p+'][CompanyName]');
				$('#workexpindiv'+p).find('[id^=CompanyName]:last').attr('id', 'CompanyName'+p);

				$('#workexpindiv'+p).find('[id^=position]:last').attr('name', 'Experience['+p+'][PositionName]');
				$('#workexpindiv'+p).find('[id^=position]:last').attr('id', 'position'+p);

				$('#workexpindiv'+p).find('[id^=CIndustryId]:last').attr('name', 'Experience['+p+'][CIndustryId]');
				$('#workexpindiv'+p).find('[id^=CIndustryId]:last').attr('id', 'CIndustryId'+p);

				$('#workexpindiv'+p).find('[id^=emppos]:last').attr('name', 'Experience['+p+'][PositionId]');
				$('#workexpindiv'+p).find('[id^=emppos]:last').attr('key', p);
				$('#workexpindiv'+p).find('[id^=emppos]:last').attr('id', 'emppos'+p);

				$('#workexpindiv'+p).find('[id^=experience-experience]:last').attr('name', 'Experience['+p+'][Experience]');
                $('#workexpindiv'+p).find('[id^=experience-experience]:last').attr('id', 'experience-experience'+p);


				$('#workexpindiv'+p).find('[id^=experience-salary]:last').attr('name', 'Experience['+p+'][Salary]');
				$('#workexpindiv'+p).find('[id^=experience-salary]:last').attr('id', 'experience-salary'+p);

				$('#workexpindiv'+p).find('.current-working').remove();

				$(".date").datepicker({maxDate: new Date(),dateFormat:'yy-mm-dd',changeMonth: true,changeYear: true});

				$('#workexpindiv'+p).find('.exp-white-role').html(addMoreRoleSkill(p));


				$(document).on('change', '#white_callar_exp_category'+p, function(){
					var value = $(this).val();
					var countC = $(this).attr('data-id-count');
					if(value != ""){
						$.ajax({
							dataType : "json",
							type : 'GET',
							url : '<?=Yii::$app->urlManager->baseUrl."/getwhiteroles"?>',
							data : {
								category_id : value
							},
							success : function(data) {
								$('#white_callar_role_exp'+countC).html("");
								if(data != ""){
									$.each(data, function(key, val) {
										var option = $('<option />');
										option.attr('value', key).text(val);
										$('#white_callar_role_exp'+countC).append(option);
									});
								}
							}
						});
					}

				});

				$("#white_callar_role_exp"+p).select2({
					placeholder: "Select Role"
				});


			$("#white_callar_skills_exp"+p).select2({
				maximumSelectionLength: 5,
				placeholder: "Select Skills",
				ajax: {
					url: '<?=Yii::$app->urlManager->baseUrl."/getwhiteskills"?>',
					dataType: 'json',
					type: "GET",
					data: function (params) {
						return {
							q: params.term,
							page: 1
						  }
					},
					processResults: function(data){
						return {
							results: $.map(data.results, function (item) {

								return {
									text: item.name,
									id: item.id
								}
							})
						};
					}
				 }
			});

		 $('#workexpindiv'+p).find('#roleskilllist').empty();




        }
		function del(id) {
            $('#workexpindiv'+id).remove();
        }

		function delMember(id) {
            $('#teamember'+id).remove();
        }




$(document).ready(function(){
  $("#click").click(function(){
    $("#workexpdiv, #main_add_blck").toggle();
  });

  $(document).on('change', '.load-qualification',function(){
	 var id = $(this).val();
	 var obj = $(this);
	 if(id != ""){
		$.ajax({
			type: "POST",
			url: '<?=Yii::$app->urlManager->baseUrl."/getcourse"?>',
			data: {'id': id},
			 success : function(response) {
				var data = JSON.parse(response);
				$(obj).parents('.row:first').find(".load-courses option:not(:first)").remove();
				$.each(data, function( index, value ) {
					$(obj).parents('.row:first').find('.load-courses').append('<option value="'+index+'">'+value+'</option>');
				});

			},

		});
	 }
  });

	$(".white_callar_role").select2({
  		placeholder: "Select Role"
	});

	$(document).on('change', '.white_callar_category', function(){
		var value = $(this).val();
		var count = $(this).attr('count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Yii::$app->urlManager->baseUrl."/getwhiteroles"?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role'+count).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role'+count).append(option);
						});
					}
				}
			});
		}

	});

	$("#white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Yii::$app->urlManager->baseUrl."/getwhiteskills"?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {

						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});

	$(".white_callar_role_exp").select2({
  		placeholder: "Select Role"
	});

	$(document).on('change', '.white_callar_exp_category', function(){
		var value = $(this).val();
		var countC = $(this).attr('data-id-count');
		if(value != ""){
			$.ajax({
				dataType : "json",
				type : 'GET',
				url : '<?=Yii::$app->urlManager->baseUrl."/getwhiteroles"?>',
				data : {
					category_id : value
				},
				success : function(data) {
					$('#white_callar_role_exp'+countC).html("");
					if(data != ""){
						$.each(data, function(key, val) {
							var option = $('<option />');
							option.attr('value', key).text(val);
							$('#white_callar_role_exp'+countC).append(option);
						});
					}
				}
			});
		}

	});

	$(".white_callar_skills").select2({
  		maximumSelectionLength: 5,
		placeholder: "Select Skills",
		ajax: {
			url: '<?=Yii::$app->urlManager->baseUrl."/getwhiteskills"?>',
			dataType: 'json',
			type: "GET",
			data: function (params) {
				return {
					q: params.term,
					page: 1
				  }
			},
			processResults: function(data){
				return {
					results: $.map(data.results, function (item) {

						return {
							text: item.name,
							id: item.id
						}
					})
				};
			}
		 }
	});

	$("#white_callar_skills_exp0").select2({
			maximumSelectionLength: 5,
			placeholder: "Select Skills",
			ajax: {
				url: '<?=Yii::$app->urlManager->baseUrl."/getwhiteskills"?>',
				dataType: 'json',
				type: "GET",
				data: function (params) {
					return {
						q: params.term,
						page: 1
					  }
				},
				processResults: function(data){
					return {
						results: $.map(data.results, function (item) {

							return {
								text: item.name,
								id: item.id
							}
						})
					};
				}
			 }
		});

});



</script>