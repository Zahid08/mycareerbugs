<?php
$this->title = 'Team Profile';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?>

  <img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">
  
	<div id="wrapper"><!-- start main wrapper -->
		<div class="inner_page second">
			<div class="container">
			  <div  id="profile-desc">
			   <div class="col-md-2 col-sm-2 col-xs-12">
			                 <div class="user-profile">
                                    <img src="<?=$teamim;?>" alt="" class="img-responsive center-block ">
                                    <h3><?=$team->Name;?></h3>
                                </div> 
			      	</div>
		         <div class="col-md-10 col-sm-10 col-xs-12"> 
                            <div class="job-short-detail">
                                <div class="heading-inner">
                                    <p class="title">Profile detail</p>
									
                                </div>
                                <dl>
                                     
                                    <dt>Phone:</dt>
                                    <dd>+<?=$team->MobileNo;?> </dd>

                                    <dt>Email:</dt>
                                    <dd><?=$team->Email;?></dd>
 
                                    <dt>Address:</dt>
                                    <dd><?=$team->Address;?></dd>

                                    <dt>City:</dt>
                                    <dd><?=$team->City;?></dd>

                                    <dt>State:</dt>
                                    <dd><?=$team->State;?></dd>

                                    <dt>Country:</dt>
                                    <dd><?=$team->Country;?> </dd>
									<dt>Pincode:</dt>
                                    <dd><?=$team->PinCode;?></dd>
                                </dl>
                            </div>
 
                        </div>
						 
	 <div class="clearfix"> </div>
	
				<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">Educational Information  <span style="display: none;"><?=$team->educations[0]->DurationFrom;?> to <?=$team->educations[0]->DurationTo;?></span></p>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Pass out Year</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->DurationTo;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Higest Qualification</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->course->CourseName;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box" style="display: none;">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Course</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->course->CourseName;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>University / College</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->University;?></h4>
                                             </div>
                                    </div>
                                </div>
								
								 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4> 	Skills</h4>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												<?php
												$skill='';
												foreach($team->empRelatedSkills as $ask=>$asv)
												{
														$skill.=$asv->skill->Skill.', ';
												}
												$skill=trim(trim($skill),",");
												?>
                                            <h4><?=$skill;?></h4>
                                             </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						 
						 <div class="clearfix"> </div>

						 		
				<?php
				if($team->experiences)
				{
						foreach($team->experiences as $k=>$val)
						{
				?>
				<div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">  Work Experience  <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span></p>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Company name</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->CompanyName;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Role</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->position->Position;?></h4>
                                             </div>
                                    </div>
                                </div>
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Industry</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                           <?php
											if($val->industry)
											{
											?>
                                            <h4><?=$val->industry->IndustryName;?></h4>
											<?php
											}
											?>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Experience</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												<?php
												$expy=$val->Experience.' Years';
												?>
                                            <h4> <?=$expy;?></h4>
                                             </div>
                                    </div>
                                </div>
								
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Salary</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												
                                            <h4> <?=$val->Salary.' Lakh';?></h4>
                                             </div>
                                    </div>
                                </div>
								
								  
                            </div>
                        </div>
  
			  <?php
						}
				}
				?>

				<div class="col-md-12 col-sm-12 col-xs-12">	

					
						   <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title"> Team Members </p>
                                </div>
						<div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">	
                                <div class="row education-box">
                                    
                                        <div class="col-xs-12 col-md-6 col-sm-6">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Min Qualification</h4>
                                           
                                        </div> 
                                    </div>
									
									   <div class="col-xs-12 col-md-6 col-sm-6">
                                        <div class="degree-info">
                                            <?php
                                            if($team->minqualification)
                                            {
                                            ?>
                                            <h4><?=$team->minqualification->MinQualification;?></h4>
                                            <?php
                                            }
                                            ?>
                                             </div>
                                    </div>
                                </div>
                              
                             
								
								 <div class="row education-box">
                                    <div class="col-md-6 col-xs-12 col-sm-6">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Skills</h4>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <div class="degree-info">
                                            <?php
                                            $teamskill='';
                                            if($team->teamRelatedSkills)
											 {
											 foreach($team->teamRelatedSkills as $sk=>$skid)
											 {
                                                $teamskill.=$skid->skill->Skill.' , ';
                                             }
                                             }
                                             $teamskill=trim($teamskill," , ");
											 ?>
                                            <h4><?=$teamskill;?></h4>
                                             </div>
                                    </div>
                                </div>
								  
                            </div>
						    </div>	
							
						
						
						<div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
										 
										   <?php
										 if($team->teams)
										 {
											 foreach($team->teams as $tk=>$tval)
											 {
												if($tval->MemberPhoto!=0)
													  {
														$teammemp=$url.$tval->memberphoto->Doc;
													  }
													  else
													  {
														$teammemp='images/user.png';
													  }
										 ?>
										   <div class="team_list_block">   
											  <img src="<?=$teammemp;?>" alt="" class="img-circle img-responsive">
											  <h3><?=$tval->MemberName;?></h3>
										  </div>
										 <?php
											 }
										 }
										 ?>

									    </div>
										
										</div>

						<div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title">    Description </p>
                                </div>
                                <div class="row education-box">
                                    
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <div class="degree-info">
                                            <p><?=$team->TeamDesc;?></p>
                                             </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
						
  </div>
            </div>
       </div>
		
		<div class="border"></div>