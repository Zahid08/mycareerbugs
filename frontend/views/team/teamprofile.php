<?php

$this->title = 'Team Profile';


use yii\helpers\Url;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\bootstrap\ActiveForm;

use common\models\Skill;
use common\models\EmployeeSkill;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;
use common\models\NaukariQualData;
use common\models\Documents;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';

$url='/backend/web/';

?>



  <img class="full_width" src="<?=$imageurl;?>images/background-main.jpg">



	<div id="wrapper"><!-- start main wrapper -->

		<div class="inner_page second">

			<div class="container">

			  <div  id="profile-desc">

			   <div class="col-md-2 col-sm-2 col-xs-12">

			                 <div class="user-profile">

                                    <img src="<?= $image;?>" alt="" class="img-responsive center-block ">

                                    <h3><?=$team->Name;?></h3>

                                </div>

			      	</div>

		         <div class="col-md-10 col-sm-10 col-xs-12">

                            <div class="job-short-detail">

                                <div class="heading-inner">

                                    <p class="title">Profile detail</p>



									<a href="<?= Url::toRoute(['team/editprofile'])?>">  <i class="fa fa-pencil-square-o orange"></i> Edit Profile</a>

                                </div>

                                <dl>



                                    <dt>Phone:</dt>

                                    <dd>+<?=$team->MobileNo;?> </dd>

								    <dt>Whatsapp:</dt>

                                    <dd>+<?=$team->whatsappno;?> </dd>


                                    <dt>Email:</dt>

                                    <dd><?=$team->Email;?></dd>



                                    <dt>Address:</dt>

                                    <dd><?=$team->Address;?></dd>



                                    <dt>City:</dt>

                                    <dd><?=$team->City;?></dd>

                                    <dt>Highest Qualification:</dt>
                                    <?php //echo "<pre>";print_r($team->educations->specialization_id);die('[['); ?>
                                    <dd><?=$team->educations[0]->nqual->name;?> </dd>
                                    <dt>Course:</dt>

                                    <dd><?=$team->educations[0]->ncourse->name;?> </dd>
                                    <dt>Specialization:</dt>
									<?php //echo '<pre>'; print_r($team->educations);?>
                                    <dd><?=$team->educations[0]->specialization_id;?> </dd>

                                    <dt>State:</dt>

                                    <dd><?=$team->State;?></dd>

									<dt>Pincode:</dt>

                                    <dd><?=$team->PinCode;?></dd>

                                </dl>

                            </div>



                        </div>



	 <div class="clearfix"> </div>



				<div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="resume-box">

                                <div class="heading-inner">

                                    <p class="title">Educational Information  <span style="display: none;"><?=$team->educations[0]->DurationFrom;?> to <?=$team->educations[0]->DurationTo;?></span></p>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Pass out Year</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->DurationTo;?></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Highest Qualification</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->nqual->name;?><?php //$team->educations[0]->course->CourseName;?></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>

                                        <div class="insti-name">
                                            <h4>Course</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->ncourse->name;?><?php //$team->educations[0]->course->CourseName;?></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>

                                        <div class="insti-name">
                                            <h4>Specialization</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->specialization_id;?></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>

                                        <div class="insti-name">
                                            <h4>University / College</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$team->educations[0]->University;?></h4>
                                        </div>
                                    </div>
                                </div>

								<div class="row education-box">
									<div class="col-md-4 col-xs-12 col-sm-4">
										<div class="resume-icon">
											<i class="fa fa-briefcase" aria-hidden="true"></i>
										</div>

										<div class="insti-name">
											<h4>Job Category</h4>
										</div>

									</div>

									<div class="col-xs-12 col-md-8 col-sm-8">
										<div class="degree-info">
											<?php $jobCategory = ArrayHelper::map(UserJobCategory::find()->where([
																'user_id' => $team->UserId
															])->all(), 'category', 'category');

												  $CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
															'user_id' => $team->UserId
														])->all(), 'category_id', 'category_id');
												  $expCategory = ArrayHelper::map(WhiteCategory::find()->where([
															'id' => $CategoryIds
														])->all(), 'name', 'name');
												$jobCategory = array_merge($jobCategory, $expCategory);
												?>

											<h4><?php echo (!empty($jobCategory)?implode(', ',$jobCategory):"");?></h4>
										</div>
									</div>
								</div>
								<div class="row education-box">
									<div class="col-md-4 col-xs-12 col-sm-4">
										<div class="resume-icon">
											<i class="fa fa-briefcase" aria-hidden="true"></i>
										</div>

										<div class="insti-name">
											<h4>Role</h4>
										</div>

									</div>

									<div class="col-xs-12 col-md-8 col-sm-8">
										<div class="degree-info">
											<?php $jobRoles1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $team->UserId
																	])->all(), 'role', 'role');
														  $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $team->UserId,
																	])->all(), 'role', 'role');
														  $jobRoles = array_merge($jobRoles1, $expRoles);
																	?>
											<h4 style="line-height:18px;"><?php echo (!empty($jobRoles)?implode(', ',$jobRoles):"");?></h4>
										</div>
									</div>
								</div>


								 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>

                                        <div class="insti-name">
                                            <h4>Skills</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
											<?php $jobSkills1 = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $team->UserId
																	])->all(), 'skill', 'skill');
														  $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $team->UserId,
																	])->all(), 'skill', 'skill');
														$jobSkills = array_merge($jobSkills1, $expSkills);
																	?>
												<h4><?php echo (!empty($jobSkills)?implode(', ',$jobSkills):"");?></h4>
										</div>
                                    </div>
                                </div>
                            </div>

                        </div>



						 <div class="clearfix"> </div>





				<?php

				if($team->experiences)

				{

						foreach($team->experiences as $k=>$val)

						{

				?>

				<div class="col-md-12 col-sm-12 col-xs-12">

                            <div class="resume-box">

                                <div class="heading-inner">

                                    <p class="title">  Work Experience  <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span></p>

                                </div>

                                <div class="row education-box">

                                    <div class="col-md-4 col-xs-12 col-sm-4">

                                        <div class="resume-icon">

                                           <i class="fa fa-file-text" aria-hidden="true"></i>

                                        </div>

                                        <div class="insti-name">

                                            <h4>Company name</h4>



                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">

                                        <div class="degree-info">

                                            <h4><?=$val->CompanyName;?></h4>

                                             </div>

                                    </div>

                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Position</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->PositionName;?></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Job Category</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->whitecategory->name;?></h4>
                                        </div>
                                    </div>
                                </div>

								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Role</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                           <?php $expRole = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $team->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'role_id', 'role'); ?>
										<h4><?php echo (!empty($expRole)?implode(', ',$expRole):"");?></h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Skill</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
											<?php $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $team->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'skill_id', 'skill');?>
                                            <h4><?php echo $expSkills ? implode(", ",$expSkills) : "N/A";?></h4>

                                        </div>
                                    </div>
                                </div>

								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>

                                        <div class="insti-name">
                                            <h4>Industry</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                           <?php

											if($val->industry)

											{

											?>

                                            <h4><?=$val->industry->IndustryName;?></h4>

											<?php

											}

											?>

                                             </div>

                                    </div>

                                </div>

                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Experience</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
												<?php
												$expy=$val->Experience.' Years';
												?>
                                            <h4> <?=$expy;?></h4>
                                        </div>
                                    </div>
                                </div>



								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Salary</h4>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4> <?=$val->Salary.' Lakh';?></h4>

                                             </div>

                                    </div>

                                </div>





                            </div>

                        </div>



			  <?php

						}

				}

				?>



				<div class="col-md-12 col-sm-12 col-xs-12">
						   <div class="resume-box">
                                <div class="heading-inner">
                                    <p class="title"> Team Members </p>
                                </div>
						<?php /*
						<div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
                             <div class="row education-box">
                                <div class="col-xs-12 col-md-6 col-sm-6">
                                   <div class="degree-info">
                                   <?php
                                        $teamskill='';
                                        if($team->teamRelatedSkills)

											 {

											 foreach($team->teamRelatedSkills as $sk=>$skid)

											 {

                                                $teamskill.=$skid->skill->Skill.' , ';

                                             }

                                             }

                                             $teamskill=trim($teamskill," , ");

											 ?>

                                            <h4><?php $teamskill;?></h4>

                                        </div>
                                    </div>
                                </div>
                            </div>
						 </div>	*/?>
						<div class="row education-box">
							<?php if($team->teams){
											 foreach($team->teams as $tk=>$tval){ ?>
											<div class="col-lg-3  col-md-3 col-sm-6 col-xs-12">
												<?php	$teammemp = Documents::getImageByAttr($tval, 'MemberPhoto', 'memberphoto');
														$qualification = NaukariQualData::find()
																	   ->select('name')
																	   ->where(['id' => $tval->MemberQualification])
																	   ->one();
														$course = NaukariQualData::find()
																	   ->select('name')
																	   ->where(['id' => $tval->MemberCourse])
																	   ->one();
														 ?>

														   <img src="<?=$teammemp;?>" alt="image" class="team_mmb_ph" style="float:left; height:100px; padding-right:10px;">
														   <div class="team_mmb_det" style="float:left;">
																<h5> <?=$tval->MemberName;?>  </h5>
																<p> <?=(isset($qualification->name)?$qualification->name:"");?>  </p>
																<p> <?=(isset($course->name)?$course->name:"");?>  </p>
																<p> <?=$tval->MemberSalary.' Lakh';?>  </p>
															</div>
											</div>
										 <?php } ?>

										<?php }?>



						</div>

										</div>



						<div class="col-md-12 col-sm-12 col-xs-12">



                            <div class="resume-box">

                                <div class="heading-inner">

                                    <p class="title">    Description </p>

                                </div>

                                <div class="row education-box">



                                    <div class="col-xs-12 col-md-12 col-sm-12">

                                        <div class="degree-info">

                                            <p><?=nl2br($team->TeamDesc);?></p>

                                             </div>

                                    </div>

                                </div>



                            </div>

                        </div>



  </div>

            </div>

       </div>



		<div class="border"></div>