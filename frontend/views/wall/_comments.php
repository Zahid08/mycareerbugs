<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Documents;
use common\models\CommentToPost;
$ppvalue = $model;
//echo '<pre>';
//print_r($ppvalue->emp);
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';

if ($ppvalue->emp->UserTypeId == 2) {
    $page = 'searchcandidate';
} elseif ($ppvalue->emp->UserTypeId == 3) {
    $page = 'searchcompany';
} elseif ($ppvalue->emp->UserTypeId == 4) {
    $page = 'searchcampus';
} elseif ($ppvalue->emp->UserTypeId == 5) {
    $page = 'searchteam';
}
//echo $ppvalue->emp->UserTypeId;
if ($ppvalue->emp->UserTypeId == 2 || $ppvalue->emp->UserTypeId == 5) {
    $ll = Documents::getImageByAttr($ppvalue->emp, 'LogoId', 'photo');
	
	if ($ppvalue->emp->PhotoId != 0) {
		$ll = $url . $ppvalue->emp->photo->Doc;
	} else {
		$ll = $imageurl . 'images/user.png';
	}    

} else {
	
    //$ll = Documents::getImageByAttr($ppvalue->emp, 'PhotoId', 'photo');
	if ($ppvalue->emp->LogoId != 0) {
		$ll = $url . $ppvalue->emp->logo->Doc;
	} else {
		$ll = $imageurl . 'images/user.png';
	}

}
if ($empid == $ppvalue->emp->UserId) {
    ?>

<div class="box-footer boxcommentedit<?=$ppvalue->CommentToPostId;?> items"
	style="display: none;">
                      <?php $form = ActiveForm::begin(); ?>
                        <img class="img-responsive img-circle img-sm" src="<?=$empdp;?>" alt="Alt Text">
	<div class="img-push">
		<div class="form-group">
			<span class="input-icon inverted"> <input type="hidden"
				name="CommentToPost[CommentToPostId]"
				value="<?=$ppvalue->CommentToPostId;?>" /> <input
				class="form-control input-lg" type="text"
				value="<?=$ppvalue->Message;?>" name="CommentToPost[Message]"
				required>

				<button type="submit" class="fa fa-envelope bg-palegreen"
					style="border: none; width: 46px;"></button>
			</span>

		</div>
	</div>
                      <?php ActiveForm::end(); ?>
                    </div>


    <div class="box-footer boxcommenteditReply<?=$ppvalue->CommentToPostId;?> items"
         style="display: none;">
        <?php $form = ActiveForm::begin(); ?>
        <img class="img-responsive img-circle img-sm" src="<?=$empdp;?>" alt="Alt Text">
        <div class="img-push">
            <div class="form-group">
			<span class="input-icon inverted"> <input type="hidden"
                                                      name="CommentToPost[CommentToPostId]"
                                                      value="<?=$ppvalue->CommentToPostId;?>" /> <input
                        class="form-control input-lg" type="text"
                        value="<?=$ppvalue->Message;?>" name="CommentToPost[Message]"
                        required>

				<button type="submit" class="fa fa-envelope bg-palegreen"
                        style="border: none; width: 46px;"></button>
			</span>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?php
}
?>

<div
	class="box-footer comment-msg-box boxcommentdel<?php echo $ppvalue->CommentToPostId;?>"
	style="display: block;">
					 
                      <div class="box-comment ">
                        <img class="img-circle img-sm" src="<?=$ll;?>" alt="User Image">
		<div class="comment-text">
			<span class="username"> <a
				href="<?= Url::toRoute([$page,'userid'=>$ppvalue->emp->UserId])?>"> <?=$ppvalue->emp->Name;?></a>
						 <?php
    if ($empid == $ppvalue->emp->UserId) {
        ?>
						 <span class="pull-right" style="cursor: pointer;"
				onclick="$('.boxcommentedit<?=$ppvalue->CommentToPostId;?>').show();"><i
					class="fa fa-edit"></i></span> <span class="pull-right"
				style="cursor: pointer;"
				onclick="delcomment(<?=$ppvalue->CommentToPostId;?>);"><i
					class="fa fa-trash"></i></span>
						  <?php
    }else{
    ?>
		<span class="pull-right click-post" comment_id= "<?=$ppvalue->CommentToPostId;?>" title="Reply" style="cursor: pointer;" ><i class="fa fa-reply"></i></span>
	<?php }?>
                          <span class="text-muted pull-right"><?=date('h:i A D M',strtotime($ppvalue->OnDate));?></span>
			</span>
                          <?=$ppvalue->Message;?>
						   <div class="reply-main" id="maincomment-<?=$ppvalue->CommentToPostId;?>">
						  <!-- Nested Comment-->
						  <?php //echo '<pre>';print_r($ppvalue->nestedComment); ?>
						  <?php if(!empty($ppvalue->nestedComment)){
									foreach($ppvalue->nestedComment as $nested){
										if ($nested->emp->UserTypeId == 2 || $nested->emp->UserTypeId == 5) {
											if ($nested->emp->PhotoId != 0) {
												$rr = $url . $nested->emp->photo->Doc;
											} else {
												$rr = $imageurl . 'images/user.png';
											}   
											
										} else {
											if ($nested->emp->LogoId != 0) {
												$rr = $url . $nested->emp->logo->Doc;
											} else {
												$rr = $imageurl . 'images/user.png';
											}
											//$rr = Documents::getImageByAttr($nested->emp, 'LogoId', 'photo');
										}
										?>
								  <div class="reply-msg boxcommentdel<?=$nested->CommentToPostId;?>">
									<div class="box-comment">
										<img class="img-circle img-sm" src="<?=$rr;?>" alt="User Image">
										<div class="comment-text">
											<?php if ($empid == $nested->emp->UserId) { ?>

                                                <span class="pull-right" style="cursor: pointer;"
                                                      onclick="$('.boxcommenteditReply<?=$ppvalue->CommentToPostId;?>').show();"><i
                                                            class="fa fa-edit"></i></span>
												 <span class="pull-right" style="cursor: pointer;" onclick="delcomment(<?=$nested->CommentToPostId;?>);"><i class="fa fa-trash"></i></span>
												  <?php
											}?>
											<span class="text-muted pull-right"><?=date('h:i A D M',strtotime($nested->OnDate));?></span>


											
											
											<?=$nested->Message;?>
										</div>
									</div>	
								  </div>
								  <div style="clear:both"></div>
						  <?php }
						  }?>
						  </div>
						   <?php $form = ActiveForm::begin(); ?>
						  <!-- Reply Comment-->

						  <div class="img-push reply-comment-<?=$ppvalue->CommentToPostId;?>" style="display:none; margin-top:10px;">
							<div class="form-group">
								<input type="hidden" name="CommentToPost[PostId]" id="commentpostid<?=$ppvalue->PostId;?>" value="<?=$ppvalue->PostId;?>">
								<input type="hidden" name="CommentToPost[previousId]" id="commentpostid<?=$ppvalue->EmpId;?>" value="<?=$ppvalue->EmpId;?>">

								<input type="hidden" name="CommentToPost[EmpId]" id="commentempid<?=$empid;?>" value="<?=$empid;?>">
								<input type="hidden" name="CommentToPost[CommentToPostId]" id="commentid<?=$ppvalue->CommentToPostId;?>" value="<?=$ppvalue->CommentToPostId;?>">
								<span class="input-icon inverted"> <input
									class="form-control input-lg" type="text"
									value="" name="CommentToPost[Message]"
									required>
								<p class="help-block help-block-error comment-error" style="color: #d73d32;"></p>
									<button type="submit" class="fa fa-envelope bg-palegreen submit-nested-comment"
										style="border: none; width: 46px;" comment_id = "<?=$ppvalue->CommentToPostId;?>"></button>
								</span>

							</div>
						</div>
						<?php ActiveForm::end(); ?>
                        </div>
	</div> 
					     
                    </div>
					
				