<?php use yii\widgets\ActiveForm;
use common\models\Industry;
use common\models\Position;
use yii\helpers\ArrayHelper;
use common\models\Documents;
use frontend\components\searchcity\SearchCity;
?>
<div class="box profile-info" id="editbox<?=$postvalue->WallPostId;?>"
	style="display: none;">
	<?php

$form = ActiveForm::begin([
    'options' => [
        'class' => 'editpostform' . $postvalue->WallPostId,
        'enctype' => 'multipart/form-data'
    ]
]);
?>
	<input type="text" id="post_title" class="form-control" name="CompanyWallPost[PostTitle]" required placeholder="Enter Post Title" value="<?=$postvalue->PostTitle;?>" /><br/>
	<textarea class="form-control input-lg p-text-area"
		name="CompanyWallPost[Post]" required rows="2"
		placeholder="Whats in your mind today?"><?=$postvalue->Post;?></textarea>
	<input type="hidden" name="CompanyWallPost[WallPostId]" value="<?=$postvalue->WallPostId;?>" />
	<div class="box-footer">
		<ul id="delimg<?=$postvalue->WallPostId;?>">
        	<?php if ($postvalue->ImageId != 0) {  
        	    $image = Documents::getImageByAttr($postvalue, 'ImageId', 'wallimage')?>
        	<li><img src="<?=$image;?>"
				style="width: 150px; height: 150px;" /></li>
			<li style="cursor: pointer;"
				onclick="delimg(<?=$postvalue->WallPostId;?>,'wall');">X</li>
        	<?php } ?>
    	</ul>
	</div>
	<div class="box-footer">
	
		<ul class="nav nav-pills">
			<li>
			<?php echo $form->field($postvalue, 'CIndustryId')->dropDownList(Industry::getIndustryLists())->label(false)?>
			</li>
			<li>
			<?php echo $form->field($postvalue, 'PositionId')->dropDownList(ArrayHelper::map(Position::getRoles(), 'PositionId', 'Position'))->label(false)?>
			</li>
			<li>
			<?php

			/*echo $form->field($postvalue, 'Locationat')
				->widget(kartik\select2\Select2::classname(), [
				'data' => $cj,
				'language' => 'en',
				'options' => [
					'placeholder' => 'Select from list ...',
					'id' => 'walllocation'
				]
			])
        ->label(false); */
    ?>
			<?php echo SearchCity::widget(['model' => $postvalue, 'attr' => 'Locationat', 'form' => $form])?>
<!-- 			<input type="text" id="pac-input" placeholder="Location At" -->
<!-- 				class="form-control" name="CompanyWallPost[Locationat]" value="< ?=$postvalue->Locationat;?>" style="display: none;" />  -->
				
				<!-- <a href="javascript:;" onclick="$(this).prev().slideToggle();"><i class="fa fa-map-marker"></i></a> -->
			
			</li>
			<li><a class="cameraa"><i class="fa fa-camera"></i><input type="file"
					name="CompanyWallPost[ImageId]"
					onchange="$(this).next().html($(this).val());" accept="image/*"><span></span></a></li>
			<li style="display:none"><a href="javascripe:;" class="cameraa"><i class=" fa fa-film"></i><input
					type="file" name="CompanyWallPost[VideoId]"
					onchange="$(this).next().html($(this).val());" accept="video/*"><span></span></a></li>
					<li>	<button type="submit" class="editpost btn btn-info pull-right"
			pid="<?=$postvalue->WallPostId;?>">Update Post</button></li>
		</ul>
	</div>
					 <?php ActiveForm::end(); ?>
					</div>