<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;

//Pjax::begin(['id' => 'postcomments-pjax']);
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => '_comments',
    'viewParams' => [
        'empdp' => $empdp,
        'empid' => $empid
    ],
    'summary' => '',
    'emptyText' => 'No Comments Found',
//     'pager' => [
//         'class' => \kop\y2sp\ScrollPager::className(),
//         'paginationSelector' => '.list-view .pagination',
//         'triggerTemplate' => 'working'
//     ]
]);
//Pjax::end();
?>