<?php

use common\models\Industry;
use common\models\Position;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
 <?php
$postvalue = $model;
$url = '/backend/web/';

if(!empty($postvalue->Slug)){
	$urlLink = Url::base().'/wall-post/'.$postvalue->Slug;
}else{
	$urlLink = Url::toRoute(['/wall/post-view','id' =>  $postvalue->WallPostId]);	
}

if ($postvalue->employer->LogoId != '') {
    $logo = $url . $postvalue->employer->logo->Doc;
} else {
    $logo = $url . 'images/user.png';
}
//$wapjobdesc =  'Job posted by ' . $allpost->CompanyName .  "<br>, Job location: " . $allpost->Location .  ", for more details visit: " . $jobUrl;
//$whatsapp_desc = $postvalue->PostTitle.' Job opportunity at ' . $postvalue->employer->Name . " for Location " . $postvalue->Locationat . " for more detils visit " . $urlLink;
$whatsapp_desc = 'Job posted by ' . $postvalue->employer->Name .  "<br>, Job location: " . $postvalue->Locationat .  ", for more details visit: " . $urlLink;
?>

<?php echo Yii::$app->controller->renderPartial('_editpost',compact('postvalue'))?>
<!--   posts -->
<div class="box box-widget">
	<div class="box-header with-border">
		<div class="user-block">
			<img class="img-circle" src="<?=$logo;?>" alt="User Image"> <span
				class="username"><a
				href="<?= Url::toRoute(['searchcompany','userid'=>$postvalue->EmployerId])?>"><?=$postvalue->employer->Name;?></a></span>
						<?php $indusrt = Industry::findOne($postvalue->CIndustryId);  $postinid = Position::findOne($postvalue->PositionId);    ?>
						<!-- INDUS -->
			<span class="description extra_top_mrg">  <?= $indusrt->IndustryName;?> - <?= $postinid->Position;?>   -   <?=$postvalue->Locationat;?>     </span>
			<span class="description top_mr_abs"
				style="font-size: 10px; position: absolute; right: 9px; top: 41px;"> 
                         <?=date('h:i a d M, Y',strtotime($postvalue->OnDate));?>    </span>
                   
						<?php
    if ($postvalue->EmployerId == $empid) {
        ?>
        <style> .item .btn.btn-default.btn-green{display:none;} </style>
						 <span class="postright" onclick="$(this).next().toggle();"><i
				class="fa fa-angle-down"></i> </span>
			<div class="postbox">
				<ul>
					<li onclick="$('#editbox<?=$postvalue->WallPostId;?>').show();">Edit</li>
					<li onclick="deletepost(<?=$postvalue->WallPostId;?>);">Delete</li>
				</ul>
			</div>
						<?php
    }
    ?>
                      </div>
                      
        <?php 
        $watchList = $postvalue->getWatchpost($postvalue->WallPostId, $empid);
        if(empty($watchList)){ ?>                  
            <a href="javascript:void(0)" id="watchtext_<?=$postvalue->WallPostId;?>"  onclick="watchList(<?=$postvalue->WallPostId;?>)" class="btn btn-default btn-green" style="border:0px; position: absolute;top: -1px;right: 0px;border-radius:0px; background-color: #f16b22;color: white;font-size: 13px;"><i class="fa fa-heart-o"></i>  Watchlist</a>
        <?php  }else{ ?>
          <a href="javascript:void(0)" class="btn btn-default btn-green" style="opacity: 0.2; position: absolute;top: 0px;right: 0px; background-color: #6c146b; border:0px; border-radius:0px; color: white;font-size: 13px;"><i class="fa fa-heart" aria-hidden="true"></i> Added</a>
        <?php } ?>
        
	</div>

	<div class="box-body" style="display: block;">
					  <?php
    if ($postvalue->wallimage) {
        ?>
						<img class="img-responsive pad show-in-modal"
			src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
						<?php
    }
    if ($postvalue->wallvideo) {
        ?>
						 <video width="320" height="240" controls>
			<source src="<?=$url.$postvalue->wallvideo->Doc;?>" type="video/mp4">
		</video> 
						<?php
    }
    ?>
						<div class="comment more">
						<p><a href="<?=$urlLink;?>"><?=$postvalue->PostTitle;?></a></p>
						<?=str_replace("\n", '<br />',  $postvalue->Post);?>
                      </div>
					  <?php
    if ($postvalue->likes) {
        $lk = $postvalue->getlike($postvalue->WallPostId, $empid);
        if ($lk) {
            if ($lk->IsLike == 1) {
                ?>
					  <button type="button" class="btn btn-default btn-xs"
			onclick="liketopost(<?=$postvalue->WallPostId;?>);" id="likedislikebutton_<?=$postvalue->WallPostId;?>"> 
			<i class="fa fa-thumbs-o-down"></i> UnLike
		</button>
					  
					  <?php
            } else {
                ?>
						<button type="button" class="btn btn-default btn-xs" id="likedislikebutton_<?=$postvalue->WallPostId;?>"
			onclick="liketopost(<?=$postvalue->WallPostId;?>);">
			<i class="fa fa-thumbs-o-up"></i> Like
		</button>
						
						<?php
            }
        } else {
            ?>
						<button type="button" class="btn btn-default btn-xs"
			onclick="liketopost(<?=$postvalue->WallPostId;?>);" id="likedislikebutton_<?=$postvalue->WallPostId;?>">
			<i class="fa fa-thumbs-o-up"></i> Likes
		</button>
						<?php
        }
        ?>
					  <?php
    } else {
        ?>
					  <button type="button" class="btn btn-default btn-xs"
			onclick="liketopost(<?=$postvalue->WallPostId;?>);" id="likedislikebutton_<?=$postvalue->WallPostId;?>">
			<i class="fa fa-thumbs-o-up"></i> Like
		</button>
					  <?php
    }
    ?>
    <span class="text-muted"
			id="likedisliketext_<?=$postvalue->WallPostId;?>"><?=count($postvalue->likes);?> likes  </span>
		<a href="<?=$urlLink;?>">View </a>
		<div class="share" style="float: right; padding-top: 0px;">
			Share On <a
				href="https://www.facebook.com/sharer/sharer.php?u=<?=$urlLink?>"
				target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i>
			</a>
			<!-- <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://developer.linkedin.com&title=LinkedIn%20Developer%20Network&summary=My%20favorite%20developer%20program&source=LinkedIn" target="_blank">
						 <i class="fa fa-linkedin"></i>
					  </a> -->
			<a href="javascript:;"
				onclick="mailtoemp('myModal_email','<?=$postvalue->WallPostId;?>');"
				style="line-height: 32px;"><i class="fa fa-envelope-o"></i> </a> <a
				href="https://api.whatsapp.com/send?text=<?=strip_tags($whatsapp_desc);?>"
				target="_blank"><i class="fa fa-whatsapp"></i></a>

		</div>
	</div>





	<div id="PostComments<?=$postvalue->WallPostId;?>" class="postcomments">
		<?php
echo Yii::$app->controller->renderPartial('_postComments', [
    'dataProvider' => $postvalue->getPostCommentData(),
    'empdp' => $empdp,
    'empid' => $empid
])?>
					</div>

	<div class="box-footer" style="display: block;">
		<img class="img-responsive img-circle img-sm" src="<?=$empdp;?>"
			alt="Alt Text">
		<div class="img-push">
			<div class="form-group">
				<span class="input-icon inverted"> <input type="hidden"
					name="CommentToPost[PostId]"
					id="commentpostid<?=$postvalue->WallPostId;?>"
					value="<?=$postvalue->WallPostId;?>" /> <input type="hidden"
					name="CommentToPost[EmpId]"
					id="commentempid<?=$postvalue->WallPostId;?>" value="<?=$empid;?>" />
					<input class="form-control input-lg" type="text"
					id="commentmessage<?=$postvalue->WallPostId;?>"
					name="CommentToPost[Message]" required>
					<p
						class="help-block help-block-error error<?=$postvalue->WallPostId;?>"
						style="color: #d73d32;"></p>
					<button type="button"
						onclick="Postcomment(<?=$postvalue->WallPostId;?>);"
						class="fa fa-envelope bg-palegreen"
						style="border: none; width: 46px;"></button>
				</span>

			</div>
		</div>

	</div>

</div>