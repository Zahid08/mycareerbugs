<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Documents;
?>
<?php 
//$img = Documents::getImageByAttr($model, 'PhotoId', 'photo');

 $term = Yii::$app->request->get()['q'];
 
if($oldJsonViewdData){
   $viewProfileArray = explode(',', $oldJsonViewdData);
   if (in_array($model->UserId, $viewProfileArray)){
    $previousSeenProfile=1;
   }
}

$visibleTo = in_array($model->UserTypeId, [
    2,
    5,
    6
]);
if($model->UserTypeId == 2){
	if ($model->PhotoId != 0) {
		$url = '/backend/web/';
		$img = $url . $model->photo->Doc;
	} else {
		$img = '/images/user.png';
	}
}else{
	if ($model->LogoId != 0) {
		$url = '/backend/web/';
		$img = $url . $model->logo->Doc;
	} else {
		$img = '/images/user.png';
	}
}
$className='';
if($previousSeenProfile){$className="viewdProfile";}

if ($model->CollarType == 'white'){
    $jobRoles1 = \yii\helpers\ArrayHelper::map(\common\models\UserWhiteRole::find()->where([
        'user_id' => $model->UserId
    ])->all(), 'role', 'role');
    $expRoles = \yii\helpers\ArrayHelper::map(\common\models\ExperienceWhiteRole::find()->where([
        'user_id' => $model->UserId,
    ])->all(), 'role', 'role');
    $jobRoles = array_merge($jobRoles1, $expRoles);

   $jobRoles= (!empty($jobRoles)?implode(', ',$jobRoles):"");

}else{
    $jobRoles=$model->getEducationRoles();
}
?>
 
<div class="search_main <?php echo $className; ?>" >
	<div class="box-header with-border">
		<div class="user-block">
			<img class="img-circle" src="<?php echo $img?>"
				alt="User Image"> 
				<span class="username"><?php 
				if($model->UserTypeId == 4){
					$name = $model->CollegeName;
				}else{
					$name = $model->Name;
				}
				
				echo Html::a($name,Url::base().'/'.str_replace(' ','',$model->Name).'-'.$model->UserId);?> 
					<?php if($visibleTo){?>
					<small>- <?php echo  $model->IsExp == 1 ? 'Experience' : 'Fresher';?></small>
					<?php } else if($model->UserTypeId == 1){?>
					<small>- <?php echo 'Admin';?></small>
					<?php } else if($model->UserTypeId == 3){?>
					<small>- <?php echo $model->EntryType;?></small>
					<?php } else if($model->UserTypeId == 4){?>
					<small>- <?php echo 'Campus';?></small>
					<?php } else if($model->UserTypeId == 7){?>
					<small>- <?php echo 'Company';?></small>
					<?php } ?> 
				</span> 
					<?php if($visibleTo){?>
					<span class="description_heading">
						Role: <?=$jobRoles;?>
					</span>
					<?php }?> 
					<span class="description_loc">
				<?php echo $model->City?>- <?php echo $model->State?></span> 
				
				<?php
				 if($model->EntryType=='Company' || $model->EntryType=='Consultancy' || $model->EntryType=='HR'){
				?>
				<?php 	echo Html::a('View Profile',Url::base().'/'.str_replace(' ','',$model->Name).'-'.$model->UserId,['class' => 'view_prf '.$className.'','target'=>"_blank"]); ?>
				
				<?php }else{ ?>
			   <?php echo Html::a('View Profile',Url::toRoute(['wall/itsearch','term' => $model->UserId,'q'=>$term]),['class' => 'view_prf '.$className.'','target'=>"_blank"])?>
			   
			   <?php } ?>
				
		</div>
	</div>
</div>

<style>.cover-info .form-group{display:none}</style>
