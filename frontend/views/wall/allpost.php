<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;

if (isset(Yii::$app->session['Employerid'])) {
    $empid = Yii::$app->session['Employerid'];
    $empdp = Yii::$app->session['EmployerDP'];
    $type = 3;
    $ppage = 'companyprofile';
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $empid = Yii::$app->session['Employeeid'];
    $empdp = Yii::$app->session['EmployeeDP'];
    $type = 2;
    $ppage = 'candidateprofile';
} elseif (Yii::$app->session['Campusid']) {
    $empid = Yii::$app->session['Campusid'];
    $empdp = Yii::$app->session['CampusDP'];
    $type = 4;
    $ppage = 'campusprofile';
} elseif (Yii::$app->session['Teamid']) {
    $empid = Yii::$app->session['Teamid'];
    $empdp = Yii::$app->session['TeamDP'];
    $type = 5;
    $ppage = 'teamprofile';
}

Pjax::begin([
    'id' => 'allpost-pjax',
    'timeout' => 500000
]);
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => [
        'class' => 'item'
    ],
    'itemView' => '_posts',
    // 'summary' => '',
    'emptyText' => 'No Posts Found',
    'viewParams' => [
        'empdp' => $empdp,
        'empid' => $empid
    ]
    // 'pager' => [
    // 'class' => \kop\y2sp\ScrollPager::className(),
    // 'paginationSelector' => '.list-view .pagination',
    // 'triggerTemplate' => 'working'
    // ]
]);
Pjax::end();

?>