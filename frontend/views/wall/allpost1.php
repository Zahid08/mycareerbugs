<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Likes;
$lik=new Likes();
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
if(isset(Yii::$app->session['Employerid']))
        {
            $empid=Yii::$app->session['Employerid'];
			$empdp=Yii::$app->session['EmployerDP'];
			$type=2;
        }
        elseif(isset(Yii::$app->session['Employeeid']))
        {
            $empid=Yii::$app->session['Employeeid'];
			$empdp=Yii::$app->session['EmployeeDP'];
			$type=3;
        }
        elseif(Yii::$app->session['Campusid'])
        {
            $empid=Yii::$app->session['Campusid'];
			$empdp=Yii::$app->session['CampusDP'];
			$type=4;
        }
        elseif(Yii::$app->session['Teamid'])
        {
            $empid=Yii::$app->session['Teamid'];
			$empdp=Yii::$app->session['TeamDP'];
			$type=5;
        }

				  if($allpost)
				  {
						foreach($allpost as $postkey=>$postvalue)
						{
							  if($postvalue->employer->LogoId!='')
							  {
									$logo=$url.$postvalue->employer->logo->Doc;
							  }
							  else
							  {
									$logo=$imageurl.'images/user.png';
							  }
				  ?>
				  <div class="box profile-info" id="editbox<?=$postvalue->WallPostId;?>" style="display: none;">
					<?php $form = ActiveForm::begin(['options' => ['class'=>'editpostform'.$postvalue->WallPostId,'enctype'=>'multipart/form-data','action'=>'https://mycareerbugs.com/wall/careerbugwall']]); ?>
						<textarea class="form-control input-lg p-text-area" name="CompanyWallPost[Post]"  required rows="2" placeholder="Whats in your mind today?"><?=$postvalue->Post;?></textarea>
						<input type="hidden" name="CompanyWallPost[WallPostId]" value="<?=$postvalue->WallPostId;?>"/>
				   <div class="box-footer">
					<ul id="delimg<?=$postvalue->WallPostId;?>">
						<?php
						if($postvalue->ImageId!=0)
						{
						?>
						<li><img src="<?=$url.$postvalue->wallimage->Doc;?>" style="width: 150px;height: 150px;" /></li>
						<li style="cursor: pointer;" onclick="delimg(<?=$postvalue->WallPostId;?>,'wall');">X</li>
						<?php
						}
						?>
					</ul>
				   </div>
					<div class="box-footer">
						<button type="submit" class="editpost btn btn-info pull-right" pid="<?=$postvalue->WallPostId;?>">Update Post</button>
						<ul class="nav nav-pills">
							<li>
									  <input type="text" id="pac-input" placeholder="Location At" class="form-control" name="CompanyWallPost[Locationat]" value="<?=$postvalue->Locationat;?>" style="display: none;"/>
								<a href="#" onclick="$(this).prev().slideToggle();"><i class="fa fa-map-marker"></i></a></li>
							<li><a class="cameraa"><i class="fa fa-camera"></i><input type="file" name="CompanyWallPost[ImageId]" onchange="$(this).next().html($(this).val());" accept=
		"image/*"><span></span></a></li>
							<li><a href="#" class="cameraa"><i class=" fa fa-film"></i><input type="file" name="CompanyWallPost[VideoId]" onchange="$(this).next().html($(this).val());" accept=
		"video/*"><span></span></a></li> 
						</ul>
					</div>
					 <?php ActiveForm::end(); ?>
					</div>
				  
                  <!--   posts -->
                  <div class="box box-widget">
                     <div class="box-header with-border">
                      <div class="user-block">
                        <img class="img-circle" src="<?=$logo;?>" alt="User Image">
                        <span class="username"><a href="<?= Url::toRoute(['searchcompany','userid'=>$postvalue->EmployerId])?>"><?=$postvalue->employer->Name;?></a></span>
                        <span class="description">Shared publicly - <?=date('h:i A D M',strtotime($postvalue->OnDate));?>   - <?=$postvalue->Locationat;?></span>
						<?php
						if($postvalue->EmployerId==$empid)
						{
						?>
						 <span class="postright" onclick="$(this).next().toggle();"><i class="fa fa-angle-down"></i> </span>
							  <div class="postbox">
									<ul>
										  <li onclick="$('#editbox<?=$postvalue->WallPostId;?>').show();">Edit</li>
										  <li onclick="deletepost(<?=$postvalue->WallPostId;?>);">Delete</li>
									</ul>
							  </div>
						<?php
						}
						?>
                      </div>
                    </div>

                    <div class="box-body" style="display: block;">
					  <?php
						if($postvalue->wallimage)
						{
						?>
						<img class="img-responsive pad show-in-modal" src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
						<?php
						}
						if($postvalue->wallvideo)
						{
						?>
						 <video width="320" height="240" controls>
							  <source src="<?=$url.$postvalue->wallvideo->Doc;?>" type="video/mp4">
							</video> 
						<?php
						}
						?>
                      <p><?=str_replace("\n", '<br />',  $postvalue->Post);?></p> 
                      
					  <?php
					  if($postvalue->likes)
					  {
						$lk=$postvalue->getlike($postvalue->WallPostId,$empid);
						if($lk)
						{
						if($lk->IsLike==1)
						{
					  ?>
					  <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-down"></i> UnLike</button>
					  <?php
						}
						else
						{
						?>
						<button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
						<?php
						}
						}else{
						?>
						<button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
						<?php
						}
						?>
                      <span class="pull-right text-muted"><?=count($postvalue->likes);?> likes  </span>
					  <?php
					  }
					  else
					  {
					  ?>
					  <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
					  <?php
					  }
					  ?>
                    </div>
					<div style="width: 100%;max-max-height: 200px;overflow: auto;">
                    <?php
					if($postvalue->comments)
					{
						foreach($postvalue->comments as $ppkey=>$ppvalue)
						{
							if($ppvalue->emp->UserTypeId==2){$page='searchcandidate';}elseif($ppvalue->emp->UserTypeId==3){$page='searchcompany';}elseif($ppvalue->emp->UserTypeId==4){$page='searchcampus';}elseif($ppvalue->emp->UserTypeId==5){$page='searchteam';}
							  if($ppvalue->emp->UserTypeId==2 || $ppvalue->emp->UserTypeId==5)
							  {
									if($ppvalue->emp->PhotoId!=0)
									{
										  $ll=$url.$ppvalue->emp->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($ppvalue->emp->LogoId!=0)
							  {
									$ll=$url.$ppvalue->emp->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
					?>
					<?php
						if($empid==$ppvalue->emp->UserId)
						{
						?>
					<div class="box-footer boxcommentedit<?=$ppvalue->CommentToPostId;?>" style="display: none;">
                      <?php $form = ActiveForm::begin(); ?>
                        <img class="img-responsive img-circle img-sm" src="<?=$empdp;?>" alt="Alt Text">
                        <div class="img-push">
                        <div class="form-group">
                                      <span class="input-icon inverted">
										  <input type="hidden" name="CommentToPost[CommentToPostId]" value="<?=$ppvalue->CommentToPostId;?>"/>
                                          <input class="form-control input-lg" type="text" value="<?=$ppvalue->Message;?>" name="CommentToPost[Message]" required>
                                          
										  <button type="submit" class="fa fa-envelope bg-palegreen" style="border: none;width: 46px;"></button>
                                      </span>
									  
                                  </div>
                        </div>
                      <?php ActiveForm::end(); ?>
                    </div>
				  <?php
						}
						?>
                    <div class="box-footer box-comments" style="display: block;">
                      <div class="box-comment">
                        <img class="img-circle img-sm" src="<?=$ll;?>" alt="User Image">
                        <div class="comment-text">
                          <span class="username">
                         <a href="<?= Url::toRoute([$page,'userid'=>$ppvalue->emp->UserId])?>"> <?=$ppvalue->emp->Name;?></a>
						 <?php
						if($empid==$ppvalue->emp->UserId)
						{
						?>
						 <span class="pull-right" style="cursor: pointer;" onclick="$('.boxcommentedit<?=$ppvalue->CommentToPostId;?>').show();"><i class="fa fa-edit"></i></span>
						  <span class="pull-right" style="cursor: pointer;" onclick="delcomment(<?=$ppvalue->CommentToPostId;?>);"><i class="fa fa-trash"></i></span>
						  <?php
						}
						?>
                          <span class="text-muted pull-right"><?=date('h:i A D M',strtotime($ppvalue->OnDate));?></span>
                          </span>
                          <?=$ppvalue->Message;?>
                        </div>
                      </div> 
                    </div>
					<?php
						}
					}
					?>
                   <div class="box-footer" style="display: block;">
                     
                        <img class="img-responsive img-circle img-sm" src="<?=$empdp;?>" alt="Alt Text">
                        <div class="img-push">
                        <div class="form-group">
                                      <span class="input-icon inverted">
										  <input type="hidden" name="CommentToPost[PostId]" id="commentpostid<?=$postvalue->WallPostId;?>" value="<?=$postvalue->WallPostId;?>"/>
										  <input type="hidden" name="CommentToPost[EmpId]" id="commentempid<?=$postvalue->WallPostId;?>"  value="<?=$empid;?>"/>
                                          <input class="form-control input-lg" type="text" id="commentmessage<?=$postvalue->WallPostId;?>" name="CommentToPost[Message]" required>
                                          <p class="help-block help-block-error error<?=$postvalue->WallPostId;?>" style="color:#d73d32;"></p>
										  <button type="button" onclick="Postcomment1(<?=$postvalue->WallPostId;?>);" class="fa fa-envelope bg-palegreen" style="border: none;width: 46px;"></button>
                                      </span>
									  
                                  </div>
                        </div>
                      
                    </div>
                  </div><!--  end posts-->
                 <?php
				  }
			}
			?>
			</div>