<?php
$this->title =Yii::$app->session['CampusName'];

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use common\models\Likes;
$lik=new Likes();
use common\models\LeaveComment;
?>
<!-- Begin page content -->
    <div class="page-content">
   
          <div class="cover profile">
            <div class=" ">
              <div class="image">
                <img src="<?=$imageurl;?>careerimg/background1.jpg" class="show-in-modal" alt="people">
              </div>  
            </div>
			
			
			 <div class="container">
				  <div class="row">
					<div class="col-md-12"> 
					
						<div class="cover-info">
						  <div class="profile_main">
							<img src="<?=Yii::$app->session['CampusDP'];?>" alt="Profile" class="">
						  </div>
						  <div class="name"><a href="#"> <?php echo Yii::$app->session['CampusName']; ?> (<?=$campus->City;?>)</a></div>
						  <ul class="cover-nav"> 
						        <li class="active"><a href="#"><i class="fa fa-user"></i> About College	</a></li>
                                <li  class=""><a href="#"><i class="fa fa-envelope"></i> Message</a></li> 								
								<li  class=""><a href="#"><i class="fa fa-eye"></i> View Comments  </a></li>  							
						  </ul> 
						  
						  <div class="form-group right_main"> 
							<input class="form-control1" name="name" id="project" placeholder="Search for People, Company and Consultant" type="text"> <input type='hidden' id="termid" />
							<input class="src1" id="searchit" value="Search" type="button"> 
                            </div>
							
						</div>
					  </div>
					</div> 
           </div> 
	     </div>
	  
	  
	   <div class="container">
		   <div class="widget-header">
              <h3 class="widget-caption">Collage  Description</h3>
            </div>
		   <div class="company_details12"> 
		                 <div class="profile_cont12">
						 <?=$campus->CompanyDesc;?>
						  </div>
		     </div>   
	   </div>
	  
	  
	
	     <div class="container">
		   <div class="widget-header">
              <h3 class="widget-caption">Dean / Director / Principal</h3>
            </div>
		   <div class="company_details12"> 
			        <div class="col-img">
		                 <div class="profile_main">
							  <?php
									if($campus->PhotoId!=0)
									{
										$campusphoto=$url.$campus->photo->Doc;
									?>
							<img src="<?=$url.$campus->photo->Doc;?>" alt="people" class="">
							<?php
							}
							?>
						  </div>
	                </div> 
					<div class="col-cont">
		                 <div class="profile_cont12">
						     <?=$campus->AboutYou;?>
						  </div>
	                </div>
		     </div>   
	   </div>
	  
	  
	  
	  
	  
	  
    <!-- Begin page content -->
 <div class="page-content like_pae">
    <div class="container">
      <div class="row">
	 <div class="col-md-3 col-xs-12" id="collage_left"> 
		<div class="widget">
		   <div class="widget-header">
              <h3 class="widget-caption">Statistics</h3>
            </div> 
		     <div class="section bordered-sky">  
					<p><a href="<?= Url::toRoute(['follower','userid'=>$campus->UserId]);?>"><span class="badge"><?=count($lik->getTotalfollow($campus->UserId));?></span>   <i class="  fa fa-thumbs-up"></i> Followers </a></p>
					<!--<p><span class="badge">15</span>   <i class=" fa fa-comments"></i> Posts</p>-->
					<!--<p><span class="badge"><?=$totalactive;?></span>   <i class=" fa fa-comments"></i> Active Jobs</p> -->
              </div> 
          </div>
		  
		  
		  
			<div class="widget" style="display: none;">
                <div class="widget-header">
                  <h3 class="widget-caption">Description</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  Mycareer Bugs, India's  Job Site provides you the best opportunity to find the right job today. We provide information about Freshers Jobs, Latest Walkins, Latest Off Campus Drives For Freshers, Bank Jobs.
                </div>
            </div>

		  <div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">Basic Skill</h3>
                </div>
                <div class="basic_skill">
               <?php
				  if($skills)
				  {
				  foreach($skills as $sk=>$sval)
				  {
				  ?>
				  <p><?=$sval;?></p>
				  <?php
				  }
				  }
				  ?>
                </div>
              </div>
		   
			  
		  <div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">  Social Network</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  
						
						<div class="row">
                          <div class="directory-info-row col-xs-12">	
	          	              <ul class="social-links">
                                  <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
                              </ul>      
						</div>
                     </div>
                </div>
              </div>

			  
		  
          <div class="widget widget-friends">
           
            <div class="widget-header">
              <h3 class="widget-caption">Followers</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
					<?php
                    $followers=$lik->getTotalfollow($campus->UserId);
                    if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeFrom->UserTypeId==2 || $fvalue->likeFrom->UserTypeId==5)
							  {
									if($fvalue->likeFrom->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeFrom->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeFrom->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeFrom->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
							   if($fvalue->likeFrom->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeFrom->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>"><img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a> </li>
					<?php
                        }
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
 
 
						  
          <div class="widget widget-friends">
           
            <div class="widget-header">
              <h3 class="widget-caption">Collage Photos</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
				  <?php
				if($campus->collegepics)
				{
				  foreach($campus->collegepics as $key=>$value)
									{
					?>
					<li> <a href=""> <img src="<?=$url.$value->pic->Doc;?>" style="height: 60px;" alt="image">    </a> </li>
				  <?php
									}
				}
				?>
                  </ul>
                </div>
              </div>
            </div>
          </div>		 
							
 
       <div class="widget widget-friends"> 
            <div class="widget-header">
              <h3 class="widget-caption">Videos</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
					         
                  </ul>
                </div>
              </div>
            </div>
          </div>
	   
	   
	   <div class="widget widget-friends">
            <div class="widget-header">
              <h3 class="widget-caption">Follow</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
					<?php
                    $followers=$lik->getMyfollow($campus->UserId);
                    if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeTo->UserTypeId==2 || $fvalue->likeTo->UserTypeId==5)
							  {
									if($fvalue->likeTo->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeTo->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeTo->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeTo->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
                    if($fvalue->likeTo->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeTo->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeTo->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeTo->UserId])?>"> <img src="<?=$ll;?>" alt="image"><?=$fvalue->likeTo->Name;?></a> </li>
					<?php
                        }
                    }
                    ?>               
                  </ul>
                </div>
              </div>
            </div>
          </div>
 
			  
        </div>
		
		
		
		
		
		
		
		 <div class="col-md-6 col-xs-12  padding-left"  id="collage_middle">  
          <div class="row">
               <!-- left posts-->
            <div class="col-md-12">
		   <div class="widget-header">
              <h3 class="widget-caption"> Quick   Update</h3>
            </div>
			
			<div id="campusprofilepost" class="col-md-12">
			<?php
			if($allpost)
			{
				  foreach($allpost as $postkey=>$postvalue)
				  {
						if($postvalue->employer->LogoId!='')
						{
							  $logo=$url.$postvalue->employer->logo->Doc;
						}
						else
						{
							  $logo=$imageurl.'images/user.png';
						}
			?>
			<div class="box box-widget">
                    <div class="box-header with-border">
                      <div class="user-block">
                        <img class="img-circle" src="<?=$logo;?>" alt="User Image">
                        <span class="username"><a href="#"><?=$postvalue->employer->Name;?></a></span>
                        <span class="description">Shared publicly - <?=date('h:i A D M',strtotime($postvalue->OnDate));?>   - <?=$postvalue->Locationat;?></span>
						 <span class="description"> </span>
                      </div>
                    </div>

                    <div class="box-body" style="display: block;">
						<?php
						if($postvalue->wallimage)
						{
						?>
						<img class="img-responsive pad show-in-modal" src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
						<?php
						}
						?>
                      <p><?=str_replace("\n", '<br />',  $postvalue->Post);?></p> 
                      
					  <?php
					  if($postvalue->likes)
					  {
						$lk=$postvalue->getlike($postvalue->WallPostId,Yii::$app->session['Campusid']);
						if($lk)
						{
						if($lk->IsLike==1)
						{
					  ?>
					  <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-down"></i> UnLike</button>
					  <?php
						}
						else
						{
						?>
						<button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
						<?php
						}
						}else{
						?>
						<button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
						<?php
						}
						?>
                      <span class="pull-right text-muted"><?=count($postvalue->likes);?> likes  </span>
					  <?php
					  }
					  else
					  {
					  ?>
					  <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
					  <?php
					  }
					  ?>
                    </div>
					<div style="width: 100%;max-height: 200px;overflow: auto;">
					<?php
					if($postvalue->comments)
					{
						foreach($postvalue->comments as $ppkey=>$ppvalue)
						{
							  if($ppvalue->emp->UserTypeId==2){$page='searchcandidate';}elseif($ppvalue->emp->UserTypeId==3){$page='searchcompany';}elseif($ppvalue->emp->UserTypeId==4){$page='searchcampus';}elseif($ppvalue->emp->UserTypeId==5){$page='searchteam';}
                            if($ppvalue->emp->UserTypeId==2 || $ppvalue->emp->UserTypeId==5)
							  {
                                if($ppvalue->emp->PhotoId!=0)
                                {
                                      $ll=$url.$ppvalue->emp->photo->Doc;
                                }
                                else
                                {
                                      $ll=$imageurl.'images/user.png';
                                }
                              }
                              else
                              {
                                if($ppvalue->emp->LogoId!=0)
                                {
                                      $ll=$url.$ppvalue->emp->logo->Doc;
                                }
                                else
                                {
                                      $ll=$imageurl.'images/user.png';
                                }
                              }
					?>
                    <div class="box-footer box-comments" style="display: block;">
                      <div class="box-comment">
                        <img class="img-circle img-sm" src="<?=$ll;?>" alt="User Image">
                        <div class="comment-text">
                         <a href="<?= Url::toRoute([$page,'userid'=>$ppvalue->emp->UserId])?>"> <span class="username">
                          <?=$ppvalue->emp->Name;?>
                          <span class="text-muted pull-right"><?=date('h:i A D M',strtotime($ppvalue->OnDate));?></span>
                          </span></a>
                          <?=$ppvalue->Message;?>
                        </div>
                      </div> 
                    </div>
					<?php
						}
					}
					?>
					</div>
                    <div class="box-footer" style="display: block;">
                     
                        <img class="img-responsive img-circle img-sm" src="<?=Yii::$app->session['CampusDP'];?>" alt="Alt Text">
                        <div class="img-push">
                        <div class="form-group">
                                      <span class="input-icon inverted">
										  <input type="hidden" name="CommentToPost[PostId]" id="commentpostid<?=$postvalue->WallPostId;?>" value="<?=$postvalue->WallPostId;?>"/>
										  <input type="hidden" name="CommentToPost[EmpId]" id="commentempid<?=$postvalue->WallPostId;?>"  value="<?=Yii::$app->session['Campusid'];?>"/>
                                          <input class="form-control input-lg" type="text" id="commentmessage<?=$postvalue->WallPostId;?>" name="CommentToPost[Message]" required>
                                          <p class="help-block help-block-error error<?=$postvalue->WallPostId;?>" style="color:#d73d32;"></p>
										  <button type="button" onclick="Postcomment2(<?=$postvalue->WallPostId;?>);" class="fa fa-envelope bg-palegreen" style="border: none;width: 46px;"></button>
                                      </span>
									  
                                  </div>
                        </div>
                      
                    </div>
                  </div>
				  				  
				  <?php
				  }
			}
			?>
			</div>
							
			   </div> 
			 </div>
        </div>
		
		
		 
		<div class="col-md-3 col-xs-12 padding-left">
		 
		
		                      <div class="widget">	  
							   <div class="widget-header">
							     <h3 class="widget-caption">  Campus Recent Job by company</h3>
							   </div>
		                       <div class="recent-widget-know"> 
								      <div class="rsw"> 
											<aside> 
												<div class="widget_block"> 
													<ul class="related-post">
														<?php
													  if($recentjob)
													  {
													  foreach($recentjob as $jkey=>$jvalue)
													  {
													  ?>
														<li>
															<a href="<?= Url::toRoute(['campus/jobdetail','JobId'=>$jvalue->JobId])?>"> <?=$jvalue->JobTitle;?></a>
															<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?> </span>
															<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
															<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?> </span>
														</li>
													  <?php
													  }
													  }
													  ?>
													</ul>
												</div> 
											</aside>
                                  </div>
							  </div> 
							</div>
							
							 
							
							
							 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Top Recruiter Company</h3>
							</div>
		                  <div class="widget-know"> 
								  <ul class=" ">
										<?php
										  if($topcompany)
										  {
												foreach($topcompany as $tk=>$tval)
												{
												if($tval->LogoId!=0)
												{
													$pimagel=$url.$tval->logo->Doc;
												}
												else
												{
													$pimagel=$imageurl.'images/user.png';
												}  
										  ?>
										<li> <a href="<?= Url::toRoute(['searchcompany','userid'=>$tval->UserId])?>"> <img src="<?=$pimagel;?>" style="width: 50px;height: 50px;" alt="image"><?=$tval->Name;?></a>
										<a href="<?= Url::toRoute(['follower','userid'=>$tval->UserId]);?>"><span><?=count($lik->getTotalfollow($tval->UserId));?> Followers</span></a> </li>
										<?php
										  }
										  }
										  ?>
								  </ul> 
								  
							</div> 
							</div>
							 
							 
							 
							 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Comments</span></h3>
							</div>
			
			<div class="widget-comments">
              <div class="row">
                <div class="col-md-12">
                  <?php
				$lv=new LeaveComment();
				$allcomment=$lv->getComment($campus->UserId);
				if($allcomment)
				{
					foreach($allcomment as $ck=>$cval)
					{
						if($cval->commentFrom->UserTypeId==2 || $cval->commentFrom->UserTypeId==5)
						{
							$ll=($cval->commentFrom->PhotoId!=0)?$url.$cval->commentFrom->photo->Doc:'images/user.png';
						}
						elseif($cval->commentFrom->UserTypeId==3 || $cval->commentFrom->UserTypeId==4)
						{
							$ll=($cval->commentFrom->LogoId!=0)?$url.$cval->commentFrom->logo->Doc:'images/user.png';
						}
						if($cval->commentFrom->UserTypeId==2){$link='searchcandidate';}elseif($cval->commentFrom->UserTypeId==3){$link='searchcompany';}elseif($cval->commentFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
				?>
                  <ul class=" ">
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"> <?=$cval->commentFrom->Name;?></a> </li>
						<li class="comments-block"> 
		                  <p><?=$cval->Comment;?></p>
						  </li>              
                  </ul>
				<?php
					}
				}
				?>
                </div>
              </div>
            </div>
						  </div>
			
		  </div>
		
		
         
      </div>
    </div>
 </div>
     </div>
	
	
<script type="text/javascript">
function liketopost(postid)
{
	 $.ajax({url:"<?= Url::toRoute(['wall/liketopost'])?>?postid="+postid,
			success:function(result)
			{
				 var res=JSON.parse(result);
				 if(res==1)
				 {
				  //location.reload();
				  searchcampuspost();
				 }
			}
	 });
}

function searchcampuspost() {
    $.ajax({url:"<?= Url::toRoute(['wall/searchcampuspost'])?>",
               success:function(results)
               {
                 $('#campusprofilepost').html(results);
               }
        });
}
</script>