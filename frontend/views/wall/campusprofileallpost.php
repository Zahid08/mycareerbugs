<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use common\models\Likes;
$lik=new Likes();
?>


<?php
			if($allpost)
			{
				  foreach($allpost as $postkey=>$postvalue)
				  {
						if($postvalue->employer->LogoId!='')
						{
							  $logo=$url.$postvalue->employer->logo->Doc;
						}
						else
						{
							  $logo=$imageurl.'images/user.png';
						}
			?>
			<div class="box box-widget">
                    <div class="box-header with-border">
                      <div class="user-block">
                        <img class="img-circle" src="<?=$logo;?>" alt="User Image">
                        <span class="username"><a href="#"><?=$postvalue->employer->Name;?></a></span>
                        <span class="description">Shared publicly - <?=date('h:i A D M',strtotime($postvalue->OnDate));?>   - <?=$postvalue->Locationat;?></span>
						 <span class="description"> </span>
                      </div>
                    </div>

                    <div class="box-body" style="display: block;">
						<?php
						if($postvalue->wallimage)
						{
						?>
						<img class="img-responsive pad show-in-modal" src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
						<?php
						}
						?>
                      <p><?=str_replace("\n", '<br />',  $postvalue->Post);?></p> 
                      
					  <?php
					  if($postvalue->likes)
					  {
						$lk=$postvalue->getlike($postvalue->WallPostId,Yii::$app->session['Campusid']);
						if($lk)
						{
						if($lk->IsLike==1)
						{
					  ?>
					  <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-down"></i> UnLike</button>
					  <?php
						}
						else
						{
						?>
						<button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
						<?php
						}
						}else{
						?>
						<button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
						<?php
						}
						?>
                      <span class="pull-right text-muted"><?=count($postvalue->likes);?> likes  </span>
					  <?php
					  }
					  else
					  {
					  ?>
					  <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"><i class="fa fa-thumbs-o-up"></i> Like</button>
					  <?php
					  }
					  ?>
                    </div>
					<div style="width: 100%;max-height: 200px;overflow: auto;">
					<?php
					if($postvalue->comments)
					{
						foreach($postvalue->comments as $ppkey=>$ppvalue)
						{
							  if($ppvalue->emp->UserTypeId==2){$page='searchcandidate';}elseif($ppvalue->emp->UserTypeId==3){$page='searchcompany';}elseif($ppvalue->emp->UserTypeId==4){$page='searchcampus';}elseif($ppvalue->emp->UserTypeId==5){$page='searchteam';}
                            if($ppvalue->emp->UserTypeId==2 || $ppvalue->emp->UserTypeId==5)
							  {
                                if($ppvalue->emp->PhotoId!=0)
                                {
                                      $ll=$url.$ppvalue->emp->photo->Doc;
                                }
                                else
                                {
                                      $ll='images/user.png';
                                }
                              }
                              else
                              {
                                if($ppvalue->emp->LogoId!=0)
                                {
                                      $ll=$url.$ppvalue->emp->logo->Doc;
                                }
                                else
                                {
                                      $ll='images/user.png';
                                }
                              }
					?>
                    <div class="box-footer box-comments" style="display: block;">
                      <div class="box-comment">
                        <img class="img-circle img-sm" src="<?=$ll;?>" alt="User Image">
                        <div class="comment-text">
                         <a href="<?= Url::toRoute([$page,'userid'=>$ppvalue->emp->UserId])?>"> <span class="username">
                          <?=$ppvalue->emp->Name;?>
                          <span class="text-muted pull-right"><?=date('h:i A D M',strtotime($ppvalue->OnDate));?></span>
                          </span></a>
                          <?=$ppvalue->Message;?>
                        </div>
                      </div> 
                    </div>
					<?php
						}
					}
					?>
					</div>
                    <div class="box-footer" style="display: block;">
                     
                        <img class="img-responsive img-circle img-sm" src="<?=Yii::$app->session['CampusDP'];?>" alt="Alt Text">
                        <div class="img-push">
                        <div class="form-group">
                                      <span class="input-icon inverted">
										  <input type="hidden" name="CommentToPost[PostId]" id="commentpostid<?=$postvalue->WallPostId;?>" value="<?=$postvalue->WallPostId;?>"/>
										  <input type="hidden" name="CommentToPost[EmpId]" id="commentempid<?=$postvalue->WallPostId;?>"  value="<?=Yii::$app->session['Campusid'];?>"/>
                                          <input class="form-control input-lg" type="text" id="commentmessage<?=$postvalue->WallPostId;?>" name="CommentToPost[Message]" required>
                                          <p class="help-block help-block-error error<?=$postvalue->WallPostId;?>" style="color:#d73d32;"></p>
										  <button type="button" onclick="Postcomment2(<?=$postvalue->WallPostId;?>);" class="fa fa-envelope bg-palegreen" style="border: none;width: 46px;"></button>
                                      </span>
									  
                                  </div>
                        </div>
                      
                    </div>
                  </div>
				  				  
				  <?php
				  }
			}
			?>