<?php
$this->title = Yii::$app->session['EmployerName'];
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';

use common\models\Likes;

$lik = new Likes();

use common\models\LeaveComment;
use common\models\Documents;
use frontend\components\searchpeople\SearchPeople;
use frontend\components\searchcity\SearchCity;
$empId = Yii::$app->session['Employerid'];
?>

    <style>
    #no-charge{display:none !important;}
    .comment.more p a {background: #e9e9e9;color: #000;padding: 9px;font-size: 12px;width: 100%;display: block;}

        .navbar {
            min-height: 41px !important;
        }
        
        .select2-selection.select2-selection--single {
            height: 34px !important;
        }
        
        #wall_description1 .widget {
            margin: 0;
            box-shadow: 0 0 0 #000;
        }
        
        #wall_description1 {
            position: fixed;
            z-index: 999;
            bottom: 0px;
            left: 0px;
            width: 100%;
            margin-bottom: 0px;
        }
        
        #wall_description1 .widget-body {
            padding: 12px 12px 0px 12px;
        }
        
        .ui-autocomplete {
            width: 179.6px;
            display: block;
            background-color: white;
            margin-top: -19px;
        }
        
        .cover.profile .cover-info .cover-nav li.no_desk {
            display: none
        }
        
        @media (max-width: 767px) {
            .cover.profile .cover-info {
                background: #6c146b !important;
                clear: both;
                height: auto;
            }
            .cover.profile .cover-info .cover-nav li a {
                color: #fff !important;
            }
            .cover.profile .cover-info .cover-nav {
                margin: 11px 0 0 0 !important;
            }
            .cover.profile .cover-info .cover-nav li.no_desk {
                display: block !important
            }
        }
    </style>

    <div class="cover profile" id="cover_photo_b">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="cover-info">
                        <div class="profile_main" style="display: none !important">
                            <img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
                        </div>
                        <div class="name" style="display: none">
                            <a href="#">My Career Bugs Wall</a>
                        </div>
                        <ul class="cover-nav" style="left: 0px; top: 0px; ">

                            <!--	<li class="active" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i></span> <span id="fstatus"><?=$fs;?></span></a></li> -->

                            <li style="display: none"><a href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>

                            <li>
                                <a href="<?= Url::base().'/'. str_replace(' ','',$model->Name).'-'.$model->UserId;?>">  My Profile
                                </a>
                            </li>
                            <li><a href="<?= Url::toRoute(['site/myactivity']);?>"> My Activity</a></li>

                           
				 
						<li><a href="<?= Url::toRoute(['site/hirecandidate']);?>"> Hire Candidare</a></li>
				  
				  

                            <li><a href="<?= Url::toRoute(['wall/companyprofile']);?>">  Wall Post </a></li>

                        </ul>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group right_main" id="mobile_view_only">

        <?php echo SearchPeople::widget()?>

    </div>

    <!-- Begin page content -->
    <div class="page-content">
        <div class="cover profile">
            <div class=" ">
                <div class="image">
                    <img src="<?=$imageurl;?>careerimg/background1.jpg" class="show-in-modal" alt="people" style="display:none">
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12" style="display:none">
                        <div class="cover-info">

                            <div class="profile_main">
                                <img src="<?=Yii::$app->session['EmployerDP'];?>" alt="people" class="">
                            </div>
                            <div class="name">
                                <a href="#"><?=Yii::$app->session['EmployerName'];?></a>
                            </div>
                            <ul class="cover-nav">
                                <li class="active"><a href="<?= Url::toRoute(['wall/companyprofile'])?>"><i									class="fa fa-user"></i> Profile Wall </a></li>
                                <!--<li  class=""><a href="#"><i class="fa fa-user-plus"></i> Follow </a></li>  
                                <li  class=""><a href="#"><i class="fa fa-envelope"></i> Leave a comment </a></li>-->
                                <li class=""><a href="<?= Url::toRoute(['site/yourpost'])?>"><i									class="fa fa-eye"></i> View Job Post</a></li>
                            </ul>
                            <div class="form-group right_main">
                                <input class="form-control1" name="name" id="project" placeholder="Search for People, Company and Consultant" type="text">
                                <input type='hidden' id="termid" />
                                <input class="src1" id="searchit" value="Search" type="button"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Begin page content -->
        <div class="page-content like_pae">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="widget no_shadow" style="display: none;">
                            <div class="action-buttons">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="half-block-left"> <a href="#" class="btn btn-azure btn-block"><i											class="fa fa-user-plus"></i> Follow</a> </div>
                                        <div class="half-block-right"> <a href="#" class="btn btn-azure btn-block"><i											class="fa fa-envelope"></i> Message</a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="profile_main" style="width:100%">
                            <img src="<?=Yii::$app->session['EmployerDP'];?>" alt="people" class="" style="width:40%; margin:0 auto; display:block;border-radius:100px">
                        </div>
                        <div class="name" style="text-align:center; margin: 0 0 10px 0">
                            <a href="#"><?=Yii::$app->session['EmployerName'];?></a>
                        </div>

                        <div class="widget">
                            <div class="widget-header">
                                <h3 class="widget-caption">Statistics</h3> </div>
                            <div class="section bordered-sky">
                                <p> <a href="<?= Url::toRoute(['follower','userid'=>$model->UserId]);?>"><span									class="badge"><?=count($lik->getTotalfollow($model->UserId));?></span>									<i class="  fa fa-thumbs-up"></i> Followers</a> </p>
                                <p> 
								<?php if($empId == $model->UserId){?>
								<a href="<?= Url::toRoute(['wall/companyprofile'])?>">
									<span class="badge"><?=count($allpost);?></span><i class=" fa fa-comments"></i> Posts
									</a> 
								<?php }else{?>	
								<a href="<?= Url::toRoute(['wall/searchcompany','userid' => $model->UserId])?>">
									<span class="badge"><?=count($allpost);?></span><i class=" fa fa-comments"></i> Posts
									</a> 
								<?php }?>
								</p>
                                <p>
								<?php if($empId == $model->UserId){?>
								<a href="<?= Url::toRoute(['site/yourpost']);?>">
									<span class="badge"><?=$totalactive;?></span> <i class=" fa fa-comments"></i> Active Jobs
								</a> 
								<?php }else{?>
								<a href="<?= Url::toRoute(['site/jobsearch','company' => $model->UserId]);?>">
									<span class="badge"><?=$totalactive;?></span> <i class=" fa fa-comments"></i> Active Jobs
								</a> 
								<?php }?>
								</p>
                                <p style="display: none;"> <span class="badge"> 250+</span> <i class=" fa fa-user"></i> Total Employee </p>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="widget-header">
                                <h3 class="widget-caption">Description</h3> </div>
                            <div class="widget-body bordered-top bordered-sky">
                                <?=$model->CompanyDesc;?>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="widget-header">
                                <h3 class="widget-caption">Basic Skill</h3> </div>
                            <div class="basic_skill">
                                <?php

    if ($skills) 
    {

        foreach ($skills as $sk => $sval) 
        {

            ?>
                                    <p>
                                        <?=$sval;?>
                                    </p>
                                    <?php
        }
    }

    ?>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="widget-header">
                                <h3 class="widget-caption">Social Network</h3> </div>
                            <div class="widget-body bordered-top bordered-sky">
                                <div class="row">
                                    <div class="directory-info-row col-xs-12">
                                        <ul class="social-links">
                                            <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget widget-friends">
                            <div class="widget-header">
                                <h3 class="widget-caption">Followers</h3> </div>
                            <div class="widget-like company">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class=" ">
                                            <?php

    $followers = $lik->getTotalfollow($model->UserId);

    if ($followers) 
    {

        foreach ($followers as $fk => $fvalue) 
        {

            if ($fvalue->likeFrom->UserTypeId == 2 || $fvalue->likeFrom->UserTypeId == 5) 
            {
                $ll = Documents::getImageByAttr($fvalue->likeFrom, 'PhotoId', 'photo');
            } 
            else 
            {
                $ll = Documents::getImageByAttr($fvalue->likeFrom, 'LogoId', 'logo');

            }

            if ($fvalue->likeFrom->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->likeFrom->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->likeFrom->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }

            ?>
                                                <li><a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>">												<img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a> </li>
                                                <?php
        }
    }

    ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget widget-friends">
                            <div class="widget-header">
                                <h3 class="widget-caption">Videos</h3> </div>
                            <div class="widget-like company">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class=" "> </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 padding-left">
                        <div class="row">
                            <!-- left posts-->
                            <div class="col-md-12">
								
				<div class="box profile-info">
            <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
                 <input type="text" id="post_title"
							class="form-control"
							name="CompanyWallPost[PostTitle]" required placeholder="Enter Post Title" /><br/>
				<textarea id="post_d"
							class="form-control input-lg p-text-area"
							name="CompanyWallPost[Post]" required rows="2"
							placeholder="Post your job here..."></textarea>
							
						<div class="box-footer">
							<ul class="nav nav-pills">
								<li><select name="CompanyWallPost[CIndustryId]"
									required="required" class="form-control bfh-states">
										<option selected="selected" value="">Select an Industry</option>
										<?php foreach($industry as $key=>$value){?>
										<option value="<?php echo $key;?>"><?=$value;?></option>
										<?php } ?>
									</select></li>

								<li><select class="questions-category form-control  "
									name="CompanyWallPost[PositionId]" required="required"
									id="emppos">
										<option value="">Select Role</option>
						<?php
        foreach ($roles as $key => $value) {
            ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
        }
        ?>
					</select></li>
								<li  class="slc_cty"><input id="citylist" type="text" class="form-control"
									name="CompanyWallPost[Locationat]" placeholder="Select City">
									<div id="cityContainer"
										style="background-color: white !important; margin-top: 18px;">
									</div></li>
								<li><a class="cameraa"><i class="fa fa-camera"></i><input
										type="file" name="CompanyWallPost[ImageId]"
										onchange="$(this).next().html($(this).val());"
										accept="image/*"><span></span></a></li>
								<li  style="display:none"><a href="javascript:void(0)" class="cameraa"><i
										class=" fa fa-film"></i><input type="file"
										name="CompanyWallPost[VideoId]"
										onchange="$(this).next().html($(this).val());"
										accept="video/*"><span></span></a></li>
										
										<li><button id="post_btn" type="submit"
								class="btn btn-info pull-right">Post</button></li>
							</ul>
							<div class="clear"></div>

							
							<div class="clear"></div>



						</div>
			


			<?php ActiveForm::end(); ?>
			</div>
				
							
							
                                <div class="box profile-info" style="display:none">
                                    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
                                        <textarea class="form-control input-lg p-text-area" name="CompanyWallPost[Post]" required rows="2" placeholder="Whats in your mind today?"></textarea>
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-info pull-right">Post</button>
                                            <ul class="nav nav-pills">
                                                <li>
                                                    <input type="text" id="pac-input" placeholder="Location At" class="form-control" name="CompanyWallPost[Locationat]" style="display: none;" /> <a href="#" onclick="$('#pac-input').slideToggle();"><i												class="fa fa-map-marker"></i></a></li>
                                                <li><a class="cameraa"><i class="fa fa-camera"></i><input												type="file" name="CompanyWallPost[ImageId]"												onchange="$(this).next().html($(this).val());"												accept="image/*"><span></span></a></li>
                                                <li><a href="javascript:void(0)" class="cameraa"><i												class=" fa fa-film"></i><input type="file"												name="CompanyWallPost[VideoId]"												onchange="$(this).next().html($(this).val());"												accept="video/*"><span></span></a></li>
                                            </ul>
                                        </div>
                                        <?php ActiveForm::end(); ?>
                                </div>
                                <div class="widget-header">
                                    <h3 class="widget-caption">Quick Update</h3> </div>
                                <div class="row">
                                    <div class="col-md-12" id="allcompanypost">
                                        <!-- post state form -->
                                        <?php

if ($allpost) 
{
	/*echo '<pre>';
	print_r($allpost);die;*/

    foreach ($allpost as $postkey => $postvalue) 
    {
		if(!empty($postvalue->Slug)){
			$urlLink = Url::base().'/wall-post/'.$postvalue->Slug;
		}else{
			$urlLink = Url::toRoute(['/wall/post-view','id' =>  $postvalue->WallPostId]);	
		}

        if ($postvalue->employer->LogoId != '') 
        {

            $logo = $url . $postvalue->employer->logo->Doc;
        } 
        else 
        {

            $logo = $url . 'images/user.png';
        }

        ?>
	<div class="box profile-info" id="editbox<?=$postvalue->WallPostId;?>" style="display: none;">
		<?php $form = ActiveForm::begin(['options' => ['class'=>'editpostform'.$postvalue->WallPostId,'enctype'=>'multipart/form-data']]); ?>
			<input type="text" id="post_title"
							class="form-control"
							name="CompanyWallPost[PostTitle]" required placeholder="Enter Post Title" value="<?=trim($postvalue->PostTitle);?>" /><br/>
			<textarea class="form-control input-lg p-text-area" name="CompanyWallPost[Post]" required rows="2" placeholder="Whats in your mind today?">
				<?=trim($postvalue->Post);?>
			</textarea>
			<input type="hidden" name="CompanyWallPost[WallPostId]" value="<?=$postvalue->WallPostId;?>" />
			<div class="box-footer">
				<ul id="delimg<?=$postvalue->WallPostId;?>">
					<?php if ($postvalue->ImageId != 0){?>
						<li><img src="<?=$url.$postvalue->wallimage->Doc;?>" style="width: 150px; height: 150px;" /></li>
						<li style="cursor: pointer;" onclick="delimg(<?=$postvalue->WallPostId;?>,'companyprofile');">X</li>
						<?php
}

?>
				</ul>
			</div>
			<div class="box-footer">
				<button type="submit" class="editpost btn btn-info pull-right" pid="<?=$postvalue->WallPostId;?>">Update Post</button>
				<ul class="nav nav-pills">
					<li><select name="CompanyWallPost[CIndustryId]"
									required="required" class="form-control bfh-states">
										<option selected="selected" value="">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>" <?php echo (($key == $postvalue->CIndustryId)?"selected":"");?>><?=$value;?></option>
						<?php } ?>
					</select></li>

								<li><select class="questions-category form-control  "
									name="CompanyWallPost[PositionId]" required="required"
									id="emppos">
										<option value="">Select Role</option>
						<?php
        foreach ($roles as $key => $value) {
            ?>
						<option value="<?=$value->PositionId;?>" <?php echo (($value->PositionId == $postvalue->PositionId)?"selected":"");?>><?=$value->Position;?></option>
						<?php
        }
        ?>
					</select></li>
								<li  class="slc_cty">
									<?php echo SearchCity::widget(['model' => $postvalue, 'attr' => 'Locationat', 'form' => $form])?>
									</li>
								<li><a class="cameraa"><i class="fa fa-camera"></i><input
										type="file" name="CompanyWallPost[ImageId]"
										onchange="$(this).next().html($(this).val());"
										accept="image/*"><span></span></a></li>
								<li  style="display:none"><a href="javascript:void(0)" class="cameraa"><i
										class=" fa fa-film"></i><input type="file"
										name="CompanyWallPost[VideoId]"
										onchange="$(this).next().html($(this).val());"
										accept="video/*"><span></span></a></li>
							</ul>
			</div>
			<?php ActiveForm::end(); ?>
	</div>
	<div class="box box-widget">
		<div class="box-header with-border">
			<div class="user-block"> <img class="img-circle" src="<?=$logo;?>" alt="User Image"> <span class="username"><a													href="<?= Url::toRoute(['searchcompany','userid'=>$postvalue->EmployerId])?>"><?=$postvalue->employer->Name;?></a></span> <span class="description">Shared publicly - <?=date('h:i A D M',strtotime($postvalue->OnDate));?>   - <?=$postvalue->Locationat;?></span> <span class="postright" onclick="$(this).next().toggle();"><i													class="fa fa-angle-down"></i> </span>
				<div class="postbox">
					<ul>
						<li style="width:100%" onclick="$('#editbox<?=$postvalue->WallPostId;?>').show();">Edit</li>
						<li style="width:100%" onclick="deletepost1(<?=$postvalue->WallPostId;?>);">Delete</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="box-body" style="display: block;">
		<?php
			if ($postvalue->wallimage){ ?>
              <img class="img-responsive pad show-in-modal" src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
        <?php }

        if ($postvalue->wallvideo){?>
                                                            
			<video width="320" height="240" controls>
				<source src="<?=$url.$postvalue->wallvideo->Doc;?>" type="video/mp4"> </video>
			<?php
        }?>
		<div class="comment more">
		<p><a href="<?=$urlLink;?>"><?=$postvalue->PostTitle;?></a></p>
        <?=str_replace("\n", '<br />',  $postvalue->Post);?>
		</div>
		<?php

        if ($postvalue->likes) 
        {

            $lk = $postvalue->getlike($postvalue->WallPostId, Yii::$app->session['Campusid']);

            if ($lk) 
            {

                if ($lk->IsLike == 1) 
                {

                    ?>
                                                                    <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-down"></i> UnLike </button>
                                                                    <?php
                } 
                else 
                {

                    ?>
                                                                        <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-up"></i> Like </button>
                                                                        <?php
                }
            } else {

                ?>
                                                                            <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-up"></i> Like </button>
                                                                            <?php
            }

            ?>
                                                                                <span class="pull-right text-muted"><?=count($postvalue->likes);?> likes  </span>
                                                                                <?php
        } 
        else 
        {

            ?>
                                                                                    <button type="button" class="btn btn-default btn-xs" onclick="liketopost(<?=$postvalue->WallPostId;?>);"> <i class="fa fa-thumbs-o-up"></i> Like </button>
                                                                                    <?php
        }

        ?>
		
		<a href="<?=$urlLink;?>">View </a>
                                               
												
												<br class="clear"/>
												<div class="share" style="float: right; padding-top: 0px;">
												<?php 
														$whatsapp_desc = 'Job opportunity at ' . $postvalue->employer->Name . " for Location " . $postvalue->Locationat . " for more detils visit " . $urlLink;
													?>													
												Share On <a
													href="https://www.facebook.com/sharer/sharer.php?u=<?=$urlLink;?>"
													target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i>
												</a>
												<!-- <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://developer.linkedin.com&title=LinkedIn%20Developer%20Network&summary=My%20favorite%20developer%20program&source=LinkedIn" target="_blank">
															 <i class="fa fa-linkedin"></i>
														  </a> -->
												<a href="javascript:;"
													onclick="mailtoemp('myModal_email','<?=$postvalue->WallPostId;?>');"
													style="line-height: 32px;"><i class="fa fa-envelope-o"></i> </a> <a
													href="https://api.whatsapp.com/send?text=<?=strip_tags($whatsapp_desc);?>"
													target="_blank"><i class="fa fa-whatsapp"></i></a>

											</div>
											
											   </div>
											   
											   
											   
											   <div id="PostComments<?=$postvalue->WallPostId;?>" class="postcomments">
														<?php
												echo Yii::$app->controller->renderPartial('_postComments', [
													'dataProvider' => $postvalue->getPostCommentData(),
													'empdp' => $empdp,
													'empid' => $empid
												])?>
																	</div>
												
												<?php /* 
                                               <div style="width: 100%; max-height: 200px; overflow: auto;">
                                                    <?php

        if ($postvalue->comments) 
        {

            foreach ($postvalue->comments as $ppkey => $ppvalue) 
            {

                if ($ppvalue->emp->UserTypeId == 2) {
                    $page = 'searchcandidate';
                } elseif ($ppvalue->emp->UserTypeId == 3) {
                    $page = 'searchcompany';
                } elseif ($ppvalue->emp->UserTypeId == 4) {
                    $page = 'searchcampus';
                } elseif ($ppvalue->emp->UserTypeId == 5) {
                    $page = 'searchteam';
                }

                if ($ppvalue->emp->UserTypeId == 2 || $ppvalue->emp->UserTypeId == 5) 
                {

                    if ($ppvalue->emp->PhotoId != 0) 
                    {

                        $ll = $url . $ppvalue->emp->photo->Doc;
                    } 
                    else 
                    {

                        $ll = $imageurl . 'images/user.png';
                    }
                } 
                else 
                {

                    if ($ppvalue->emp->LogoId != 0) 
                    {

                        $ll = $url . $ppvalue->emp->logo->Doc;
                    } 
                    else 
                    {

                        $ll = $imageurl . 'images/user.png';
                    }
                }

                ?>
                                                        <?php

                if (Yii::$app->session['Employerid'] == $ppvalue->emp->UserId) 
                {

                    ?>
                                                            <div class="box-footer boxcommentedit<?=$ppvalue->CommentToPostId;?>" style="display: none;">
                                                                <?php $form = ActiveForm::begin(); ?>
                                                                    <img class="img-responsive img-circle img-sm" src="<?=Yii::$app->session['EmployerDP'];?>" alt="Alt Text">
                                                                    <div class="img-push">
                                                                        <div class="form-group"> <span class="input-icon inverted"> <input type="hidden"															name="CommentToPost[CommentToPostId]"															value="<?=$ppvalue->CommentToPostId;?>" /> <input															class="form-control input-lg" type="text"															value="<?=$ppvalue->Message;?>"															name="CommentToPost[Message]" required>															<button type="submit" class="fa fa-envelope bg-palegreen"																style="border: none; width: 46px;"></button>														</span> </div>
                                                                    </div>
                                                                    <?php ActiveForm::end(); ?>
                                                            </div>
                                                            <?php
                }

                ?>
                                                                <div class="box-footer box-comments" style="display: block;">
                                                                    <div class="box-comment"> <img class="img-circle img-sm" src="<?=$ll;?>" alt="User Image">
                                                                        <div class="comment-text"> <span class="username"> <a															href="<?= Url::toRoute([$page,'userid'=>$ppvalue->emp->UserId])?>"><?=$ppvalue->emp->Name;?></a>															<span class="text-muted pull-right"><?=date('h:i A D M',strtotime($ppvalue->OnDate));?></span>
                                                                            <?php

                if (Yii::$app->session['Employerid'] == $ppvalue->emp->UserId) 
                {

                    ?>
                                                                                <span class="pull-right" style="cursor: pointer;" onclick="$('.boxcommentedit<?=$ppvalue->CommentToPostId;?>').show();"><i																class="fa fa-edit"></i></span> <span class="pull-right" style="cursor: pointer;" onclick="delcomment(<?=$ppvalue->CommentToPostId;?>,'companyprofile');"><i																class="fa fa-trash"></i></span>
                                                                                <?php
                }

                ?>
                                                                                    </span>
                                                                                    <?=$ppvalue->Message;?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php
            }
        }

        ?>										
                                                </div>
												
												
												*/?>
												
                                                <div class="box-footer" style="display: block;"> <img class="img-responsive img-circle img-sm" src="<?=Yii::$app->session['EmployerDP'];?>" alt="Alt Text">
                                                    <div class="img-push">
                                                        <div class="form-group"> <span class="input-icon inverted"> <input type="hidden"														name="CommentToPost[PostId]"														id="commentpostid<?=$postvalue->WallPostId;?>"														value="<?=$postvalue->WallPostId;?>" /> <input														type="hidden" name="CommentToPost[EmpId]"														id="commentempid<?=$postvalue->WallPostId;?>"														value="<?=Yii::$app->session['Employerid'];?>" /> <input														class="form-control input-lg" type="text"														id="commentmessage<?=$postvalue->WallPostId;?>"														name="CommentToPost[Message]" required>														<p															class="help-block help-block-error error<?=$postvalue->WallPostId;?>"															style="color: #d73d32;"></p>														<button type="button"															onclick="Postcomment(<?=$postvalue->WallPostId;?>);"															class="fa fa-envelope bg-palegreen"															style="border: none; width: 46px;"></button>													</span> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php
    }
}

?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 padding-left">
                        <div class="widget">
                            <div class="widget-header">
                                <h3 class="widget-caption">								Suggested Candidate</span>							</h3> </div>
                            <div class="widget-know">
                                <ul class=" ">
                                    <?php

        if ($suggested) 
        {

            foreach ($suggested as $sk => $sval) 
            {
                $ph = Documents::getImageByAttr($sval->user, 'PhotoId', 'photo');

                ?>
                                        <li><a href="<?= Url::toRoute(['searchcandidate','userid'=>$sval->user->UserId])?>">										<img src="<?=$ph;?>" alt="image"><?=$sval->user->Name;?>
										<span><?=$sval->user->EntryType;?></span>								</a></li>
                                        <?php
            }
        }

        ?>
                                </ul>
                            </div>
                        </div>      <div class="clear"></div>   
                        <div class="widget widget-friends">
                            <div class="widget-header">
                                <h3 class="widget-caption">Follow</h3> </div>
                            <div class="widget-like company">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class=" ">
                                            <?php

    $followers = $lik->getMyfollow($model->UserId);

    if ($followers) 
    {

        foreach ($followers as $fk => $fvalue) 
        {

            if ($fvalue->likeTo->UserTypeId == 2 || $fvalue->likeTo->UserTypeId == 5) 
            {
                $ll = Documents::getImageByAttr($fvalue->likeTo, 'PhotoId', 'photo');

            } 
            else 
            {
                $ll = Documents::getImageByAttr($fvalue->likeTo, 'LogoId', 'logo');

            }

            if ($fvalue->likeTo->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->likeTo->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->likeTo->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }

            ?>
                                                <li><a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeTo->UserId])?>">												<img src="<?=$ll;?>" alt="image"><?=$fvalue->likeTo->Name;?></a> </li>
                                                <?php
        }
    }

    ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clear"></div>         
               
                        <div class="widget">
                            <div class="widget-header">
                                <h3 class="widget-caption">								<span>Latest Reviews</span>							</h3> </div>
                            <div class="widget-comments">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php

                $lv = new LeaveComment();

                $allcomment = $lv->getComment($model->UserId);

                if ($allcomment) 
                {

                    foreach ($allcomment as $ck => $cval) 
                    {

                        if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) 
                        {
                            $ll = Documents::getImageByAttr($cval->commentFrom, 'PhotoId', 'photo');
                        } 
                        elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) 
                        {
                            $ll = Documents::getImageByAttr($cval->commentFrom, 'LogoId', 'logo');
                        }

                        if ($cval->commentFrom->UserTypeId == 2) {
                            $link = 'searchcandidate';
                        } elseif ($cval->commentFrom->UserTypeId == 3) {
                            $link = 'searchcompany';
                        } elseif ($cval->commentFrom->UserTypeId == 4) {
                            $link = 'searchcampus';
                        } else {
                            $link = 'searchteam';
                        }

                        ?>
                                            <ul class=" ">
                                                <li><a href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>">												<img src="<?=$ll;?>" alt="image">   <?=$cval->commentFrom->Name;?></a> </li>
                                                <li class="comments-block">
                                                    <p>
                                                        <?=$cval->Comment;?>
                                                    </p>
                                                </li>
                                            </ul>
                                            <?php
                    }
                }

                ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="widget">
                            <div class="advertisement bordered-sky"> <img src="<?=$imageurl;?>careerimg/adban_block/ban.jpg"> <img class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban.jpg"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function liketopost(postid) {
            $.ajax({
                url: "<?= Url::toRoute(['wall/liketopost'])?>?postid=" + postid,
                success: function(result) {
                    var res = JSON.parse(result);
                    if (res == 1) {
                        //location.reload();
                        allpost1();
                    }
                }
            });
        }
		$(document).ready(function () {  
			$("#citylist").autocomplete({ 
				source: "<?php echo Url::toRoute(['/'])?>"+'wall/getcities', 
				minLength:1,
				appendTo: "#cityContainer" 
			});  
		});
    </script>
	<style>
		a.morelink {
			text-decoration: none;
			outline: none;
		}
		
		.morecontent span {
			display: none;
		}
	</style>