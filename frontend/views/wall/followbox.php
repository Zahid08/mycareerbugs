<?php
use yii\helpers\Url;

use yii\helpers\Html;

use yii\bootstrap\ActiveForm;

use common\models\Likes;

use common\models\LeaveComment;

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';

$url = '/backend/web/';

?>
<?php

$lik = new Likes();

$isfollow = $lik->getIsfollow($empid, $userid);

if (empty($isfollow)) {

    if ($empid != $userid) {

        ?>

<div class="half-block-left"
	onclick="follow(<?=$userid;?>,<?=$empid;?>);">

	<a href="javascript:;" class="btn btn-azure btn-block"><i
		class="fa fa-user-plus"></i> Follow</a>

</div>

<?php
    }
} else {

    ?>

<div class="half-block-left"
	onclick="unfollow(<?=$userid;?>,<?=$empid;?>);">

	<a href="javascript:;" class="btn btn-azure btn-block"><i
		class="fa fa-user-plus"></i> UnFollow</a>

</div>

<?php
}

?>
								  <?php

        if ($empid != $userid) {

            ?>

<div class="half-block-right">

	<a href="javascript:;" data-toggle="modal" data-target="#myModalmessage"
		class="btn btn-azure btn-block"><i class="fa fa-envelope"></i> Message</a>

</div>

<?php
        }

        ?>
|<?=count($lik->getTotalfollow($userid));?>