<?php
$this->title ='Followers';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use common\models\Likes;
$lik=new Likes();
?>

    <!-- Begin page content -->
    <div class="row page-content">
   
          <div class="cover profile">
            <div class=" ">
              <div class="image">
                <img src="<?=$imageurl;?>careerimg/background.jpg" class="show-in-modal" alt="people">
              </div>  
            </div>
			
			
			 <div class="container">
				  <div class="row">
					<div class="col-md-12"> 
					
						<div class="cover-info">
						  <div class="profile_main">
							<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
						  </div>
						  <div class="name"><a href="#">Career Bugs</a></div>
						  <ul class="cover-nav"> 
								 
						  </ul> 
						</div>
					  </div>
					</div>
       
      </div>
	  
	     </div>
	  
	  
    <!-- Begin page content -->
 <div class="page-content like_pae">
    <div class="container">
      <div class="row">
	
		 <div class="col-md-12">
	 
          <ul class="directory-list" style="display: none;">
                <li><a href="#">a</a></li>
                <li><a href="#">b</a></li>
                <li><a href="#">c</a></li>
                <li><a href="#">d</a></li>
                <li><a href="#">e</a></li>
                <li><a href="#">f</a></li>
                <li><a href="#">g</a></li>
                <li><a href="#">h</a></li>
                <li><a href="#">i</a></li>
                <li><a href="#">j</a></li>
                <li><a href="#">k</a></li>
                <li><a href="#">l</a></li>
                <li><a href="#">m</a></li>
                <li><a href="#">n</a></li>
                <li><a href="#">o</a></li>
                <li><a href="#">p</a></li>
                <li><a href="#">q</a></li>
                <li><a href="#">r</a></li>
                <li><a href="#">s</a></li>
                <li><a href="#">t</a></li>
                <li><a href="#">u</a></li>
                <li><a href="#">v</a></li>
                <li><a href="#">w</a></li>
                <li><a href="#">x</a></li>
                <li><a href="#">y</a></li>
                <li><a href="#">z</a></li>
          </ul>
       
   
		  
		
		
            <div class="panel panel-default" id="followers_block">
              
              <div class="panel-body">
                <p class="common-friends">  Followers</p>
				 
                <?php
                if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeFrom->UserTypeId==2 || $fvalue->likeFrom->UserTypeId==5)
							  {
									if($fvalue->likeFrom->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeFrom->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeFrom->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeFrom->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
							  
							  if($fvalue->likeFrom->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeFrom->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
				<div class="panel">
                  <div class="panel-body">
                      <div class="media">
                          <a class="pull-left" href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>">
                              <img class="thumb media-object" style="width: 110px;height: 110px;" src="<?=$ll;?>" alt="">
                          </a>
                          <div class="media-body">
                              <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>"><h4><?=$fvalue->likeFrom->Name;?>  <br> <span class="text-muted small"><small><?=$fvalue->likeFrom->City;?></small> </span></h4> </a>
                              <?php
                                    if($fvalue->likeFrom->UserTypeId==2)
                                    {
                                    ?>
                              <address> 
							  <strong>Skill :</strong> <br>
                                 <div class="skill_bl">
                                    <?php
                                    foreach($fvalue->likeFrom->empRelatedSkills as $ask=>$asv)
                                    {
                                        ?>
                                    <span> <?=$asv->skill->Skill;?> </span>
                                   <?php
                                    }
                                    ?>
                                 </div>
                              </address>
                                 <?php
                                    }
                                    ?>
                          </div>
                      </div>
                  </div>
                </div>
				<?php
                        }
                    }
                        ?>
				
                
              </div>
             
            </div>
          
      </div>
         
      </div>
    </div>
 </div>
     </div>