<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request Password Reset';
?>

<style>
  #wall_forget{padding:40px 0 0 0}
 </style>
	 
	 
    <!-- Begin page content -->
    <div id="landing_page_banner" class="page-content">
      
	  	<div class="white"> 
						 <div class="omb_login" id="wall_forget"> 
						
						<h6>Reset Password<h6>

						<div class="row omb_row-sm-offset-3">
							<div class="col-xs-12 col-sm-6">	
                                <?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm']]); ?>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input class="form-control" name="AllUser[Email]" required  placeholder="Email address">
									</div>
								  
									 <!-- <span class="help-block">Password error</span>  -->

									<button class="btn btn-lg btn-primary btn-block" type="submit">Reset</button>
								<?php ActiveForm::end(); ?>
							</div>
						</div>
						  
					</div> 
		 <div class="clear"></div> 
      </div>
    </div>