<?php
$this->title = 'My Career Bugs Wall, MCB';

use common\models\City;
use common\models\Follow;
use common\models\Likes;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use frontend\components\sidebar\SidebarWidget;
use frontend\components\searchpeople\SearchPeople;
use common\components\loadmore\LoadMore;

$lik = new Likes();
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
$cj = ArrayHelper::map(City::find()->all(), 'CityName', 'CityName');

if (isset(Yii::$app->session['Employerid'])) {
    $empid = Yii::$app->session['Employerid'];
    $empdp = Yii::$app->session['EmployerDP'];
    $type = 3;
    $ppage = 'companyprofile';
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $empid = Yii::$app->session['Employeeid'];
    $empdp = Yii::$app->session['EmployeeDP'];
    $type = 2;
    $ppage = 'candidateprofile';
} elseif (Yii::$app->session['Campusid']) {
    $empid = Yii::$app->session['Campusid'];
    $empdp = Yii::$app->session['CampusDP'];
    $type = 4;
    $ppage = 'campusprofile';
} elseif (Yii::$app->session['Teamid']) {
    $empid = Yii::$app->session['Teamid'];
    $empdp = Yii::$app->session['TeamDP'];
    $type = 5;
    $ppage = 'teamprofile';
}

$follow = new Follow();
$isfollow = $follow->getIsfollow($empid);
$totalfollow = $follow->getTotalfollow($empid);
$totalfollowlist = $follow->getTotalfollowlist($empid);
if ($isfollow == 1) {
    $fs = 'Unfollow';
} else {
    $fs = 'Follow';
}
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css">
<script src="https://www.mycareerbugs.com/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js"></script>

<style>

.comment.more p a{background: #e9e9e9;
    color: #000;
    padding: 9px;
    font-size: 12px;
    width: 100%;
    display: block;}
    
    .comment.more p a:hover{background:#dedddd}

    .slc_cty .form-control.ui-autocomplete-input{width: 145px;
    background: #71126e !important;
    color: #fff !important;
    opacity: 1;
    border-radius: 4px !important;}
    
#cityContainer-filter .ui-autocomplete {
	margin-left: 51%;
	width: 343.888px;
	display: block;
	margin-bottom: 10%;
}

#allpost-pjax .summary {
	position: absolute;
	z-index: 999;
	right: 23px;
	top: -24px;
	font-size: 11px;
	color: #fff;
}

a.morelink {
	text-decoration: none;
	outline: none;
}

.morecontent span {
	display: none;
}

.empty {
	font-size: 12px;
	padding-left: 11px;
	margin-bottom: -3px;
}

.comment {
	font-size: 12px;
	margin: 0 10px 10px 10px;
}

.postcomments {
	width: 100%;
	max-height: 170px;
	overflow-y: scroll;
}

.postcomments_view_more {
	width: 100%;
	cursor: pointer;
}

.LessComments {
	display: none;
}

#ClickToSeeMoreComments {
	cursor: pointer;
}

#ClickToSeeLessComments {
	cursor: pointer;
}
</style>
<style>
.navbar {
	min-height: 41px !important;
}

.select2-selection.select2-selection--single {
	height: 34px !important;
}

#wall_description1 .widget {
	margin: 0;
	box-shadow: 0 0 0 #000;
}

#wall_description1 {
	position: fixed !important;
	z-index: 9999999999;
	bottom: 0px;
	background:#fff;
	left: 0px;
	width: 100%;
	margin-bottom: 0px;
}



#wall_description1 .widget-body {
	padding: 12px 12px 0px 12px;
}

.ui-autocomplete {
	width: 179.6px;
	display: block;
	background-color: white;
	margin-top: -19px;
}
.button_click-1{display:none;}

@media (max-width: 767px) {
#w2{display:none;}
.filter_tab{padding-bottom:15px}
.cover.profile .cover-info {background: #6c146b !important;clear: both;height: auto;}
.cover.profile .cover-info .cover-nav li a{color:#fff !important;}
.cover.profile .cover-info .cover-nav {margin: 11px 0 0 0 !important;}
.button_click-1{display:block !important;    width: 32px;float: right;    border-radius: 4px; position:absolute;right:10px; top:10px; min-height: 28px;border: 0!important;background: #6d136b!important;}
.button_click-1 img{margin:0 auto;display:block;width: 10px;padding: 9px 0 0;}
#cityContainer-filter .ui-autocomplete {bottom: -121px !important;top: auto !important;width: 92%!important;    height: 168px !important;}    
#mobile_view_only{z-index:9}
.container .col-lg-3.col-md-3.col-sm-12.col-xs-12.mg_bm1{margin-bottom:0px !important;}

.first_bl, .second_bl{width:49%;}
.first_bl select, .second_bl select, .nav .slc_cty input, .third_bl a{width:100% !important;}
.nav .slc_cty{width:84%;}
.third_bl{width:13%;}
.last_bl{width:98%;margin:-16px 0 7px 0}
#post_btn{width:100% !Important;}
body{ padding:45px 0 0 0;}
#head{position:fixed; top:0px; left:0px; z-index:99999999}
#no-charge{display:none !important;}
.src1 {width: 14%;}
.src1 {border-radius:0 0 !important;}
}


</style>

<div class="page-content">
	<div id="landing_page_banner" class="page-content">
		<div class="white" style="display: none">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123" href="#"> Login </a></li>

						</ul>
					</div>
				</div>
				<div class="directory-info-row "
					style="width: 210px; margin: 30px auto 0; display: block">
					<ul class="social-links">
						<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
						<li><a href="#" title="Blog"><i class="fa fa-bold"></i> </a></li>
						<li><a href="#" title="Blog"><i class="fa fa-envelope"></i> </a></li>
					</ul>
				</div>
			</div>

			<div class="step form">
				<div class="omb_login">
					<h6>Login</h6>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="email" class="form-control" name="AllUser[Email]"
									required placeholder="Email address">
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control"
									name="AllUser[Password]" required placeholder="Password">
							</div>
							<!-- <span class="help-block">Password error</span>  -->

							<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
					</div>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-3">
							<p class="center-left">
								Not Yet Register ? <a
									href="<?= Url::toRoute(['site/register'])?>" class="color">
									Register Now </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-3">
							<p class="omb_forgotPwd">
								<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
									password?</a>
							</p>
						</div>
					</div>
					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<span class="omb_spanOr">or</span>
						</div>
					</div>
					<?php yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['wall/auth']]) ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>




<div class="cover profile" id="cover_photo_b">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
					<div class="profile_main" style="display: none !important">
						<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
					</div>
					<div class="name" style="display: none">
						<a href="#">My Career Bugs Wall</a>
					</div>
					<ul class="cover-nav" style="left: 0px; top: 0px; ">

					<!--	<li class="active" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i></span> <span id="fstatus"><?=$fs;?></span></a></li> -->


						<li style="display: none"><a
							href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>
					
						<?php if ($profile->UserTypeId == 0) {?> 
				     
						<li><a href="<?= Url::toRoute(['site/index']);?>">  Home  </a></li> 
				  <?php } ?>
					
					
						<?php if ($profile->UserTypeId == 0) {?> 
				     
						<li><a href="<?= Url::toRoute(['wall/candidateprofile']);?>">   My Profile  </a></li> 
				  <?php } ?>
				  
				 
						<?php if ($profile->UserTypeId == 3) {?> 
						<li><a href="<?= Url::base().'/'. str_replace(' ','',$profile->Name).'-'.$profile->UserId;?>"> My Profile
						</a></li>
							  <?php } ?>
						
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>">  My Activity</a></li>
								
							 
				 
				  
				  	<?php if ($profile->UserTypeId == 3) {?>  
						<li><a href="<?= Url::toRoute(['site/hirecandidate']);?>"> Hire Candidare</a></li>
				  <?php } ?>
				  
				  
				    	<?php if ($profile->UserTypeId == 0) {?> 
				     
						<li><a href="<?= Url::toRoute(['site/jobsearch']);?>">  Job Search</a></li> 
				  <?php } ?>
				   
				   
				  	<?php if ($profile->UserTypeId == 3) {?>  
						<li><a href="<?= Url::toRoute(['wall/companyprofile']);?>"> Wall Post </a></li>
					<?php } ?>	
					
					<?php if ($profile->UserTypeId == 5) {?>
						<li><a href="<?= Url::toRoute(['team/teamprofile'])?>"><i
									class="fa fa fa-user"></i> View Profile</a></li>
						<li><a href="<?= Url::toRoute(['team/editprofile'])?>"><i
									class="fa fa-pencil-square-o"></i> Edit Profile</a></li>
					<?php }?>
					
					
					
					
								
					</ul>
			 <!--	<div class="form-group right_main" id="no_dis_mbl1">
					 Insert code
					</div> -->
					
				</div>
			</div>
		</div>
	</div>
</div>




 <div class="form-group right_main" id="mobile_view_only"> 
 	<?php echo SearchPeople::widget()?> 
</div>  




</div>
<div class="container">
	<div class="row">
		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 mg_bm1">
			<div class="widget" id="static_block">
				<div class="widget-header">
					<h3 class="widget-caption">Number of Posts</h3>
				</div>
				<div class="section bordered-sky">
					<p style="display:none">
						<a href="<?= $user->EntryType == 'Company' ? '#' :  Url::toRoute(['wallfollower']);?>" style="cursor: <?= $user->EntryType == 'Company' ? 'default' : 'pointer' ?>"><span
							class="badge wallfollowcount"><?=$totalfollow;?></span> <i
							class="  fa fa-thumbs-up"></i> Followers</a>
					</p>
				<?php if ($profile->UserTypeId == 3) {?>
				 <p>
						<a href="#allcompanypost"><span class="badge"> <?=$postCount;?> </span>
							<i class=" fa fa-comments"></i> Posts</a>
					</p>
				  <?php } ?>
              </div>
			</div>

			<div class="widget" id="wall_description1" style="display: block">


				<div class="widget-body bordered-top bordered-sky">
				    <h2 class="filter_tab" style="display:none">Filter Job Option</h2>
				    <div class="button_click-1">
				          <span style="cursor: pointer;">	<img src="<?=$imageurl;?>careerimg/right-arrow.png"></span>
				    </div>
				    
               <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
		<div class="row main">
						<div class="xs-12 col-sm-12 main-center">
							<div class="form-group" style="margin-bottom: 7px;">
								<select name="CompanyWallPost[CIndustryId]" id="CIndustryId"
									style="width: 100%; float: none; margin-bottom: 7px;"
									class="form-control bfh-states">
									<option selected="selected" value="0">Select an Industry</option>
									<?php foreach($industry as $key=>$value){?>
									<option value="<?php echo $key;?>"><?=$value;?></option>
									<?php } ?>
								</select> 
					
								<select class="questions-category form-control"
									name="CompanyWallPost[PositionId]" id="PositionId"
									style="width: 100%; float: none; margin-bottom: 7px;"
									id="emppos">
									<option value="0">Select Role</option>
									<?php foreach ($roles as $key => $value) { ?>
											<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
									<?php } ?>
								</select>
								<div class="sts" style="width: 100%; display: block;" id="locat">
									<input id="citylist-filter" type="text" class="form-control"
										name="CompanyWallPost[Locationat]" placeholder="City">
									<div id="cityContainer-filter"
										style="background-color: white !important; margin-top: 14px;">
									</div>
								</div>



								<div class="button_block33">
									<div class="form-group" style="margin-bottom: 0">
										<input type="button" onclick="searchpost();" value="Submit"
											class="btn btn-default btn-green" /> <input type="button"
											onclick="searchallpost();" value="Show All Post"
											class="btn btn-default btn-green" />
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
						
			 <?php ActiveForm::end(); ?>
                </div>
			</div>






			<div class="widget" id="wall_contact">
				<div class="widget-header">
					<h3 class="widget-caption">Contact Us</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">
					<div class="row">
						<div class="col-xs-3">Email:</div>
						<div class="col-xs-9">info@mycareerbugs.com</div>
						<div class="col-xs-3">Phone:</div>
						<div class="col-xs-9">+ 91-8240369924</div>
						<div class="col-xs-3">Address:</div>
						<div class="col-xs-9">CF 318 Saltlake Sector 1</div>
						<div class="col-xs-3">URL:</div>
						<div class="col-xs-9">www.mycareerbugs.com</div>
					</div>

					<div class="row">
						<div class="directory-info-row col-xs-12">
							<ul class="social-links">
								<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>


			<div class="widget" id="wall_description">
				<div class="widget-header">
					<h3 class="widget-caption">Career quote</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">Choose a job you
					love, and you will never have to work a day in your life. —
					Confucius</div>
			</div>



			<div class="widget widget-friends" id="wall_followers" style="display:none">
				<div class="widget-header">
					<h3 class="widget-caption">Followers</h3>
				</div>
				<div class="widget-like company">
					<div class="row">
						<div class="col-md-12">
							<ul class="img-grid">
					<?php
    if ($totalfollowlist) {
        foreach ($totalfollowlist as $fk => $fvalue) {
            if ($fvalue->followBy->UserTypeId == 2 || $fvalue->followBy->UserTypeId == 5) {
                if ($fvalue->followBy->PhotoId != 0) {
                    $ll = $url . $fvalue->followBy->photo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            } else {
                if ($fvalue->followBy->LogoId != 0) {
                    $ll = $url . $fvalue->followBy->logo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            }
            if ($fvalue->followBy->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->followBy->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->followBy->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }
            ?>
						<li><a
									href="<?= Url::toRoute([$link,'userid'=>$fvalue->followBy->UserId])?>"><img
										src="<?=$ll;?>" alt="image" style="width: 43px; height: 43px;"><?=$fvalue->followBy->Name;?></a>
								</li>
					<?php
        }
    }
    ?>              
                  </ul>
				  <?php
    if ($totalfollow > 8) {
        ?>
					<a href="<?= Url::toRoute(['wallfollower']);?>"><span class="badge">See
									More</span></a>
					<?php
    }
    ?>
                </div>
					</div>
				</div>
			</div>


			<div class="widget" id="wall_advertisement" style="display:none">
				<div class="widget-header">
					<h3 class="widget-caption">Advertise</h3>
				</div>
				<div class="advertisement bordered-sky">
					<img src="https://mycareerbugs.com/images/adban_block/ban.gif"
						style="margin: 0; width: 100%">

					<!--<img src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img
						class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban.jpg"> -->
				</div>
			</div>
		</div>
		<div class="col-lg-6  col-md-6 col-sm-12 col-xs-12 padding-left">
			<div class="row">
				<!-- left posts-->
				<div class="col-md-12">
				<?php
    if ($type == 3) {
        ?>
				<div class="box profile-info">
            <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
                 <input style="padding: 10px; height: 40px;" type="text" id="post_title"
							class="form-control"
							name="CompanyWallPost[PostTitle]" required placeholder="Enter Post Title" /><br/>
				<textarea id="post_d"
							class="form-control input-lg p-text-area"
							name="CompanyWallPost[Post]" required rows="2"
							placeholder="Post your job here..."></textarea>
							
						<div class="box-footer">
							<ul class="nav nav-pills">
								<li class="first_bl"><select name="CompanyWallPost[CIndustryId]"
									required="required" class="form-control bfh-states">
										<option selected="selected" value="">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>"><?=$value;?></option>
						<?php } ?>
					</select></li>

								<li class="second_bl"><select class="questions-category form-control  "
									name="CompanyWallPost[PositionId]" required="required"
									id="emppos">
										<option value="">Select Role</option>
						<?php
        foreach ($roles as $key => $value) {
            ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
        }
        ?>
					</select></li>
								<li  class="slc_cty">

                                    <select class="questions-category form-control"
                                            id="naukari-location" name="CompanyWallPost[Locationat][]"
                                            multiple="multiple">
                                    </select>

<!--                                    <input id="citylist" type="text" class="form-control"-->
<!--                                           name="CompanyWallPost[Locationat]" placeholder="Select City">-->
							</li>
								<li class="third_bl"><a class="cameraa"><i class="fa fa-camera"></i><input
										type="file" name="CompanyWallPost[ImageId]"
										onchange="$(this).next().html($(this).val());"
										accept="image/*"><span></span></a></li>
								<!--<li  style="display:none"><a href="javascript:void(0)" class="cameraa"><i
										class=" fa fa-film"></i><input type="file"
										name="CompanyWallPost[VideoId]"
										onchange="$(this).next().html($(this).val());"
										accept="video/*"><span></span></a></li> -->
										
										<li class="last_bl"><button id="post_btn" type="submit"
								class="btn btn-info pull-right">Post</button></li>
							</ul>
							<div class="clear"></div>

							
							<div class="clear"></div>



						</div>
			


			<?php ActiveForm::end(); ?>
			</div>
				<?php
    }
    ?>
				
				
				
			<div class="widget-header">
						<h3 class="widget-caption">Job Update</h3>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div class="row">
						<div class="col-md-12" id="listview-post">
							<!-- post state form -->
                 <?php

                /*
                 * $postModel = CompanyWallPost::find()->where([
                 * 'IsDelete' => 0,
                 * 'IsHide' => 0,
                 * 'EmployerId' => Yii::$app->session['Employerid']
                 * ]);
                 */

                // Pjax::begin([
                // 'id' => 'allpost-pjax'
                // ]);
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => [
                        'class' => 'item'
                    ],
                    'itemView' => '_posts',
                    // 'summary' => '',
                    'emptyText' => 'No Posts Found',
                    'viewParams' => [
                        'empdp' => $empdp,
                        'empid' => $empid
                    ],
//                     'pager' => [
//                         'class' => LoadMore::class
//                     ],
                    // 'pager' => [
                    // 'class' => \kop\y2sp\ScrollPager::className(),
                    // 'paginationSelector' => '.list-view .pagination',
                    // 'triggerTemplate' => 'working'
                    // ]
                ]);
                // Pjax::end();

                ?>
            </div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 padding-left">
			<?php echo SidebarWidget::widget()?>
		</div>

	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalpostsearch" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Search Post</h4>
			</div>
		    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
		<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
					<br>
					<div class="form-group">
						<select name="CompanyWallPost[CIndustryId]" id="CIndustryId"
							style="width: 27%; float: left; margin-left: 10px;"
							class="form-control bfh-states">

							<option selected="selected" value="0">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $value->IndustryId;?>"><?=$value;?></option>
						<?php } ?>
					</select> <select class="questions-category form-control  "
							name="CompanyWallPost[PositionId]" id="PositionId"
							style="width: 20%; float: left; margin-left: 10px;" id="emppos">
							<option value="0">Select Role</option>
						<?php
    foreach ($roles as $key => $value) {
        ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
    }
    ?>
					</select>
						<div class="sts"
							style="width: 100%; padding-left: 315px; padding-right: 125px;">
			 
											
					<?php

    echo $form->field($wallpost, 'Locationat')
        ->widget(kartik\select2\Select2::classname(), [
        'data' => $cj,
        'language' => 'en',
        'options' => [
            'placeholder' => 'Select from list ...',
            'id' => 'walllocation'
        ]
    ])
        ->label(false);
    ?>
											
								</div>

					</div>
					<br> <br>
					<div class="clear"></div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="button" onclick="searchpost();" value="Submit"
								class="btn btn-default btn-green" /> <input type="button"
								onclick="searchallpost();" value="Show All Post"
								class="btn btn-default btn-green" />
						</div>
					</div>
				</div>
			</div>
		
			 <?php ActiveForm::end(); ?>
	  </div>
	</div>
</div>
<!-- Modal -->

<div class="modal fade" id="myModal_email" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Share Post</h4>
			</div>

			<div class="row main" style="padding:30px">
				<div class="xs-12 col-sm-12 main-center">
		<?php
			$form1 = ActiveForm::begin([
				'options' => [
					'id' => 'sendshareemail-share',
					'class' => 'row',
					'enctype' => 'multipart/form-data'
				]
			]);
			?>
		<div class="form-group">
			<input type="text" id="MailId" class="form-control"
							name="Mail[EmailId]" Placeholder="Enter E-Mail">
					</div>
					
		<?= $form1->field($mailmodel, 'Subject')->textInput(['value'=>'Post From My Career Bugs','maxlength' => true,'Placeholder'=>'Subject'])->label('Subject') ?>

					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green','id' => 'sendshareemail-share-submit']) ?>
								</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>
		</div>
	</div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">
function liketopost(postid)
{
	 $.ajax({
		 url : "<?= Url::toRoute(['wall/liketopost'])?>?postid="+postid,
		 success : function(res)
		 {
			if(res.status == 'OK'){
				$('#likedisliketext_'+postid).empty();
				$('#likedisliketext_'+postid).html(res.total_like+' likes');
				$('#likedislikebutton_'+postid).empty();
				$('#likedislikebutton_'+postid).html(res.button);
			}
		 }
	 });
}

function wallfollow(empid) {
    $.ajax({
        url:"<?= Url::toRoute(['wall/wallfollow'])?>?empid="+empid,
		success:function(result)
		{
			 var res=result;
			 res=res.split('|');
			 $('#fstatus').html(res[0]);
			 $('.wallfollowcount').html(res[1]);
		}
	 });
}
var postid=null;

function mailtoemp(id,post) {
	console.log('modal_open'+id+'post'+post);
	$('#'+id).modal('show');
	postid = post;
// 	tinyMCE.activeEditor.setContent($("#editbox"+post).find('textarea').text());
}


$(document).on('submit', '#sendshareemail-share', function(event) {
    event.preventDefault();
    var email = $('#MailId').val(),
    	wallpostid = postid;
    if(email.length==0){
    	return false;
    }
    var Subject = $('#mail-subject').val();
    var text = $('#mail-mailtext').text();
    $.ajax({
    	type: "POST",
    	url: '<?=Yii::$app->urlManager->baseUrl."/jobsharemail"?>',
    	data: {'email': email,'Subject':Subject,'text':text,'wallpostid': wallpostid},
    	success : function(response) {
    		$('#myModal_email').modal('hide');
    	}, 
    })
});

$(document).ready(function () {  
    $("#citylist").autocomplete({ 
    	source: "<?php echo Url::toRoute(['/'])?>"+'wall/getcities', 
    	minLength:1,
    	appendTo: "#cityContainer" 
    });  
});  
$(document).ready(function () {  
    $("#citylist-filter").autocomplete({ 
    	source: "<?php echo Url::toRoute(['/'])?>"+'wall/getcities', 
    	minLength:1,
    	appendTo: "#cityContainer-filter" 
    });
	
	$(document).on('click', '.click-post', function(){
		var commentId = $(this).attr('comment_id');
		$('.reply-comment-'+commentId).show();
	});
	
	$(document).on('click', '.submit-nested-comment', function(){
		var message = $(this).closest("form").find('input[type=text]').val();
		$('#qloader').show();
		var obj = $(this);
		var postid = $(this).attr('comment_id');
		$(this).closest("form").find('.comment-error').html('');
		if(message == ""){
			$(this).closest("form").find('.comment-error').html('Comment can not be blank');
		}else{
			var formData = $(this).closest("form").serialize();
			$.ajax({
			url : "<?=Yii::$app->urlManager->baseUrl."/wall/nestedcommenttopost"?>",
			type : 'POST',
			data : formData,
			dataType: 'JSON',
			success : function(response) {
				
				if(response['status'] == 'OK'){
					var html = '<div class="reply-msg boxcommentdel'+response['comment_id']+'"><div class="box-comment"><img class="img-circle img-sm" src="'+response['image']+'" alt="User Image"><div class="comment-text"><span class="pull-right" style="cursor: pointer;" onclick="delcomment('+response['comment_id']+');"><i class="fa fa-trash"></i></span><span class="text-muted pull-right">'+response['date']+'</span>'+response['message']+'</div></div></div>';
					$('#maincomment-'+postid).append(html);
					obj.closest("form").find('input[type=text]').val("");
					$('.reply-comment-'+postid).hide();
					$('#qloader').hide();
					var objDiv = document.getElementById("#PostComments" + postid);
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				$('#qloader').hide();
				
				
			}
		});
		}
		return false;
	});
	
}); 
function searchpost() {
    var walllocation = $('#citylist-filter').val();  
    console.log('Wall : '+ walllocation);
	var CIndustryId = $('#CIndustryId').val();
	var PositionId = $('#PositionId').val(); 
	$('.searcherror').html('');
    $.ajax({
        url:"<?= Url::toRoute(['wall/locationsearch'])?>?walllocation="+walllocation+'&CIndustryId='+CIndustryId+'&PositionId='+PositionId,
        dataType: 'HTML',
		success:function(result)
		{
//			$.pjax.reload({container: '#allpost-pjax', async: false});
 			  $('#listview-post').html(result);
// 			  $('#myModalpostsearch').modal('hide');
		}
	});
}

$(".filter_tab, .button_click-1").click(function(){
  $("#w2").toggle(300);
});




function searchallpost() {
	location.reload();
//     allpost();
//     $('#myModalpostsearch').modal('hide');
}


function watchList(postid){
	 $.ajax({
	 url : "<?= Url::toRoute(['wall/watchopost'])?>?postid="+postid,
		 success : function(res)
		 {
			if(res.status == 'OK'){
				//$('#watchtext_'+postid).empty();
				$('#watchtext_'+postid).html("Watched");
				$('#watchtext_'+postid).addClass("watchedClass");
			}
		 }
	 });
	 
}



$("#naukari-location").select2({
    maximumSelectionLength: 5,
    placeholder: "Select upto 5 Location",
    ajax: {
        url: '<?=Url::toRoute(['getpreferredlocation']);?>',
        dataType: 'json',
        type: "GET",
        data: function (params) {

            return {
                q: params.term,
                page: 1
            }
        },
        processResults: function(data){
            return {
                results: $.map(data.results, function (item) {

                    return {
                        text: item.CityName,
                        id: item.id
                    }
                })
            };
        }
    }
});


$('br').each(function () {
  const {nodeName} = this;

  let node = this;
  
  while (node = node.previousSibling) {
    if (node.nodeType !== Node.TEXT_NODE || node.nodeValue.trim() !== '') {
      break;
    };
  }
    
  if (node && node !== this && node.nodeName === nodeName) {
    $(node).remove();
  }
});

</script>