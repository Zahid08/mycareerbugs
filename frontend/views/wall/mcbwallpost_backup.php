<?php
$this->title = 'My Career Bugs Wall, MCB';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Likes;
use common\models\Follow;
use common\models\City;
use common\models\Industry;
use common\models\Position;
use kartik\select2\Select2;
$lik = new Likes();
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
use dosamigos\tinymce\TinyMce;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\CompanyWallPost;
use yii\widgets\Pjax;

$ctyloc = City::find()->all();

foreach ($ctyloc as $cty) {
    $che[] = $cty->CityName;
    $keys[] = $cty->CityName;
}
$cj = array_combine($che, $keys);

if (isset(Yii::$app->session['Employerid'])) {
    $empid = Yii::$app->session['Employerid'];
    $empdp = Yii::$app->session['EmployerDP'];
    $type = 3;
    $ppage = 'companyprofile';
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $empid = Yii::$app->session['Employeeid'];
    $empdp = Yii::$app->session['EmployeeDP'];
    $type = 2;
    $ppage = 'candidateprofile';
} elseif (Yii::$app->session['Campusid']) {
    $empid = Yii::$app->session['Campusid'];
    $empdp = Yii::$app->session['CampusDP'];
    $type = 4;
    $ppage = 'campusprofile';
} elseif (Yii::$app->session['Teamid']) {
    $empid = Yii::$app->session['Teamid'];
    $empdp = Yii::$app->session['TeamDP'];
    $type = 5;
    $ppage = 'teamprofile';
}

$follow = new Follow();
$isfollow = $follow->getIsfollow($empid);
$totalfollow = $follow->getTotalfollow($empid);
$totalfollowlist = $follow->getTotalfollowlist($empid);
if ($isfollow == 1) {
    $fs = 'Unfollow';
} else {
    $fs = 'Follow';
}
?>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css">
<script src="https://www.mycareerbugs.com/js/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js"></script>

<style>
.navbar {
	min-height: 41px !important;
}

.select2-selection.select2-selection--single {
	height: 34px !important;
}

#wall_description1 .widget {
	margin: 0;
	box-shadow: 0 0 0 #000;
}

#wall_description1 {
	position: fixed;
	z-index: 999;
	bottom: 0px;
	left: 0px;
	width: 100%;
	margin-bottom: 0px;
}

#wall_description1 select {
	width: 23% !important;
	margin: 0 1% !important;
	float: left !important;
}

#wall_description1 .sts {
	width: 23% !important;
	margin: 0 1% !important;
	float: left !important;
}

#wall_description1 .button_block33 {
	width: 23% !important;
	margin: 0 1% 0 0 !important;
	float: left !important;
}

#wall_description1 .widget-body {
	padding: 12px 12px 0px 12px;
}

.ui-autocomplete {
	width: 179.6px;
	display: block;
	background-color: white;
	margin-top: -19px;
}
</style>

<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
<!-- Begin page content -->
<div class="page-content">
	<div id="landing_page_banner" class="page-content">

		<div class="white" style="display: none">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123" href="#"> Login </a></li>

						</ul>
					</div>
				</div>
				<div class="directory-info-row "
					style="width: 210px; margin: 30px auto 0; display: block">
					<ul class="social-links">
						<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
						<li><a href="#" title="Blog"><i class="fa fa-bold"></i> </a></li>
						<li><a href="#" title="Blog"><i class="fa fa-envelope"></i> </a></li>
					</ul>
				</div>
			</div>

			<div class="step form">
				<div class="omb_login">

					<h6>
						Login
						<h6>

							<div class="row omb_row-sm-offset-3">
								<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="email" class="form-control" name="AllUser[Email]"
											required placeholder="Email address">
									</div>

									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control"
											name="AllUser[Password]" required placeholder="Password">
									</div>

									<!-- <span class="help-block">Password error</span>  -->

									<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
							</div>
							<div class="row omb_row-sm-offset-3">
								<div class="col-xs-12 col-sm-3">
									<p class="center-left">
										Not Yet Register ? <a
											href="<?= Url::toRoute(['site/register'])?>" class="color">
											Register Now </a>
									</p>
								</div>
								<div class="col-xs-12 col-sm-3">
									<p class="omb_forgotPwd">
										<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
											password?</a>
									</p>
								</div>
							</div>


							<div class="row omb_row-sm-offset-3 omb_loginOr">
								<div class="col-xs-12 col-sm-6">
									<span class="omb_spanOr">or</span>
								</div>
							</div>


							<!--<div class="row omb_row-sm-offset-3 omb_socialButtons">
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-facebook">
										<i class="fa fa-facebook visible-xs"></i>
										<span class="hidden-xs">Facebook</span>
									</a>
								</div>
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-twitter">
										<i class="fa fa-twitter visible-xs"></i>
										<span class="hidden-xs">Twitter</span>
									</a>
								</div>	
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-google">
										<i class="fa fa-google-plus visible-xs"></i>
										<span class="hidden-xs">Google+</span>
									</a>
								</div>	
							</div>-->
							<?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['wall/auth']]) ?>
				
				
				
				
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>


<!-- <div class="form-group right_main" id="mobile_view_only">
	<input class="form-control1" name="name" id="project"
		placeholder="Search for People, Company and Consultant" type="text"> <input
		type='hidden' id="termid" /> <input class="src1" id="searchit"
		value="Search" type="button">
</div> -->


<div class="cover profile" id="cover_photo_b">
	<div class="" style="display: none">
		<div class="image">
			<img src="<?=$imageurl;?>careerimg/background.jpg"
				class="show-in-modal" alt="people">
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
					<div class="profile_main" style="display: none">
						<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
					</div>
					<div class="name" style="display: none">
						<a href="#">My Career Bugs Wall</a>
					</div>
					<ul class="cover-nav" style="left: 0px; top: 0px;">

						<li class="active" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i> <span id="fstatus"><?=$fs;?></span></a></li>


						<li style="display: none"><a
							href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>
						<li><a href="<?= Url::toRoute([$ppage]);?>"> <i
								class="  fa fa-user"></i> My Profile
						</a></li>
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>"><i
								class="  fa fa-user"></i> My Activity</a></li>
					</ul>
					<div class="form-group right_main">
						<form
							action="<?= Url::toRoute(['/wall/search-company-index','userid' => $_GET['userid']])?>"
							method="get">
							<input class="form-control1" name="q" id="project"
								placeholder="Search for People, Company and Consultant"
								type="text"> <input type='hidden' id="termid" /> <input
								type="submit" class="src1">
						</form>

						<!-- <a href="< ?= Url::toRoute(['/wall/search-company-index','userid' => $_GET['userid']])?>"
							class="src1" id="searchit">Search</a> -->
					</div>
				</div>

			</div>

		</div>

	</div>

</div>
<div id="companySearchResult"
	style="z-index: 999; position: absolute;
	/* height: 800px; */ float: right; position: absolute; margin-left: 64%; background-color: aqua;">
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12">
			<div class="widget">
				<div class="widget-header">
					<h3 class="widget-caption">Statistics</h3>
				</div>
				<div class="section bordered-sky">

					<p>
						<a href="<?= $user->EntryType == Company ? '#' :  Url::toRoute(['wallfollower']);?>" style="cursor: <?= $user->EntryType == Company ? 'default' : 'pointer' ?>"><span
							class="badge wallfollowcount"><?=$totalfollow;?></span> <i
							class="  fa fa-thumbs-up"></i> Followers</a>
					</p>
				<?php
    if ($profile->UserTypeId == 3) {
        ?>
				 <p>
						<a href="#allcompanypost"><span class="badge"><?=count($allpost);?></span>
							<i class=" fa fa-comments"></i> Posts</a>
					</p>
				  <?php
    }
    ?>
              </div>
			</div>



			<div class="widget" id="wall_description" style="display: none">
				<div class="widget-header">
					<h3 class="widget-caption">Description</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">My Career Bugs,
					India's Job Portal Site provide you best opportunity for finding
					the right job based on your criteria. Our team believes that
					everyone is unique and we have tailored the amazing scientifically
					cutting-edge based approach.</div>
			</div>


			<div class="widget" id="wall_description1" style="display: block">


				<div class="widget-body bordered-top bordered-sky">
               <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
		<div class="row main">
						<div class="xs-12 col-sm-12 main-center">
							<div class="form-group" style="margin-bottom: 7px;">
								<select name="CompanyWallPost[CIndustryId]" id="CIndustryId"
									style="width: 100%; float: none; margin-bottom: 7px;"
									class="form-control bfh-states">

									<option selected="selected" value="0">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>"><?=$value;?></option>
						<?php } ?>
					</select> <select class="questions-category form-control  "
									name="CompanyWallPost[PositionId]" id="PositionId"
									style="width: 100%; float: none; margin-bottom: 7px;"
									id="emppos">
									<option value="0">Select Role</option>
						<?php
    foreach ($roles as $key => $value) {
        ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
    }
    ?>
					</select>
								<div class="sts" style="width: 100%; display: block;" id="locat">
			 
											
					<?php

    echo $form->field($wallpost, 'Locationat')
        ->widget(kartik\select2\Select2::classname(), [
        'data' => $cj,
        'language' => 'en',
        'options' => [
            'placeholder' => 'Select from list ...',
            'id' => 'walllocation'
        ]
    ])
        ->label(false);
    ?>
											
								</div>



								<div class="button_block33">
									<div class="form-group" style="margin-bottom: 0">
										<input type="button" onclick="searchpost();" value="Submit"
											class="btn btn-default btn-green" /> <input type="button"
											onclick="searchallpost();" value="Show All Post"
											class="btn btn-default btn-green" />
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
						
			 <?php ActiveForm::end(); ?>
                </div>
			</div>



			<div class="widget" id="wall_description">
				<div class="widget-header">
					<h3 class="widget-caption">Career quote</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">Choose a job you
					love, and you will never have to work a day in your life. —
					Confucius</div>
			</div>


			<div class="widget" id="wall_contact">
				<div class="widget-header">
					<h3 class="widget-caption">Contact Us</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">
					<div class="row">
						<div class="col-xs-3">Email:</div>
						<div class="col-xs-9">info@mycareerbug.com</div>
						<div class="col-xs-3">Phone:</div>
						<div class="col-xs-9">+ 91-9123876037</div>
						<div class="col-xs-3">Address:</div>
						<div class="col-xs-9">CF 318 Saltlake Sector 1</div>

						<div class="col-xs-3">URL:</div>
						<div class="col-xs-9">www.mycareerbug.com</div>



					</div>

					<div class="row">
						<div class="directory-info-row col-xs-12">
							<ul class="social-links">
								<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>




			<div class="widget widget-friends" id="wall_followers">

				<div class="widget-header">
					<h3 class="widget-caption">Followers</h3>
				</div>
				<div class="widget-like company">
					<div class="row">
						<div class="col-md-12">
							<ul class="img-grid">
					<?php
    if ($totalfollowlist) {
        foreach ($totalfollowlist as $fk => $fvalue) {
            if ($fvalue->followBy->UserTypeId == 2 || $fvalue->followBy->UserTypeId == 5) {
                if ($fvalue->followBy->PhotoId != 0) {
                    $ll = $url . $fvalue->followBy->photo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            } else {
                if ($fvalue->followBy->LogoId != 0) {
                    $ll = $url . $fvalue->followBy->logo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            }
            if ($fvalue->followBy->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->followBy->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->followBy->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }
            ?>
						<li><a
									href="<?= Url::toRoute([$link,'userid'=>$fvalue->followBy->UserId])?>"><img
										src="<?=$ll;?>" alt="image" style="width: 43px; height: 43px;"><?=$fvalue->followBy->Name;?></a>
								</li>
					<?php
        }
    }
    ?>              
                  </ul>
				  <?php
    if ($totalfollow > 8) {
        ?>
					<a href="<?= Url::toRoute(['wallfollower']);?>"><span class="badge">See
									More</span></a>
					<?php
    }
    ?>
                </div>
					</div>
				</div>
			</div>


			<div class="widget" id="wall_advertisement">
				<div class="widget-header">
					<h3 class="widget-caption">Advertise</h3>
				</div>
				<div class="advertisement bordered-sky">
					<img src="<?=$imageurl;?>careerimg/adban_block/ban.jpg"> <img
						class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg">
					<img src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img
						class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban.jpg">
				</div>
			</div>






		</div>








		<div class="col-lg-6  col-md-6 col-sm-12 col-xs-12 padding-left">

			<div class="row">

				<!-- left posts-->
				<div class="col-md-12">
				
				<?php
    if ($type == 3) {
        ?>
				<div class="box profile-info">
            <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
                <textarea id="post_d"
							class="form-control input-lg p-text-area"
							name="CompanyWallPost[Post]" required rows="2"
							placeholder="Post your job here..."></textarea>

						<div class="box-footer">

							<ul class="nav nav-pills">
								<li><select name="CompanyWallPost[CIndustryId]"
									required="required" class="form-control bfh-states">

										<option selected="selected" value="">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>"><?=$value;?></option>
						<?php } ?>
					</select></li>

								<li><select class="questions-category form-control  "
									name="CompanyWallPost[PositionId]" required="required"
									id="emppos">
										<option value="">Select Role</option>
						<?php
        foreach ($roles as $key => $value) {
            ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
        }
        ?>
					</select></li>

								<li><input id="citylist" type="text" class="form-control"
									name="CompanyWallPost[Locationat]" placeholder="City">
									<div id="cityContainer"
										style="background-color: white !important; margin-top: 28px;">
									</div> <!-- <input type="text" id="citylist"> --> <!-- <style> -->

									<!-- </style> --> <!-- <div id="cityContainer"> --> <!-- </div> -->
								</li>



								<!-- 								<li><select class="itemName form-control" -->
								<!-- 									placeholder="Select City" id="itemName" required="required" -->
								<!-- 									name="CompanyWallPost[Locationat]"> -->

								<!-- 										<option value="" disabled selected>Select your City</option> -->

								<!-- 					 < ?php -->
								<!--         foreach ($ctyloc as $key => $cty) { -->
								<!--             ?> -->
								<!-- 						 <option value="< ?=$cty->CityName;?>">< ?=$cty->CityName;?></option> -->
								<!-- 						< ?php -->
								<!--         } -->
								<!--         ?> -->
								<!-- 					</select></li> -->

								<li><a class="cameraa"><i class="fa fa-camera"></i><input
										type="file" name="CompanyWallPost[ImageId]"
										onchange="$(this).next().html($(this).val());"
										accept="image/*"><span></span></a></li>
								<li><a href="javascript:void(0)" class="cameraa"><i
										class=" fa fa-film"></i><input type="file"
										name="CompanyWallPost[VideoId]"
										onchange="$(this).next().html($(this).val());"
										accept="video/*"><span></span></a></li>
							</ul>
							<div class="clear"></div>

							<button id="post_btn" type="submit"
								class="btn btn-info pull-right">Post</button>
							<div class="clear"></div>



						</div>
			 <?php ActiveForm::end(); ?>
			</div>
				<?php
    }
    ?>
				
				
				
			<div class="widget-header">
						<h3 class="widget-caption">Job Update</h3>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!--	  
			  <a href="#" data-toggle="modal" data-target="#myModalpostsearch"><span class="item-location">
				<i class="fa fa-map-marker"></i> Select an Industry 
			  </span></a>
			  <a href="#" data-toggle="modal" data-target="#myModalpostsearch"><span class="item-location">
				<i class="fa fa-map-marker"></i> Select Role 
			  </span></a>
			  <a href="#" data-toggle="modal" data-target="#myModalpostsearch"><span class="item-location">
				<i class="fa fa-map-marker"></i> Select Location 
			  </span></a>
			  -->
					</div>


					<div class="row">
						<div class="col-md-12" id="allcompanypost">
							<!-- post state form -->
                 <?php

                /*
                 * $postModel = CompanyWallPost::find()->where([
                 * 'IsDelete' => 0,
                 * 'IsHide' => 0,
                 * 'EmployerId' => Yii::$app->session['Employerid']
                 * ]);
                 * $dataProvider = new ActiveDataProvider([
                 * 'query' => $postModel,
                 * 'pagination' => [
                 * 'pageSize' => 4
                 * ]
                 * ]);
                 * Pjax::begin();
                 * echo ListView::widget([
                 * 'dataProvider' => $dataProvider,
                 * 'itemOptions' => [
                 * 'class' => 'item'
                 * ],
                 * 'itemView' => '_posts',
                 * 'summary' => '',
                 * 'emptyText' => 'No Posts Found',
                 * 'viewParams' => [
                 * 'empdp' => $empdp
                 * ],
                 * 'pager' => [
                 * 'class' => \kop\y2sp\ScrollPager::className(),
                 * 'paginationSelector' => '.list-view .pagination',
                 * 'triggerTemplate' => 'working'
                 * ]
                 * ]);
                 * Pjax::end();
                 */

                ?>
				  <?php
    if ($allpost) {
        foreach ($allpost->each() as $postkey => $postvalue) {
            if ($postvalue->employer->LogoId != '') {
                $logo = $url . $postvalue->employer->logo->Doc;
            } else {
                $logo = $url . 'images/user.png';
            }
            $whatsapp_desc = 'Job opportunity at  ' . $postvalue->employer->Name . " for Location  " . $postvalue->Locationat . " for more detils visit " . Url::toRoute([
                'searchcompany',
                'userid' => $postvalue->EmployerId
            ]);
            ?>
				  <div class="box profile-info"
								id="editbox<?=$postvalue->WallPostId;?>" style="display: none;">
					<?php $form = ActiveForm::begin(['options' => ['class'=>'editpostform'.$postvalue->WallPostId,'enctype'=>'multipart/form-data']]); ?>
						<textarea class="form-control input-lg p-text-area"
									name="CompanyWallPost[Post]" required rows="2"
									placeholder="Whats in your mind today?"><?=$postvalue->Post;?></textarea>
								<input type="hidden" name="CompanyWallPost[WallPostId]"
									value="<?=$postvalue->WallPostId;?>" />
								<div class="box-footer">
									<ul id="delimg<?=$postvalue->WallPostId;?>">
						<?php
            if ($postvalue->ImageId != 0) {
                ?>
						<li><img src="<?=$url.$postvalue->wallimage->Doc;?>"
											style="width: 150px; height: 150px;" /></li>
										<li style="cursor: pointer;"
											onclick="delimg(<?=$postvalue->WallPostId;?>,'wall');">X</li>
						<?php
            }
            ?>
					</ul>
								</div>
								<div class="box-footer">
									<button type="submit" class="editpost btn btn-info pull-right"
										pid="<?=$postvalue->WallPostId;?>">Update Post</button>
									<ul class="nav nav-pills">
										<li><input type="text" id="pac-input"
											placeholder="Location At" class="form-control"
											name="CompanyWallPost[Locationat]"
											value="<?=$postvalue->Locationat;?>" style="display: none;" />
											<a href="#" onclick="$(this).prev().slideToggle();"><i
												class="fa fa-map-marker"></i></a></li>
										<li><a class="cameraa"><i class="fa fa-camera"></i><input
												type="file" name="CompanyWallPost[ImageId]"
												onchange="$(this).next().html($(this).val());"
												accept="image/*"><span></span></a></li>
										<li><a href="#" class="cameraa"><i class=" fa fa-film"></i><input
												type="file" name="CompanyWallPost[VideoId]"
												onchange="$(this).next().html($(this).val());"
												accept="video/*"><span></span></a></li>
									</ul>
								</div>
					 <?php ActiveForm::end(); ?>
					</div>

							<!--   posts -->
							<div class="box box-widget">
								<div class="box-header with-border">
									<div class="user-block">
										<img class="img-circle" src="<?=$logo;?>" alt="User Image"> <span
											class="username"><a
											href="<?= Url::toRoute(['searchcompany','userid'=>$postvalue->EmployerId])?>"><?=$postvalue->employer->Name;?></a></span>
						<?php $indusrt = Industry::findOne($postvalue->CIndustryId);  $postinid = Position::findOne($postvalue->PositionId);    ?>
						<!-- INDUS -->
										<span class="description extra_top_mrg">  <?= $indusrt->IndustryName;?> - <?= $postinid->Position;?>   -   <?=$postvalue->Locationat;?>     </span>
										<span class="description top_mr_abs"
											style="font-size: 10px; position: absolute; right: 9px; top: 41px;"> 
                         <?=date('h:i A D M',strtotime($postvalue->OnDate));?>    </span>
                   
						<?php
            if ($postvalue->EmployerId == $empid) {
                ?>
						 <span class="postright" onclick="$(this).next().toggle();"><i
											class="fa fa-angle-down"></i> </span>
										<div class="postbox">
											<ul>
												<li
													onclick="$('#editbox<?=$postvalue->WallPostId;?>').show();">Edit</li>
												<li onclick="deletepost(<?=$postvalue->WallPostId;?>);">Delete</li>
											</ul>
										</div>
						<?php
            }
            ?>
                      </div>
								</div>

								<div class="box-body" style="display: block;">
					  <?php
            if ($postvalue->wallimage) {
                ?>
						<img class="img-responsive pad show-in-modal"
										src="<?=$url.$postvalue->wallimage->Doc;?>" alt="Photo">
						<?php
            }
            if ($postvalue->wallvideo) {
                ?>
						 <video width="320" height="240" controls>
										<source src="<?=$url.$postvalue->wallvideo->Doc;?>"
											type="video/mp4">
									</video> 
						<?php
            }
            ?>
						<div class="comment more">
                      <?=str_replace("\n", '<br />',  $postvalue->Post);?>
                      </div>
					  <?php
            if ($postvalue->likes) {
                $lk = $postvalue->getlike($postvalue->WallPostId, $empid);
                if ($lk) {
                    if ($lk->IsLike == 1) {
                        ?>
					  <button type="button" class="btn btn-default btn-xs"
										onclick="liketopost(<?=$postvalue->WallPostId;?>);">
										<i class="fa fa-thumbs-o-down"></i> UnLike
									</button>
					  
					  <?php
                    } else {
                        ?>
						<button type="button" class="btn btn-default btn-xs"
										onclick="liketopost(<?=$postvalue->WallPostId;?>);">
										<i class="fa fa-thumbs-o-up"></i> Like
									</button>
						
						<?php
                    }
                } else {
                    ?>
						<button type="button" class="btn btn-default btn-xs"
										onclick="liketopost(<?=$postvalue->WallPostId;?>);">
										<i class="fa fa-thumbs-o-up"></i> Likes
									</button>
						<?php
                }
                ?>
					  
					  
                      <span class=" text-muted"><?=count($postvalue->likes);?> likes  </span>
					  <?php
            } else {
                ?>
					  <button type="button" class="btn btn-default btn-xs"
										onclick="liketopost(<?=$postvalue->WallPostId;?>);">
										<i class="fa fa-thumbs-o-up"></i> Like
									</button>
					  <?php
            }
            ?>
					  <a
										href="<?= Url::toRoute(['/wall/post-view','id' =>  $postvalue->WallPostId])?>">View</a>
									<div class="share" style="float: right; padding-top: 0px;">
										Share On <a
											href="https://www.facebook.com/sharer/sharer.php?u=<?= Url::toRoute(['searchcompany','userid'=>$postvalue->EmployerId])?>"
											target="_blank"> <i class="fa fa-facebook" aria-hidden="true"></i>
										</a>
										<!-- <a href="https://www.linkedin.com/shareArticle?mini=true&url=http://developer.linkedin.com&title=LinkedIn%20Developer%20Network&summary=My%20favorite%20developer%20program&source=LinkedIn" target="_blank">
						 <i class="fa fa-linkedin"></i>
					  </a> -->
										<a href="javascript:;"
											onclick="mailtoemp('myModal_email','<?=$postvalue->WallPostId;?>');"
											style="line-height: 32px;"><i class="fa fa-envelope-o"></i> </a>
										<a
											href="https://web.whatsapp.com/send?text=<?=strip_tags($whatsapp_desc);?>"
											target="_blank"><i class="fa fa-whatsapp"></i></a>

									</div>
								</div>





								<div id="PostComments<?=$postvalue->WallPostId;?>"
									class="postcomments">
								<?php

            echo Yii::$app->controller->renderPartial('_postComments', [
                'dataProvider' => $postvalue->getPostCommentData(),
                'empdp' => $empdp
            ])?>
					</div>

								<div class="box-footer" style="display: block;">
									<img class="img-responsive img-circle img-sm"
										src="<?=$empdp;?>" alt="Alt Text">
									<div class="img-push">
										<div class="form-group">
											<span class="input-icon inverted"> <input type="hidden"
												name="CommentToPost[PostId]"
												id="commentpostid<?=$postvalue->WallPostId;?>"
												value="<?=$postvalue->WallPostId;?>" /> <input type="hidden"
												name="CommentToPost[EmpId]"
												id="commentempid<?=$postvalue->WallPostId;?>"
												value="<?=$empid;?>" /> <input class="form-control input-lg"
												type="text" id="commentmessage<?=$postvalue->WallPostId;?>"
												name="CommentToPost[Message]" required>
												<p
													class="help-block help-block-error error<?=$postvalue->WallPostId;?>"
													style="color: #d73d32;"></p>
												<button type="button"
													onclick="Postcomment(<?=$postvalue->WallPostId;?>);"
													class="fa fa-envelope bg-palegreen"
													style="border: none; width: 46px;"></button>
											</span>

										</div>
									</div>

								</div>

							</div>
							<!--  end posts-->
                 <?php
        }
    }
    ?>
                </div>
					</div>
				</div>
				<!-- end left posts-->



			</div>
		</div>





		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 padding-left">
						<?php
    if (! empty($suggested)) {
        ?>
							  <div class="widget">
				<div class="widget-header">
					<h3 class="widget-caption">
						Suggested Candidate</span>
					</h3>
				</div>
				<div class="widget-know">
					<ul class=" ">
								  <?php

        foreach ($suggested as $sk => $sval) {
            if ($sval->user->IsDelete == 1)
                continue;

            if ($sval->user->PhotoId != 0) {
                $ph = $url . $sval->user->photo->Doc;
                // backend/web/imageupload/

                if (! file_exists($_SERVER['DOCUMENT_ROOT'] . '/backend/web/' . $sval->user->photo->Doc)) {
                    $ph = $imageurl . 'images/user.png';
                }
            } else {
                $ph = $imageurl . 'images/user.png';
            }
            ?>
										<li><a
							href="<?= Url::toRoute(['searchcandidate','userid'=>$sval->user->UserId])?>">
								<img src="<?=$ph;?>" alt="image"><?=$sval->user->Name;?>
										<span><?=$sval->user->EntryType;?></span>
						</a></li>
									<?php
        }
        ?>
								  </ul>

				</div>
			</div>
							  <?php
    }
    ?>
							  <?php
        if (! empty($recentjob)) {
            ?>
		                      <div class="widget">
				<div class="widget-header">
					<h3 class="widget-caption">Recent Job</h3>
				</div>
				<div class="recent-widget-know">
					<div class="rsw">
						<aside>
							<div class="widget_block">
								<ul class="related-post">
														<?php
            foreach ($recentjob as $jkey => $jvalue) {
                ?>
														<li><a
										href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>"> <?=$jvalue->JobTitle;?></a>
										<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?> </span>
										<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
										<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?> </span>
									</li>
													  <?php
            }
            ?>
														<a href="<?= Url::toRoute(['site/recentjob'])?>"
										class="sidebar_view_all"> View all </a>
								</ul>
							</div>
						</aside>
					</div>
				</div>
			</div>
							<?php
        }
        ?>
							
							
							<?php
    if (! empty($peopleyouknow)) {
        ?>
		                      <div class="widget">
				<div class="widget-header">
					<h3 class="widget-caption">People you know</h3>
				</div>
				<div class="widget-know">
					<ul class="img-grid">
										<?php
        foreach ($peopleyouknow as $pk => $pval) {
            if ($pval->PhotoId != 0) {
                $pimage = $url . $pval->photo->Doc;
            } else {
                $pimage = $imageurl . 'images/user.png';
            }
            ?>
										<li><a href=""> <img src="<?=$pimage;?>" alt="image"> <?=$pval->Name;?>
										<span>
										  <?php
            if ($pval->experiences) {
                $tc = count($pval->experiences);
                echo $pval->experiences[$tc - 1]->position->Position . ' at ' . $pval->experiences[$tc - 1]->CompanyName;
            }
            ?>
										</span></a></li>
										<?php
        }
        ?>
								  </ul>
				</div>
			</div>
							<?php
    }
    ?>
							
							
							<?php
    if (! empty($topcompany)) {
        ?>
							 <div class="widget">
				<div class="widget-header">
					<h3 class="widget-caption">Top Recruiter Company</h3>
				</div>
				<div class="widget-know">
					<ul class="img-grid">
										<?php
        foreach ($topcompany as $tk => $tval) {
            if ($tval->LogoId != 0) {
                $pimagel = $url . $tval->logo->Doc;
            } else {
                $pimagel = $imageurl . 'images/user.png';
            }
            ?>
										<li><a
							href="<?= Url::toRoute(['searchcompany','userid'=>$tval->UserId])?>">
								<img src="<?=$pimagel;?>" style="width: 50px; height: 50px;"
								alt="image"><?=$tval->Name;?>
										<span><?=count($lik->getTotalfollow($tval->UserId));?> Followers</span>
						</a></li>
										<?php
        }
        ?>
								  </ul>

				</div>
			</div>
							  <?php
    }
    ?>
		  </div>




	</div>
</div>



</div>
</div>
</div>




<!-- Modal -->
<div class="modal fade" id="myModalpostsearch" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Search Post</h4>
			</div>
		    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
		<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
					<br>
					<div class="form-group">
						<select name="CompanyWallPost[CIndustryId]" id="CIndustryId"
							style="width: 27%; float: left; margin-left: 10px;"
							class="form-control bfh-states">

							<option selected="selected" value="0">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>"><?=$value;?></option>
						<?php } ?>
					</select> <select class="questions-category form-control  "
							name="CompanyWallPost[PositionId]" id="PositionId"
							style="width: 20%; float: left; margin-left: 10px;" id="emppos">
							<option value="0">Select Role</option>
						<?php
    foreach ($roles as $key => $value) {
        ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
    }
    ?>
					</select>
						<div class="sts"
							style="width: 100%; padding-left: 315px; padding-right: 125px;">
			 
											
					<?php

    echo $form->field($wallpost, 'Locationat')
        ->widget(kartik\select2\Select2::classname(), [
        'data' => $cj,
        'language' => 'en',
        'options' => [
            'placeholder' => 'Select from list ...',
            'id' => 'walllocation'
        ]
    ])
        ->label(false);
    ?>
											
								</div>

					</div>
					<br> <br>
					<div class="clear"></div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="button" onclick="searchpost();" value="Submit"
								class="btn btn-default btn-green" /> <input type="button"
								onclick="searchallpost();" value="Show All Post"
								class="btn btn-default btn-green" />
						</div>
					</div>
				</div>
			</div>
		
			 <?php ActiveForm::end(); ?>
	  </div>
	</div>
</div>
<style>
a.morelink {
	text-decoration: none;
	outline: none;
}

.morecontent span {
	display: none;
}

.comment {
	margin: 10px;
}

.postcomments {
	width: 100%;
	max-height: 170px;
	overflow-y: scroll;
}

.postcomments_view_more {
	width: 100%;
	cursor: pointer;
}

.LessComments {
	display: none;
}

#ClickToSeeMoreComments {
	cursor: pointer;
}

#ClickToSeeLessComments {
	cursor: pointer;
}
</style>
<!-- Modal -->
<script type="text/javascript">
function liketopost(postid)
{
	 $.ajax({url:"<?= Url::toRoute(['wall/liketopost'])?>?postid="+postid,
			success:function(result)
			{
				 var res=JSON.parse(result);
				 if(res==1)
				 {
				 // location.reload();
				 allpost();
				 }
			}
	 });
}

function searchpost() {
    var walllocation = $('#walllocation').val();  
	var CIndustryId = $('#CIndustryId').val();
	var PositionId = $('#PositionId').val(); 
		$('.searcherror').html('');
        $.ajax({url:"<?= Url::toRoute(['wall/locationsearch'])?>?walllocation="+walllocation+'&CIndustryId='+CIndustryId+'&PositionId='+PositionId,
			success:function(result)
			{
				  $('#allcompanypost').html(result);
				  $('#myModalpostsearch').modal('hide');
			}
			});
   
}

function searchallpost() {
    allpost();$('#myModalpostsearch').modal('hide');
}

function wallfollow(empid) {
    $.ajax({url:"<?= Url::toRoute(['wall/wallfollow'])?>?empid="+empid,
			success:function(result)
			{
				 var res=result;
				 res=res.split('|');
				 $('#fstatus').html(res[0]);
				 $('.wallfollowcount').html(res[1]);
				 
			}
	 });
}

function MoreComments(id,wallid)
{ 

document.getElementById('ClickToSeeMoreComments'+id).style.display = "none";
document.getElementById('ClickToSeeLessComments'+id).classList.remove("LessComments"); 
    document.getElementById('PostComments').classList.remove("postcomments");
	document.getElementById('PostComments').classList.remove("postcomments_view_more");  
  
}
function LessComments(id,wallid)
{ 
	document.getElementById('ClickToSeeMoreComments'+id).style.display = "block";
	document.getElementById('ClickToSeeLessComments'+id).classList.add("LessComments"); 
    document.getElementById('PostComments').classList.remove("postcomments_view_more");
	document.getElementById('PostComments').classList.add("postcomments");
	 
}
var postid=null;

function mailtoemp(id,post) {
	console.log('modal_open'+id+'post'+post);
	$('#'+id).modal('show');
	postid = post;
// 	tinyMCE.activeEditor.setContent($("#editbox"+post).find('textarea').text());
}

$( document ).ready(function() {
	$(document).on('submit', '#sendshareemail-share', function(event) {
	
	event.preventDefault();
	var email = $('#MailId').val(),
		wallpostid = postid;
	if(email.length==0){
	return false;
	}
	var Subject = $('#mail-subject').val();
	var text = $('#mail-mailtext').text();
	$.ajax({
		type: "POST",
		url: '<?=Yii::$app->urlManager->baseUrl."/jobsharemail"?>',
		data: {'email': email,'Subject':Subject,'text':text,'wallpostid': wallpostid},
		success : function(response) {
			$('#myModal_email').modal('hide');
		}, 
	})
});
}); 
</script>
<div class="modal fade" id="myModal_email" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Share Post</h4>
			</div>

			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
		<?php

$form1 = ActiveForm::begin([
    'options' => [
        'id' => 'sendshareemail-share',
        'class' => 'row',
        'enctype' => 'multipart/form-data'
    ]
]);
?>
		<div class="form-group">
						<input type="text" id="MailId" class="form-control"
							name="Mail[EmailId]" Placeholder="Enter E-Mail">
					</div>
					
		<?= $form1->field($mailmodel, 'Subject')->textInput(['value'=>'Post From Mycareerbug','maxlength' => true,'Placeholder'=>'Subject'])->label('Subject') ?>
		

					<!-- <div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Job Description* </label>
										
								< ?=$form1->field($mailmodel, 'MailText')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small','language' => 'en_CA','clientOptions' => ['plugins' => ["advlist autolink lists link charmap print preview anchor","searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);?>
                              </div>

					</div> -->

					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green','id' => 'sendshareemail-share-submit']) ?>
								</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">  
        $(document).ready(function () {  
            $("#citylist").autocomplete({ 
            	source: "<?php echo Url::toRoute(['/'])?>"+'wall/getcities', 
            	minLength:1,
            	appendTo: "#cityContainer"  
//                 source: function (request, responce) {  
//                     $.ajax({  
//                         url: "< ?php echo Url::toRoute(['/'])?>"+'wall/getcities',  
//                         method: "post",  
//                         contentType: "application/json;charset=utf-8",  
//                         data: JSON.stringify({ term: request.term }),  
//                         dataType: 'json',  
//                         success: function (data) {  
//                             responce(data.d);  
//                         },  
//                         error: function (err) {  
//                             alert(err);  
//                         }  
//                     });  
//                 }  
            });  
        });  
    </script>
