<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

//Pjax::begin();
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => [
        'class' => 'item'
    ],
    'itemView' => '_posts',
    'summary' => '',
    'emptyText' => 'No Posts Found',
    'viewParams' => [
        'empdp' => $empdp
    ],
    'pager' => [
        'class' => \kop\y2sp\ScrollPager::className(),
        'paginationSelector' => '.list-view .pagination',
        'triggerTemplate' => 'working'
    ]
]);
//Pjax::end();
?>