<?php
$this->title = $wallpostData->PostTitle;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Likes;
use common\models\Follow;
use common\models\City;
use common\models\Industry;
use common\models\Position;
use kartik\select2\Select2;
$lik = new Likes();
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
use dosamigos\tinymce\TinyMce;
use yii\widgets\ListView;
use frontend\components\searchpeople\SearchPeople;
use frontend\components\sidebar\SidebarWidget;

if ($wallpostData->employer->LogoId != 0) {
    $ph = $url . $wallpostData->employer->logo->Doc;
} else {
    $ph = 'images/user.png';
}

$ctyloc = City::find()->all();

foreach ($ctyloc as $cty) {
    $che[] = $cty->CityName;
    $keys[] = $cty->CityName;
}
$cj = array_combine($che, $keys);

if (isset(Yii::$app->session['Employerid'])) {
    $empid = Yii::$app->session['Employerid'];
    $empdp = Yii::$app->session['EmployerDP'];
    $type = 3;
    $ppage = 'companyprofile';
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $empid = Yii::$app->session['Employeeid'];
    $empdp = Yii::$app->session['EmployeeDP'];
    $type = 2;
    $ppage = 'candidateprofile';
} elseif (Yii::$app->session['Campusid']) {
    $empid = Yii::$app->session['Campusid'];
    $empdp = Yii::$app->session['CampusDP'];
    $type = 4;
    $ppage = 'campusprofile';
} elseif (Yii::$app->session['Teamid']) {
    $empid = Yii::$app->session['Teamid'];
    $empdp = Yii::$app->session['TeamDP'];
    $type = 5;
    $ppage = 'teamprofile';
}

if(empty($empdp)){
	$empdp = $url . 'images/user.png';
}

$follow = new Follow();
$isfollow = $follow->getIsfollow($empid);
$totalfollow = $follow->getTotalfollow($empid);
$totalfollowlist = $follow->getTotalfollowlist($empid);
if ($isfollow == 1) {
    $fs = 'Unfollow';
} else {
    $fs = 'Follow';
}
?>
<script src="https://www.mycareerbugs.com/js/jquery.min.js"></script>
<style>
.comment {
    font-size: 12px;
	margin:0 10px 10px 10px;
}

.postcomments {
	width: 100%;
	max-height: 170px;
	overflow-y: scroll;
}
.navbar {
	min-height: 41px !important;
}

.select2-selection.select2-selection--single {
	height: 34px !important;
}
</style>

<style>
#login_system {
	position: fixed;
	bottom: 0px;
	width: 100%;
	z-index: 99999;
	background: url("/careerimg/rep.png") repeat;
	height: 100vh;
}


.white {
	width: 100%;
	min-height: 270px;
	top: 95%;
	position: absolute;
	left: 50%;
	margin: -225px 0 0 -50%;
	background: #fff;
	padding: 20px 20px 20px 20px;
	border-top: 2px solid #f16b22;
}

#login_system .step.form {
	padding: 20px 0
}

#login_system .step.form img {
	width: 250px;
	margin: 0 auto;
	display: block
}

.wall_logo_1 {
	margin-bottom: 10px !Important
}

#head {
	padding: 0px;
	border-bottom: 1px solid #f16b22
}

#landing_page_banner_button .head_right {
	width: 35%
}

.ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content {
	background-color: azure !important;
	margin-top: -19px !important;
}
</style>

<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
<!-- Begin page content -->

<?php 
if (isset(Yii::$app->session['Employerid'])) {
    $lg = Yii::$app->session['EmployerDP'];
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $lg = Yii::$app->session['EmployeeDP'];
} elseif (isset(Yii::$app->session['Campusid'])) {
    $lg = Yii::$app->session['CampusDP'];
} elseif (isset(Yii::$app->session['Teamid'])) {
    $lg = Yii::$app->session['TeamDP'];
}

?>

<?php if(!isset($lg)){ ?>

<div class="header-top" id="head">
	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<a class="logo_top" href=" "> <img class="wall_logo_1"
					src="<?=$imageurl;?>careerimg/logo.png" alt="MCB Logo"
					style="width: 100%">
				</a>
			</div>
			<div class="col-lg-8  col-md-8 col-sm-6 col-xs- 2 main-nav">
				<!-- Main Navigation -->

				<div class="head_right">
					<ul style="margin: 15px 0 0 0;">
						<li><a class="btn-123"
							href="https://www.mycareerbugs.com/site/employersregister">Employer
								Zone</a></li>
						<li class=""><strong> Are you recruiting?</strong> <a
							href="https://www.mycareerbugs.com/content/help"> <span>How we can
									help</span>
						</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->

			</div>

		</div>
	</div>
</div>

<div class="page-content">
	<div id="login_system">
		<div class="white">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123"
								href="<?= Url::toRoute(['site/login?wall='.$wallpostId])?>"> Login </a></li>

						</ul>
					</div>
				</div>

			</div>

			<div class="step form">
				<div class="omb_login">

					<h6>Login</h6>

					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="email" class="form-control" name="AllUser[Email]"
									required placeholder="Email address">
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control"
									name="AllUser[Password]" required placeholder="Password">
							</div>

							<!-- <span class="help-block">Password error</span>  -->

							<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
					</div>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-3">
							<p class="center-left">
								Not Yet Register ? <a
									href="<?= Url::toRoute(['site/register'])?>" class="color">
									Register Now </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-3">
							<p class="omb_forgotPwd">
								<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
									password?</a>
							</p>
						</div>
					</div>


					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<span class="omb_spanOr">or</span>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<?php } ?>



<div class="cover profile" id="cover_photo_b">
	<div class=" ">
		<div class="image">
			<img src="<?=$imageurl;?>careerimg/background1.jpg"
				class="show-in-modal" alt="MCB">
		</div>
	</div>

	<?php //echo '<pre>'; print_r($model);die;?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="cover-info">
					<div class="profile_main">
							<img src="<?=$ph;?>" alt="people" class="">
					</div>
					<div class="name">
						<a href="#"><?=$model->Name;?></a>
					</div>
					<ul class="cover-nav">
						<li class="active profile"><a href="<?= Url::base().'/'. str_replace(' ','',$model->Name).'-'.$model->UserId;?>"
							onclick="if(isMobile==true){$('#mobileleavecomment').hide();}else{$('#leavecomment').hide();}$(this).parent().toggleClass('active');$('.comm').removeClass('active');$('#quickupdate').show();"><i
								class="fa fa-user"></i> My Profile </a></li>
							 
								<?php if($empid == $model->UserId){ ?> 
								<li class=""><a href="<?= Url::toRoute(['site/yourpost']);?>"><i class="fa fa-eye"></i> View Job Post</a></li>
								<?php }else{?>
								<li class=""><a href="<?= Url::toRoute(['site/jobsearch','company'=>$model->UserId])?>"><i
								class="fa fa-eye"></i> View Job Post</a></li>
								<?php }?>

	                            <li class="">
								<?php if($empid == $model->UserId){ ?> 
								<a
							href="<?= Url::toRoute(['site/companyprofile']);?>"><i
								class="fa fa-eye"></i>  Wall Post</a>
								<?php }else{?>
								<a
							href="<?= Url::toRoute(['wall/searchcompany','userid' => $model->UserId])?>"><i
								class="fa fa-eye"></i>  Wall Post</a>
								
								<?php }?>
								
								</li>
								
					</ul> 

					<div class="form-group right_main">
						<?php echo SearchPeople::widget()?>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>




<div class="page-content">
	<div id="landing_page_banner" class="page-content">

		<div class="white" style="display: none">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123" href="#"> Login </a></li>

						</ul>
					</div>
				</div>
				<div class="directory-info-row "
					style="width: 210px; margin: 30px auto 0; display: block">
					<ul class="social-links">
						<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
						<li><a href="#" title="Blog"><i class="fa fa-bold"></i> </a></li>
						<li><a href="#" title="Blog"><i class="fa fa-envelope"></i> </a></li>
					</ul>
				</div>
			</div>

			<div class="step form">
				<div class="omb_login">

					<h6>Login</h6>

					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="email" class="form-control" name="AllUser[Email]"
									required placeholder="Email address">
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control"
									name="AllUser[Password]" required placeholder="Password">
							</div>

							<!-- <span class="help-block">Password error</span>  -->

							<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
					</div>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-3">
							<p class="center-left">
								Not Yet Register ? <a
									href="<?= Url::toRoute(['site/register'])?>" class="color">
									Register Now </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-3">
							<p class="omb_forgotPwd">
								<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
									password?</a>
							</p>
						</div>
					</div>


					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<span class="omb_spanOr">or</span>
						</div>
					</div>


					<!--<div class="row omb_row-sm-offset-3 omb_socialButtons">
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-facebook">
										<i class="fa fa-facebook visible-xs"></i>
										<span class="hidden-xs">Facebook</span>
									</a>
								</div>
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-twitter">
										<i class="fa fa-twitter visible-xs"></i>
										<span class="hidden-xs">Twitter</span>
									</a>
								</div>	
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-google">
										<i class="fa fa-google-plus visible-xs"></i>
										<span class="hidden-xs">Google+</span>
									</a>
								</div>	
							</div>-->
							<?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['wall/auth']]) ?>
					
				
				</div>

			</div>





			<div class="clear"></div>


		</div>
	</div>
</div>


<div class="form-group right_main" id="mobile_view_only">
	<input class="form-control1" name="name" id="project"
		placeholder="Search for People, Company and Consultant" type="text"> <input
		type='hidden' id="termid" /> <input class="src1" id="searchit"
		value="Search" type="button">
</div>


<div class="cover profile" id="cover_photo_b" style="display:none">
	<div class="" style="display: none">
		<div class="image">
			<img src="<?=$imageurl;?>careerimg/background.jpg"
				class="show-in-modal" alt="people">
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
					<div class="profile_main" style="display: none">
						<img src="<?=$imageurl;?>careerimg/logo.jpg" alt="Logo">
					</div>
					<div class="name" style="display: none">
						<a href="#">My Career Bugs Wall</a>
					</div>
					<ul class="cover-nav" style="left: 0px; top: 0px;">
						<li class="active" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i> <span id="fstatus"><?=$fs;?></span></a></li>
						<li style="display: none"><a
							href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>
						<li><a href="<?= Url::toRoute([$ppage]);?>"> <i
								class="  fa fa-user"></i> My Profile
						</a></li>
						<!--<li>  <a href="#" data-toggle="modal" data-target="#myModalpostsearch"> 
				<i class="fa fa-map-marker"></i> Select Location 
		 </a> </li>-->
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>"><i
								class="  fa fa-user"></i> My Activity</a></li>
					</ul>
					<div class="form-group right_main" id="no_display_on_mobile">
						<?php echo SearchPeople::widget()?>
					</div>
				</div>

			</div>
		</div>

	</div>

</div>



<div class="container">
	<div class="row">
		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12">
			<div class="widget">
				<div class="widget-header">
					<h3 class="widget-caption">Statistics</h3>
				</div>
				<div class="section bordered-sky">
					<p>
						<a href="<?= $user->EntryType == 'Company' ? '#' :  Url::toRoute(['wallfollower']);?>" style="cursor: <?= $user->EntryType == 'Company' ? 'default' : 'pointer' ?>"><span
							class="badge wallfollowcount"><?=$totalfollow;?></span> <i
							class="  fa fa-thumbs-up"></i> Followers</a>
					</p>
				<?php
    if ($profile->UserTypeId == 3) {
        if (!is_array($allpost)){
            $allpost = [
                $allpost
            ];
        }
        ?>
				 <p>
						<a href="#allcompanypost"><span class="badge"><?=count($allpost);?></span>
							<i class=" fa fa-comments"></i> Posts</a>
					</p>
				  <?php
    }
    ?>
              </div>
			</div>

			<div class="widget" id="wall_description">
				<div class="widget-header">
					<h3 class="widget-caption">Career quote</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">Choose a job you
					love, and you will never have to work a day in your life. —
					Confucius</div>
			</div>


			<div class="widget" id="wall_contact">
				<div class="widget-header">
					<h3 class="widget-caption">Contact Us</h3>
				</div>
				<div class="widget-body bordered-top bordered-sky">
					<div class="row">
						<div class="col-xs-3">Email:</div>
						<div class="col-xs-9">info@mycareerbug.com</div>
						<div class="col-xs-3">Phone:</div>
						<div class="col-xs-9">+ 91-9123876037</div>
						<div class="col-xs-3">Address:</div>
						<div class="col-xs-9">CF 318 Saltlake Sector 1</div>
						<div class="col-xs-3">URL:</div>
						<div class="col-xs-9">www.mycareerbug.com</div>
					</div>

					<div class="row">
						<div class="directory-info-row col-xs-12">
							<ul class="social-links">
								<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>




			<div class="widget widget-friends" id="wall_followers">

				<div class="widget-header">
					<h3 class="widget-caption">Followers</h3>
				</div>
				<div class="widget-like company">
					<div class="row">
						<div class="col-md-12">
							<ul class="img-grid">
					<?php
    if ($totalfollowlist) {
        foreach ($totalfollowlist as $fk => $fvalue) {
            if ($fvalue->followBy->UserTypeId == 2 || $fvalue->followBy->UserTypeId == 5) {
                if ($fvalue->followBy->PhotoId != 0) {
                    $ll = $url . $fvalue->followBy->photo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            } else {
                if ($fvalue->followBy->LogoId != 0) {
                    $ll = $url . $fvalue->followBy->logo->Doc;
                } else {
                    $ll = $imageurl . 'images/user.png';
                }
            }
            if ($fvalue->followBy->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->followBy->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->followBy->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }
            ?>
						<li><a
									href="<?= Url::toRoute([$link,'userid'=>$fvalue->followBy->UserId])?>"><img
										src="<?=$ll;?>" alt="image" style="width: 43px; height: 43px;"><?=$fvalue->followBy->Name;?></a>
								</li>
					<?php
        }
    }
    ?>              
                  </ul>
				  <?php
    if ($totalfollow > 8) {
        ?>
					<a href="<?= Url::toRoute(['wallfollower']);?>"><span class="badge">See
									More</span></a>
					<?php
    }
    ?>
                </div>
					</div>
				</div>
			</div>


			<div class="widget" id="wall_advertisement">
				<div class="widget-header">
					<h3 class="widget-caption">Advertise</h3>
				</div>
				<div class="advertisement bordered-sky">
					<img src="<?=$imageurl;?>careerimg/adban_block/ban.jpg"> <img
						class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg">
					<img src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img
						class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban.jpg">
				</div>
			</div>
		</div>
		<div class="col-lg-6  col-md-6 col-sm-12 col-xs-12 padding-left">
			<div class="row">
				<!-- left posts-->
				<div class="col-md-12">
					<div class="widget-header">
						<h3 class="widget-caption">Post</h3>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>


					<div class="row">
						<div class="col-md-12">
				  <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => [
            'class' => 'item'
        ],
        'itemView' => '_posts',
        // 'summary' => '',
        'emptyText' => 'No Posts Found',
        'viewParams' => [
            'empdp' => $empdp,
            'empid' => $empid
        ]
    ]);
    ?>
                </div>
					</div>
				</div>
				<!-- end left posts-->



			</div>
		</div>





		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 padding-left">
					<?php echo SidebarWidget::widget()?>	
		  </div>




	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalpostsearch" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Search Post</h4>
			</div>
		    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
		<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
					<br>
					<div class="form-group">
						<select name="CompanyWallPost[CIndustryId]" id="CIndustryId"
							style="width: 27%; float: left; margin-left: 10px;"
							class="form-control bfh-states">

							<option selected="selected" value="0">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>"><?=$value;?></option>
						<?php } ?>
					</select> <select class="questions-category form-control  "
							name="CompanyWallPost[PositionId]" id="PositionId"
							style="width: 20%; float: left; margin-left: 10px;" id="emppos">
							<option value="0">Select Role</option>
						<?php
    foreach ($roles as $key => $value) {
        ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
    }
    ?>
					</select>
						<div class="sts"
							style="width: 100%; padding-left: 315px; padding-right: 125px;">
			 
											
					<?php

    echo $form->field($wallpost, 'Locationat')
        ->widget(kartik\select2\Select2::classname(), [
        'data' => $cj,
        'language' => 'en',
        'options' => [
            'placeholder' => 'Select from list ...',
            'id' => 'walllocation'
        ]
    ])
        ->label(false);
    ?>
											
								</div>

					</div>
					<br> <br>
					<div class="clear"></div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<input type="button" onclick="searchpost();" value="Submit"
								class="btn btn-default btn-green" /> <input type="button"
								onclick="searchallpost();" value="Show All Post"
								class="btn btn-default btn-green" />
						</div>
					</div>
				</div>
			</div>
		
			 <?php ActiveForm::end(); ?>
	  </div>
	</div>
</div>
<style>
a.morelink {
	text-decoration: none;
	outline: none;
}


a.morelink{display:none;}

.comment {
	margin: 10px;
}

.postcomments {
	width: 100%;


	
}

.postcomments_view_more {
	width: 100%;
	cursor: pointer;
}



#ClickToSeeMoreComments {
	cursor: pointer;
}

#ClickToSeeLessComments {
	cursor: pointer;
}
</style>
<!-- Modal -->
<script type="text/javascript">
function liketopost(postid)
{
	 $.ajax({
		 url : "<?= Url::toRoute(['wall/liketopost'])?>?postid="+postid,
		 success : function(res)
		 {
			if(res.status == 'OK'){
				$('#likedisliketext_'+postid).empty();
				$('#likedisliketext_'+postid).html(res.total_like+' likes');
				$('#likedislikebutton_'+postid).empty();
				$('#likedislikebutton_'+postid).html(res.button);
			}
		 }
	 });
}

function searchpost() {
    var walllocation = $('#walllocation').val();  
	var CIndustryId = $('#CIndustryId').val();
	var PositionId = $('#PositionId').val(); 
	$('.searcherror').html('');
    $.ajax({
        url:"<?= Url::toRoute(['wall/locationsearch'])?>?walllocation="+walllocation+'&CIndustryId='+CIndustryId+'&PositionId='+PositionId,
        dataType: 'HTML',
		success:function(result)
		{
//			$.pjax.reload({container: '#allpost-pjax', async: false});
 			  $('#listview-post').html(result);
// 			  $('#myModalpostsearch').modal('hide');
		}
	});
}

function searchallpost() {
	location.reload();
//     allpost();
//     $('#myModalpostsearch').modal('hide');
}

function wallfollow(empid) {
    $.ajax({
        url:"<?= Url::toRoute(['wall/wallfollow'])?>?empid="+empid,
		success:function(result)
		{
			 var res=result;
			 res=res.split('|');
			 $('#fstatus').html(res[0]);
			 $('.wallfollowcount').html(res[1]);
		}
	 });
}
function mailtoemp(id,post) {
					$('#'+id).modal('show');
					tinyMCE.activeEditor.setContent($("#editbox"+post).find('textarea').text());
        		}

        		$( document ).ready(function() {
		 			$(document).on('submit', '#sendshareemail', function(event) {
		 			
					event.preventDefault();
					var email = $('#MailId').val();
					if(email.length==0){
					return false;
					}
					var Subject = $('#mail-subject').val();
					var text = $('#mail-mailtext').text();
					$.ajax({
            			type: "POST",
            			url: '<?=Yii::$app->urlManager->baseUrl."/jobsharemail"?>',
            			data: {'email': email,'Subject':Subject,'text':text},
            			success : function(response) {
            				$('#myModal_email').modal('hide');
        				}, 
        			})
				});
		 		}); 
				
$(document).ready(function(){
	$(document).on('click', '.click-post', function(){
		var commentId = $(this).attr('comment_id');
		$('.reply-comment-'+commentId).show();
	});
	
	$(document).on('click', '.submit-nested-comment', function(){
		var message = $(this).closest("form").find('input[type=text]').val();
		$('#qloader').show();
		var obj = $(this);
		var postid = $(this).attr('comment_id');
		$(this).closest("form").find('.comment-error').html('');
		if(message == ""){
			$(this).closest("form").find('.comment-error').html('Comment can not be blank');
		}else{
			var formData = $(this).closest("form").serialize();
			$.ajax({
			url : "<?=Yii::$app->urlManager->baseUrl."/wall/nestedcommenttopost"?>",
			type : 'POST',
			data : formData,
			dataType: 'JSON',
			success : function(response) {
				
				if(response['status'] == 'OK'){
					var html = '<div class="reply-msg boxcommentdel'+response['comment_id']+'"><div class="box-comment"><img class="img-circle img-sm" src="'+response['image']+'" alt="User Image"><div class="comment-text"><span class="pull-right" style="cursor: pointer;" onclick="delcomment('+response['comment_id']+');"><i class="fa fa-trash"></i></span><span class="text-muted pull-right">'+response['date']+'</span>'+response['message']+'</div></div></div>';
					$('#maincomment-'+postid).append(html);
					obj.closest("form").find('input[type=text]').val("");
					$('.reply-comment-'+postid).hide();
					$('#qloader').hide();
					var objDiv = document.getElementById("#PostComments" + postid);
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				$('#qloader').hide();
				
				
			}
		});
		}
		return false;
	});
	
});				
</script>
<div class="modal fade" id="myModal_email" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Share Post</h4>
			</div>

			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
		<?php $form1 = ActiveForm::begin(['options' => ['id'=>'sendshareemail','class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<div class="form-group">
						<input type="text" id="MailId" class="form-control"
							name="Mail[EmailId]" Placeholder="Enter E-Mail">
					</div>
					<div class="form-group">
		<?= $form1->field($mailmodel, 'Subject')->textInput(['value'=>'Post From Mycareerbug','maxlength' => true,'Placeholder'=>'Subject'])->label('Subject') ?>
		</div>

					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Job Description* </label>
										
								<?=$form1->field($mailmodel, 'MailText')->widget(TinyMce::className(), ['options' => ['rows' => 6],'class' => 'form-control textarea-small','language' => 'en_CA','clientOptions' => ['plugins' => ["advlist autolink lists link charmap print preview anchor","searchreplace visualblocks code fullscreen","insertdatetime media table  paste spellchecker"],'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"]])->label(false);?>
                              </div>

					</div>

					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green']) ?>
								</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>
		</div>
	</div>
</div>
