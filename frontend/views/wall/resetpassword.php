<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request Password Reset';
?>

<style>
  #wall_forget{padding:40px 0 0 0}
 </style>
	 
	 
    <!-- Begin page content -->
    <div id="landing_page_banner" class="page-content">
      
	  	<div class="white"> 
						 <div class="omb_login" id="wall_forget"> 
						
						<h6>Reset Password<h6>

						<div class="row omb_row-sm-offset-3">
							<div class="col-xs-12 col-sm-6">	
                                <?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm']]); ?>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="password" class="form-control"  name="AllUser[Password]" required id="pawd" placeholder="Password" autocomplete="off">
									</div>
								 <span class="help-block"></span>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="password" class="form-control"  name="AllUser[ConfirmPassword]" required id="cpawd"  placeholder="Confirm Password"  onblur="if($('#cpawd').val()!=''){if($('#cpawd').val()!=$('#pawd').val()){alert('Confirm password must be same with password field');$('#cpawd').val('').focus(); return false;}}"/>
									</div>
									<span class="help-block"></span>	
								 
									  <span class="help-block"> &nbsp  </span> 
									 <!-- <span class="help-block">Password error</span>  -->

									<button class="btn btn-lg btn-primary btn-block" type="submit">Reset</button>
								<?php ActiveForm::end(); ?>
							</div>
						</div>
						  
					</div> 
		 <div class="clear"></div> 
      </div>
    </div>