<?php
$this->title = 'Search - My Career Bugs Wall, MCB';

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\Likes;
use common\models\Follow;
use common\models\City;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use frontend\components\searchpeople\SearchPeople;
use frontend\components\sidebar\SidebarWidget;
    
use common\components\loadmore\LoadMore;

$lik = new Likes();
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';

$ctyloc = City::find()->all();

foreach ($ctyloc as $cty) {
    $che[] = $cty->CityName;
    $keys[] = $cty->CityName;
}
$cj = array_combine($che, $keys);

if (isset(Yii::$app->session['Employerid'])) {
    $empid = Yii::$app->session['Employerid'];
    $empdp = Yii::$app->session['EmployerDP'];
    $type = 3;
    $ppage = 'companyprofile';
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $empid = Yii::$app->session['Employeeid'];
    $empdp = Yii::$app->session['EmployeeDP'];
    $type = 2;
    $ppage = 'candidateprofile';
} elseif (Yii::$app->session['Campusid']) {
    $empid = Yii::$app->session['Campusid'];
    $empdp = Yii::$app->session['CampusDP'];
    $type = 4;
    $ppage = 'campusprofile';
} elseif (Yii::$app->session['Teamid']) {
    $empid = Yii::$app->session['Teamid'];
    $empdp = Yii::$app->session['TeamDP'];
    $type = 5;
    $ppage = 'teamprofile';
}

$follow = new Follow();
$isfollow = $follow->getIsfollow($empid);
$totalfollow = $follow->getTotalfollow($empid);
$totalfollowlist = $follow->getTotalfollowlist($empid);
$fs = 'Follow';
if ($isfollow == 1) {
    $fs = 'Unfollow';
}
?>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.css">
<script src="https://www.mycareerbugs.com/js/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.js"></script>

<style>
.navbar {	min-height: 41px !important;}
.select2-selection.select2-selection--single {	height: 34px !important;}
#wall_description1 .widget {	margin: 0;	box-shadow: 0 0 0 #000;}
#wall_description1 {	position: fixed;	z-index: 999;	bottom: 0px;	left: 0px;	width: 100%;	margin-bottom: 0px;}
#wall_description1 select {	width: 23% !important;	margin: 0 1% !important;	float: left !important;}
#wall_description1 .sts {	width: 23% !important;	margin: 0 1% !important;	float: left !important;}
#wall_description1 .button_block33 {	width: 23% !important;	margin: 0 1% 0 0 !important;	float: left !important;}
#wall_description1 .widget-body {	padding: 12px 12px 0px 12px;}
.ui-autocomplete {	width: 179.6px;	display: block;	background-color: white;	margin-top: -19px;}

.list-view .box-header.with-border{clear:both; overflow:hidden;}

.user-block .username small {background: #d0cbc9;color: #4c4845 !important;padding: 3px 8px 4px 8px;font-size: 11px !important;letter-spacing: 0.4px;}
.view_prf.viewdProfile{font-weight: bold;color: #fff !important;background-color: #6c146b !important;border: 1px solid #fff !important;position:relative;}
.view_prf.viewdProfile::before{content: "Viewed";position: Absolute;left: 0px;top: 0px;width: 100%;height: 33px;color: #fff !important;text-align: center;background-color: #6c146b !important;border: 1px solid #fff !important;line-height: 30px;}

 #w0-error-0{    background: #2ed418;
    color: #fff;
    border-color: #2ed418;
    line-height: 9px;}
 .alert-danger{background:  red !Important; color:#fff;    border-color:  red !Important;}
	.user-block a { color: #6d136a;font-size: 14px;}
	.search_main{padding:0; background:#fff}
	.description_heading{color:rgba(0,0,0,.7); margin-left: 12px; font-size:12px}
	.description_loc{ margin-left: 72px; font-size:12px;display:block;color: #999;}
	.search_main .username{margin-left:72px}
	.user-block .username small{ font-size:12px; color: #999; font-weight:normal}
	.search_main a.view_prf{font-weight:bold; color: #6c146b;background-color: #fff; border:1px solid #6c146b; display: inline-block; padding: 6px 12px;margin-bottom: 0;font-size: 12px; line-height: 1.42857143; position:absolute; right:15px;top:25px;}
	.search_main .img-circle{width:60px; height:60px}
	.search_main .box-header{padding:15px}
	
	
	 
	
	
#wall_description1 .widget {
	margin: 0;
	box-shadow: 0 0 0 #000;
}

#wall_description1 {
	position: fixed;
	z-index: 999;
	bottom: 0px;
	left: 0px;
	width: 100%;
	margin-bottom: 0px;
}



#wall_description1 .widget-body {
	padding: 12px 12px 0px 12px;
}

.ui-autocomplete {
	width: 179.6px;
	display: block;
	background-color: white;
	margin-top: -19px;
}
 #cover_photo_b{display:none;}

@media (max-width: 767px) {
    #cover_photo_b{display:block !important;}
    
#cover_photo_b .cover-info {
    background: #6c146b !important;
    clear: both;
    height: auto;}
    
    #cover_photo_b .cover-info .cover-nav li a{color:#fff !important;}

#cover_photo_b .cover-info .cover-nav {
    margin: 11px 0 0 0 !important;}
    
    .cover.profile{display:none;}
    
}




.cover.profile .cover-info .cover-nav{margin: 0 0 0 0 !important;}

 
</style>
  
  
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>-->
<!-- Begin page content -->
<div class="page-content">
	<div id="landing_page_banner" class="page-content">

		<div class="white" style="display: none">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123" href="#"> Login </a></li>

						</ul>
					</div>
				</div>
				<div class="directory-info-row "
					style="width: 210px; margin: 30px auto 0; display: block">
					<ul class="social-links">
						<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
						<li><a href="#" title="Blog"><i class="fa fa-bold"></i> </a></li>
						<li><a href="#" title="Blog"><i class="fa fa-envelope"></i> </a></li>
					</ul>
				</div>
			</div>

			<div class="step form">
				<div class="omb_login">

					<h6>
						Login
						<h6>

							<div class="row omb_row-sm-offset-3">
								<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="email" class="form-control" name="AllUser[Email]"
											required placeholder="Email address">
									</div>

									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input type="password" class="form-control"
											name="AllUser[Password]" required placeholder="Password">
									</div>

									<!-- <span class="help-block">Password error</span>  -->

									<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
							</div>
							<div class="row omb_row-sm-offset-3">
								<div class="col-xs-12 col-sm-3">
									<p class="center-left">
										Not Yet Register ? <a
											href="<?= Url::toRoute(['site/register'])?>" class="color">
											Register Now </a>
									</p>
								</div>
								<div class="col-xs-12 col-sm-3">
									<p class="omb_forgotPwd">
										<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
											password?</a>
									</p>
								</div>
							</div>


							<div class="row omb_row-sm-offset-3 omb_loginOr">
								<div class="col-xs-12 col-sm-6">
									<span class="omb_spanOr">or</span>
								</div>
							</div>


							<!--<div class="row omb_row-sm-offset-3 omb_socialButtons">
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-facebook">
										<i class="fa fa-facebook visible-xs"></i>
										<span class="hidden-xs">Facebook</span>
									</a>
								</div>
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-twitter">
										<i class="fa fa-twitter visible-xs"></i>
										<span class="hidden-xs">Twitter</span>
									</a>
								</div>	
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-google">
										<i class="fa fa-google-plus visible-xs"></i>
										<span class="hidden-xs">Google+</span>
									</a>
								</div>	
							</div>-->
							<?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['wall/auth']]) ?>
				
				
				
				
				
				
				
				
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>


<!-- <div class="form-group right_main" id="mobile_view_only">
	<input class="form-control1" name="name" id="project"
		placeholder="Search for People, Company and Consultant" type="text"> <input
		type='hidden' id="termid" /> <input class="src1" id="searchit"
		value="Search" type="button">
</div> -->



<?php

$useragent = $_SERVER['HTTP_USER_AGENT'];

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){ ?>

 <div class="form-group right_main" id="mobile_view_only">
 
 	<?php echo SearchPeople::widget()?>
			 
</div>  
<?php } ?>
 

 


<div class="cover profile" style="overflow: initial !important;">
<!--	<div class="" style="display: none">
		<div class="image">
			<img src="<?=$imageurl;?>careerimg/background.jpg"
				class="show-in-modal" alt="people">
		</div>
	</div>-->
	
	
	<div class="container">
	    
	 
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info">
			  
					<ul class="cover-nav" style="left: 0px; top: 0px;">

						<li class="active" style="display: none" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i> <span id="fstatus"><?=$fs;?></span></a></li>


						<li style="display: none"><a
							href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>
						<li><a href="<?= Url::toRoute([$ppage]);?>">  My Profile
						</a></li>
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>">  My Activity</a></li>
						
							<li><a href="<?= Url::toRoute(['wall/mcbwallpost']);?>"> Wall Post</a></li>
					</ul>

					<div class="form-group right_main"  id="no_dis_mbl1">
						<?php echo SearchPeople::widget()?>
						

						<!-- <a href="< ?= Url::toRoute(['/wall/search-company-index','userid' => $_GET['userid']])?>"
							class="src1" id="searchit">Search</a> -->
					</div>
				</div>

			</div>

		</div>

	</div>

</div>
<div id="companySearchResult"
	style="z-index: 999; position: absolute;
	/* height: 800px; */ float: right; position: absolute; margin-left: 64%; background-color: aqua;">
</div>
<div class="container">
	<div class="row">
	
	<?php 
	if($type==3){
	if($viewLimit>0){ ?>
	  <div id="w0-error-0" class="alert alert-info fade in">Basic Plan - Your search profile limit is:  10 / <?php echo $viewLimit; ?></div>  
 <?php	}else{ ?>
      <div id="w0-error-0" class="alert-danger alert fade in">Your basic plan is expired, Please contact <Span style="background:#fff; color:red;margin: 0 7px 0 3px;">+91 8240369924 </Span> to renew your plan</div>  
<?php }} ?>    
		<div class="col-lg-9  col-md-6 col-sm-12 col-xs-12 padding-left">
			<div class="widget-header">
				<h3 class="widget-caption">Search Results</h3>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

			</div>


			<div class="row">
				<div class="col-md-12">
				<div class="box box-widget">
								<div class="box-header with-border">
					<?php echo ListView::widget([
					    'dataProvider' => $searchResult,
					    'itemOptions' => ['class' => 'item'],
					    'itemView' => '_searchResult',
					    'viewParams' => ['oldJsonViewdData' => $oldJsonViewdData],
					   // 'summary' => '',
					    'emptyText' => 'No Results Found'
					]);?>
					</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end left posts-->
		<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 padding-left">
		<?php echo SidebarWidget::widget()?>
		</div>
	</div>
</div>

<?php /*
<div class="col-lg-3  col-md-3 col-sm-12 col-xs-12 padding-left">
	<?php if (! empty($suggested)) { ?>
							  <div class="widget">
		<div class="widget-header">
			<h3 class="widget-caption">
				Suggested Candidate</span>
			</h3>
		</div>
		<div class="widget-know">
			<ul class=" ">
								  <?php

    foreach ($suggested as $sk => $sval) {
        if ($sval->user->IsDelete == 1)
            continue;

        if ($sval->user->PhotoId != 0) {
            $ph = $url . $sval->user->photo->Doc;
            // backend/web/imageupload/

            if (! file_exists($_SERVER['DOCUMENT_ROOT'] . '/backend/web/' . $sval->user->photo->Doc)) {
                $ph = $imageurl . 'images/user.png';
            }
        } else {
            $ph = $imageurl . 'images/user.png';
        }
        ?>
										<li><a
					href="<?= Url::toRoute(['searchcandidate','userid'=>$sval->user->UserId])?>">
						<img src="<?=$ph;?>" alt="image"><?=$sval->user->Name;?>
										<span><?=$sval->user->EntryType;?></span>
				</a></li>
									<?php
    }
    ?>
								  </ul>

		</div>
	</div>
							  <?php
}
?>
							  <?php
        if (! empty($recentjob)) {
            ?>
		                      <div class="widget">
		<div class="widget-header">
			<h3 class="widget-caption">Recent Job</h3>
		</div>
		<div class="recent-widget-know">
			<div class="rsw">
				<aside>
					<div class="widget_block">
						<ul class="related-post">
														<?php
            foreach ($recentjob as $jkey => $jvalue) {
                ?>
														<li><a
								href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>"> <?=$jvalue->JobTitle;?></a>
								<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?> </span>
								<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
								<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?> </span>
							</li>
													  <?php
            }
            ?>
														<a href="<?= Url::toRoute(['site/recentjob'])?>"
								class="sidebar_view_all"> View all </a>
						</ul>
					</div>
				</aside>
			</div>
		</div>
	</div>
							<?php
        }
        ?>
							
							
							<?php
    if (! empty($peopleyouknow)) {
        ?>
		                      <div class="widget">
		<div class="widget-header">
			<h3 class="widget-caption">People you know</h3>
		</div>
		<div class="widget-know">
			<ul class="img-grid">
										<?php
        foreach ($peopleyouknow as $pk => $pval) {
            if ($pval->PhotoId != 0) {
                $pimage = $url . $pval->photo->Doc;
            } else {
                $pimage = $imageurl . 'images/user.png';
            }
            ?>
										<li><a href=""> <img src="<?=$pimage;?>" alt="image"> <?=$pval->Name;?>
										<span>
										  <?php
            if ($pval->experiences) {
                $tc = count($pval->experiences);
                echo $pval->experiences[$tc - 1]->position->Position . ' at ' . $pval->experiences[$tc - 1]->CompanyName;
            }
            ?>
										</span></a></li>
										<?php
        }
        ?>
								  </ul>
		</div>
	</div>
							<?php
    }
    ?>
							
							
							<?php
    if (! empty($topcompany)) {
        ?>
							 <div class="widget">
		<div class="widget-header">
			<h3 class="widget-caption">Top Recruiter Company</h3>
		</div>
		<div class="widget-know">
			<ul class="img-grid">
										<?php
        foreach ($topcompany as $tk => $tval) {
            if ($tval->LogoId != 0) {
                $pimagel = $url . $tval->logo->Doc;
            } else {
                $pimagel = $imageurl . 'images/user.png';
            }
            ?>
										<li><a
					href="<?= Url::toRoute(['searchcompany','userid'=>$tval->UserId])?>">
						<img src="<?=$pimagel;?>" style="width: 50px; height: 50px;"
						alt="image"><?=$tval->Name;?>
										<span><?=count($lik->getTotalfollow($tval->UserId));?> Followers</span>
				</a></li>
										<?php
        }
        ?>
								  </ul>

		</div>
	</div>
							  <?php
    }
    ?>
	</div> */?>
		  </div>
</div>

<script type="text/javascript">  
        $(document).ready(function () {  
            $("#citylist").autocomplete({ 
            	source: "<?php echo Url::toRoute(['/'])?>"+'wall/getcities', 
            	minLength:1,
            	appendTo: "#cityContainer"
            });  
        });  
    </script>
