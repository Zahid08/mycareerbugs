<?php
$this->title =$campus->Name;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Likes;
use common\models\LeaveComment;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
if($campus->LogoId!=0)
{
    $ph=$url.$campus->logo->Doc;
}
else
{
    $ph=$imageurl.'images/user.png';
}
?>
<!-- Begin page content -->
    <div class="page-content">
   
          <div class="cover profile">
            <div class=" ">
              <div class="image">
                <img src="<?=$imageurl;?>careerimg/background1.jpg" class="show-in-modal" alt="people">
              </div>  
            </div>
			
			
			 <div class="container">
				  <div class="row">
					<div class="col-md-12"> 
					
						<div class="cover-info">
						  <div class="profile_main">
							<img src="<?=$ph;?>" alt="Profile" class="">
						  </div>
						  <div class="name"><a href="#"> <?php echo $campus->Name; ?> (<?=$campus->City;?>)</a></div>
						  <ul class="cover-nav"> 
								<li  class="active profile"><a href="#" onclick="if(isMobile==true){$('#mobileleavecomment').hide();}else{$('#leavecomment').hide();}$(this).parent().toggleClass('active');$('.comm').removeClass('active');$('#quickupdate').show();"><i class="fa fa-user"></i> Profile Wall </a></li>
                                <?php
								if($empid!=$campus->UserId)
								{
									?>
                                <li  class="comm"><a href="#" onclick="if(isMobile==true){$('#mobileleavecomment').slideToggle();}else{$('#leavecomment').slideToggle();}$(this).parent().toggleClass('active');$('#quickupdate').toggle();$('.profile').toggleClass('active');"><i class="fa fa-envelope"></i> Leave a comment </a></li> <?php } ?> 								
								<li  class=""><a href="#"><i class="fa fa-eye"></i> View Comments  </a></li>  							
						  </ul> 
						  
						  <div class="form-group right_main"> 
							<input class="form-control1" name="name" id="project" placeholder="Search for People, Company and Consultant" type="text"> <input type='hidden' id="termid" />
							<input class="src1" id="searchit" value="Search" type="button"> 
                            </div>
							
						</div>
					  </div>
					</div> 
           </div> 
	     </div>
	  
	  
	   <div class="container">
		   <div class="widget-header">
              <h3 class="widget-caption">College  Description</h3>
            </div>
		   <div class="company_details12"> 
		                 <div class="profile_cont12">
						 <?=$campus->CompanyDesc;?>
						  </div>
		     </div>   
	   </div>
	  
	  
	
	     <div class="container">
		   <div class="widget-header">
              <h3 class="widget-caption">Dean / Director / Principal</h3>
            </div>
		   <div class="company_details12"> 
			        <div class="col-img">
		                 <div class="profile_main">
							  <?php
									if($campus->PhotoId!=0)
									{
										$campusphoto=$url.$campus->photo->Doc;
									?>
							<img src="<?=$url.$campus->photo->Doc;?>" alt="people" class="">
							<?php
							}
							?>
						  </div>
	                </div> 
					<div class="col-cont">
		                 <div class="profile_cont12">
						     <?=$campus->AboutYou;?>
						  </div>
	                </div>
		     </div>   
	   </div>
	  
	  
	  
	  
	  
	  
    <!-- Begin page content -->
 <div class="page-content like_pae">
    <div class="container">
      <div class="row">
	 <div class="col-md-3 col-xs-12" id="collage_left">
	    
	    
	    <div id="mobileleavecomment" style="display: none;">
				<div class="widget-header">
					<h3 class="widget-caption"> Leave Comment</h3>
				  </div>
				 <div class="widget">	
							<div class="resume-box">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcampus','userid'=>$campus->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
							</div>
				 </div>
				 
				 
				 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Comments</span></h3>
							</div>
			<div class="widget-comments">
              <div class="row">
                <div class="col-md-12">
                  <?php
				$lv=new LeaveComment();
				$allcomment=$lv->getComment($campus->UserId);
				if($allcomment)
				{
					foreach($allcomment as $ck=>$cval)
					{
						if($cval->commentFrom->UserTypeId==2 || $cval->commentFrom->UserTypeId==5)
						{
							$ll=($cval->commentFrom->PhotoId!=0)?$url.$cval->commentFrom->photo->Doc:$imageurl.'images/user.png';
						}
						elseif($cval->commentFrom->UserTypeId==3 || $cval->commentFrom->UserTypeId==4)
						{
							$ll=($cval->commentFrom->LogoId!=0)?$url.$cval->commentFrom->logo->Doc:$imageurl.'images/user.png';
						}
						
				?>
                  <ul class=" ">
						<li> <a href=""> <img src="<?=$ll;?>" alt="image">   Career Bugs</a> </li>
						<li class="comments-block"> 
		                  <p><?=$cval->Comment;?></p>
						  </li>              
                  </ul>
				<?php
					}
				}
				?>
                </div>
              </div>
            </div>
						  </div>	
			</div>
	    
	    
        
        <div class="widget no_shadow"> 
				<div class="action-buttons">
				  <div class="row">
				 <div class="col-md-12" id="followbox">
                    <?php
                    $lik=new Likes();
                    $isfollow=$lik->getIsfollow($empid,$campus->UserId);
                    if(empty($isfollow))
                    {
                      if($empid!=$campus->UserId)
                      {
                    ?>
                      <div class="half-block-left" onclick="follow(<?=$campus->UserId;?>,<?=$empid;?>);">
                            <a  class="btn btn-azure btn-block"><i class="fa fa-user-plus"></i> Follow</a>
                        </div>
                      <?php
                      }
                    }
                    else
                    {
                  ?>
                      <div class="half-block-left" onclick="unfollow(<?=$campus->UserId;?>,<?=$empid;?>);">
                            <a href="#" class="btn btn-azure btn-block"><i class="fa fa-user-plus"></i> UnFollow</a>
                      </div>
                        <?php
                    }
                    ?>
                  <?php
								if($empid!=$campus->UserId)
								{
									?>
                  <div class="half-block-right">
                      <a href="#" data-toggle="modal" data-target="#myModalmessage" class="btn btn-azure btn-block"><i class="fa fa-envelope"></i> Message</a>
                  </div>
				  <?php
								}
								?>
                </div>
               </div> 
				</div>  			   
			 </div>
        
		<div class="widget">
		   <div class="widget-header">
              <h3 class="widget-caption">Statistics</h3>
            </div> 
		     <div class="section bordered-sky">  
					<p><a href="<?= Url::toRoute(['follower','userid'=>$campus->UserId]);?>"><span class="badge" id="followcount"><?=count($lik->getTotalfollow($campus->UserId));?></span>   <i class="  fa fa-thumbs-up"></i> Followers</a></p>
					<p><span class="badge"><?=$totalactive;?></span>   <i class=" fa fa-comments"></i> Active Jobs</p> 
              </div> 
          </div>
		  
		  
		  
			<div class="widget" style="display: none;">
                <div class="widget-header">
                  <h3 class="widget-caption">Description</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  Career Bugs, India's  Job Site provides you the best opportunity to find the right job today. We provide information about Freshers Jobs, Latest Walkins, Latest Off Campus Drives For Freshers, Bank Jobs.
                </div>
            </div>

		  <div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">Basic Skill</h3>
                </div>
                <div class="basic_skill">
               <?php
				  if($skills)
				  {
				  foreach($skills as $sk=>$sval)
				  {
				  ?>
				  <p><?=$sval;?></p>
				  <?php
				  }
				  }
				  ?>
                </div>
              </div>
		   
			  
		  <div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">  Social Network</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  
						
						<div class="row">
                          <div class="directory-info-row col-xs-12">	
	          	              <ul class="social-links">
                                  <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
                              </ul>      
						</div>
                     </div>
                </div>
              </div>

			  
		  
          
 
 
						  
          <div class="widget widget-friends">
           
            <div class="widget-header">
              <h3 class="widget-caption">College Photos</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
				  <?php
				if($campus->collegepics)
				{
				  foreach($campus->collegepics as $key=>$value)
									{
					?>
					<li> <a href=""> <img src="<?=$url.$value->pic->Doc;?>" style="height: 60px;" alt="image">    </a> </li>
				  <?php
									}
				}
				?>
                  </ul>
                </div>
              </div>
            </div>
          </div>		 
							
 
       <div class="widget widget-friends"> 
            <div class="widget-header">
              <h3 class="widget-caption">Videos</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
					         
                  </ul>
                </div>
              </div>
            </div>
          </div>
 
			  
        </div>
		
		
		
		
		
		
		
		 <div class="col-md-6 col-xs-12  padding-left"  id="collage_middle">  
          <div class="row">
               <!-- left posts-->
            <div class="col-md-12">
				
		<div id="leavecomment" style="display: none;">
				<div class="widget-header">
					<h3 class="widget-caption"> Leave Comment</h3>
				  </div>
				 <div class="widget">	
							<div class="resume-box">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcampus','userid'=>$campus->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
							</div>
				 </div>
				 
				 
				 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Comments</span></h3>
							</div>
			<div class="widget-comments">
              <div class="row">
                <div class="col-md-12">
                  <?php
				$lv=new LeaveComment();
				$allcomment=$lv->getComment($campus->UserId);
				if($allcomment)
				{
					foreach($allcomment as $ck=>$cval)
					{
						if($cval->commentFrom->UserTypeId==2 || $cval->commentFrom->UserTypeId==5)
						{
							$ll=($cval->commentFrom->PhotoId!=0)?$url.$cval->commentFrom->photo->Doc:$imageurl.'images/user.png';
						}
						elseif($cval->commentFrom->UserTypeId==3 || $cval->commentFrom->UserTypeId==4)
						{
							$ll=($cval->commentFrom->LogoId!=0)?$url.$cval->commentFrom->logo->Doc:$imageurl.'images/user.png';
						}
						if($cval->commentFrom->UserTypeId==2){$link='searchcandidate';}elseif($cval->commentFrom->UserTypeId==3){$link='searchcompany';}elseif($cval->commentFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
				?>
                  <ul class=" ">
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"><?=$cval->commentFrom->Name;?></a> </li>
						<li class="comments-block"> 
		                  <p><?=$cval->Comment;?></p>
						  </li>              
                  </ul>
				<?php
					}
				}
				?>
                </div>
              </div>
            </div>
						  </div>	
			</div>
				
				
                    
				<div id="quickupdate">
                    <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Followers</span></h3>
							</div>
			
			<div class="widget-like">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
                    <?php
                    $followers=$lik->getTotalfollow($campus->UserId);
                    if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeFrom->UserTypeId==2 || $fvalue->likeFrom->UserTypeId==5)
							  {
									if($fvalue->likeFrom->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeFrom->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeFrom->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeFrom->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
                     if($fvalue->likeFrom->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeFrom->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a> </li>
					<?php
                        }
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>       
						  </div>
				
					</div>
			
			   </div> 
			 </div>
        </div>
		
		
		 
		<div class="col-md-3 col-xs-12 padding-left">
		
		
		                      <div class="widget">	  
							   <div class="widget-header">
							     <h3 class="widget-caption">  Campus Recent Job by company</h3>
							   </div>
		                       <div class="recent-widget-know"> 
								      <div class="rsw"> 
											<aside> 
												<div class="widget_block"> 
													<ul class="related-post">
														<?php
													  if($recentjob)
													  {
													  foreach($recentjob as $jkey=>$jvalue)
													  {
													  ?>
														<li>
															<a href="<?= Url::toRoute(['campus/jobdetail','JobId'=>$jvalue->JobId])?>"> <?=$jvalue->JobTitle;?></a>
															<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?> </span>
															<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
															<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?> </span>
														</li>
													  <?php
													  }
													  }
													  ?>
													</ul>
												</div> 
											</aside>
                                  </div>
							  </div> 
							</div>
							
							 
							
							
							 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Top Recruiter Company</h3>
							</div>
		                  <div class="widget-know"> 
								  <ul class=" ">
										<?php
										  if($topcompany)
										  {
												foreach($topcompany as $tk=>$tval)
												{
												if($tval->LogoId!=0)
												{
													$pimagel=$url.$tval->logo->Doc;
												}
												else
												{
													$pimagel=$imageurl.'images/user.png';
												}  
										  ?>
										<li> <a href="<?= Url::toRoute(['searchcompany','userid'=>$tval->UserId])?>"> <img src="<?=$pimagel;?>" style="width: 50px;height: 50px;" alt="image"><?=$tval->Name;?></a>
										<span><a href="<?= Url::toRoute(['follower','userid'=>$tval->UserId]);?>"><?=count($lik->getTotalfollow($tval->UserId));?> Followers</span></a> </li>
										<?php
										  }
										  }
										  ?>
								  </ul> 
								  
							</div> 
							</div>
			
		  </div>
		
		
         
      </div>
    </div>
 </div>
     </div>
	
	
	 	 		 <!-- Modal -->
<div class="modal fade" id="myModalcomment" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Leave Comment</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcampus','userid'=>$campus->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
		
	  </div>  
	  </div>
</div>
			<!-- Modal -->
			
<!-- Modal -->
<div class="modal fade" id="myModalmessage" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcampus','userid'=>$campus->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
		
	  </div>  
	  </div>
</div>
			<!-- Modal -->	
<script type="text/javascript">
function liketopost(postid)
{
	 $.ajax({url:"<?= Url::toRoute(['wall/liketopost'])?>?postid="+postid,
			success:function(result)
			{
				 var res=JSON.parse(result);
				 if(res==1)
				 {
				  location.reload();
				 }
			}
	 });
}
</script>