<?php
$this->title = $profile->Name;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\AllUser;
use common\models\Likes;
use common\models\LeaveComment;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;  
use common\components\loadmore\LoadMore;

use frontend\components\sidebar\SidebarWidget;
use frontend\components\searchpeople\SearchPeople;
use common\models\Skill;
use common\models\EmployeeSkill;
use common\models\Documents;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;
$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
 
$url = '/backend/web/';
if ($profile->PhotoId != 0) {
    $ph = $url . $profile->photo->Doc;
} else {
    $ph = $url . 'images/user.png';
}
?><?php

$alusr = AllUser::findOne($empid);
?>

<style>
body {
	background: #e9eaed;
}

.navbar-fixed-top, .navbar-fixed-bottom {position: relative !important;margin: 0;}
.navbar {height: 40px}
.cover.profile .cover-info .cover-nav {left: 0px;}

#profile_menu_mobile{display:none;}

#cover_photo_b{display:none;}

@media (max-width: 767px) {
    
    
   
.education-box {
    border-bottom: 1px solid #ccc;
    margin-bottom: 20px;
    padding: 0 0 20px;
}


    #mobile_mnu li{width:50%;}
      #mobile_mnu{position:relative; z-index:999;}
    #cover_photo_b{display:block;}
.cover.profile{background:none !important;}
#cover_photo_b{display:block;}

#cover_photo_b .cover-info { background: #6c146b !important; clear: both;height: auto;}
   
#cover_photo_b .cover-info .cover-nav li a{color:#fff !important;}
#cover_photo_b .cover-info .cover-nav { margin: 11px 0 0 0 !important;}

#profile_menu_desktop{display:none;}    
  #profile_menu_mobile{display:block;}  
}


</style>
<!-- Begin page content -->
<div class="page-content">
    
<div class="cover profile" id="cover_photo_b">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cover-info"> 
					<ul class="cover-nav" style="left: 0px; top: 0px; ">

					<!--	<li class="active" onclick="wallfollow(<?=$empid;?>);"><a
							href="javascript:void(0)"><span class="badge"><i
									class="fa fa-user-plus"></i></span> <span id="fstatus"><?=$fs;?></span></a></li> -->


						<li style="display: none"><a
							href="<?= Url::toRoute(['wallfollower']);?>"><span
								class="badge wallfollowcount"><?=$totalfollow;?></span> <i
								class="  fa fa-thumbs-up"></i> Followers</a></li>
					
					
						<li><a href="#"> <i
								class="  fa fa-user"></i> My Profile
						</a></li>
						<li><a href="<?= Url::toRoute(['site/myactivity']);?>"><i
								class="  fa fa-user"></i> My Activity</a></li>
								
						 		<li><a href="<?= Url::toRoute(['site/jobsearch']);?>"><i
								class="  fa fa-user"></i> Job Search</a></li>
								
									<li><a href="<?= Url::toRoute(['wall/mcbwallpost']);?>"><i
								class="  fa fa-user"></i> Wall Post </a></li>
								
								
					</ul>
				 
				</div>
			</div>
		</div>
	</div>
</div>

    
    
    
    
    
    
    
    
    
              	<div class="form-group right_main" id="mobile_view_only">
							<?php echo SearchPeople::widget()?>
						</div>
    
    
    
    
    
    
    
    
    
	<div class="cover profile" style="overflow: initial !important;">


		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="cover-info">

					<div class="profile_main" style="width: 110px !important; margin: 0 auto ;"  id="mobile_view_only">
						<img src="<?=$ph;?>" alt="profile Photo" class=""
							style="width: 110px !important; height: 110px; border-radius: 200px">
					</div>



						<ul class="cover-nav" id="mobile_mnu">
							<li class="active profile"><a href="#descritpion"
								onclick="if(isMobile==true){$('#mobileleavecomment').hide();}else{$('#leavecomment').hide();}$(this).parent().toggleClass('active');$('.comm').removeClass('active');$('#quickupdate').show();">
								  About                         <?=$profile->Name;?>                        </a></li>
						<?php
							if ($empid != $profile->UserId && isset(Yii::$app->session['Employerid'])) {?>                     
							<li class="comm" style=""><a href="#"
								onclick="if(isMobile==true){$('#mobileleavecomment').slideToggle();}else{$('#leavecomment').slideToggle();}$(this).parent().toggleClass('active');$('#quickupdate').toggle();$('.profile').toggleClass('active');">
									<i class="fa fa-envelope"> </i> Write a Review
							</a></li>                     <?php } ?>							                  </ul>
					 
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<div id="companySearchResult"
		style="z-index: 999; position: absolute;
	/* height: 800px; */ float: right; position: absolute; margin-left: 64%; background-color: aqua;"></div>
	<!-- Begin page content -->
	<div class="page-content like_pae">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
				
					<div id="mobileleavecomment" style="display: none;">
						<div class="widget-header">
							<h3 class="widget-caption"> Write Reviews</h3>
						</div>
						<div class="widget">
							<div class="resume-box">                        <?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcandidate','userid'=>$profile->UserId])]); ?>                        <div
									class="form-group">                           <?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>                        </div>
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">                              <?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                           </div>
								</div>                        <?php ActiveForm::end(); ?>                     </div>
						</div>
						<div class="widget">
							<div class="widget-header">
								<h3 class="widget-caption">
									<span> Reviews </span>

								</h3>
							</div>
							<div class="widget-comments">
								<div class="row">
									<div class="col-md-12">                              <?php
        $lv = new LeaveComment();
        $allcomment = $lv->getComment($profile->UserId);
        if ($allcomment) {
            foreach ($allcomment as $ck => $cval) {
                if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) {
                    $ll = ($cval->commentFrom->PhotoId != 0) ? $url . $cval->commentFrom->photo->Doc : 'images/user.png';
                } elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) {
                    $ll = ($cval->commentFrom->LogoId != 0) ? $url . $cval->commentFrom->logo->Doc : 'images/user.png';
                }
                ?>                              <ul class=" ">
											<li><a href=""> <img src="<?=$ll;?>" alt="image"> Career Bugs
											</a></li>
											<li class="comments-block">
												<p>                                       <?=$cval->Comment;?>                                    </p>
											</li>
										</ul>                              <?php
            }
        }
        ?>                           </div>
								</div>
							</div>
						</div>
					</div>

					<div class="profile_main" style="width: 110px; margin: 0 auto;" id="no_dis_mbl1">
						<img src="<?=$ph;?>" alt="profile Photo" class=""
							style="width: 110px; height: 110px; border-radius: 200px">
					</div>


					<div class="name"
						style="text-align: center; margin-bottom: 10px; font-size: 18px;">
						<a href="#">                     <?=$profile->Name;?>                     </a>
					</div>


					<div class="widget no_shadow">
						<div class="action-buttons">
							<div class="row">
								<div class="col-md-12" id="followbox">                           <?php
        $lik = new Likes();
        $isfollow = $lik->getIsfollow($empid, $profile->UserId);
        if (empty($isfollow)) {
            if ($empid != $profile->UserId) {
                ?>                           <div
										class="half-block-left"
										onclick="follow(<?=$profile->UserId;?>,<?=$empid;?>);">                              <?php if($alusr->UserTypeId != 2 && $alusr->UserTypeId != 5){ ?>                              <a
											href="javascript:;" class="btn btn-azure btn-block"> <i
											class="fa fa-user-plus"> </i> Follow
										</a>                              <?php  } 	?>                           </div>                           <?php
            }
        } else {
            ?>                           <div class="half-block-left"
										onclick="unfollow(<?=$profile->UserId;?>,<?=$empid;?>);">                              <?php if($alusr->UserTypeId != 2 && $alusr->UserTypeId != 5){ ?>                              <a
											href="javascript:;" class="btn btn-azure btn-block"> <i
											class="fa fa-user-plus"> </i> UnFollow
										</a>                              <?php  } 	?>                           </div>                           <?php
        }
        ?>                           <?php
        if ($empid != $profile->UserId) {
            ?>                           <div class="half-block-right"> 
											<?php if($alusr->UserTypeId != 2 && $alusr->UserTypeId != 5){ ?>                              
											<a href="javascript:;" data-toggle="modal"
											data-target="#myModalmessage" class="btn btn-azure btn-block">
											<i class="fa fa-envelope"> </i> Message
										</a> <?php  } 	?>                           </div>                           <?php  } 	?>                        </div>
							</div>
						</div>
					</div>





					<div class="widget">
						<div class="widget-header">
							<h3 class="widget-caption">Statistics</h3>
						</div>
						<div class="section bordered-sky">
							<p>
								<a
									href="<?= Url::toRoute(['follower','userid'=>$profile->UserId]);?>">
									<span class="badge" id="followcount">                        <?=count($lik->getTotalfollow($profile->UserId));?>                        </span>
									<i class="fa fa-thumbs-o-up"> </i> Followers
								</a>
							</p>
						</div>
					</div>
					<div class="widget">
						<div class="widget-header">
							<h3 class="widget-caption">Profile details</h3>
						</div>
						<div class="job-short-detail">
							<dl>
							<!--	<dt>Mobile No:</dt>
								<dd><?=$profile->MobileNo;?></dd>

								<dt>Whatsapp:</dt>
								<dd><?=$profile->whatsappno;?></dd> -->

								<dt>Email:</dt>
								<dd><?=$profile->Email;?></dd>  

								<dt>Language:</dt>
								<dd><?=$profile->language;?></dd>

								<dt>Gender:</dt>
								<dd><?=$profile->Gender;?></dd>

								<dt>Work Time:</dt>
								<dd><?=$profile->WorkType;?></dd>
								<dt>Age:</dt>
								<dd><?=$profile->Age;?></dd>
							</dl>
						</div>
					</div>
					<div class="widget">
						<div class="widget-header">
							<h3 class="widget-caption">Socail Network</h3>
						</div>
						<div class="widget-body bordered-top bordered-sky">
							<div class="row">
								<div class="directory-info-row col-xs-12">
									<ul class="social-links">
										<li><a href="#" title="Facebook"> <i class="fa fa-facebook"> </i>
										</a></li>
										<li><a href="#" title="Twitter"> <i class="fa fa-twitter"> </i>
										</a></li>
										<li><a href="#" title="LinkedIn"> <i class="fa fa-linkedin"> </i>
										</a></li>
										<li><a href="#" title="Skype"> <i class="fa fa-skype"> </i>
										</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 padding-left">
					<div class="row">
						<!-- left posts-->
						<div class="col-md-12">

							<div id="leavecomment" style="display: none;">
								<div class="widget-header">
									<h3 class="widget-caption"> Write Review  </h3>
								</div>
								<div class="widget">
									<div class="resume-box">                              
									<?php
        $form = ActiveForm::begin([
            'options' => [
                'class' => 'row',
                'enctype' => 'multipart/form-data'
            ],
            'action' => Url::toRoute([
                'wall/searchcandidate',
                'userid' => $profile->UserId
            ])
        ]);
    ?>                              <div class="form-group"> 
                                    	<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>                              
                                    </div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">                                    
											<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                                 
										</div>
									</div>                             
									 <?php ActiveForm::end(); ?>                           
									 </div>
								</div>
							</div>

							<div class="widget" id="descritpion">
								<div class="widget-header">
									<h3 class="widget-caption">Description</h3>
								</div>
								<div class="widget-body bordered-top bordered-sky">
									<?=$profile->AboutYou;?>                        
								</div>
							</div>

							<div class="widget-header">
								<h3 class="widget-caption">Prefered Job Location</h3>
							</div>
							<div class="widget">
								<div class="resume-box">
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"> </i>
											</div>
											<div class="insti-name">
												<h4>Locations</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> 
                                      <?=$profile->getPreferedJobLocation();?> 
                                   </h4>
											</div>
										</div>
									</div>
								</div>
							</div>





	<div class="widget-header">
								<h3 class="widget-caption">Interested Area</h3>
							</div>
						
						
						
								<div class="widget">
								<div class="resume-box">
									
									<?php 
									if($profile->CollarType == 'white'){ ?>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Job Category</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $jobCategory = ArrayHelper::map(UserJobCategory::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'category', 'category'); 
																	
														  $CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $profile->UserId
																])->all(), 'category_id', 'category_id');
														  $expCategory = ArrayHelper::map(WhiteCategory::find()->where([
																	'id' => $CategoryIds
																])->all(), 'name', 'name');
														$jobCategory = array_merge($jobCategory, $expCategory);	
																	
																	
																	
																	
																	?>
													<h4 style="line-height:18px;"><?php echo (!empty($jobCategory)?implode(', ',$jobCategory):"");?></h4>
												</div> 
											</div>
										</div>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Role</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $jobRoles1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'role', 'role');
														  $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $profile->UserId,
																	])->all(), 'role', 'role');
														  $jobRoles = array_merge($jobRoles1, $expRoles);		
																	?>
													<h4 style="line-height:18px;"><?php echo (!empty($jobRoles)?implode(', ',$jobRoles):"");?></h4>
												</div> 
											</div>
										</div>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Skills</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $jobSkills1 = ArrayHelper::map(UserWhiteSkill::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'skill', 'skill'); 
														  $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $profile->UserId,
																	])->all(), 'skill', 'skill');
														$jobSkills = array_merge($jobSkills1, $expSkills);			
																	?>
													<h4 style="line-height:18px;"><?php echo (!empty($jobSkills)?implode(', ',$jobSkills):"");?></h4>
												</div> 
											</div>
										</div>
										
										
										
								<?php }else{ ?>
								 
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Role:  </h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->getEducationRoles();?></h4>
											</div>
										</div>
									</div>
									
									 
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Skills:  </h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4><?php  
												$skillArray = array();
												foreach ($profile->empRelatedSkills as $ask => $asv) {
													$skillArray[$asv->skill->SkillId] = $asv->skill->Skill;
														?>
													  <?php  }  ?> 
													  
													  <?php 
														$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
															'IsDelete' => 0,
															'UserId' => $profile->UserId,
														])->all(), 'Employeeskillid', 'SkillId');
														
														$skills = ArrayHelper::map(Skill::find()->where([
															'IsDelete' => 0,
															'SkillId' => $employeeSkill
														])->all(), 'SkillId', 'Skill');
														
														
														$blueSkill = array_merge($skillArray, $skills);
														$blueSkill = array_unique($blueSkill);
														if(!empty($blueSkill )){
															echo implode(', ',$blueSkill);
														}
													  ?>
													  
												</h4>
											</div>
										</div>
									</div>
								<?php }?>	
									
										</div>
								</div>
							
									
									
									
									
									
							<div class="widget-header">
								<h3 class="widget-caption">Address</h3>
							</div>
							<div class="widget">
								<div class="resume-box">
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"> </i>
											</div>
											<div class="insti-name">
												<h4>Address :</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->Address;?> </h4>
											</div>
										</div>
										
											</div>
										
											<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"> </i>
											</div>
											<div class="insti-name">
												<h4>Country :</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?= 'India';?> </h4>
											</div>
										</div>
										
											</div>
										
											<div class="row education-box">
											    
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"> </i>
											</div>
											<div class="insti-name">
												<h4>State :</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->State;?> </h4>
											</div>
										</div>
											</div>
										
											<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"> </i>
											</div>
											<div class="insti-name">
												<h4>City :</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->City;?> </h4>
											</div>
										</div>
										
											</div>
										
											<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"> </i>
											</div>
											<div class="insti-name">
												<h4>Pin Code :</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->PinCode;?> </h4>
											</div>
										</div>
									</div>
								</div>
							</div>



							<div class="widget-header">
								<h3 class="widget-caption">Education details</h3>
							</div>

							<div class="widget">
								<div class="resume-box">
										<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Language</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4>  <?=$profile->language;?> </h4>
											</div>
										</div>
									</div>	
									
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Passout Year</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->educations[0]->PassingYear;?> </h4>
											</div>
										</div>
									</div>
							
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Highest Qualification</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->educations[0]->nqual;?> </h4>
											</div>
										</div>
									</div>

									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Course</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$profile->educations[0]->course->name;?> </h4>
											</div>
										</div>
									</div>
								
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-briefcase" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>University / College</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4><?=$profile->educations[0]->University;?></h4>
											</div>
										</div>
									</div> 
									
									
										<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-file-text" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Specialization</h4>

											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
													<h4><?=$profile->educations[0]->specialization_id;?></h4>
											</div>
										</div>
									</div>
									
								</div>
							</div>



							<div class="widget">		
							<?php
    if (empty($profile->experiences)) :
        ?>
							<div class="resume-box">
									<div class="row education-box">
										<div class="col-xs-12 col-md-8 col-sm-8">No Work Experience</div>
									</div>
								</div>
							<?php endif;?>                           <?php
    if ($profile->experiences) {
        foreach ($profile->experiences as $k => $val) {
            ?>                           <div class="widget-header">
									<h3 class="widget-caption">
										Work Experience <span>                                 <?=$val->YearFrom;?> to                                  <?=$val->YearTo;?>                                 </span>
									</h3>
								</div>
								<div class="resume-box">
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
													<i class="fa fa-briefcase" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Company name</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4>                                          <?=$val->CompanyName;?>                                       </h4>
											</div>
										</div>
									</div>
							<?php if($profile->CollarType == "white"){?>
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												 <i class="fa fa-file-text" aria-hidden="true"></i>
											</div>
											<div class="insti-name"> 
												<h4>Job Category</h4> 
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<?php $CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $profile->UserId,
																	'experience_id' => $val->ExperienceId
																])->all(), 'category_id', 'category_id');
													  $expCategory = WhiteCategory::find()->where([
																	'id' => $CategoryIds
																])->one();
																?>
												<h4 style="line-height:18px;"><?php echo (!empty($expCategory->name)?$expCategory->name:"");?></h4>
											</div> 
										</div>
									</div>
									
									<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Role</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $profile->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'role_id', 'role'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($expRoles)?implode(', ',$expRoles):"");?></h4>
												</div> 
											</div>
										</div>
										
										<div class="row education-box">
											<div class="col-md-4 col-xs-12 col-sm-4">
												<div class="resume-icon">
													 <i class="fa fa-file-text" aria-hidden="true"></i>
												</div>
												<div class="insti-name"> 
													<h4>Skills</h4> 
												</div>
											</div>
											<div class="col-xs-12 col-md-8 col-sm-8">
												<div class="degree-info">
													<?php $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																		'user_id' => $profile->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'skill_id', 'skill'); ?>
													<h4 style="line-height:18px;"><?php echo (!empty($expSkills)?implode(', ',$expSkills):"");?></h4>
												</div> 
											</div>
										</div>
									
								
								
								
								<?php }else{?>
									
									
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Role</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4><?=$val->position->Position;?></h4>
											</div>
										</div>
									</div>
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Skill</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
										    <div class="degree-info">
												<?php 
													/*Get Experience Skill*/
													$employeeSkill = ArrayHelper::map(EmployeeSkill::find()->where([
														'IsDelete' => 0,
														'SkillRoleId' => $val->PositionId,
														'UserId' => $profile->UserId,
														'ExperienceId' => $val->ExperienceId
													])->all(), 'Employeeskillid', 'SkillId');
													
													$skills = ArrayHelper::map(Skill::find()->where([
														'IsDelete' => 0,
														'SkillId' => $employeeSkill
													])->all(), 'SkillId', 'Skill');
													
												?>
											
											
												<h4><?php echo $skills ? implode(", ",$skills) : "";?></h4>
											</div>
										</div>
									</div>
									
							<?php }?>
									
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-files-o" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Position</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4><?=$val->PositionName;?></h4>
											</div>
										</div>
									</div>
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-industry" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Industry</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">  
											<h4><?=isset($val->industry->IndustryName) ? $val->industry->IndustryName : 'Not Set';?></h4>
                                             </div>
										</div>
									</div>
									<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-calendar" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Experience</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4>                                           <?=$val->Experience;?> Year                                       </h4>
											</div>
										</div>
									</div>
									
									
										<div class="row education-box">
										<div class="col-md-4 col-xs-12 col-sm-4">
											<div class="resume-icon">
												<i class="fa fa-rupee" aria-hidden="true"></i>
											</div>
											<div class="insti-name">
												<h4>Salary</h4>
											</div>
										</div>
										<div class="col-xs-12 col-md-8 col-sm-8">
											<div class="degree-info">
												<h4> <?=$val->Salary;?> Lakhs  </h4>
											</div>
										</div>
									</div>
									
									
									
									
								</div>                           <?php
        }
    }
    ?>                        </div>



							<div class="widget-header">
								<h3 class="widget-caption">
									Followers </span>
								</h3>
							</div>


							<div class="widget">
									<?php
        $followers = $lik->getTotalfollow($profile->UserId);
        if (empty($followers)) :
            ?>
							<div class="resume-box">
									<div class="row education-box">
										<div class="col-xs-12 col-md-8 col-sm-8">No Followers</div>
									</div>
								</div>
							<?php endif;?>
									                                       <?php

                                                if ($followers) {
                                                    foreach ($followers as $fk => $fvalue) {
                                                        if ($fvalue->likeFrom->UserTypeId == 2 || $fvalue->likeFrom->UserTypeId == 5) {
                                                            if ($fvalue->likeFrom->PhotoId != 0) {
                                                                $ll = $url . $fvalue->likeFrom->photo->Doc;
                                                            } else {
                                                                $ll = 'images/user.png';
                                                            }
                                                        } else {
                                                            if ($fvalue->likeFrom->LogoId != 0) {
                                                                $ll = $url . $fvalue->likeFrom->logo->Doc;
                                                            } else {
                                                                $ll = 'images/user.png';
                                                            }
                                                        }
                                                        if ($fvalue->likeFrom->UserTypeId == 2) {
                                                            $link = 'searchcandidate';
                                                        } elseif ($fvalue->likeFrom->UserTypeId == 3) {
                                                            $link = 'searchcompany';
                                                        } elseif ($fvalue->likeFrom->UserTypeId == 4) {
                                                            $link = 'searchcampus';
                                                        } else {
                                                            $link = 'searchteam';
                                                        }
                                                        ?>
                                            
                                            <div class="widget-like">
									<div class="row">
										<div class="col-md-12">
											<ul class=" ">
												<li><a
													href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>">
														<img src="<?=$ll;?>" alt="image">                                          <?=$fvalue->likeFrom->Name;?>                                          </a></li>                                       <?php
                                                    }
                                                    ?>
                                        </ul>
										</div>
									</div>
								</div>
									<?php
                                                }
                                                ?>                                    
								</div>

<div class="clear"></div>

							<div class="widget">
								<div class="widget-header">
									<h3 class="widget-caption">
										Reviews </span>
									</h3>
								</div>
								<div class="widget-comments">
									<div class="row">
										<div class="col-md-12">                                    <?php
        $lv = new LeaveComment();
        $allcomment = $lv->getComment($profile->UserId);
        if ($allcomment) {
            foreach ($allcomment as $ck => $cval) {
                if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) {
                    $ll = ($cval->commentFrom->PhotoId != 0) ? $url . $cval->commentFrom->photo->Doc : '/backend/web/imageupload/user.png';
                } elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) {
                    $ll = ($cval->commentFrom->LogoId != 0) ? $url . $cval->commentFrom->logo->Doc : '/backend/web/imageupload/user.png';
                }
                if ($cval->commentFrom->UserTypeId == 2) {
                    $link = 'searchcandidate';
                } elseif ($cval->commentFrom->UserTypeId == 3) {
                    $link = 'searchcompany';
                } elseif ($cval->commentFrom->UserTypeId == 4) {
                    $link = 'searchcampus';
                } else {
                    $link = 'searchteam';
                }
                ?>                                    <ul class=" ">
												<li><a
													href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>">
														<img src="<?=$ll;?>" alt="image">                                          <?=$cval->commentFrom->Name;?>                                          </a></li>
												<li class="comments-block">
													<p>                                             <?=$cval->Comment;?>                                          </p>
												</li>
											</ul>                                    <?php
            }
        }
        ?>                                 </div>
									</div>
								</div>
							</div>



						</div>
					</div>
				</div>

				<div class="col-md-3 padding-left">
					<?php echo SidebarWidget::widget()?>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
<!-- Modal -->
<div class="modal fade" id="myModalcomment" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Write Review </h4>
			</div>
			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">               <?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcandidate','userid'=>$profile->UserId])]); ?>               <div
						class="form-group">                  <?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>               </div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">                     <?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                  </div>
					</div>               <?php ActiveForm::end(); ?>            </div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->

<div class="modal fade" id="myModalmessage" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">               <?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcandidate','userid'=>$profile->UserId])]); ?>               <div
						class="form-group">                  <?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>               </div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">                     <?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                  </div>
					</div>               <?php ActiveForm::end(); ?>            </div>
			</div>
		</div>
	</div>
</div>
