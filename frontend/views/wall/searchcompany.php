<?php
$this->title = $model->Name;

use common\models\AllUser;
use common\models\LeaveComment;
use common\models\Likes;
use frontend\components\searchpeople\SearchPeople;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use frontend\components\sidebar\SidebarWidget;

$url = '/backend/web/';

$imageurl = Yii::$app->getUrlManager()->getBaseUrl() . '/';
$url = '/backend/web/';
if ($model->LogoId != 0) {
    $ph = $url . $model->logo->Doc;
} else {
    $ph = 'images/user.png';
}
if (isset(Yii::$app->session['Employerid'])) {
    $empid = Yii::$app->session['Employerid'];
    $empdp = Yii::$app->session['EmployerDP'];
    $type = 3;
    $ppage = 'companyprofile';
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $empid = Yii::$app->session['Employeeid'];
    $empdp = Yii::$app->session['EmployeeDP'];
    $type = 2;
    $ppage = 'candidateprofile';
} elseif (Yii::$app->session['Campusid']) {
    $empid = Yii::$app->session['Campusid'];
    $empdp = Yii::$app->session['CampusDP'];
    $type = 4;
    $ppage = 'campusprofile';
} elseif (Yii::$app->session['Teamid']) {
    $empid = Yii::$app->session['Teamid'];
    $empdp = Yii::$app->session['TeamDP'];
    $type = 5;
    $ppage = 'teamprofile';
}

if(empty($empdp)){
	$empdp = $url . 'images/user.png';
}

if (isset(Yii::$app->session['Employerid'])) {
    $lg = Yii::$app->session['EmployerDP'];
} elseif (isset(Yii::$app->session['Employeeid'])) {
    $lg = Yii::$app->session['EmployeeDP'];
} elseif (isset(Yii::$app->session['Campusid'])) {
    $lg = Yii::$app->session['CampusDP'];
} elseif (isset(Yii::$app->session['Teamid'])) {
    $lg = Yii::$app->session['TeamDP'];
}
$alusr = AllUser::findOne($empid);
?>
<style>
#login_system {
	position: fixed;
	bottom: 0px;
	width: 100%;
	z-index: 99999;
	background: url("../careerimg/rep.png") repeat;
	height: 100vh;
}

.white {
	width: 100%;
	min-height: 270px;
	top: 95%;
	position: absolute;
	left: 50%;
	margin: -225px 0 0 -50%;
	background: #fff;
	padding: 20px 20px 20px 20px;
	border-top: 2px solid #f16b22;
}

#login_system .step.form {
	padding: 20px 0
}

#login_system .step.form img {
	width: 250px;
	margin: 0 auto;
	display: block
}

.wall_logo_1 {
	margin-bottom: 10px !Important
}

#head {
	padding: 0px;
	border-bottom: 1px solid #f16b22
}

#landing_page_banner_button .head_right {
	width: 35%
}

.ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content {
	background-color: azure !important;
	margin-top: -19px !important;
}
</style>
<script src="https://www.mycareerbugs.com/js/jquery.min.js"></script>
<!-- Begin page content -->
<?php if(!isset($lg)){ ?>

<div class="header-top" id="head">
	<div class="container">
		<div class="row">
			<div class="col-lg-4  col-md-4 col-sm-4 col-xs-12">
				<a class="logo_top" href=" "> <img class="wall_logo_1"
					src="<?=$imageurl;?>careerimg/logo.png" alt="MCB Logo"
					style="width: 100%">
				</a>
			</div>
			<div class="col-lg-8  col-md-8 col-sm-6 col-xs- 2 main-nav">
				<!-- Main Navigation -->

				<div class="head_right">
					<ul style="margin: 15px 0 0 0;">
						<li><a class="btn-123"
							href="https://mycareerbugs.com/site/employersregister">Employer
								Zone</a></li>
						<li class=""><strong> Are you recruiting?</strong> <a
							href="https://mycareerbugs.com/content/help"> <span>How we can
									help</span>
						</a></li>
					</ul>
				</div>
				<!--/.nav-collapse -->

			</div>

		</div>
	</div>
</div>

<div class="page-content">
	<div id="login_system">
		<div class="white">
			<div class="step form" style="display: block">
				<img src="<?=$imageurl;?>careerimg/wall.png">
				<div id="landing_page_banner_button">
					<div class="head_right">
						<ul>
							<li><a class="btn-123"
								href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>
							<li class="messages"><a class="btn-123"
								href="<?= Url::toRoute(['site/login'])?>"> Login </a></li>

						</ul>
					</div>
				</div>

			</div>

			<div class="step form">
				<div class="omb_login">

					<h6>Login</h6>

					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="email" class="form-control" name="AllUser[Email]"
									required placeholder="Email address">
							</div>

							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" class="form-control"
									name="AllUser[Password]" required placeholder="Password">
							</div>

							<!-- <span class="help-block">Password error</span>  -->

							<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
					</div>
					<div class="row omb_row-sm-offset-3">
						<div class="col-xs-12 col-sm-3">
							<p class="center-left">
								Not Yet Register ? <a
									href="<?= Url::toRoute(['site/register'])?>" class="color">
									Register Now </a>
							</p>
						</div>
						<div class="col-xs-12 col-sm-3">
							<p class="omb_forgotPwd">
								<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot
									password?</a>
							</p>
						</div>
					</div>


					<div class="row omb_row-sm-offset-3 omb_loginOr">
						<div class="col-xs-12 col-sm-6">
							<span class="omb_spanOr">or</span>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
<?php } ?>

<div class="cover profile">
<!--	<div class=" ">
		<div class="image">
			<img src="<?=$imageurl;?>careerimg/background1.jpg"
				class="show-in-modal" alt="MCB">
		</div>
	</div>
 -->

	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<div class="cover-info">
				<!--	<div class="profile_main">
						<img src="<?=$ph;?>" alt="people" class="">
					</div>
					<div class="name">
						<a href="#"><?=$model->Name;?></a>
					</div> -->
					<ul class="cover-nav" style="left:0px">
					    
					    	<li class=""><a href="<?= Url::base().'/'.str_replace(' ',"",$model->Name).'-'.$model->UserId;?>"><i class=""></i>Profile</a></li> 
							<li class=""><a href="<?=Url::toRoute(['site/jobsearch','company'=>$model->UserId]);?>" target="_blank"><i class=""></i>Job Post</a></li>
							<li class="active profile" ><a href="<?= Url::toRoute(['wall/searchcompany','userid'=>$model->UserId])?>"
							onclick="if(isMobile==true){$('#mobileleavecomment').hide();}else{$('#leavecomment').hide();}$(this).parent().toggleClass('active');$('.comm').removeClass('active');$('#quickupdate').show();"  target="_blank"><i
								class=""></i>  Wall Post </a></li> 
					</ul>

		 
				</div>
			</div>
		</div>
	</div>
</div>




<!-- Begin page content -->
<div class="page-content like_pae">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
 
<div class="profile_main" style="width:100%; border-top:2px solid #f16b22; padding:15px 20px 0 20px; margin:0px 0 0 0; background:#fff; clear:both; overflow:hidden">
<img src="<?=$ph;?>" alt="people" class="" style="width:100px;display:block;  margin:0 auto 15px; border-radius:200px"> 
	<a href="#" style="text-align:center;display:block"><?=$model->Name;?></a>
<div style="width:100%; height:2px; background:#eeeeee; margin: 10px 0 10px   0; clear:both; overflow:hidden"></div>
</div>
	 
				
				<div id="mobileleavecomment" style="display: none;">
					<div class="widget-header">
						<h3 class="widget-caption">Leave Comment</h3>
					</div>
					<div class="widget">
						<div class="resume-box">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcompany','userid'=>$model->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							</div>
		<?php ActiveForm::end(); ?>
							</div>
					</div>

					<div class="widget">
						<div class="widget-header">
							<h3 class="widget-caption">
								Comments</span>
							</h3>
						</div>

						<div class="widget-comments">
							<div class="row">
								<div class="col-md-12">
                  <?php
                $lv = new LeaveComment();
                $allcomment = $lv->getComment($model->UserId);
                if ($allcomment) {
                    foreach ($allcomment as $ck => $cval) {
                        if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) {
                            $ll = ($cval->commentFrom->PhotoId != 0) ? $url . $cval->commentFrom->photo->Doc : 'images/user.png';
                        } elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) {
                            $ll = ($cval->commentFrom->LogoId != 0) ? $url . $cval->commentFrom->logo->Doc : 'images/user.png';
                        }

                        ?>
                  <ul class=" ">
										<li><a href=""> <img src="<?=$ll;?>" alt="image"> Career Bugs
										</a></li>
										<li class="comments-block">
											<p><?=$cval->Comment;?></p>
										</li>
									</ul>
				<?php
                    }
                }
                ?>
                </div>
							</div>
						</div>
					</div>

				</div>




				<div class="widget no_shadow" style="background: #fff;padding: 0 10px 10px 10px;margin: 0;">
					<div class="action-buttons">
						<div class="row">
							<div class="col-md-12" id="followbox">
									<?php
        $lik = new Likes();
        $isfollow = $lik->getIsfollow($empid, $model->UserId);
        if (empty($isfollow)) {
            if ($empid != $model->UserId) {
                ?>
									  <div class="half-block-left"
									onclick="follow(<?=$model->UserId;?>,<?=$empid;?>);">
										<?php if($alusr->UserTypeId != 1 ){ ?>
											<a href="#" class="btn btn-azure btn-block"><i
										class="fa fa-user-plus"></i> Follow</a>
											  <?php  } 	?>
										</div>
									  <?php
            }
        } else {
            ?>
									  <div class="half-block-left"
									onclick="unfollow(<?=$model->UserId;?>,<?=$empid;?>);">
									  <?php if($alusr->UserTypeId != 1 ){ ?>
											<a href="#" class="btn btn-azure btn-block"><i
										class="fa fa-user-plus"></i> UnFollow</a>
												  <?php  } 	?>
									  </div>
										<?php
        }
        ?>
								  <?php
        if ($empid != $model->UserId) {
            ?>
                  <div class="half-block-right">
				  <?php if($alusr->UserTypeId != 1 ){ ?>
                      <a href="#" data-toggle="modal"
										data-target="#myModalmessage" class="btn btn-azure btn-block"><i
										class="fa fa-envelope"></i> Message</a>
					   <?php  } 	?>
                  </div>
				  <?php
        }
        ?>
                </div>
						</div>
					</div>
				</div>
				
				
				<div class="widget">
				
					<div class="section bordered-sky" style="border:0px; box-shadow: none;">
						<p>
							<a style="cursor: default;" href="#"><span class="badge" id="followcount"> <?=count($lik->getTotalfollow($model->UserId));?> </span><i class="  fa fa-thumbs-up"></i> Followers</a></p>
					
						<p><a href="<?= Url::toRoute(['wall/searchcompany', 'userid' => $model->UserId])?>"><span class="badge"><?=$postCount;?> </span><i class=" fa fa-comments"></i> Posts</a></p>
						 
						<p><a 	href="<?=Url::toRoute(['site/jobsearch','company'=>$model->UserId]);?>"><span class="badge"> <?=$totalactive;?> </span>	<i class=" fa fa-comments"></i> Active Jobs</a></p>
						
						
					</div>
				</div>



				<div class="widget">
					<div class="widget-header">
						<h3 class="widget-caption">Description</h3>
					</div>
					<div class="widget-body bordered-top bordered-sky">
                  <?=$model->CompanyDesc;?>
                </div>
				</div>

				<div class="widget">
					<div class="widget-header">
						<h3 class="widget-caption">Basic Skill</h3>
					</div>
					<div class="basic_skill">
				  <?php
    if ($skills) {
        foreach ($skills as $sk => $sval) {
            ?>
				  <p><?=$sval;?></p>
				  <?php
        }
    }
    ?>
                </div>
				</div>


		 


				<div class="widget widget-friends">

					<div class="widget-header">
						<h3 class="widget-caption">Followers</h3>
					</div>
					<div class="widget-like company">
						<div class="row">
							<div class="col-md-12">
								<ul class=" ">
					<?php
    $followers = $lik->getTotalfollow($model->UserId);
    if ($followers) {
        foreach ($followers as $fk => $fvalue) {
            if ($fvalue->likeFrom->UserTypeId == 2 || $fvalue->likeFrom->UserTypeId == 5) {
                if ($fvalue->likeFrom->PhotoId != 0) {
                    $ll = $url . $fvalue->likeFrom->photo->Doc;
                } else {
                    $ll = $url .'images/user.png';
                }
            } else {
                if ($fvalue->likeFrom->LogoId != 0) {
                    $ll = $url . $fvalue->likeFrom->logo->Doc;
                } else {
                    $ll = $url .'images/user.png';
                }
            }
            if ($fvalue->likeFrom->UserTypeId == 2) {
                $link = 'searchcandidate';
            } elseif ($fvalue->likeFrom->UserTypeId == 3) {
                $link = 'searchcompany';
            } elseif ($fvalue->likeFrom->UserTypeId == 4) {
                $link = 'searchcampus';
            } else {
                $link = 'searchteam';
            }
			if(empty($ll)){
				 $ll = $url .'images/user.png';
			}
			
            ?>
						<li><a
										href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>">
											<img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a>
									</li>
					<?php
        }
    }
    ?>     
                  </ul>
							</div>
						</div>
					</div>
				</div>


 


			</div>




			<div class="col-md-6 padding-left">
				<div class="row">
					<!-- left posts-->
					<div class="col-md-12">
					
				<?php if ($type == 3) {?>
				<div class="box profile-info">
            <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>
                 <input type="text" id="post_title"
							class="form-control"
							name="CompanyWallPost[PostTitle]" required placeholder="Enter Post Title" /><br/>
				<textarea id="post_d"
							class="form-control input-lg p-text-area"
							name="CompanyWallPost[Post]" required rows="2"
							placeholder="Post your job here..."></textarea>
							
						<div class="box-footer">
							<ul class="nav nav-pills">
								<li><select name="CompanyWallPost[CIndustryId]"
									required="required" class="form-control bfh-states">
										<option selected="selected" value="">Select an Industry</option>
						<?php foreach($industry as $key=>$value){?>
						<option value="<?php echo $key;?>"><?=$value;?></option>
						<?php } ?>
					</select></li>

								<li><select class="questions-category form-control  "
									name="CompanyWallPost[PositionId]" required="required"
									id="emppos">
										<option value="">Select Role</option>
						<?php
        foreach ($roles as $key => $value) {
            ?>
						<option value="<?=$value->PositionId;?>"><?=$value->Position;?></option>
						<?php
        }
        ?>
					</select></li>
								<li  class="slc_cty">
                                    <select class="questions-category form-control"
                                            id="naukari-location" name="CompanyWallPost[Locationat][]"
                                            multiple="multiple">
                                    </select>
                                </li>
								<li><a class="cameraa"><i class="fa fa-camera"></i><input
										type="file" name="CompanyWallPost[ImageId]"
										onchange="$(this).next().html($(this).val());"
										accept="image/*"><span></span></a></li>
								<li  style="display:none"><a href="javascript:void(0)" class="cameraa"><i
										class=" fa fa-film"></i><input type="file"
										name="CompanyWallPost[VideoId]"
										onchange="$(this).next().html($(this).val());"
										accept="video/*"><span></span></a></li>
										
										<li><button id="post_btn" type="submit"
								class="btn btn-info pull-right">Post</button></li>
							</ul>
							<div class="clear"></div>

							
							<div class="clear"></div>



						</div>
			


			<?php ActiveForm::end(); ?>
			</div>
		<?php } ?>


						<div id="leavecomment" style="display: none;">
							<div class="widget-header">
								<h3 class="widget-caption">Leave Comment</h3>
							</div>
							<div class="widget">
								<div class="resume-box">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcompany','userid'=>$model->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
									</div>
		<?php ActiveForm::end(); ?>
							</div>
							</div>

							<div class="widget">
								<div class="widget-header">
									<h3 class="widget-caption">
										Comments</span>
									</h3>
								</div>

								<div class="widget-comments">
									<div class="row">
										<div class="col-md-12">
                  <?php
                $lv = new LeaveComment();
                $allcomment = $lv->getComment($model->UserId);
                if ($allcomment) {
                    foreach ($allcomment as $ck => $cval) {
                        if ($cval->commentFrom->UserTypeId == 2 || $cval->commentFrom->UserTypeId == 5) {
                            $ll = ($cval->commentFrom->PhotoId != 0) ? $url . $cval->commentFrom->photo->Doc : 'images/user.png';
                        } elseif ($cval->commentFrom->UserTypeId == 3 || $cval->commentFrom->UserTypeId == 4) {
                            $ll = ($cval->commentFrom->LogoId != 0) ? $url . $cval->commentFrom->logo->Doc : 'images/user.png';
                        }
                        if ($cval->commentFrom->UserTypeId == 2) {
                            $link = 'searchcandidate';
                        } elseif ($cval->commentFrom->UserTypeId == 3) {
                            $link = 'searchcompany';
                        } elseif ($cval->commentFrom->UserTypeId == 4) {
                            $link = 'searchcampus';
                        } else {
                            $link = 'searchteam';
                        }
                        ?>
                  <ul class=" ">
												<li><a
													href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>">
														<img src="<?=$ll;?>" alt="image"><?=$cval->commentFrom->Name;?></a>
												</li>
												<li class="comments-block">
													<p><?=$cval->Comment;?></p>
												</li>
											</ul>
				<?php
                    }
                }
                ?>
                </div>
									</div>
								</div>
							</div>

						</div>
						
						
						
						
						
						<div id="quickupdate">

							<div class="widget-header">
								<h3 class="widget-caption">Quick Updates</h3>
							</div>

<?php

                Pjax::begin([
                    'id' => 'allpost-pjax'
                ]);
                echo ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemOptions' => [
                        'class' => 'item'
                    ],
                    'itemView' => '_posts',
                    // 'summary' => '',
                    'emptyText' => 'No Posts Found',
                    'viewParams' => [
                        'empdp' => $empdp,
                        'empid' => $empid
                    ]
                    // 'pager' => [
                    // 'class' => \kop\y2sp\ScrollPager::className(),
                    // 'paginationSelector' => '.list-view .pagination',
                    // 'triggerTemplate' => 'working'
                    // ]
                ]);
                Pjax::end();

                ?>

						</div>






					</div>
				</div>
			</div>






			<div class="col-md-3 padding-left">

				<div class="widget">
					<div class="widget-header">
						<h3 class="widget-caption">
							Suggested Candidate</span>
						</h3>
					</div>
					<div class="widget-know" style="display: none;">
						<ul class=" ">
								  <?php
        if ($suggested) {
            foreach ($suggested as $sk => $sval) {
                if ($sval->user->PhotoId != 0) {
                    $ph = $url . $sval->user->photo->Doc;
                } else {
                    $ph = 'images/user.png';
                }
                ?>
										<li><a
								href="<?= Url::toRoute(['searchcandidate','userid'=>$sval->user->UserId])?>">
									<img src="<?=$ph;?>" alt="image"><?=$sval->user->Name;?>
										<span><?=$sval->user->EntryType;?></span>
							</a></li>
									<?php
            }
        }
        ?>
								  </ul>

					</div>
				</div>


 



				<div class="widget">
					<div class="advertisement bordered-sky">
						<img src="<?=$imageurl;?>careerimg/adban_block/ban.jpg"> <img
							class="no_mar"
							src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img
							src="<?=$imageurl;?>careerimg/adban_block/ban1.jpg"> <img
							class="no_mar" src="<?=$imageurl;?>careerimg/adban_block/ban.jpg">
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalcomment" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Leave Comment</h4>
			</div>

			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcompany','userid'=>$model->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>

		</div>
	</div>
</div>
<!-- Modal -->

<!-- Modal -->
<div class="modal fade" id="myModalmessage" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>

			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcompany','userid'=>$model->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>
		</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>

		</div>
	</div>
</div>
<!-- Modal -->

<div class="modal fade" id="myModal_email" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Share Post</h4>
			</div>

			<div class="row main" style="padding:30px">
				<div class="xs-12 col-sm-12 main-center">
		<?php
			$form1 = ActiveForm::begin([
				'options' => [
					'id' => 'sendshareemail-share',
					'class' => 'row',
					'enctype' => 'multipart/form-data'
				]
			]);
			?>
		<div class="form-group">
			<input type="text" id="MailId" class="form-control"
							name="Mail[EmailId]" Placeholder="Enter E-Mail">
					</div>

		<?= $form1->field($mailmodel, 'Subject')->textInput(['value'=>'Post From My Career Bugs','maxlength' => true,'Placeholder'=>'Subject'])->label('Subject') ?>

					<div class="col-md-12 col-sm-12 col-xs-12">

						<div class="form-group">
									<?= Html::submitButton('SEND', ['class' => 'btn btn-default btn-green','id' => 'sendshareemail-share-submit']) ?>
								</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>
		</div>
	</div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<style>
a.morelink {
	text-decoration: none;
	outline: none;
}

.morecontent span {
	display: none;
}

.comment {
	margin: 10px;
}
</style>
<script type="text/javascript">

function mailtoemp(id,post) {
	console.log('modal_open'+id+'post'+post);
	$('#'+id).modal('show');
	postid = post;
// 	tinyMCE.activeEditor.setContent($("#editbox"+post).find('textarea').text());
}
function liketopost(postid)
{
	 $.ajax({
		 url : "<?= Url::toRoute(['wall/liketopost'])?>?postid="+postid,
		 success : function(res)
		 {
			if(res.status == 'OK'){
				$('#likedisliketext_'+postid).empty();
				$('#likedisliketext_'+postid).html(res.total_like+' likes');
				$('#likedislikebutton_'+postid).empty();
				$('#likedislikebutton_'+postid).html(res.button);
			}
		 }
	 });
}

function searchcompanypost(userid,empid) {
    $.ajax({url:"<?= Url::toRoute(['wall/searchcompanypost'])?>?userid="+userid+"&empid="+empid,
               success:function(results)
               {
                 $('#searchcompanypost').html(results);
               }
        });
}

$(document).ready(function(){
	$(document).on('click', '.click-post', function(){
		var commentId = $(this).attr('comment_id');
		$('.reply-comment-'+commentId).show();
	});
	
	$(document).ready(function () {  
		$("#citylist").autocomplete({ 
			source: "<?php echo Url::toRoute(['/'])?>"+'wall/getcities', 
			minLength:1,
			appendTo: "#cityContainer" 
		});  
	});


    $("#naukari-location").select2({
        maximumSelectionLength: 5,
        placeholder: "Select upto 5 Location",
        ajax: {
            url: '<?=Url::toRoute(['getpreferredlocation']);?>',
            dataType: 'json',
            type: "GET",
            data: function (params) {

                return {
                    q: params.term,
                    page: 1
                }
            },
            processResults: function(data){
                return {
                    results: $.map(data.results, function (item) {

                        return {
                            text: item.CityName,
                            id: item.id
                        }
                    })
                };
            }
        }
    });

	$(document).on('click', '.submit-nested-comment', function(){
		var message = $(this).closest("form").find('input[type=text]').val();
		$('#qloader').show();
		var obj = $(this);
		var postid = $(this).attr('comment_id');
		$(this).closest("form").find('.comment-error').html('');
		if(message == ""){
			$(this).closest("form").find('.comment-error').html('Comment can not be blank');
		}else{
			var formData = $(this).closest("form").serialize();
			$.ajax({
			url : "<?=Yii::$app->urlManager->baseUrl."/wall/nestedcommenttopost"?>",
			type : 'POST',
			data : formData,
			dataType: 'JSON',
			success : function(response) {
				
				if(response['status'] == 'OK'){
					var html = '<div class="reply-msg boxcommentdel'+response['comment_id']+'"><div class="box-comment"><img class="img-circle img-sm" src="'+response['image']+'" alt="User Image"><div class="comment-text"><span class="pull-right" style="cursor: pointer;" onclick="delcomment('+response['comment_id']+');"><i class="fa fa-trash"></i></span><span class="text-muted pull-right">'+response['date']+'</span>'+response['message']+'</div></div></div>';
					$('#maincomment-'+postid).append(html);
					obj.closest("form").find('input[type=text]').val("");
					$('.reply-comment-'+postid).hide();
					$('#qloader').hide();
					var objDiv = document.getElementById("#PostComments" + postid);
					objDiv.scrollTop = objDiv.scrollHeight;
				}
				$('#qloader').hide();
				
				
			}
		});
		}
		return false;
	});
	
});	
</script>



<div class="modal fade" id="myModalmessage" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">               <?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchcandidate','userid'=>$profile->UserId])]); ?>               <div
						class="form-group">                  <?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>               </div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">                     <?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>                  </div>
					</div>               <?php ActiveForm::end(); ?>            </div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->


