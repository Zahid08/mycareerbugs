<?php
$this->title =$profile->Name;

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Likes;
use common\models\AllUser;
use common\models\LeaveComment;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
if($profile->PhotoId!=0)
{
    $ph=$url.$profile->photo->Doc;
}
else
{
    $ph='images/user.png';
}
?>



<?php
$alusr = AllUser::findOne($empid); 

?>


<!-- Begin page content -->
    <div class="page-content">
   
          <div class="cover profile">
            <div class=" ">
              <div class="image">
                <img src="<?=$imageurl;?>careerimg/background1.jpg" class="show-in-modal" alt="people">
              </div>  
            </div>
			
			
			 <div class="container">
				  <div class="row">
					<div class="col-md-12"> 
					
						<div class="cover-info">
						  <div class="profile_main">
							<img src="<?=$ph;?>" alt="people" class="">
						  </div>
						  <div class="name"><a href="#"><?=$profile->Name;?></a></div>
						  <ul class="cover-nav"> 
							<li  class="active profile"><a href="#" onclick="if(isMobile==true){$('#mobileleavecomment').hide();}else{$('#leavecomment').hide();}$(this).parent().toggleClass('active');$('.comm').removeClass('active');$('#quickupdate').show();"><i class="fa fa-user"></i> Profile Wall </a></li>
							<?php
								if($empid!=$profile->UserId)
								{
									?>
                                <li  class="comm"><a href="#" onclick="if(isMobile==true){$('#mobileleavecomment').slideToggle();}else{$('#leavecomment').slideToggle();}$(this).parent().toggleClass('active');$('#quickupdate').toggle();$('.profile').toggleClass('active');"><i class="fa fa-envelope"></i> Leave a comment </a></li> <?php } ?>	
						  </ul> 
						  
						  <div class="form-group right_main"> 
							<input class="form-control1" name="name" id="project" placeholder="Search for People, Company and Consultant" type="text"> <input type='hidden' id="termid" />
							<input class="src1" id="searchit" value="Search" type="button"> 
                            </div>
							
						</div>
					  </div>
					</div>
       
      </div>
	  
	     </div>
	  
	  
    <!-- Begin page content -->
 <div class="page-content like_pae">
    <div class="container">
      <div class="row">
	  <div class="col-md-3">
	    
     <div class="widget no_shadow"> 
				<div class="action-buttons">
				  <div class="row">
				 <div class="col-md-12" id="followbox">
              
                 <?php
              $lik=new Likes();
              $isfollow=$lik->getIsfollow($empid,$profile->UserId);
              if(empty($isfollow))
              {
                if($empid!=$profile->UserId)
                {
              ?>
                <div class="half-block-left" onclick="follow(<?=$profile->UserId;?>,<?=$empid;?>);">
				 <?php if($alusr->UserTypeId != 2 && $alusr->UserTypeId != 5){ ?>
                      <a href="#" class="btn btn-azure btn-block"><i class="fa fa-user-plus"></i> Follow</a>
					     <?php  } 	?>
                  </div>
                <?php
                }
              }
              else
              {
            ?>
                <div class="half-block-left" onclick="unfollow(<?=$profile->UserId;?>,<?=$empid;?>);">
				<?php if($alusr->UserTypeId != 2 && $alusr->UserTypeId != 5){ ?>
                            <a href="#" class="btn btn-azure btn-block"><i class="fa fa-user-plus"></i> UnFollow</a>
							   <?php  } 	?>
                      </div>
                  <?php
              }
              ?>
                  <?php
								if($empid!=$profile->UserId)
								{
									?>
                  <div class="half-block-right">
				  <?php if($alusr->UserTypeId != 2 && $alusr->UserTypeId != 5){ ?>
                      <a href="#" data-toggle="modal" data-target="#myModalmessage" class="btn btn-azure btn-block"><i class="fa fa-envelope"></i> Message</a>
					     <?php  } 	?>
                  </div>
				  <?php
								}
								?>
                </div>
               </div> 
				</div>  			   
			 </div>

			 
			 
		  <div class="widget">
		   <div class="widget-header">
              <h3 class="widget-caption">Statistics</h3>
            </div> 
		     <div class="section bordered-sky">  
					<p><a href="<?= Url::toRoute(['follower','userid'=>$profile->UserId]);?>"><span class="badge" id="followcount"><?=count($lik->getTotalfollow($profile->UserId));?></span>   <i class="fa fa-thumbs-o-up"></i>  Followers</a></p> 
              </div> 
          </div>
		  

		  
	
	
	<div class="widget">
	<div class="widget-header">
              <h3 class="widget-caption">  Profile details</h3>
            </div>

                            <div class="job-short-detail">
                             
                                <dl>
                                    <dt>Location</dt>
                                    <dd><?=$profile->Address.' ,'.$profile->City.' ,'.$profile->State;?></dd>
									<?php
									if($profile->experiences)
									{
									$cnt=count($profile->experiences);
										  ?>
                                    <dt>Current:</dt>
                                    <dd><?=$profile->experiences[$cnt-1]->CompanyName;?></dd>
									<?php
									}
									?>
                                    <dt>Education:</dt>
                                    <dd><?=$profile->educations[0]->University;?></dd>

                                    
                                </dl>
                            </div>
							
							 </div>
							
		   
			  
			<div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">Basic Skill</h3>
                </div>
                <div class="basic_skill">
				  <?php
				  foreach($profile->empRelatedSkills as $ask=>$asv)
				  {
				  ?>
                  <p><?=$asv->skill->Skill;?></p>
				 <?php
				  }
				  ?>
                </div>
              </div>

			 
    
							
							
							
		  <div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">  Socail Network</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  
						
						<div class="row">
                          <div class="directory-info-row col-xs-12">	
	          	              <ul class="social-links">
                                  <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
                              </ul>      
						</div>
                     </div>
                </div>
              </div>
			  
			  
			  
        </div>
		 
		
		<div class="col-md-6 padding-left">
          <div class="row">
               <!-- left posts-->
            <div class="col-md-12">
			 
			 
		  <div class="widget" style="display:none;">
                <div class="widget-header">
                  <h3 class="widget-caption">Description</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  Career Bugs, India's  Job Site provides you the best opportunity to find the right job today. We provide information about Freshers Jobs, Latest Walkins, Latest Off Campus Drives For Freshers, Bank Jobs.
                </div>
              </div>
		   
		 
				<div id="leavecomment" style="display: none;">
				<div class="widget-header">
					<h3 class="widget-caption"> Leave Comment</h3>
				  </div>
				 <div class="widget">	
							<div class="resume-box">
				<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchteam','userid'=>$profile->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
							</div>
				 </div>
				 
				  <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Comments</span></h3>
							</div>
			
			<div class="widget-comments">
              <div class="row">
                <div class="col-md-12">
                  <?php
				$lv=new LeaveComment();
				$allcomment=$lv->getComment($profile->UserId);
				if($allcomment)
				{
					foreach($allcomment as $ck=>$cval)
					{
						if($cval->commentFrom->UserTypeId==2 || $cval->commentFrom->UserTypeId==5)
						{
							$ll=($cval->commentFrom->PhotoId!=0)?$url.$cval->commentFrom->photo->Doc:'images/user.png';
						}
						elseif($cval->commentFrom->UserTypeId==3 || $cval->commentFrom->UserTypeId==4)
						{
							$ll=($cval->commentFrom->LogoId!=0)?$url.$cval->commentFrom->logo->Doc:'images/user.png';
						}
						if($cval->commentFrom->UserTypeId==2){$link='searchcandidate';}elseif($cval->commentFrom->UserTypeId==3){$link='searchcompany';}elseif($cval->commentFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
				?>
                  <ul class=" ">
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"> <?=$cval->commentFrom->Name;?></a> </li>
						<li class="comments-block"> 
		                  <p><?=$cval->Comment;?></p>
						  </li>              
                  </ul>
				<?php
					}
				}
				?>
                </div>
              </div>
            </div>
						  </div>
				 
				 
				</div>
				
		   				
		<div id="quickupdate">
		   <div class="widget-header">
              <h3 class="widget-caption">  Education details</h3>
            </div>
				
							 <div class="widget">	
							<div class="resume-box">
                                
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Higest Qualification</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->HighestQualification;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Course</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4> <?=$profile->educations[0]->course->name;?> </h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>University / College</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->University;?></h4>
                                             </div>
                                    </div>
                                </div>
								
								 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4> 	Skills</h4>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
										  <?php
												$skill='';
												foreach($profile->empRelatedSkills as $ask=>$asv)
												{
														$skill.=$asv->skill->Skill.', ';
												}
												$skill=trim(trim($skill),",");
												?>
                                            <h4><?=$skill;?></h4>
                                             </div>
                                    </div>
                                </div>
                            </div>
    </div>
                              <div class="widget">	
			<?php
				if($profile->experiences)
				{
						foreach($profile->experiences as $k=>$val)
						{
				?>
           
               <div class="widget-header">
              <h3 class="widget-caption">  Work Experience   <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span></h3>
            </div> 
			      <div class="resume-box"> 
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Company name</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->CompanyName;?></h4>
                                             </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Role</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->position->Position;?></h4>
                                             </div>
                                    </div>
                                </div>
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Industry</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
										  <div class="degree-info">
                                           <?php
											if($val->industry)
											{
											?>
                                            <h4><?=$val->industry->IndustryName;?></h4>
											<?php
											}
											?>
                                          </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Experience</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4>> <?=$val->Experience;?> Year</h4>
                                             </div>
                                    </div>
                                </div>
								
				
				      </div>
				  <?php
						}
				}
				?>
			
			</div>

						  <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Team Member</span></h3>
							</div>
			
							  <div class="widget-like">
								<div class="row">
								  <div class="col-md-12">
									<ul class=" ">
										  <?php
										 if($profile->teams)
										 {
											 foreach($profile->teams as $tk=>$tval)
											 {
												if($tval->MemberPhoto!=0)
													  {
														$teammemp=$url.$tval->memberphoto->Doc;
													  }
													  else
													  {
														$teammemp='images/user.png';
													  }
										 ?>
										  <li> <a href=""> <img src="<?=$teammemp;?>" alt="image"><?=$tval->MemberName;?></a> </li>
										  <?php
											 }
										 }
										 ?>
									</ul>
								  </div>
								</div>
							  </div>       
						  </div>	
							
						 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Followers</span></h3>
							</div>
			
			<div class="widget-like">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
                    <?php
                    $followers=$lik->getTotalfollow($profile->UserId);
                    if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeFrom->UserTypeId==2 || $fvalue->likeFrom->UserTypeId==5)
							  {
									if($fvalue->likeFrom->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeFrom->photo->Doc;
									}
									else
									{
										  $ll='images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeFrom->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeFrom->logo->Doc;
							  }
							  else
							  {
									$ll='images/user.png';
							  }
							  }
                    if($fvalue->likeFrom->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeFrom->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>"><img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a> </li>
					<?php
                        }
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>       
						  </div>
							
		  </div>
							
								
				  
						 	
							
							
							
							
							
			   </div> 
			 </div>
        </div>
		 
		 
		 
		  
		<div class="col-md-3 padding-left">
		
		
		                      <div class="widget">	  
							   <div class="widget-header">
							     <h3 class="widget-caption">  Recent Job</span></h3>
							   </div>
		                       <div class="recent-widget-know"> 
								      <div class="rsw"> 
											<aside> 
												<div class="widget_block"> 
													<ul class="related-post">
													  <?php
													  if($recentjob)
													  {
													  foreach($recentjob as $jkey=>$jvalue)
													  {
													  ?>
														<li>
															<a href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>"> <?=$jvalue->JobTitle;?></a>
															<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?> </span>
															<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
															<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?> </span>
														</li>
													  <?php
													  }
													  }
													  ?>
														<a href="<?= Url::toRoute(['site/recentjob'])?>" class="sidebar_view_all"> View all  </a> 
													</ul>
												</div> 
											</aside>
                                  </div>
							  </div> 
							</div>
							
							
							
							
		                      <div class="widget">	  
							   <div class="widget-header">
							     <h3 class="widget-caption"> People you know</span></h3>
							   </div>
		                       <div class="widget-know"> 
								  <ul class=" ">
									<?php
									if($peopleyouknow)
									{
										  foreach($peopleyouknow as $pk=>$pval)
										  {
										  if($pval->PhotoId!=0)
										  {
											  $pimage=$url.$pval->photo->Doc;
										  }
										  else
										  {
											  $pimage='images/user.png';
										  }
										  ?>
										<li> <a href=""> <img src="<?=$pimage;?>" alt="image"> <?=$pval->Name;?>
										<span>
										  <?php
										  if($pval->experiences)
										  {
												$tc=count($pval->experiences);
												echo $pval->experiences[$tc-1]->position->Position.' at '.$pval->experiences[$tc-1]->CompanyName;
										  }
										  ?>
										</span></a> </li>
										<?php
										  }
									}
									?>
								  </ul>  
							  </div> 
							</div>
							
							
							
							
							 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Top Recruiter Company</span></h3>
							</div>
		                  <div class="widget-know"> 
								  <ul class=" ">
										  <?php
										  if($topcompany)
										  {
												foreach($topcompany as $tk=>$tval)
												{
												if($tval->LogoId!=0)
												{
													$pimagel=$url.$tval->logo->Doc;
												}
												else
												{
													$pimagel='images/user.png';
												}  
										  ?>
										<li> <a href="<?= Url::toRoute(['searchcompany','userid'=>$tval->UserId])?>"> <img src="<?=$pimagel;?>" style="width: 50px;height: 50px;" alt="image"><?=$tval->Name;?></a>
										<span><a href="<?= Url::toRoute(['follower','userid'=>$tval->UserId]);?>"><?=count($lik->getTotalfollow($tval->UserId));?> Followers</span></a> </li>
										<?php
										  }
										  }
										  ?>
								  </ul> 
								  
							</div> 
							</div>
			
		  </div>
		  
		  
		  
		  
		  
         
      </div>
    </div>
 </div>
     </div>

<!-- Modal -->
<div class="modal fade" id="myModalcomment" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Leave Comment</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchteam','userid'=>$profile->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($leavecomment, 'Comment')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
		
	  </div>  
	  </div>
</div>
			<!-- Modal -->
			
<!-- Modal -->
<div class="modal fade" id="myModalmessage" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
	     <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
		
		<div class="row main">
		<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data'],'action'=>Url::toRoute(['wall/searchteam','userid'=>$profile->UserId])]); ?>
		<div class="form-group">
		<?= $form->field($message, 'Message')->textArea(['row' => 4])->label(false) ?>
		</div>
							  <div class="col-md-12 col-sm-12 col-xs-12">
									<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
							  </div>
		<?php ActiveForm::end(); ?>
		</div>
		</div>
		
	  </div>  
	  </div>
</div>
			<!-- Modal -->	