<?php
$this->title =Yii::$app->session['TeamName'];

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
use common\models\Skill;
use common\models\EmployeeSkill;
use common\models\Likes;
use common\models\LeaveComment;
use common\models\Documents;
use common\models\NaukariQualData;
use common\models\ExperienceWhiteRole;
use common\models\ExperienceWhiteSkill;
use common\models\UserWhiteRole;
use common\models\UserJobCategory;
use common\models\UserWhiteSkill;
use common\models\WhiteCategory;
$lik=new Likes();
?>
<!-- Begin page content -->
    <div class="page-content">
   
          <div class="cover profile">
            <div class=" ">
              <div class="image">
                <img src="<?=$imageurl;?>careerimg/background1.jpg" class="show-in-modal" alt="people">
              </div>  
            </div>
			
			
			 <div class="container">
				  <div class="row">
					<div class="col-md-12"> 
					
						<div class="cover-info">
						  <div class="profile_main">
							<img src="<?=Yii::$app->session['TeamDP'];?>" alt="team-image"
							class="img-responsive center-block ">
						  </div>
						  <div class="name"><a href="#"><?=Yii::$app->session['TeamName'];?></a></div>
						  <ul class="cover-nav"> 
							<li class="active"><a href="#"><i class="fa fa-user"></i> About <?=Yii::$app->session['TeamName'];?></a></li>							
							<li><a href="<?= Url::toRoute(['/myactivity'])?>"><i
									class="fa fa-files-o"></i> My Activity</a></li>
							<li class="active"><a href="#" data-toggle="modal"
								data-target="#myModalcvupload"><i class="fa fa-upload azure"></i>
									Upload CV</a></li>
							<li class=" "><a href="<?= Url::toRoute(['team/editprofile'])?>"><i
									class="fa fa-pencil-square-o"></i> Edit Profile</a></li>						
						  </ul> 
						  
						 
						  
						  <div class="form-group right_main"> 
							<input class="form-control1" name="name" id="project" placeholder="Search for People, Company and Consultant" type="text"> <input type='hidden' id="termid" />
							<input class="src1" id="searchit" value="Search" type="button"> 
                            </div>
							
						</div>
					  </div>
					</div>
       
      </div>
	  
	     </div>
	  
	  
    <!-- Begin page content -->
 <div class="page-content like_pae">
    <div class="container">
      <div class="row">
	  <div class="col-md-3">
     <div class="widget no_shadow"> 
				<div class="action-buttons">
				  <div class="row">
				 <div class="col-md-12">
              
                  <div class="half-block-left">
                      <a href="#" class="btn btn-azure btn-block"><i class="fa fa-user-plus"></i> Follow</a>
                  </div>
                  <div class="half-block-right">
                      <a href="#" class="btn btn-azure btn-block"><i class="fa fa-envelope"></i> Message</a>
                  </div>
                </div>
               </div> 
				</div>  			   
			 </div>

			 
			 
		  <div class="widget">
		   <div class="widget-header">
              <h3 class="widget-caption">Statistics</h3>
            </div> 
		     <div class="section bordered-sky">  
					<p><a href="<?= Url::toRoute(['follower','userid'=>$profile->UserId]);?>"><span class="badge"><?=count($lik->getTotalfollow($profile->UserId));?></span>   <i class="fa fa-thumbs-o-up"></i>  Followers</a></p> 
              </div> 
          </div>
		  

		  
	
	
	<div class="widget">
	<div class="widget-header">
              <h3 class="widget-caption">  Profile details</h3>
            </div>
                            <div class="job-short-detail">
                                <dl>
                                   <!-- <dt>Location</dt>
                                    <dd><?=$profile->Address.' ,'.$profile->City.' ,'.$profile->State;?></dd>
								 -->
								 
									<dt>Phone:</dt>                                    <dd>+<?=$profile->MobileNo;?> </dd>                                    								    <dt>Whatsapp:</dt>                                    <dd>+<?=$profile->whatsappno;?> </dd>
                                     <dt>Email</dt>
                                      <dd>+<?=$profile->Email;?> </dd> 
                                      
                                       <dt>Address:</dt>

                                    <dd><?=$profile->Address;?></dd>
                                    <dt>State:</dt>

                                    <dd><?=$profile->State;?></dd>
                                    
                                    <dt>City:</dt> 
                                    <dd><?=$profile->City;?></dd>
                                    <dt>Pincode:</dt>
                                    <dd><?=$profile->PinCode;?></dd>
                                    <dt>Salary:</dt>
                                    <dd><?=(!empty($profile->Salary)?$profile->Salary.' Lakhs':"");?></dd>
									<dt>Team Members:</dt>
                                    <dd><?=$profile->NoOfTeamMember;?></dd>
                                    
                                </dl>
                            
                            </div>
							
							 </div>
							
		   
			  
			<div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">Basic Skill</h3>
                </div>
                <div class="basic_skill">
				<?php $jobSkills1 = ArrayHelper::map(UserWhiteSkill::find()->where([
									'user_id' => $profile->UserId
								])->all(), 'skill', 'skill'); 
					  $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
									'user_id' => $profile->UserId,
								])->all(), 'skill', 'skill');
					$jobSkills = array_merge($jobSkills1, $expSkills);			
																	?>
				
				 <?php echo (!empty($jobSkills)?implode(', ',$jobSkills):"");?>
                </div>
              </div>

			 
    
							
							
							
		  <div class="widget">
                <div class="widget-header">
                  <h3 class="widget-caption">  Socail Network</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  
						
						<div class="row">
                          <div class="directory-info-row col-xs-12">	
	          	              <ul class="social-links">
                                  <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                  <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                  <li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
                              </ul>      
						</div>
                     </div>
                </div>
              </div>
			 
			 
			
			<div class="widget widget-friends">
            <div class="widget-header">
              <h3 class="widget-caption">Follow</h3>
            </div>
            <div class="widget-like company">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
					<?php
                    $followers=$lik->getMyfollow($profile->UserId);
                    if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeTo->UserTypeId==2 || $fvalue->likeTo->UserTypeId==5)
							  {
									if($fvalue->likeTo->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeTo->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeTo->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeTo->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
                     if($fvalue->likeTo->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeTo->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeTo->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeTo->UserId])?>"> <img src="<?=$ll;?>" alt="image"><?=$fvalue->likeTo->Name;?></a> </li>
					<?php
                        }
                    }
                    ?>               
                  </ul>
                </div>
              </div>
            </div>
          </div> 
			  
			  
        </div>
		 
		
		<div class="col-md-6 padding-left">
          <div class="row">
               <!-- left posts-->
            <div class="col-md-12">
			 
			 
		  <div class="widget" >
                <div class="widget-header">
                  <h3 class="widget-caption">Description</h3>
                </div>
                <div class="widget-body bordered-top bordered-sky">
                  <?=nl2br($profile->TeamDesc);?>
                </div>
              </div>
		   
		 
		   <div class="widget-header">
              <h3 class="widget-caption">   Interested Areas</h3>
            </div> 
            
            		<div class="widget">
            		    	<div class="resume-box">								
							<div class="row education-box">      
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>

									<div class="insti-name">
										<h4>Job Category</h4>
									</div>

								</div>

								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<?php $jobCategory = ArrayHelper::map(UserJobCategory::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'category', 'category'); 
																	
														  $CategoryIds = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																	'user_id' => $profile->UserId
																])->all(), 'category_id', 'category_id');
														  $expCategory = ArrayHelper::map(WhiteCategory::find()->where([
																	'id' => $CategoryIds
																])->all(), 'name', 'name');
														$jobCategory = array_merge($jobCategory, $expCategory);	?>
										<h4 ><?php echo (!empty($jobCategory)?implode(', ',$jobCategory):"");?></h4>
									</div>
								</div>
							</div>
							<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>

									<div class="insti-name">
										<h4>Role</h4>
									</div>

								</div>

								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
										<?php $jobRoles1 = ArrayHelper::map(UserWhiteRole::find()->where([
																		'user_id' => $profile->UserId
																	])->all(), 'role', 'role');
														  $expRoles = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $profile->UserId,
																	])->all(), 'role', 'role');
														  $jobRoles = array_merge($jobRoles1, $expRoles);		
																	?>
										
										<h4><?php echo (!empty($jobRoles)?implode(', ',$jobRoles):"");?></h4>
									</div>
								</div>
							</div>
								
								 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Skills</h4>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
										  
                                            <h4> <?php echo (!empty($jobSkills)?implode(', ',$jobSkills):"");?></h4>
											

                                             </div>
                                    </div>
                                </div>
								
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                             <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Industry</h4>                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
											<?php echo $profile->industry->IndustryName;?>
											<h4> </h4>
										</div>
                                    </div>
                                </div>
								
								
							</div>   
		   				
		   				    </div>
              </div>   
		   				
		   				
						  
		   <div class="widget-header">
              <h3 class="widget-caption">  Education details</h3>
            </div><?php /*  echo "<pre>";print_r($profile->educations);die(); */?>
				<div class="widget">	
							<div class="resume-box">								<div class="row education-box">                               
							<div class="col-md-4 col-xs-12 col-sm-4">                                       
							<div class="resume-icon">                                        
							<i class="fa fa-file-text" aria-hidden="true"></i>                                     
							</div>                                       
							<div class="insti-name">                           
							<h4>Pass out Year</h4>                                 
							</div>                                  
							</div>                                  
							<div class="col-xs-12 col-md-8 col-sm-8">    
							<div class="degree-info">                                            <h4><?=$profile->educations[0]->DurationTo;?></h4>                                        </div>                                    </div>                                </div>							                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Higest Qualification</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4>    <?=$profile->educations[0]->nqual->name;?><?php //$profile->educations[0]->course->CourseName;?></h4>
                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Course</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4> <?=$profile->educations[0]->ncourse->name;?> <?php //$profile->educations[0]->course->CourseName;?></h4>
                                             </div>
                                    </div>                                </div>                           
                                    <div class="row education-box">                                    <div class="col-md-4 col-xs-12 col-sm-4">                
                                    <div class="resume-icon">                                    
                                    <i class="fa fa-files-o" aria-hidden="true"></i>         
                                    </div>                                   
                                    <div class="insti-name">                        
                                    <h4>Specialization</h4>                           
                                    </div>                                    </div>     
                                    <div class="col-xs-12 col-md-8 col-sm-8">              
                                    <div class="degree-info">                                  
                                    <h4><?=$profile->educations[0]->specialization_id;?></h4>     
                                    </div>                                 
                                    </div>               
                                    </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>University / College</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$profile->educations[0]->University;?></h4>
                                             </div>
                                    </div>
                                </div>
								
							
                            </div>
    </div>
			<div class="widget">
			<?php				if($profile->experiences)
				{
						foreach($profile->experiences as $k=>$val)
						{
				?>               <div class="widget-header">              <h3 class="widget-caption">  Work Experience   <span><?=$val->YearFrom;?> to <?=$val->YearTo;?></span></h3>            </div> 			      <div class="resume-box">                                 <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                           <i class="fa fa-file-text" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Company name</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->CompanyName;?></h4>
                                        </div>
                                    </div>
                                </div>                                                                <div class="row education-box">                                    <div class="col-md-4 col-xs-12 col-sm-4">                                        <div class="resume-icon">                                           <i class="fa fa-file-text" aria-hidden="true"></i>                                        </div>                                        <div class="insti-name">                                            <h4>Position</h4>                                        </div>                                    </div>                                    <div class="col-xs-12 col-md-8 col-sm-8">                                        <div class="degree-info">                                            <h4><?=$val->PositionName;?></h4>                                        </div>                                    </div>                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Job Category</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->whitecategory->name;?></h4>
                                        </div>
                                    </div>
                                </div>
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                            <i class="fa fa-files-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Role</h4> 
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                             <?php $expRole = ArrayHelper::map(ExperienceWhiteRole::find()->where([
																		'user_id' => $profile->UserId,
																		'experience_id' => $val->ExperienceId
																	])->all(), 'role_id', 'role'); ?>
											<h4><?php echo (!empty($expRole)?implode(', ',$expRole):"");?></h4>
                                        </div>
                                    </div>
                                </div>                                                                
								<div class="row education-box">
								<div class="col-md-4 col-xs-12 col-sm-4">
									<div class="resume-icon">
										<i class="fa fa-briefcase" aria-hidden="true"></i>
									</div>
									<div class="insti-name">
										<h4>Skills</h4>
									</div>
								</div>
								<div class="col-xs-12 col-md-8 col-sm-8">
									<div class="degree-info">
									<?php $expSkills = ArrayHelper::map(ExperienceWhiteSkill::find()->where([
																	'user_id' => $profile->UserId,
																	'experience_id' => $val->ExperienceId
																])->all(), 'skill_id', 'skill');?>
                                   <h4><?php echo $expSkills ? implode(", ",$expSkills) : "N/A";?></h4>
								</div>
								</div>
							</div>
								
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Industry</h4>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
										  <div class="degree-info">
                                           <?php
											if($val->industry)
											{
											?>
                                            <h4><?=$val->industry->IndustryName;?></h4>
											<?php
											}
											?>
                                          </div>
                                    </div>
                                </div>
                                <div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Experience</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4>> <?=$val->Experience;?> Year</h4>
                                             </div>
                                    </div>
                                </div>
								<div class="row education-box">
                                    <div class="col-md-4 col-xs-12 col-sm-4">
                                        <div class="resume-icon">
                                              <i class="fa fa-briefcase" aria-hidden="true"></i>
                                        </div>
                                        <div class="insti-name">
                                            <h4>Salary</h4>
                                           
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-8 col-sm-8">
                                        <div class="degree-info">
                                            <h4><?=$val->Salary;?> Lakh</h4>
                                             </div>
                                    </div>
                                </div>
				
				      </div>
				  <?php
						}
				}
				?>
			
			</div>
			
			<style>
		     	.clear{clear:both; overflow:hidden;margin:10px 0;      padding: 0 0 6px 0;   border-bottom: 1px solid #cccccc}
			    .widget-likes{clear:both; overflow:hidden;padding:10px 20px; background:#fff;}
			    .team_mmb_ph{width:75px;height:75px;border-radius:100px; float:left; margin:0 20px 0 0;}
			    .team_mmb_det{float:left; width:400px}
			    .team_mmb_det h2{font-size:12px;margin:0; padding: 0 0 7px 0;}
			    
			    
			</style>

						  <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Team Member</span></h3>
							</div>
			
							  <div class="widget-likes">
								<div class="row">
								  <div class="col-md-12">
								 
										  <?php
										 if($profile->teams)
										 { 
											//echo '<pre>';
											//print_r($profile->teams);die;
											 foreach($profile->teams as $tk=>$tval) 
											 { 											     /* echo "<pre>ss";											     print_r($tval->MemberPhoto);											     die(); */											     											     $teammemp = Documents::getImageByAttr($tval, 'MemberPhoto', 'memberphoto');
												/* if($tval->MemberPhoto!=0)
													  { 
														$teammemp=$url.$tval->memberphoto->Doc;
													  }
													  else
													  {
														$teammemp=$imageurl.'images/user.png';
													  } */
													  $qualification = NaukariQualData::find()
													   ->select('name')
													   ->where(['id' => $tval->MemberQualification])
													   ->one();
													  $course = NaukariQualData::find()
													   ->select('name')
													   ->where(['id' => $tval->MemberCourse])
													   ->one(); 
										 ?>
										     
										       
										       <img src="<?=$teammemp;?>" alt="image" class="team_mmb_ph"> 
										       <div class="team_mmb_det">
                                                    <h2> <?=$tval->MemberName;?>  </h2> 
                                                    <h2> <?=(isset($qualification->name)?$qualification->name:"");?>  </h2> 
                                                    <h2> <?=(isset($course->name)?$course->name:"");?>  </h2>  
                                                    <h2> <?=$tval->MemberSalary.' Lakh';?>  </h2> 
										        </div>
										        <div class="clear"></div>
										  <?php
											 }
										 }
										 ?>
								 
								  </div>
								</div>
							  </div>       
						  </div>	
							
							<div class="widget" style="display:none">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Followers</span></h3>
							</div>
			
			<div class="widget-like">
              <div class="row">
                <div class="col-md-12">
                  <ul class=" ">
                    <?php
                    $followers=$lik->getTotalfollow($profile->UserId);
                    if($followers)
                    {
                        foreach($followers as $fk=>$fvalue)
                        {
                            if($fvalue->likeFrom->UserTypeId==2 || $fvalue->likeFrom->UserTypeId==5)
							  {
									if($fvalue->likeFrom->PhotoId!=0)
									{
										  $ll=$url.$fvalue->likeFrom->photo->Doc;
									}
									else
									{
										  $ll=$imageurl.'images/user.png';
									}
							  }
							  else
							  {
							  if($fvalue->likeFrom->LogoId!=0)
							  {
									$ll=$url.$fvalue->likeFrom->logo->Doc;
							  }
							  else
							  {
									$ll=$imageurl.'images/user.png';
							  }
							  }
                     if($fvalue->likeFrom->UserTypeId==2){$link='searchcandidate';}elseif($fvalue->likeFrom->UserTypeId==3){$link='searchcompany';}elseif($fvalue->likeFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
                    ?>
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$fvalue->likeFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"><?=$fvalue->likeFrom->Name;?></a> </li>
					<?php
                        }
                    }
                    ?>
                  </ul>
                </div>
              </div>
            </div>       
						  </div>
							
							
							
								
				  
						  <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Comments</span></h3>
							</div>
			
			<div class="widget-comments">
              <div class="row">
                <div class="col-md-12">
                  <?php
				$lv=new LeaveComment();
				$allcomment=$lv->getComment($profile->UserId);
				if($allcomment)
				{
					foreach($allcomment as $ck=>$cval)
					{
						if($cval->commentFrom->UserTypeId==2 || $cval->commentFrom->UserTypeId==5)
						{
							$ll=($cval->commentFrom->PhotoId!=0)?$url.$cval->commentFrom->photo->Doc:$imageurl.'images/user.png';
						}
						elseif($cval->commentFrom->UserTypeId==3 || $cval->commentFrom->UserTypeId==4)
						{
							$ll=($cval->commentFrom->LogoId!=0)?$url.$cval->commentFrom->logo->Doc:$imageurl.'images/user.png';
						}
						if($cval->commentFrom->UserTypeId==2){$link='searchcandidate';}elseif($cval->commentFrom->UserTypeId==3){$link='searchcompany';}elseif($cval->commentFrom->UserTypeId==4){$link='searchcampus';}else{$link='searchteam';}
				?>
                  <ul class=" ">
						<li> <a href="<?= Url::toRoute([$link,'userid'=>$cval->commentFrom->UserId])?>"> <img src="<?=$ll;?>" alt="image"><?=$cval->commentFrom->Name;?></a> </li>
						<li class="comments-block"> 
		                  <p><?=$cval->Comment;?></p>
						  </li>              
                  </ul>
				<?php
					}
				}
				?>
                </div>
              </div>
            </div>
                              
                                 
						  </div>	
							
							
							
							
							
			   </div> 
			 </div>
        </div>
		 
		 
		 
		  
		<div class="col-md-3 padding-left">
		
		
		                      <div class="widget">	  
							   <div class="widget-header">
							     <h3 class="widget-caption">  Recent Job</span></h3>
							   </div>
		                       <div class="recent-widget-know"> 
								      <div class="rsw"> 
											<aside> 
												<div class="widget_block"> 
													<ul class="related-post">
													  <?php
													  if($recentjob)
													  {
													  foreach($recentjob as $jkey=>$jvalue)
													  {
													  ?>
														<li>
															<a href="<?= Url::toRoute(['site/jobdetail','JobId'=>$jvalue->JobId])?>"> <?=$jvalue->JobTitle;?></a>
															<span><i class="fa fa-suitcase"></i>Position:  <?=$jvalue->position->Position;?> </span>
															<span><i class="fa fa-calendar"></i>Place: <?=$jvalue->Location;?> </span>
															<span><i class="fa fa-clock-o"></i>Post Time: <?=date('d M Y, h:i A',strtotime($jvalue->OnDate));?> </span>
														</li>
													  <?php
													  }
													  }
													  ?>
														<a href="<?= Url::toRoute(['site/recentjob'])?>" class="sidebar_view_all"> View all  </a> 
													</ul>
												</div> 
											</aside>
                                  </div>
							  </div> 
							</div>
							
							
							
							
		                      <div class="widget">	  
							   <div class="widget-header">
							     <h3 class="widget-caption"> People you know</span></h3>
							   </div>
		                       <div class="widget-know"> 
								  <ul class=" ">
									<?php
									if($peopleyouknow)
									{
										  foreach($peopleyouknow as $pk=>$pval)
										  {
										  if($pval->PhotoId!=0)
										  {
											  $pimage=$url.$pval->photo->Doc;
										  }
										  else
										  {
											  $pimage=$imageurl.'images/user.png';
										  }
										  ?>
										<li> <a href=""> <img src="<?=$pimage;?>" alt="image"> <?=$pval->Name;?>
										<span>
										  <?php
										  if($pval->experiences)
										  {
												$tc=count($pval->experiences);
												echo $pval->experiences[$tc-1]->position->Position.' at '.$pval->experiences[$tc-1]->CompanyName;
										  }
										  ?>
										</span></a> </li>
										<?php
										  }
									}
									?>
								  </ul>  
							  </div> 
							</div>
							
							
							
							
							 <div class="widget">	  
							   <div class="widget-header">
							  <h3 class="widget-caption"> Top Recruiter Company</span></h3>
							</div>
		                  <div class="widget-know"> 
								  <ul class=" ">
										  <?php
										  if($topcompany)
										  {
												foreach($topcompany as $tk=>$tval)
												{
												if($tval->LogoId!=0)
												{
													$pimagel=$url.$tval->logo->Doc;
												}
												else
												{
													$pimagel=$imageurl.'images/user.png';
												}  
										  ?>
										<li> <a href="<?= Url::toRoute(['searchcompany','userid'=>$tval->UserId])?>"> <img src="<?=$pimagel;?>" style="width: 50px;height: 50px;" alt="image"><?=$tval->Name;?></a>
										<span><a href="<?= Url::toRoute(['follower','userid'=>$tval->UserId]);?>"><?=count($lik->getTotalfollow($tval->UserId));?> Followers</span></a> </li>
										<?php
										  }
										  }
										  ?>
								  </ul> 
								  
							</div> 
							</div>
			
		  </div>
		  
		  
		  
		  
		  
         
      </div>
    </div>
 </div>
     </div>

<!-- Modal -->
<div class="modal fade" id="myModalcvupload" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Quick Review and update your profile</h4>
			</div>

			<div class="row main">
				<div class="xs-12 col-sm-12 main-center">
		<?php $form = ActiveForm::begin(['options' => ['class' => 'row','enctype'=>'multipart/form-data']]); ?>
		<div class="form-group" id="delete-resume" style="float:right; padding-top:20px;">
			<?php
				if ($profile->CVId != 0) {
					$na = explode(" ", $profile->Name);
					$name = $na[0];
					$extar = explode(".", $profile->cV->Doc);
					$ext = $extar[1];
					?>
					<a href='<?=$url.$profile->cV->Doc;?>' class="cv" style="position:unset!important;" 
					download="<?=$name.'.'.$ext;?>"><?=$profile->Name;?> Resume</a>
					
					<a class="remove-resume" href="javascript:void(0);">
									&nbsp;&nbsp;<i style="color:red" class="fa fa-remove" aria-hidden="true"></i>
								</a>
				<?php
					}
				?>
		</div>
		<div class="form-group">
		<?= $form->field($profile, 'CVId')->fileInput(['required'=>true,'accept'=>'.doc, .docx,.rtf,.pdf','id'=>'cvid'])->hint('doc,docx,pdf,rtf - 2MB max') ?>
		</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
									<?= Html::submitButton('Submit', ['class' => 'btn btn-default btn-green']) ?>
									</div>
					</div>
		<?php ActiveForm::end(); ?>
		</div>
			</div>

		</div>
	</div>
</div>
<!-- Modal -->

<script type="text/javascript">
	/*Remove Resume*/
	$(document).on('click', '.remove-resume', function(){
		$.ajax({
			dataType : "json",
			type : 'GET',
			url : '<?=Url::toRoute(['team/deleteuserresume']);?>',
			data : false,
			success : function(data) {
				if(data == true){
					$('#delete-resume').html("");
				}				
			}
		});		
	});
</script>