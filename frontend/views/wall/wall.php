<?php
$this->title = 'MCB Wall, My Career Bugs - mycareerbugs.com';

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$imageurl=Yii::$app->getUrlManager()->getBaseUrl().'/';
$url='/backend/web/';
?> 
<script type="application/ld+json">           
  {              
  "@context" : "http://schema.org",       
  "@type" : "Organization",       
  "name" : "My Career Bugs",          
  "url" : "http://mycareerbugs.com/wall/",   
  "sameAs" : [ "https://www.facebook.com/mycareerbugs", "https://twitter.com/mycareerbugs",  "https://in.linkedin.com/jobs/my-career-bugs-jobs"  ]
  } 
  </script> 
    <!-- Begin page content -->
    <div id="landing_page_banner" class="page-content" style="height:100vh">
      
	  	<div class="white"> 
		    <div class="step form" style="display:block">
          <img src="<?=$imageurl;?>careerimg/wall.png" alt="My Career Bugs Wall"> 
	            <div id="landing_page_banner_button"> 
					<div class="head_right"> 
					   <ul>    
                           <li><a class="btn-123" href="<?= Url::toRoute(['site/register'])?>">Sign Up</a></li>					   
						   <li  class="messages"><a class="btn-123" href="#"> Login </a></li>
						     
					   </ul> 
			    	</div> 	
				</div> 		 
                 <div class="directory-info-row " style="width:210px; margin:30px auto 0; display:block">	
					  <ul class="social-links">
						  <li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						  <li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						  <li><a href="#" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
						  <li><a href="#" title="Skype"><i class="fa fa-skype"></i></a></li>
						  <li><a href="#" title="Blog"><i class="fa fa-bold"></i> </a></li>
						  <li><a href="#" title="Blog"><i class="fa fa-envelope"></i> </a></li>
					  </ul>      
				 </div>  
					 </div>  
					 
				   <div class="step form">
						 <div class="omb_login"> 
						
						<h6>Login<h6>

						<div class="row omb_row-sm-offset-3">
							<div class="col-xs-12 col-sm-6">	
								<?php $form = ActiveForm::begin(['options' => ['class' => 'omb_loginForm','enctype'=>'multipart/form-data']]); ?>
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-user"></i></span>
										<input type="email" class="form-control" name="AllUser[Email]" required placeholder="Email address">
									</div>
								 					
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-lock"></i></span>
										<input  type="password" class="form-control" name="AllUser[Password]" required placeholder="Password">
									</div>
								 
									 <!-- <span class="help-block">Password error</span>  -->

									<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
								<?php ActiveForm::end(); ?>
							</div>
						</div>
						<div class="row omb_row-sm-offset-3">
							<div class="col-xs-12 col-sm-3">
								  <p class="center-left"> Not Yet Register ?  <a href="<?= Url::toRoute(['site/register'])?>" class="color"> Register Now </a></p>
							</div>
							<div class="col-xs-12 col-sm-3">
								<p class="omb_forgotPwd">
									<a href="<?= Url::toRoute(['wall/forgotpassword'])?>">Forgot password?</a>
								</p>
							</div>
						</div>
					 
						
						<div class="row omb_row-sm-offset-3 omb_loginOr">
							<div class="col-xs-12 col-sm-6"> 
							<hr class="omb_hrOr">
								<span class="omb_spanOr">or</span>
							</div>
						</div>

						
							<!--<div class="row omb_row-sm-offset-3 omb_socialButtons">
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-facebook">
										<i class="fa fa-facebook visible-xs"></i>
										<span class="hidden-xs">Facebook</span>
									</a>
								</div>
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-twitter">
										<i class="fa fa-twitter visible-xs"></i>
										<span class="hidden-xs">Twitter</span>
									</a>
								</div>	
								<div class="col-xs-4 col-sm-2">
									<a href="#" class="btn btn-lg btn-block omb_btn-google">
										<i class="fa fa-google-plus visible-xs"></i>
										<span class="hidden-xs">Google+</span>
									</a>
								</div>	
							</div>-->
							<?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>
					</div>
						 
					</div>
 
 <style>
  ul.auth-clients .linkedin.auth-link{display:none;}
  ul.auth-clients{    width: 85px;margin: 24px auto 0;clear: both;overflow: hidden;padding: 0;}
  .auth-clients li{ margin: 0 5px;}
 .omb_hrOr {
    background-color: #cdcdcd;
    height: 1px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
}
 </style>
 

						 
		 <div class="clear"></div>
		  
		 
      </div>
    </div>
     </div>